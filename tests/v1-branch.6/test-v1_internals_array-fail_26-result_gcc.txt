 ##############################################################################
 # Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

: In instantiation of ‘struct tice::v1::error::array::make::from_tuple::value::element_has_value_as_constexpr_copyable_data_member<false, 6ul, {anonymous}::Element<(<unnamed>::Element_type)0u, int> >’:$
^v1/test-v1_internals_array-fail_26.cpp:38:85:   required from here$
: error: static assertion failed: Element has no constexpr (not static or not constexpr) copyable (copy constructor maybe deleted or not constexpr) data member `value'$
