 ##############################################################################
 # Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

: In instantiation of ‘struct tice::v1::error::program::sequence_of_ETE_delay_is_followed_only_by_sequence_of_Correlation<false, 9ul, tice::v1::Node<tice::v1::comp::Unit<std::integral_constant<void (\*)(int), fn4>, std::ratio<1l, 100l> >, std::ratio<1l> > >’:$
^v1/test-v1_internals_program-fail_20.cpp:49:39:   required from here$
: error: static assertion failed: Sequence of tice::v1::ETE_delay class templates can only be followed by sequence of tice::v1::Correlation class templates$
