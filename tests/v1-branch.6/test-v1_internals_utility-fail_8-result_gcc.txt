 ##############################################################################
 # Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

: In instantiation of ‘struct tice::v1::internals::error::utility::list::update::element_update_has_value_as_member_typedef<false, 2ul, std::integral_constant<int, 2>, void, {anonymous}::update_element<I, 2ul, std::integral_constant<int, 2>, void> >’:$
^v1/test-v1_internals_utility-fail_8.cpp:42:126:   required from here$
: error: static assertion failed: \[DEBUG\] Update to previous list element at this index given updater data has no member typedef `value'$
