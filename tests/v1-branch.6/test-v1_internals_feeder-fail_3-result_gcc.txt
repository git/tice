 ##############################################################################
 # Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

: In instantiation of ‘struct tice::v1::error::feeder::arg_is_channel<false, 2ul, tice::v1::Node<tice::v1::comp::Unit<std::integral_constant<int (\*)(), fn2>, std::ratio<1l> >, std::ratio<4l, 3l> > >’:$
^v1/test-v1_internals_feeder-fail_3.cpp:50:85:   required from here$
: error: static assertion failed: Channel is not specified using class template tice::v1::Chan_inlit or tice::v1::Chan$
