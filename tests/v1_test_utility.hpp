/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <type_traits>

// The presence of an opening parenthesis following `H' will force Emacs to
// indent a comma-separated list of `H' actual parameters that are distributed
// over multiple lines in a single column whose left-most vertical boundary is
// located at the opening parenthesis.  The use of this macro shall have no
// side-effect unless some of the actual parameters use one or more macros, in
// which case the use of this macro will force those macros to be replaced
// earlier than they should.
#define H(...)                                  \
  __VA_ARGS__

namespace tice {
  namespace v1 {
    namespace tests {
      namespace utility {

        template<typename arg__1, typename arg__2>
        struct template_args_are_equal {
          static_assert(std::is_same<arg__1, arg__2>::value,
                        "Template args are not equal");
        };

      }
      namespace utility {

        template<typename arg__metaprogram>
        struct run {
          static_assert(sizeof(arg__metaprogram) >= 0 /* this must always be true */,
                        "C++ metaprogram is run");
        };

      }
    }
  }
}
