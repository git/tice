 ##############################################################################
 # Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

: fatal error: static_assert failed "Consumer node is not specified using class template tice::v1::Node"$
: note: in instantiation of template class 'tice::v1::error::feeder::arg_is_consumer_node<false, 3, std::ratio<1, 2> >' requested here$
^v1/test-v1_internals_feeder-fail_1.cpp:35:3: note: .\+ requested here$
