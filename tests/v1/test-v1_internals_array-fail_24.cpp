/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

#include "test-v1_internals_array-common.hpp"

namespace {

  template<Element_type type = ACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER>
  using Test_element = Element<type>;

}

int main() {
  using namespace tice::v1;
  tests::utility::run<array::make::from_tuple::value<Tuple<Test_element<>,
                                                           Test_element<>,
                                                           Test_element<>,
                                                           Test_element<MISSING>,
                                                           Test_element<>,
                                                           Test_element<>>>>();
}
