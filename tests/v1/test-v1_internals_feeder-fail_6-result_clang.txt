 ##############################################################################
 # Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

: fatal error: static_assert failed "Producer's return value is not assignable to channel"$
: note: in instantiation of template class 'tice::v1::error::feeder::producer_return_value_is_assignable_to_channel<false, 1, 2, R1, int>' requested here$
^v1/test-v1_internals_feeder-fail_6.cpp:46:3: note: .\+ requested here$
