/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

double fn1() {
  return .24;
}

double fn2(double arg1) {
  return arg1;
}

double fn3(double arg1, int arg2) {
  return arg1 / arg2;
}

void fn4() {
}

void fn5(double arg1) {
}

void fn6(double arg1, int arg2) {
}

int main() {
  using namespace tice::v1;
  typedef Comp(&fn1, Ratio<1>) comp1;
  typedef Comp(&fn2, Ratio<1, 3>) comp2;
  typedef Comp(&fn3, Ratio<1, 12>) comp3;
  typedef Comp(&fn4, Ratio<7, 8>) comp4;
  typedef Comp(&fn5, Ratio<4, 3>) comp5;
  typedef Comp(&fn6, Ratio<9, 8>) comp6;
  typedef Node<comp1, Ratio<4, 3>> node1;
  typedef Node<comp2, Ratio<4, 3>> node2;
  typedef Node<comp3, Ratio<4, 3>> node3;
  typedef Node<comp4, Ratio<4, 3>> node4;
  typedef Node<comp5, Ratio<4, 3>> node5;
  typedef Node<comp6, Ratio<4, 3>> node6;
  tests::utility::run<node1>();
  tests::utility::run<node2>();
  tests::utility::run<node3>();
  tests::utility::run<node4>();
  tests::utility::run<node5>();
  tests::utility::run<node6>();

  tests::utility::template_args_are_equal<
    Tuple<typename node1::type, typename node1::period, typename node1::wcet,
          typename node1::fn_info::return_type, typename node1::fn_info::param_types, typename node1::fn_info::fn_ptr>,
    Tuple<node::type::SENSOR, Ratio<4, 3>, Ratio<1>, double, internals::tuple::construct<I>, Value<double(*)(), &fn1>>>();

  tests::utility::template_args_are_equal<
    Tuple<typename node2::type, typename node2::period, typename node2::wcet,
          typename node2::fn_info::return_type, typename node2::fn_info::param_types, typename node2::fn_info::fn_ptr>,
    Tuple<node::type::INTERMEDIARY, Ratio<4, 3>, Ratio<1, 3>, double, internals::tuple::construct<I, double>, Value<double(*)(double), &fn2>>>();

  tests::utility::template_args_are_equal<
    Tuple<typename node3::type, typename node3::period, typename node3::wcet,
          typename node3::fn_info::return_type, typename node3::fn_info::param_types, typename node3::fn_info::fn_ptr>,
    Tuple<node::type::INTERMEDIARY,
          Ratio<4, 3>, Ratio<1, 12>, double, internals::tuple::construct<I, double, int>, Value<double(*)(double, int), &fn3>>>();

  tests::utility::template_args_are_equal<
    Tuple<typename node4::type, typename node4::period, typename node4::wcet,
          typename node4::fn_info::return_type, typename node4::fn_info::param_types, typename node4::fn_info::fn_ptr>,
    Tuple<node::type::SENSOR, Ratio<4, 3>, Ratio<7, 8>, void, internals::tuple::construct<I>, Value<void(*)(), &fn4>>>();

  tests::utility::template_args_are_equal<
    Tuple<typename node5::type, typename node5::period, typename node5::wcet,
          typename node5::fn_info::return_type, typename node5::fn_info::param_types, typename node5::fn_info::fn_ptr>,
    Tuple<node::type::ACTUATOR, Ratio<4, 3>, Ratio<4, 3>, void, internals::tuple::construct<I, double>, Value<void(*)(double), &fn5>>>();

  tests::utility::template_args_are_equal<
    Tuple<typename node6::type, typename node6::period, typename node6::wcet,
          typename node6::fn_info::return_type, typename node6::fn_info::param_types, typename node6::fn_info::fn_ptr>,
    Tuple<node::type::ACTUATOR, Ratio<4, 3>, Ratio<9, 8>, void, internals::tuple::construct<I, double, int>, Value<void(*)(double, int), &fn6>>>();
}
