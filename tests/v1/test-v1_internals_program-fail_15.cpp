/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

int fn1() {
  return 3;
}

int fn2() {
  return 4;
}

int fn3(int a, int b) {
  return a + b;
}

void fn4(int a) {
}

void fn5(int a) {
}

void fn6(int a) {
}

void fn7(int a) {
}

int main() {
  using namespace tice::v1;
  typedef Node<Comp(&fn1, Ratio<1, 100>), Ratio<1>> node1;
  typedef Node<Comp(&fn2, Ratio<1, 100>), Ratio<1>> node2;
  typedef Node<Comp(&fn3, Ratio<1, 100>), Ratio<1>> node3;
  typedef Node<Comp(&fn4, Ratio<1, 100>), Ratio<1>> node4;
  typedef Node<Comp(&fn5, Ratio<1, 100>), Ratio<1>> node5;
  typedef Node<Comp(&fn6, Ratio<1, 100>), Ratio<1>> node6;
  typedef Node<Comp(&fn7, Ratio<1, 100>), Ratio<1>> node7;
  tests::utility::run<Program<HW<Core_ids<>>,
                              node1, node2, node3, node4, node5, node6, node7,
                              Feeder<node1, Chan_inlit<int, 0>,
                                     node2, Chan_inlit<int, 0>, node3>,
                              Feeder<node3, Chan_inlit<int, 0>, node4>,
                              Feeder<node3, Chan_inlit<int, 0>, node5>,
                              Feeder<node1, Chan_inlit<int, 0>, node6>,
                              Feeder<node2, Chan_inlit<int, 0>, node7>,
                              ETE_delay<node1, node4, Ratio<0>, Ratio<1000>>,
                              ETE_delay<node2, node5, Ratio<0>, Ratio<1000>>,
                              Correlation<node7, Ratio<1000>, node2, node1>>>();
}
