/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

namespace {

  class Element_not_convertible_to_bool {
    int data;
  public:
    constexpr Element_not_convertible_to_bool(int data) : data(data) {
    }
  };

  template<tice::v1::dict::Key arg__key, int arg__value>
  struct dict_entry {
    static constexpr tice::v1::dict::Key key {arg__key};
    static constexpr Element_not_convertible_to_bool value {arg__value};
  };

}

int main() {
  using namespace tice::v1;
  tests::utility::run<array::update::value<Array<bool, 1, 0, 1, 0, 0>,
                                           Tuple<dict_entry<2, 1>>>>();
}
