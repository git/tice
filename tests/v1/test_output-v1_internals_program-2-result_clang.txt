	.text
	.file	"test_output-v1_internals_program-2.cpp"
	.globl	main                    # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	leaq	-8(%rbp), %rdi
	callq	_ZN4ProgC2Ev
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
                                        # -- End function
	.section	.text._ZN4ProgC2Ev,"axG",@progbits,_ZN4ProgC2Ev,comdat
	.weak	_ZN4ProgC2Ev            # -- Begin function _ZN4ProgC2Ev
	.p2align	4, 0x90
	.type	_ZN4ProgC2Ev,@function
_ZN4ProgC2Ev:                           # @_ZN4ProgC2Ev
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end1:
	.size	_ZN4ProgC2Ev, .Lfunc_end1-_ZN4ProgC2Ev
	.cfi_endproc
                                        # -- End function

	.ident	"clang .\+"
	.section	".note.GNU-stack","",@progbits
	.addrsig
