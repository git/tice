/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

int main() {
  using namespace tice::v1;

  typedef HW<Core_ids<>> hw1;
  tests::utility::run<hw1>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, hw1::backend_is_enabled>, Value<bool, array::is_empty<hw1::core_ids>::value>>,
    Tuple<Value<bool, false>, Value<bool, true>>>();

  typedef HW<Core_ids<1>> hw2;
  tests::utility::run<hw2>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, hw2::backend_is_enabled>>,
    Tuple<Value<bool, true>>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<hw2::core_ids, Array<int, 1>>::value>,
    Value<bool, true>>();

  typedef HW<Core_ids<1, 2>> hw3;
  tests::utility::run<hw3>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, hw3::backend_is_enabled>>,
    Tuple<Value<bool, true>>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<hw3::core_ids, Array<int, 1, 2>>::value>,
    Value<bool, true>>();

  typedef HW<Core_ids<3, 1, 2>> hw4;
  tests::utility::run<hw4>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, hw4::backend_is_enabled>>,
    Tuple<Value<bool, true>>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<hw4::core_ids, Array<int, 3, 1, 2>>::value>,
    Value<bool, true>>();
}
