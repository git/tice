/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace {

  enum Element_type {MISSING,
                     ACCESSIBLE_MEMBER_TYPEDEF,
                     ACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER,
                     INACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER,
                     ACCESSIBLE_STATIC_NONCONST_DATA_MEMBER,
                     ACCESSIBLE_NONSTATIC_CONST_DATA_MEMBER,
                     ACCESSIBLE_NONSTATIC_NONCONST_DATA_MEMBER};

  template<Element_type dummy_type = ACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER, typename arg__type = int>
  struct Element;

  template<typename arg__type>
  struct Element<MISSING, arg__type> {
  };

  template<typename arg__type>
  struct Element<ACCESSIBLE_MEMBER_TYPEDEF, arg__type> {
    typedef arg__type value;
  };

  template<typename arg__type>
  struct Element<ACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER, arg__type> {
    static constexpr arg__type value {-1};
  };

  template<typename arg__type>
  struct Element<INACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER, arg__type> {
  private:
    static constexpr arg__type value {-1};
  };

  template<typename arg__type>
  struct Element<ACCESSIBLE_STATIC_NONCONST_DATA_MEMBER, arg__type> {
    static arg__type value;
  };

  template<typename arg__type>
  struct Element<ACCESSIBLE_NONSTATIC_CONST_DATA_MEMBER, arg__type> {
    const arg__type value {-1};
  };

  template<typename arg__type>
  struct Element<ACCESSIBLE_NONSTATIC_NONCONST_DATA_MEMBER, arg__type> {
    arg__type value;
  };

}
