/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <initializer_list>
#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

namespace {

  template<typename arg__Program__program>
  constexpr bool check_adjacency_list(std::initializer_list<tice::v1::dict::Key> expected_list) {
    tice::v1::array::Size node_count = arg__Program__program::front_end::adjacency_list::size;
    tice::v1::array::Idx i = 0, j = 0;
    for (auto itr = expected_list.begin(); itr != expected_list.end(); ++itr) {
      if (node_count == 0) {
        return false;
      }
      if (arg__Program__program::front_end::adjacency_list::elems[i][j++] != *itr) {
        return false;
      }
      if (*itr == tice::v1::internals::program::adjacency_list::EOL::value) {
        ++i;
        j = 0;
        --node_count;
      }
    }
    return node_count == 0;
  }

}
namespace {

  template<bool arg__is_producer_node_id, tice::v1::dict::Key arg__node_id>
  struct producer_node_id_checker_msg {
    static_assert(arg__is_producer_node_id,
                  "[DEBUG TESTCASE] The specified node ID is not a producer node ID");
  };

  template<bool arg__is_sink_node_id, tice::v1::dict::Key arg__node_id>
  struct sink_node_id_checker_msg {
    static_assert(arg__is_sink_node_id,
                  "[DEBUG TESTCASE] The specified node ID is not a sink node ID");
  };

  template<typename prog, tice::v1::dict::Key arg__producer_node_id, tice::v1::dict::Key arg__sink_node_id>
  struct producer_and_sink_are_connected :
    prog::front_end::template producer_and_sink_are_connected<I, arg__producer_node_id, arg__sink_node_id,
                                                              producer_node_id_checker_msg, sink_node_id_checker_msg> {
  };

}

int fn1() {
  return 3;
}

int fn2() {
  return 4;
}

int fn3(int a, int b) {
  return a + b;
}

void fn4(int a) {
}

void fn5(int a) {
}

void fn6(int a) {
}

void fn7(int a) {
}

void fn8() {
}

int fn9(int a) {
  return a;
}

void fn10(int a, int b) {
}

int main() {
  using namespace tice::v1;
  typedef Node<Comp(&fn1, Ratio<1, 101>), Ratio<1, 2>> node1;
  typedef Node<Comp(&fn2, Ratio<2, 102>), Ratio<2, 3>> node2;
  typedef Node<Comp(&fn3, Ratio<3, 103>), Ratio<3, 4>> node3;
  typedef Node<Comp(&fn4, Ratio<4, 104>), Ratio<4, 5>> node4;
  typedef Node<Comp(&fn5, Ratio<5, 105>), Ratio<5, 6>> node5;
  typedef Node<Comp(&fn6, Ratio<6, 106>), Ratio<6, 7>> node6;
  typedef Node<Comp(&fn7, Ratio<7, 107>), Ratio<7, 8>> node7;
  typedef Node<Comp(&fn8, Ratio<8, 108>), Ratio<8, 9>> node8;
  typedef Node<Comp(&fn9, Ratio<9, 109>), Ratio<9, 10>> node9;
  typedef Node<Comp(&fn10, Ratio<10, 110>), Ratio<10, 11>> node10;

#define Program(hw, node, ...) internals::program::construct<I, hw, node, internals::tuple::construct<I, __VA_ARGS__>>
#define Program_(hw, node) internals::program::construct<I, hw, node, internals::tuple::construct<I>>

  typedef Program(HW<Core_ids<>>, node1, node2) prog1;
  tests::utility::run<prog1>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, prog1::hw_desc::backend_is_enabled>, typename prog1::front_end::node_dict,
          Value<array::Size, prog1::front_end::node_count>, Value<array::Size, prog1::front_end::arc_count>>,
    Tuple<Value<bool, false>, internals::tuple::construct<I, internals::program::dict_Entry<I, 0, node1>,
                                                          internals::program::dict_Entry<I, 1, node2>>,
          Value<array::Size, 2>, Value<array::Size, 0>>>();
  static_assert(check_adjacency_list<prog1>({/*  0: */
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 1: */
                                             tice::v1::internals::program::adjacency_list::EOL::value}), "Unexpected adjacency list");
  tests::utility::template_args_are_equal<
    typename prog1::front_end::channel_list,
    internals::tuple::construct<I>>();
  tests::utility::template_args_are_equal<
    typename prog1::front_end::channel_idx_list,
    internals::tuple::construct<I, internals::pair::construct<I, internals::tuple::construct<I>,
                                                              internals::program::Array_idx_list<I>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I>>>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::is_sensor_node,
                                             Array<bool, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::is_intermediary_node,
                                             Array<bool, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::is_actuator_node,
                                             Array<bool, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::sensor_node_ids,
                                             Array<dict::Key, 0, 1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::intermediary_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::actuator_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::is_consumer_node,
                                             Array<bool, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::is_producer_node,
                                             Array<bool, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::is_isolated_node,
                                             Array<bool, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::is_source_node,
                                             Array<bool, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::is_sink_node,
                                             Array<bool, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::consumer_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::producer_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::isolated_node_ids,
                                             Array<dict::Key, 0, 1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::source_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog1::front_end::sink_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog1::front_end::node_wcet::elems[0] == 1 / (double) 101
                 && prog1::front_end::node_wcet::elems[1] == 2 / (double) 102)>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog1::front_end::node_period::elems[0] == 1 / (double) 2
                 && prog1::front_end::node_period::elems[1] == 2 / (double) 3)>,
    Value<bool, true>>();

  typedef Program(HW<Core_ids<>>, node2, node1) prog2;
  tests::utility::run<prog2>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, prog2::hw_desc::backend_is_enabled>, typename prog2::front_end::node_dict,
          Value<array::Size, prog2::front_end::node_count>, Value<array::Size, prog2::front_end::arc_count>>,
    Tuple<Value<bool, false>, internals::tuple::construct<I, internals::program::dict_Entry<I, 0, node2>,
                                                          internals::program::dict_Entry<I, 1, node1>>,
          Value<array::Size, 2>, Value<array::Size, 0>>>();
  static_assert(check_adjacency_list<prog2>({/*  0: */
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 1: */
                                             tice::v1::internals::program::adjacency_list::EOL::value}), "Unexpected adjacency list");
  tests::utility::template_args_are_equal<
    typename prog2::front_end::channel_list,
    internals::tuple::construct<I>>();
  tests::utility::template_args_are_equal<
    typename prog2::front_end::channel_idx_list,
    internals::tuple::construct<I, internals::pair::construct<I, internals::tuple::construct<I>,
                                                              internals::program::Array_idx_list<I>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I>>>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::is_sensor_node,
                                             Array<bool, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::is_intermediary_node,
                                             Array<bool, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::is_actuator_node,
                                             Array<bool, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::sensor_node_ids,
                                             Array<dict::Key, 0, 1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::intermediary_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::actuator_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::is_consumer_node,
                                             Array<bool, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::is_producer_node,
                                             Array<bool, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::is_isolated_node,
                                             Array<bool, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::is_source_node,
                                             Array<bool, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::is_sink_node,
                                             Array<bool, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::consumer_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::producer_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::isolated_node_ids,
                                             Array<dict::Key, 0, 1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::source_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog2::front_end::sink_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog2::front_end::node_wcet::elems[0] == 2 / (double) 102
                 && prog2::front_end::node_wcet::elems[1] == 1 / (double) 101)>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog2::front_end::node_period::elems[0] == 2 / (double) 3
                 && prog2::front_end::node_period::elems[1] == 1 / (double) 2)>,
    Value<bool, true>>();

  typedef Program(HW<Core_ids<>>,
                  node1, node2, node3, node4,
                  Feeder<H(node1, Chan_inlit<int, 0>,
                           node2, Chan_inlit<int, 0>, node3)>,
                  Feeder<node3, Chan_inlit<int, 0>, node4>) prog3;
  tests::utility::run<prog3>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, prog3::hw_desc::backend_is_enabled>, typename prog3::front_end::node_dict,
          Value<array::Size, prog3::front_end::node_count>, Value<array::Size, prog3::front_end::arc_count>>,
    Tuple<Value<bool, false>, internals::tuple::construct<I, internals::program::dict_Entry<I, 0, node1>,
                                                          internals::program::dict_Entry<I, 1, node2>,
                                                          internals::program::dict_Entry<I, 2, node3>,
                                                          internals::program::dict_Entry<I, 3, node4>>,
          Value<array::Size, 4>, Value<array::Size, 3>>>();
  static_assert(check_adjacency_list<prog3>({/*  0: */2,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 1: */2,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 2: */3,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 3: */
                                             tice::v1::internals::program::adjacency_list::EOL::value}), "Unexpected adjacency list");
  tests::utility::template_args_are_equal<
    typename prog3::front_end::channel_list,
    internals::tuple::construct<I, internals::program::Channel<I, 0, 2, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 1, 2, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 2, 3, Chan_inlit<int, 0>>>>();
  tests::utility::template_args_are_equal<
    typename prog3::front_end::channel_idx_list,
    internals::tuple::construct<I, internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 2, 0>>,
                                                              internals::program::Array_idx_list<I>>,
                                internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 2, 1>>,
                                                           internals::program::Array_idx_list<I>>,
                                internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 3, 2>>,
                                                           internals::program::Array_idx_list<I, 0, 1>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 2>>>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::is_sensor_node,
                                             Array<bool, true, true, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::is_intermediary_node,
                                             Array<bool, false, false, true, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::is_actuator_node,
                                             Array<bool, false, false, false, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::sensor_node_ids,
                                             Array<dict::Key, 0, 1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::intermediary_node_ids,
                                             Array<dict::Key, 2>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::actuator_node_ids,
                                             Array<dict::Key, 3>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::is_consumer_node,
                                             Array<bool, false, false, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::is_producer_node,
                                             Array<bool, true, true, true, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::is_isolated_node,
                                             Array<bool, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::is_source_node,
                                             Array<bool, true, true, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::is_sink_node,
                                             Array<bool, false, false, false, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::consumer_node_ids,
                                             Array<dict::Key, 2, 3>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::producer_node_ids,
                                             Array<dict::Key, 0, 1, 2>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::isolated_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::source_node_ids,
                                             Array<dict::Key, 0, 1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog3::front_end::sink_node_ids,
                                             Array<dict::Key, 3>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, producer_and_sink_are_connected<prog3, 0, 3>::value>,
          Value<bool, producer_and_sink_are_connected<prog3, 1, 3>::value>,
          Value<bool, producer_and_sink_are_connected<prog3, 2, 3>::value>>,
    Tuple<Value<bool, true>,
          Value<bool, true>,
          Value<bool, true>>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog3::front_end::node_wcet::elems[0] == 1 / (double) 101
                 && prog3::front_end::node_wcet::elems[1] == 2 / (double) 102
                 && prog3::front_end::node_wcet::elems[2] == 3 / (double) 103
                 && prog3::front_end::node_wcet::elems[3] == 4 / (double) 104)>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog3::front_end::node_period::elems[0] == 1 / (double) 2
                 && prog3::front_end::node_period::elems[1] == 2 / (double) 3
                 && prog3::front_end::node_period::elems[2] == 3 / (double) 4
                 && prog3::front_end::node_period::elems[3] == 4 / (double) 5)>,
    Value<bool, true>>();

  typedef Program(HW<Core_ids<>>,
                  node1, node2, node3, node4, node5,
                  Feeder<H(node1, Chan_inlit<int, 0>,
                           node2, Chan_inlit<int, 0>, node3)>,
                  Feeder<node3, Chan_inlit<int, 0>, node4>,
                  Feeder<node3, Chan_inlit<int, 0>, node5>) prog4;
  tests::utility::run<prog4>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, prog4::hw_desc::backend_is_enabled>, typename prog4::front_end::node_dict,
          Value<array::Size, prog4::front_end::node_count>, Value<array::Size, prog4::front_end::arc_count>>,
    Tuple<Value<bool, false>, internals::tuple::construct<I, internals::program::dict_Entry<I, 0, node1>,
                                                          internals::program::dict_Entry<I, 1, node2>,
                                                          internals::program::dict_Entry<I, 2, node3>,
                                                          internals::program::dict_Entry<I, 3, node4>,
                                                          internals::program::dict_Entry<I, 4, node5>>,
          Value<array::Size, 5>, Value<array::Size, 4>>>();
  static_assert(check_adjacency_list<prog4>({/*  0: */2,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 1: */2,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 2: */3, 4,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 3: */
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 4: */
                                             tice::v1::internals::program::adjacency_list::EOL::value}), "Unexpected adjacency list");
  tests::utility::template_args_are_equal<
    typename prog4::front_end::channel_list,
    internals::tuple::construct<I, internals::program::Channel<I, 0, 2, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 1, 2, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 2, 3, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 2, 4, Chan_inlit<int, 0>>>>();
  tests::utility::template_args_are_equal<
    typename prog4::front_end::channel_idx_list,
    internals::tuple::construct<I, internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 2, 0>>,
                                                              internals::program::Array_idx_list<I>>,
                                internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 2, 1>>,
                                                           internals::program::Array_idx_list<I>>,
                                internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 3, 2>,
                                                                                          internals::program::dict_Entry_array_idx<I, 4, 3>>,
                                                           internals::program::Array_idx_list<I, 0, 1>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 2>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 3>>>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::is_sensor_node,
                                             Array<bool, true, true, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::is_intermediary_node,
                                             Array<bool, false, false, true, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::is_actuator_node,
                                             Array<bool, false, false, false, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::sensor_node_ids,
                                             Array<dict::Key, 0, 1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::intermediary_node_ids,
                                             Array<dict::Key, 2>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::actuator_node_ids,
                                             Array<dict::Key, 3, 4>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::is_consumer_node,
                                             Array<bool, false, false, true, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::is_producer_node,
                                             Array<bool, true, true, true, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::is_isolated_node,
                                             Array<bool, false, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::is_source_node,
                                             Array<bool, true, true, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::is_sink_node,
                                             Array<bool, false, false, false, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::consumer_node_ids,
                                             Array<dict::Key, 2, 3, 4>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::producer_node_ids,
                                             Array<dict::Key, 0, 1, 2>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::isolated_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::source_node_ids,
                                             Array<dict::Key, 0, 1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog4::front_end::sink_node_ids,
                                             Array<dict::Key, 3, 4>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, producer_and_sink_are_connected<prog4, 0, 3>::value>, Value<bool, producer_and_sink_are_connected<prog4, 0, 4>::value>,
          Value<bool, producer_and_sink_are_connected<prog4, 1, 3>::value>, Value<bool, producer_and_sink_are_connected<prog4, 1, 4>::value>,
          Value<bool, producer_and_sink_are_connected<prog4, 2, 3>::value>, Value<bool, producer_and_sink_are_connected<prog4, 2, 4>::value>>,
    Tuple<Value<bool, true>                                , Value<bool, true>,
          Value<bool, true>                                , Value<bool, true>,
          Value<bool, true>                                , Value<bool, true>>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog4::front_end::node_wcet::elems[0] == 1 / (double) 101
                 && prog4::front_end::node_wcet::elems[1] == 2 / (double) 102
                 && prog4::front_end::node_wcet::elems[2] == 3 / (double) 103
                 && prog4::front_end::node_wcet::elems[3] == 4 / (double) 104
                 && prog4::front_end::node_wcet::elems[4] == 5 / (double) 105)>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog4::front_end::node_period::elems[0] == 1 / (double) 2
                 && prog4::front_end::node_period::elems[1] == 2 / (double) 3
                 && prog4::front_end::node_period::elems[2] == 3 / (double) 4
                 && prog4::front_end::node_period::elems[3] == 4 / (double) 5
                 && prog4::front_end::node_period::elems[4] == 5 / (double) 6)>,
    Value<bool, true>>();

  typedef Program(HW<Core_ids<>>,
                  node1, node2, node3, node4, node5, node6, node7,
                  Feeder<H(node1, Chan_inlit<int, 0>,
                           node2, Chan_inlit<int, 0>, node3)>,
                  Feeder<node3, Chan_inlit<int, 0>, node4>,
                  Feeder<node3, Chan_inlit<int, 0>, node5>,
                  Feeder<node1, Chan_inlit<int, 0>, node6>,
                  Feeder<node2, Chan_inlit<int, 0>, node7>) prog5;
  tests::utility::run<prog5>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, prog5::hw_desc::backend_is_enabled>, typename prog5::front_end::node_dict,
          Value<array::Size, prog5::front_end::node_count>, Value<array::Size, prog5::front_end::arc_count>>,
    Tuple<Value<bool, false>, internals::tuple::construct<I, internals::program::dict_Entry<I, 0, node1>,
                                                          internals::program::dict_Entry<I, 1, node2>,
                                                          internals::program::dict_Entry<I, 2, node3>,
                                                          internals::program::dict_Entry<I, 3, node4>,
                                                          internals::program::dict_Entry<I, 4, node5>,
                                                          internals::program::dict_Entry<I, 5, node6>,
                                                          internals::program::dict_Entry<I, 6, node7>>,
          Value<array::Size, 7>, Value<array::Size, 6>>>();
  static_assert(check_adjacency_list<prog5>({/*  0: */2, 5,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 1: */2, 6,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 2: */3, 4,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 3: */
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 4: */
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 5: */
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 6: */
                                             tice::v1::internals::program::adjacency_list::EOL::value}), "Unexpected adjacency list");
  tests::utility::template_args_are_equal<
    typename prog5::front_end::channel_list,
    internals::tuple::construct<I, internals::program::Channel<I, 0, 2, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 1, 2, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 2, 3, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 2, 4, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 0, 5, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 1, 6, Chan_inlit<int, 0>>>>();
  tests::utility::template_args_are_equal<
    typename prog5::front_end::channel_idx_list,
    internals::tuple::construct<I, internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 2, 0>,
                                                                                             internals::program::dict_Entry_array_idx<I, 5, 4>>,
                                                              internals::program::Array_idx_list<I>>,
                                internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 2, 1>,
                                                                                          internals::program::dict_Entry_array_idx<I, 6, 5>>,
                                                           internals::program::Array_idx_list<I>>,
                                internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 3, 2>,
                                                                                          internals::program::dict_Entry_array_idx<I, 4, 3>>,
                                                           internals::program::Array_idx_list<I, 0, 1>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 2>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 3>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 4>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 5>>>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::is_sensor_node,
                                             Array<bool, true, true, false, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::is_intermediary_node,
                                             Array<bool, false, false, true, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::is_actuator_node,
                                             Array<bool, false, false, false, true, true, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::sensor_node_ids,
                                             Array<dict::Key, 0, 1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::intermediary_node_ids,
                                             Array<dict::Key, 2>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::actuator_node_ids,
                                             Array<dict::Key, 3, 4, 5, 6>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::is_consumer_node,
                                             Array<bool, false, false, true, true, true, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::is_producer_node,
                                             Array<bool, true, true, true, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::is_isolated_node,
                                             Array<bool, false, false, false, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::is_source_node,
                                             Array<bool, true, true, false, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::is_sink_node,
                                             Array<bool, false, false, false, true, true, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::consumer_node_ids,
                                             Array<dict::Key, 2, 3, 4, 5, 6>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::producer_node_ids,
                                             Array<dict::Key, 0, 1, 2>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::isolated_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::source_node_ids,
                                             Array<dict::Key, 0, 1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog5::front_end::sink_node_ids,
                                             Array<dict::Key, 3, 4, 5, 6>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, producer_and_sink_are_connected<prog5, 0, 3>::value>, Value<bool, producer_and_sink_are_connected<prog5, 0, 4>::value>,
          Value<bool, producer_and_sink_are_connected<prog5, 0, 5>::value>, Value<bool, producer_and_sink_are_connected<prog5, 0, 6>::value>,
          Value<bool, producer_and_sink_are_connected<prog5, 1, 3>::value>, Value<bool, producer_and_sink_are_connected<prog5, 1, 4>::value>,
          Value<bool, producer_and_sink_are_connected<prog5, 1, 5>::value>, Value<bool, producer_and_sink_are_connected<prog5, 1, 6>::value>,
          Value<bool, producer_and_sink_are_connected<prog5, 2, 3>::value>, Value<bool, producer_and_sink_are_connected<prog5, 2, 4>::value>,
          Value<bool, producer_and_sink_are_connected<prog5, 2, 5>::value>, Value<bool, producer_and_sink_are_connected<prog5, 2, 6>::value>>,
    Tuple<Value<bool, true>                                , Value<bool, true>,
          Value<bool, true>                                , Value<bool, false>,
          Value<bool, true>                                , Value<bool, true>,
          Value<bool, false>                               , Value<bool, true>,
          Value<bool, true>                                , Value<bool, true>,
          Value<bool, false>                               , Value<bool, false>>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog5::front_end::node_wcet::elems[0] == 1 / (double) 101
                 && prog5::front_end::node_wcet::elems[1] == 2 / (double) 102
                 && prog5::front_end::node_wcet::elems[2] == 3 / (double) 103
                 && prog5::front_end::node_wcet::elems[3] == 4 / (double) 104
                 && prog5::front_end::node_wcet::elems[4] == 5 / (double) 105
                 && prog5::front_end::node_wcet::elems[5] == 6 / (double) 106
                 && prog5::front_end::node_wcet::elems[6] == 7 / (double) 107)>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog5::front_end::node_period::elems[0] == 1 / (double) 2
                 && prog5::front_end::node_period::elems[1] == 2 / (double) 3
                 && prog5::front_end::node_period::elems[2] == 3 / (double) 4
                 && prog5::front_end::node_period::elems[3] == 4 / (double) 5
                 && prog5::front_end::node_period::elems[4] == 5 / (double) 6
                 && prog5::front_end::node_period::elems[5] == 6 / (double) 7
                 && prog5::front_end::node_period::elems[6] == 7 / (double) 8)>,
    Value<bool, true>>();

  typedef Program(HW<Core_ids<>>,
                  node1, node2, node3, node4, node5, node6, node7, node8,
                  Feeder<H(node1, Chan_inlit<int, 0>,
                           node2, Chan_inlit<int, 0>, node3)>,
                  Feeder<node3, Chan_inlit<int, 0>, node4>,
                  Feeder<node3, Chan_inlit<int, 0>, node5>,
                  Feeder<node1, Chan_inlit<int, 0>, node6>,
                  Feeder<node2, Chan_inlit<int, 0>, node7>,
                  ETE_delay<node1, node4, Ratio<13, 10>, Ratio<3>>,
                  ETE_delay<node2, node5, Ratio<3, 2>, Ratio<7, 2>>,
                  Correlation<node4, Ratio<2, 3>, node1, node2>,
                  Correlation<node5, Ratio<2, 3>, node1, node2>) prog6;
  tests::utility::run<prog6>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, prog6::hw_desc::backend_is_enabled>, typename prog6::front_end::node_dict,
          Value<array::Size, prog6::front_end::node_count>, Value<array::Size, prog6::front_end::arc_count>>,
    Tuple<Value<bool, false>, internals::tuple::construct<I, internals::program::dict_Entry<I, 0, node1>,
                                                          internals::program::dict_Entry<I, 1, node2>,
                                                          internals::program::dict_Entry<I, 2, node3>,
                                                          internals::program::dict_Entry<I, 3, node4>,
                                                          internals::program::dict_Entry<I, 4, node5>,
                                                          internals::program::dict_Entry<I, 5, node6>,
                                                          internals::program::dict_Entry<I, 6, node7>,
                                                          internals::program::dict_Entry<I, 7, node8>>,
          Value<array::Size, 8>, Value<array::Size, 6>>>();
  static_assert(check_adjacency_list<prog6>({/*  0: */2, 5,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 1: */2, 6,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 2: */3, 4,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 3: */
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 4: */
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 5: */
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 6: */
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 7: */
                                             tice::v1::internals::program::adjacency_list::EOL::value}), "Unexpected adjacency list");
  tests::utility::template_args_are_equal<
    typename prog6::front_end::channel_list,
    internals::tuple::construct<I, internals::program::Channel<I, 0, 2, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 1, 2, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 2, 3, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 2, 4, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 0, 5, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 1, 6, Chan_inlit<int, 0>>>>();
  tests::utility::template_args_are_equal<
    typename prog6::front_end::channel_idx_list,
    internals::tuple::construct<I, internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 2, 0>,
                                                                                             internals::program::dict_Entry_array_idx<I, 5, 4>>,
                                                              internals::program::Array_idx_list<I>>,
                                internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 2, 1>,
                                                                                          internals::program::dict_Entry_array_idx<I, 6, 5>>,
                                                           internals::program::Array_idx_list<I>>,
                                internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 3, 2>,
                                                                                          internals::program::dict_Entry_array_idx<I, 4, 3>>,
                                                           internals::program::Array_idx_list<I, 0, 1>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 2>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 3>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 4>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 5>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I>>>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::is_sensor_node,
                                             Array<bool, true, true, false, false, false, false, false, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::is_intermediary_node,
                                             Array<bool, false, false, true, false, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::is_actuator_node,
                                             Array<bool, false, false, false, true, true, true, true, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::sensor_node_ids,
                                             Array<dict::Key, 0, 1, 7>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::intermediary_node_ids,
                                             Array<dict::Key, 2>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::actuator_node_ids,
                                             Array<dict::Key, 3, 4, 5, 6>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::is_consumer_node,
                                             Array<bool, false, false, true, true, true, true, true, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::is_producer_node,
                                             Array<bool, true, true, true, false, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::is_isolated_node,
                                             Array<bool, false, false, false, false, false, false, false, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::is_source_node,
                                             Array<bool, true, true, false, false, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::is_sink_node,
                                             Array<bool, false, false, false, true, true, true, true, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::consumer_node_ids,
                                             Array<dict::Key, 2, 3, 4, 5, 6>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::producer_node_ids,
                                             Array<dict::Key, 0, 1, 2>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::isolated_node_ids,
                                             Array<dict::Key, 7>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::source_node_ids,
                                             Array<dict::Key, 0, 1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog6::front_end::sink_node_ids,
                                             Array<dict::Key, 3, 4, 5, 6>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, producer_and_sink_are_connected<prog6, 0, 3>::value>, Value<bool, producer_and_sink_are_connected<prog6, 0, 4>::value>,
          Value<bool, producer_and_sink_are_connected<prog6, 0, 5>::value>, Value<bool, producer_and_sink_are_connected<prog6, 0, 6>::value>,
          Value<bool, producer_and_sink_are_connected<prog6, 1, 3>::value>, Value<bool, producer_and_sink_are_connected<prog6, 1, 4>::value>,
          Value<bool, producer_and_sink_are_connected<prog6, 1, 5>::value>, Value<bool, producer_and_sink_are_connected<prog6, 1, 6>::value>,
          Value<bool, producer_and_sink_are_connected<prog6, 2, 3>::value>, Value<bool, producer_and_sink_are_connected<prog6, 2, 4>::value>,
          Value<bool, producer_and_sink_are_connected<prog6, 2, 5>::value>, Value<bool, producer_and_sink_are_connected<prog6, 2, 6>::value>>,
    Tuple<Value<bool, true>                                , Value<bool, true>,
          Value<bool, true>                                , Value<bool, false>,
          Value<bool, true>                                , Value<bool, true>,
          Value<bool, false>                               , Value<bool, true>,
          Value<bool, true>                                , Value<bool, true>,
          Value<bool, false>                               , Value<bool, false>>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog6::front_end::node_wcet::elems[0] == 1 / (double) 101
                 && prog6::front_end::node_wcet::elems[1] == 2 / (double) 102
                 && prog6::front_end::node_wcet::elems[2] == 3 / (double) 103
                 && prog6::front_end::node_wcet::elems[3] == 4 / (double) 104
                 && prog6::front_end::node_wcet::elems[4] == 5 / (double) 105
                 && prog6::front_end::node_wcet::elems[5] == 6 / (double) 106
                 && prog6::front_end::node_wcet::elems[6] == 7 / (double) 107
                 && prog6::front_end::node_wcet::elems[7] == 8 / (double) 108)>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog6::front_end::node_period::elems[0] == 1 / (double) 2
                 && prog6::front_end::node_period::elems[1] == 2 / (double) 3
                 && prog6::front_end::node_period::elems[2] == 3 / (double) 4
                 && prog6::front_end::node_period::elems[3] == 4 / (double) 5
                 && prog6::front_end::node_period::elems[4] == 5 / (double) 6
                 && prog6::front_end::node_period::elems[5] == 6 / (double) 7
                 && prog6::front_end::node_period::elems[6] == 7 / (double) 8
                 && prog6::front_end::node_period::elems[7] == 8 / (double) 9)>,
    Value<bool, true>>();

  typedef Program(HW<Core_ids<>>,
                  node1, node2, node3, node4, node5, node6, node7, node8,
                  Feeder<H(node1, Chan_inlit<int, 0>,
                           node2, Chan_inlit<int, 0>, node3)>,
                  Feeder<node3, Chan_inlit<int, 0>, node4>,
                  Feeder<node3, Chan_inlit<int, 0>, node5>,
                  Feeder<node1, Chan_inlit<int, 0>, node6>,
                  Feeder<node2, Chan_inlit<int, 0>, node7>,
                  ETE_delay<node1, node4, Ratio<13, 10>, Ratio<3>>,
                  ETE_delay<node2, node5, Ratio<3, 2>, Ratio<7, 2>>) prog7;
  tests::utility::run<prog7>();

  typedef Program(HW<Core_ids<>>,
                  node1, node2, node3, node4, node5, node6, node7, node8,
                  Feeder<H(node1, Chan_inlit<int, 0>,
                           node2, Chan_inlit<int, 0>, node3)>,
                  Feeder<node3, Chan_inlit<int, 0>, node4>,
                  Feeder<node3, Chan_inlit<int, 0>, node5>,
                  Feeder<node1, Chan_inlit<int, 0>, node6>,
                  Feeder<node2, Chan_inlit<int, 0>, node7>,
                  Correlation<node4, Ratio<2, 3>, node1, node2>,
                  Correlation<node5, Ratio<2, 3>, node1, node2>) prog8;
  tests::utility::run<prog8>();

  typedef Program(HW<Core_ids<>>,
                  node1, node4, node9, node5, node10,
                  Feeder<node1, Chan_inlit<int, 0>, node4>,
                  Feeder<H(node1, Chan_inlit<int, 0>,
                           node9, Chan_inlit<int, 0>, node10)>,
                  Feeder<node1, Chan_inlit<int, 0>, node9>,
                  Feeder<node9, Chan_inlit<int, 0>, node5>) prog9;
  tests::utility::run<prog9>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, prog9::hw_desc::backend_is_enabled>, typename prog9::front_end::node_dict,
          Value<array::Size, prog9::front_end::node_count>, Value<array::Size, prog9::front_end::arc_count>>,
    Tuple<Value<bool, false>, internals::tuple::construct<I, internals::program::dict_Entry<I, 0, node1>,
                                                          internals::program::dict_Entry<I, 1, node4>,
                                                          internals::program::dict_Entry<I, 2, node9>,
                                                          internals::program::dict_Entry<I, 3, node5>,
                                                          internals::program::dict_Entry<I, 4, node10>>,
          Value<array::Size, 5>, Value<array::Size, 5>>>();
  static_assert(check_adjacency_list<prog9>({/*  0: */1, 4, 2,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 1: */
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 2: */4, 3,
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 3: */
                                             tice::v1::internals::program::adjacency_list::EOL::value
                                             ,/* 4: */
                                             tice::v1::internals::program::adjacency_list::EOL::value}), "Unexpected adjacency list");
  tests::utility::template_args_are_equal<
    typename prog9::front_end::channel_list,
    internals::tuple::construct<I, internals::program::Channel<I, 0, 1, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 0, 4, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 2, 4, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 0, 2, Chan_inlit<int, 0>>,
                                internals::program::Channel<I, 2, 3, Chan_inlit<int, 0>>>>();
  tests::utility::template_args_are_equal<
    typename prog9::front_end::channel_idx_list,
    internals::tuple::construct<I, internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 1, 0>,
                                                                                             internals::program::dict_Entry_array_idx<I, 4, 1>,
                                                                                             internals::program::dict_Entry_array_idx<I, 2, 3>>,
                                                              internals::program::Array_idx_list<I>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 0>>,
                                internals::pair::construct<I, internals::tuple::construct<I, internals::program::dict_Entry_array_idx<I, 4, 2>,
                                                                                          internals::program::dict_Entry_array_idx<I, 3, 4>>,
                                                           internals::program::Array_idx_list<I, 3>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 4>>,
                                internals::pair::construct<I, internals::tuple::construct<I>,
                                                           internals::program::Array_idx_list<I, 1, 2>>>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::is_sensor_node,
                                             Array<bool, true, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::is_intermediary_node,
                                             Array<bool, false, false, true, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::is_actuator_node,
                                             Array<bool, false, true, false, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::sensor_node_ids,
                                             Array<dict::Key, 0>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::intermediary_node_ids,
                                             Array<dict::Key, 2>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::actuator_node_ids,
                                             Array<dict::Key, 1, 3, 4>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::is_consumer_node,
                                             Array<bool, false, true, true, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::is_producer_node,
                                             Array<bool, true, false, true, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::is_isolated_node,
                                             Array<bool, false, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::is_source_node,
                                             Array<bool, true, false, false, false, false>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::is_sink_node,
                                             Array<bool, false, true, false, true, true>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::consumer_node_ids,
                                             Array<dict::Key, 1, 2, 3, 4>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::producer_node_ids,
                                             Array<dict::Key, 0, 2>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::isolated_node_ids,
                                             array::Empty<dict::Key>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::source_node_ids,
                                             Array<dict::Key, 0>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<prog9::front_end::sink_node_ids,
                                             Array<dict::Key, 1, 3, 4>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, producer_and_sink_are_connected<prog9, 0, 1>::value>, Value<bool, producer_and_sink_are_connected<prog9, 0, 3>::value>,
          Value<bool, producer_and_sink_are_connected<prog9, 0, 4>::value>,
          Value<bool, producer_and_sink_are_connected<prog9, 2, 1>::value>, Value<bool, producer_and_sink_are_connected<prog9, 2, 3>::value>,
          Value<bool, producer_and_sink_are_connected<prog9, 2, 4>::value>>,
    Tuple<Value<bool, true>                                , Value<bool, true>,
          Value<bool, true>                                ,
          Value<bool, false>                               , Value<bool, true>,
          Value<bool, true>                                                   >>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog9::front_end::node_wcet::elems[0] == 1 / (double) 101
                 && prog9::front_end::node_wcet::elems[1] == 4 / (double) 104
                 && prog9::front_end::node_wcet::elems[2] == 9 / (double) 109
                 && prog9::front_end::node_wcet::elems[3] == 5 / (double) 105
                 && prog9::front_end::node_wcet::elems[4] == 10 / (double) 110)>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, (prog9::front_end::node_period::elems[0] == 1 / (double) 2
                 && prog9::front_end::node_period::elems[1] == 4 / (double) 5
                 && prog9::front_end::node_period::elems[2] == 9 / (double) 10
                 && prog9::front_end::node_period::elems[3] == 5 / (double) 6
                 && prog9::front_end::node_period::elems[4] == 10 / (double) 11)>,
    Value<bool, true>>();

  {//\begin{10}
    extern int fn_v1();
    extern int fn_v2();
    extern int fn_v3(int);
    extern void fn_v4(int, int, int);

    typedef Node<Comp(&fn_v1, Ratio<1, 1000>), Ratio<2>> v1;
    typedef Node<Comp(&fn_v2, Ratio<1, 1000>), Ratio<1>> v2;
    typedef Node<Comp(&fn_v3, Ratio<1, 1000>), Ratio<2>> v3;
    typedef Node<Comp(&fn_v4, Ratio<1, 1000>), Ratio<2>> v4;

    typedef Program(HW<Core_ids<>>,
                    v1, v2, v3, v4,
                    Feeder<H(v1, Chan_inlit<int, 0>,
                             v2, Chan_inlit<int, 0>,
                             v3, Chan_inlit<int, 0>, v4)>,
                    Feeder<v1, Chan_inlit<int, 0>, v3>,
                    ETE_delay<v1, v4, Ratio<2>, Ratio<6>>,
                    Correlation<v4, Ratio<3>, v1, v2>) prog;
    tests::utility::run<prog>();
  }//\end{10}

  tests::utility::run<Program_(HW<Core_ids<0>>,
                               H(Node<Comp(&fn1, Ratio<1>), Ratio<1>>))>();
  tests::utility::run<Program(HW<Core_ids<0>>,
                              H(Node<Comp(&fn1, Ratio<2, 3>), Ratio<1>>),
                              Node<Comp(&fn2, Ratio<1, 3>), Ratio<1>>)>();
  tests::utility::run<Program(HW<Core_ids<0>>,
                              H(Node<Comp(&fn1, Ratio<2>), Ratio<3>>),
                              Node<Comp(&fn2, Ratio<1>), Ratio<3>>)>();

#undef Program
#undef Program_
}
