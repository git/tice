/******************************************************************************
 * Copyright (C) 2018  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <type_traits>
#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

namespace {
  constexpr int initval_constexpr = 16;
}

int main() {
  static constexpr int initval_static_constexpr = 8;
  static const int initval_static_const = 7;
  constexpr int initval_local_constexpr = 4;
  const int initval_local_const = 1;
  using namespace tice::v1;
  tests::utility::run<Chan_inlit<std::remove_cv_t<decltype(initval_constexpr)>, initval_constexpr>>();
  tests::utility::run<Chan_inlit<decltype(initval_constexpr > 8), (initval_constexpr > 8)>>();
  tests::utility::run<Chan_inlit<std::remove_cv_t<decltype(initval_constexpr)>, initval_constexpr - 8>>();
  tests::utility::run<Chan_inlit<std::remove_cv_t<decltype(initval_constexpr)>, 90 / 11>>();
  tests::utility::run<Chan_inlit<std::remove_cv_t<decltype(initval_constexpr)>, 11>>();
  tests::utility::run<Chan_inlit<long long, initval_constexpr>>();
  tests::utility::run<Chan_inlit<bool, (initval_constexpr > 8)>>();
  tests::utility::run<Chan_inlit<long long, initval_constexpr - 8>>();
  tests::utility::run<Chan_inlit<long long, 90 / 11>>();
  tests::utility::run<Chan_inlit<long long, 11>>();
  tests::utility::run<Chan_inlit<long long, initval_static_constexpr>>();
  tests::utility::run<Chan_inlit<long long, initval_static_const>>();
  tests::utility::run<Chan_inlit<long long, initval_local_constexpr>>();
  tests::utility::run<Chan_inlit<long long, initval_local_const>>();

  typedef Chan_inlit<std::remove_cv_t<decltype(initval_constexpr)>, initval_constexpr> chan1;
  tests::utility::template_args_are_equal<
    Tuple<chan1::type, chan1::initial_value>,
    Tuple<int, Value<int, 16>>>();

  typedef Chan_inlit<decltype(initval_constexpr > 8), (initval_constexpr > 8)> chan2;
  tests::utility::template_args_are_equal<
    Tuple<chan2::type, chan2::initial_value>,
    Tuple<bool, Value<bool, true>>>();

  typedef Chan_inlit<std::remove_cv_t<decltype(initval_constexpr)>, initval_constexpr - 8> chan3;
  tests::utility::template_args_are_equal<
    Tuple<chan3::type, chan3::initial_value>,
    Tuple<int, Value<int, 8>>>();

  typedef Chan_inlit<std::remove_cv_t<decltype(initval_constexpr)>, 90 / 11> chan4;
  tests::utility::template_args_are_equal<
    Tuple<chan4::type, chan4::initial_value>,
    Tuple<int, Value<int, 8>>>();

  typedef Chan_inlit<std::remove_cv_t<decltype(initval_constexpr)>, 11> chan5;
  tests::utility::template_args_are_equal<
    Tuple<chan5::type, chan5::initial_value>,
    Tuple<int, Value<int, 11>>>();

  typedef Chan_inlit<long long, initval_constexpr> chan6;
  tests::utility::template_args_are_equal<
    Tuple<chan6::type, chan6::initial_value>,
    Tuple<long long, Value<long long, 16>>>();

  typedef Chan_inlit<bool, (initval_constexpr > 8)> chan7;
  tests::utility::template_args_are_equal<
    Tuple<chan7::type, chan7::initial_value>,
    Tuple<bool, Value<bool, true>>>();

  typedef Chan_inlit<long long, initval_constexpr - 8> chan8;
  tests::utility::template_args_are_equal<
    Tuple<chan8::type, chan8::initial_value>,
    Tuple<long long, Value<long long, 8>>>();

  typedef Chan_inlit<long long, 90 / 11> chan9;
  tests::utility::template_args_are_equal<
    Tuple<chan9::type, chan9::initial_value>,
    Tuple<long long, Value<long long, 8>>>();

  typedef Chan_inlit<long long, 11> chan10;
  tests::utility::template_args_are_equal<
    Tuple<chan10::type, chan10::initial_value>,
    Tuple<long long, Value<long long, 11>>>();

  typedef Chan_inlit<long long, initval_static_constexpr> chan11;
  tests::utility::template_args_are_equal<
    Tuple<chan11::type, chan11::initial_value>,
    Tuple<long long, Value<long long, 8>>>();

  typedef Chan_inlit<long long, initval_static_const> chan12;
  tests::utility::template_args_are_equal<
    Tuple<chan12::type, chan12::initial_value>,
    Tuple<long long, Value<long long, 7>>>();

  typedef Chan_inlit<long long, initval_local_constexpr> chan13;
  tests::utility::template_args_are_equal<
    Tuple<chan13::type, chan13::initial_value>,
    Tuple<long long, Value<long long, 4>>>();

  typedef Chan_inlit<long long, initval_local_const> chan14;
  tests::utility::template_args_are_equal<
    Tuple<chan14::type, chan14::initial_value>,
    Tuple<long long, Value<long long, 1>>>();
}
