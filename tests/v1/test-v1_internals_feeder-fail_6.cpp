/******************************************************************************
 * Copyright (C) 2018  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

struct R1 {
  int value;
};

R1 fn1() {
  return {2};
}

int fn2() {
  return 17;
}

int fn3(R1 arg1, int arg2) {
  return arg1.value + arg2;
}

static const int initval = 8;

int main() {
  using namespace tice::v1;
  typedef Node<Comp(&fn1, Ratio<1>), Ratio<4, 3>> v_p_1;
  typedef Node<Comp(&fn2, Ratio<1>), Ratio<4, 3>> v_p_2;
  typedef Node<Comp(&fn3, Ratio<1>), Ratio<4, 3>> v_c;
    
  tests::utility::run<Feeder<v_p_1, Chan<int, &initval>,
                             v_p_2, Chan<int, &initval>, v_c>>();
}
