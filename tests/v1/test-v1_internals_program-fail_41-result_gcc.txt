 ##############################################################################
 # Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

: In instantiation of ‘struct tice::v1::error::program::gedf_schedulability_test_is_successful<false, std::ratio<1334073, 1000000>, std::ratio<2285715, 1000000>, std::ratio<1000000, 1000000>, std::ratio<571429, 1000000> >’:$
^v1/test-v1_internals_program-fail_41.cpp:54:58:   required from here$
: error: static assertion failed: gEDF (global earliest-deadline first) backend cannot realize the expressed Tice model because the total processor utilization is greater than its bound and/or the processor utilization of some task exceeds the maximum bound (Try reducing the WCETs of the function blocks first before redesigning the Tice model with greater periods and/or less nodes)$
