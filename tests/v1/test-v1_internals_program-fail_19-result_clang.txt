 ##############################################################################
 # Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

: fatal error: static_assert failed "Sequence of tice::v1::Correlation class templates cannot be followed by anything else"$
: note: in instantiation of template class 'tice::v1::error::program::sequence_of_Correlation_is_followed_by_nothing<false, 9, tice::v1::ETE_delay<tice::v1::Node<tice::v1::comp::Unit<std::integral_constant<int (\*)(), &fn1>, std::ratio<1, 100> >, std::ratio<1, 1> >, tice::v1::Node<tice::v1::comp::Unit<std::integral_constant<void (\*)(int), &fn4>, std::ratio<1, 100> >, std::ratio<1, 1> >, std::ratio<0, 1>, std::ratio<1000, 1> > >' requested here$
^v1/test-v1_internals_program-fail_19.cpp:43:3: note: .\+ requested here$
