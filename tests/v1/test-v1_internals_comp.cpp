/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

double fn1() {
  return .24;
}

static double fn2(double arg1) {
  return arg1;
}

double fn3(double arg1, int arg2) {
  return arg1 / arg2;
}

static void fn4() {
}

void fn3(double arg1) {
}

static void fn6(double arg1, int arg2) {
}

int main() {
  typedef Comp(&fn1, tice::v1::Ratio<1>) comp1;
  tice::v1::tests::utility::run<comp1>();
  typedef Comp(&fn2, tice::v1::Ratio<1>) comp2;
  tice::v1::tests::utility::run<comp2>();
  typedef tice::v1::comp::Unit<tice::v1::Value<double(*)(double, int), &fn3>, tice::v1::Ratio<1>> comp3;
  tice::v1::tests::utility::run<comp3>();
  using namespace tice::v1;
  typedef Comp(&fn4, Ratio<1>) comp4;
  tests::utility::run<comp4>();
  typedef tice::v1::comp::Unit<tice::v1::Value<void(*)(double), &fn3>, tice::v1::Ratio<1>> comp5;
  tests::utility::run<comp5>();
  typedef Comp(&fn6, Ratio<1>) comp6;
  tests::utility::run<comp6>();

  tests::utility::template_args_are_equal<
    Tuple<typename comp1::type, typename comp2::type, typename comp3::type, typename comp4::type, typename comp5::type, typename comp6::type>,
    Tuple<node::type::SENSOR, node::type::INTERMEDIARY, node::type::INTERMEDIARY, node::type::SENSOR, node::type::ACTUATOR, node::type::ACTUATOR>
    >();
}
