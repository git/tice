/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

namespace {

  template<typename, tice::v1::array::Idx idx, typename prev_list_element, typename update_data>
  struct update_element {
    static constexpr bool is_complete = false;
    typedef update_data data;
    typedef prev_list_element value;
  };

  template<typename I, typename prev_list_element, typename update_data>
  struct update_element<I, 2, prev_list_element, update_data> {
    static constexpr bool is_complete = false;
    typedef update_data data;
  };

}

int main() {
  using namespace tice::v1;
  tests::utility::run<internals::utility::list::update<I, internals::tuple::construct<I, Value<int, 0>, Value<int, 1>, Value<int, 2>, Value<int, 3>,
                                                                                      Value<int, 4>>, update_element, void>>();
}
