/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <limits>
#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

namespace {

  constexpr double x {1.7};

  template<const double &v>
  struct Double {
    typedef double value_type;
    static constexpr double value {v};
  };

}

int main() {
  using namespace tice::v1;

  typedef dict::Entry_key<0> entry0;
  tests::utility::template_args_are_equal<
    Value<entry0::value_type, entry0::value>,
    Value<dict::Key, 0>>();

  typedef dict::Entry<0, Tuple<Value<int, 1>, Value<int, 91>>> entry1;
  tests::utility::template_args_are_equal<
    Tuple<Value<dict::Key, entry1::key>, typename entry1::value>,
    Tuple<Value<dict::Key, 0>, Tuple<Value<int, 1>, Value<int, 91>>>>();

  typedef dict::Entry_int<0, -21> entry2;
  tests::utility::template_args_are_equal<
    Tuple<Value<dict::Key, entry2::key>, Value<typename entry2::value_type, entry2::value>>,
    Tuple<Value<dict::Key, 0>, Value<int, -21>>>();

  typedef dict::Entry<42, void, Double<x>> entry3;
  tests::utility::template_args_are_equal<
    Tuple<Value<dict::Key, entry3::key>, entry3::value_type>,
    Tuple<Value<dict::Key, 42>, double>>();
  tests::utility::template_args_are_equal<
    Value<bool, equal_lvalues(entry3::value, x)>,
    Value<bool, true>>();
}
