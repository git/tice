 ##############################################################################
 # Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

: fatal error: static_assert failed "Min and max end-to-end delays are not respected"$
: note: in instantiation of template class 'tice::v1::error::program::end_to_end_delay_is_between_min_and_max_delays<false, 8, std::ratio<2, 1>, std::ratio<6000000, 1000000>, std::ratio<4, 1>, tice::v1::error::Path_forming_node_pos<2, 4, 5>, 1>' requested here$
^v1/test-v1_internals_program-fail_43.cpp:45:3: note: .\+ requested here$
