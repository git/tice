 ##############################################################################
 # Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

: fatal error: static_assert failed "Arg 2 has no member typedef `element_type' (maybe not using tice::v1::Array in the first place)"$
: note: in instantiation of template class 'tice::v1::error::array::are_elementwise_equal::arg_2_has_element_type_as_member_typedef<false, tice::v1::Tuple<> >' requested here$
^v1/test-v1_internals_array-fail_43.cpp:24:3: note: .\+ requested here$
