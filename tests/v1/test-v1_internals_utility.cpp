/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

#include "test-v1_internals_utility-common.hpp"

namespace {

  template<typename, tice::v1::array::Idx idx>
  struct get_element_1 {
    typedef tice::v1::Value<int, idx * 100> value;
  };

}
namespace {

  template<typename, tice::v1::array::Idx idx, typename prev_list_element, typename update_data>
  struct update_element_1 {
    static constexpr bool is_complete = false;
    typedef tice::v1::Value<int, prev_list_element::value> data;
    typedef tice::v1::Value<int, update_data::value - prev_list_element::value> value;
  };

}
namespace {

  template<typename, tice::v1::array::Idx idx, typename prev_list_element, typename update_data>
  struct update_element_2 {
    static constexpr bool is_complete = idx == 2;
    typedef update_data data;
    typedef tice::v1::Value<int, 10 * prev_list_element::value> value;
  };

}
namespace {

  template<typename, tice::v1::array::Idx idx, typename prev_list_element, typename update_data>
  struct update_element_3 {
    static constexpr bool is_complete = idx == 0;
    typedef update_data data;
    typedef tice::v1::Value<int, 10 + prev_list_element::value> value;
  };

}
namespace {

  template<typename, tice::v1::array::Idx idx, typename element, typename iterator_data>
  struct iterator_1;

  template<typename I, tice::v1::array::Idx idx, typename element, typename... args>
  struct iterator_1<I, idx, element, tice::v1::internals::tuple::construct<I, args...>> {
    typedef tice::v1::internals::tuple::construct<I, tice::v1::Value<int, 10 + idx>, args...> value;
  };

}
namespace {

  template<bool v>
  struct accept_bool {
    static constexpr bool value = v;
  };

}
namespace {

  enum member_test_dummy_type {MISSING,
                               ACCESSIBLE_MEMBER_CLASS,
                               ACCESSIBLE_CONSTEXPR_MEMBER_FUNCTION,
                               ACCESSIBLE_STATIC_CONSTEXPR_MEMBER_FUNCTION,
                               ACCESSIBLE_MEMBER_TYPEDEF,
                               INACCESSIBLE_MEMBER_TYPEDEF,
                               ACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER,
                               INACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER,
                               ACCESSIBLE_STATIC_NONCONST_DATA_MEMBER,
                               ACCESSIBLE_NONSTATIC_CONST_DATA_MEMBER,
                               ACCESSIBLE_NONSTATIC_NONCONST_DATA_MEMBER};

#define generate(ident, suffix, /* initial value */ ...)                                                                                           \
                                                                                                                                                   \
  template<member_test_dummy_type dummy_type, typename arg__type>                                                                                  \
  struct ident##_member_test_dummy##suffix;                                                                                                        \
                                                                                                                                                   \
  template<typename arg__type>                                                                                                                     \
  struct ident##_member_test_dummy##suffix<MISSING, arg__type> {                                                                                   \
  };                                                                                                                                               \
                                                                                                                                                   \
  template<typename arg__type>                                                                                                                     \
  struct ident##_member_test_dummy##suffix<ACCESSIBLE_MEMBER_CLASS, arg__type> {                                                                   \
    struct ident {                                                                                                                                 \
      int data;                                                                                                                                    \
    };                                                                                                                                             \
  };                                                                                                                                               \
                                                                                                                                                   \
  template<typename arg__type>                                                                                                                     \
  struct ident##_member_test_dummy##suffix<ACCESSIBLE_CONSTEXPR_MEMBER_FUNCTION, arg__type> {                                                      \
    constexpr bool ident(const std::remove_reference_t<arg__type> &x) {                                                                            \
      return &x == &x;                                                                                                                             \
    }                                                                                                                                              \
  };                                                                                                                                               \
                                                                                                                                                   \
  template<typename arg__type>                                                                                                                     \
  struct ident##_member_test_dummy##suffix<ACCESSIBLE_STATIC_CONSTEXPR_MEMBER_FUNCTION, arg__type> {                                               \
    static constexpr bool ident(const std::remove_reference_t<arg__type> &x) {                                                                     \
      return &x == &x;                                                                                                                             \
    }                                                                                                                                              \
  };                                                                                                                                               \
                                                                                                                                                   \
  template<typename arg__type>                                                                                                                     \
  struct ident##_member_test_dummy##suffix<ACCESSIBLE_MEMBER_TYPEDEF, arg__type> {                                                                 \
    typedef arg__type ident;                                                                                                                       \
  };                                                                                                                                               \
                                                                                                                                                   \
  template<typename arg__type>                                                                                                                     \
  struct ident##_member_test_dummy##suffix<INACCESSIBLE_MEMBER_TYPEDEF, arg__type> {                                                               \
  private:                                                                                                                                         \
    typedef arg__type ident;                                                                                                                       \
  };                                                                                                                                               \
                                                                                                                                                   \
  template<typename arg__type>                                                                                                                     \
  struct ident##_member_test_dummy##suffix<ACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER, arg__type> {                                                   \
    static constexpr arg__type ident {__VA_ARGS__};                                                                                                \
  };                                                                                                                                               \
                                                                                                                                                   \
  template<typename arg__type>                                                                                                                     \
  struct ident##_member_test_dummy##suffix<INACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER, arg__type> {                                                 \
  private:                                                                                                                                         \
    static constexpr arg__type ident {__VA_ARGS__};                                                                                                \
  };                                                                                                                                               \
                                                                                                                                                   \
  template<typename arg__type>                                                                                                                     \
  struct ident##_member_test_dummy##suffix<ACCESSIBLE_STATIC_NONCONST_DATA_MEMBER, arg__type> {                                                    \
    static arg__type ident;                                                                                                                        \
  };                                                                                                                                               \
                                                                                                                                                   \
  template<typename arg__type>                                                                                                                     \
  struct ident##_member_test_dummy##suffix<ACCESSIBLE_NONSTATIC_CONST_DATA_MEMBER, arg__type> {                                                    \
    const arg__type ident {__VA_ARGS__};                                                                                                           \
  };                                                                                                                                               \
                                                                                                                                                   \
  template<typename arg__type>                                                                                                                     \
  struct ident##_member_test_dummy##suffix<ACCESSIBLE_NONSTATIC_NONCONST_DATA_MEMBER, arg__type> {                                                 \
    arg__type ident;                                                                                                                               \
  };                                                                                                                                               \
                                                                                                                                                   \
  template<typename arg,                                                                                                                           \
           bool = tice::v1::internals::utility::has_##ident##_as_member_typedef<I, arg>::value,                                                    \
           bool = tice::v1::internals::utility::has_##ident##_as_data_member<I, arg>::value,                                                       \
           bool = tice::v1::internals::utility::has_##ident##_as_static_data_member<I, arg>::value,                                                \
           bool = tice::v1::internals::utility::has_##ident##_as_constexpr_copyable_data_member<I, arg>::value>                                    \
  struct ident##_member_test_row##suffix;                                                                                                          \
                                                                                                                                                   \
  template<typename arg__type,                                                                                                                     \
           typename = ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<MISSING, arg__type>>,                                      \
           typename = ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_MEMBER_CLASS, arg__type>>,                      \
           typename = ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_CONSTEXPR_MEMBER_FUNCTION, arg__type>>,         \
           typename = ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_STATIC_CONSTEXPR_MEMBER_FUNCTION, arg__type>>,  \
           typename = ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_MEMBER_TYPEDEF, arg__type>>,                    \
           typename = ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<INACCESSIBLE_MEMBER_TYPEDEF, arg__type>>,                  \
           typename = ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER, arg__type>>,      \
           typename = ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<INACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER, arg__type>>,    \
           typename = ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_STATIC_NONCONST_DATA_MEMBER, arg__type>>,       \
           typename = ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_NONSTATIC_CONST_DATA_MEMBER, arg__type>>,       \
           typename = ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_NONSTATIC_NONCONST_DATA_MEMBER, arg__type>>>    \
  struct ident##_member_test_matrix##suffix

  generate(type,,);         // type_member_test_dummy
  generate(value_type,,);   // value_type_member_test_dummy
  generate(key,,);          // key_member_test_dummy
  generate(value,,);        // value_member_test_dummy
  generate(element_type,,); // element_type_member_test_dummy
  generate(size,,);         // size_member_test_dummy
  generate(elems,,);        // elems_member_test_dummy
  generate(elems_ptr,,);    // elems_member_test_dummy

  struct singleton {
    int data;
  };

  constexpr singleton lref_test_data {777};

  generate(type,         __lref, lref_test_data); //         type_member_test_dummy__lref
  generate(value_type,   __lref, lref_test_data); //   value_type_member_test_dummy__lref
  generate(key,          __lref, lref_test_data); //          key_member_test_dummy__lref
  generate(value,        __lref, lref_test_data); //        value_member_test_dummy__lref
  generate(element_type, __lref, lref_test_data); // element_type_member_test_dummy__lref
  generate(size,         __lref, lref_test_data); //         size_member_test_dummy__lref
  generate(elems,        __lref, lref_test_data); //        elems_member_test_dummy__lref
  generate(elems_ptr,    __lref, lref_test_data); //    elems_ptr_member_test_dummy__lref

  generate(type,         __array, {100}, {200}, {300}, {400}, {500}); //         type_member_test_dummy__array
  generate(value_type,   __array, {100}, {200}, {300}, {400}, {500}); //   value_type_member_test_dummy__array
  generate(key,          __array, {100}, {200}, {300}, {400}, {500}); //          key_member_test_dummy__array
  generate(value,        __array, {100}, {200}, {300}, {400}, {500}); //        value_member_test_dummy__array
  generate(element_type, __array, {100}, {200}, {300}, {400}, {500}); // element_type_member_test_dummy__array
  generate(size,         __array, {100}, {200}, {300}, {400}, {500}); //         size_member_test_dummy__array
  generate(elems,        __array, {100}, {200}, {300}, {400}, {500}); //        elems_member_test_dummy__array
  generate(elems_ptr,    __array, {100}, {200}, {300}, {400}, {500}); //    elems_ptr_member_test_dummy__array

  generate(type,         __3D_array, {{100, 200}, {300, 400}}, {{500, 600}, {700, 800}}); //         type_member_test_dummy__3D_array
  generate(value_type,   __3D_array, {{100, 200}, {300, 400}}, {{500, 600}, {700, 800}}); //   value_type_member_test_dummy__3D_array
  generate(key,          __3D_array, {{100, 200}, {300, 400}}, {{500, 600}, {700, 800}}); //          key_member_test_dummy__3D_array
  generate(value,        __3D_array, {{100, 200}, {300, 400}}, {{500, 600}, {700, 800}}); //        value_member_test_dummy__3D_array
  generate(element_type, __3D_array, {{100, 200}, {300, 400}}, {{500, 600}, {700, 800}}); // element_type_member_test_dummy__3D_array
  generate(size,         __3D_array, {{100, 200}, {300, 400}}, {{500, 600}, {700, 800}}); //         size_member_test_dummy__3D_array
  generate(elems,        __3D_array, {{100, 200}, {300, 400}}, {{500, 600}, {700, 800}}); //        elems_member_test_dummy__3D_array
  generate(elems_ptr,    __3D_array, {{100, 200}, {300, 400}}, {{500, 600}, {700, 800}}); //    elems_ptr_member_test_dummy__3D_array

  generate(type,         __empty_array,); //         type_member_test_dummy__empty_array
  generate(value_type,   __empty_array,); //   value_type_member_test_dummy__empty_array
  generate(key,          __empty_array,); //          key_member_test_dummy__empty_array
  generate(value,        __empty_array,); //        value_member_test_dummy__empty_array
  generate(element_type, __empty_array,); // element_type_member_test_dummy__empty_array
  generate(size,         __empty_array,); //         size_member_test_dummy__empty_array
  generate(elems,        __empty_array,); //        elems_member_test_dummy__empty_array
  generate(elems_ptr,    __empty_array,); //    elems_ptr_member_test_dummy__empty_array

  struct empty {
  };

  class empty__member_typedef_only {
    typedef void anonymous_1;
    typedef int anonymous_2;
    typedef empty anonymous_3;
  public:
    typedef void anonymous_4;
    typedef int anonymous_5;
    typedef empty anonymous_6;
  };

  class empty__member_class_only {
    struct anonymous_1 {
      int member;
    };
    struct anonymous_2 {
      double member;
    };
    struct anonymous_3 {
      empty member;
    };
  public:
    struct anonymous_4 {
      int member;
    };
    struct anonymous_5 {
      double member;
    };
    struct anonymous_6 {
      empty member;
    };
  };

  class empty__member_function_only {
    static constexpr int anonymous_1(int a) {
      return a;
    };
    static constexpr double anonymous_2(double a) {
      return a;
    };
    static constexpr empty anonymous_3(empty a) {
      return a;
    };
  public:
    static constexpr int anonymous_4(int a) {
      return a;
    };
    static constexpr double anonymous_5(double a) {
      return a;
    };
    static constexpr empty anonymous_6(empty a) {
      return a;
    };
  };

  struct noncopyable {
    constexpr noncopyable() {}
    constexpr noncopyable(const noncopyable &) = delete;
    constexpr noncopyable(const noncopyable &&) = delete;
  };

  struct nonconstructible_noncopyable {
    constexpr nonconstructible_noncopyable(const nonconstructible_noncopyable &) = delete;
    constexpr nonconstructible_noncopyable(const nonconstructible_noncopyable &&) = delete;
  };

  generate(type,         __non_default_constructible, 100); //         type_member_test_dummy__non_default_constructible
  generate(value_type,   __non_default_constructible, 100); //   value_type_member_test_dummy__non_default_constructible
  generate(key,          __non_default_constructible, 100); //          key_member_test_dummy__non_default_constructible
  generate(value,        __non_default_constructible, 100); //        value_member_test_dummy__non_default_constructible
  generate(element_type, __non_default_constructible, 100); // element_type_member_test_dummy__non_default_constructible
  generate(size,         __non_default_constructible, 100); //         size_member_test_dummy__non_default_constructible
  generate(elems,        __non_default_constructible, 100); //        elems_member_test_dummy__non_default_constructible
  generate(elems_ptr,    __non_default_constructible, 100); //    elems_ptr_member_test_dummy__non_default_constructible

  struct non_default_constructible {
    int data;
    constexpr non_default_constructible(int data) : data(data) {
    }
  };

#undef generate

}
namespace {

  template<typename, tice::v1::array::Idx idx, typename prev_list_element, typename update_data>
  struct sum_element_1 {
    static constexpr bool is_complete {false};
    typedef prev_list_element value;
    typedef tice::v1::Value<typename update_data::value_type, update_data::value + value::value> data;
  };

}
namespace {

  struct an_incomplete_type;

}
namespace {

  struct an_abstract_class {
    virtual void print_me() = 0;
  };

}
namespace {

  template<typename I, typename arg>
  using unary_always_false = tice::v1::internals::utility::always_false<I>;

  template<typename I, typename arg>
  using unary_always_true = tice::v1::internals::utility::always_true<I>;

}
namespace {

  void fn_with_default_argument_1(double, int = 0);

}
namespace {

  struct double_array {
    static constexpr double elems[] = {.5, .1, .5};
  };

  constexpr double double_1 = .5;

}
namespace {

  struct Value_A {
    static constexpr int value {0};
  };

  struct Value_B {
    static constexpr int value {0};
  };

}

int main() {
  //\begin{commons,c++17-clang-1}
  using namespace tice::v1;
  //\end{commons,c++17-clang-1}

#define generate(ident, result)                                                                         \
                                                                                                        \
  tests::utility::template_args_are_equal<Value<bool, !!internals::utility::ident<void>()>,             \
                                          Value<bool, result>>();                                       \
  tests::utility::template_args_are_equal<Value<bool, !!internals::utility::ident<decltype(20)>()>,     \
                                          Value<bool, result>>();                                       \
  tests::utility::template_args_are_equal<accept_bool<!!internals::utility::ident<void>()>,             \
                                          accept_bool<result>>();                                       \
  tests::utility::template_args_are_equal<accept_bool<!!internals::utility::ident<decltype(20)>()>,     \
                                          accept_bool<result>>();                                       \
  tests::utility::template_args_are_equal<accept_bool<(internals::utility::ident<I>())>,                \
                                          accept_bool<result>>();                                       \
  tests::utility::template_args_are_equal<accept_bool<internals::utility::ident<I>::value>,             \
                                          accept_bool<result>>()

  generate(always_false, false);
  generate(always_true, true);

#undef generate

  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_callable<I, void(*)(),
                                                                                      tice::v1::internals::tuple::construct<I>>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_callable<I, void(*)(),
                                                                                      tice::v1::internals::tuple::construct<I, int>>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_callable<I, void(*)(double),
                                                                                      tice::v1::internals::tuple::construct<I, int>>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_callable<I, void(*)(int &),
                                                                                      tice::v1::internals::tuple::construct<I, int>>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_callable<I, void(*)(const int &),
                                                                                      tice::v1::internals::tuple::construct<I, int>>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_callable<I, void(*)(int &),
                                                                                      tice::v1::internals::tuple::construct<I, int &>>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_callable<I, void(*)(int &&),
                                                                                      tice::v1::internals::tuple::construct<I, int>>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_callable<I, void(*)(const int &),
                                                                                      tice::v1::internals::tuple::construct<I, int &&>>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_callable<I, void(*)(double, int),
                                                                                      tice::v1::internals::tuple::construct<I, int>>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_callable<I, decltype(&fn_with_default_argument_1),
                                                                                      tice::v1::internals::tuple::construct<I, int>>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<
    Value<bool, internals::utility::is_callable<I, void(*)(an_incomplete_type), tice::v1::internals::tuple::construct<I,
                                                                                                                      an_incomplete_type>>::value>,
    Value<bool, false>>();
  tests::utility::template_args_are_equal<
    Value<bool, internals::utility::is_callable<I, void(*)(const an_incomplete_type &),
                                                tice::v1::internals::tuple::construct<I, an_incomplete_type>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, internals::utility::negation<I, internals::utility::always_true<I>>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::negation<I, internals::utility::always_false<I>>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_complete<I, an_incomplete_type>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_complete<I, an_abstract_class>::value>,
                                          Value<bool, true>>();

  typedef internals::utility::chain<I, internals::utility::is_complete, an_incomplete_type> chain_1;
  tests::utility::template_args_are_equal<Tuple<Value<bool, chain_1::value::value>,
                                                Value<bool, std::is_same<typename chain_1::arg, an_incomplete_type>::value>>,
                                          Tuple<Value<bool, false>, Value<bool, true>>>();

  tests::utility::template_args_are_equal<Value<bool, internals::utility::application<I, unary_always_true, I>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, internals::utility::preconditioned_application<I, false, unary_always_true, I,
                                                                                                     internals::utility::always_false<I>>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::preconditioned_application<I, true, unary_always_true, I,
                                                                                                     internals::utility::always_false<I>>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_abstract<I, an_incomplete_type>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_abstract<I, empty>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::is_abstract<I, an_abstract_class>::value>,
                                          Value<bool, true>>();

  //\begin{internals::utility::nontype_nontemplate_args_eq: no indirection}
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, bool, int, true, 1>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, int, int, 1, 1>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, int, int, 1, 2>::value>,
                                          Value<bool, false>>();
  //\end{internals::utility::nontype_nontemplate_args_eq: no indirection}

  //\begin{internals::utility::nontype_nontemplate_args_eq: no indirection, but dereferenced}
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, int, int, 1, 1,
                                                                                                      0, 0>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, int, int, 1, 1,
                                                                                                      0, 1>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, int, int, 1, 1,
                                                                                                      1, 0>::value>,
                                          Value<bool, false>>();
  //\end{internals::utility::nontype_nontemplate_args_eq: no indirection, but dereferenced}

  //\begin{internals::utility::nontype_nontemplate_args_eq: cases needing single indirection}
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *, const double *,
                                                                                                      double_array::elems, double_array::elems,
                                                                                                      0, 2>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *, const double *,
                                                                                                      double_array::elems, double_array::elems,
                                                                                                      0, 1>::value>,
                                          Value<bool, false>>();
  //\begin{c++17-clang-1}
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *, const double &,
                                                                                                      double_array::elems, double_1,
                                                                                                      0>::value>,
                                          Value<bool, true>>();
  //\end{c++17-clang-1}
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, void(*const *)(), void(*)(),
                                                                                                      fn_ptr_array::elems, fn_1,
                                                                                                      0>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, void(*const *)(), void(*)(),
                                                                                                      fn_ptr_array::elems, fn_1,
                                                                                                      1>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, void(*)(), void(*const *)(),
                                                                                                      fn_1, fn_ptr_array::elems,
                                                                                                      array::Invalid_idx::value, 0>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, void(*)(), void(*const *)(),
                                                                                                      fn_1, fn_ptr_array::elems,
                                                                                                      array::Invalid_idx::value, 1>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *, const double *,
                                                                                                      &carrier::x, &carrier::x,
                                                                                                      0, 0>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *, const double *,
                                                                                                      &carrier::x, &carrier::y,
                                                                                                      0, 0>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *, const double *,
                                                                                                      &carrier::x, &carrier::z,
                                                                                                      0, 0>::value>,
                                          Value<bool, false>>();
  //\end{internals::utility::nontype_nontemplate_args_eq: cases needing single indirection}

  //\begin{equal_lvalues, internals::utility::lvalues_are_equal, and their semantics}
  tests::utility::template_args_are_equal<Value<bool, equal_lvalues(carrier::x, carrier::x)>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::lvalues_are_equal<I, decltype(carrier::x), decltype(carrier::x),
                                                                                            &carrier::x, &carrier::x>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(carrier::x) *,
                                                                                                      decltype(carrier::x) *,
                                                                                                      &carrier::x, &carrier::x,
                                                                                                      0, 0>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, equal_lvalues(carrier::x, carrier::y)>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::lvalues_are_equal<I, decltype(carrier::x), decltype(carrier::y),
                                                                                            &carrier::x, &carrier::y>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(carrier::x) *,
                                                                                                      decltype(carrier::y) *,
                                                                                                      &carrier::x, &carrier::y,
                                                                                                      0, 0>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, equal_lvalues(carrier::x, carrier::z)>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::lvalues_are_equal<I, decltype(carrier::x), decltype(carrier::z),
                                                                                            &carrier::x, &carrier::z>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(carrier::x) *,
                                                                                                      decltype(carrier::z) *,
                                                                                                      &carrier::x, &carrier::z,
                                                                                                      0, 0>::value>,
                                          Value<bool, false>>();

  tests::utility::template_args_are_equal<Value<bool, equal_lvalues(fn_1_ptr_1::value, fn_1_ptr_2::value)>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::lvalues_are_equal<I, decltype(fn_1_ptr_1::value),
                                                                                            decltype(fn_1_ptr_2::value),
                                                                                            &fn_1_ptr_1::value, &fn_1_ptr_2::value>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(fn_1_ptr_1::value) *,
                                                                                                      decltype(fn_1_ptr_2::value) *,
                                                                                                      &fn_1_ptr_1::value, &fn_1_ptr_2::value,
                                                                                                      0, 0>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, equal_lvalues(fn_1_ptr_1::value, fn_2_ptr_1::value)>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::lvalues_are_equal<I, decltype(fn_1_ptr_1::value),
                                                                                            decltype(fn_2_ptr_1::value),
                                                                                            &fn_1_ptr_1::value, &fn_2_ptr_1::value>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I,
                                                                                                      decltype(fn_1_ptr_1::value) *,
                                                                                                      decltype(fn_2_ptr_1::value) *,
                                                                                                      &fn_1_ptr_1::value, &fn_2_ptr_1::value,
                                                                                                      0, 0>::value>,
                                          Value<bool, false>>();
  //\end{equal_lvalues, internals::utility::lvalues_are_equal, and their semantics}

  //\begin{internals::utility::nontype_nontemplate_args_eq: no indirection needed for function pointer}
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, void(*)(), void(*)(),
                                                                                                      fn_1, fn_2>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, void(*)(), void(*)(),
                                                                                                      fn_2, fn_2>::value>,
                                          Value<bool, true>>();
  //\end{internals::utility::nontype_nontemplate_args_eq: no indirection needed for function pointer}

  //\begin{equal_lvalues, internals::utility::lvalues_are_equal, and their semantics}
  tests::utility::template_args_are_equal<Value<bool, equal_lvalues(fn_1, fn_2)>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::lvalues_are_equal<I, decltype(fn_1), decltype(fn_2),
                                                                                            fn_1, fn_2>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(fn_1) *, decltype(fn_2) *,
                                                                                                      fn_1, fn_2>::value>,
                                          Value<bool, false>>();

  tests::utility::template_args_are_equal<Value<bool, equal_lvalues(fn_2, fn_2)>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::lvalues_are_equal<I, decltype(fn_2), decltype(fn_2),
                                                                                            fn_2, fn_2>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(fn_2) *, decltype(fn_2) *,
                                                                                                      fn_2, fn_2>::value>,
                                          Value<bool, true>>();
  //\end{equal_lvalues, internals::utility::lvalues_are_equal, and their semantics}

  //\begin{internals::utility::nontype_nontemplate_args_eq: no indirection needed for array}
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *, const double *,
                                                                                                      array_mapper::x, array_mapper::x>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *, const double *,
                                                                                                      array_mapper::x, array_mapper::alpha>::value>,
                                          Value<bool, false>>();
  //\end{internals::utility::nontype_nontemplate_args_eq: no indirection needed for array}

  //\begin{equal_lvalues, internals::utility::lvalues_are_equal, and their semantics}
  tests::utility::template_args_are_equal<Value<bool, equal_lvalues(array_mapper::x, array_mapper::x)>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::lvalues_are_equal<I, decltype(array_mapper::x), decltype(array_mapper::x),
                                                                                            &array_mapper::x, &array_mapper::x>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I,
                                                                                                      decltype(array_mapper::x) *,
                                                                                                      decltype(array_mapper::x) *,
                                                                                                      &array_mapper::x, &array_mapper::x,
                                                                                                      0, 0>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, equal_lvalues(array_mapper::x, array_mapper::alpha)>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::lvalues_are_equal<I, decltype(array_mapper::x),
                                                                                            decltype(array_mapper::alpha),
                                                                                            &array_mapper::x, &array_mapper::alpha>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I,
                                                                                                      decltype(array_mapper::x) *,
                                                                                                      decltype(array_mapper::alpha) *,
                                                                                                      &array_mapper::x, &array_mapper::alpha,
                                                                                                      0, 0>::value>,
                                          Value<bool, false>>();
  //\end{equal_lvalues, internals::utility::lvalues_are_equal, and their semantics}

  //\begin{internals::utility::nontype_nontemplate_args_eq: cases needing double indirections}
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *const *, const double *,
                                                                                                      array_mapper::elems_ptr, array_result,
                                                                                                      0, 3, 0>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *const *, const double *,
                                                                                                      array_mapper::elems_ptr, array_result,
                                                                                                      0, 4, 1>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *const *, const double *,
                                                                                                      array_mapper::elems_ptr, array_result,
                                                                                                      0, 3, 2>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *const *, const double *,
                                                                                                      array_mapper::elems_ptr, array_result,
                                                                                                      0, 4, 2>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *, const double *,
                                                                                                      array_mapper::alpha, array_result,
                                                                                                      0, 0>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *, const double *const *,
                                                                                                      &carrier::x, carrier::x_ptr_ptr,
                                                                                                      0, 0, array::Invalid_idx::value, 0>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, const double *, const double *const *,
                                                                                                      &carrier::x, &carrier::x_ptr,
                                                                                                      0, 0, array::Invalid_idx::value, 0>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I,
                                                                                                      decltype(array_mapper::elems_as_auto) *,
                                                                                                      const double *,
                                                                                                      &array_mapper::elems_as_auto, &carrier::x,
                                                                                                      0, 0, 1>::value>,
                                          Value<bool, true>>();
  //\end{internals::utility::nontype_nontemplate_args_eq: cases needing double indirections}

  //\begin{point_equal_lvalues, internals::utility::arrays_are_point_equal, and their semantics}
  tests::utility::template_args_are_equal<Value<bool, point_equal_lvalues(array_mapper::alpha, array_result, H(10, 0))>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_point_equal<I, decltype(array_mapper::alpha),
                                                                                                 decltype(array_result),
                                                                                                 &array_mapper::alpha, &array_result,
                                                                                                 0>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(array_mapper::alpha) *,
                                                                                                      decltype(array_result) *,
                                                                                                      &array_mapper::alpha, &array_result,
                                                                                                      0, 0, 0, 0>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, point_equal_lvalues(array_mapper::x, array_result, 0)>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_point_equal<I, decltype(array_mapper::x),
                                                                                                 decltype(array_result),
                                                                                                 &array_mapper::x, &array_result,
                                                                                                 0>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(array_mapper::x) *,
                                                                                                      decltype(array_result) *,
                                                                                                      &array_mapper::x, &array_result,
                                                                                                      0, 0, 0, 0>::value>,
                                          Value<bool, false>>();

  tests::utility::template_args_are_equal<Value<bool, point_equal_lvalues(fn_ptr_array::elems, fn_array, 0)>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_point_equal<I, decltype(fn_ptr_array::elems),
                                                                                                 decltype(fn_array),
                                                                                                 &fn_ptr_array::elems, &fn_array,
                                                                                                 0>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(fn_ptr_array::elems) *,
                                                                                                      decltype(fn_array) *,
                                                                                                      &fn_ptr_array::elems, &fn_array,
                                                                                                      0, 0, 0, 0>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, point_equal_lvalues(fn_ptr_array::elems, fn_array_reversed, 0)>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_point_equal<I, decltype(fn_ptr_array::elems),
                                                                                                 decltype(fn_array_reversed),
                                                                                                 &fn_ptr_array::elems, &fn_array_reversed,
                                                                                                 0>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(fn_ptr_array::elems) *,
                                                                                                      decltype(fn_array_reversed) *,
                                                                                                      &fn_ptr_array::elems, &fn_array_reversed,
                                                                                                      0, 0, 0, 0>::value>,
                                          Value<bool, false>>();

  tests::utility::template_args_are_equal<Value<bool, point_equal_lvalues(fn_ptr_array::elems, array_result, 0)>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_point_equal<I, decltype(fn_ptr_array::elems),
                                                                                                 decltype(array_result),
                                                                                                 &fn_ptr_array::elems, &array_result,
                                                                                                 0>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(fn_ptr_array::elems) *,
                                                                                                      decltype(array_result) *,
                                                                                                      &fn_ptr_array::elems, &array_result,
                                                                                                      0, 0, 0, 0>::value>,
                                          Value<bool, false>>();

  tests::utility::template_args_are_equal<Value<bool, point_equal_lvalues(array_mapper::elems_as_auto, array_result_trunk, 1)>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_point_equal<I, decltype(array_mapper::elems_as_auto),
                                                                                                 decltype(array_result_trunk),
                                                                                                 &array_mapper::elems_as_auto, &array_result_trunk,
                                                                                                 1>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(array_mapper::elems_as_auto) *,
                                                                                                      decltype(array_result_trunk) *,
                                                                                                      &array_mapper::elems_as_auto,
                                                                                                      &array_result_trunk,
                                                                                                      0, 0, 1, 1>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, point_equal_lvalues(array_mapper::elems_as_auto, array_result, 1)>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_point_equal<I, decltype(array_mapper::elems_as_auto),
                                                                                                 decltype(array_result),
                                                                                                 &array_mapper::elems_as_auto, &array_result,
                                                                                                 1>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(array_mapper::elems_as_auto) *,
                                                                                                      decltype(array_result) *,
                                                                                                      &array_mapper::elems_as_auto, &array_result,
                                                                                                      0, 0, 1, 1>::value>,
                                          Value<bool, false>>();
  //\end{point_equal_lvalues, internals::utility::arrays_are_point_equal, and their semantics}

  //\begin{pair_equal_lvalues, internals::utility::arrays_are_pair_equal, and their semantics}
  tests::utility::template_args_are_equal<Value<bool, pair_equal_lvalues(array_mapper::elems_as_auto, array_result, H(10, 0), H(10, 3))>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_pair_equal<I, decltype(array_mapper::elems_as_auto),
                                                                                                decltype(array_result),
                                                                                                &array_mapper::elems_as_auto, &array_result,
                                                                                                0, 3>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(array_mapper::elems_as_auto) *,
                                                                                                      decltype(array_result) *,
                                                                                                      &array_mapper::elems_as_auto, &array_result,
                                                                                                      0, 0, 0, 3>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, pair_equal_lvalues(array_mapper::elems_as_auto, array_result, 0, 2)>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_pair_equal<I, decltype(array_mapper::elems_as_auto),
                                                                                                decltype(array_result),
                                                                                                &array_mapper::elems_as_auto, &array_result,
                                                                                                0, 2>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(array_mapper::elems_as_auto) *,
                                                                                                      decltype(array_result) *,
                                                                                                      &array_mapper::elems_as_auto, &array_result,
                                                                                                      0, 0, 0, 2>::value>,
                                          Value<bool, false>>();

  tests::utility::template_args_are_equal<Value<bool, pair_equal_lvalues(fn_array, fn_array_reversed, 0, 1)>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_pair_equal<I, decltype(fn_array),
                                                                                                decltype(fn_array_reversed),
                                                                                                &fn_array, &fn_array_reversed,
                                                                                                0, 1>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(fn_array) *,
                                                                                                      decltype(fn_array_reversed) *,
                                                                                                      &fn_array, &fn_array_reversed,
                                                                                                      0, 0, 0, 1>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, pair_equal_lvalues(fn_array, fn_array_reversed, 0, 0)>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_pair_equal<I, decltype(fn_array),
                                                                                                decltype(fn_array_reversed),
                                                                                                &fn_array, &fn_array_reversed,
                                                                                                0, 0>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(fn_array) *,
                                                                                                      decltype(fn_array_reversed) *,
                                                                                                      &fn_array, &fn_array_reversed,
                                                                                                      0, 0, 0, 0>::value>,
                                          Value<bool, false>>();

  tests::utility::template_args_are_equal<Value<bool, pair_equal_lvalues(array_result, array_result, 0, 2)>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_pair_equal<I, decltype(array_result),
                                                                                                decltype(array_result),
                                                                                                &array_result, &array_result,
                                                                                                0, 2>::value>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(array_result) *,
                                                                                                      decltype(array_result) *,
                                                                                                      &array_result, &array_result,
                                                                                                      0, 0, 0, 2>::value>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<bool, pair_equal_lvalues(array_result, array_result, 0, 1)>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::arrays_are_pair_equal<I, decltype(array_result),
                                                                                                decltype(array_result),
                                                                                                &array_result, &array_result,
                                                                                                0, 1>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::nontype_nontemplate_args_eq<I, decltype(array_result) *,
                                                                                                      decltype(array_result) *,
                                                                                                      &array_result, &array_result,
                                                                                                      0, 0, 0, 1>::value>,
                                          Value<bool, false>>();
  //\end{pair_equal_lvalues, internals::utility::arrays_are_pair_equal, and their semantics}

#define generate(ident, suffix, type) /*                                             (has_constexpr_copyable_data_member__##ident)---.*/        \
    /*                                                                                 (has_static_data_member__##ident)----------.  |*/        \
    /*                                                                                   (has_data_member__##ident)------------.  |  |*/        \
    /*                                                                                     (has_member_typedef__##ident)----.  |  |  |*/        \
    H(ident##_member_test_matrix##suffix<type>, ident##_member_test_matrix##suffix<type,                                  /*|  |  |  |*/        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<MISSING                                    , type>, 0, 0, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_MEMBER_CLASS                    , type>, 1, 0, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_CONSTEXPR_MEMBER_FUNCTION       , type>, 0, 0, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_STATIC_CONSTEXPR_MEMBER_FUNCTION, type>, 0, 1, 1, 1>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_MEMBER_TYPEDEF                  , type>, 1, 0, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<INACCESSIBLE_MEMBER_TYPEDEF                , type>, 0, 0, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER    , type>, 0, 1, 1, 1>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<INACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER  , type>, 0, 0, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_STATIC_NONCONST_DATA_MEMBER     , type>, 0, 1, 1, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_NONSTATIC_CONST_DATA_MEMBER     , type>, 0, 1, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_NONSTATIC_NONCONST_DATA_MEMBER  , type>, 0, 1, 0, 0>>)

  tests::utility::template_args_are_equal<generate(type,        , int)>();
  tests::utility::template_args_are_equal<generate(value_type,  , int)>();
  tests::utility::template_args_are_equal<generate(key,         , int)>();
  tests::utility::template_args_are_equal<generate(value,       , int)>();
  tests::utility::template_args_are_equal<generate(element_type,, int)>();
  tests::utility::template_args_are_equal<generate(size,        , int)>();
  tests::utility::template_args_are_equal<generate(elems,       , int)>();

  tests::utility::template_args_are_equal<generate(type,        , double)>();
  tests::utility::template_args_are_equal<generate(value_type,  , double)>();
  tests::utility::template_args_are_equal<generate(key,         , double)>();
  tests::utility::template_args_are_equal<generate(value,       , double)>();
  tests::utility::template_args_are_equal<generate(element_type,, double)>();
  tests::utility::template_args_are_equal<generate(size,        , double)>();
  tests::utility::template_args_are_equal<generate(elems,       , double)>();

  tests::utility::template_args_are_equal<generate(type,        , singleton)>();
  tests::utility::template_args_are_equal<generate(value_type,  , singleton)>();
  tests::utility::template_args_are_equal<generate(key,         , singleton)>();
  tests::utility::template_args_are_equal<generate(value,       , singleton)>();
  tests::utility::template_args_are_equal<generate(element_type,, singleton)>();
  tests::utility::template_args_are_equal<generate(size,        , singleton)>();
  tests::utility::template_args_are_equal<generate(elems,       , singleton)>();

  tests::utility::template_args_are_equal<generate(type,        , singleton *)>();
  tests::utility::template_args_are_equal<generate(value_type,  , singleton *)>();
  tests::utility::template_args_are_equal<generate(key,         , singleton *)>();
  tests::utility::template_args_are_equal<generate(value,       , singleton *)>();
  tests::utility::template_args_are_equal<generate(element_type,, singleton *)>();
  tests::utility::template_args_are_equal<generate(size,        , singleton *)>();
  tests::utility::template_args_are_equal<generate(elems,       , singleton *)>();

  tests::utility::template_args_are_equal<generate(type,         __lref, const singleton &)>();
  tests::utility::template_args_are_equal<generate(value_type,   __lref, const singleton &)>();
  tests::utility::template_args_are_equal<generate(key,          __lref, const singleton &)>();
  tests::utility::template_args_are_equal<generate(value,        __lref, const singleton &)>();
  tests::utility::template_args_are_equal<generate(element_type, __lref, const singleton &)>();
  tests::utility::template_args_are_equal<generate(size,         __lref, const singleton &)>();
  tests::utility::template_args_are_equal<generate(elems,        __lref, const singleton &)>();

  tests::utility::template_args_are_equal<generate(type,         __non_default_constructible, non_default_constructible)>();
  tests::utility::template_args_are_equal<generate(value_type,   __non_default_constructible, non_default_constructible)>();
  tests::utility::template_args_are_equal<generate(key,          __non_default_constructible, non_default_constructible)>();
  tests::utility::template_args_are_equal<generate(value,        __non_default_constructible, non_default_constructible)>();
  tests::utility::template_args_are_equal<generate(element_type, __non_default_constructible, non_default_constructible)>();
  tests::utility::template_args_are_equal<generate(size,         __non_default_constructible, non_default_constructible)>();
  tests::utility::template_args_are_equal<generate(elems,        __non_default_constructible, non_default_constructible)>();

#undef generate

#define generate(ident, suffix, type) /*                                             (has_constexpr_copyable_data_member__##ident)---.*/        \
    /*                                                                                 (has_static_data_member__##ident)----------.  |*/        \
    /*                                                                                   (has_data_member__##ident)------------.  |  |*/        \
    /*                                                                                     (has_member_typedef__##ident)----.  |  |  |*/        \
    H(ident##_member_test_matrix##suffix<type>, ident##_member_test_matrix##suffix<type,                                  /*|  |  |  |*/        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<MISSING                                    , type>, 0, 0, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_MEMBER_CLASS                    , type>, 1, 0, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_CONSTEXPR_MEMBER_FUNCTION       , type>, 0, 0, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_STATIC_CONSTEXPR_MEMBER_FUNCTION, type>, 0, 1, 1, 1>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_MEMBER_TYPEDEF                  , type>, 1, 0, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<INACCESSIBLE_MEMBER_TYPEDEF                , type>, 0, 0, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER    , type>, 0, 1, 1, 1>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<INACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER  , type>, 0, 0, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_STATIC_NONCONST_DATA_MEMBER     , type>, 0, 1, 1, 1 /*!*/>,  \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_NONSTATIC_CONST_DATA_MEMBER     , type>, 0, 1, 0, 0>,        \
      ident##_member_test_row##suffix<ident##_member_test_dummy##suffix<ACCESSIBLE_NONSTATIC_NONCONST_DATA_MEMBER  , type>, 0, 1, 0, 0>>)

  tests::utility::template_args_are_equal<generate(type,        , empty)>();
  tests::utility::template_args_are_equal<generate(value_type,  , empty)>();
  tests::utility::template_args_are_equal<generate(key,         , empty)>();
  tests::utility::template_args_are_equal<generate(value,       , empty)>();
  tests::utility::template_args_are_equal<generate(element_type,, empty)>();
  tests::utility::template_args_are_equal<generate(size,        , empty)>();
  tests::utility::template_args_are_equal<generate(elems,       , empty)>();

  tests::utility::template_args_are_equal<generate(type,        , empty__member_typedef_only)>();
  tests::utility::template_args_are_equal<generate(value_type,  , empty__member_typedef_only)>();
  tests::utility::template_args_are_equal<generate(key,         , empty__member_typedef_only)>();
  tests::utility::template_args_are_equal<generate(value,       , empty__member_typedef_only)>();
  tests::utility::template_args_are_equal<generate(element_type,, empty__member_typedef_only)>();
  tests::utility::template_args_are_equal<generate(size,        , empty__member_typedef_only)>();
  tests::utility::template_args_are_equal<generate(elems,       , empty__member_typedef_only)>();

  tests::utility::template_args_are_equal<generate(type,        , empty__member_class_only)>();
  tests::utility::template_args_are_equal<generate(value_type,  , empty__member_class_only)>();
  tests::utility::template_args_are_equal<generate(key,         , empty__member_class_only)>();
  tests::utility::template_args_are_equal<generate(value,       , empty__member_class_only)>();
  tests::utility::template_args_are_equal<generate(element_type,, empty__member_class_only)>();
  tests::utility::template_args_are_equal<generate(size,        , empty__member_class_only)>();
  tests::utility::template_args_are_equal<generate(elems,       , empty__member_class_only)>();

  tests::utility::template_args_are_equal<generate(type,        , empty__member_function_only)>();
  tests::utility::template_args_are_equal<generate(value_type,  , empty__member_function_only)>();
  tests::utility::template_args_are_equal<generate(key,         , empty__member_function_only)>();
  tests::utility::template_args_are_equal<generate(value,       , empty__member_function_only)>();
  tests::utility::template_args_are_equal<generate(element_type,, empty__member_function_only)>();
  tests::utility::template_args_are_equal<generate(size,        , empty__member_function_only)>();
  tests::utility::template_args_are_equal<generate(elems,       , empty__member_function_only)>();

  tests::utility::template_args_are_equal<generate(type,         __array, singleton[5])>();
  tests::utility::template_args_are_equal<generate(value_type,   __array, singleton[5])>();
  tests::utility::template_args_are_equal<generate(key,          __array, singleton[5])>();
  tests::utility::template_args_are_equal<generate(value,        __array, singleton[5])>();
  tests::utility::template_args_are_equal<generate(element_type, __array, singleton[5])>();
  tests::utility::template_args_are_equal<generate(size,         __array, singleton[5])>();
  tests::utility::template_args_are_equal<generate(elems,        __array, singleton[5])>();

  tests::utility::template_args_are_equal<generate(type,         __3D_array, singleton[2][2][2])>();
  tests::utility::template_args_are_equal<generate(value_type,   __3D_array, singleton[2][2][2])>();
  tests::utility::template_args_are_equal<generate(key,          __3D_array, singleton[2][2][2])>();
  tests::utility::template_args_are_equal<generate(value,        __3D_array, singleton[2][2][2])>();
  tests::utility::template_args_are_equal<generate(element_type, __3D_array, singleton[2][2][2])>();
  tests::utility::template_args_are_equal<generate(size,         __3D_array, singleton[2][2][2])>();
  tests::utility::template_args_are_equal<generate(elems,        __3D_array, singleton[2][2][2])>();

  // \begin{invalid tests under a strict conformance to ISO C++ using `-pedantic-errors'}
  // tests::utility::template_args_are_equal<generate(type,         __empty_array, singleton[0])>();
  // tests::utility::template_args_are_equal<generate(value_type,   __empty_array, singleton[0])>();
  // tests::utility::template_args_are_equal<generate(key,          __empty_array, singleton[0])>();
  // tests::utility::template_args_are_equal<generate(value,        __empty_array, singleton[0])>();
  // tests::utility::template_args_are_equal<generate(element_type, __empty_array, singleton[0])>();
  // tests::utility::template_args_are_equal<generate(size,         __empty_array, singleton[0])>();
  // tests::utility::template_args_are_equal<generate(elems,        __empty_array, singleton[0])>();
  // \end{invalid tests under a strict conformance to ISO C++ using `-pedantic-errors'}

#undef generate

#define generate(ident, type) /*                                     (has_constexpr_copyable_data_member__##ident)---.*/        \
    /*                                                                 (has_static_data_member__##ident)----------.  |*/        \
    /*                                                                   (has_data_member__##ident)------------.  |  |*/        \
    /*                                                                     (has_member_typedef__##ident)----.  |  |  |*/        \
    H(ident##_member_test_matrix<type>, ident##_member_test_matrix<type,                                  /*|  |  |  |*/        \
      ident##_member_test_row<ident##_member_test_dummy<MISSING                                    , type>, 0, 0, 0, 0>,        \
      ident##_member_test_row<ident##_member_test_dummy<ACCESSIBLE_MEMBER_CLASS                    , type>, 1, 0, 0, 0>,        \
      ident##_member_test_row<ident##_member_test_dummy<ACCESSIBLE_CONSTEXPR_MEMBER_FUNCTION       , type>, 0, 0, 0, 0>,        \
      ident##_member_test_row<ident##_member_test_dummy<ACCESSIBLE_STATIC_CONSTEXPR_MEMBER_FUNCTION, type>, 0, 1, 1, 1>,        \
      ident##_member_test_row<ident##_member_test_dummy<ACCESSIBLE_MEMBER_TYPEDEF                  , type>, 1, 0, 0, 0>,        \
      ident##_member_test_row<ident##_member_test_dummy<INACCESSIBLE_MEMBER_TYPEDEF                , type>, 0, 0, 0, 0>,        \
      ident##_member_test_row<ident##_member_test_dummy<ACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER    , type>, 0, 1, 1, 0 /*!*/>,  \
      ident##_member_test_row<ident##_member_test_dummy<INACCESSIBLE_STATIC_CONSTEXPR_DATA_MEMBER  , type>, 0, 0, 0, 0>,        \
      ident##_member_test_row<ident##_member_test_dummy<ACCESSIBLE_STATIC_NONCONST_DATA_MEMBER     , type>, 0, 1, 1, 0>,        \
      ident##_member_test_row<ident##_member_test_dummy<ACCESSIBLE_NONSTATIC_CONST_DATA_MEMBER     , type>, 0, 1, 0, 0>,        \
      ident##_member_test_row<ident##_member_test_dummy<ACCESSIBLE_NONSTATIC_NONCONST_DATA_MEMBER  , type>, 0, 1, 0, 0>>)

  tests::utility::template_args_are_equal<generate(type,         noncopyable)>();
  tests::utility::template_args_are_equal<generate(value_type,   noncopyable)>();
  tests::utility::template_args_are_equal<generate(key,          noncopyable)>();
  tests::utility::template_args_are_equal<generate(value,        noncopyable)>();
  tests::utility::template_args_are_equal<generate(element_type, noncopyable)>();
  tests::utility::template_args_are_equal<generate(size,         noncopyable)>();
  tests::utility::template_args_are_equal<generate(elems,        noncopyable)>();

  tests::utility::template_args_are_equal<generate(type,         nonconstructible_noncopyable)>();
  tests::utility::template_args_are_equal<generate(value_type,   nonconstructible_noncopyable)>();
  tests::utility::template_args_are_equal<generate(key,          nonconstructible_noncopyable)>();
  tests::utility::template_args_are_equal<generate(value,        nonconstructible_noncopyable)>();
  tests::utility::template_args_are_equal<generate(element_type, nonconstructible_noncopyable)>();
  tests::utility::template_args_are_equal<generate(size,         nonconstructible_noncopyable)>();
  tests::utility::template_args_are_equal<generate(elems,        nonconstructible_noncopyable)>();

#undef generate

  typedef typename internals::utility::list::make<I, 0, get_element_1>::value list1;
  tests::utility::template_args_are_equal<list1, internals::tuple::construct<I>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::update<I, list1, update_element_1, Value<int, 0>>::value,
                                          internals::tuple::construct<I>>();

  typedef typename internals::utility::list::make<I, 1, get_element_1>::value list2;
  tests::utility::template_args_are_equal<list2, internals::tuple::construct<I, Value<int, 0>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::update<I, list2, update_element_1, Value<int, 0>>::value,
                                          internals::tuple::construct<I, Value<int, 0>>>();

  typedef typename internals::utility::list::make<I, 2, get_element_1>::value list3;
  tests::utility::template_args_are_equal<list3, internals::tuple::construct<I, Value<int, 0>,
                                                                             Value<int, 100>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::update<I, list3, update_element_1, Value<int, 0>>::value,
                                          internals::tuple::construct<I, Value<int, 0>,
                                                                      Value<int, -100>>>();

  typedef typename internals::utility::list::make<I, 3, get_element_1>::value list4;
  tests::utility::template_args_are_equal<list4, internals::tuple::construct<I, Value<int, 0>,
                                                                             Value<int, 100>,
                                                                             Value<int, 200>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::update<I, list4, update_element_1, Value<int, 0>>::value,
                                          internals::tuple::construct<I, Value<int, 0>,
                                                                      Value<int, -100>,
                                                                      Value<int, -100>>>();

  typedef typename internals::utility::list::make<I, 5, get_element_1>::value list5;
  tests::utility::template_args_are_equal<list5, internals::tuple::construct<I, Value<int, 0>,
                                                                             Value<int, 100>,
                                                                             Value<int, 200>,
                                                                             Value<int, 300>,
                                                                             Value<int, 400>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::update<I, list5, update_element_2, void>::value,
                                          internals::tuple::construct<I, Value<int, 0>,
                                                                      Value<int, 1000>,
                                                                      Value<int, 2000>,
                                                                      Value<int, 300>,
                                                                      Value<int, 400>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::update<I, list5, update_element_3, void>::value,
                                          internals::tuple::construct<I, Value<int, 10>,
                                                                      Value<int, 100>,
                                                                      Value<int, 200>,
                                                                      Value<int, 300>,
                                                                      Value<int, 400>>>();
  typedef internals::utility::list::iterate<I, list5, iterator_1, tice::v1::internals::tuple::construct<I>> iteration_result_1;
  tests::utility::template_args_are_equal<typename iteration_result_1::value,
                                          internals::tuple::construct<I, Value<int, 14>,
                                                                      Value<int, 13>,
                                                                      Value<int, 12>,
                                                                      Value<int, 11>,
                                                                      Value<int, 10>>>();

  tests::utility::template_args_are_equal<typename internals::utility::list::update<I, list5, sum_element_1, Value<unsigned, 0>>::data,
                                          Value<unsigned, 1000>>();

  tests::utility::template_args_are_equal<typename internals::utility::list::append<I, list5, Value<unsigned, 500>>::value,
                                          internals::tuple::construct<I, Value<int, 0>,
                                                                      Value<int, 100>,
                                                                      Value<int, 200>,
                                                                      Value<int, 300>,
                                                                      Value<int, 400>,
                                                                      Value<unsigned, 500>>>();

  tests::utility::template_args_are_equal<typename internals::utility::list::append<I, internals::tuple::construct<I>, Value<int, 500>>::value,
                                          internals::tuple::construct<I, Value<int, 500>>>();

  tests::utility::template_args_are_equal<typename
                                          internals::utility::list::append<I,
                                                                           typename internals::utility::list::append<I,
                                                                                                                     internals::tuple::construct<I>,
                                                                                                                     Value<int, 500>>::value,
                                                                           Value<unsigned, 600>>::value,
                                          internals::tuple::construct<I, Value<int, 500>,
                                                                      Value<unsigned, 600>>>();

  tests::utility::template_args_are_equal<typename internals::utility::list::cat<I, Value<unsigned, 600>>::value,
                                          internals::tuple::construct<I, Value<unsigned, 600>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::cat<I, Value<unsigned, 600>, Value<unsigned, 700>>::value,
                                          internals::tuple::construct<I, Value<unsigned, 600>, Value<unsigned, 700>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::cat<I, internals::tuple::construct<I, Value<unsigned, 600>>>::value,
                                          internals::tuple::construct<I, Value<unsigned, 600>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::cat<I, internals::tuple::construct<I, Value<unsigned, 600>>,
                                                                                 Value<unsigned, 700>>::value,
                                          internals::tuple::construct<I, Value<unsigned, 600>, Value<unsigned, 700>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::cat<I, Value<unsigned, 600>,
                                                                                 internals::tuple::construct<I, Value<unsigned, 700>>>::value,
                                          internals::tuple::construct<I, Value<unsigned, 600>, Value<unsigned, 700>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::cat<I, internals::tuple::construct<I, Value<unsigned, 600>>,
                                                                                 internals::tuple::construct<I, Value<unsigned, 700>>>::value,
                                          internals::tuple::construct<I, Value<unsigned, 600>, Value<unsigned, 700>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::cat<I, internals::tuple::construct<I, Value<unsigned, 600>,
                                                                                                                Value<unsigned, 700>>,
                                                                                 internals::tuple::construct<I, Value<unsigned, 800>>>::value,
                                          internals::tuple::construct<I, Value<unsigned, 600>, Value<unsigned, 700>, Value<unsigned, 800>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::cat<I, internals::tuple::construct<I, Value<unsigned, 600>>,
                                                                                 internals::tuple::construct<I, Value<unsigned, 700>,
                                                                                                             Value<unsigned, 800>>>::value,
                                          internals::tuple::construct<I, Value<unsigned, 600>, Value<unsigned, 700>, Value<unsigned, 800>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::cat<I, internals::tuple::construct<I, Value<unsigned, 600>>,
                                                                                 Value<unsigned, 700>,
                                                                                 internals::tuple::construct<I, Value<unsigned, 800>>>::value,
                                          internals::tuple::construct<I, Value<unsigned, 600>, Value<unsigned, 700>, Value<unsigned, 800>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::cat<I, internals::tuple::construct<I, Value<unsigned, 600>>,
                                                                                 Value<unsigned, 700>, Value<unsigned, 800>>::value,
                                          internals::tuple::construct<I, Value<unsigned, 600>, Value<unsigned, 700>, Value<unsigned, 800>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::cat<I, internals::tuple::construct<I, Value<unsigned, 600>>,
                                                                                 internals::tuple::construct<I, Value<unsigned, 700>>,
                                                                                 Value<unsigned, 800>>::value,
                                          internals::tuple::construct<I, Value<unsigned, 600>, Value<unsigned, 700>, Value<unsigned, 800>>>();
  tests::utility::template_args_are_equal<typename internals::utility::list::cat<I, internals::tuple::construct<I, Value<unsigned, 600>>,
                                                                                 internals::tuple::construct<I, Value<unsigned, 700>,
                                                                                                             Value<unsigned, 800>>,
                                                                                 Value<unsigned, 900>>::value,
                                          internals::tuple::construct<I, Value<unsigned, 600>, Value<unsigned, 700>, Value<unsigned, 800>,
                                                                      Value<unsigned, 900>>>();

  tests::utility::template_args_are_equal<Value<bool, internals::utility::fp::Double<I, Ratio<1, 3>>::value == 1.0/3.0>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, internals::utility::fp::Double<I, Ratio<1, 10>>::value == 1.0/10.0>,
                                          Value<bool, true>>();

  tests::utility::template_args_are_equal<typename internals::utility::min<I, Value_A, Value_B>::value, Value_B>();
  tests::utility::template_args_are_equal<Value<bool, std::is_same<typename internals::utility::min<I, Value_A, Value_B>::value, Value_A>::value>,
                                          Value<bool, false>>();
  tests::utility::template_args_are_equal<typename internals::utility::max<I, Value_A, Value_B>::value, Value_A>();
  tests::utility::template_args_are_equal<Value<bool, std::is_same<typename internals::utility::max<I, Value_A, Value_B>::value, Value_B>::value>,
                                          Value<bool, false>>();

  tests::utility::template_args_are_equal<typename internals::utility::min<I, internals::utility::fp::Double<I, Ratio<1, 3>>,
                                                                           internals::utility::fp::Double<I, Ratio<1, 10>>>::value,
                                          internals::utility::fp::Double<I, Ratio<1, 10>>>();
  tests::utility::template_args_are_equal<typename internals::utility::min<I, internals::utility::fp::Double<I, Ratio<1, 10>>,
                                                                           internals::utility::fp::Double<I, Ratio<1, 3>>>::value,
                                          internals::utility::fp::Double<I, Ratio<1, 10>>>();
  tests::utility::template_args_are_equal<typename internals::utility::max<I, internals::utility::fp::Double<I, Ratio<1, 3>>,
                                                                           internals::utility::fp::Double<I, Ratio<1, 10>>>::value,
                                          internals::utility::fp::Double<I, Ratio<1, 3>>>();
  tests::utility::template_args_are_equal<typename internals::utility::max<I, internals::utility::fp::Double<I, Ratio<1, 10>>,
                                                                           internals::utility::fp::Double<I, Ratio<1, 3>>>::value,
                                          internals::utility::fp::Double<I, Ratio<1, 3>>>();
}
