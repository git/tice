/******************************************************************************
 * Copyright (C) 2018  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <type_traits>
#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

namespace {
  constexpr double initval_constexpr = 1.6;
  const double initval_const = 1.0;
  double initval = .4;
  const double &initval_ref = initval;
}

int main() {
  using namespace tice::v1;
  tests::utility::run<Chan<std::remove_cv_t<decltype(initval_constexpr)>, &initval_constexpr>>();
  tests::utility::run<Chan<std::remove_cv_t<decltype(initval_const)>, &initval_const>>();
  tests::utility::run<Chan<decltype(initval), &initval>>();
  tests::utility::run<Chan<double, &initval>>();
  tests::utility::run<Chan<double, &initval_const>>();
  tests::utility::run<Chan<double, &initval_constexpr>>();

  typedef Chan<std::remove_cv_t<decltype(initval_constexpr)>, &initval_constexpr> chan1;
  tests::utility::template_args_are_equal<
    Tuple<chan1::type, chan1::initial_value>,
    Tuple<double, Value<const double *, &initval_constexpr>>>();

  typedef Chan<std::remove_cv_t<decltype(initval_const)>, &initval_const> chan2;
  tests::utility::template_args_are_equal<
    Tuple<chan2::type, chan2::initial_value>,
    Tuple<double, Value<const double *, &initval_const>>>();

  typedef Chan<decltype(initval), &initval> chan3;
  tests::utility::template_args_are_equal<
    Tuple<chan3::type, chan3::initial_value>,
    Tuple<double, Value<const double *, &initval>>>();

  typedef Chan<double, &initval> chan4;
  tests::utility::template_args_are_equal<
    Tuple<chan4::type, chan4::initial_value>,
    Tuple<double, Value<const double *, &initval>>>();

  typedef Chan<double, &initval_const> chan5;
  tests::utility::template_args_are_equal<
    Tuple<chan5::type, chan5::initial_value>,
    Tuple<double, Value<const double *, &initval_const>>>();

  typedef Chan<double, &initval_constexpr> chan6;
  tests::utility::template_args_are_equal<
    Tuple<chan6::type, chan6::initial_value>,
    Tuple<double, Value<const double *, &initval_constexpr>>>();
}
