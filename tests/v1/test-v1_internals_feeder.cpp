/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

struct A {
  constexpr A() : data(.0) {
  }
  constexpr A(double data) : data(data) {
  }
  constexpr operator double() const {
    return data;
  }
protected:
  double data;
};

struct B : A {
public:
  constexpr B() : A(.0), data_sqrt(.0) {
  }
  constexpr B(const A& a) : A(a), data_sqrt(sqrt(data)) {
  }
  constexpr operator double() const {
    return data_sqrt;
  }
protected:
  double data_sqrt;
  constexpr double sqrt(double S) {
    if (S < 0 || S == 0 || S == 1) {
      return S;
    }
    double z = S;
    do {
      z = (z / 2.0 + S / 2.0 / z);
    } while (z * z - S >= 1E-15);
    return z;
  }
};

A fn1() {
  return {.24};
}

int fn2() {
  return 45;
}

A fn3(const A &arg1, const A &arg2) {
  return {static_cast<double>(arg1)
          / static_cast<double>(arg2)};
}

bool fn4(const B &arg1) {
  return static_cast<double>(arg1) > 1.0;
}

void fn5(bool arg1) {
}

void fn6(double arg1) {
}

namespace {
  constexpr A arc_1_3_init_val(B(7.3));
  const double arc_3_6_init_val = 1.3;

  int arc_2_3_init_val;
  B arc_3_4_init_val;
}

int main(int argc, char *argv[]) {

  for (arc_2_3_init_val = 0; argv[0][arc_2_3_init_val]; ++arc_2_3_init_val);
  arc_3_4_init_val = A(arc_2_3_init_val);

  using namespace tice::v1;
  typedef Comp(&fn1, Ratio<1, 1000>) comp1;
  typedef Comp(&fn2, Ratio<1, 1000>) comp2;
  typedef Comp(&fn3, Ratio<1, 1000>) comp3;
  typedef Comp(&fn4, Ratio<1, 1000>) comp4;
  typedef Comp(&fn5, Ratio<1, 1000>) comp5;
  typedef Comp(&fn6, Ratio<1, 1000>) comp6;
  typedef Node<comp1, Ratio<4, 3>> node1;
  typedef Node<comp2, Ratio<4, 3>> node2;
  typedef Node<comp3, Ratio<4, 3>> node3;
  typedef Node<comp4, Ratio<4, 3>> node4;
  typedef Node<comp5, Ratio<4, 3>> node5;
  typedef Node<comp6, Ratio<4, 3>> node6;
  tests::utility::run<Feeder<node1, Chan<A, &arc_1_3_init_val>,
                             node2, Chan<int, &arc_2_3_init_val>, node3>>();
  tests::utility::run<Feeder<node3, Chan<B, &arc_3_4_init_val>, node4>>();
  tests::utility::run<Feeder<node4,
                             Chan_inlit<bool, (static_cast<double>(arc_1_3_init_val) > 5.0)>,
                             node5>>();
  tests::utility::run<Feeder<node3, Chan<double, &arc_3_6_init_val>, node6>>();

  tests::utility::template_args_are_equal<
    Feeder<node1, Chan<A, &arc_1_3_init_val>,
           node2, Chan<int, &arc_2_3_init_val>, node3>::arcs,
    internals::tuple::construct<I, internals::pair::construct<I, internals::pair::construct<I, node1, node3>, Chan<A, &arc_1_3_init_val>>,
                                internals::pair::construct<I, internals::pair::construct<I, node2, node3>, Chan<int, &arc_2_3_init_val>>>>();
  tests::utility::template_args_are_equal<
    Feeder<node3, Chan<B, &arc_3_4_init_val>, node4>::arcs,
    internals::tuple::construct<I, internals::pair::construct<I, internals::pair::construct<I, node3, node4>, Chan<B, &arc_3_4_init_val>>>>();
  tests::utility::template_args_are_equal<
    Feeder<node4, Chan_inlit<bool, (static_cast<double>(arc_1_3_init_val) > 5.0)>, node5>::arcs,
    internals::tuple::construct<I, internals::pair::construct<I, internals::pair::construct<I, node4, node5>,
                                                              Chan_inlit<bool, (static_cast<double>(arc_1_3_init_val) > 5.0)>>>>();
  tests::utility::template_args_are_equal<
    Feeder<node3, Chan<double, &arc_3_6_init_val>, node6>::arcs,
    internals::tuple::construct<I, internals::pair::construct<I, internals::pair::construct<I, node3, node6>, Chan<double, &arc_3_6_init_val>>>>();
}
