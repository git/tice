/******************************************************************************
 * Copyright (C) 2018  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

int fn1() {
  return 2;
}

struct Private_double {
  Private_double() : value(.0) {};
  constexpr Private_double(double d) : value(d) {};
  Private_double(const Private_double &) = delete;
  double value;
};

Private_double fn2() {
  return {.64};
}

double fn3(int arg1, const Private_double &arg2) {
  return arg1 + arg2.value;
}

static constexpr double init_1 = 2.56;
static constexpr Private_double init_2 = {1.28};

int main() {
  using namespace tice::v1;
  typedef Node<Comp(&fn1, Ratio<1>), Ratio<4, 3>> v_p_1;
  typedef Node<Comp(&fn2, Ratio<1>), Ratio<4, 3>> v_p_2;
  typedef Node<Comp(&fn3, Ratio<1>), Ratio<4, 3>> v_c;

  tests::utility::run<Feeder<v_p_1, Chan_inlit<int, 8>,
                             v_p_2, Chan<Private_double, &init_2>, v_c>>();
}
