 ##############################################################################
 # Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

: fatal error: static_assert failed "Arg 1 cannot decay to a non-function pointer type (e.g., both `T\[\]' and `T\[n\]' decay to `T \*' for any valid type `T' and size `n')"$
: note: in instantiation of template class 'tice::v1::error::utility::point_equal_lvalues::arg_1_is_array_type<false, void (\*const)()>' requested here$
^v1/test-v1_internals_utility-fail_25.cpp:27:75: note: .\+ requested here$
