 ##############################################################################
 # Copyright (C) 2018  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

: In instantiation of ‘struct tice::v1::error::array::make::value::value_is_convertible_to_array_element_type<false, const int, {anonymous}::Int_phobic_element>’:$
^v1/test-v1_internals_array-fail_17.cpp:37:74:   required from here$
: error: static assertion failed: Arg 2 has its `value' nonconvertible to array element type$
