/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

int fn1() {
  return 3;
}

int fn2() {
  return 4;
}

void fn4(int a) {
}

int main() {
  using namespace tice::v1;
  typedef Node<Comp(&fn1, Ratio<1, 100>), Ratio<1>> sensor_node_1;
  typedef Node<Comp(&fn2, Ratio<1, 100>), Ratio<1>> sensor_node_2;
  typedef Node<Comp(&fn4, Ratio<1, 100>), Ratio<1>> actuator_node;
  typedef Ratio<0> threshold_1;
  typedef Ratio<10> threshold_2;

  typedef Correlation<actuator_node, threshold_1, sensor_node_1> correlation_1;
  tests::utility::template_args_are_equal<Tuple<correlation_1::actuator, typename correlation_1::threshold, typename correlation_1::sensors>,
                                          Tuple<actuator_node, threshold_1, internals::tuple::construct<I, sensor_node_1>>>();

  typedef Correlation<actuator_node, threshold_1, sensor_node_1, sensor_node_2> correlation_2;
  tests::utility::template_args_are_equal<Tuple<correlation_2::actuator, typename correlation_2::threshold, typename correlation_2::sensors>,
                                          Tuple<actuator_node, threshold_1, internals::tuple::construct<I, sensor_node_1, sensor_node_2>>>();

  typedef Correlation<actuator_node, threshold_1, sensor_node_2, sensor_node_1> correlation_3;
  tests::utility::template_args_are_equal<Tuple<correlation_3::actuator, typename correlation_3::threshold, typename correlation_3::sensors>,
                                          Tuple<actuator_node, threshold_1, internals::tuple::construct<I, sensor_node_2, sensor_node_1>>>();

  typedef Correlation<actuator_node, threshold_2, sensor_node_1, sensor_node_2> correlation_4;
  tests::utility::template_args_are_equal<Tuple<correlation_4::actuator, typename correlation_4::threshold, typename correlation_4::sensors>,
                                          Tuple<actuator_node, threshold_2, internals::tuple::construct<I, sensor_node_1, sensor_node_2>>>();
}
