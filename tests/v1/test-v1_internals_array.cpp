/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

namespace {

  struct Element_assignable_to_int_and_vice_versa {
    int data;
    constexpr Element_assignable_to_int_and_vice_versa(int data) : data{data} {
    }
    constexpr operator int() const {
      return data;
    }
  };

  template<typename T>
  struct Element_1 {
    static constexpr T value {-1};
  };

}
namespace {

  constexpr tice::v1::Pair<bool, tice::v1::array::Idx> true_to_idx(const tice::v1::array::Idx &i, const bool &elem) {
    if (elem) {
      return {true, i};
    } else {
      return {false, tice::v1::array::Invalid_idx::value};
    }
  }

}
namespace {

  constexpr tice::v1::Pair<bool, tice::v1::array::Idx> remove_zero_idx(const tice::v1::array::Idx &i, const tice::v1::array::Idx &elem) {
    return {elem != 0, elem};
  }

}
namespace {

  constexpr int a {-1};

  constexpr const int &reference_to_a = a;

  struct init_val_whose_value_is_reference_to_a {
    static constexpr const int &value = a;
  };

  template<tice::v1::dict::Key arg__key>
  using Entry_reference_to_a = tice::v1::dict::Entry<arg__key, void, tice::v1::Value<const int &, a>>;

  constexpr int b {1};

}
namespace {

  template<tice::v1::dict::Key arg__key>
  struct dict_Entry_double {
    static constexpr tice::v1::dict::Key key {arg__key};
    static constexpr double value {-2.5};
  };

}
namespace {

  template<tice::v1::dict::Key arg__key, typename... args__three_Array__of__two_int__subarray>
  struct dict_Entry_int_array_3x2 {
    static constexpr tice::v1::dict::Key key {arg__key};
    static constexpr const int *const *value {tice::v1::Array<const int *, args__three_Array__of__two_int__subarray::elems...>::elems};
  };

  template<tice::v1::dict::Key arg__key, typename... args__three_Array__of__two_int__subarray>
  struct dict_Entry_int_array_3x2_2 {
    static constexpr tice::v1::dict::Key key {arg__key};
    static constexpr const int *const *value {tice::v1::array::make::nested::value<args__three_Array__of__two_int__subarray...>::elems};
  };

}
namespace {

  template<typename arg__Array__array>
  constexpr bool array_4x3x2_elems_are_correct = (true
                                                  && arg__Array__array::elems[0][0][0] == 1  && arg__Array__array::elems[0][0][1] == 2
                                                  && arg__Array__array::elems[0][1][0] == 3  && arg__Array__array::elems[0][1][1] == 4
                                                  && arg__Array__array::elems[0][2][0] == 5  && arg__Array__array::elems[0][2][1] == 6

                                                  && arg__Array__array::elems[1][0][0] == 7  && arg__Array__array::elems[1][0][1] == 8
                                                  && arg__Array__array::elems[1][1][0] == 9  && arg__Array__array::elems[1][1][1] == 10
                                                  && arg__Array__array::elems[1][2][0] == 11 && arg__Array__array::elems[1][2][1] == 12

                                                  && arg__Array__array::elems[2][0][0] == 13 && arg__Array__array::elems[2][0][1] == 14
                                                  && arg__Array__array::elems[2][1][0] == 15 && arg__Array__array::elems[2][1][1] == 16
                                                  && arg__Array__array::elems[2][2][0] == 17 && arg__Array__array::elems[2][2][1] == 18

                                                  && arg__Array__array::elems[3][0][0] == 19 && arg__Array__array::elems[3][0][1] == 20
                                                  && arg__Array__array::elems[3][1][0] == 21 && arg__Array__array::elems[3][1][1] == 22
                                                  && arg__Array__array::elems[3][2][0] == 23 && arg__Array__array::elems[3][2][1] == 24);

  template<int... args>
  struct array_int_2 {
    typedef int element_type;
    static constexpr tice::v1::array::Size size {2};
    typedef element_type type[size];
    static constexpr type elems {args...};
    static constexpr const int *elems_ptr[1] {elems};
  };

  template<typename arg__Array__array>
  constexpr bool array_4x3x2_nondistinct_elems_are_correct = (true
                                                              && arg__Array__array::elems[0][0][0] == 1  && arg__Array__array::elems[0][0][1] == 2
                                                              && arg__Array__array::elems[0][1][0] == 3  && arg__Array__array::elems[0][1][1] == 4
                                                              && arg__Array__array::elems[0][2][0] == 5  && arg__Array__array::elems[0][2][1] == 6

                                                              && arg__Array__array::elems[1][0][0] == 7  && arg__Array__array::elems[1][0][1] == 8
                                                              && arg__Array__array::elems[1][1][0] == 9  && arg__Array__array::elems[1][1][1] == 10
                                                              && arg__Array__array::elems[1][2][0] == 11 && arg__Array__array::elems[1][2][1] == 12

                                                              && arg__Array__array::elems[2][0][0] == 13 && arg__Array__array::elems[2][0][1] == 14
                                                              && arg__Array__array::elems[2][1][0] == 15 && arg__Array__array::elems[2][1][1] == 16
                                                              && arg__Array__array::elems[2][2][0] == 17 && arg__Array__array::elems[2][2][1] == 18

                                                              && arg__Array__array::elems[3][0][0] == 1  && arg__Array__array::elems[3][0][1] == 2
                                                              && arg__Array__array::elems[3][1][0] == 3  && arg__Array__array::elems[3][1][1] == 4
                                                              && arg__Array__array::elems[3][2][0] == 5  && arg__Array__array::elems[3][2][1] == 6);

  constexpr tice::v1::Pair<bool,
                           const int *const *> array_4x3x2_ftm_fn_1(const tice::v1::array::Idx &idx, const int *const *const &prev_array_element) {
    return {true, prev_array_element};
  }
  constexpr tice::v1::Pair<bool,
                           const int *const *> array_4x3x2_ftm_fn_2(const tice::v1::array::Idx &idx, const int *const *const &prev_array_element) {
    return {false, nullptr};
  }
  constexpr tice::v1::Pair<bool,
                           const int *const *> array_4x3x2_ftm_fn_3(const tice::v1::array::Idx &idx, const int *const *const &prev_array_element) {
    using namespace tice::v1;
    return {true, (idx < 3
                   ? prev_array_element
                   : Array<const int *, Array<int, 1, 2>::elems, Array<int, 3, 4>::elems, Array<int, 5, 6>::elems>::elems)};
  }
  constexpr tice::v1::Pair<bool,
                           const int *const *> array_4x3x2_2_ftm_fn_3(const tice::v1::array::Idx &idx,
                                                                      const int *const *const &prev_array_element)
  {
    using namespace tice::v1;
    return {true, (idx < 3
                   ? prev_array_element
                   : array::make::nested::value<Array<int, 1, 2>, Array<int, 3, 4>, Array<int, 5, 6>>::elems)};
  }

}
namespace {

  struct dict_Entry_with_internal_linkage_whose_value_is_function {
    static constexpr tice::v1::dict::Key key {0};
    static int value(int a) {
      return 2 * a;
    }
  };

}

struct dict_Entry_with_external_linkage_whose_value_is_function {
  static constexpr tice::v1::dict::Key key {0};
  static int value(int a);
};

int main() {
  using namespace tice::v1;

  tests::utility::template_args_are_equal<
    Value<bool, array::element_type_is_valid<int>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::element_type_is_valid<int &>::value>,
    Value<bool, false>>();

  typedef array::Empty<int> empty_int_array;
  tests::utility::template_args_are_equal<
    Value<bool, array::is_empty<empty_int_array>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, internals::utility::has_type_as_member_typedef<I, empty_int_array>::value>,
          Value<bool, internals::utility::has_elems_as_data_member<I, empty_int_array>::value>>,
    Tuple<Value<bool, false>, Value<bool, false>>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::elements_are_pairwise_distinct<empty_int_array>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<empty_int_array, empty_int_array>::value>,
    Value<bool, true>>();

  typedef array::make::value<10, Value<int, -1>> array1;
  tests::utility::template_args_are_equal<
    Value<bool, array::is_empty<array1>::value>,
    Value<bool, false>>();
  typedef Array<int, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1> array1_ref;
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array1, array1_ref>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array1,
                                             Array<int,
                                                   reference_to_a, reference_to_a, reference_to_a, reference_to_a, reference_to_a,
                                                   reference_to_a, reference_to_a, reference_to_a, reference_to_a, reference_to_a>>::value>,
    Value<bool, true>>();
  typedef array::are_elementwise_equal<array1,
                                       Array<int,
                                             b, reference_to_a, reference_to_a, reference_to_a, reference_to_a,
                                             reference_to_a, reference_to_a, reference_to_a, reference_to_a, reference_to_a>>
    elementwise_equal_test_1;
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, elementwise_equal_test_1::value>, Value<array::Idx, elementwise_equal_test_1::idx>>,
    Tuple<Value<bool, false>, Value<array::Idx, 0>>>();
  typedef array::are_elementwise_equal<array1,
                                       Array<int,
                                             reference_to_a, reference_to_a, reference_to_a, reference_to_a, reference_to_a,
                                             reference_to_a, reference_to_a, reference_to_a, reference_to_a, b>>
    elementwise_equal_test_2;
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, elementwise_equal_test_2::value>, Value<array::Idx, elementwise_equal_test_2::idx>>,
    Tuple<Value<bool, false>, Value<array::Idx, 9>>>();
  typedef array::are_elementwise_equal<array1,
                                       Array<int,
                                             reference_to_a, reference_to_a, reference_to_a, reference_to_a, reference_to_a,
                                             reference_to_a, b, reference_to_a, reference_to_a, reference_to_a>>
    elementwise_equal_test_3;
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, elementwise_equal_test_3::value>, Value<array::Idx, elementwise_equal_test_3::idx>>,
    Tuple<Value<bool, false>, Value<array::Idx, 6>>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::make::value<1, dict::Entry_key<7>>, Array<dict::Key, 7>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::make::value<1, init_val_whose_value_is_reference_to_a>, Array<int, -1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::make::value<3, Element_1<Element_assignable_to_int_and_vice_versa>, int>,
                                             Array<int, -1, -1, -1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::make::value<3, Element_1<double>, int>,
                                             Array<int, -1, -1, -1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::make::from_tuple::value<Tuple<dict::Entry_int<10, -1>,
                                                                                  dict::Entry_int<0, 3>,
                                                                                  dict::Entry_int<0, 9>>>,
                                             Array<int, -1, 3, 9>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::make::from_tuple::value<Tuple<init_val_whose_value_is_reference_to_a>>,
                                             Array<int, -1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::make::from_tuple::value<Tuple<Element_1<int>,
                                                                                  Element_1<double>,
                                                                                  Element_1<int>,
                                                                                  Element_1<Element_assignable_to_int_and_vice_versa>>>,
                                             Array<int, -1, -1, -1, -1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::make::from_tuple::value<Tuple<Element_1<Element_assignable_to_int_and_vice_versa>>, int>,
                                             Array<int, -1>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::make::from_tuple::value<Tuple<dict_Entry_with_internal_linkage_whose_value_is_function>,
                                                                            int(*)(int)>,
                                             Array<int(*)(int), dict_Entry_with_internal_linkage_whose_value_is_function::value>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::make::from_tuple::value<Tuple<dict_Entry_with_external_linkage_whose_value_is_function>,
                                                                            int(*)(int)>,
                                             Array<int(*)(int), dict_Entry_with_external_linkage_whose_value_is_function::value>>::value>,
    Value<bool, true>>();
  typedef array::elements_are_pairwise_distinct<array1> check_distinct_1;
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, check_distinct_1::value>, Value<array::Idx, check_distinct_1::lhs_idx>, Value<array::Idx, check_distinct_1::rhs_idx>>,
    Tuple<Value<bool, false>, Value<array::Idx, 0>, Value<array::Idx, 1>>>();

  typedef array::update::value<array::Empty<int>, Tuple<>> array_update_corner_case_1;
  tests::utility::template_args_are_equal<
    Tuple<array_update_corner_case_1::element_type, Value<array::Size, array_update_corner_case_1::size>,
          Value<bool, internals::utility::has_type_as_member_typedef<I, array_update_corner_case_1>::value>,
          Value<bool, internals::utility::has_elems_as_data_member<I, array_update_corner_case_1>::value>>,
    Tuple<int, Value<array::Size, 0>, Value<bool, false>, Value<bool, false>>>();

  typedef array::update::value<array::Empty<int>, Tuple<dict::Entry_int<0, 1>>> array_update_corner_case_2;
  tests::utility::template_args_are_equal<
    Tuple<array_update_corner_case_2::element_type, Value<array::Size, array_update_corner_case_2::size>,
          Value<bool, internals::utility::has_type_as_member_typedef<I, array_update_corner_case_2>::value>,
          Value<bool, internals::utility::has_elems_as_data_member<I, array_update_corner_case_2>::value>>,
    Tuple<int, Value<array::Size, 0>, Value<bool, false>, Value<bool, false>>>();

  typedef array::update::value<Array<int, 1>, Tuple<>> array_update_corner_case_3;
  tests::utility::template_args_are_equal<
    Tuple<array_update_corner_case_3::element_type, Value<array::Size, array_update_corner_case_3::size>, array_update_corner_case_3::type,
          std::remove_reference_t<decltype(array_update_corner_case_3::elems)>>,
    Tuple<int, Value<array::Size, 1>, int[1], const int[1]>>();

  typedef array::update::value<array1, Tuple<dict::Entry_int<0, 1>,
                                             dict::Entry_int<1, 2>,
                                             dict::Entry_int<2, 3>,
                                             dict::Entry_int<3, 4>,
                                             dict::Entry_int<4, 5>,
                                             dict::Entry_int<5, 6>,
                                             dict::Entry_int<6, 7>,
                                             dict::Entry_int<7, 8>,
                                             dict::Entry_int<8, 9>,
                                             dict::Entry_int<9, 10>>> array2;
  typedef Array<int, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10> array2_ref;
  tests::utility::template_args_are_equal<
    Tuple<array2::type, array2::element_type, Value<array::Size, array2::size>>,
    Tuple<array2_ref::type, array2_ref::element_type, Value<array::Size, array2_ref::size>>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array2, array2_ref>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<array2, Tuple<>>, array2_ref>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<array1, Tuple<dict::Entry_int<9, 10>,
                                                                                dict::Entry_int<8, 9>,
                                                                                dict::Entry_int<7, 8>,
                                                                                dict::Entry_int<6, 7>,
                                                                                dict::Entry_int<5, 6>,
                                                                                dict::Entry_int<4, 5>,
                                                                                dict::Entry_int<3, 4>,
                                                                                dict::Entry_int<2, 3>,
                                                                                dict::Entry_int<1, 2>,
                                                                                dict::Entry_int<0, 1>
                                                                                >>, array2_ref>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1, 2, 3, 4, 5>, Tuple<Entry_reference_to_a<0>,
                                                                                                   Entry_reference_to_a<3>>>,
                                             Array<int, -1, 2, 3, -1, 5>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1>, Tuple<dict_Entry_double<0>>>,
                                             Array<int, -2>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int(*)(int), nullptr>,
                                                                  Tuple<dict_Entry_with_internal_linkage_whose_value_is_function>>,
                                             Array<int(*)(int), dict_Entry_with_internal_linkage_whose_value_is_function::value>>::value>,
    Value<bool, true>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int(*)(int), nullptr>,
                                                                  Tuple<dict_Entry_with_external_linkage_whose_value_is_function>>,
                                             Array<int(*)(int), dict_Entry_with_external_linkage_whose_value_is_function::value>>::value>,
    Value<bool, true>>();
  typedef array::are_elementwise_equal<array1, array2> check_equal_1_2;
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, check_equal_1_2::value>, Value<array::Idx, check_equal_1_2::idx>>,
    Tuple<Value<bool, false>, Value<array::Idx, 0>>>();
  tests::utility::template_args_are_equal<
    Value<bool, array::elements_are_pairwise_distinct<array2>::value>,
    Value<bool, true>>();

  typedef Array<int, 1, 2, 3, 4, 5, 1> array3;
  typedef array::elements_are_pairwise_distinct<array3> check_distinct_3;
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, check_distinct_3::value>, Value<array::Idx, check_distinct_3::lhs_idx>, Value<array::Idx, check_distinct_3::rhs_idx>>,
    Tuple<Value<bool, false>, Value<array::Idx, 0>, Value<array::Idx, 5>>>();

  typedef Array<int, 1, 2, 3, 4, 4> array4;
  typedef array::elements_are_pairwise_distinct<array4> check_distinct_4;
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, check_distinct_4::value>, Value<array::Idx, check_distinct_4::lhs_idx>, Value<array::Idx, check_distinct_4::rhs_idx>>,
    Tuple<Value<bool, false>, Value<array::Idx, 3>, Value<array::Idx, 4>>>();

  typedef Array<int, 1, 2, 3, 2, 5> array5;
  typedef array::elements_are_pairwise_distinct<array5> check_distinct_5;
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, check_distinct_5::value>, Value<array::Idx, check_distinct_5::lhs_idx>, Value<array::Idx, check_distinct_5::rhs_idx>>,
    Tuple<Value<bool, false>, Value<array::Idx, 1>, Value<array::Idx, 3>>>();

  typedef array::are_elementwise_equal<array4, array5> check_equal_4_5;
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, check_equal_4_5::value>, Value<array::Idx, check_equal_4_5::idx>>,
    Tuple<Value<bool, false>, Value<array::Idx, 3>>>();

  typedef Array<int, 1> array6;
  tests::utility::template_args_are_equal<
    Value<bool, array::elements_are_pairwise_distinct<array6>::value>,
    Value<bool, true>>();

  typedef Array<int, 1, 2> array7;
  tests::utility::template_args_are_equal<
    Value<bool, array::elements_are_pairwise_distinct<array7>::value>,
    Value<bool, true>>();

  typedef Array<int, 1, 1> array8;
  typedef array::elements_are_pairwise_distinct<array8> check_distinct_8;
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, check_distinct_8::value>, Value<array::Idx, check_distinct_8::lhs_idx>, Value<array::Idx, check_distinct_8::rhs_idx>>,
    Tuple<Value<bool, false>, Value<array::Idx, 0>, Value<array::Idx, 1>>>();

  typedef Array<int, 2> array9;
  typedef array::are_elementwise_equal<array6, array9> check_equal_6_9;
  tests::utility::template_args_are_equal<
    Tuple<Value<bool, check_equal_6_9::value>, Value<array::Idx, check_equal_6_9::idx>>,
    Tuple<Value<bool, false>, Value<array::Idx, 0>>>();

  typedef Array<int, 1> array10;
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array6, array10>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<array9, Tuple<dict::Entry_int<0, 1>>>, array10>::value>,
    Value<bool, true>>();

  typedef Array<int, -1, 2, -1, 4, 5, 6, -1, 8, -1, -1> array11_ref;
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<array1, Tuple<dict::Entry_int<1, 2>,
                                                                                dict::Entry_int<3, 4>,
                                                                                dict::Entry_int<4, 5>,
                                                                                dict::Entry_int<5, 6>,
                                                                                dict::Entry_int<7, 8>>>, array11_ref>::value>,
    Value<bool, true>>();

  typedef Array<int, -1, 8, -1, 6, 5, 4, -1, 2, -1, -1> array12_ref;
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<array1, Tuple<dict::Entry_int<7, 2>,
                                                                                dict::Entry_int<5, 4>,
                                                                                dict::Entry_int<4, 5>,
                                                                                dict::Entry_int<3, 6>,
                                                                                dict::Entry_int<1, 8>>>, array12_ref>::value>,
    Value<bool, true>>();

  typedef Array<int, -1, -8, -1, -1, -1, -1, -1, -2, -1, -1> array13_ref;
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<array1, Tuple<dict::Entry_int<7, -2>,
                                                                                dict::Entry_int<1, -8>>>, array13_ref>::value>,
    Value<bool, true>>();

  typedef Array<int, -1, -1, -1, -1, -1, -1, -1, -2, -1, -1> array14_ref;
  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<array1, Tuple<dict::Entry_int<7, -2>>>, array14_ref>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1, 2, 3, 4, 5>,
                                                                  Tuple<dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<1, 0>,
                                                                        dict::Entry_int<4, 0>>>,
                                             Array<int, 1, 0, 0, 4, 0>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1, 2, 3, 4, 5>,
                                                                  Tuple<dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<1, 0>,
                                                                        dict::Entry_int<40, 0>>,
                                                                  array::update::on_dupd_index::BREAK,
                                                                  array::update::on_stray_index::CONTINUE>,
                                             Array<int, 1, 0, 0, 4, 5>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1, 2, 3, 4, 5>,
                                                                  Tuple<dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<10, 0>,
                                                                        dict::Entry_int<4, 0>>,
                                                                  array::update::on_dupd_index::BREAK,
                                                                  array::update::on_stray_index::CONTINUE>,
                                             Array<int, 1, 2, 0, 4, 0>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1, 2, 3, 4, 5>,
                                                                  Tuple<dict::Entry_int<20, 0>,
                                                                        dict::Entry_int<1, 0>,
                                                                        dict::Entry_int<4, 0>>,
                                                                  array::update::on_dupd_index::BREAK,
                                                                  array::update::on_stray_index::CONTINUE>,
                                             Array<int, 1, 0, 3, 4, 0>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1, 2, 3, 4, 5>,
                                                                  Tuple<dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<1, 0>,
                                                                        dict::Entry_int<2, 0>>,
                                                                  array::update::on_dupd_index::CONTINUE_IF_IDENTICAL>,
                                             Array<int, 1, 0, 0, 4, 5>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1, 2, 3, 4, 5>,
                                                                  Tuple<dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<1, 0>,
                                                                        dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<4, 0>,
                                                                        dict::Entry_int<1, 0>>,
                                                                  array::update::on_dupd_index::CONTINUE_IF_IDENTICAL>,
                                             Array<int, 1, 0, 0, 4, 0>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1, 2, 3, 4, 5>,
                                                                  Tuple<dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<1, 0>,
                                                                        dict::Entry_int<2, 1>,
                                                                        dict::Entry_int<2, 2>,
                                                                        dict::Entry_int<4, 0>,
                                                                        dict::Entry_int<1, 1>>,
                                                                  array::update::on_dupd_index::CONTINUE_KEEP_FIRST>,
                                             Array<int, 1, 0, 0, 4, 0>>().value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1, 2, 3, 4, 5>,
                                                                  Tuple<dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<1, 0>,
                                                                        dict::Entry_int<2, 1>,
                                                                        dict::Entry_int<2, 2>,
                                                                        dict::Entry_int<4, 0>,
                                                                        dict::Entry_int<1, 1>>,
                                                                  array::update::on_dupd_index::CONTINUE_KEEP_LAST>,
                                             Array<int, 1, 1, 2, 4, 0>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1, 2, 3, 4, 5>,
                                                                  Tuple<dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<1, 0>,
                                                                        dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<40, 0>,
                                                                        dict::Entry_int<1, 0>>,
                                                                  array::update::on_dupd_index::CONTINUE_IF_IDENTICAL,
                                                                  array::update::on_stray_index::CONTINUE>,
                                             Array<int, 1, 0, 0, 4, 5>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1, 2, 3, 4, 5>,
                                                                  Tuple<dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<1, 0>,
                                                                        dict::Entry_int<2, 1>,
                                                                        dict::Entry_int<2, 2>,
                                                                        dict::Entry_int<40, 0>,
                                                                        dict::Entry_int<1, 1>>,
                                                                  array::update::on_dupd_index::CONTINUE_KEEP_FIRST,
                                                                  array::update::on_stray_index::CONTINUE>,
                                             Array<int, 1, 0, 0, 4, 5>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::update::value<Array<int, 1, 2, 3, 4, 5>,
                                                                  Tuple<dict::Entry_int<2, 0>,
                                                                        dict::Entry_int<1, 0>,
                                                                        dict::Entry_int<2, 1>,
                                                                        dict::Entry_int<2, 2>,
                                                                        dict::Entry_int<40, 0>,
                                                                        dict::Entry_int<1, 1>>,
                                                                  array::update::on_dupd_index::CONTINUE_KEEP_LAST,
                                                                  array::update::on_stray_index::CONTINUE>,
                                             Array<int, 1, 1, 2, 4, 5>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::make::from_tuple::value<Tuple<dict::Entry_int<10, -1>,
                                                                                  dict::Entry_int<0, 3>,
                                                                                  dict::Entry_int<0, 9>>>,
                                             Array<int, -1, 3, 9>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<Value<array::Size, array::filter_then_map::value<Array<bool, false>, array::Idx, true_to_idx>::size>,
                                          Value<array::Size, 0>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::filter_then_map::value<Array<bool, true>, array::Idx, true_to_idx>,
                                             Array<array::Idx, 0>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::filter_then_map::value<Array<bool, true, false, false, false, false>, array::Idx, true_to_idx>,
                                             Array<array::Idx, 0>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<array::filter_then_map::value<Array<bool, false, false, true, false, true>, array::Idx, true_to_idx>,
                                             Array<array::Idx, 2, 4>>::value>,
    Value<bool, true>>();
  typedef array::filter_then_map::value<array::Empty<bool>, array::Idx, true_to_idx> filter_then_map_1;
  tests::utility::template_args_are_equal<
    Tuple<filter_then_map_1::element_type, Value<array::Size, filter_then_map_1::size>,
          Value<bool, internals::utility::has_type_as_member_typedef<I, filter_then_map_1>::value>,
          Value<bool, internals::utility::has_elems_as_constexpr_copyable_data_member<I, filter_then_map_1>::value>>,
    Tuple<array::Idx, Value<array::Size, 0>, Value<bool, false>, Value<bool, false>>>();
  typedef array::filter_then_map::value<Array<bool, false, false, false, false, false>, array::Idx, true_to_idx> filter_then_map_2;
  tests::utility::template_args_are_equal<
    Tuple<filter_then_map_2::element_type, Value<array::Size, filter_then_map_2::size>,
          Value<bool, internals::utility::has_type_as_member_typedef<I, filter_then_map_2>::value>,
          Value<bool, internals::utility::has_elems_as_constexpr_copyable_data_member<I, filter_then_map_2>::value>>,
    Tuple<array::Idx, Value<array::Size, 0>, Value<bool, false>, Value<bool, false>>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<
      array::filter_then_map::value<
        array::update::value<
          array::filter_then_map::value<
            Array<bool, false, false, true, false, true>,
            array::Idx, true_to_idx>,
          Tuple<dict::Entry_int<0, 0>, dict::Entry_int<1, 3>>>,
        array::Idx, remove_zero_idx>,
                  Array<array::Idx, 3>>::value>,
    Value<bool, true>>();

  tests::utility::template_args_are_equal<
    Value<bool, array::are_elementwise_equal<
      array::update::value<
        array::filter_then_map::value<
          Array<array::Idx, 0, 0, 0>,
          array::Idx, remove_zero_idx>,
        Tuple<dict::Entry_int<0, 0>, dict::Entry_int<1, 3>>>,
                  array::Empty<array::Idx>>::value>,
    Value<bool, true>>();

  //\begin{multidimensional array capability}
  typedef Array<const int *const *,
                Array<const int *, // array_4x3x2[0]
                      Array<int, 1, 2>::elems,   // array_4x3x2[0][0]
                      Array<int, 3, 4>::elems,   // array_4x3x2[0][1]
                      Array<int, 5, 6>::elems    // array_4x3x2[0][2]
                      >::elems,
                Array<const int *, // array_4x3x2[1]
                      Array<int, 7, 8>::elems,   // array_4x3x2[1][0]
                      Array<int, 9, 10>::elems,  // array_4x3x2[1][1]
                      Array<int, 11, 12>::elems  // array_4x3x2[1][2]
                      >::elems,
                Array<const int *, // array_4x3x2[2]
                      Array<int, 13, 14>::elems, // array_4x3x2[2][0]
                      Array<int, 15, 16>::elems, // array_4x3x2[2][1]
                      Array<int, 17, 18>::elems  // array_4x3x2[2][2]
                      >::elems,
                Array<const int *, // array_4x3x2[3]
                      Array<int, 19, 20>::elems, // array_4x3x2[3][0]
                      Array<int, 21, 22>::elems, // array_4x3x2[3][1]
                      Array<int, 23, 24>::elems  // array_4x3x2[3][2]
                      >::elems
                > array_4x3x2;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_elems_are_correct<array_4x3x2>>,                           Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, array::are_elementwise_equal<array_4x3x2, array_4x3x2>::value>,        Value<bool, true>>();

  typedef array::update::value<array_4x3x2,
                               Tuple<dict_Entry_int_array_3x2<0, Array<int, 1, 2>, Array<int, 3, 4>, Array<int, 5, 6>>>
                               > array_4x3x2_updt_1;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_elems_are_correct<array_4x3x2>>,                           Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, array::are_elementwise_equal<array_4x3x2, array_4x3x2_updt_1>::value>, Value<bool, true>>();

  typedef array::update::value<array_4x3x2,
                               Tuple<dict_Entry_int_array_3x2<0, array_int_2<1, 2>, array_int_2<3, 4>, array_int_2<5, 6>>>
                               > array_4x3x2_updt_2;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_elems_are_correct<array_4x3x2>>,                           Value<bool, true>>();
  typedef array::are_elementwise_equal<array_4x3x2, array_4x3x2_updt_2> are_elementwise_equal_shallow_test_1;
  tests::utility::template_args_are_equal<Tuple<Value<array::Idx, are_elementwise_equal_shallow_test_1::idx>,
                                                Value<bool, are_elementwise_equal_shallow_test_1::value>>,
                                          Tuple<Value<array::Idx, 0>,                                                        Value<bool, false>>>();

  typedef array::update::value<array_4x3x2,
                               Tuple<dict_Entry_int_array_3x2<3, Array<int, 19, 20>, Array<int, 21, 22>, Array<int, 23, 24>>>
                               > array_4x3x2_updt_3;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_elems_are_correct<array_4x3x2>>,                           Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, array::are_elementwise_equal<array_4x3x2, array_4x3x2_updt_3>::value>, Value<bool, true>>();

  typedef array::update::value<array_4x3x2,
                               Tuple<dict_Entry_int_array_3x2<3, array_int_2<19, 20>, array_int_2<21, 22>, array_int_2<23, 24>>>
                               > array_4x3x2_updt_4;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_elems_are_correct<array_4x3x2>>,                           Value<bool, true>>();
  typedef array::are_elementwise_equal<array_4x3x2, array_4x3x2_updt_4> are_elementwise_equal_shallow_test_4;
  tests::utility::template_args_are_equal<Tuple<Value<array::Idx, are_elementwise_equal_shallow_test_4::idx>,
                                                Value<bool, are_elementwise_equal_shallow_test_4::value>>,
                                          Tuple<Value<array::Idx, 3>,                                                        Value<bool, false>>>();
  // First, `array::update::value' performs a single-level update, which only updates `array_4x3x2[i]'.
  // If a multi-level update were performed, `array::update::value' could choose to update either `array_4x3x2[i]' or `array_4x3x2[i][j]' or
  // `array_4x3x2[i][j][k]'.
  // Second, `array::are_elementwise_equal' performs a single-level check, which only checks whether `array_4x3x2[i] == array_4x3x2_updt_##n[i]'.
  // If a multi-level check were performed, `array::are_elementwise_equal' could choose to check whether
  // `array_4x3x2[i] == array_4x3x2_updt_##n[i]' or `array_4x3x2[i][j] == array_4x3x2_updt_##n[i][j]' or
  // `array_4x3x2[i][j][k] == array_4x3x2_updt_##n[i][j][k]'.
  // This observation applies to the other array subunit APIs as demonstrated below.

  // 1. `array::elements_are_pairwise_distinct':
  typedef array::update::value<array_4x3x2,
                               Tuple<dict_Entry_int_array_3x2<3, Array<int, 1, 2>, Array<int, 3, 4>, Array<int, 5, 6>>>
                               > array_4x3x2_nondistinct_1;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_nondistinct_elems_are_correct<array_4x3x2_nondistinct_1>>,
                                          Value<bool, true>>();
  typedef array::elements_are_pairwise_distinct<array_4x3x2_nondistinct_1> elements_are_pairwise_distinct_shallow_test_1;
  tests::utility::template_args_are_equal<Tuple<Value<bool, elements_are_pairwise_distinct_shallow_test_1::value>,
                                                Value<array::Idx, elements_are_pairwise_distinct_shallow_test_1::lhs_idx>,
                                                Value<array::Idx, elements_are_pairwise_distinct_shallow_test_1::rhs_idx>>,
                                          Tuple<Value<bool, false>, Value<array::Idx, 0>, Value<array::Idx, 3>>>();

  typedef array::update::value<array_4x3x2,
                               Tuple<dict_Entry_int_array_3x2<3, array_int_2<1, 2>, array_int_2<3, 4>, array_int_2<5, 6>>>
                               > array_4x3x2_nondistinct_2;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_nondistinct_elems_are_correct<array_4x3x2_nondistinct_2>>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, array::elements_are_pairwise_distinct<array_4x3x2_nondistinct_2>::value>,
                                          Value<bool, true>>();

  // 2. `array::filter_then_map::value':
  // 2.1. An identity map:
  typedef array::filter_then_map::value<array_4x3x2, typename array_4x3x2::element_type, array_4x3x2_ftm_fn_1> array_4x3x2_ftm_1;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_elems_are_correct<array_4x3x2_ftm_1>>,                    Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, array::are_elementwise_equal<array_4x3x2, array_4x3x2_ftm_1>::value>, Value<bool, true>>();
  // 2.2. An erasing filter:
  typedef array::filter_then_map::value<array_4x3x2, typename array_4x3x2::element_type, array_4x3x2_ftm_fn_2> array_4x3x2_ftm_2;
  tests::utility::template_args_are_equal<Value<bool, array::is_empty<array_4x3x2_ftm_2>::value>, Value<bool, true>>();
  // 2.3. A replacing filter:
  typedef array::filter_then_map::value<array_4x3x2, typename array_4x3x2::element_type, array_4x3x2_ftm_fn_3> array_4x3x2_ftm_3;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_nondistinct_elems_are_correct<array_4x3x2_ftm_3>>,        Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, array::are_elementwise_equal<array_4x3x2_nondistinct_1, array_4x3x2_ftm_3>::value>,
                                          Value<bool, true>>();

  // The last two array subunit APIs, namely `array::make::value' and `array::make::from_tuple::value', are not for creating a multidimensional
  // array because they are designed to extend the functionality of `Array' to values that cannot be used as a template nontype nontemplate
  // argument.  This design is reflected in the way the two subunit APIs takes as their input `static constexpr' data member `value', not `elems'.

  //\end{multidimensional array capability}

  //\begin{multidimensional array capability using array::make::nested::value}
  typedef array::make::nested::value<array::make::nested::value<Array<int, 1, 2>,
                                                                Array<int, 3, 4>,
                                                                      Array<int, 5, 6>>,
                                     array::make::nested::value<Array<int, 7, 8>,
                                                                Array<int, 9, 10>,
                                                                      Array<int, 11, 12>>,
                                     array::make::nested::value<Array<int, 13, 14>,
                                                                Array<int, 15, 16>,
                                                                      Array<int, 17, 18>>,
                                     array::make::nested::value<Array<int, 19, 20>,
                                                                Array<int, 21, 22>,
                                                                      Array<int, 23, 24>>> array_4x3x2_2;

  // Note that the one line below is expected to be false unless `tice::v1::Array' and `tice::v1::array::make::nested::value' share an underlying
  // implementation:
  tests::utility::template_args_are_equal<Value<bool, array::are_elementwise_equal<array_4x3x2, array_4x3x2_2>::value>,      Value<bool, false>>();

  // Note that the following lines simply repeat every tests within section `multidimensional array capability' on `array_4x3x2_2' by using
  // `tice::v1::array::make::nested::value' in the auxiliary function `array_4x3x2_2_ftm_fn_3' and the data structure `dict_Entry_int_array_3x2_2':
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_elems_are_correct<array_4x3x2_2>>,                         Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, array::are_elementwise_equal<array_4x3x2_2, array_4x3x2_2>::value>,    Value<bool, true>>();

  typedef array::update::value<array_4x3x2_2,
                               Tuple<dict_Entry_int_array_3x2_2<0, Array<int, 1, 2>, Array<int, 3, 4>, Array<int, 5, 6>>>
                               > array_4x3x2_2_updt_1;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_elems_are_correct<array_4x3x2_2>>,                         Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool,
                                                array::are_elementwise_equal<array_4x3x2_2, array_4x3x2_2_updt_1>::value>,   Value<bool, true>>();

  typedef array::update::value<array_4x3x2_2,
                               Tuple<dict_Entry_int_array_3x2_2<0, array_int_2<1, 2>, array_int_2<3, 4>, array_int_2<5, 6>>>
                               > array_4x3x2_2_updt_2;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_elems_are_correct<array_4x3x2_2>>,                         Value<bool, true>>();
  typedef array::are_elementwise_equal<array_4x3x2_2, array_4x3x2_2_updt_2> are_elementwise_equal_shallow_test_1_2;
  tests::utility::template_args_are_equal<Tuple<Value<array::Idx, are_elementwise_equal_shallow_test_1_2::idx>,
                                                Value<bool, are_elementwise_equal_shallow_test_1_2::value>>,
                                          Tuple<Value<array::Idx, 0>,                                                        Value<bool, false>>>();

  typedef array::update::value<array_4x3x2_2,
                               Tuple<dict_Entry_int_array_3x2_2<3, Array<int, 19, 20>, Array<int, 21, 22>, Array<int, 23, 24>>>
                               > array_4x3x2_2_updt_3;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_elems_are_correct<array_4x3x2_2>>,                         Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool,
                                                array::are_elementwise_equal<array_4x3x2_2, array_4x3x2_2_updt_3>::value>,   Value<bool, true>>();

  typedef array::update::value<array_4x3x2_2,
                               Tuple<dict_Entry_int_array_3x2_2<3, array_int_2<19, 20>, array_int_2<21, 22>, array_int_2<23, 24>>>
                               > array_4x3x2_2_updt_4;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_elems_are_correct<array_4x3x2_2>>,                         Value<bool, true>>();
  typedef array::are_elementwise_equal<array_4x3x2_2, array_4x3x2_2_updt_4> are_elementwise_equal_shallow_test_4_2;
  tests::utility::template_args_are_equal<Tuple<Value<array::Idx, are_elementwise_equal_shallow_test_4_2::idx>,
                                                Value<bool, are_elementwise_equal_shallow_test_4_2::value>>,
                                          Tuple<Value<array::Idx, 3>,                                                        Value<bool, false>>>();

  // 1. `array::elements_are_pairwise_distinct':
  typedef array::update::value<array_4x3x2_2,
                               Tuple<dict_Entry_int_array_3x2_2<3, Array<int, 1, 2>, Array<int, 3, 4>, Array<int, 5, 6>>>
                               > array_4x3x2_2_nondistinct_1;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_nondistinct_elems_are_correct<array_4x3x2_2_nondistinct_1>>,
                                          Value<bool, true>>();
  typedef array::elements_are_pairwise_distinct<array_4x3x2_2_nondistinct_1> elements_are_pairwise_distinct_shallow_test_1_2;
  tests::utility::template_args_are_equal<Tuple<Value<bool, elements_are_pairwise_distinct_shallow_test_1_2::value>,
                                                Value<array::Idx, elements_are_pairwise_distinct_shallow_test_1_2::lhs_idx>,
                                                Value<array::Idx, elements_are_pairwise_distinct_shallow_test_1_2::rhs_idx>>,
                                          Tuple<Value<bool, false>, Value<array::Idx, 0>, Value<array::Idx, 3>>>();

  typedef array::update::value<array_4x3x2_2,
                               Tuple<dict_Entry_int_array_3x2_2<3, array_int_2<1, 2>, array_int_2<3, 4>, array_int_2<5, 6>>>
                               > array_4x3x2_2_nondistinct_2;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_nondistinct_elems_are_correct<array_4x3x2_2_nondistinct_2>>,
                                          Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, array::elements_are_pairwise_distinct<array_4x3x2_2_nondistinct_2>::value>,
                                          Value<bool, true>>();

  // 2. `array::filter_then_map::value':
  // 2.1. An identity map:
  typedef array::filter_then_map::value<array_4x3x2_2, typename array_4x3x2_2::element_type, array_4x3x2_ftm_fn_1> array_4x3x2_2_ftm_1_2;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_elems_are_correct<array_4x3x2_2_ftm_1_2>>,                 Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool,
                                                array::are_elementwise_equal<array_4x3x2_2, array_4x3x2_2_ftm_1_2>::value>,  Value<bool, true>>();
  // 2.2. An erasing filter:
  typedef array::filter_then_map::value<array_4x3x2_2, typename array_4x3x2_2::element_type, array_4x3x2_ftm_fn_2> array_4x3x2_2_ftm_2_2;
  tests::utility::template_args_are_equal<Value<bool, array::is_empty<array_4x3x2_2_ftm_2_2>::value>, Value<bool, true>>();
  // 2.3. A replacing filter:
  typedef array::filter_then_map::value<array_4x3x2_2, typename array_4x3x2_2::element_type, array_4x3x2_2_ftm_fn_3> array_4x3x2_2_ftm_3_2;
  tests::utility::template_args_are_equal<Value<bool, array_4x3x2_nondistinct_elems_are_correct<array_4x3x2_2_ftm_3_2>>,     Value<bool, true>>();
  tests::utility::template_args_are_equal<Value<bool, array::are_elementwise_equal<array_4x3x2_2_nondistinct_1, array_4x3x2_2_ftm_3_2>::value>,
                                          Value<bool, true>>();
  //\end{multidimensional array capability using array::make::nested::value}
}
