/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

int fn1() {
  return 3;
}

void fn2(int a) {
}

int main() {
  using namespace tice::v1;
  typedef Node<Comp(&fn1, Ratio<1, 100>), Ratio<1>> node1;
  typedef Node<Comp(&fn2, Ratio<1, 100>), Ratio<1>> node2;
  tests::utility::run<Program<HW<Core_ids<1, 2, 3>>, node1, node2,
                              Feeder<node1, Chan_inlit<int, 0>,
                                     Node<Comp(&fn2, Ratio<1, 10>), Ratio<1>>>>>();
}
