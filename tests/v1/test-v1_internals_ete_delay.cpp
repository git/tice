/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "../../v1.hpp"
#include "../v1_test_utility.hpp"

int fn1() {
  return 3;
}

void fn4(int a) {
}

int main() {
  using namespace tice::v1;
  typedef Node<Comp(&fn1, Ratio<1, 100>), Ratio<1>> sensor_node;
  typedef Node<Comp(&fn4, Ratio<1, 100>), Ratio<1>> actuator_node;
  typedef Ratio<0> min_delay;
  typedef Ratio<10> max_delay;

  typedef ETE_delay<sensor_node, actuator_node, min_delay, max_delay> ete_delay_1;
  tests::utility::template_args_are_equal<Tuple<typename ete_delay_1::sensor, typename ete_delay_1::actuator,
                                                typename ete_delay_1::min, typename ete_delay_1::max>,
                                          Tuple<sensor_node, actuator_node, min_delay, max_delay>>();

  typedef ETE_delay<sensor_node, actuator_node, max_delay, max_delay> ete_delay_2;
  tests::utility::template_args_are_equal<Tuple<typename ete_delay_2::sensor, typename ete_delay_2::actuator,
                                                typename ete_delay_2::min, typename ete_delay_2::max>,
                                          Tuple<sensor_node, actuator_node, max_delay, max_delay>>();
}
