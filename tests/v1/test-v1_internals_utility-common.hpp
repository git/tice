/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace {

  struct array_constructor {
    double elems[3];
    constexpr array_constructor(const double alpha[3], const double x[3]) : elems{} {
      for (int i = 0; i < 3; ++i) {
        elems[i] = alpha[i] * x[i];
      }
    }
  };

  struct array_mapper {
    static constexpr double x[] {3, 5, 3};
    static constexpr double alpha[] {.5, .1, .5};
    static constexpr array_constructor result {alpha, x};
    static constexpr const double (&elems)[3] {result.elems};
    static constexpr auto elems_as_auto {result.elems};
    static constexpr const double *elems_ptr[1] {elems};
  };

  constexpr double array_result[] = {.5, .1, .5, .5 * 3, .1 * 5};
  constexpr double array_result_trunk[] = {.5 * 3, .1 * 5};

}
namespace {

  struct carrier {
    static constexpr double x {.1 * 5};
    static constexpr const double *x_ptr {&x};
    static constexpr const double *x_ptr_ptr[] = {x_ptr};
    static constexpr double y {.1 * 5};
    static constexpr double z {1.0 / 3.0};
  };

}
namespace {

  void fn_1() {
  }

  void fn_2() {
  }

  struct fn_ptr_array {
    typedef void (*fn_ptr_type)();
    static constexpr fn_ptr_type elems[] = {fn_1, fn_2};
  };

  constexpr void(*fn_array[])() = {fn_1, fn_2};
  constexpr void(*fn_array_reversed[])() = {fn_2, fn_1};

  typedef tice::v1::Value<decltype(&fn_1), fn_1> fn_1_ptr_1;

  typedef tice::v1::Value<decltype(&fn_2), fn_2> fn_2_ptr_1;

  struct fn_1_ptr_2 {
    typedef decltype(&fn_1) value_type;
    static constexpr value_type value {fn_1};
  };

}
