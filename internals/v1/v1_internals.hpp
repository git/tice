/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <cstddef>

#include "v1_internals_debug.hpp"

//\begin{template pattern support}
class I;
//\end{template pattern support}

//\begin{forward declarations}
namespace tice {
  namespace v1 {
    namespace internals {
      namespace utility {

        template<typename /* arg__nondeducible_type__ */ I>
        struct always_false;

      }
    }
    namespace code {

      template<typename arg__type, arg__type arg__value>
      struct lvalue;

      namespace empty {

        class setup;

      }
      namespace gedf {

        // The parameter `arg__Core_ids__core_ids' must not be empty.
        // The parameter `arg__tuple_construct__of__dict_Entry__of__Node__node_dict' must not be empty.  The elements of the tuple must be number
        // consecutively from left-to-right starting from zero.  Every node in the tuple must have already been validated.
        // The parameters `arg__tuple_construct__of__Channel__channel_list' and
        // `arg__tuple_construct__of__pair_construct__of__tuple_dict_Entry_array_idx__and__Array_idx_list__channel_idx_list' must be valid in
        // themselves and with respect to the nodes in `arg__tuple_construct__of__dict_Entry__of__Node__node_dict'.
        template<typename arg__Core_ids__core_ids,
                 typename arg__tuple_construct__of__dict_Entry__of__Node__node_dict,
                 typename arg__tuple_construct__of__Channel__channel_list,
                 typename arg__tuple_construct__of__pair_construct__of__tuple_dict_Entry_array_idx__and__Array_idx_list__channel_idx_list>
        class setup;

      }
    }
  }
}
//\end{forward declarations}

#include "v1_internals_tuple.hpp"

#include "v1_internals_utility.hpp"

#include "v1_internals_pair.hpp"
#include "v1_internals_dict.hpp"
#include "v1_internals_array.hpp"
#include "v1_internals_core_ids.hpp"

#include "v1_internals_hw.hpp"
#include "v1_internals_comp.hpp"
#include "v1_internals_node.hpp"
#include "v1_internals_chan_inlit.hpp"
#include "v1_internals_chan.hpp"
#include "v1_internals_feeder.hpp"
#include "v1_internals_ete_delay.hpp"
#include "v1_internals_correlation.hpp"
#include "v1_internals_program.hpp"

#include "v1_internals_code.hpp"

#include "v1_internals_utility_cleanup.hpp"
