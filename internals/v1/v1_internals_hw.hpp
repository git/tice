/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace error {
        namespace hw {
          namespace construct {

            template<bool arg__is_Core_ids, typename arg__actual_parameter>
            struct arg_2_is_Core_ids {
              static_assert(arg__is_Core_ids,
                            "[DEBUG] Processor core IDs are not specified using tice::v1::Core_ids");
            };

            template<bool arg__are_not_duplicated, v1::error::Pos arg__core_id_1_pos, v1::error::Pos arg__core_id_2_pos, int arg__core_id>
            struct arg_2_core_ids_are_not_duplicated {
              static_assert(arg__are_not_duplicated,
                            "[DEBUG] Processor core IDs are not pairwise distinct");
            };

          }
        }
      }
      namespace hw {

        template<typename I, typename arg__Array__array>
        using array_elements_are_pairwise_distinct = array::elements_are_pairwise_distinct<I, arg__Array__array>;

      }
      namespace hw {

#include "v1_internals_begin.hpp"

#define base_1A(I, _1) I                        \
        opts  (_1, arg__Core_ids__core_ids, ())
#define decl_1A(I, _1)                          \
        base_1A(I##I,                           \
                type(_1, typename))
#define args_1A(I, _1) base_1A(I, _1)
#define prms_1A(...) decl_1A(__VA_ARGS__)

#define base_1B(_d1, I, _1) I                           \
        opts  (_1, arg__distinctness_checker_msg, _d1)
#define decl_1B(_d1, I, _1)                                                                                                                     \
        base_1B(_d1, I##I,                                                                                                                      \
                type(_1, template<H(bool arg__are_not_duplicated_, v1::error::Pos arg__core_id_1_pos_, v1::error::Pos arg__core_id_2_pos_,      \
                                    int arg__core_id_)> class))
#define args_1B(I, _1) base_1B((), I, _1)
#define prms_1B(...) decl_1B((), __VA_ARGS__)

#define base_1C(I, _1) I                        \
        opts  (_1, arg__utility_chain__array_elements_are_pairwise_distinct, ())
#define decl_1C(I, _1)                          \
        base_1C(I##I,                           \
                type(_1, typename))
#define args_1C(I, _1) base_1C(I, _1)
#define prms_1C(...) decl_1C(__VA_ARGS__)

#define base_1D(I, _1) I                        \
        opts  (_1, arg__Array__core_ids, ())
#define decl_1D(I, _1)                          \
        base_1D(I##I,                           \
                type(_1, typename))
#define args_1D(I, _1) base_1D(I, _1)
#define prms_1D(...) decl_1D(__VA_ARGS__)

        template<prms_1D(I, _)>
        struct construct__parse_arg_1__result;

#define base_2B(_d1, I, args_1B, _1) I                  \
        opts  (_1, arg__core_ids_validator_msg, _d1)    \
          args_1B
#define decl_2B(_1B_d1, _d1, I, _1B_1, _1)                                                              \
        base_2B(_d1, I##I,                                                                              \
                decl_1B(_1B_d1, , _1B_1),                                                               \
                type(_1, template<bool arg__is_Core_ids_, typename arg__actual_parameter_> class))
#define args_2B(I, _1B_1, _1) base_2B((), I, args_1B(, _1B_1), _1)
#define prms_2B(...) decl_2B((), (), __VA_ARGS__)

#define base_2A(args_1A, args_2B)               \
        args_1A                                 \
        args_2B
#define decl_2A(_1B_d1, _2B_d1, I, _1A_1, _1B_1, _2B_1)         \
        base_2A(prms_1A(I, _1A_1),                              \
                decl_2B(_1B_d1, _2B_d1, , _1B_1, _2B_1))
#define args_2A(I, _1A_1, _1B_1, _2B_1) base_2A(args_1A(I, _1A_1), args_2B(, _1B_1, _2B_1))
#define prms_2A(...) decl_2A((), (), __VA_ARGS__)

        template<prms_2A(I, _, _, _)>
        struct construct__parse_arg_1;

#define base_3A(args_2A)                        \
        args_2A
#define decl_3A(_1B_d1, _2B_d1, I, _1A_1, _1B_1, _2B_1)                 \
        base_3A(decl_2A(_1B_d1, _2B_d1, I, _1A_1, _1B_1, _2B_1))
#define args_3A(I, _1A_1, _1B_1, _2B_1) base_3A(args_2A(I, _1A_1, _1B_1, _2B_1))
#define prms_3A(...) decl_3A((), (), __VA_ARGS__)

        template<decl_3A(_1B_d1(error::hw::construct::arg_2_is_Core_ids),
                         _2B_d1(error::hw::construct::arg_2_core_ids_are_not_duplicated),
                         I, _, _, _)>
        struct construct
        /* {
         *   with template<int... args__core_id>
         *        Core_ids<args__core_id...> = arg__Core_ids__core_ids
         *   {
         *     static constexpr bool backend_is_enabled {sizeof...(args__core_id) > 0};
         *     if (backend_is_enabled) {
         *       typedef array::construct<I, false, int, tuple::construct<I, std::integral_constant<int, args__core_id>...>> core_ids;
         *     } else {
         *       typedef array::construct<I, true, int> core_ids;
         *     }
         *   }
         *   invariant {
         *     array::elements_are_pairwise_distinct<I, self::core_ids>::value == true;
         *   }
         * }
         */;

#define base_2C(args_1C, args_1B)               \
        args_1C                                 \
        args_1B
#define decl_2C(I, _1C_1, _1B_1)                \
        base_2C(prms_1C(I, _1C_1),              \
                prms_1B(, _1B_1))
#define args_2C(I, _1C_1, _1B_1) base_2C(args_1C(I, _1C_1), args_1B(, _1B_1))
#define prms_2C(...) decl_2C(__VA_ARGS__)

        template<prms_2C(I, _, _)>
        struct construct__parse_arg_1__check_distinctness;

        

        template<prms_3A(I, _, _, _)>
        struct construct :
          construct__parse_arg_1<args_2A(I, _, _, _)> {
        };

        

        template<prms_2A(I, _, _, _)>
        struct construct__parse_arg_1 :
          arg__core_ids_validator_msg<!!utility::always_false<I>(), arg__Core_ids__core_ids> {
        };

        template<prms_2A(I, X, _, _)>
        struct construct__parse_arg_1<args_2A(I, R(v1::Core_ids<>), _, _)> :
          construct__parse_arg_1__result<args_1D(I, R(array::construct<I, true, int>))> {
        };

        template<prms_2A(I, R(int arg__core_id, int... args__core_id), _, _)>
        struct construct__parse_arg_1<args_2A(I, R(v1::Core_ids<arg__core_id, args__core_id...>), _, _)> :
          construct__parse_arg_1__check_distinctness<
          H(args_2C(I,
                    R(utility::chain<H(I, array_elements_are_pairwise_distinct,
                                       array::construct<H(I, false, int,
                                                          tuple::construct<H(I, std::integral_constant<int, arg__core_id>,
                                                                             std::integral_constant<int, args__core_id>...)>)>)>),
                    _))> {
        };

        

        template<prms_2C(I, _, _)>
        struct construct__parse_arg_1__check_distinctness :
#define result arg__utility_chain__array_elements_are_pairwise_distinct::value
          arg__distinctness_checker_msg<result::value, result::lhs_idx + 1, result::rhs_idx + 1,
                                        arg__utility_chain__array_elements_are_pairwise_distinct::arg::elems[(result::lhs_idx
                                                                                                              == v1::array::Invalid_idx::value)
                                                                                                             ? 0
                                                                                                             : result::lhs_idx]>,
          construct__parse_arg_1__result<args_1D(I, R(typename arg__utility_chain__array_elements_are_pairwise_distinct::arg))> {
        };
#undef result

        

        template<prms_1D(I, _)>
        struct construct__parse_arg_1__result {
          static constexpr bool backend_is_enabled {!array::is_empty<I, arg__Array__core_ids>::value};
          typedef arg__Array__core_ids core_ids;
        };

#include "v1_internals_end.hpp"
      }
    }
  }
}

namespace tice {
  namespace v1 {

    template<typename arg__Core_ids__core_ids>
    class HW :
      public internals::hw::construct<I, arg__Core_ids__core_ids,
                                      error::hw::arg_1_is_Core_ids,
                                      error::hw::arg_1_core_ids_are_not_duplicated> {
    };

  }
}
