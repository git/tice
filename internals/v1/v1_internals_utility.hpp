/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

// The presence of an opening parenthesis following `H' will force Emacs to
// indent a comma-separated list of `H' actual parameters that are distributed
// over multiple lines in a single column whose left-most vertical boundary is
// located at the opening parenthesis.  The use of this macro shall have no
// side-effect unless some of the actual parameters use one or more macros, in
// which case the use of this macro will force those macros to be replaced
// earlier than they should.
#define H(...)                                  \
  __VA_ARGS__

#define run_metaprogram(...)                                            \
  static_assert(sizeof(__VA_ARGS__) >= 0, "C++ metaprogram is run")

namespace tice {
  namespace v1 {
    namespace internals {
      namespace error {
        namespace utility {
          namespace nontype_nontemplate_args_eq {

            template<bool arg__is_correct,
                     v1::array::Idx arg__lhs_idx_1, v1::array::Idx arg__rhs_idx_1,
                     v1::array::Idx arg__lhs_idx_2, v1::array::Idx arg__rhs_idx_2>
            struct usage_is_correct {
              static_assert(arg__is_correct,
                            "[DEBUG] Either `arg__lhs_idx_1' is invalid but `arg__lhs_idx_2' is not"
                            " or `arg__rhs_idx_1' is invalid but `arg__rhs_idx_2' is not");
            };

          }
          namespace arrays_are_pair_equal {

            template<bool arg__is_array_type, typename arg__actual_parameter>
            struct arg_2_is_array_type {
              static_assert(arg__is_array_type,
                            "[DEBUG] Arg 2 cannot decay to a non-function pointer type"
                            " (e.g., both `T[]' and `T[n]' decay to `T *' for any valid type `T' and size `n')");
            };

            template<bool arg__is_array_type, typename arg__actual_parameter>
            struct arg_3_is_array_type {
              static_assert(arg__is_array_type,
                            "[DEBUG] Arg 3 cannot decay to a non-function pointer type"
                            " (e.g., both `T[]' and `T[n]' decay to `T *' for any valid type `T' and size `n')");
            };

            template<bool arg__is_valid_idx, v1::array::Idx arg__idx>
            struct arg_6_is_valid_idx {
              static_assert(arg__is_valid_idx,
                            "[DEBUG] Arg 6 is an invalid array index");
            };

            template<bool arg__is_valid_idx, v1::array::Idx arg__idx>
            struct arg_7_is_valid_idx {
              static_assert(arg__is_valid_idx,
                            "[DEBUG] Arg 7 is an invalid array index");
            };

          }
          namespace arrays_are_point_equal {

            template<bool arg__is_array_type, typename arg__actual_parameter>
            struct arg_2_is_array_type {
              static_assert(arg__is_array_type,
                            "[DEBUG] Arg 2 cannot decay to a non-function pointer type"
                            " (e.g., both `T[]' and `T[n]' decay to `T *' for any valid type `T' and size `n')");
            };

            template<bool arg__is_array_type, typename arg__actual_parameter>
            struct arg_3_is_array_type {
              static_assert(arg__is_array_type,
                            "[DEBUG] Arg 3 cannot decay to a non-function pointer type"
                            " (e.g., both `T[]' and `T[n]' decay to `T *' for any valid type `T' and size `n')");
            };

            template<bool arg__is_valid_idx, v1::array::Idx arg__idx>
            struct arg_6_is_valid_idx {
              static_assert(arg__is_valid_idx,
                            "[DEBUG] Arg 6 is an invalid array index");
            };

          }
          namespace fn_info {

            template<bool arg__is_std_integral_constant__of__function_pointer, typename arg__actual_parameter>
            struct arg_2_is_std_integral_constant__of__function_pointer {
              static_assert(arg__is_std_integral_constant__of__function_pointer,
                            "[DEBUG] Arg 2 is not class template std::integral_constant instantiated with a function pointer");
            };

          }
          namespace is_callable {

            template<bool arg__is_function_pointer, typename arg__actual_parameter>
            struct arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Arg 2 is not a function pointer");
            };

            template<bool arg__is_tuple_construct, typename arg__actual_parameter>
            struct arg_3_is_tuple_construct {
              static_assert(arg__is_tuple_construct,
                            "[DEBUG] Arg 3 is not class template tice::v1::internals::tuple::construct");
            };

          }
          namespace check_pairwise_distinctness_of_nodes {

            template<bool arg__are_pairwise_distinct, v1::error::Pos arg__head_pos, v1::error::Pos arg__tail_pos>
            struct nodes_are_pairwise_distinct {
              static_assert(arg__are_pairwise_distinct,
                            "[DEBUG] Nodes are not pairwise distinct");
            };

            template<bool arg__is_Node, v1::error::Pos arg__head_pos, typename arg__actual_parameter>
            struct head_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Head is not class template tice::v1::Node");
            };

            template<bool arg__is_Node, v1::error::Pos arg__tail_pos, typename arg__actual_parameter>
            struct tail_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Tail is not class template tice::v1::Node");
            };

          }
          namespace ratio {
            namespace validate {

              template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
              struct arg_2_has_nonzero_denominator {
                static_assert(arg__has_nonzero_denominator,
                              "[DEBUG] Arg 2 is not std::ratio instance with nonzero denominator");
              };

              template<bool arg__is_std_ratio, typename arg__actual_parameter>
              struct arg_2_is_std_ratio {
                static_assert(arg__is_std_ratio,
                              "[DEBUG] Arg 2 is not class template std::ratio");
              };

            }
            namespace validate_positive {

              template<bool arg__is_positive, typename arg__actual_parameter>
              struct arg_2_is_positive {
                static_assert(arg__is_positive,
                              "[DEBUG] Arg 2 is not positive std::ratio instance");
              };

              template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
              struct arg_2_has_nonzero_denominator {
                static_assert(arg__has_nonzero_denominator,
                              "[DEBUG] Arg 2 is not std::ratio instance with nonzero denominator");
              };

              template<bool arg__is_std_ratio, typename arg__actual_parameter>
              struct arg_2_is_std_ratio {
                static_assert(arg__is_std_ratio,
                              "[DEBUG] Arg 2 is not class template std::ratio");
              };

            }
            namespace validate_nonnegative {

              template<bool arg__is_nonnegative, typename arg__actual_parameter>
              struct arg_2_is_nonnegative {
                static_assert(arg__is_nonnegative,
                              "[DEBUG] Arg 2 is not nonnegative std::ratio instance");
              };

              template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
              struct arg_2_has_nonzero_denominator {
                static_assert(arg__has_nonzero_denominator,
                              "[DEBUG] Arg 2 is not std::ratio instance with nonzero denominator");
              };

              template<bool arg__is_std_ratio, typename arg__actual_parameter>
              struct arg_2_is_std_ratio {
                static_assert(arg__is_std_ratio,
                              "[DEBUG] Arg 2 is not class template std::ratio");
              };

            }
          }
          namespace select {
            namespace between {

              template<bool arg__has_value_as_constexpr_copyable_data_member, typename arg__actual_parameter>
              struct arg_2_has_value_as_constexpr_copyable_data_member {
                static_assert(arg__has_value_as_constexpr_copyable_data_member,
                              "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `value'");
              };

              template<bool arg__has_value_as_constexpr_copyable_data_member, typename arg__actual_parameter>
              struct arg_3_has_value_as_constexpr_copyable_data_member {
                static_assert(arg__has_value_as_constexpr_copyable_data_member,
                              "[DEBUG] Arg 3 has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `value'");
              };

              template<bool arg__is_valid_selection_operator, typename arg__actual_parameter, typename arg__available_operators>
              struct arg_4_is_valid_selection_operator {
                static_assert(arg__is_valid_selection_operator,
                              "[DEBUG] Arg 4 is not a valid selection operator");
              };

              template<bool arg__is_defined, typename arg__operator, typename arg__lhs_value_type, typename arg__rhs_value_type>
              struct arg_4_is_defined {
                static_assert(arg__is_defined,
                              "[DEBUG] Arg 4 is not defined for the types of the data members `value' of arg 2 and 3");
              };

            }
          }
          namespace min {

            template<bool arg__has_value_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_value_as_constexpr_copyable_data_member {
              static_assert(arg__has_value_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `value'");
            };

            template<bool arg__has_value_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_3_has_value_as_constexpr_copyable_data_member {
              static_assert(arg__has_value_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 3 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `value'");
            };

            template<bool arg__is_defined, typename arg__lhs_value_type, typename arg__rhs_value_type>
            struct operator_less_than_is_defined {
              static_assert(arg__is_defined,
                            "[DEBUG] Operator `<' is not defined for the given types");
            };

          }
          namespace max {

            template<bool arg__has_value_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_value_as_constexpr_copyable_data_member {
              static_assert(arg__has_value_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `value'");
            };

            template<bool arg__has_value_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_3_has_value_as_constexpr_copyable_data_member {
              static_assert(arg__has_value_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 3 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `value'");
            };

            template<bool arg__is_defined, typename arg__lhs_value_type, typename arg__rhs_value_type>
            struct operator_less_than_is_defined {
              static_assert(arg__is_defined,
                            "[DEBUG] Operator `<' is not defined for the given types");
            };

          }
          namespace fp {
            namespace Double {

              template<bool arg__is_valid, typename arg__actual_parameter>
              struct arg_2_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Arg 2 is not class template std::ratio");
              };

            }
            namespace Ratio {

              template<bool arg__is_not_nullptr>
              struct arg_2_is_not_nullptr {
                static_assert(arg__is_not_nullptr,
                              "[DEBUG] Arg 2 is not a pointer to a value of type `double'");
              };

              template<bool arg__is_valid, typename arg__actual_parameter>
              struct arg_3_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Arg 3 is not class template std::ratio");
              };

              template<bool arg__is_not_nullptr>
              struct arg_4_is_not_nullptr {
                static_assert(arg__is_not_nullptr,
                              "[DEBUG] Arg 4 is not a pointer to a rounding function of type `std::intmax_t(double)'");
              };

            }
            namespace round {

              template<bool arg__is_not_positive_infinity>
              struct arg__operand_is_not_positive_infinity {
                static_assert(arg__is_not_positive_infinity,
                              "[DEBUG] Round operation is undefined on positive infinity");
              };

            }
            namespace ceil {

              template<bool arg__is_not_positive_infinity>
              struct arg__operand_is_not_positive_infinity {
                static_assert(arg__is_not_positive_infinity,
                              "[DEBUG] Ceil operation is undefined on positive infinity");
              };

            }
            namespace floor {

              template<bool arg__is_not_positive_infinity>
              struct arg__operand_is_not_positive_infinity {
                static_assert(arg__is_not_positive_infinity,
                              "[DEBUG] Floor operation is undefined on positive infinity");
              };

            }
          }
          namespace list {
            namespace make {

              template<bool arg__is_nonnegative, v1::array::Size arg__actual_parameter>
              struct arg_2_is_nonnegative {
                static_assert(arg__is_nonnegative,
                              "[DEBUG] Arg 2 is not nonnegative");
              };

              template<bool arg__has_value_as_member_typedef, v1::array::Idx arg__idx, typename arg__element_produced>
              struct element_produced_has_value_as_member_typedef {
                static_assert(arg__has_value_as_member_typedef,
                              "[DEBUG] Element produced at this index has no member typedef `value'");
              };

            }
            namespace update {

              template<bool arg__is_tuple_construct, typename arg__actual_parameter>
              struct arg_2_is_tuple_construct {
                static_assert(arg__is_tuple_construct,
                              "[DEBUG] Arg 2 is not class template tice::v1::internals::tuple::construct");
              };

              template<bool arg__has_data_as_member_typedef,
                       v1::array::Idx arg__idx, typename arg__prev_list_element, typename arg__updater_data, typename arg__element_update>
              struct element_update_has_data_as_member_typedef {
                static_assert(arg__has_data_as_member_typedef,
                              "[DEBUG] Update to previous list element at this index given updater data has no member typedef `data'");
              };

              template<bool arg__has_value_as_member_typedef,
                       v1::array::Idx arg__idx, typename arg__prev_list_element, typename arg__updater_data, typename arg__element_update>
              struct element_update_has_value_as_member_typedef {
                static_assert(arg__has_value_as_member_typedef,
                              "[DEBUG] Update to previous list element at this index given updater data has no member typedef `value'");
              };

              template<bool arg__has_complete_flag_as_constexpr_copyable_data_member,
                       v1::array::Idx arg__idx, typename arg__prev_list_element, typename arg__updater_data, typename arg__element_update>
              struct element_update_has_complete_flag_as_constexpr_copyable_data_member {
                static_assert(arg__has_complete_flag_as_constexpr_copyable_data_member,
                              "[DEBUG] Update to previous list element at this index given updater data has"
                              " no constexpr (not static or not constexpr) copyable (copy constructor maybe deleted or not constexpr) data member"
                              " `is_complete' of type `bool'");
              };

            }
            namespace append {

              template<bool arg__is_tuple_construct, typename arg__actual_parameter>
              struct arg_2_is_tuple_construct {
                static_assert(arg__is_tuple_construct,
                              "[DEBUG] Arg 2 is not class template tice::v1::internals::tuple::construct");
              };

            }
            namespace iterate {

              template<bool arg__is_tuple_construct, typename arg__actual_parameter>
              struct arg_2_is_tuple_construct {
                static_assert(arg__is_tuple_construct,
                              "[DEBUG] Arg 2 is not class template tice::v1::internals::tuple::construct");
              };

            }
          }
        }
      }
      namespace utility {

#include "v1_internals_begin.hpp"

#define base_1(_d2, I, _1, _2) I                \
        opts  (_1, arg, ())                     \
          opts(_2, arg__value, _d2)
#define decl_1(_d2, I, _1, _2)                  \
        base_1(_d2, I##I,                       \
               type(_1, typename),              \
               type(_2, typename))
#define args_1(I, _1, _2) base_1((), I, _1, _2)
#define prms_1(...) decl_1((), __VA_ARGS__)

        template<decl_1(_d2(I),
                        I, _, _)>
        struct run_metaprogram
        /* {
         *   if (std::is_same<arg__value, I>::value == false) {
         *     typedef arg__value value;
         *   }
         * }
         */;

        

        template<prms_1(I, _, X)>
        struct run_metaprogram<args_1(I, _, R(I))> {
          run_metaprogram(arg);
        };

        template<prms_1(I, _, _)>
        struct run_metaprogram {
          run_metaprogram(arg);
          typedef arg__value value;
        };

#include "v1_internals_end.hpp"
      }
      namespace utility {

        template<typename /* arg__nondeducible_type__ */ I>
        struct always_false :
          std::false_type {
        };

      }
      namespace utility {

        template<typename /* arg__nondeducible_type__ */ I>
        struct always_true :
          std::true_type {
        };

      }
      namespace utility {

        template<typename I, typename arg__being_or_deriving__std_integral_constant__of__bool__proposition>
        using negation = std::integral_constant<bool, !arg__being_or_deriving__std_integral_constant__of__bool__proposition::value>;

      }
      namespace utility {

        struct Y {
        };

      }
      namespace utility {

        struct N {
        };

      }
      namespace utility {

#include "v1_internals_begin.hpp"

#define base_1(_d5, _d6, _d7, _d8, I, _1, _2, _3, _4, _5, _6, _7, _8) I \
        opts  (_1, arg__lhs_type, ())                                   \
          opts(_2, arg__rhs_type, ())                                   \
          opts(_3, arg__lhs, ())                                        \
          opts(_4, arg__rhs, ())                                        \
          opts(_5, arg__lhs_idx_1, _d5)                                 \
          opts(_6, arg__rhs_idx_1, _d6)                                 \
          opts(_7, arg__lhs_idx_2, _d7)                                 \
          opts(_8, arg__rhs_idx_2, _d8)
#define decl_1(_d5, _d6, _d7, _d8, I, _1, _2, _3, _4, _5, _6, _7, _8)   \
        base_1(_d5, _d6, _d7, _d8, I##I,                                \
               type(_1, typename),                                      \
               type(_2, typename),                                      \
               type(_3, arg__lhs_type),                                 \
               type(_4, arg__rhs_type),                                 \
               type(_5, v1::array::Idx),                                \
               type(_6, v1::array::Idx),                                \
               type(_7, v1::array::Idx),                                \
               type(_8, v1::array::Idx))
#define args_1(I, _1, _2, _3, _4, _5, _6, _7, _8) base_1((), (), (), (), I, _1, _2, _3, _4, _5, _6, _7, _8)
#define prms_1(...) decl_1((), (), (), (), __VA_ARGS__)

#define base_2(_d1, args_1, _1)                         \
        args_1                                          \
        opts  (_1, arg__usage_validator_msg, _d1)
#define decl_2(_1_d5, _1_d6, _1_d7, _1_d8, _d1, I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1)          \
        base_2(_d1,                                                                                             \
               decl_1(_1_d5, _1_d6, _1_d7, _1_d8, I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8),           \
               type(_1, template<H(bool arg__is_correct_,                                                       \
                                   v1::array::Idx arg__lhs_idx_1_, v1::array::Idx arg__rhs_idx_1_,              \
                                   v1::array::Idx arg__lhs_idx_2_, v1::array::Idx arg__rhs_idx_2_)> class))
#define args_2(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1) base_2((), args_1(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8), _1)
#define prms_2(...) decl_2((), (), (), (), (), __VA_ARGS__)

        template<decl_2(_1_d5(v1::array::Invalid_idx::value),
                        _1_d6(v1::array::Invalid_idx::value),
                        _1_d7(v1::array::Invalid_idx::value),
                        _1_d8(v1::array::Invalid_idx::value),
                        _d1  (error::utility::nontype_nontemplate_args_eq::usage_is_correct),
                        I, _, _, _, _, _, _, _, _, _)>
        struct nontype_nontemplate_args_eq
        /* {
         *   include std::integral_constant<bool,
         *                                  (arg__lhs_idx_1 == v1::array::Invalid_idx::value && arg__rhs_idx_1 == v1::array::Invalid_idx::value
         *                                   ? arg__lhs == arg__rhs                                                                          // #1
         *                                   : arg__lhs_idx_1 == v1::array::Invalid_idx::value
         *                                   ? (arg__rhs_idx_2 == v1::array::Invalid_idx::value
         *                                      ? arg__lhs == arg__rhs[arg__rhs_idx_1]                                                       // #2
         *                                      : arg__lhs == arg__rhs[arg__rhs_idx_1][arg__rhs_idx_2])                                      // #3
         *                                   : arg__rhs_idx_1 == v1::array::Invalid_idx::value
         *                                   ? (arg__lhs_idx_2 == v1::array::Invalid_idx::value
         *                                      ? arg__lhs[arg__lhs_idx_1] == arg__rhs                                                       // #4
         *                                      : arg__lhs[arg__lhs_idx_1][arg__lhs_idx_2] == arg__rhs)                                      // #5
         *                                   : (arg__lhs_idx_2 == v1::array::Invalid_idx::value && arg__rhs_idx_2 == v1::array::Invalid_idx::value
         *                                      ? arg__lhs[arg__lhs_idx_1] == arg__rhs[arg__rhs_idx_1]                                       // #6
         *                                      : arg__lhs_idx_2 == v1::array::Invalid_idx::value
         *                                      ? arg__lhs[arg__lhs_idx_1] == arg__rhs[arg__rhs_idx_1][arg__rhs_idx_2]                       // #7
         *                                      : arg__rhs_idx_2 == v1::array::Invalid_idx::value
         *                                      ? arg__lhs[arg__lhs_idx_1][arg__lhs_idx_2] == arg__rhs[arg__rhs_idx_1]                       // #8
         *                                      : arg__lhs[arg__lhs_idx_1][arg__lhs_idx_2] == arg__rhs[arg__rhs_idx_1][arg__rhs_idx_2]))>;   // #9
         * }
         */;

#define base_3(_d1, _d2, _d3, _d4, args_2, _1, _2, _3, _4)      \
        args_2                                                  \
        opts  (_1, arg__lhs_idx_1_is_valid, _d1)                \
          opts(_2, arg__rhs_idx_1_is_valid, _d2)                \
          opts(_3, arg__lhs_idx_2_is_valid, _d3)                \
          opts(_4, arg__rhs_idx_2_is_valid, _d4)
#define decl_3(_d1, _d2, _d3, _d4, I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _2_1, _1, _2, _3, _4)     \
        base_3(_d1, _d2, _d3, _d4,                                                                              \
               prms_2(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _2_1),                                 \
               type(_1, bool),                                                                                  \
               type(_2, bool),                                                                                  \
               type(_3, bool),                                                                                  \
               type(_4, bool))
#define args_3(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _2_1, _1, _2, _3, _4)                         \
        base_3((), (), (), (), args_2(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _2_1), _1, _2, _3, _4)
#define prms_3(...) decl_3((), (), (), (), __VA_ARGS__)

        template<decl_3(_d1(arg__lhs_idx_1 != v1::array::Invalid_idx::value),
                        _d2(arg__rhs_idx_1 != v1::array::Invalid_idx::value),
                        _d3(arg__lhs_idx_2 != v1::array::Invalid_idx::value),
                        _d4(arg__rhs_idx_2 != v1::array::Invalid_idx::value),
                        I, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct nontype_nontemplate_args_eq__run;

        

        template<prms_2(I, _, _, _, _, _, _, _, _, _)>
        struct nontype_nontemplate_args_eq :
          nontype_nontemplate_args_eq__run<args_3(I, _, _, _, _, _, _, _, _, _, X, X, X, X)> {
        };

#define prms_1_1(I, _5, _6, _7, _8) prms_1(I, _, _, _, _, _5, _6, _7, _8)
#define args_1_1(I, _5, _6, _7, _8) args_1(I, _, _, _, _, _5, _6, _7, _8)

#define I0 X
#define I1 _

#define generate_idx_00(ident)
#define generate_idx_10(ident) [arg__##ident##_idx_1]
#define generate_idx_11(ident) [arg__##ident##_idx_1][arg__##ident##_idx_2]

#define generate_test(_5, _6, _7, _8)                                                                                                           \
                                                                                                                                                \
        template<prms_1_1(I, I##_5, I##_6, I##_7, I##_8)>                                                                                       \
        constexpr Y                                                                                                                             \
        nontype_nontemplate_args_eq__test_##_5##_6##_7##_8(I(*)[arg__lhs generate_idx_##_5##_7(lhs) == arg__rhs generate_idx_##_6##_8(rhs)      \
                                                                /* As reported in https://gcc.gnu.org/bugzilla/show_bug.cgi?id=89074, if        \
                                                                 * `arg__lhs' and `arg__rhs' are equal, GCC accepts this comparison as a        \
                                                                 * valid constexpr and evaluates the constexpr to `true'.  However, when they   \
                                                                 * are not equal and happen to be pointers, GCC rejects this comparison as an   \
                                                                 * invalid constexpr and raises a compile-time error with the diagnostic that   \
                                                                 * the comparison cannot be used in a context requiring a constexpr.  Hence,    \
                                                                 * putting the comparison in this SFINAE context results in `Y' when two        \
                                                                 * pointers are deemed equal by GCC and `N' otherwise.  Clang, however, does    \
                                                                 * not reject the comparison as an invalid constexpr when `arg__lhs' and        \
                                                                 * `arg__rhs' are inequal pointers.  This means that in this SFINAE context,    \
                                                                 * the result would always be `Y' regardless of whether `arg__lhs' and          \
                                                                 * `arg__rhs' are equal pointers because Clang encounters no failure in         \
                                                                 * performing the comparison.  Hence, to accommodate Clang w.r.t. a pointer     \
                                                                 * comparison, an eventual GCC bug fix, and the comparisons of other types      \
                                                                 * for which GCC does not reject (e.g., comparing two `int's), the result of    \
                                                                 * this comparison must be used to specify an array size so that when the       \
                                                                 * comparison returns `false', by [conv.prom]p{6} of ISO-IEC 14882:2014         \
                                                                 * (C++14 standard) the result is zero, and by [dcl.array]p{1} of the           \
                                                                 * standard, which requires an array size to be positive, a failure occurs,     \
                                                                 * giving `N' instead of `Y' as desired.                                        \
                                                                 */                                                                       ]);   \
        template<prms_1_1(I, I##_5, I##_6, I##_7, I##_8)>                                                                                       \
        constexpr N nontype_nontemplate_args_eq__test_##_5##_6##_7##_8(...)

#define prms_3_1(I, _1, _2, _3, _4) prms_3(I, _, _, _, _, _, _, _, _, _, _1, _2, _3, _4)
#define args_3_1(I, _1, _2, _3, _4) args_3(I, _, _, _, _, _, _, _, _, _, _1, _2, _3, _4)

#define II0 R(false)
#define II1 R(true)

#define III0(ident, level) v1::array::Invalid_idx::value
#define III1(ident, level) arg__##ident##_idx_##level

#define generate_tmpl(_1, _2, _3, _4)                                                                                                              \
                                                                                                                                                   \
        template<prms_3_1(I, X, X, X, X)>                                                                                                          \
        struct nontype_nontemplate_args_eq__run<args_3_1(I, II##_1, II##_2, II##_3, II##_4)> :                                                     \
          std::integral_constant<H(bool,                                                                                                           \
                                   std::is_same<H(decltype(nontype_nontemplate_args_eq__test_##_1##_2##_3##_4<args_1_1(I, I##_1, I##_2,            \
                                                                                                                       I##_3, I##_4)>(nullptr)),   \
                                                  Y)>::value)> {                                                                                   \
        }

#define generate_both(lhs_idx_1_is_valid, rhs_idx_1_is_valid, lhs_idx_2_is_valid, rhs_idx_2_is_valid)           \
        generate_test(lhs_idx_1_is_valid, rhs_idx_1_is_valid, lhs_idx_2_is_valid, rhs_idx_2_is_valid);          \
        generate_tmpl(lhs_idx_1_is_valid, rhs_idx_1_is_valid, lhs_idx_2_is_valid, rhs_idx_2_is_valid)

        generate_both(0, 0, 0, 0); // #1
        generate_both(0, 1, 0, 0); // #2
        generate_both(0, 1, 0, 1); // #3
        generate_both(1, 0, 0, 0); // #4
        generate_both(1, 0, 1, 0); // #5
        generate_both(1, 1, 0, 0); // #6
        generate_both(1, 1, 0, 1); // #7
        generate_both(1, 1, 1, 0); // #8
        generate_both(1, 1, 1, 1); // #9

        template<prms_3_1(I, _, _, _, _)>
        struct nontype_nontemplate_args_eq__run :
          arg__usage_validator_msg<!!always_false<I>(), arg__lhs_idx_1, arg__rhs_idx_1, arg__lhs_idx_2, arg__rhs_idx_2> {
        };

#undef I0
#undef I1
#undef generate_idx_00
#undef generate_idx_10
#undef generate_idx_11
#undef generate_test
#undef II0
#undef II1
#undef III0
#undef III1
#undef generate_tmpl
#undef generate_both

#include "v1_internals_end.hpp"
      }
      namespace utility {

        template<typename I, typename arg__lhs_lvalue_type, typename arg__rhs_lvalue_type,
                 arg__lhs_lvalue_type *arg__lhs_lvalue, arg__rhs_lvalue_type *arg__rhs_lvalue>
        using lvalues_are_equal = nontype_nontemplate_args_eq<I, arg__lhs_lvalue_type *, arg__rhs_lvalue_type *, arg__lhs_lvalue, arg__rhs_lvalue,
                                                              std::is_function<arg__lhs_lvalue_type>::value ? v1::array::Invalid_idx::value : 0,
                                                              std::is_function<arg__rhs_lvalue_type>::value ? v1::array::Invalid_idx::value : 0>;

      }
      namespace utility {

#include "v1_internals_begin.hpp"

#define base_1A(_d7, _d8, _d9, I, _1, _2, _3, _4, _5, _6, _7, _8, _9) I \
        opts  (_1, arg__lhs_lvalue_type, ())                            \
          opts(_2, arg__rhs_lvalue_type, ())                            \
          opts(_3, arg__lhs_idx, ())                                    \
          opts(_4, arg__lhs_type_validator_msg, ())                     \
          opts(_5, arg__rhs_type_validator_msg, ())                     \
          opts(_6, arg__lhs_idx_validator_msg, ())                      \
          opts(_7, arg__is_point_equal, _d7)                            \
        opts  (_8, arg__rhs_idx, _d8)                                   \
        opts  (_9, arg__rhs_idx_validator_msg, _d9)
#define decl_1A(_d7, _d8, _d9, I, _1, _2, _3, _4, _5, _6, _7, _8, _9)                           \
        base_1A(_d7, _d8, _d9, I##I,                                                            \
                type(_1, typename),                                                             \
                type(_2, typename),                                                             \
                type(_3, v1::array::Idx),                                                       \
                type(_4, template<bool arg__is_array_, typename arg__actual_parameter_> class), \
                type(_5, template<bool arg__is_array_, typename arg__actual_parameter_> class), \
                type(_6, template<bool arg__is_valid_idx_, array::Idx arg__idx_> class),        \
                type(_7, bool),                                                                 \
                type(_8, v1::array::Idx),                                                       \
                type(_9, template<bool arg__is_valid_idx_, array::Idx arg__idx_> class))
#define args_1A(I, _1, _2, _3, _4, _5, _6, _7, _8, _9) base_1A((), (), (), I, _1, _2, _3, _4, _5, _6, _7, _8, _9)
#define prms_1A(...) decl_1A((), (), (), __VA_ARGS__)

        template<decl_1A(_d7(true),
                         _d8(v1::array::Invalid_idx::value),
                         _d9(error::utility::arrays_are_pair_equal::arg_7_is_valid_idx),
                         I, _, _, _, _, _, _, _, _, _)>
        struct validate_arrays_are_equal_args;

#define base_1B(I, _1) I                        \
        opts  (_1, arg__lvalue_type, ())
#define decl_1B(I, _1)                          \
        base_1B(I##I,                           \
                type(_1, typename))
#define args_1B(I, _1) base_1B(I, _1)
#define prms_1B(...) decl_1B(__VA_ARGS__)

        template<prms_1B(I, _)>
        struct validate_arrays_are_equal_args__type_is_valid :
          std::integral_constant<bool, (std::is_array<arg__lvalue_type>::value
                                        || (std::is_pointer<arg__lvalue_type>::value
                                            && !std::is_function<std::remove_pointer_t<arg__lvalue_type>>::value))> {
        };

#define base_1C(I, _1) I                        \
        opts  (_1, arg__idx, ())
#define decl_1C(I, _1)                          \
        base_1C(I##I,                           \
                type(_1, v1::array::Idx))
#define args_1C(I, _1) base_1C(I, _1)
#define prms_1C(...) decl_1C(__VA_ARGS__)

        template<prms_1C(I, _)>
        struct validate_arrays_are_equal_args__idx_is_valid :
          std::integral_constant<bool, (arg__idx != v1::array::Invalid_idx::value)> {
        };

        

        template<prms_1A(I, _, _, _, _, _, _, _, _, _)>
        struct validate_arrays_are_equal_args :
          arg__lhs_type_validator_msg<validate_arrays_are_equal_args__type_is_valid<args_1B(I, R(arg__lhs_lvalue_type))>::value,
                                      arg__lhs_lvalue_type>,
          arg__rhs_type_validator_msg<validate_arrays_are_equal_args__type_is_valid<args_1B(I, R(arg__rhs_lvalue_type))>::value,
                                      arg__rhs_lvalue_type>,
          arg__lhs_idx_validator_msg<validate_arrays_are_equal_args__idx_is_valid<args_1C(I, R(arg__lhs_idx))>::value, arg__lhs_idx>,
          arg__rhs_idx_validator_msg<arg__is_point_equal
                                     || validate_arrays_are_equal_args__idx_is_valid<args_1C(I, R(arg__rhs_idx))>::value, arg__rhs_idx> {
        };

#include "v1_internals_end.hpp"
      }
      namespace utility {

        template<typename I, typename arg__lhs_array_type, typename arg__rhs_array_type,
                 arg__lhs_array_type *arg__lhs_array, arg__rhs_array_type *arg__rhs_array,
                 v1::array::Idx arg__lhs_idx, v1::array::Idx arg__rhs_idx>
        struct arrays_are_pair_equal :
          validate_arrays_are_equal_args<I, arg__lhs_array_type, arg__rhs_array_type, arg__lhs_idx,
                                         error::utility::arrays_are_pair_equal::arg_2_is_array_type,
                                         error::utility::arrays_are_pair_equal::arg_3_is_array_type,
                                         error::utility::arrays_are_pair_equal::arg_6_is_valid_idx,
                                         false, arg__rhs_idx>,
          nontype_nontemplate_args_eq<I, arg__lhs_array_type *, arg__rhs_array_type *, arg__lhs_array, arg__rhs_array,
                                      0, 0, arg__lhs_idx, arg__rhs_idx>
        {/*
          * include nontype_nontemplate_args_eq<I, arg__lhs_array_type *, arg__rhs_array_type *, arg__lhs_array, arg__rhs_array,
          *                                     0, 0, arg__lhs_idx, arg__rhs_idx>;
          */
        };

      }
      namespace utility {

        template<typename I, typename arg__lhs_array_type, typename arg__rhs_array_type,
                 arg__lhs_array_type *arg__lhs_array, arg__rhs_array_type *arg__rhs_array,
                 v1::array::Idx arg__idx>
        struct arrays_are_point_equal :
          validate_arrays_are_equal_args<I, arg__lhs_array_type, arg__rhs_array_type, arg__idx,
                                         error::utility::arrays_are_point_equal::arg_2_is_array_type,
                                         error::utility::arrays_are_point_equal::arg_3_is_array_type,
                                         error::utility::arrays_are_point_equal::arg_6_is_valid_idx>,
          nontype_nontemplate_args_eq<I, arg__lhs_array_type *, arg__rhs_array_type *, arg__lhs_array, arg__rhs_array,
                                      0, 0, arg__idx, arg__idx>
        {/*
          * include nontype_nontemplate_args_eq<I, arg__lhs_array_type *, arg__rhs_array_type *, arg__lhs_array, arg__rhs_array,
          *                                     0, 0, arg__idx, arg__idx>;
          */
        };

      }
      namespace utility {

#include "v1_internals_begin.hpp"

#define base_1(_d2, I, _1, _2) I                                        \
        opts  (_1, arg__std_integral_constant__function_ptr, ())        \
          opts(_2, arg__function_ptr_checker_msg, _d2)
#define decl_1(_d2, I, _1, _2)                                                                                                          \
        base_1(_d2, I##I,                                                                                                               \
               type(_1, typename),                                                                                                      \
               type(_2, template<bool arg__is_std_integral_constant__of__function_pointer_, typename arg__actual_parameter_> class))
#define args_1(I, _1, _2) base_1((), I, _1, _2)
#define prms_1(...) decl_1((), __VA_ARGS__)

        template<decl_1(_d2(error::utility::fn_info::arg_2_is_std_integral_constant__of__function_pointer),
                        I, _, _)>
        struct fn_info
        /* {
         *   with template<typename arg__return_type, typename... args__formal_param_type,
         *                 arg__return_type(*function_ptr)(args__formal_param_type...)>
         *        std::integral_constant<arg__return_type(*)(args__formal_param_type...), function_ptr> = arg__std_integral_constant__function_ptr
         *   {
         *     typedef arg__return_type return_type;
         *     typedef tuple::construct<I, args__formal_param_type...> param_types;
         *     typedef std::integral_constant<arg__return_type(*)(args__formal_param_type...), function_ptr> fn_ptr;
         *   }
         * }
         */;

        

        template<prms_1(I, _, _)>
        struct fn_info :
          arg__function_ptr_checker_msg<!!always_false<I>(), arg__std_integral_constant__function_ptr> {
        };

        template<prms_1(I, R(typename arg__return_type, typename... args__formal_param_type,
                             arg__return_type(*function_ptr)(args__formal_param_type...)), _)>
        struct fn_info<args_1(I, R(std::integral_constant<arg__return_type(*)(args__formal_param_type...), function_ptr>), _)> {
          typedef arg__return_type return_type;
          typedef tuple::construct<I, args__formal_param_type...> param_types;
          typedef std::integral_constant<arg__return_type(*)(args__formal_param_type...), function_ptr> fn_ptr;
        };

#include "v1_internals_end.hpp"
      }
      namespace utility {

        template<typename I, typename arg__LHS, typename arg__RHS>
        struct simple_assignment_ok /* based on ISO-IEC 14882:2014 (C++14 standard) [expr.ass]p{3,4} */
          : std::integral_constant<bool,
                                   std::is_union<arg__LHS>::value || std::is_class<arg__LHS>::value
                                   ? std::is_assignable<arg__LHS, arg__RHS>::value
                                   : std::is_convertible<arg__RHS, std::remove_cv_t<arg__LHS>>::value> {
        };

      }
      namespace utility {

        template<typename arg>
        constexpr Y is_complete__test(typename std::integral_constant<std::size_t, sizeof(arg)>::value_type *);
        template<typename arg>
        constexpr N is_complete__test(...);

        template<typename I, typename arg>
        struct is_complete :
          std::integral_constant<bool, std::is_same<decltype(is_complete__test<arg>(nullptr)), Y>::value> {
        };

      }
      namespace utility {

        struct const_cmp_op_deletion_prevention {
          template<typename arg__type>
          bool constexpr operator==(arg__type rhs) const noexcept {
            return std::is_same<decltype(*this), decltype(rhs)>::value;
          }
        };

      }

#define generate(ident)                                                                                                                            \
                                                                                                                                                   \
      namespace utility {                                                                                                                          \
                                                                                                                                                   \
        template<typename arg>                                                                                                                     \
        constexpr Y has_##ident##_as_member_typedef__test(std::remove_reference_t<typename arg::ident> *);                                         \
        template<typename arg>                                                                                                                     \
        constexpr N has_##ident##_as_member_typedef__test(...);                                                                                    \
                                                                                                                                                   \
        template<typename I, typename arg>                                                                                                         \
        struct has_##ident##_as_member_typedef :                                                                                                   \
          std::integral_constant<bool, std::is_same<decltype(has_##ident##_as_member_typedef__test<arg>(nullptr)), Y>::value> {                    \
        };                                                                                                                                         \
                                                                                                                                                   \
      }                                                                                                                                            \
      namespace utility {                                                                                                                          \
                                                                                                                                                   \
        template<typename arg>                                                                                                                     \
        constexpr Y has_##ident##_as_data_member__test(std::remove_reference_t<decltype(arg::ident                                                 \
                                                                                        /* The use of `::' is valid for both static & nonstatic    \
                                                                                         * member by ISO-IEC 14882:2014 (C++14 standard)           \
                                                                                         * [dcl.type.simple]p{4} that refers to                    \
                                                                                         * [expr.prim.general]p{7,8,9}                             \
                                                                                         */       )> *);                                           \
        template<typename arg>                                                                                                                     \
        constexpr N has_##ident##_as_data_member__test(...);                                                                                       \
                                                                                                                                                   \
        template<typename I, typename arg>                                                                                                         \
        struct has_##ident##_as_data_member :                                                                                                      \
          std::integral_constant<bool, std::is_same<decltype(has_##ident##_as_data_member__test<arg>(nullptr)), Y>::value> {                       \
        };                                                                                                                                         \
                                                                                                                                                   \
      }                                                                                                                                            \
      namespace utility {                                                                                                                          \
                                                                                                                                                   \
        template<typename arg>                                                                                                                     \
        constexpr Y has_##ident##_as_static_data_member__test(decltype(arg::ident) arg::*);                                                        \
        template<typename arg>                                                                                                                     \
        constexpr N has_##ident##_as_static_data_member__test(...);                                                                                \
                                                                                                                                                   \
        template<typename I, typename arg, bool is_reference>                                                                                      \
        struct has_##ident##_as_static_data_member__run_test :                                                                                     \
          std::integral_constant<bool, std::is_same<decltype(has_##ident##_as_static_data_member__test<arg>(&arg::ident)), N>::value> {            \
        };                                                                                                                                         \
                                                                                                                                                   \
        template<typename arg>                                                                                                                     \
        constexpr Y has_##ident##_as_static_data_member__test(decltype(&arg::ident));                                                              \
        template<typename arg>                                                                                                                     \
        constexpr N has_##ident##_as_static_data_member__test(...);                                                                                \
                                                                                                                                                   \
        template<typename I, typename arg>                                                                                                         \
        struct has_##ident##_as_static_data_member__run_test<I, arg, true> :                                                                       \
          std::integral_constant<bool, std::is_same<decltype(has_##ident##_as_static_data_member__test<arg>(nullptr)), Y>::value> {                \
        };                                                                                                                                         \
                                                                                                                                                   \
        template<typename I, typename arg, bool arg__has_member>                                                                                   \
        struct has_##ident##_as_static_data_member__reference_check :                                                                              \
          has_##ident##_as_static_data_member__run_test<I, arg, std::is_reference<decltype(arg::ident)>::value> {                                  \
        };                                                                                                                                         \
                                                                                                                                                   \
        template<typename I, typename arg>                                                                                                         \
        struct has_##ident##_as_static_data_member__reference_check<I, arg, false> :                                                               \
          std::false_type {                                                                                                                        \
        };                                                                                                                                         \
                                                                                                                                                   \
        template<typename I, typename arg>                                                                                                         \
        struct has_##ident##_as_static_data_member :                                                                                               \
          has_##ident##_as_static_data_member__reference_check<I, arg, has_##ident##_as_data_member<I, arg>::value> {                              \
          /*                                                                                                                                       \
           * include std::integral_constant<bool, true iff `arg' has a static data member whose identifier is `ident'>;                            \
           */                                                                                                                                      \
        };                                                                                                                                         \
                                                                                                                                                   \
      }                                                                                                                                            \
      namespace utility {                                                                                                                          \
                                                                                                                                                   \
        template<typename arg>                                                                                                                     \
        constexpr Y has_##ident##_as_constexpr_copyable_data_member__test(typename                                                                 \
                                                                          std::integral_constant<bool, H(const_cmp_op_deletion_prevention()        \
                                                                                                         == arg::ident)>::value_type *);           \
        template<typename arg>                                                                                                                     \
        constexpr N has_##ident##_as_constexpr_copyable_data_member__test(...);                                                                    \
                                                                                                                                                   \
        template<typename I, typename arg, bool arg__has_member>                                                                                   \
        struct has_##ident##_as_constexpr_copyable_data_member__run_test :                                                                         \
          std::integral_constant<bool, std::is_same<decltype(has_##ident##_as_constexpr_copyable_data_member__test<arg>(nullptr)), Y>::value> {    \
        };                                                                                                                                         \
                                                                                                                                                   \
        template<typename I, typename arg>                                                                                                         \
        struct has_##ident##_as_constexpr_copyable_data_member__run_test<I, arg, false> :                                                          \
          std::false_type {                                                                                                                        \
        };                                                                                                                                         \
                                                                                                                                                   \
        template<typename I, typename arg>                                                                                                         \
        struct has_##ident##_as_constexpr_copyable_data_member :                                                                                   \
          has_##ident##_as_constexpr_copyable_data_member__run_test<I, arg, has_##ident##_as_static_data_member<I, arg>::value> {                  \
          /*                                                                                                                                       \
           * include std::integral_constant<bool, true iff `arg' has a static constexpr copyable (copy constructor is constexpr and not deleted)   \
           *                                      data member whose identifier is `ident'>;                                                        \
           */                                                                                                                                      \
        };                                                                                                                                         \
                                                                                                                                                   \
      }

      generate(type)          /* has_type_as_member_typedef
                               * has_type_as_data_member
                               * has_type_as_static_data_member
                               * has_type_as_constexpr_copyable_data_member
                               */
      generate(value_type)    /* has_value_type_as_member_typedef
                               * has_value_type_as_data_member
                               * has_value_type_as_static_data_member
                               * has_value_type_as_constexpr_copyable_data_member
                               */
      generate(key)           /* has_key_as_member_typedef
                               * has_key_as_data_member
                               * has_key_as_static_data_member
                               * has_key_as_constexpr_copyable_data_member
                               */
      generate(value)         /* has_value_as_member_typedef
                               * has_value_as_data_member
                               * has_value_as_static_data_member
                               * has_value_as_constexpr_copyable_data_member
                               */
      generate(element_type)  /* has_element_type_as_member_typedef
                               * has_element_type_as_data_member
                               * has_element_type_as_static_data_member
                               * has_element_type_as_constexpr_copyable_data_member
                               */
      generate(size)          /* has_size_as_member_typedef
                               * has_size_as_data_member
                               * has_size_as_static_data_member
                               * has_size_as_constexpr_copyable_data_member
                               */
      generate(elems)         /* has_elems_as_member_typedef
                               * has_elems_as_data_member
                               * has_elems_as_static_data_member
                               * has_elems_as_constexpr_copyable_data_member
                               */
      generate(elems_ptr)     /* has_elems_ptr_as_member_typedef
                               * has_elems_ptr_as_data_member
                               * has_elems_ptr_as_static_data_member
                               * has_elems_ptr_as_constexpr_copyable_data_member
                               */
      generate(data)          /* has_data_as_member_typedef
                               * has_data_as_data_member
                               * has_data_as_static_data_member
                               * has_data_as_constexpr_copyable_data_member
                               */
      generate(is_complete)   /* has_is_complete_as_member_typedef
                               * has_is_complete_as_data_member
                               * has_is_complete_as_static_data_member
                               * has_is_complete_as_constexpr_copyable_data_member
                               */

#undef generate

      namespace utility {

#define generate(ident)                                                                                                                            \
                                                                                                                                                   \
        template<typename I, typename arg, bool arg__has_member>                                                                                   \
        struct validate_##ident##_as_flag__test :                                                                                                  \
          std::is_same<std::remove_reference_t<decltype(arg::ident)>, const bool> {                                                                \
        };                                                                                                                                         \
                                                                                                                                                   \
        template<typename I, typename arg>                                                                                                         \
        struct validate_##ident##_as_flag__test<I, arg, false> :                                                                                   \
          std::false_type {                                                                                                                        \
        };                                                                                                                                         \
                                                                                                                                                   \
        template<typename I, typename arg>                                                                                                         \
        struct validate_##ident##_as_flag :                                                                                                        \
          validate_##ident##_as_flag__test<I, arg, has_##ident##_as_constexpr_copyable_data_member<I, arg>::value> {                               \
          /*                                                                                                                                       \
           * include std::integral_constant<bool, true iff `arg' has a static constexpr data member of type `bool' whose identifier is `ident'>;   \
           */                                                                                                                                      \
        }

        generate(is_complete); // validate_is_complete_as_flag

#undef generate
      }
      namespace utility {

#include "v1_internals_begin.hpp"

#define base_1(I, _1, _2, _3, _4) I                     \
        opts  (_1, arg__precondition_is_met, ())        \
          opts(_2, arg__function, ())                   \
          opts(_3, arg, ())                             \
          opts(_4, arg__unmet_precondition_result, ())
#define decl_1(I, _1, _2, _3, _4)                                       \
        base_1(I##I,                                                    \
               type(_1, bool),                                          \
               type(_2, template<typename I_, typename arg_> class),    \
               type(_3, typename),                                      \
               type(_4, typename))
#define args_1(I, _1, _2, _3, _4) base_1(I, _1, _2, _3, _4)
#define prms_1(...) decl_1(__VA_ARGS__)

        template<prms_1(I, _, _, _, _)>
        struct preconditioned_application
        /* {
         *   if (arg__precondition_is_met) {
         *     include arg__function<I, arg>;
         *   } else {
         *     include arg__unmet_precondition_result;
         *   }
         * }
         */;

        

        template<prms_1(I, X, _, _, _)>
        struct preconditioned_application<args_1(I, R(true), _, _, _)> :
          arg__function<I, arg> {
        };

        template<prms_1(I, _, _, _, _)>
        struct preconditioned_application :
          arg__unmet_precondition_result {
        };

#include "v1_internals_end.hpp"
      }
      namespace utility {

        template<typename I, template<typename I_, typename arg_> class arg__function, typename arg>
        using application = preconditioned_application<I, true, arg__function, arg, I>;

      }
      namespace utility {

        template<typename I, template<typename I_, typename arg_> class arg__function, typename arg__function_arg>
        struct chain {
          typedef arg__function_arg arg;
          typedef arg__function<I, arg> value;
        };

      }
      namespace utility {

        template<typename I, typename arg>
        using std_is_abstract = std::is_abstract<arg>;

      }
      namespace utility {

        template<typename I, typename arg>
        using is_abstract = preconditioned_application<I, application<I, is_complete, arg>::value,
                                                       std_is_abstract, arg, std::false_type>;

      }
      namespace utility {

#include "v1_internals_begin.hpp"

#define base_1(_d3, _d4, I, _1, _2, _3, _4) I                           \
        opts  (_1, arg__function_pointer, ())                           \
          opts(_2, arg__tuple_construct__of__actual_param_types, ())    \
          opts(_3, arg__function_ptr_checker_msg, _d3)                  \
          opts(_4, arg__tuple_checker_msg, _d4)
#define decl_1(_d3, _d4, I, _1, _2, _3, _4)                                                                     \
        base_1(_d3, _d4, I##I,                                                                                  \
               type(_1, typename),                                                                              \
               type(_2, typename),                                                                              \
               type(_3, template<bool arg__is_function_pointer_, typename arg__actual_parameter_> class),       \
               type(_4, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class))
#define args_1(I, _1, _2, _3, _4) base_1((), (), I, _1, _2, _3, _4)
#define prms_1(...) decl_1((), (), __VA_ARGS__)

        template<decl_1(_d3(error::utility::is_callable::arg_2_is_function_pointer),
                        _d4(error::utility::is_callable::arg_3_is_tuple_construct),
                        I, _, _, _, _)>
        struct is_callable
        /* {
         *   include std::integral_constant<bool, true iff `arg__function_pointer' can be called with the given argument types
         *                                        `arg__tuple_construct__of__actual_param_types'>;
         * }
         */;

        

        template<prms_1(I, _, _, _, _)>
        struct is_callable :
          arg__tuple_checker_msg<!!always_false<I>(), arg__tuple_construct__of__actual_param_types> {
        };

        template<prms_1(I, _, R(typename... args__actual_param_type), _, _)>
        struct is_callable<args_1(I, _, R(tuple::construct<I, args__actual_param_type...>), _, _)> :
          arg__function_ptr_checker_msg<!!always_false<I>(), arg__function_pointer> {
        };

        template<prms_1(I, R(typename arg__return_type, typename... args__formal_param_type), R(typename... args__actual_param_type), _, _)>
        struct is_callable<args_1(I, R(arg__return_type(*)(args__formal_param_type...)), R(tuple::construct<I, args__actual_param_type...>),
                                  _, _)> :
          has_type_as_member_typedef<I, std::result_of<arg__return_type(&(args__actual_param_type...))(args__formal_param_type...)>> {
        };

#include "v1_internals_end.hpp"
      }
      namespace utility {

#include "v1_internals_begin.hpp"

#define base_1(_d6, I, _1, _2, _3, _4, _5, _6) I        \
        opts  (_1, arg__checker, ())                    \
          opts(_2, arg__head_pos, ())                   \
          opts(_3, arg__head_pos_inc, ())               \
          opts(_4, arg__tail_pos, ())                   \
          opts(_5, arg__tail, ())                       \
          opts(_6, args__head, _d6)
#define decl_1(I, _1, _2, _3, _4, _5, _6)                                                               \
        base_1((), I##I,                                                                                \
               type(_1, H(template<H(typename I_, typename arg__head_, typename arg__tail_,             \
                                     v1::error::Pos arg__head_pos_, v1::error::Pos arg__tail_pos_)>     \
                          class)),                                                                      \
               type(_2, v1::error::Pos),                                                                \
               type(_3, v1::error::Pos),                                                                \
               type(_4, v1::error::Pos),                                                                \
               type(_5, typename),                                                                      \
               type(_6, typename...))
#define args_1(I, _1, _2, _3, _4, _5, _6) base_1((...), I, _1, _2, _3, _4, _5, _6)
#define prms_1(...) decl_1(__VA_ARGS__)

        template<prms_1(I, _, _, _, _, _, _)>
        struct check_pairwise_distinctness;

        

        template<prms_1(I, _, X, _, _, _, X)>
        struct check_pairwise_distinctness<args_1(I, _, R(arg__tail_pos), _, _, _, X)> {
        };

        template<prms_1(I, _, _, _, _, _, R(typename arg__head, typename... args__head))>
        struct check_pairwise_distinctness<args_1(I, _, _, _, _, _, R(arg__head, args__head...))> :
          arg__checker<I, arg__head, arg__tail, arg__head_pos, arg__tail_pos>,
          check_pairwise_distinctness<args_1(I, _, R(arg__head_pos + arg__head_pos_inc), _, _, _, R(args__head...))> {
        };

#include "v1_internals_end.hpp"
      }
      namespace utility {

#include "v1_internals_begin.hpp"

#define base_1A(_d5, I, _1, _2, _3, _4, _5) I   \
        opts  (_1, arg__Node__head, ())         \
          opts(_2, arg__Node__tail, ())         \
          opts(_3, arg__head_pos, ())           \
          opts(_4, arg__tail_pos, ())           \
          opts(_5, arg__checker_msg, _d5)
#define decl_1A(_d5, I, _1, _2, _3, _4, _5)                                                                                                     \
        base_1A(_d5, I##I,                                                                                                                      \
                type(_1, typename),                                                                                                             \
                type(_2, typename),                                                                                                             \
                type(_3, v1::error::Pos),                                                                                                       \
                type(_4, v1::error::Pos),                                                                                                       \
                type(_5, template<bool arg__head_and_tail_are_distinct_, v1::error::Pos arg__head_pos_, v1::error::Pos arg__tail_pos_> class))
#define args_1A(I, _1, _2, _3, _4, _5) base_1A((), I, _1, _2, _3, _4, _5)
#define prms_1A(...) decl_1A((), __VA_ARGS__)

        template<prms_1A(I, _, _, _, _, _)>
        struct check_pairwise_distinctness_of_nodes__test;

#define base_1B(_d1, _d2, I, _1, _2) I          \
        opts  (_1, arg__head_checker_msg, _d1)  \
          opts(_2, arg__tail_checker_msg, _d2)
#define decl_1B(_d1, _d2, I, _1, _2)                                                                                            \
        base_1B(_d1, _d2, I##I,                                                                                                 \
                type(_1, template<bool arg__is_Node_, v1::error::Pos arg__head_pos_, typename arg__actual_parameter_> class),   \
                type(_2, template<bool arg__is_Node_, v1::error::Pos arg__tail_pos_, typename arg__actual_parameter_> class))
#define args_1B(I, _1, _2) base_1B((), (), I, _1, _2)
#define prms_1B(...) decl_1B((), (), __VA_ARGS__)

#define base_2A(args_1A, args_1B)               \
        args_1A                                 \
        args_1B
#define decl_2A(_1A_d5, _1B_d1, _1B_d2, I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1B_1, _1B_2)     \
        base_2A(decl_1A(_1A_d5, I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5),                          \
                decl_1B(_1B_d1, _1B_d2, , _1B_1, _1B_2))
#define args_2A(I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1B_1, _1B_2) base_2A(args_1A(I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5), args_1B(, _1B_1, _1B_2))
#define prms_2A(...) decl_2A((), (), (), __VA_ARGS__)

        template<decl_2A(_1A_d5(error::utility::check_pairwise_distinctness_of_nodes::nodes_are_pairwise_distinct),
                         _1B_d1(error::utility::check_pairwise_distinctness_of_nodes::head_is_Node),
                         _1B_d2(error::utility::check_pairwise_distinctness_of_nodes::tail_is_Node),
                         I, _, _, _, _, _, _, _)>
        struct check_pairwise_distinctness_of_nodes;

#define base_1C(I, _1, _2, _3) I                \
        opts  (_1, arg__Node__node, ())         \
          opts(_2, arg__node_pos, ())           \
          opts(_3, arg__node_checker_msg, ())
#define decl_1C(I, _1, _2, _3)                                                                                                  \
        base_1C(I##I,                                                                                                           \
                type(_1, typename),                                                                                             \
                type(_2, v1::error::Pos),                                                                                       \
                type(_3, template<bool arg__is_Node_, v1::error::Pos arg__node_pos_, typename arg__actual_parameter_> class))
#define args_1C(I, _1, _2, _3) base_1C(I, _1, _2, _3)
#define prms_1C(...) decl_1C(__VA_ARGS__)

        template<prms_1C(I, _, _, _)>
        struct check_pairwise_distinctness_of_nodes__check_node;

        

        template<prms_2A(I, _, _, _, _, _, _, _)>
        struct check_pairwise_distinctness_of_nodes :
          check_pairwise_distinctness_of_nodes__check_node<args_1C(I, R(arg__Node__head), R(arg__head_pos), R(arg__head_checker_msg))>,
          check_pairwise_distinctness_of_nodes__check_node<args_1C(I, R(arg__Node__tail), R(arg__tail_pos), R(arg__tail_checker_msg))>,
          check_pairwise_distinctness_of_nodes__test<args_1A(I, _, _, _, _, _)> {
        };

        

        template<prms_1C(I, _, _, _)>
        struct check_pairwise_distinctness_of_nodes__check_node :
          arg__node_checker_msg<!!utility::always_false<I>(), arg__node_pos, arg__Node__node> {
        };

        template<prms_1C(I, R(typename... args__node_arg), _, _)>
        struct check_pairwise_distinctness_of_nodes__check_node<args_1C(I, R(v1::Node<args__node_arg...>), _, _)>  {
        };

        

        template<prms_1A(I, _, _, _, _, _)>
        struct check_pairwise_distinctness_of_nodes__test :
          arg__checker_msg<!lvalues_are_equal<I,
                                              const typename arg__Node__head::fn_info::fn_ptr::value_type,
                                              const typename arg__Node__tail::fn_info::fn_ptr::value_type,
                                              &arg__Node__head::fn_info::fn_ptr::value,
                                              &arg__Node__tail::fn_info::fn_ptr::value>::value,
                           arg__head_pos, arg__tail_pos> {
        };

#include "v1_internals_end.hpp"
      }
      namespace utility {

        // When using this factory of checkers as an argument to a template template-parameter, it is safe to always use the keyword `template' to
        // avoid compile-time error as illustrated by the following compilable C++14 program:
        //
        // template<template<typename> class>
        // struct template_having_template_parameter {
        //   struct checker;
        // };
        //
        // template<typename>
        // struct template_expecting_checker_as_template_argument {
        // };
        //
        // template<template<typename> class B>
        // struct tester :
        //   template_expecting_checker_as_template_argument<template_having_template_parameter<B>::template checker> {
        // };
        //
        // To quote [temp.names]p4 of the C++14 standard: When the name of a member template specialization appears after . or -> in a
        // postfix-expression or after a nested-name-specifier in a qualified-id, and the object expression of the postfix-expression is
        // type-dependent or the nested-name-specifier in the qualified-id refers to a dependent type, but the name is not a member of the current
        // instantiation (14.6.2.1), the member template name must be prefixed by the keyword template.  Otherwise the name is assumed to name a
        // non-template.
        //
        // To quote [temp.spec]p4 of the C++14 standard: An instantiated template specialization can be either implicitly instantiated (14.7.1) for
        // a given argument list or be explicitly instantiated (14.7.2).  A specialization is a class, function, or class member that is either
        // instantiated or explicitly specialized (14.7.3).
        //
        // Lastly, to quote [temp.names]p5 of the C++14 standard: A name prefixed by the keyword template shall be a template-id or the name shall
        // refer to a class template.  [ Note: The keyword template may not be applied to non-template members of class templates. --- end note ]
        // [ Note: As is the case with the typename prefix, the template prefix is allowed in cases where it is not strictly necessary;
        // i.e., when the nested-name-specifier or the expression on the left of the -> or . is not dependent on a template-parameter, or
        // the use does not appear in the scope of a template. --- end note ]
        template<typename I,
                 template<bool arg__head_and_tail_are_distinct_, v1::error::Pos arg__head_pos_, v1::error::Pos arg__tail_pos_>
                 class arg__checker_msg = error::utility::check_pairwise_distinctness_of_nodes::nodes_are_pairwise_distinct,
                 template<bool arg__is_Node_, v1::error::Pos arg__head_pos_, typename arg__actual_parameter_>
                 class arg__head_checker_msg = error::utility::check_pairwise_distinctness_of_nodes::head_is_Node,
                 template<bool arg__is_Node_, v1::error::Pos arg__tail_pos_, typename arg__actual_parameter_>
                 class arg__tail_checker_msg = error::utility::check_pairwise_distinctness_of_nodes::tail_is_Node>
        struct nodes_pairwise_distinctness {

          template<typename I_, typename arg__Node__head_, typename arg__Node__tail_, v1::error::Pos arg__head_pos_, v1::error::Pos arg__tail_pos_>
          using checker = check_pairwise_distinctness_of_nodes<I_, arg__Node__head_, arg__Node__tail_, arg__head_pos_, arg__tail_pos_,
                                                               arg__checker_msg, arg__head_checker_msg, arg__tail_checker_msg>;

        };

      }
      namespace utility {
        namespace ratio {

#include "v1_internals_begin.hpp"

#define base_1(_d2, _d3, I, _1, _2, _3) I               \
          opts  (_1, arg, ())                           \
            opts(_2, arg__denominator_checker_msg, _d2) \
            opts(_3, arg__ratio_checker_msg, _d3)
#define decl_1(_d2, _d3, I, _1, _2, _3)                                                                         \
          base_1(_d2, _d3, I##I,                                                                                \
                 type(_1, typename),                                                                            \
                 type(_2, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class), \
                 type(_3, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class))
#define args_1(I, _1, _2, _3) base_1((), (), I, _1, _2, _3)
#define prms_1(...) decl_1((), (), __VA_ARGS__)

          template<decl_1(_d2(error::utility::ratio::validate::arg_2_has_nonzero_denominator),
                          _d3(error::utility::ratio::validate::arg_2_is_std_ratio),
                          I, _, _, _)>
          struct validate;

          

          template<prms_1(I, _, _, _)>
          struct validate :
            arg__ratio_checker_msg<!!utility::always_false<I>(), arg> {
          };

          template<prms_1(I, R(std::intmax_t arg__num, std::intmax_t arg__den), _, _)>
          struct validate<args_1(I, R(std::ratio<arg__num, arg__den>), _, _)> :
            arg__denominator_checker_msg<arg__den != 0, std::ratio<arg__num, arg__den>> {
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace utility {
        namespace ratio {

#include "v1_internals_begin.hpp"

#define base_1(_d2, I, _1, _2) I                        \
          opts  (_1, arg, ())                           \
            opts(_2, arg__positive_checker_msg, _d2)
#define decl_1(_d2, I, _1, _2)                                                                          \
          base_1(_d2, I##I,                                                                             \
                 type(_1, typename),                                                                    \
                 type(_2, template<bool arg__is_positive_, typename arg__actual_parameter_> class))
#define args_1(I, _1, _2) base_1((), I, _1, _2)
#define prms_1(...) decl_1((), __VA_ARGS__)

          template<prms_1(I, _, _)>
          struct validate_positive__check_value;

#define base_2(_d1, _d2, args_1, _1, _2)                \
          args_1                                        \
          opts  (_1, arg__denominator_checker_msg, _d1) \
            opts(_2, arg__ratio_checker_msg, _d2)
#define decl_2(_1_d2, _d1, _d2, I, _1_1, _1_2, _1, _2)                                                          \
          base_2(_d1, _d2,                                                                                      \
                 decl_1(_1_d2, I, _1_1, _1_2),                                                                  \
                 type(_1, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class), \
                 type(_2, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class))
#define args_2(I, _1_1, _1_2, _1, _2) base_2((), (), args_1(I, _1_1, _1_2), _1, _2)
#define prms_2(...) decl_2((), (), (), __VA_ARGS__)

          template<decl_2(_1_d2(error::utility::ratio::validate_positive::arg_2_is_positive),
                          _d1  (error::utility::ratio::validate_positive::arg_2_has_nonzero_denominator),
                          _d2  (error::utility::ratio::validate_positive::arg_2_is_std_ratio),
                          I, _, _, _, _)>
          struct validate_positive;

          

          template<prms_2(I, _, _, _, _)>
          struct validate_positive :
            validate<I, arg, arg__denominator_checker_msg, arg__ratio_checker_msg>,
            validate_positive__check_value<args_1(I, _, _)> {
          };

          

          template<prms_1(I, _, _)>
          struct validate_positive__check_value :
            arg__positive_checker_msg<std::ratio_greater<arg, std::ratio<0>>::value, arg> {
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace utility {
        namespace ratio {

#include "v1_internals_begin.hpp"

#define base_1(_d2, I, _1, _2) I                        \
          opts  (_1, arg, ())                           \
            opts(_2, arg__nonnegative_checker_msg, _d2)
#define decl_1(_d2, I, _1, _2)                                                                          \
          base_1(_d2, I##I,                                                                             \
                 type(_1, typename),                                                                    \
                 type(_2, template<bool arg__is_nonnegative_, typename arg__actual_parameter_> class))
#define args_1(I, _1, _2) base_1((), I, _1, _2)
#define prms_1(...) decl_1((), __VA_ARGS__)

          template<prms_1(I, _, _)>
          struct validate_nonnegative__check_value;

#define base_2(_d1, _d2, args_1, _1, _2)                \
          args_1                                        \
          opts  (_1, arg__denominator_checker_msg, _d1) \
            opts(_2, arg__ratio_checker_msg, _d2)
#define decl_2(_1_d2, _d1, _d2, I, _1_1, _1_2, _1, _2)                                                          \
          base_2(_d1, _d2,                                                                                      \
                 decl_1(_1_d2, I, _1_1, _1_2),                                                                  \
                 type(_1, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class), \
                 type(_2, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class))
#define args_2(I, _1_1, _1_2, _1, _2) base_2((), (), args_1(I, _1_1, _1_2), _1, _2)
#define prms_2(...) decl_2((), (), (), __VA_ARGS__)

          template<decl_2(_1_d2(error::utility::ratio::validate_nonnegative::arg_2_is_nonnegative),
                          _d1  (error::utility::ratio::validate_nonnegative::arg_2_has_nonzero_denominator),
                          _d2  (error::utility::ratio::validate_nonnegative::arg_2_is_std_ratio),
                          I, _, _, _, _)>
          struct validate_nonnegative;

          

          template<prms_2(I, _, _, _, _)>
          struct validate_nonnegative :
            validate<I, arg, arg__denominator_checker_msg, arg__ratio_checker_msg>,
            validate_nonnegative__check_value<args_1(I, _, _)> {
          };

          

          template<prms_1(I, _, _)>
          struct validate_nonnegative__check_value :
            arg__nonnegative_checker_msg<std::ratio_greater_equal<arg, std::ratio<0>>::value, arg> {
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace utility {
        namespace select {

          struct MIN; // `arg__lhs::value < arg__rhs::value' must be defined; if equal, arg__rhs is selected
          struct MAX; // `arg__lhs::value < arg__rhs::value' must be defined; if equal, arg__lhs is selected

        }
      }
      namespace utility {
        namespace select {

#include "v1_internals_begin.hpp"

#define base_1(I, _1, _2) I                     \
          opts  (_1, arg__lhs, ())              \
            opts(_2, arg__rhs, ())
#define decl_1(I, _1, _2)                       \
          base_1(I##I,                          \
                 type(_1, typename),            \
                 type(_2, typename))
#define args_1(I, _1, _2) base_1(I, _1, _2)
#define prms_1(...) decl_1(__VA_ARGS__)

          template<prms_1(I, _, _)>
          Y between__check_operation_availability__MIN(int (*)[arg__lhs::value < arg__rhs::value ? 1 : 2]);
          template<prms_1(I, _, _)>
          N between__check_operation_availability__MIN(...);

          template<prms_1(I, _, _)>
          Y between__check_operation_availability__MAX(int (*)[arg__lhs::value < arg__rhs::value ? 1 : 2]);
          template<prms_1(I, _, _)>
          N between__check_operation_availability__MAX(...);

#define base_2(args_1, _1)                      \
          args_1                                \
          opts  (_1, arg__operator, ())
#define decl_2(I, _1_1, _1_2, _1)               \
          base_2(prms_1(I, _1_1, _1_2),         \
                 type(_1, typename))
#define args_2(I, _1_1, _1_2, _1) base_2(args_1(I, _1_1, _1_2), _1)
#define prms_2(...) decl_2(__VA_ARGS__)

#define base_3A(_d1, args_2, _1)                \
          args_2                                \
          opts  (_1, arg__is_lhs_selected, _d1)
#define decl_3A(_d1, I, _1_1, _1_2, _2_1, _1)   \
          base_3A(_d1,                          \
                  prms_2(I, _1_1, _1_2, _2_1),  \
                  type(_1, bool))
#define args_3A(I, _1_1, _1_2, _2_1, _1) base_3A((), args_2(I, _1_1, _1_2, _2_1), _1)
#define prms_3A(...) decl_3A((), __VA_ARGS__)

          template<decl_3A(_d1(std::is_same<arg__operator, MIN>::value
                               ?  (arg__lhs::value < arg__rhs::value)
                               : !(arg__lhs::value < arg__rhs::value)),
                           I, _, _, _, _)>
          struct between__run;

#define base_3B(_d1, args_2, _1)                                        \
          args_2                                                        \
          opts  (_1, arg__operation_availability_checker_msg, _d1)
#define decl_3B(_d1, I, _1_1, _1_2, _2_1, _1)                                                                   \
          base_3B(_d1,                                                                                          \
                  prms_2(I, _1_1, _1_2, _2_1),                                                                  \
                  type(_1, template<H(bool arg__is_defined_, typename arg__operator_,                           \
                                      typename arg__lhs_value_type_, typename arg__rhs_value_type_)> class))
#define args_3B(I, _1_1, _1_2, _2_1, _1) base_3B((), args_2(I, _1_1, _1_2, _2_1), _1)
#define prms_3B(...) decl_3B((), __VA_ARGS__)

#define base_4BA(_d1, args_3B, _1)              \
          args_3B                               \
          opts  (_1, arg__is_available, _d1)
#define decl_4BA(_d1, I, _1_1, _1_2, _2_1, _3B_1, _1)   \
          base_4BA(_d1,                                 \
                   prms_3B(I, _1_1, _1_2, _2_1, _3B_1), \
                   type(_1, bool))
#define args_4BA(I, _1_1, _1_2, _2_1, _3B_1, _1) base_4BA((), args_3B(I, _1_1, _1_2, _2_1, _3B_1), _1)
#define prms_4BA(...) decl_4BA((), __VA_ARGS__)

          template<decl_4BA(_d1(std::is_same<MIN, arg__operator>::value
                                ? std::is_same<Y, decltype(between__check_operation_availability__MIN<args_1(I, _, _)>(nullptr))>::value
                                : std::is_same<Y, decltype(between__check_operation_availability__MAX<args_1(I, _, _)>(nullptr))>::value),
                            I, _, _, _, _, _)>
          struct between__check_operation_availability;

#define base_4BB(_d1, _d2, _d3, args_3B, _1, _2, _3)    \
          args_3B                                       \
          opts  (_1, arg__lhs_validator_msg, _d1)       \
            opts(_2, arg__rhs_validator_msg, _d2)       \
            opts(_3, arg__operator_checker_msg, _d3)
#define decl_4BB(_3B_d1, _d1, _d2, _d3, I, _1_1, _1_2, _2_1, _3B_1, _1, _2, _3)                                                         \
          base_4BB(_d1, _d2, _d3,                                                                                                       \
                   decl_3B(_3B_d1, I, _1_1, _1_2, _2_1, _3B_1),                                                                         \
                   type(_1, template<bool arg__has_value_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),   \
                   type(_2, template<bool arg__has_value_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),   \
                   type(_3, template<H(bool arg__is_valid_selection_operator_, typename arg__actual_parameter_,                         \
                                       typename arg__available_operators_)> class))
#define args_4BB(I, _1_1, _1_2, _2_1, _3B_1, _1, _2, _3) base_4BB((), (), (), args_3B(I, _1_1, _1_2, _2_1, _3B_1), _1, _2, _3)
#define prms_4BB(...) decl_4BB((), (), (), (), __VA_ARGS__)

          template<decl_4BB(_3B_d1(error::utility::select::between::arg_4_is_defined),
                            _d1   (error::utility::select::between::arg_2_has_value_as_constexpr_copyable_data_member),
                            _d2   (error::utility::select::between::arg_3_has_value_as_constexpr_copyable_data_member),
                            _d3   (error::utility::select::between::arg_4_is_valid_selection_operator),
                            I, _, _, _, _, _, _, _)>
          struct between
          /* {
           *   if (std::is_same<MIN, arg__operator>::value) {
           *     if (arg__lhs::value < arg__rhs::value) {
           *       typedef arg__lhs value;
           *     } else {
           *       typedef arg__rhs value;
           *     }
           *   } else if (std::is_same<MAX, arg__operator>::value) {
           *     if (arg__lhs::value < arg__rhs::value) {
           *       typedef arg__rhs value;
           *     } else {
           *       typedef arg__lhs value;
           *     }
           *   }
           * }
           */;

          

          template<typename...>
          struct available_operators;

          template<prms_4BB(I, _, _, _, _, _, _, _)>
          struct between :
            arg__lhs_validator_msg<has_value_as_constexpr_copyable_data_member<I, arg__lhs>::value, arg__lhs>,
            arg__rhs_validator_msg<has_value_as_constexpr_copyable_data_member<I, arg__rhs>::value, arg__rhs>,
            arg__operator_checker_msg<H((std::is_same<MIN, arg__operator>::value
                                         || std::is_same<MAX, arg__operator>::value), arg__operator,
                                        available_operators<MIN, MAX>)>,
            between__check_operation_availability<args_4BA(I, _, _, _, _, X)> {
          };

          

          template<prms_4BA(I, _, _, _, _, _)>
          struct between__check_operation_availability :
            arg__operation_availability_checker_msg<!!utility::always_false<I>(),
                                                    arg__operator, decltype(arg__lhs::value), decltype(arg__rhs::value)> {
          };

          template<prms_4BA(I, _, _, _, _, X)>
          struct between__check_operation_availability<args_4BA(I, _, _, _, _, R(true))> :
            between__run<args_3A(I, _, _, _, X)> {
          };

          

          template<prms_3A(I, _, _, _, _)>
          struct between__run
          {
            typedef arg__rhs value;
          };

          template<prms_3A(I, _, _, _, X)>
          struct between__run<args_3A(I, _, _, _, R(true))>
          {
            typedef arg__lhs value;
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace utility {

#include "v1_internals_begin.hpp"

#define base_1(_d1, I, _1) I                                            \
        opts  (_1, arg__operation_availability_checker_msg, _d1)
#define decl_1(_d1, I, _1)                                                                                                      \
        base_1(_d1, I##I,                                                                                                       \
               type(_1, template<bool arg__is_defined_, typename arg__lhs_value_type_, typename arg__rhs_value_type_> class))
#define args_1(I, _1) base_1((), I, _1)
#define prms_1(...) decl_1((), __VA_ARGS__)

        template<prms_1(I, _)>
        struct min__adaptor__arg__operation_availability_checker_msg;

#define base_2(_d3, _d4, I, _1, _2, args_1, _3, _4) I   \
        opts  (_1, arg__lhs, ())                        \
          opts(_2, arg__rhs, ())                        \
          args_1                                        \
          opts(_3, arg__lhs_validator_msg, _d3)         \
          opts(_4, arg__rhs_validator_msg, _d4)
#define decl_2(_1_d1, _d3, _d4, I, _1, _2, _1_1, _3, _4)                                                                                \
        base_2(_d3, _d4, I##I,                                                                                                          \
               type(_1, typename),                                                                                                      \
               type(_2, typename),                                                                                                      \
               decl_1(_1_d1, , _1_1),                                                                                                   \
               type(_3, template<bool arg__has_value_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),       \
               type(_4, template<bool arg__has_value_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class))
#define args_2(I, _1, _2, _1_1, _3, _4) base_2((), (), I, _1, _2, args_1(, _1_1), _3, _4)
#define prms_2(...) decl_2((), (), (), __VA_ARGS__)

        template<decl_2(_1_d1(error::utility::min::operator_less_than_is_defined),
                        _d3  (error::utility::min::arg_2_has_value_as_constexpr_copyable_data_member),
                        _d4  (error::utility::min::arg_3_has_value_as_constexpr_copyable_data_member),
                        I, _, _, _, _, _)>
        struct min;

        

        template<prms_2(I, _, _, _, _, _)>
        struct min :
          select::between<I, arg__lhs, arg__rhs, select::MIN,
                          min__adaptor__arg__operation_availability_checker_msg<I, arg__operation_availability_checker_msg>::template msg,
                          arg__lhs_validator_msg, arg__rhs_validator_msg> {
        };

        

        template<prms_1(I, _)>
        struct min__adaptor__arg__operation_availability_checker_msg {

          template<bool arg__is_defined_, typename arg__operator_, typename arg__lhs_value_type_, typename arg__rhs_value_type_>
          using msg = arg__operation_availability_checker_msg<arg__is_defined_, arg__lhs_value_type_, arg__rhs_value_type_>;

        };

#include "v1_internals_end.hpp"
      }
      namespace utility {

#include "v1_internals_begin.hpp"

#define base_1(_d1, I, _1) I                                            \
        opts  (_1, arg__operation_availability_checker_msg, _d1)
#define decl_1(_d1, I, _1)                                                                                                      \
        base_1(_d1, I##I,                                                                                                       \
               type(_1, template<bool arg__is_defined_, typename arg__lhs_value_type_, typename arg__rhs_value_type_> class))
#define args_1(I, _1) base_1((), I, _1)
#define prms_1(...) decl_1((), __VA_ARGS__)

        template<prms_1(I, _)>
        struct max__adaptor__arg__operation_availability_checker_msg;

#define base_2(_d3, _d4, I, _1, _2, args_1, _3, _4) I   \
        opts  (_1, arg__lhs, ())                        \
          opts(_2, arg__rhs, ())                        \
          args_1                                        \
          opts(_3, arg__lhs_validator_msg, _d3)         \
          opts(_4, arg__rhs_validator_msg, _d4)
#define decl_2(_1_d1, _d3, _d4, I, _1, _2, _1_1, _3, _4)                                                                                \
        base_2(_d3, _d4, I##I,                                                                                                          \
               type(_1, typename),                                                                                                      \
               type(_2, typename),                                                                                                      \
               decl_1(_1_d1, , _1_1),                                                                                                   \
               type(_3, template<bool arg__has_value_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),       \
               type(_4, template<bool arg__has_value_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class))
#define args_2(I, _1, _2, _1_1, _3, _4) base_2((), (), I, _1, _2, args_1(, _1_1), _3, _4)
#define prms_2(...) decl_2((), (), (), __VA_ARGS__)

        template<decl_2(_1_d1(error::utility::max::operator_less_than_is_defined),
                        _d3  (error::utility::max::arg_2_has_value_as_constexpr_copyable_data_member),
                        _d4  (error::utility::max::arg_3_has_value_as_constexpr_copyable_data_member),
                        I, _, _, _, _, _)>
        struct max;

        

        template<prms_2(I, _, _, _, _, _)>
        struct max :
          select::between<I, arg__lhs, arg__rhs, select::MAX,
                          max__adaptor__arg__operation_availability_checker_msg<I, arg__operation_availability_checker_msg>::template msg,
                          arg__lhs_validator_msg, arg__rhs_validator_msg> {
        };

        

        template<prms_1(I, _)>
        struct max__adaptor__arg__operation_availability_checker_msg {

          template<bool arg__is_defined_, typename arg__operator_, typename arg__lhs_value_type_, typename arg__rhs_value_type_>
          using msg = arg__operation_availability_checker_msg<arg__is_defined_, arg__lhs_value_type_, arg__rhs_value_type_>;

        };

#include "v1_internals_end.hpp"
      }
      namespace utility {
        namespace fp {

#include "v1_internals_begin.hpp"

#define base_1(_d2, I, _1, _2) I             \
          opts  (_1, arg, ())                \
            opts(_2, arg__checker_msg, _d2)
#define decl_1(_d2, I, _1, _2)                                                                          \
          base_1(_d2, I##I,                                                                             \
                 type(_1, typename),                                                                    \
                 type(_2, template<bool arg__is_valid_, typename arg__actual_parameter_> class))
#define args_1(I, _1, _2) base_1((), I, _1, _2)
#define prms_1(...) decl_1((), __VA_ARGS__)

          template<decl_1(_d2(error::utility::fp::Double::arg_2_is_valid),
                          I, _, _)>
          struct Double;

          

          template<prms_1(I, _, _)>
          struct Double :
            arg__checker_msg<!!utility::always_false<I>(), arg> {
          };

          template<prms_1(I, R(std::intmax_t arg__numerator, std::intmax_t arg__denominator), _)>
          struct Double<args_1(I, R(std::ratio<arg__numerator, arg__denominator>), _)> {
            static constexpr double value {arg__numerator / static_cast<double>(arg__denominator)};
            typedef std::ratio<arg__numerator, arg__denominator> rational_value;
          };


#include "v1_internals_end.hpp"
        }
      }
      namespace utility {
        namespace fp {

#include "v1_internals_begin.hpp"

#define base_1(_d4, _d5, _d6, I, _1, _2, _3, _4, _5, _6) I      \
          opts  (_1, arg__pointer_to_value, ())                 \
            opts(_2, arg__std_ratio__reference, ())             \
            opts(_3, arg__rounding_function, ())                \
            opts(_4, arg__pointer_checker_msg, _d4)             \
            opts(_5, arg__reference_checker_msg, _d5)           \
            opts(_6, arg__rounding_function_checker_msg, _d6)
#define decl_1(_d4, _d5, _d6, I, _1, _2, _3, _4, _5, _6)                                                \
          base_1(_d4, _d5, _d6, I##I,                                                                   \
                 type(_1, const double *),                                                              \
                 type(_2, typename),                                                                    \
                 typr(_3, std::intmax_t (*arg__rounding_function)(double)),                             \
                 type(_4, template<bool arg__is_not_nullptr_> class),                                   \
                 type(_5, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),    \
                 type(_6, template<bool arg__is_not_nullptr_> class))
#define args_1(I, _1, _2, _3, _4, _5, _6) base_1((), (), (), I, _1, _2, _3, _4, _5, _6)
#define prms_1(...) decl_1((), (), (), __VA_ARGS__)

          template<decl_1(_d4(error::utility::fp::Ratio::arg_2_is_not_nullptr),
                          _d5(error::utility::fp::Ratio::arg_3_is_valid),
                          _d6(error::utility::fp::Ratio::arg_4_is_not_nullptr),
                          I, _, _, _, _, _, _)>
          struct Ratio;

          

          template<prms_1(I, _, _, _, _, _, _)>
          struct Ratio :
            arg__reference_checker_msg<!!utility::always_false<I>(), arg__std_ratio__reference> {
          };

          template<prms_1(I, X, R(std::intmax_t arg__numerator, std::intmax_t arg__denominator), _, _, _, _)>
          struct Ratio<args_1(I, R(nullptr), R(std::ratio<arg__numerator, arg__denominator>), _, _, _, _)> :
            arg__pointer_checker_msg<!!utility::always_false<I>()> {
          };

          template<prms_1(I, _, R(std::intmax_t arg__numerator, std::intmax_t arg__denominator), X, _, _, _)>
          struct Ratio<args_1(I, _, R(std::ratio<arg__numerator, arg__denominator>), R(nullptr), _, _, _)> :
            arg__rounding_function_checker_msg<!!utility::always_false<I>()> {
          };

          template<prms_1(I, _, R(std::intmax_t arg__numerator, std::intmax_t arg__denominator), _, _, _, _)>
          struct Ratio<args_1(I, _, R(std::ratio<arg__numerator, arg__denominator>), _, _, _, _)>
          {
            typedef std::ratio<arg__rounding_function(arg__denominator * *arg__pointer_to_value), arg__denominator> value;
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace utility {
        namespace fp {

          struct positive_infinity {
            static constexpr double value {std::numeric_limits<double>::infinity()};
          };

        }
      }
      namespace utility {
        namespace fp {

          struct epsilon {
            static constexpr double value {1E4 * std::numeric_limits<double>::epsilon()};
          };

        }
      }
      namespace utility {
        namespace fp {

          constexpr bool eq(double lhs, double rhs) {
            if (lhs == positive_infinity::value || rhs == positive_infinity::value) {
              return false;
            }
            double diff = lhs - rhs;
            return -epsilon::value < diff && diff < epsilon::value;
          }

        }
      }
      namespace utility {
        namespace fp {

          constexpr bool lt(double lhs, double rhs) {
            if (lhs == positive_infinity::value && rhs == positive_infinity::value) {
              return false;
            } else if (lhs == positive_infinity::value) {
              return false;
            } else if (rhs == positive_infinity::value) {
              return true;
            } else {
              return lhs < rhs - epsilon::value;
            }
          }

        }
      }
      namespace utility {
        namespace fp {

          constexpr bool gt(double lhs, double rhs) {
            if (lhs == positive_infinity::value && rhs == positive_infinity::value) {
              return false;
            } else if (lhs == positive_infinity::value) {
              return true;
            } else if (rhs == positive_infinity::value) {
              return false;
            } else {
              return lhs > rhs + epsilon::value;
            }
          }

        }
      }
      namespace utility {
        namespace fp {

          constexpr bool is_integral(double x) {
            if (x == positive_infinity::value) {
              return false;
            } else {
              std::intmax_t integral_part = static_cast<std::intmax_t>(x);
              double diff = x - integral_part;
              return (diff < 0
                      ? -epsilon::value < diff
                      : diff < epsilon::value);
            }
          }

        }
      }
      namespace utility {
        namespace fp {

          template<typename I,
                   bool arg__operand_is_not_positive_infinity,
                   H(template<bool arg__is_not_positive_infinity_>
                     class arg__positive_infinity_checker_msg = error::utility::fp::round::arg__operand_is_not_positive_infinity)>
          constexpr std::intmax_t round(double x) {
            run_metaprogram(arg__positive_infinity_checker_msg<arg__operand_is_not_positive_infinity>);
            std::intmax_t integral_part = static_cast<std::intmax_t>(x);
            return integral_part + (x < 0
                                    ? (integral_part - x < 0.5 ? 0 : -1)
                                    : (x - integral_part < 0.5 ? 0 :  1));
          }

        }
      }
      namespace utility {
        namespace fp {

          template<typename I,
                   bool arg__operand_is_not_positive_infinity,
                   H(template<bool arg__is_not_positive_infinity_>
                     class arg__positive_infinity_checker_msg = error::utility::fp::ceil::arg__operand_is_not_positive_infinity)>
          constexpr std::intmax_t ceil(double x) {
            run_metaprogram(arg__positive_infinity_checker_msg<arg__operand_is_not_positive_infinity>);
            std::intmax_t integral_part = static_cast<std::intmax_t>(x);
            return integral_part + (x < 0
                                    ? ((integral_part - 1) - x <= -epsilon::value ? 0 : -1)
                                    : (x - integral_part < epsilon::value ? 0 : 1));
          }

        }
      }
      namespace utility {
        namespace fp {

          template<typename I,
                   bool arg__operand_is_not_positive_infinity,
                   H(template<bool arg__is_not_positive_infinity_>
                     class arg__positive_infinity_checker_msg = error::utility::fp::floor::arg__operand_is_not_positive_infinity)>
          constexpr std::intmax_t floor(double x) {
            run_metaprogram(arg__positive_infinity_checker_msg<arg__operand_is_not_positive_infinity>);
            std::intmax_t integral_part = static_cast<std::intmax_t>(x);
            return integral_part + (x < 0
                                    ? (x - integral_part <= -epsilon::value ? -1 : 0)
                                    : ((integral_part + 1) - x < epsilon::value ? 1 : 0));
          }

        }
      }
      namespace utility {
        namespace fp {

          constexpr double abs(double x) {
            if (x == positive_infinity::value) {
              return x;
            } else {
              return x < 0 ? -x : x;
            }
          }

        }
      }
      namespace utility {
        namespace list {

#include "v1_internals_begin.hpp"

#define base_1A(I, _1, _2) I                    \
          opts  (_1, arg__size, ())             \
            opts(_2, arg__producer, ())
#define decl_1A(I, _1, _2)                                                              \
          base_1A(I##I,                                                                 \
                  type(_1, v1::array::Size),                                            \
                  type(_2, template<typename I_, v1::array::Idx arg__idx_> class))
#define args_1A(I, _1, _2) base_1A(I, _1, _2)
#define prms_1A(...) decl_1A(__VA_ARGS__)

#define base_1B(_d1, I, _1) I                           \
          opts  (_1, arg__value_checker_msg, _d1)
#define decl_1B(_d1, I, _1)                                                                                                                     \
          base_1B(_d1, I##I,                                                                                                                    \
                  type(_1, template<bool arg__has_value_as_member_typedef_, v1::array::Idx arg__idx_, typename arg__element_produced_> class))
#define args_1B(I, _1) base_1B((), I, _1)
#define prms_1B(...) decl_1B((), __VA_ARGS__)

#define base_2A(_d1, args_1A, _1, args_1B)              \
          args_1A                                       \
          opts  (_1, arg__size_checker_msg, _d1)        \
            args_1B
#define decl_2A(_d1, _1B_d1, I, _1A_1, _1A_2, _1, _1B_1)                                                        \
          base_2A(_d1,                                                                                          \
                  prms_1A(I, _1A_1, _1A_2),                                                                     \
                  type(_1, template<bool arg__is_nonnegative_, v1::array::Size arg__actual_parameter_> class),  \
                  decl_1B(_1B_d1, , _1B_1))
#define args_2A(I, _1A_1, _1A_2, _1, _1B_1) base_2A((), args_1A(I, _1A_1, _1A_2), _1, args_1B(, _1B_1))
#define prms_2A(...) decl_2A((), (), __VA_ARGS__)

          template<decl_2A(_d1   (error::utility::list::make::arg_2_is_nonnegative),
                           _1B_d1(error::utility::list::make::element_produced_has_value_as_member_typedef),
                           I, _, _, _, _)>
          struct make
          /* {
           *   typedef tuple::construct<I,
           *                            arg__producer<I, 0>::value, arg__producer<I, 1>::value, ..., arg__producer<I, arg__size - 1>::value> value;
           * }
           */;

#define base_2B(_d1, args_1A, args_1B, _1)      \
          args_1A                               \
          args_1B                               \
          opts  (_1, args__element, _d1)
#define decl_2B(I, _1A_1, _1A_2, _1B_1, _1)     \
          base_2B((),                           \
                  prms_1A(I, _1A_1, _1A_2),     \
                  prms_1B(, _1B_1),             \
                  type(_1, typename...))
#define args_2B(I, _1A_1, _1A_2, _1B_1, _1) base_2B((...), args_1A(I, _1A_1, _1A_2), args_1B(, _1B_1), _1)
#define prms_2B(...) decl_2B(__VA_ARGS__)

#define base_3BA(I, _1, args_2B) I              \
          opts  (_1, arg__has_next, ())         \
            args_2B
#define decl_3BA(I, _1, _1A_1, _1A_2, _1B_1, _2B_1)             \
          base_3BA(I##I,                                        \
                   type(_1, bool),                              \
                   prms_2B(, _1A_1, _1A_2, _1B_1, _2B_1))
#define args_3BA(I, _1, _1A_1, _1A_2, _1B_1, _2B_1) base_3BA(I, _1, args_2B(, _1A_1, _1A_2, _1B_1, _2B_1))
#define prms_3BA(...) decl_3BA(__VA_ARGS__)

          template<prms_3BA(I, _, _, _, _, _)>
          struct make__result;

#define base_3BB(I, _1, args_2B) I              \
          opts  (_1, arg, ())                   \
            args_2B
#define decl_3BB(I, _1, _1A_1, _1A_2, _1B_1, _2B_1)             \
          base_3BB(I##I,                                        \
                   type(_1, typename),                          \
                   prms_2B(, _1A_1, _1A_2, _1B_1, _2B_1))
#define args_3BB(I, _1, _1A_1, _1A_2, _1B_1, _2B_1) base_3BB(I, _1, args_2B(, _1A_1, _1A_2, _1B_1, _2B_1))
#define prms_3BB(...) decl_3BB(__VA_ARGS__)

          template<prms_3BB(I, _, _, _, _, _)>
          struct make__next;

#define base_4BB(args_3BB)                      \
          args_3BB
#define decl_4BB(I, _3BB_1, _1A_1, _1A_2, _1B_1, _2B_1)                 \
          base_4BB(prms_3BB(I, _3BB_1, _1A_1, _1A_2, _1B_1, _2B_1))
#define args_4BB(I, _3BB_1, _1A_1, _1A_2, _1B_1, _2B_1) base_4BB(args_3BB(I, _3BB_1, _1A_1, _1A_2, _1B_1, _2B_1))
#define prms_4BB(...) decl_4BB(__VA_ARGS__)

          template<prms_4BB(I, _, _, _, _, _)>
          struct make__with_value;

          

          template<prms_2A(I, _, _, _, _)>
          struct make :
            arg__size_checker_msg<(arg__size >= 0), arg__size>,
            make__result<args_3BA(I, R(arg__size != 0), _, _, _, X)> {
          };

          

          template<prms_3BA(I, X, _, _, _, _)>
          struct make__result<args_3BA(I, R(false), _, _, _, _)> {
            typedef tuple::construct<I, args__element...> value;
          };

          template<prms_3BA(I, _, _, _, _, _)>
          struct make__result :
            make__next<args_3BB(I, R(arg__producer<I, arg__size - 1>), R(arg__size - 1), _, _, _)> {
          };

          

          template<prms_3BB(I, _, _, _, _, _)>
          struct make__next :
            arg__value_checker_msg<has_value_as_member_typedef<I, arg>::value, arg__size, arg>,
            make__with_value<args_4BB(I, _, _, _, _, _)> {
          };

          

          template<prms_4BB(I, _, _, _, _, _)>
          struct make__with_value :
            make__result<args_3BA(I, R(arg__size != 0), _, _, _, R(typename arg::value, args__element...))> {
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace utility {
        namespace list {

#include "v1_internals_begin.hpp"

#define base_1A(I, _1, _2) I                                    \
          opts  (_1, arg__tuple_construct__prev_list, ())       \
            opts(_2, arg__updater, ())
#define decl_1A(I, _1, _2)                                                                                                              \
          base_1A(I##I,                                                                                                                 \
                  type(_1, typename),                                                                                                   \
                  type(_2, template<H(typename I_,                                                                                      \
                                      v1::array::Idx arg__idx_, typename arg__prev_list_element_, typename arg__updater_data_)> class))
#define args_1A(I, _1, _2) base_1A(I, _1, _2)
#define prms_1A(...) decl_1A(__VA_ARGS__)

#define base_1B(I, _1) I                        \
          opts  (_1, arg__updater_data, ())
#define decl_1B(I, _1)                          \
          base_1B(I##I,                         \
                  type(_1, typename))
#define args_1B(I, _1) base_1B(I, _1)
#define prms_1B(...) decl_1B(__VA_ARGS__)

#define base_1C(_d1, _d2, _d3, I, _1, _2, _3) I                 \
          opts  (_1, arg__data_checker_msg, _d1)                \
            opts(_2, arg__value_checker_msg, _d2)               \
            opts(_3, arg__complete_flag_checker_msg, _d3)
#define decl_1C(_d1, _d2, _d3, I, _1, _2, _3)                                                                                   \
          base_1C(_d1, _d2, _d3, I##I,                                                                                          \
                  type(_1, template<H(bool arg__has_data_as_member_typedef_,                                                    \
                                      v1::array::Idx arg__idx_, typename arg__prev_list_element_, typename arg__updater_data_,  \
                                      typename arg__element_update_)> class),                                                   \
                  type(_2, template<H(bool arg__has_value_as_member_typedef_,                                                   \
                                      v1::array::Idx arg__idx_, typename arg__prev_list_element_, typename arg__updater_data_,  \
                                      typename arg__element_update_)> class),                                                   \
                  type(_3, template<H(bool arg__has_complete_flag_as_constexpr_copyable_data_member_,                           \
                                      v1::array::Idx arg__idx_, typename arg__prev_list_element_, typename arg__updater_data_,  \
                                      typename arg__element_update_)> class))
#define args_1C(I, _1, _2, _3) base_1C((), (), (), I, _1, _2, _3)
#define prms_1C(...) decl_1C((), (), (), __VA_ARGS__)

#define base_2A(_d1, args_1A, args_1B, _1, args_1C)     \
          args_1A                                       \
          args_1B                                       \
          opts  (_1, arg__tuple_checker_msg, _d1)       \
            args_1C
#define decl_2A(_d1, _1C_d1, _1C_d2, _1C_d3, I, _1A_1, _1A_2, _1B_1, _1, _1C_1, _1C_2, _1C_3)                   \
          base_2A(_d1,                                                                                          \
                  prms_1A(I, _1A_1, _1A_2),                                                                     \
                  prms_1B(, _1B_1),                                                                             \
                  type(_1, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class),     \
                  decl_1C(_1C_d1, _1C_d2, _1C_d3, , _1C_1, _1C_2, _1C_3))
#define args_2A(I, _1A_1, _1A_2, _1B_1, _1, _1C_1, _1C_2, _1C_3)                                        \
          base_2A((), args_1A(I, _1A_1, _1A_2), args_1B(, _1B_1), _1, args_1C(, _1C_1, _1C_2, _1C_3))
#define prms_2A(...) decl_2A((), (), (), (), __VA_ARGS__)

          template<decl_2A(_d1   (error::utility::list::update::arg_2_is_tuple_construct),
                           _1C_d1(error::utility::list::update::element_update_has_data_as_member_typedef),
                           _1C_d2(error::utility::list::update::element_update_has_value_as_member_typedef),
                           _1C_d3(error::utility::list::update::element_update_has_complete_flag_as_constexpr_copyable_data_member),
                           I, _, _, _, _, _, _, _)>
          struct update
          /* {
           *   if (tuple::size<I, arg__std_tuple__prev_list>::value == 0) {
           *     typedef arg__updater_data data;
           *     typedef tuple::construct<I> value;
           *   } else {
           *     let tuple::construct<I,
           *       arg__updater<I, 0, typename tuple::get_pos<I, 1, arg__tuple_construct__prev_list>::value,
           *                    arg__updater_data>::value,
           *       arg__updater<I, 1, typename tuple::get_pos<I, 2, arg__tuple_construct__prev_list>::value,
           *                    arg__updater<I, 0, typename tuple::get_pos<I, 1, arg__tuple_construct__prev_list>::value,
           *                                 arg__updater_data>::data>::value,
           *       arg__updater<I, 2, typename tuple::get_pos<I, 3, arg__tuple_construct__prev_list>::value,
           *                    arg__updater<I, 1, typename tuple::get_pos<I, 2, arg__tuple_construct__prev_list>::value,
           *                                 arg__updater<I, 0, typename tuple::get_pos<I, 1, arg__tuple_construct__prev_list>::value,
           *                                              arg__updater_data>::data>::data>::value,
           *       ...
           *       arg__updater<I, n, typename tuple::get_pos<I, n + 1, arg__tuple_construct__prev_list>::value,
           *                    arg__updater<I, n - 1, typename tuple::get_pos<I, n, arg__tuple_construct__prev_list>::value, ...>::data>::value
           *     > processed_elements;
           *     with template<typename I, typename... args> tuple::construct<I, args...> = arg__tuple_construct__prev_list {
           *       let tuple::construct<I, args...@[&](std::size_t i, auto arg__element) {
           *         return {i > n, arg__element};
           *       }> unprocessed_elements;
           *     }
           *
           *     typedef arg__updater<I, n, typename tuple::get_pos<I, n + 1, arg__tuple_construct__prev_list>::value,
           *                          arg__updater<I, n - 1, tuple::get_pos<I, n, arg__tuple_construct__prev_list>, ...>::value::data>::data data;
           *
           *     invariant {
           *       0 <= n && n < tuple::size<I, arg__tuple_construct__prev_list>::value;
           *       for (int i = 1; i <= n; ++i) {
           *         arg__updater<I, i - 1, typename tuple::get_pos<I, i, arg__tuple_construct__prev_list>::value,
           *                      arg__updater<I, i - 2, typename tuple::get_pos<I, i - 1, arg__tuple_construct__prev_list>::value,
           *                                   ...>::data>::is_complete == false;
           *       }
           *       if (n < tuple::size<I, arg__tuple_construct__prev_list>::value - 1) {
           *         arg__updater<I, n, typename tuple::get_pos<I, n + 1, arg__tuple_construct__prev_list>::value,
           *                      arg__updater<I, n - 1, typename tuple::get_pos<I, n, arg__tuple_construct__prev_list>::value,
           *                                   ...>::data>::is_complete == true;
           *       }
           *     }
           *
           *     if (tuple::size<I, unprocessed_elements>::value > 0) {
           *       with template<typename I, typename... args__processed> tuple::construct<I, args__processed...> = processed_elements {
           *         with template<typename I, typename... args__unprocessed> tuple::construct<I, args__unprocessed...> = unprocessed_elements {
           *           typedef tuple::construct<I, args__processed..., args__unprocessed...> value;
           *         }
           *       }
           *     } else {
           *       typedef processed_elements value;
           *     }
           *   }
           * }
           */;

#define base_2B(args_1B, _1)                                    \
          args_1B                                               \
          opts  (_1, arg__tuple_construct__new_list, ())
#define decl_2B(I, _1B_1, _1)                   \
          base_2B(prms_1B(I, _1B_1),            \
                  type(_1, typename))
#define args_2B(I, _1B_1, _1) base_2B(args_1B(I, _1B_1), _1)
#define prms_2B(...) decl_2B(__VA_ARGS__)

          template<prms_2B(I, _, _)>
          struct update__base;

#define base_3B(args_1A, args_2B, args_1C)      \
          args_1A                               \
          args_2B                               \
          args_1C
#define decl_3B(I, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3)     \
          base_3B(prms_1A(I, _1A_1, _1A_2),                             \
                  prms_2B(, _1B_1, _2B_1),                              \
                  prms_1C(, _1C_1, _1C_2, _1C_3))
#define args_3B(I, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3)                                     \
          base_3B(args_1A(I, _1A_1, _1A_2), args_2B(, _1B_1, _2B_1), args_1C(, _1C_1, _1C_2, _1C_3))
#define prms_3B(...) decl_3B(__VA_ARGS__)

#define base_4BA(I, _1, args_3B) I                              \
          opts  (_1, arg__updater_has_no_more_update, ())       \
            args_3B
#define decl_4BA(I, _1, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3)        \
          base_4BA(I##I,                                                        \
                   type(_1, bool),                                              \
                   prms_3B(, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3))
#define args_4BA(I, _1, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3)                \
          base_4BA(I, _1, args_3B(, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3))
#define prms_4BA(...) decl_4BA(__VA_ARGS__)

          template<prms_4BA(I, _, _, _, _, _, _, _, _)>
          struct update__result;

#define base_4BB(I, _1, args_3B) I              \
          opts  (_1, arg__update, ())           \
            args_3B
#define decl_4BB(I, _1, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3)        \
          base_4BB(I##I,                                                        \
                   type(_1, typename),                                          \
                   prms_3B(, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3))
#define args_4BB(I, _1, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3)                \
          base_4BB(I, _1, args_3B(, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3))
#define prms_4BB(...) decl_4BB(__VA_ARGS__)

          template<prms_4BB(I, _, _, _, _, _, _, _, _)>
          struct update__next;

#define base_5BB(args_4BB)                      \
          args_4BB
#define decl_5BB(I, _4BB_1, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3)    \
          prms_4BB(I, _4BB_1, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3)
#define args_5BB(I, _4BB_1, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3)                    \
          base_5BB(args_4BB(I, _4BB_1, _1A_1, _1A_2, _1B_1, _2B_1, _1C_1, _1C_2, _1C_3))
#define prms_5BB(...) decl_5BB(__VA_ARGS__)

          template<prms_5BB(I, _, _, _, _, _, _, _, _)>
          struct update__valid;

          

          template<prms_2A(I, _, _, _, _, _, _, _)>
          struct update :
            arg__tuple_checker_msg<!!always_false<I>(), arg__tuple_construct__prev_list> {
          };

          template<prms_2A(I, R(typename... args__element), _, _, _, _, _, _)>
          struct update<args_2A(I, R(tuple::construct<I, args__element...>), _, _, _, _, _, _)> :
            update__result<args_4BA(I, R(false), R(tuple::construct<I, args__element...>), _, _, R(tuple::construct<I>), _, _, _)> {
          };

          

          template<prms_4BA(I, _, _, _, _, _, _, _, _)>
          struct update__result :
            update__base<args_2B(I, _, _)> {
          };

#define prms_4BA_1(I)                                                                                                   \
          prms_4BA(I, X, R(typename arg__prev_list_element, typename... args__prev_list_remaining_element), _, _,       \
                   R(typename... args__new_list_element), _, _, _)
#define args_4BA_1(I, _1)                                                                                               \
          args_4BA(I, _1, R(tuple::construct<I, arg__prev_list_element, args__prev_list_remaining_element...>), _, _,   \
                   R(tuple::construct<I, args__new_list_element...>), _, _, _)

          template<prms_4BA_1(I)>
          struct update__result<args_4BA_1(I, R(true))> :
            update__base<args_2B(I, _,
                                 R(tuple::construct<I, args__new_list_element..., arg__prev_list_element, args__prev_list_remaining_element...>))> {
          };

          template<prms_4BA_1(I)>
          struct update__result<args_4BA_1(I, R(false))> :
            update__next<args_4BB(I, R(arg__updater<I, sizeof...(args__new_list_element), arg__prev_list_element, arg__updater_data>),
                                  R(tuple::construct<I, arg__prev_list_element, args__prev_list_remaining_element...>), _, _,
                                  R(tuple::construct<I, args__new_list_element...>), _, _, _)> {
          };

          

          template<prms_2B(I, _, _)>
          struct update__base {
            typedef arg__updater_data data;
            typedef arg__tuple_construct__new_list value;
          };

          

          template<prms_4BB(I, _, R(typename arg__prev_list_element, typename... args__prev_list_remaining_element), _, _,
                            R(typename... args__new_list_element), _, _, _)>
          struct update__next<args_4BB(I, _, R(tuple::construct<I, arg__prev_list_element, args__prev_list_remaining_element...>), _, _,
                                       R(tuple::construct<I, args__new_list_element...>), _, _, _)> :
            arg__data_checker_msg<has_data_as_member_typedef<I, arg__update>::value,
                                  sizeof...(args__new_list_element), arg__prev_list_element, arg__updater_data, arg__update>,
            arg__value_checker_msg<has_value_as_member_typedef<I, arg__update>::value,
                                   sizeof...(args__new_list_element), arg__prev_list_element, arg__updater_data, arg__update>,
            arg__complete_flag_checker_msg<validate_is_complete_as_flag<I, arg__update>::value,
                                           sizeof...(args__new_list_element), arg__prev_list_element, arg__updater_data, arg__update>,
            update__valid<args_5BB(I, _, R(tuple::construct<I, arg__prev_list_element, args__prev_list_remaining_element...>), _, _,
                                   R(tuple::construct<I, args__new_list_element...>), _, _, _)> {
          };

          

          template<prms_5BB(I, _, R(typename arg__prev_list_element, typename... args__prev_list_remaining_element), _, _,
                            R(typename... args__new_list_element), _, _, _)>
          struct update__valid<args_5BB(I, _, R(tuple::construct<I, arg__prev_list_element, args__prev_list_remaining_element...>), _, _,
                                        R(tuple::construct<I, args__new_list_element...>), _, _, _)> :
            update__result<args_4BA(I, R(arg__update::is_complete),
                                    R(tuple::construct<I, args__prev_list_remaining_element...>), _, R(typename arg__update::data),
                                    R(tuple::construct<I, args__new_list_element..., typename arg__update::value>), _, _, _)> {
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace utility {
        namespace list {

#include "v1_internals_begin.hpp"

#define base_1(_d3, I, _1, _2, _3) I                    \
          opts  (_1, arg__tuple_construct__list, ())    \
            opts(_2, arg__element_to_append, ())        \
            opts(_3, arg__tuple_checker_msg, _d3)
#define decl_1(_d3, I, _1, _2, _3)                                                                              \
          base_1(_d3, I##I,                                                                                     \
                 type(_1, typename),                                                                            \
                 type(_2, typename),                                                                            \
                 type(_3, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class))
#define args_1(I, _1, _2, _3) base_1((), I, _1, _2, _3)
#define prms_1(...) decl_1((), __VA_ARGS__)

          template<decl_1(_d3(error::utility::list::append::arg_2_is_tuple_construct),
                          I, _, _, _)>
          struct append
          /* {
           *   with template<typename I, typename... args> tuple::construct<I, args...> = arg__tuple_construct__list {
           *     typedef tuple::construct<I, args..., arg__element_to_append> value;
           *   }
           * }
           */;

          

          template<prms_1(I, _, _, _)>
          struct append :
            arg__tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct__list> {
          };

          template<prms_1(I, R(typename... args), _, _)>
          struct append<args_1(I, R(tuple::construct<I, args...>), _, _)>
          {
            typedef tuple::construct<I, args..., arg__element_to_append> value;
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace utility {
        namespace list {

#include "v1_internals_begin.hpp"

#define base_1A(_d4, I, _1, _2, _3, _4) I               \
          opts  (_1, arg__tuple_construct__list, ())    \
            opts(_2, arg__iterator, ())                 \
            opts(_3, arg__iterator_data, ())            \
            opts(_4, arg__tuple_checker_msg, _d4)
#define decl_1A(_d4, I, _1, _2, _3, _4)                                                                                         \
          base_1A(_d4, I##I,                                                                                                    \
                  type(_1, typename),                                                                                           \
                  type(_2, template<H(typename I_,                                                                              \
                                      v1::array::Idx arg__idx_, typename arg__element_, typename arg__iterator_data_)> class),  \
                  type(_3, typename),                                                                                           \
                  type(_4, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class))
#define args_1A(I, _1, _2, _3, _4) base_1A((), I, _1, _2, _3, _4)
#define prms_1A(...) decl_1A((), __VA_ARGS__)

          template<decl_1A(_d4(error::utility::list::iterate::arg_2_is_tuple_construct),
                           I, _, _, _, _)>
          class iterate;

#define base_1B(I, _1, _2, _3) I                \
          opts  (_1, arg__idx_, ())             \
            opts(_2, arg__element_, ())         \
            opts(_3, arg__iterator_data_, ())
#define decl_1B(I, _1, _2, _3)                  \
            base_1B(I##I,                       \
                    type(_1, v1::array::Idx),   \
                    type(_2, typename),         \
                    type(_3, typename))
#define args_1B(I, _1, _2, _3) base_1B(I, _1, _2, _3)
#define prms_1B(...) decl_1B(__VA_ARGS__)

          

          template<prms_1A(I, _, _, _, _)>
          class iterate
          {
            template<prms_1B(I_, _, _, _)>
            struct data_updater
            {
              static constexpr bool is_complete {false};
              typedef typename arg__iterator<args_1B(I_, _, _, _)>::value data;
              typedef arg__element_ value;
            };
            typedef update<args_1A(I, _, R(data_updater), _, X)> result;
          public:
            typedef typename result::data value;
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace utility {
        namespace list {

#include "v1_internals_begin.hpp"

#define base_1(_d1, I, _1) I                    \
          opts  (_1, args, _d1)
#define decl_1(I, _1)                           \
          base_1((), I##I,                      \
                 type(_1, typename...))
#define args_1(I, _1) base_1((...), I, _1)
#define prms_1(...) decl_1(__VA_ARGS__)

          template<prms_1(I, _)>
          struct cat;

#define base_2(I, _1, args_1) I                         \
          opts  (_1, arg__tuple_construct__result, ())  \
            args_1
#define decl_2(I, _1, _1_1)                     \
          base_2(I##I,                          \
                 type(_1, typename),            \
                 prms_1(, _1_1))
#define args_2(I, _1, _1_1) base_2(I, _1, args_1(, _1_1))
#define prms_2(...) decl_2(__VA_ARGS__)

          template<prms_2(I, _, _)>
          struct cat__run;

          

          template<prms_1(I, R(typename arg__result))>
          struct cat<args_1(I, R(arg__result))> {
            typedef tuple::construct<I, arg__result> value;
          };

          template<prms_1(I, R(typename... args__result))>
          struct cat<args_1(I, R(tuple::construct<I, args__result...>))> {
            typedef tuple::construct<I, args__result...> value;
          };

          template<prms_1(I, R(typename arg__result, typename... args))>
          struct cat<args_1(I, R(arg__result, args...))> :
            cat__run<args_2(I, R(tuple::construct<I, arg__result>), _)> {
          };

          template<prms_1(I, R(typename... args__result, typename... args))>
          struct cat<args_1(I, R(tuple::construct<I, args__result...>, args...))> :
            cat__run<args_2(I, R(tuple::construct<I, args__result...>), _)> {
          };

          

          template<prms_2(I, R(typename... args__result), R(typename arg__next))>
          struct cat__run<args_2(I, R(tuple::construct<I, args__result...>), R(arg__next))> {
            typedef tuple::construct<I, args__result..., arg__next> value;
          };

          template<prms_2(I, R(typename... args__result), R(typename... args__next))>
          struct cat__run<args_2(I, R(tuple::construct<I, args__result...>), R(tuple::construct<I, args__next...>))> {
            typedef tuple::construct<I, args__result..., args__next...> value;
          };

          template<prms_2(I, R(typename... args__result, typename arg__next), _)>
          struct cat__run<args_2(I, R(tuple::construct<I, args__result...>, arg__next), _)> :
            cat__run<args_2(I, R(tuple::construct<I, args__result..., arg__next>), _)> {
          };

          template<prms_2(I, R(typename... args__result, typename... args__next), _)>
          struct cat__run<args_2(I, R(tuple::construct<I, args__result...>, tuple::construct<I, args__next...>), _)> :
            cat__run<args_2(I, R(tuple::construct<I, args__result..., args__next...>), _)> {
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace utility {
        namespace error_pos_adaptor {

          template<typename I, v1::error::Pos arg__pos,
                   H(template<bool arg__is_comp_Unit_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
                     class arg__node_computation_checker_msg),
                   H(template<bool arg__is_function_pointer_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
                     class arg__node_computation_fn_ptr_checker_msg),
                   H(template<bool arg__is_std_ratio_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
                     class arg__node_computation_wcet_checker_msg),
                   H(template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
                     class arg__node_computation_wcet_den_checker_msg),
                   H(template<bool arg__is_positive_, v1::error::Pos arg__pos_, typename arg__wcet_>
                     class arg__node_computation_wcet_validator_msg),
                   H(template<bool arg__is_std_ratio_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
                     class arg__node_period_checker_msg),
            H(template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
              class arg__node_period_den_checker_msg),
            H(template<bool arg__is_greater_than_or_equal_, v1::error::Pos arg__pos_, typename arg__period_, typename arg__wcet_>
              class arg__node_period_validator_msg)>
          struct node_msgs
          {
            template<bool arg__is_comp_Unit_, typename arg__actual_parameter_>
            using computation_checker_msg = arg__node_computation_checker_msg<arg__is_comp_Unit_, arg__pos, arg__actual_parameter_>;

            template<bool arg__is_function_pointer_, typename arg__actual_parameter_>
            using computation_fn_ptr_checker_msg = arg__node_computation_fn_ptr_checker_msg<H(arg__is_function_pointer_, arg__pos,
                                                                                              arg__actual_parameter_)>;

            template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
            using computation_wcet_checker_msg = arg__node_computation_wcet_checker_msg<arg__is_std_ratio_, arg__pos, arg__actual_parameter_>;

            template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
            using computation_wcet_den_checker_msg = arg__node_computation_wcet_den_checker_msg<H(arg__has_nonzero_denominator_, arg__pos,
                                                                                                  arg__actual_parameter_)>;

            template<bool arg__is_positive_, typename arg__wcet_>
            using computation_wcet_validator_msg = arg__node_computation_wcet_validator_msg<arg__is_positive_, arg__pos, arg__wcet_>;

            template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
            using period_checker_msg = arg__node_period_checker_msg<arg__is_std_ratio_, arg__pos, arg__actual_parameter_>;

            template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
            using period_den_checker_msg = arg__node_period_den_checker_msg<arg__has_nonzero_denominator_, arg__pos, arg__actual_parameter_>;

            template<bool arg__is_greater_than_or_equal_, typename arg__period_, typename arg__wcet_>
            using period_validator_msg = arg__node_period_validator_msg<arg__is_greater_than_or_equal_, arg__pos, arg__period_, arg__wcet_>;
          };

        }
      }
    }
  }
}

namespace tice {
  namespace v1 {
    namespace utility {

      template<typename arg__lhs_lvalue_type, typename arg__rhs_lvalue_type,
               arg__lhs_lvalue_type *arg__lhs_lvalue, arg__rhs_lvalue_type *arg__rhs_lvalue>
      class Lvalue_comparison :
        public internals::utility::nontype_nontemplate_args_eq<I, arg__lhs_lvalue_type *, arg__rhs_lvalue_type *, arg__lhs_lvalue, arg__rhs_lvalue,
                                                               std::is_function<arg__lhs_lvalue_type>::value ? array::Invalid_idx::value : 0,
                                                               std::is_function<arg__rhs_lvalue_type>::value ? array::Invalid_idx::value : 0> {
      };

      template<typename arg__lhs_array_type, typename arg__rhs_array_type,
               arg__lhs_array_type *arg__lhs_array, arg__rhs_array_type *arg__rhs_array,
               array::Idx arg__idx>
      class Array_comparison_at_a_point :
        private internals::utility::validate_arrays_are_equal_args<I, arg__lhs_array_type, arg__rhs_array_type, arg__idx,
                                                                   error::utility::point_equal_lvalues::arg_1_is_array_type,
                                                                   error::utility::point_equal_lvalues::arg_2_is_array_type,
                                                                   error::utility::point_equal_lvalues::arg_3_is_valid_idx>,
        public internals::utility::nontype_nontemplate_args_eq<I, arg__lhs_array_type *, arg__rhs_array_type *, arg__lhs_array, arg__rhs_array,
                                                               0, 0, arg__idx, arg__idx> {
      };

      template<typename arg__lhs_array_type, typename arg__rhs_array_type,
               arg__lhs_array_type *arg__lhs_array, arg__rhs_array_type *arg__rhs_array,
               array::Idx arg__lhs_idx, array::Idx arg__rhs_idx>
      class Array_comparison_at_a_pair :
        private internals::utility::validate_arrays_are_equal_args<I, arg__lhs_array_type, arg__rhs_array_type, arg__lhs_idx,
                                                                   error::utility::pair_equal_lvalues::arg_1_is_array_type,
                                                                   error::utility::pair_equal_lvalues::arg_2_is_array_type,
                                                                   error::utility::pair_equal_lvalues::arg_3_is_valid_idx,
                                                                   false, arg__rhs_idx,
                                                                   error::utility::pair_equal_lvalues::arg_4_is_valid_idx>,
        public internals::utility::nontype_nontemplate_args_eq<I, arg__lhs_array_type *, arg__rhs_array_type *, arg__lhs_array, arg__rhs_array,
                                                               0, 0, arg__lhs_idx, arg__rhs_idx> {
      };

    }
  }
}
