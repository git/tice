/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace error {
        namespace ete_delay {
          namespace construct {

            template<bool arg__is_Node, typename arg__actual_parameter>
            struct arg_2_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Arg 2 is not class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, typename arg__actual_parameter>
            struct arg_2_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, typename arg__actual_parameter>
            struct arg_2_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, typename arg__actual_parameter>
            struct arg_2_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_2_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, typename arg__wcet>
            struct arg_2_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, typename arg__actual_parameter>
            struct arg_2_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, typename arg__period, typename arg__wcet>
            struct arg_2_arg_3_is_greater_than_or_equal_to_arg_2_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__is_sensor_node, typename arg__node_type>
            struct arg_2_is_sensor_node {
              static_assert(arg__is_sensor_node,
                            "[DEBUG] Arg 2 is not sensor node");
            };

            template<bool arg__is_Node, typename arg__actual_parameter>
            struct arg_3_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Arg 3 is not class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, typename arg__actual_parameter>
            struct arg_3_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, typename arg__actual_parameter>
            struct arg_3_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, typename arg__actual_parameter>
            struct arg_3_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_3_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, typename arg__wcet>
            struct arg_3_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, typename arg__actual_parameter>
            struct arg_3_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_3_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, typename arg__period, typename arg__wcet>
            struct arg_3_arg_3_is_greater_than_or_equal_to_arg_3_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__is_actuator_node, typename arg__node_type>
            struct arg_3_is_actuator_node {
              static_assert(arg__is_actuator_node,
                            "[DEBUG] Arg 3 is not actuator node");
            };

            template<bool arg__is_std_ratio, typename arg__actual_parameter>
            struct arg_4_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Arg 4 is not class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_4_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 4 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_nonnegative, typename arg__actual_parameter>
            struct arg_4_is_nonnegative {
              static_assert(arg__is_nonnegative,
                            "[DEBUG] End-to-end minimum delay is not nonnegative");
            };

            template<bool arg__is_std_ratio, typename arg__actual_parameter>
            struct arg_5_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Arg 5 is not class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_5_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 5 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, typename arg__actual_parameter>
            struct arg_5_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] End-to-end maximum delay is not positive");
            };

            template<bool arg__is_less_than_or_equal_to_arg_5, typename arg__min_delay, typename arg__max_delay>
            struct arg_4_is_less_than_or_equal_to_arg_5 {
              static_assert(arg__is_less_than_or_equal_to_arg_5,
                            "[DEBUG] End-to-end minimum delay is not less than or equal to end-to-end maximum delay");
            };

          }
        }
      }
      namespace ete_delay {

#include "v1_internals_begin.hpp"

#define base_1(_d3, I, _1, _2, _3) I                    \
        opts  (_1, arg__std_ratio__min, ())             \
          opts(_2, arg__std_ratio__max, ())             \
          opts(_3, arg__min_max_checker_msg, _d3)
#define decl_1(_d3, I, _1, _2, _3)                                                                                                              \
        base_1(_d3, I##I,                                                                                                                       \
               type(_1, typename),                                                                                                              \
               type(_2, typename),                                                                                                              \
               type(_3, template<bool arg__is_less_than_or_equal_to_max_delay_, typename arg__min_delay_, typename arg__max_delay_> class))
#define args_1(I, _1, _2, _3) base_1((), I, _1, _2, _3)
#define prms_1(...) decl_1((), __VA_ARGS__)

      template<prms_1(I, _, _, _)>
      struct construct__args_validated;

#define base_2(_d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18, _d19, _d20,     \
               _d21, _d22, _d23, _d24, _d25, _d26, _d27, _d28,                                                          \
               I, _1, _2, args_1, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20,    \
               _21, _22, _23, _24, _25, _26, _27, _28) I                                                                \
        opts  (_1 , arg__Node__sensor, ())                                                                              \
        opts  (_2 , arg__Node__actuator, ())                                                                            \
        args_1                                                                                                          \
        opts  (_3 , arg__2_checker_msg, _d3)                                                                            \
        opts  (_4 , arg__2_validator_msg, _d4)                                                                          \
        opts  (_5 , arg__3_checker_msg, _d5)                                                                            \
        opts  (_6 , arg__3_validator_msg, _d6)                                                                          \
        opts  (_7 , arg__4_checker_msg, _d7)                                                                            \
        opts  (_8 , arg__4_den_checker_msg, _d8)                                                                        \
        opts  (_9 , arg__4_validator_msg, _d9)                                                                          \
        opts  (_10, arg__5_checker_msg, _d10)                                                                           \
        opts  (_11, arg__5_den_checker_msg, _d11)                                                                       \
        opts  (_12, arg__5_validator_msg, _d12)                                                                         \
        opts  (_13, arg__2_computation_checker_msg, _d13)                                                               \
        opts  (_14, arg__2_computation_fn_ptr_checker_msg, _d14)                                                        \
        opts  (_15, arg__2_computation_wcet_checker_msg, _d15)                                                          \
        opts  (_16, arg__2_computation_wcet_den_checker_msg, _d16)                                                      \
        opts  (_17, arg__2_computation_wcet_validator_msg, _d17)                                                        \
        opts  (_18, arg__2_period_checker_msg, _d18)                                                                    \
        opts  (_19, arg__2_period_den_checker_msg, _d19)                                                                \
        opts  (_20, arg__2_period_validator_msg, _d20)                                                                  \
        opts  (_21, arg__3_computation_checker_msg, _d21)                                                               \
        opts  (_22, arg__3_computation_fn_ptr_checker_msg, _d22)                                                        \
        opts  (_23, arg__3_computation_wcet_checker_msg, _d23)                                                          \
        opts  (_24, arg__3_computation_wcet_den_checker_msg, _d24)                                                      \
        opts  (_25, arg__3_computation_wcet_validator_msg, _d25)                                                        \
        opts  (_26, arg__3_period_checker_msg, _d26)                                                                    \
        opts  (_27, arg__3_period_den_checker_msg, _d27)                                                                \
        opts  (_28, arg__3_period_validator_msg, _d28)
#define decl_2(_1_d3, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18, _d19, _d20,      \
               _d21, _d22, _d23, _d24, _d25, _d26, _d27, _d28,                                                                  \
               I, _1, _2, _1_1, _1_2, _1_3, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20,  \
               _21, _22, _23, _24, _25, _26, _27, _28)                                                                          \
        base_2(_d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18, _d19, _d20,             \
               _d21, _d22, _d23, _d24, _d25, _d26, _d27, _d28, I##I,                                                            \
               type(_1, typename),                                                                                              \
               type(_2, typename),                                                                                              \
               decl_1(_1_d3, , _1_1, _1_2, _1_3),                                                                               \
               type(_3, template<bool arg__is_Node_, typename arg__actual_parameter_> class),                                   \
               type(_4, template<bool arg__is_sensor_node_, typename arg__node_type_> class),                                   \
               type(_5, template<bool arg__is_Node_, typename arg__actual_parameter_> class),                                   \
               type(_6, template<bool arg__is_actuator_node_, typename arg__node_type_> class),                                 \
               type(_7, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                              \
               type(_8, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                   \
               type(_9, template<bool arg__is_nonnegative_, typename arg__actual_parameter_> class),                            \
               type(_10, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                             \
               type(_11, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                  \
               type(_12, template<bool arg__is_positive_, typename arg__actual_parameter_> class),                              \
               type(_13, template<bool arg__is_comp_Unit_, typename arg__actual_parameter_> class),                             \
               type(_14, template<bool arg__is_function_pointer_, typename arg__actual_parameter_> class),                      \
               type(_15, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                             \
               type(_16, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                  \
               type(_17, template<bool arg__is_positive_, typename arg__wcet_> class),                                          \
               type(_18, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                             \
               type(_19, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                  \
               type(_20, template<bool arg__is_greater_than_or_equal_, typename arg__period_, typename arg__wcet_> class),      \
               type(_21, template<bool arg__is_comp_Unit_, typename arg__actual_parameter_> class),                             \
               type(_22, template<bool arg__is_function_pointer_, typename arg__actual_parameter_> class),                      \
               type(_23, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                             \
               type(_24, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                  \
               type(_25, template<bool arg__is_positive_, typename arg__wcet_> class),                                          \
               type(_26, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                             \
               type(_27, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                  \
               type(_28, template<bool arg__is_greater_than_or_equal_, typename arg__period_, typename arg__wcet_> class))
#define args_2(I, _1, _2, _1_1, _1_2, _1_3, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20,  \
               _21, _22, _23, _24, _25, _26, _27, _28)                                                                          \
        base_2((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (),                                          \
               (), (), (), (), (), (), (), (), I, _1, _2,                                                                       \
               args_1(, _1_1, _1_2, _1_3), _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20,   \
               _21, _22, _23, _24, _25, _26, _27, _28)
#define prms_2(...) decl_2((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_2(_1_d3(error::ete_delay::construct::arg_4_is_less_than_or_equal_to_arg_5),
                        _d3  (error::ete_delay::construct::arg_2_is_Node),
                        _d4  (error::ete_delay::construct::arg_2_is_sensor_node),
                        _d5  (error::ete_delay::construct::arg_3_is_Node),
                        _d6  (error::ete_delay::construct::arg_3_is_actuator_node),
                        _d7  (error::ete_delay::construct::arg_4_is_std_ratio),
                        _d8  (error::ete_delay::construct::arg_4_has_nonzero_denominator),
                        _d9  (error::ete_delay::construct::arg_4_is_nonnegative),
                        _d10 (error::ete_delay::construct::arg_5_is_std_ratio),
                        _d11 (error::ete_delay::construct::arg_5_has_nonzero_denominator),
                        _d12 (error::ete_delay::construct::arg_5_is_positive),
                        _d13 (error::ete_delay::construct::arg_2_arg_2_is_comp_Unit),
                        _d14 (error::ete_delay::construct::arg_2_arg_2_arg_2_is_function_pointer),
                        _d15 (error::ete_delay::construct::arg_2_arg_2_arg_3_is_Ratio),
                        _d16 (error::ete_delay::construct::arg_2_arg_2_arg_3_has_nonzero_denominator),
                        _d17 (error::ete_delay::construct::arg_2_arg_2_arg_3_is_positive),
                        _d18 (error::ete_delay::construct::arg_2_arg_3_is_std_ratio),
                        _d19 (error::ete_delay::construct::arg_2_arg_3_has_nonzero_denominator),
                        _d20 (error::ete_delay::construct::arg_2_arg_3_is_greater_than_or_equal_to_arg_2_arg_2_arg_3),
                        _d21 (error::ete_delay::construct::arg_3_arg_2_is_comp_Unit),
                        _d22 (error::ete_delay::construct::arg_3_arg_2_arg_2_is_function_pointer),
                        _d23 (error::ete_delay::construct::arg_3_arg_2_arg_3_is_Ratio),
                        _d24 (error::ete_delay::construct::arg_3_arg_2_arg_3_has_nonzero_denominator),
                        _d25 (error::ete_delay::construct::arg_3_arg_2_arg_3_is_positive),
                        _d26 (error::ete_delay::construct::arg_3_arg_3_is_std_ratio),
                        _d27 (error::ete_delay::construct::arg_3_arg_3_has_nonzero_denominator),
                        _d28 (error::ete_delay::construct::arg_3_arg_3_is_greater_than_or_equal_to_arg_3_arg_2_arg_3),
                        I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct
        /* {
         *   typedef arg__Node__sensor sensor;
         *   typedef arg__Node__actuator actuator;
         *   typedef arg__std_ratio__min min;
         *   typedef arg__std_ratio__max max;
         * }
         */;

        

        template<prms_2(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct :
          node::validate_type<I, arg__Node__sensor, v1::node::type::SENSOR, arg__2_validator_msg, arg__2_checker_msg,
                              arg__2_computation_checker_msg, arg__2_computation_fn_ptr_checker_msg, arg__2_computation_wcet_checker_msg,
                              arg__2_computation_wcet_den_checker_msg, arg__2_computation_wcet_validator_msg, arg__2_period_checker_msg,
                              arg__2_period_den_checker_msg, arg__2_period_validator_msg>,
          node::validate_type<I, arg__Node__actuator, v1::node::type::ACTUATOR, arg__3_validator_msg, arg__3_checker_msg,
                              arg__3_computation_checker_msg, arg__3_computation_fn_ptr_checker_msg, arg__3_computation_wcet_checker_msg,
                              arg__3_computation_wcet_den_checker_msg, arg__3_computation_wcet_validator_msg, arg__3_period_checker_msg,
                              arg__3_period_den_checker_msg, arg__3_period_validator_msg>,
          utility::ratio::validate_nonnegative<I, arg__std_ratio__min, arg__4_validator_msg, arg__4_den_checker_msg, arg__4_checker_msg>,
          utility::ratio::validate_positive<I, arg__std_ratio__max, arg__5_validator_msg, arg__5_den_checker_msg, arg__5_checker_msg>,
          construct__args_validated<args_1(I, _, _, _)>
        {
          typedef arg__Node__sensor sensor;
          typedef arg__Node__actuator actuator;
        };

        

        template<prms_1(I, _, _, _)>
        struct construct__args_validated :
          arg__min_max_checker_msg<std::ratio_less_equal<arg__std_ratio__min, arg__std_ratio__max>::value,
                                   arg__std_ratio__min, arg__std_ratio__max>
        {
          typedef arg__std_ratio__min min;
          typedef arg__std_ratio__max max;
        };

#include "v1_internals_end.hpp"
      }
    }
  }
}

namespace tice {
  namespace v1 {

    template<typename arg__Node__sensor, typename arg__Node__actuator,
             typename arg__Ratio__min_delay, typename arg__Ratio__max_delay>
    class ETE_delay :
      public internals::ete_delay::construct<I, arg__Node__sensor, arg__Node__actuator, arg__Ratio__min_delay, arg__Ratio__max_delay,
                                             error::ete_delay::arg_3_is_less_than_or_equal_to_arg_4,
                                             error::ete_delay::arg_1_is_Node,
                                             error::ete_delay::arg_1_is_sensor_node,
                                             error::ete_delay::arg_2_is_Node,
                                             error::ete_delay::arg_2_is_actuator_node,
                                             error::ete_delay::arg_3_is_Ratio,
                                             error::ete_delay::arg_3_has_nonzero_denominator,
                                             error::ete_delay::arg_3_is_nonnegative,
                                             error::ete_delay::arg_4_is_Ratio,
                                             error::ete_delay::arg_4_has_nonzero_denominator,
                                             error::ete_delay::arg_4_is_positive,
                                             error::ete_delay::arg_1_arg_1_is_Comp,
                                             error::ete_delay::arg_1_arg_1_arg_1_is_function_pointer,
                                             error::ete_delay::arg_1_arg_1_arg_2_is_Ratio,
                                             error::ete_delay::arg_1_arg_1_arg_2_has_nonzero_denominator,
                                             error::ete_delay::arg_1_arg_1_arg_2_is_positive,
                                             error::ete_delay::arg_1_arg_2_is_Ratio,
                                             error::ete_delay::arg_1_arg_2_has_nonzero_denominator,
                                             error::ete_delay::arg_1_arg_2_is_greater_than_or_equal_to_arg_1_arg_1_arg_2,
                                             error::ete_delay::arg_2_arg_1_is_Comp,
                                             error::ete_delay::arg_2_arg_1_arg_1_is_function_pointer,
                                             error::ete_delay::arg_2_arg_1_arg_2_is_Ratio,
                                             error::ete_delay::arg_2_arg_1_arg_2_has_nonzero_denominator,
                                             error::ete_delay::arg_2_arg_1_arg_2_is_positive,
                                             error::ete_delay::arg_2_arg_2_is_Ratio,
                                             error::ete_delay::arg_2_arg_2_has_nonzero_denominator,
                                             error::ete_delay::arg_2_arg_2_is_greater_than_or_equal_to_arg_2_arg_1_arg_2> {
    };

  }
}
