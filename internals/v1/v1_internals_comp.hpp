/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace error {
        namespace comp {
          namespace construct {

            template<bool arg__is_function_pointer, typename arg__actual_parameter>
            struct arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_std_ratio, typename arg__actual_parameter>
            struct arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] WCET is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] WCET is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, typename arg__wcet>
            struct arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] WCET is not positive");
            };

          }
        }
      }
      namespace comp {

#include "v1_internals_begin.hpp"

#define base_1A(I, _1) I                                                \
        opts  (_1, arg__std_integral_constant__function_ptr, ())
#define decl_1A(I, _1)                          \
        base_1A(I##I,                           \
                type(_1, typename))
#define args_1A(I, _1) base_1A(I, _1)
#define prms_1A(...) decl_1A(__VA_ARGS__)

        template<prms_1A(I, _)>
        struct construct__parse_arg_1__base;

#define base_1B(_d1, I, _1) I                           \
        opts  (_1, arg__function_ptr_checker_msg, _d1)
#define decl_1B(_d1, I, _1)                                                                                     \
        base_1B(_d1, I##I,                                                                                      \
                type(_1, template<bool arg__is_function_pointer_, typename arg__actual_parameter_> class))
#define args_1B(I, _1) base_1B((), I, _1)
#define prms_1B(...) decl_1B((), __VA_ARGS__)

#define base_2A(args_1A, args_1B)               \
        args_1A                                 \
        args_1B
#define decl_2A(I, _1A_1, _1B_1)                \
        prms_1A(I, _1A_1)                       \
        prms_1B(, _1B_1)
#define args_2A(I, _1A_1, _1B_1) base_2A(args_1A(I, _1A_1), args_1B(, _1B_1))
#define prms_2A(...) decl_2A(__VA_ARGS__)

        template<prms_2A(I, _, _)>
          struct construct__parse_arg_1;

#define base_1C(I, _1) I                        \
        opts  (_1, arg__std_ratio__wcet, ())
#define decl_1C(I, _1)                          \
        base_1C(I##I,                           \
                type(_1, typename))
#define args_1C(I, _1) base_1C(I, _1)
#define prms_1C(...) decl_1C(__VA_ARGS__)

        template<prms_1C(I, _)>
        struct construct__parse_arg_2;

#define base_2B(_d1, _d2, _d3, args_1A, args_1C, args_1B, _1, _2, _3)   \
        args_1A                                                         \
        args_1C                                                         \
        args_1B                                                         \
        opts  (_1, arg__ratio_checker_msg, _d1)                         \
          opts(_2, arg__ratio_den_checker_msg, _d2)                     \
          opts(_3, arg__wcet_validator_msg, _d3)
#define decl_2B(_1B_d1, _d1, _d2, _d3, I, _1A_1, _1C_1, _1B_1, _1, _2, _3)                                      \
        base_2B(_d1, _d2, _d3,                                                                                  \
                prms_1A(I, _1A_1),                                                                              \
                prms_1C(, _1C_1),                                                                               \
                decl_1B(_1B_d1, , _1B_1),                                                                       \
                type(_1, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),             \
                type(_2, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),  \
                type(_3, template<bool arg__is_positive_, typename arg__wcet_> class))
#define args_2B(I, _1A_1, _1C_1, _1B_1, _1, _2, _3) base_2B((), (), (), args_1A(I, _1A_1), args_1C(, _1C_1), args_1B(, _1B_1), _1, _2, _3)
#define prms_2B(...) decl_2B((), (), (), (), __VA_ARGS__)

        template<decl_2B(_1B_d1(error::comp::construct::arg_2_is_function_pointer),
                         _d1   (error::comp::construct::arg_3_is_std_ratio),
                         _d2   (error::comp::construct::arg_3_has_nonzero_denominator),
                         _d3   (error::comp::construct::arg_3_is_positive),
                         I, _, _, _, _, _, _)>
        struct construct
        /* {
         *   typedef (arg__std_integral_constant__function_ptr == template<typename arg__T, typename... args__T, auto arg__ptr>
         *                                                        std::integral_constant<void(*)(arg__T, args__T...), arg__ptr>
         *            ? v1::node::type::ACTUATOR
         *            : arg__std_integral_constant__function_ptr == template<typename arg__R, auto arg__ptr>
         *                                                          std::integral_constant<arg__R(*)(), arg__ptr>
         *            ? v1::node::type::SENSOR
         *            : v1::node::type::INTERMEDIARY) type;
         *   typedef utility::fn_info<I, arg__std_integral_constant__function_ptr> fn_info;
         *   typedef arg__std_ratio__wcet wcet;
         * }
         */;

        

        template<prms_2B(I, _, _, _, _, _, _)>
        struct construct :
          construct__parse_arg_1<args_2A(I, _, _)>,
          utility::ratio::validate_positive<I, arg__std_ratio__wcet, arg__wcet_validator_msg, arg__ratio_den_checker_msg, arg__ratio_checker_msg>,
          construct__parse_arg_2<args_1C(I, _)> {
        };

        

        template<prms_2A(I, _, _)>
        struct construct__parse_arg_1 :
          arg__function_ptr_checker_msg<!!utility::always_false<I>(), typename arg__std_integral_constant__function_ptr::value_type> {
        };

        template<prms_2A(I, R(typename arg__return_type, typename arg__param_type, typename... args__param_type,
                              arg__return_type(*arg__function_ptr)(arg__param_type, args__param_type...)), _)>
        struct construct__parse_arg_1<args_2A(I, R(std::integral_constant<arg__return_type(*)(arg__param_type, args__param_type...),
                                                   arg__function_ptr>), _)> :
          construct__parse_arg_1__base<args_1A(I, R(std::integral_constant<arg__return_type(*)(arg__param_type, args__param_type...),
                                                    arg__function_ptr>))>
        {
          typedef v1::node::type::INTERMEDIARY type;
        };

        template<prms_2A(I, R(typename arg__return_type, arg__return_type(*arg__function_ptr)()), _)>
        struct construct__parse_arg_1<args_2A(I, R(std::integral_constant<arg__return_type(*)(), arg__function_ptr>), _)> :
          construct__parse_arg_1__base<args_1A(I, R(std::integral_constant<arg__return_type(*)(), arg__function_ptr>))>
        {
          typedef v1::node::type::SENSOR type;
        };

        template<prms_2A(I, R(typename arg__param_type, typename... args__param_type,
                              void (*arg__function_ptr)(arg__param_type, args__param_type...)), _)>
        struct construct__parse_arg_1<args_2A(I, R(std::integral_constant<void(*)(arg__param_type, args__param_type...), arg__function_ptr>), _)> :
          construct__parse_arg_1__base<args_1A(I, R(std::integral_constant<void(*)(arg__param_type, args__param_type...), arg__function_ptr>))>
        {
          typedef v1::node::type::ACTUATOR type;
        };

        

        template<prms_1A(I, _)>
        struct construct__parse_arg_1__base {
          typedef utility::fn_info<I, arg__std_integral_constant__function_ptr> fn_info;
        };

        

        template<prms_1C(I, _)>
        struct construct__parse_arg_2
        {
          typedef arg__std_ratio__wcet wcet;
        };

#include "v1_internals_end.hpp"
      }
    }
  }
}

namespace tice {
  namespace v1 {
    namespace comp {

      template<typename arg__std_integral_constant__function_ptr, typename arg__Ratio__wcet>
      class Unit :
        public internals::comp::construct<I, arg__std_integral_constant__function_ptr, arg__Ratio__wcet,
                                          error::comp::arg_1_is_function_pointer,
                                          error::comp::arg_2_is_Ratio,
                                          error::comp::arg_2_has_nonzero_denominator,
                                          error::comp::arg_2_is_positive> {
      };

    }
  }
}
