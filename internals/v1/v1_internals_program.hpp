/******************************************************************************
 * Copyright (C) 2018-2020  Tadeus Prastowo <0x66726565@gmail.com>            *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace error {
        namespace program {
          namespace parser_remaining_args {

            template<bool arg__is_tuple_construct, typename arg__actual_parameter>
            struct arg_2_is_tuple_construct {
              static_assert(arg__is_tuple_construct,
                            "[DEBUG] Arg 2 is not class template tice::v1::internals::tuple::construct");
            };

            template<bool arg__is_valid, v1::error::Pos arg__actual_parameter, bool arg__2_is_empty_tuple>
            struct arg_3_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Arg 3 is either not tice::v1::error::Invalid_pos::value when arg 2 is an empty"
                            " tice::v1::internals::tuple::construct or tice::v1::error::Invalid_pos::value when arg 2 is a nonempty"
                            " tice::v1::internals::tuple::construct");
            };

          }
          namespace node_id_test_array {

            template<bool arg__is_positive, v1::array::Size arg__size>
            struct arg_2_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Arg 2 is not positive");
            };

            template<bool arg__is_tuple_construct, typename arg__actual_parameter>
            struct arg_3_is_tuple_construct {
              static_assert(arg__is_tuple_construct,
                            "[DEBUG] Arg 3 is not class template tice::v1::internals::tuple::construct");
            };

            template<bool arg__is_tuple_construct_of_dict_Entry_key, v1::error::Pos arg__dict_Entry_key_pos, typename arg__actual_parameter>
            struct arg_3_is_tuple_construct_of_dict_Entry_key {
              static_assert(arg__is_tuple_construct_of_dict_Entry_key,
                            "[DEBUG] Element is not class template tice::v1::internals::program::dict_Entry_key");
            };

          }
          namespace node_id_array {

            template<bool arg__is_array_of_bool, typename arg__array_element_type>
            struct arg_2_is_array_of_bool {
              static_assert(arg__is_array_of_bool,
                            "[DEBUG] Array element type is not bool");
            };

            template<bool arg__has_element_type_as_member_typedef, typename arg__actual_parameter>
            struct arg_2_has_element_type_as_member_typedef {
              static_assert(arg__has_element_type_as_member_typedef,
                            "[DEBUG] Arg 2 has no member typedef `element_type'"
                            " (maybe not using tice::v1::internals::program::node_id_test_array in the first place)");
            };

            template<bool arg__has_size_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_size_as_constexpr_copyable_data_member {
              static_assert(arg__has_size_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                            " (maybe not using tice::v1::internals::program::node_id_test_array in the first place)");
            };

            template<bool arg__has_type_as_member_typedef, typename arg__actual_parameter>
            struct arg_2_has_type_as_member_typedef {
              static_assert(arg__has_type_as_member_typedef,
                            "[DEBUG] Arg 2 has no member typedef `type'"
                            " (maybe not using tice::v1::internals::program::node_id_test_array in the first place)");
            };

            template<bool arg__has_elems_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_elems_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                            " (maybe not using tice::v1::internals::program::node_id_test_array in the first place)");
            };

            template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_elems_ptr_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                            " (maybe not using tice::v1::internals::program::node_id_test_array in the first place)");
            };

            template<bool arg__is_valid, typename arg__element_type>
            struct arg_2_element_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's member typedef `element_type' is not valid array element type"
                            " (maybe not using tice::v1::internals::program::node_id_test_array in the first place)");
            };

            template<bool arg__is_valid, typename arg__size_type>
            struct arg_2_size_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's data member `size' is not `const tice::v1::array::Idx'"
                            " (maybe not using tice::v1::internals::program::node_id_test_array in the first place)");
            };

            template<bool arg__is_valid, typename arg__type>
            struct arg_2_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's member typedef `type' is not `element_type[size]'"
                            " (maybe not using tice::v1::internals::program::node_id_test_array in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_type>
            struct arg_2_elems_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's data member `elems' is not `const element_type[size]'"
                            " (maybe not using tice::v1::internals::program::node_id_test_array in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_ptr_type>
            struct arg_2_elems_ptr_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's data member `elems_ptr' is not `const element_type *[]'"
                            " (maybe not using tice::v1::internals::program::node_id_test_array in the first place)");
            };

            template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
            struct arg_2_elems_ptr_points_to_elems {
              static_assert(arg__points_to_elems,
                            "[DEBUG] Arg 2's data member `elems_ptr' fails to point to data member `elems' at index 0"
                            " (maybe not using tice::v1::internals::program::node_id_test_array in the first place)");
            };

          }
          namespace parse_nodes {

            template<bool arg__are_distinct, v1::error::Pos arg__node_1_pos, v1::error::Pos arg__node_2_pos>
            struct nodes_are_distinct {
              static_assert(arg__are_distinct,
                            "[DEBUG] Duplicated nodes are not allowed");
            };

            template<bool arg__is_tuple_construct, typename arg__actual_parameter>
            struct arg_3_is_tuple_construct {
              static_assert(arg__is_tuple_construct,
                            "[DEBUG] Arg 3 is not class template tice::v1::internals::tuple::construct");
            };

            template<bool arg__is_nonempty_tuple_construct>
            struct arg_3_is_nonempty_tuple_construct {
              static_assert(arg__is_nonempty_tuple_construct,
                            "[DEBUG] Arg 3 is not tice::v1::internals::tuple::construct with at least one element");
            };

            template<bool arg__is_Node, typename arg__actual_parameter>
            struct first_element_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] First element of arg 3 is not class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, v1::error::Pos arg__pos, typename arg__wcet>
            struct node_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, v1::error::Pos arg__pos, typename arg__period, typename arg__wcet>
            struct node_arg_3_is_greater_than_or_equal_to_node_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__are_equal_and_positive, v1::array::Size arg__node_dict_size, v1::array::Size arg__sensor_node_test_array_size,
                     v1::array::Size arg__intermediary_node_test_array_size, v1::array::Size arg__actuator_node_test_array_size,
                     v1::array::Size arg__sensor_node_id_array_size, v1::array::Size arg__intermediary_node_id_array_size,
                     v1::array::Size arg__actuator_node_id_array_size>
            struct sizes_of_node_dict_and_test_and_id_arrays_are_equal_and_positive {
              static_assert(arg__are_equal_and_positive,
                            "[DEBUG] Either sensor, intermediary, and actuator node test and the sum of ID array sizes fail to match node dict size"
                            " or all the sizes are not positive");
            };

            template<bool arg__is_either_sensor_or_intermediary_or_actuator, v1::error::Pos arg__node_pos>
            struct node_type_is_either_sensor_or_intermediary_or_actuator {
              static_assert(arg__is_either_sensor_or_intermediary_or_actuator,
                            "[DEBUG] Node is not categorized as a sensor/intermediary/actuator");
            };

            template<bool arg__is_not_both_sensor_and_intermediary, v1::error::Pos arg__node_pos>
            struct node_type_is_not_both_sensor_and_intermediary {
              static_assert(arg__is_not_both_sensor_and_intermediary,
                            "[DEBUG] Node categorization is not disjoint");
            };

            template<bool arg__is_not_both_sensor_and_actuator, v1::error::Pos arg__node_pos>
            struct node_type_is_not_both_sensor_and_actuator {
              static_assert(arg__is_not_both_sensor_and_actuator,
                            "[DEBUG] Node categorization is not disjoint");
            };

            template<bool arg__is_not_both_intermediary_and_actuator, v1::error::Pos arg__node_pos>
            struct node_type_is_not_both_intermediary_and_actuator {
              static_assert(arg__is_not_both_intermediary_and_actuator,
                            "[DEBUG] Node categorization is not disjoint");
            };

            template<bool arg__is_in_sensor_test_array, v1::dict::Key arg__node_id>
            struct sensor_node_id_is_in_sensor_test_array {
              static_assert(arg__is_in_sensor_test_array,
                            "[DEBUG] Node ID array disagrees with its test array");
            };

            template<bool arg__is_in_intermediary_test_array, v1::dict::Key arg__node_id>
            struct intermediary_node_id_is_in_intermediary_test_array {
              static_assert(arg__is_in_intermediary_test_array,
                            "[DEBUG] Node ID array disagrees with its test array");
            };

            template<bool arg__is_in_actuator_test_array, v1::dict::Key arg__node_id>
            struct actuator_node_id_is_in_actuator_test_array {
              static_assert(arg__is_in_actuator_test_array,
                            "[DEBUG] Node ID array disagrees with its test array");
            };

          }
          namespace get_node_id {

            template<bool arg__has_been_specified, v1::error::Pos arg__holder_pos, v1::error::Pos arg__node_pos>
            struct node_has_been_specified {
              static_assert(arg__has_been_specified,
                            "[DEBUG] tice::v1::Node specified as an argument to a holder"
                            " (tice::v1::Feeder or tice::v1::ETE_delay or tice::v1::Correlation)"
                            " is not specified as an argument to tice::v1::Program and so is not assigned an ID");
            };

            template<bool arg__is_tuple_construct, typename arg__actual_parameter>
            struct arg_4_is_tuple_construct {
              static_assert(arg__is_tuple_construct,
                            "[DEBUG] Arg 4 is not class template tice::v1::internals::tuple::construct");
            };

            template<bool arg__is_nonempty_tuple_construct>
            struct arg_4_is_nonempty_tuple_construct {
              static_assert(arg__is_nonempty_tuple_construct,
                            "[DEBUG] Arg 4 is not tice::v1::internals::tuple::construct with at least one element");
            };

            template<bool arg__is_tuple_construct_of_dict_Entry, typename arg__actual_parameter>
            struct arg_4_is_tuple_construct_of_dict_Entry {
              static_assert(arg__is_tuple_construct_of_dict_Entry,
                            "[DEBUG] Arg 4 is not tice::v1::internals::tuple::construct whose elements are class templates"
                            " tice::v1::internals::program::dict_Entry");
            };

            template<bool arg__is_tuple_construct_of_dict_Entry_of_Node, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct arg_4_is_tuple_construct_of_dict_Entry_of_Node {
              static_assert(arg__is_tuple_construct_of_dict_Entry_of_Node,
                            "[DEBUG] Element is not class template tice::v1::internals::program::dict_Entry that carries"
                            " class template tice::v1::Node");
            };

            template<bool arg__is_Node, typename arg__actual_parameter>
            struct arg_5_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Arg 5 is not class template tice::v1::Node");
            };

          }
          namespace Channel {

            template<bool arg__is_Chan_inlit_or_Chan, typename arg__actual_parameter>
            struct arg_3_is_either_Chan_inlit_or_Chan {
              static_assert(arg__is_Chan_inlit_or_Chan,
                            "[DEBUG] Arg 3 is neither class template tice::v1::Chan nor class template tice::v1::Chan_inlit");
            };

          }
          namespace parse_feeders {

            template<bool arg__has_no_cycle, typename arg__node_pos_forming_closed_path>
            struct graph_has_no_cycle {
              static_assert(arg__has_no_cycle,
                            "[DEBUG] Tice graph cannot have a cycle");
            };

            template<bool arg__is_missing, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos>
            struct node_element_is_missing {
              static_assert(arg__is_missing,
                            "[DEBUG] Tuple ends when an element of type tice::v1::Node is expected");
            };

            template<bool arg__is_producer_node, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_element>
            struct element_is_producer_node {
              static_assert(arg__is_producer_node,
                            "[DEBUG] Producer node element is not specified using class template tice::v1::Node");
            };

            template<bool arg__is_consumer_node, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_element>
            struct element_is_consumer_node {
              static_assert(arg__is_consumer_node,
                            "[DEBUG] Consumer node element is not specified using class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__wcet>
            struct node_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos,
                     typename arg__period, typename arg__wcet>
            struct node_arg_3_is_greater_than_or_equal_to_node_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__is_equal, v1::error::Pos arg__feeder_pos, unsigned arg__channel_count, unsigned arg__parameter_count>
            struct channel_and_consumer_parameter_counts_are_equal {
              static_assert(arg__is_equal,
                            "[DEBUG] Consumer's parameter count and incoming arc count differ");
            };

            template<bool arg__is_callable, v1::error::Pos arg__feeder_pos, typename arg__consumer_function>
            struct consumer_function_is_callable {
              static_assert(arg__is_callable,
                            "[DEBUG] Consumer function is not callable");
            };

            template<bool arg__is_missing, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos>
            struct channel_element_is_missing {
              static_assert(arg__is_missing,
                            "[DEBUG] Tuple ends when an element of type tice::v1::Chan_inlit or tice::v1::Chan is expected");
            };

            template<bool arg__is_channel, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_element>
            struct element_is_channel {
              static_assert(arg__is_channel,
                            "[DEBUG] Channel element is not specified using class template tice::v1::Chan_inlit or tice::v1::Chan");
            };

            template<bool arg__is_object_type, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct channel_arg_2_is_object_type {
              static_assert(arg__is_object_type,
                            "[DEBUG] Channel type is not an object type (neither void nor reference type nor function type)");
            };

            template<bool arg__is_cv_unqualified, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct channel_arg_2_is_cv_unqualified {
              static_assert(arg__is_cv_unqualified,
                            "[DEBUG] Channel type cannot be cv-qualified (qualified with either const or volatile or both)");
            };

            template<bool arg__is_not_nullptr, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos>
            struct channel_arg_3_is_not_nullptr {
              static_assert(arg__is_not_nullptr,
                            "[DEBUG] Channel initial value pointer cannot be null");
            };

            template<bool arg__is_assignable, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__producer_pos, v1::error::Pos arg__channel_pos,
                     typename arg__return_type, typename arg__channel_type>
            struct producer_return_value_is_assignable_to_channel {
              static_assert(arg__is_assignable,
                            "[DEBUG] Producer's return value is not assignable to channel");
            };

            template<bool arg__can_initialize, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__channel_pos,
                     typename arg__init_val_type, typename arg__channel_type>
            struct init_val_can_initialize_channel {
              static_assert(arg__can_initialize,
                            "[DEBUG] Initial value cannot initialize channel");
            };

            template<bool arg__is_compatible, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__channel_pos, v1::error::Pos arg__parameter_pos,
                     typename arg__channel_type, typename arg__parameter_type>
            struct channel_and_consumer_parameter_are_compatible {
              static_assert(arg__is_compatible,
                            "[DEBUG] Channel cannot initialize consumer's corresponding parameter");
            };

            template<bool arg__has_no_multiple_edges, v1::error::Pos arg__feeder_pos,
                     v1::error::Pos arg__producer_1_pos, v1::error::Pos arg__producer_2_pos>
            struct graph_has_no_multiple_edges {
              static_assert(arg__has_no_multiple_edges,
                            "[DEBUG] Tice graph cannot have multiple edges");
            };

            template<bool arg__has_no_loop, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__producer_pos, v1::error::Pos arg__consumer_pos>
            struct graph_has_no_loop {
              static_assert(arg__has_no_loop,
                            "[DEBUG] Tice graph cannot have a loop");
            };

            template<bool arg__are_distinct, v1::error::Pos arg__feeder_1_pos, v1::error::Pos arg__feeder_2_pos>
            struct feeders_are_distinct {
              static_assert(arg__are_distinct,
                            "[DEBUG] Two feeders are not allowed to specify the same consumer");
            };

            template<bool arg__has_been_specified, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__node_pos>
            struct node_has_been_specified {
              static_assert(arg__has_been_specified,
                            "[DEBUG] tice::v1::Node specified as an argument to tice::v1::Feeder"
                            " is not specified as an argument to tice::v1::Program and so is not assigned an ID");
            };

            template<bool arg__is_tuple_construct, typename arg__actual_parameter>
            struct arg_2_is_tuple_construct {
              static_assert(arg__is_tuple_construct,
                            "[DEBUG] Arg 2 is not class template tice::v1::internals::tuple::construct");
            };

            template<bool arg__is_nonempty_tuple_construct>
            struct arg_2_is_nonempty_tuple_construct {
              static_assert(arg__is_nonempty_tuple_construct,
                            "[DEBUG] Arg 2 is not tice::v1::internals::tuple::construct with at least one element");
            };

            template<bool arg__is_tuple_construct_of_dict_Entry_of_Node, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct arg_2_is_tuple_construct_of_dict_Entry_of_Node {
              static_assert(arg__is_tuple_construct_of_dict_Entry_of_Node,
                            "[DEBUG] Element is not class template tice::v1::internals::program::dict_Entry that carries"
                            " class template tice::v1::Node");
            };

            template<bool arg__has_linearly_increasing_node_ids_starting_at_zero_with_gradient_one,
                     v1::error::Pos arg__element_pos, v1::dict::Key arg__actual_node_id>
            struct arg_2_has_linearly_increasing_node_ids_starting_at_zero_with_gradient_one {
              static_assert(arg__has_linearly_increasing_node_ids_starting_at_zero_with_gradient_one,
                            "[DEBUG] Arg 2 may use a different node ID assignment scheme because the actual node ID fails to satisfy the equation"
                            " element-pos = node-id + 1");
            };

            template<bool arg__is_tuple_construct, typename arg__actual_parameter>
            struct arg_3_is_tuple_construct {
              static_assert(arg__is_tuple_construct,
                            "[DEBUG] Arg 3 is not class template tice::v1::internals::tuple::construct");
            };

          }
          namespace construct {

            template<bool arg__is_HW, typename arg__actual_parameter>
            struct arg_2_is_HW {
              static_assert(arg__is_HW,
                            "[DEBUG] Arg 2 is not class template tice::v1::HW");
            };

            template<bool arg__is_Core_ids, typename arg__actual_parameter>
            struct arg_2_arg_2_is_Core_ids {
              static_assert(arg__is_Core_ids,
                            "[DEBUG] Processor core IDs are not specified using class template tice::v1::Core_ids");
            };

            template<bool arg__are_not_duplicated, v1::error::Pos arg__core_id_1_pos, v1::error::Pos arg__core_id_2_pos, int arg__core_id>
            struct arg_2_arg_2_core_ids_are_not_duplicated {
              static_assert(arg__are_not_duplicated,
                            "[DEBUG] Processor core IDs are not pairwise distinct");
            };

            template<bool arg__is_Node, typename arg__actual_parameter>
            struct arg_3_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Arg 3 is not class template tice::v1::Node");
            };

            template<bool arg__is_followed_by_sequence_of_Feeder, v1::error::Pos arg__pos, typename arg__follower>
            struct sequence_of_Node_is_followed_only_by_sequence_of_Feeder {
              static_assert(arg__is_followed_by_sequence_of_Feeder,
                            "[DEBUG]"
                            " Sequence of tice::v1::Node class templates can only be followed by sequence of tice::v1::Feeder class templates");
            };

            template<bool arg__is_followed_only_by_sequence_of_ETE_delay_or_Correlation, v1::error::Pos arg__pos, typename arg__follower>
            struct sequence_of_Feeder_is_followed_only_by_sequence_of_ETE_delay_or_Correlation {
              static_assert(arg__is_followed_only_by_sequence_of_ETE_delay_or_Correlation,
                            "[DEBUG] Sequence of tice::v1::Feeder class templates can only be followed by sequence of"
                            " either tice::v1::ETE_delay or tice::v1::Correlation class templates");
            };

            template<bool arg__is_followed_only_by_sequence_of_Correlation, v1::error::Pos arg__pos, typename arg__follower>
            struct sequence_of_ETE_delay_is_followed_only_by_sequence_of_Correlation {
              static_assert(arg__is_followed_only_by_sequence_of_Correlation,
                            "[DEBUG] Sequence of tice::v1::ETE_delay class templates can only be followed by sequence of"
                            " tice::v1::Correlation class templates");
            };

            template<bool arg__is_followed_by_nothing, v1::error::Pos arg__pos, typename arg__follower>
            struct sequence_of_Correlation_is_followed_by_nothing {
              static_assert(arg__is_followed_by_nothing,
                            "[DEBUG] Sequence of tice::v1::Correlation class templates cannot be followed by anything else");
            };

            template<bool arg__is_comp_Unit, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, v1::error::Pos arg__pos, typename arg__wcet>
            struct node_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, v1::error::Pos arg__pos, typename arg__period, typename arg__wcet>
            struct node_arg_3_is_greater_than_or_equal_to_node_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__are_distinct, v1::error::Pos arg__node_1_pos, v1::error::Pos arg__node_2_pos>
            struct nodes_are_distinct {
              static_assert(arg__are_distinct,
                            "[DEBUG] Duplicated nodes are not allowed");
            };

            template<bool arg__is_already_specified,
                     v1::error::Pos arg__feeder_or_ete_delay_or_correlation_pos, v1::error::Pos arg__node_pos_in_feeder_or_ete_delay_or_correlation>
            struct node_is_already_specified {
              static_assert(arg__is_already_specified,
                            "[DEBUG] Feeder or ETE_delay or Correlation refers to a node that is not included in the arguments to Program");
            };

            template<bool arg__is_producer_node_id, v1::error::Pos arg__ete_delay_or_correlation_pos,
                     v1::error::Pos arg__node_pos_in_ete_delay_or_correlation, v1::dict::Key arg__node_id>
            struct node_id_is_producer_node_id {
              static_assert(arg__is_producer_node_id,
                            "[DEBUG]"
                            " ETE_delay or Correlation has a node whose ID according to the result of parsing feeders is not a producer node ID"
                            " (if the node position is stated as zero (i.e., an invalid position), the node comprises a path that is being"
                            " traversed from the sensor node to the actuator node specified in the ETE_delay or Correlation)");
            };

            template<bool arg__is_sink_node_id, v1::error::Pos arg__ete_delay_or_correlation_pos,
                     v1::error::Pos arg__node_pos_in_ete_delay_or_correlation, v1::dict::Key arg__node_id>
            struct node_id_is_sink_node_id {
              static_assert(arg__is_sink_node_id,
                            "[DEBUG]"
                            " ETE_delay or Correlation has a node whose ID according to the result of parsing feeders is not a sink node ID");
            };

            template<bool arg__are_distinct, v1::error::Pos arg__feeder_1_pos, v1::error::Pos arg__feeder_2_pos>
            struct feeders_are_distinct {
              static_assert(arg__are_distinct,
                            "[DEBUG] Two feeders are not allowed to specify the same consumer");
            };

            template<bool arg__has_no_cycle, typename arg__node_pos_forming_closed_path>
            struct graph_has_no_cycle {
              static_assert(arg__has_no_cycle,
                            "[DEBUG] Tice graph cannot have a cycle");
            };

            template<bool arg__is_missing, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos>
            struct feeder_node_element_is_missing {
              static_assert(arg__is_missing,
                            "[DEBUG] Tuple ends when an element of type tice::v1::Node is expected");
            };

            template<bool arg__is_producer_node, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_element>
            struct feeder_element_is_producer_node {
              static_assert(arg__is_producer_node,
                            "[DEBUG] Producer node element is not specified using class template tice::v1::Node");
            };

            template<bool arg__is_consumer_node, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_element>
            struct feeder_element_is_consumer_node {
              static_assert(arg__is_consumer_node,
                            "[DEBUG] Consumer node element is not specified using class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct feeder_node_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct feeder_node_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct feeder_node_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct feeder_node_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__wcet>
            struct feeder_node_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct feeder_node_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct feeder_node_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos,
                     typename arg__period, typename arg__wcet>
            struct feeder_node_arg_3_is_greater_than_or_equal_to_node_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__is_equal, v1::error::Pos arg__feeder_pos, unsigned arg__channel_count, unsigned arg__parameter_count>
            struct feeder_channel_and_consumer_parameter_counts_are_equal {
              static_assert(arg__is_equal,
                            "[DEBUG] Consumer's parameter count and incoming arc count differ");
            };

            template<bool arg__is_callable, v1::error::Pos arg__feeder_pos, typename arg__consumer_function>
            struct feeder_consumer_function_is_callable {
              static_assert(arg__is_callable,
                            "[DEBUG] Consumer function is not callable");
            };

            template<bool arg__is_missing, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos>
            struct feeder_channel_element_is_missing {
              static_assert(arg__is_missing,
                            "[DEBUG] Tuple ends when an element of type tice::v1::Chan_inlit or tice::v1::Chan is expected");
            };

            template<bool arg__is_channel, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_element>
            struct feeder_element_is_channel {
              static_assert(arg__is_channel,
                            "[DEBUG] Channel element is not specified using class template tice::v1::Chan_inlit or tice::v1::Chan");
            };

            template<bool arg__is_object_type, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct feeder_channel_arg_2_is_object_type {
              static_assert(arg__is_object_type,
                            "[DEBUG] Channel type is not an object type (neither void nor reference type nor function type)");
            };

            template<bool arg__is_cv_unqualified, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct feeder_channel_arg_2_is_cv_unqualified {
              static_assert(arg__is_cv_unqualified,
                            "[DEBUG] Channel type cannot be cv-qualified (qualified with either const or volatile or both)");
            };

            template<bool arg__is_not_nullptr, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__pos>
            struct feeder_channel_arg_3_is_not_nullptr {
              static_assert(arg__is_not_nullptr,
                            "[DEBUG] Channel initial value pointer cannot be null");
            };

            template<bool arg__is_assignable, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__producer_pos, v1::error::Pos arg__channel_pos,
                     typename arg__return_type, typename arg__channel_type>
            struct feeder_producer_return_value_is_assignable_to_channel {
              static_assert(arg__is_assignable,
                            "[DEBUG] Producer's return value is not assignable to channel");
            };

            template<bool arg__can_initialize, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__channel_pos,
                     typename arg__init_val_type, typename arg__channel_type>
            struct feeder_init_val_can_initialize_channel {
              static_assert(arg__can_initialize,
                            "[DEBUG] Initial value cannot initialize channel");
            };

            template<bool arg__is_compatible, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__channel_pos, v1::error::Pos arg__parameter_pos,
                     typename arg__channel_type, typename arg__parameter_type>
            struct feeder_channel_and_consumer_parameter_are_compatible {
              static_assert(arg__is_compatible,
                            "[DEBUG] Channel cannot initialize consumer's corresponding parameter");
            };

            template<bool arg__has_no_multiple_edges, v1::error::Pos arg__feeder_pos,
                     v1::error::Pos arg__producer_1_pos, v1::error::Pos arg__producer_2_pos>
            struct graph_has_no_multiple_edges {
              static_assert(arg__has_no_multiple_edges,
                            "[DEBUG] Tice graph cannot have multiple edges");
            };

            template<bool arg__has_no_loop, v1::error::Pos arg__feeder_pos, v1::error::Pos arg__producer_pos, v1::error::Pos arg__consumer_pos>
            struct graph_has_no_loop {
              static_assert(arg__has_no_loop,
                            "[DEBUG] Tice graph cannot have a loop");
            };

            template<bool arg__are_connected, v1::error::Pos arg__constraint_pos,
                     v1::error::Pos arg__sensor_node_pos, v1::error::Pos arg__actuator_node_pos>
            struct sensor_and_actuator_nodes_are_connected {
              static_assert(arg__are_connected,
                            "[DEBUG] There is no path from sensor to actuator");
            };

            template<bool arg__is_sensor_node, v1::error::Pos arg__node_pos, typename arg__node_type>
            struct isolated_node_is_sensor_node {
              static_assert(arg__is_sensor_node,
                            "[DEBUG] Isolated node is not sensor node");
            };

            template<bool arg__is_sensor_node, v1::error::Pos arg__node_pos, typename arg__node_type>
            struct source_node_is_sensor_node {
              static_assert(arg__is_sensor_node,
                            "[DEBUG] Source node is not sensor node");
            };

            template<bool arg__is_actuator_node, v1::error::Pos arg__node_pos, typename arg__node_type>
            struct sink_node_is_actuator_node {
              static_assert(arg__is_actuator_node,
                            "[DEBUG] Sink node is not actuator node");
            };

            template<bool arg__is_Node, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_2_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Arg 2 is not class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_2_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_2_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_2_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_2_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, v1::error::Pos arg__ete_delay_pos, typename arg__wcet>
            struct ete_delay_arg_2_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_2_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, v1::error::Pos arg__ete_delay_pos, typename arg__period, typename arg__wcet>
            struct ete_delay_arg_2_arg_3_is_greater_than_or_equal_to_ete_delay_arg_2_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__is_sensor_node, v1::error::Pos arg__ete_delay_pos, typename arg__node_type>
            struct ete_delay_arg_2_is_sensor_node {
              static_assert(arg__is_sensor_node,
                            "[DEBUG] Arg 2 is not sensor node");
            };

            template<bool arg__is_Node, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_3_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Arg 3 is not class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_3_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_3_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_3_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_3_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, v1::error::Pos arg__ete_delay_pos, typename arg__wcet>
            struct ete_delay_arg_3_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_3_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_3_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, v1::error::Pos arg__ete_delay_pos, typename arg__period, typename arg__wcet>
            struct ete_delay_arg_3_arg_3_is_greater_than_or_equal_to_ete_delay_arg_3_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__is_actuator_node, v1::error::Pos arg__ete_delay_pos, typename arg__node_type>
            struct ete_delay_arg_3_is_actuator_node {
              static_assert(arg__is_actuator_node,
                            "[DEBUG] Arg 3 is not actuator node");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_4_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Arg 4 is not class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_4_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 4 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_nonnegative, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_4_is_nonnegative {
              static_assert(arg__is_nonnegative,
                            "[DEBUG] End-to-end minimum delay is not nonnegative");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_5_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Arg 5 is not class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_5_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 5 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, v1::error::Pos arg__ete_delay_pos, typename arg__actual_parameter>
            struct ete_delay_arg_5_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] End-to-end maximum delay is not positive");
            };

            template<bool arg__is_less_than_or_equal_to_arg_5, v1::error::Pos arg__ete_delay_pos, typename arg__min_delay, typename arg__max_delay>
            struct ete_delay_arg_4_is_less_than_or_equal_to_ete_delay_arg_5 {
              static_assert(arg__is_less_than_or_equal_to_arg_5,
                            "[DEBUG] End-to-end minimum delay is not less than or equal to end-to-end maximum delay");
            };

            template<bool arg__is_between_min_and_max_delays, v1::error::Pos arg__constraint_pos,
                     typename arg__min, typename arg__end_to_end_delay, typename arg__max, typename arg__end_to_end_path,
                     v1::error::Pos arg__sensor_k_th_period>
            struct end_to_end_delay_is_between_min_and_max_delays {
              static_assert(arg__is_between_min_and_max_delays,
                            "[DEBUG] Min and max end-to-end delays are not respected");
            };

            template<bool arg__is_Node, v1::error::Pos arg__correlation_pos, typename arg__actual_parameter>
            struct correlation_arg_2_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Arg 2 is not class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, v1::error::Pos arg__correlation_pos, typename arg__actual_parameter>
            struct correlation_arg_2_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, v1::error::Pos arg__correlation_pos, typename arg__actual_parameter>
            struct correlation_arg_2_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, v1::error::Pos arg__correlation_pos, typename arg__actual_parameter>
            struct correlation_arg_2_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__correlation_pos, typename arg__actual_parameter>
            struct correlation_arg_2_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, v1::error::Pos arg__correlation_pos, typename arg__wcet>
            struct correlation_arg_2_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__correlation_pos, typename arg__actual_parameter>
            struct correlation_arg_2_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__correlation_pos, typename arg__actual_parameter>
            struct correlation_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, v1::error::Pos arg__correlation_pos, typename arg__period, typename arg__wcet>
            struct correlation_arg_2_arg_3_is_greater_than_or_equal_to_correlation_arg_2_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__is_actuator_node, v1::error::Pos arg__correlation_pos, typename arg__node_type>
            struct correlation_arg_2_is_actuator_node {
              static_assert(arg__is_actuator_node,
                            "[DEBUG] Arg 2 is not actuator node");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__correlation_pos, typename arg__actual_parameter>
            struct correlation_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Arg 3 is not class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__correlation_pos, typename arg__actual_parameter>
            struct correlation_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_nonnegative, v1::error::Pos arg__correlation_pos, typename arg__actual_parameter>
            struct correlation_arg_3_is_nonnegative {
              static_assert(arg__is_nonnegative,
                            "[DEBUG] Correlation threshold is not nonnegative");
            };

            template<bool arg__is_Node, v1::error::Pos arg__correlation_pos, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct correlation_arg_4_element_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Arg 4's element is not class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, v1::error::Pos arg__correlation_pos, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct correlation_arg_4_element_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, v1::error::Pos arg__correlation_pos, v1::error::Pos arg__element_pos,
                     typename arg__actual_parameter>
            struct correlation_arg_4_element_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, v1::error::Pos arg__correlation_pos, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct correlation_arg_4_element_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__correlation_pos, v1::error::Pos arg__element_pos,
                     typename arg__actual_parameter>
            struct correlation_arg_4_element_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, v1::error::Pos arg__correlation_pos, v1::error::Pos arg__element_pos, typename arg__wcet>
            struct correlation_arg_4_element_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__correlation_pos, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct correlation_arg_4_element_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__correlation_pos, v1::error::Pos arg__element_pos,
                     typename arg__actual_parameter>
            struct correlation_arg_4_element_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, v1::error::Pos arg__correlation_pos, v1::error::Pos arg__element_pos,
                     typename arg__period, typename arg__wcet>
            struct correlation_arg_4_element_arg_3_is_greater_than_or_equal_to_correlation_arg_4_element_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__is_sensor_node, v1::error::Pos arg__correlation_pos, v1::error::Pos arg__element_pos, typename arg__node_type>
            struct correlation_arg_4_element_is_sensor_node {
              static_assert(arg__is_sensor_node,
                            "[DEBUG] Arg 4's element is not sensor node");
            };

            template<bool arg__are_distinct, v1::error::Pos arg__correlation_pos, v1::error::Pos arg__node_1_pos, v1::error::Pos arg__node_2_pos>
            struct correlation_nodes_are_distinct {
              static_assert(arg__are_distinct,
                            "[DEBUG] Duplicated nodes are not allowed");
            };

            template<bool arg__is_within_threshold, v1::error::Pos arg__constraint_pos, typename arg__correlation, typename arg__threshold,
                     typename arg__confluent_path_1, typename arg__confluent_path_2, v1::error::Pos arg__confluent_node_k_th_period>
            struct correlation_is_within_threshold {
              static_assert(arg__is_within_threshold,
                            "[DEBUG] Correlation threshold is not respected");
            };

            template<bool arg__is_available, typename arg__HW__hw_desc>
            struct backend_is_available {
              static_assert(arg__is_available,
                            "[DEBUG] Backend to realize the expressed Tice model is unavailable for the given hardware description");
            };


            template<bool arg__is_successful, typename arg__total_utilization, typename arg__total_utilization_bound,
                     typename arg__max_utilization, typename arg__max_utilization_bound>
            struct gedf_schedulability_test_is_successful {
              static_assert(arg__is_successful,
                            "[DEBUG] gEDF (global earliest-deadline first) backend cannot realize the expressed Tice model because the total"
                            " processor utilization is greater than its bound and/or the processor utilization of some task exceeds the maximum"
                            " bound (Try reducing the WCETs of the function blocks first before redesigning the Tice model with greater periods"
                            " and/or less nodes)");
            };

            template<bool arg__is_tuple_construct, typename arg__actual_parameter>
            struct arg_4_is_tuple_construct {
              static_assert(arg__is_tuple_construct,
                            "[DEBUG] Arg 4 is not class template tice::v1::internals::tuple::construct");
            };

            template<bool arg__is_in_sensing_release_domain, v1::array::Idx arg__confluent_node_release_idx, typename arg__path>
            struct confluent_node_release_idx_is_in_sensing_release_domain {
              static_assert(arg__is_in_sensing_release_domain,
                            "[DEBUG] The confluent node found as the last node in the path receives non-sensing data along the path"
                            " when the confluent node is released at this release index");
            };

          }
        }
      }
      namespace program {

#include "v1_internals_begin.hpp"

#define base_1(_d1, _d2, _d3, _d4, I, _1, _2, _3, _4) I         \
        opts  (_1, arg__tuple_construct__remaining_args, _d1)   \
          opts(_2, arg__pos, _d2)                               \
          opts(_3, arg__tuple_checker_msg, _d3)                 \
          opts(_4, arg__pos_checker_msg, _d4)
#define decl_1(_d1, _d2, _d3, _d4, I, _1, _2, _3, _4)                                                                                           \
        base_1(_d1, _d2, _d3, _d4, I##I,                                                                                                        \
               type(_1, typename),                                                                                                              \
               type(_2, v1::error::Pos),                                                                                                        \
               type(_3, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class),                                        \
               type(_4, template<bool arg__is_tuple_construct_, v1::error::Pos arg__actual_parameter_, bool arg__2_is_empty_tuple_> class))
#define args_1(I, _1, _2, _3, _4) base_1((), (), (), (), I, _1, _2, _3, _4)
#define prms_1(...) decl_1((), (), (), (), __VA_ARGS__)

        template<decl_1(_d1(tuple::construct<I>),
                        _d2(v1::error::Invalid_pos::value),
                        _d3(error::program::parser_remaining_args::arg_2_is_tuple_construct),
                        _d4(error::program::parser_remaining_args::arg_3_is_valid),
                        I, _, _, _, _)>
        struct parser_remaining_args
        /* {
         *   typedef arg__tuple_construct__remaining_args remaining_args;
         *   static constexpr v1::error::Pos remaining_args_pos {arg__pos};
         *   invariant {
         *     if (tuple::size<I, remaining_args>::value > 0) {
         *       arg__pos != v1::error::Invalid_pos::value;
         *     } else {
         *       arg__pos == v1::error::Invalid_pos::value;
         *     }
         *   }
         * }
         */;

        

        template<prms_1(I, _, _, _, _)>
        struct parser_remaining_args :
          arg__tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct__remaining_args> {
        };

        template<prms_1(I, R(typename... args), _, _, _)>
        struct parser_remaining_args<args_1(I, R(tuple::construct<I, args...>), _, _, _)> :
          arg__pos_checker_msg<((sizeof...(args) > 0 || arg__pos == v1::error::Invalid_pos::value)
                                && (sizeof...(args) <= 0 || arg__pos != v1::error::Invalid_pos::value)), arg__pos, (sizeof...(args) == 0)>
        {
          typedef tuple::construct<I, args...> remaining_args;
          static constexpr v1::error::Pos remaining_args_pos {arg__pos};
        };

#include "v1_internals_end.hpp"
      }
      namespace program {

        template<typename I, v1::dict::Key arg__key>
        using dict_Entry_key = dict::construct_entry_key<I, arg__key>;

      }
      namespace program {

        template<typename I, v1::dict::Key arg__key, typename arg__value, typename arg__specialization = void>
        using dict_Entry = dict::construct_entry<I, arg__key, arg__value, arg__specialization>;

      }
      namespace program {

        template<typename I, v1::dict::Key arg__key, bool arg__value>
        using dict_Entry_bool = dict_Entry<I, arg__key, void, std::integral_constant<bool, arg__value>>;

      }
      namespace program {

#include "v1_internals_begin.hpp"

#define base_1A(_d1, I, _1) I                                   \
        opts  (_1, arg__dict_Entry_key_checker_msg, _d1)
#define decl_1A(_d1, I, _1)                                                                                                     \
        base_1A(_d1, I##I,                                                                                                      \
                type(_1, template<H(bool arg__is_tuple_construct_of_dict_Entry_key_, v1::error::Pos arg__dict_Entry_key_pos_,   \
                                    typename arg__actual_parameter_)> class))
#define args_1A(I, _1) base_1A((), I, _1)
#define prms_1A(...) decl_1A((), __VA_ARGS__)

        template<prms_1A(I, _)>
        struct node_id_test_array__data;

#define base_1B(I, _1, _2, _3) I                        \
        opts  (_1, arg__idx, ())                        \
          opts(_2, arg__dict_Entry_key__node_id, ())    \
          opts(_3, arg__data, ())
#define decl_1B(I, _1, _2, _3)                  \
        base_1B(I##I,                           \
                type(_1, v1::array::Idx),       \
                type(_2, typename),             \
                type(_3, typename))
#define args_1B(I, _1, _2, _3) base_1B(I, _1, _2, _3)
#define prms_1B(...) decl_1B(__VA_ARGS__)

        template<prms_1B(I, _, _, _)>
        struct node_id_test_array__node_id_dict_Entry_key_to_dict_Entry_bool;

#define base_2A(_d3, _d4, I, _1, _2, _3, _4, args_1A) I                         \
        opts  (_1, arg__node_count, ())                                         \
          opts(_2, arg__tuple_construct__of__dict_Entry_key__node_id, ())       \
          opts(_3, arg__count_checker_msg, _d3)                                 \
          opts(_4, arg__tuple_checker_msg, _d4)                       \
          args_1A
#define decl_2A(_d3, _d4, _1A_d1, I, _1, _2, _3, _4, _1A_1)                                                     \
        base_2A(_d3, _d4, I##I,                                                                                 \
                type(_1, v1::array::Size),                                                                      \
                type(_2, typename),                                                                             \
                type(_3, template<bool arg__is_positive_, v1::array::Size arg__size_> class),                   \
                type(_4, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class),       \
                decl_1A(_1A_d1, , _1A_1))
#define args_2A(I, _1, _2, _3, _4, _1A_1) base_2A((), (), I, _1, _2, _3, _4, args_1A(, _1A_1))
#define prms_2A(...) decl_2A((), (), (), __VA_ARGS__)

        template<decl_2A(_d3   (error::program::node_id_test_array::arg_2_is_positive),
                         _d4   (error::program::node_id_test_array::arg_3_is_tuple_construct),
                         _1A_d1(error::program::node_id_test_array::arg_3_is_tuple_construct_of_dict_Entry_key),
                         I, _, _, _, _, _)>
        using node_id_test_array = array::update::value<I, array::make::value<I, arg__node_count,
                                                                              std::integral_constant<bool, false>,
                                                                              v1::array::make::init_val_value_type,
                                                                              arg__count_checker_msg>,
                                                        H(typename
                                                          utility::list::update<H(I,
                                                                                  arg__tuple_construct__of__dict_Entry_key__node_id,
                                                                                  node_id_test_array__node_id_dict_Entry_key_to_dict_Entry_bool,
                                                                                  node_id_test_array__data<args_1A(I, _)>,
                                                                                  arg__tuple_checker_msg)>::value)>;

        

#define prms_1B_1(I, _2) prms_1B(I, _, _2, RR(prms_1A(, _)))
#define args_1B_1(I, _2) args_1B(I, _, _2, R(node_id_test_array__data<args_1A(I, _)>))

        template<prms_1B_1(I, _)>
        struct node_id_test_array__node_id_dict_Entry_key_to_dict_Entry_bool<args_1B_1(I, _)> :
          arg__dict_Entry_key_checker_msg<!!utility::always_false<I>(), arg__idx + 1, arg__dict_Entry_key__node_id> {
        };

        template<prms_1B_1(I, R(v1::dict::Key arg__key))>
        struct node_id_test_array__node_id_dict_Entry_key_to_dict_Entry_bool<args_1B_1(I, R(dict_Entry_key<I, arg__key>))> {
          static constexpr bool is_complete {false};
          typedef node_id_test_array__data<args_1A(I, _)> data;
          typedef dict_Entry_bool<I, arg__key, true> value;
        };

#include "v1_internals_end.hpp"
      }
      namespace program {

#include "v1_internals_begin.hpp"

#define base_1(_d2, I, _1, _2) I                                \
        opts  (_1, arg__node_id_test_array, ())                 \
          opts(_2, arg__element_type_is_bool_checker_msg, _d2)
#define decl_1(_d2, I, _1, _2)                                                                                  \
        base_1(_d2, I##I,                                                                                       \
               type(_1, typename),                                                                              \
               type(_2, template<bool arg__is_array_of_dict_Key_, typename arg__array_element_type_> class))
#define args_1(I, _1, _2) base_1((), I, _1, _2)
#define prms_1(...) decl_1((), __VA_ARGS__)

        template<prms_1(I, _, _)>
        struct node_id_array__validated_array;

#define base_2(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, args_1, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)   \
        args_1                                                                                                                  \
        opts  (_1 , arg__element_type_checker_msg, _d1)                                                                         \
          opts(_2 , arg__size_checker_msg, _d2)                                                                                 \
          opts(_3 , arg__type_checker_msg, _d3)                                                                                 \
          opts(_4 , arg__elems_checker_msg, _d4)                                                                                \
          opts(_5 , arg__elems_ptr_checker_msg, _d5)                                                                            \
          opts(_6 , arg__element_type_validator_msg, _d6)                                                                       \
        opts(_7 , arg__size_type_validator_msg, _d7)                                                                            \
        opts(_8 , arg__type_validator_msg, _d8)                                                                                 \
        opts(_9 , arg__elems_type_validator_msg, _d9)                                                                           \
        opts(_10, arg__elems_ptr_type_validator_msg, _d10)                                                                      \
        opts(_11, arg__elems_ptr_value_checker_msg, _d11)
#define decl_2(_1_d2, _d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, I, _1_1, _1_2, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)     \
        base_2(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11,                                                                         \
               decl_1(_1_d2, I, _1_1, _1_2),                                                                                                    \
               type(_1, template<bool arg__has_element_type_as_member_typedef_, typename arg__actual_parameter_> class),                        \
               type(_2, template<bool arg__has_size_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),                \
               type(_3, template<bool arg__has_type_as_member_typedef_, typename arg__actual_parameter_> class),                                \
               type(_4, template<bool arg__has_elems_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),               \
               type(_5, template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),           \
               type(_6, template<bool arg__is_valid_, typename arg__element_type_> class),                                                      \
               type(_7, template<bool arg__is_valid_, typename arg__size_type_> class),                                                         \
               type(_8, template<bool arg__is_valid_, typename arg__type_> class),                                                              \
               type(_9, template<bool arg__is_valid_, typename arg__elems_type_> class),                                                        \
               type(_10, template<bool arg__is_valid_, typename arg__elems_ptr_type_> class),                                                   \
               type(_11, template<bool arg__is_valid_, typename arg__elems_ptr_type_, arg__elems_ptr_type_ arg__elems_ptr_> class))
#define args_2(I, _1_1, _1_2, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)                                                     \
        base_2((), (), (), (), (), (), (), (), (), (), (), args_1(I, _1_1, _1_2), _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)
#define prms_2(...) decl_2((), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_2(_1_d2(error::program::node_id_array::arg_2_is_array_of_bool),
                        _d1  (error::program::node_id_array::arg_2_has_element_type_as_member_typedef),
                        _d2  (error::program::node_id_array::arg_2_has_size_as_constexpr_copyable_data_member),
                        _d3  (error::program::node_id_array::arg_2_has_type_as_member_typedef),
                        _d4  (error::program::node_id_array::arg_2_has_elems_as_constexpr_copyable_data_member),
                        _d5  (error::program::node_id_array::arg_2_has_elems_ptr_as_constexpr_copyable_data_member),
                        _d6  (error::program::node_id_array::arg_2_element_type_is_valid),
                        _d7  (error::program::node_id_array::arg_2_size_type_is_valid),
                        _d8  (error::program::node_id_array::arg_2_type_is_valid),
                        _d9  (error::program::node_id_array::arg_2_elems_type_is_valid),
                        _d10 (error::program::node_id_array::arg_2_elems_ptr_type_is_valid),
                        _d11 (error::program::node_id_array::arg_2_elems_ptr_points_to_elems),
                        I, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct node_id_array
        /* {
         *   include array::filter_then_map::value<I, arg__node_id_test_array, v1::dict::Key,
         *                                         [](const v1::array::Idx &idx, const bool &is_true) -> pair::construct<I, bool, v1::dict::Key> {
         *                                           return {is_true, idx};
         *                                         }>;
         * }
         */;

        

        template<prms_2(I, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct node_id_array :
          array::validate<I, arg__node_id_test_array, arg__type_validator_msg, arg__elems_type_validator_msg,
                          arg__elems_ptr_type_validator_msg, arg__elems_ptr_value_checker_msg,
                          arg__type_checker_msg, arg__elems_checker_msg, arg__elems_ptr_checker_msg,
                          arg__element_type_validator_msg, arg__size_type_validator_msg,
                          arg__element_type_checker_msg, arg__size_checker_msg>,
          node_id_array__validated_array<args_1(I, _, _)> {
        };

        

        static constexpr pair::construct<I, bool, v1::dict::Key> node_id_array__true_to_node_id(const v1::array::Idx &idx, const bool &is_true) {
          return {is_true, idx};
        }

        template<prms_1(I, _, _)>
        struct node_id_array__validated_array :
          arg__element_type_is_bool_checker_msg<std::is_same<typename arg__node_id_test_array::element_type, bool>::value,
                                                typename arg__node_id_test_array::element_type>,
          array::filter_then_map::value<I, arg__node_id_test_array, v1::dict::Key, node_id_array__true_to_node_id> {
        };

#include "v1_internals_end.hpp"
      }
      namespace program {

#include "v1_internals_begin.hpp"

#define base_1A(_d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11) I       \
        opts  (_1 , arg__pos, ())                                                                                       \
          opts(_2 , arg__tuple_construct__input, ())                                                                    \
          opts(_3 , arg__node_computation_checker_msg, _d3)                                                             \
          opts(_4 , arg__node_computation_fn_ptr_checker_msg, _d4)                                                      \
        opts  (_5 , arg__node_computation_wcet_checker_msg, _d5)                                                        \
        opts  (_6 , arg__node_computation_wcet_den_checker_msg, _d6)                                                    \
        opts  (_7 , arg__node_computation_wcet_validator_msg, _d7)                                                      \
        opts  (_8 , arg__node_period_checker_msg, _d8)                                                                  \
        opts  (_9 , arg__node_period_den_checker_msg, _d9)                                                              \
        opts  (_10, arg__node_period_validator_msg, _d10)                                                               \
        opts  (_11, arg__distinct_node_checker_msg, _d11)
#define decl_1A(_d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)                                 \
        base_1A(_d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, I##I,                                                                            \
                type(_1, v1::error::Pos),                                                                                                       \
                type(_2, typename),                                                                                                             \
                type(_3, template<bool arg__is_comp_Unit_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),                   \
                type(_4, template<bool arg__is_function_pointer_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),            \
                type(_5, template<bool arg__is_std_ratio_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),                   \
                type(_6, template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),        \
                type(_7, template<bool arg__is_positive_, v1::error::Pos arg__pos_, typename arg__wcet_> class),                                \
                type(_8, template<bool arg__is_std_ratio_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),                   \
                type(_9, template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),        \
                type(_10, template<H(bool arg__is_greater_than_or_equal_, v1::error::Pos arg__pos_, typename arg__period_, typename arg__wcet_> \
                                     class)),                                                                                                   \
                type(_11, template<bool arg__are_distinct_, v1::error::Pos arg__node_1_pos_, v1::error::Pos arg__node_2_pos_> class))
#define args_1A(I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)                                        \
        base_1A((), (), (), (), (), (), (), (), (), I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)
#define prms_1A(...) decl_1A((), (), (), (), (), (), (), (), (), __VA_ARGS__)

#define base_2AA(_d1, _d2, _d3, _d4, _d5, _d6, _d7, args_1A, _1, _2, _3, _4, _5, _6, _7)        \
        args_1A                                                                                 \
        opts  (_1, arg__node_id, _d1)                                                           \
          opts(_2, arg__tuple_construct__of__dict_Entry_key__sensor_node_id, _d2)               \
          opts(_3, arg__tuple_construct__of__dict_Entry_key__intermediary_node_id, _d3)         \
          opts(_4, arg__tuple_construct__of__dict_Entry_key__actuator_node_id, _d4)             \
        opts  (_5, arg__tuple_construct__of__dict_Entry__of__Node__node_dict, _d5)              \
        opts  (_6, arg__tuple_construct__of__utility_fp_Double__node_period, _d6)               \
        opts  (_7, arg__tuple_construct__of__utility_fp_Double__node_wcet, _d7)
#define decl_2AA(_d1, _d2, _d3, _d4, _d5, _d6, _d7,                                                                             \
                 I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11, _1, _2, _3, _4, _5, _6, _7)  \
        base_2AA(_d1, _d2, _d3, _d4, _d5, _d6, _d7,                                                                             \
                 prms_1A(I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11),                     \
                 type(_1, v1::dict::Key),                                                                                       \
                 type(_2, typename),                                                                                            \
                 type(_3, typename),                                                                                            \
                 type(_4, typename),                                                                                            \
                 type(_5, typename),                                                                                            \
                 type(_6, typename),                                                                                            \
                 type(_7, typename))
#define args_2AA(I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11, _1, _2, _3, _4, _5, _6, _7)          \
        base_2AA((), (), (), (), (), (), (), args_1A(I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11), \
                 _1, _2, _3, _4, _5, _6, _7)
#define prms_2AA(...) decl_2AA((), (), (), (), (), (), (), __VA_ARGS__)

#define base_3AAC(_d1, args_2AA, _1)                            \
        args_2AA                                                \
        opts  (_1, arg__error_pos_adaptor__node_msgs, _d1)
#define decl_3AAC(_2AA_d1, _2AA_d2, _2AA_d3, _2AA_d4, _2AA_d5, _2AA_d6, _2AA_d7, _d1,                           \
                  I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11,             \
                  _2AA_1, _2AA_2, _2AA_3, _2AA_4, _2AA_5, _2AA_6, _2AA_7, _1)                                   \
        base_3AAC(_d1,                                                                                          \
                  decl_2AA(_2AA_d1, _2AA_d2, _2AA_d3, _2AA_d4, _2AA_d5, _2AA_d6, _2AA_d7,                       \
                           I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11,    \
                           _2AA_1, _2AA_2, _2AA_3, _2AA_4, _2AA_5, _2AA_6, _2AA_7),                             \
                  type(_1, typename))
#define args_3AAC(I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11,                     \
                  _2AA_1, _2AA_2, _2AA_3, _2AA_4, _2AA_5, _2AA_6, _2AA_7, _1)                                           \
        base_3AAC((), args_2AA(I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11,        \
                               _2AA_1, _2AA_2, _2AA_3, _2AA_4, _2AA_5, _2AA_6, _2AA_7), _1)
#define prms_3AAC(...) decl_3AAC((), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_3AAC(_2AA_d1(0),
                           _2AA_d2(tuple::construct<I>),
                           _2AA_d3(tuple::construct<I>),
                           _2AA_d4(tuple::construct<I>),
                           _2AA_d5(tuple::construct<I>),
                           _2AA_d6(tuple::construct<I>),
                           _2AA_d7(tuple::construct<I>),
                           _d1    (utility::error_pos_adaptor::node_msgs<H(I, arg__pos, arg__node_computation_checker_msg,
                                                                           arg__node_computation_fn_ptr_checker_msg,
                                                                           arg__node_computation_wcet_checker_msg,
                                                                           arg__node_computation_wcet_den_checker_msg,
                                                                           arg__node_computation_wcet_validator_msg,
                                                                           arg__node_period_checker_msg,
                                                                           arg__node_period_den_checker_msg,
                                                                           arg__node_period_validator_msg)>),
                           I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes__next;

#define base_3AAA(I, _1, args_2AA) I            \
        opts  (_1, arg__Node__valid_node, ())   \
          args_2AA
#define decl_3AAA(I, _1, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11,         \
                  _2AA_1, _2AA_2, _2AA_3, _2AA_4, _2AA_5, _2AA_6, _2AA_7)                                       \
        base_3AAA(I##I,                                                                                         \
                  type(_1, typename),                                                                           \
                  prms_2AA(, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11,     \
                           _2AA_1, _2AA_2, _2AA_3, _2AA_4, _2AA_5, _2AA_6, _2AA_7))
#define args_3AAA(I, _1, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11,                 \
                  _2AA_1, _2AA_2, _2AA_3, _2AA_4, _2AA_5, _2AA_6, _2AA_7)                                               \
        base_3AAA(I, _1, args_2AA(, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11,      \
                                  _2AA_1, _2AA_2, _2AA_3, _2AA_4, _2AA_5, _2AA_6, _2AA_7))
#define prms_3AAA(...) decl_3AAA(__VA_ARGS__)

        template<prms_3AAA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes__node_validated;

#define base_3AAB(I, _1, args_2AA) I            \
        opts  (_1, arg__node_type, ())          \
          args_2AA
#define decl_3AAB(I, _1, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11,         \
                  _2AA_1, _2AA_2, _2AA_3, _2AA_4, _2AA_5, _2AA_6, _2AA_7)                                       \
        base_3AAB(I##I,                                                                                         \
                  type(_1, typename),                                                                           \
                  prms_2AA(, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11,     \
                           _2AA_1, _2AA_2, _2AA_3, _2AA_4, _2AA_5, _2AA_6, _2AA_7))
#define args_3AAB(I, _1, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11,                 \
                  _2AA_1, _2AA_2, _2AA_3, _2AA_4, _2AA_5, _2AA_6, _2AA_7)                                               \
        base_3AAB(I, _1, args_2AA(, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11,      \
                                  _2AA_1, _2AA_2, _2AA_3, _2AA_4, _2AA_5, _2AA_6, _2AA_7))
#define prms_3AAB(...) decl_3AAB(__VA_ARGS__)

        template<prms_3AAB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes__categorize_node;

#define base_1B(_d1, _d2, _d3, _d4, _d5, _d6, _d7, I, _1, _2, _3, _4, _5, _6, _7) I     \
        opts  (_1, arg__totality_checker_msg, _d1)                                      \
          opts(_2, arg__disjoint_sensor_intermediary_checker_msg, _d2)                  \
          opts(_3, arg__disjoint_sensor_actuator_checker_msg, _d3)                      \
          opts(_4, arg__disjoint_intermediary_actuator_checker_msg, _d4)                \
          opts(_5, arg__sensor_node_id_checker_msg, _d5)                                \
          opts(_6, arg__intermediary_node_id_checker_msg, _d6)                          \
          opts(_7, arg__actuator_node_id_checker_msg, _d7)
#define decl_1B(_d1, _d2, _d3, _d4, _d5, _d6, _d7, I, _1, _2, _3, _4, _5, _6, _7)                                                       \
        base_1B(_d1, _d2, _d3, _d4, _d5, _d6, _d7, I##I,                                                                                \
                type(_1, template<bool arg__is_either_sensor_or_intermediary_or_actuator_, v1::error::Pos arg__node_pos_> class),       \
                type(_2, template<bool arg__is_not_both_sensor_and_intermediary_, v1::error::Pos arg__node_pos_> class),                \
                type(_3, template<bool arg__is_not_both_sensor_and_actuator_, v1::error::Pos arg__node_pos_> class),                    \
                type(_4, template<bool arg__is_not_both_intermediary_and_actuator_, v1::error::Pos arg__node_pos_> class),              \
                type(_5, template<bool arg__is_in_sensor_test_array_, v1::dict::Key arg__node_id_> class),                              \
                type(_6, template<bool arg__is_in_intermediary_test_array_, v1::dict::Key arg__node_id_> class),                        \
                type(_7, template<bool arg__is_in_actuator_test_array_, v1::dict::Key arg__node_id_> class))
#define args_1B(I, _1, _2, _3, _4, _5, _6, _7) base_1B((), (), (), (), (), (), (), I, _1, _2, _3, _4, _5, _6, _7)
#define prms_1B(...) decl_1B((), (), (), (), (), (), (), __VA_ARGS__)

#define base_2B(_d1, I, _1, args_1B) I          \
        opts  (_1, arg__size_checker_msg, _d1)  \
          args_1B
#define decl_2B(_d1, _1B_d1, _1B_d2, _1B_d3, _1B_d4, _1B_d5, _1B_d6, _1B_d7, I, _1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7)               \
        base_2B(_d1, I##I,                                                                                                                         \
                type(_1, template<H(bool arg__are_equal_, v1::array::Size arg__node_dict_size_, v1::array::Size arg__sensor_node_test_array_size_, \
                                    v1::array::Size arg__intermediary_node_test_array_size_, v1::array::Size arg__actuator_node_test_array_size_,  \
                                    v1::array::Size arg__sensor_node_id_array_size_, v1::array::Size arg__intermediary_node_id_array_size_,        \
                                    v1::array::Size arg__actuator_node_id_array_size_)> class),                                                    \
                decl_1B(_1B_d1, _1B_d2, _1B_d3, _1B_d4, _1B_d5, _1B_d6, _1B_d7, , _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7))
#define args_2B(I, _1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7)                 \
        base_2B((), I, _1, args_1B(, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7))
#define prms_2B(...) decl_2B((), (), (), (), (), (), (), (), __VA_ARGS__)

#define base_1C(_d2, _d3, I, _1, _2, _3) I      \
        opts  (_1, arg__end, ())                \
          opts(_2, arg__i, _d2)                 \
          opts(_3, arg__is_done, _d3)
#define decl_1C(_d2, _d3, I, _1, _2, _3)        \
        base_1C(_d2, _d3, I##I,                 \
                type(_1, v1::array::Idx),       \
                type(_2, v1::array::Idx),       \
                type(_3, bool))
#define args_1C(I, _1, _2, _3) base_1C((), (), I, _1, _2, _3)
#define prms_1C(...) decl_1C((), (), __VA_ARGS__)

#define base_2CA(I, _1, _2, _3, args_1C) I              \
        opts  (_1, arg__node_id_test_array, ())         \
          opts(_2, arg__node_id_array, ())              \
          opts(_3, arg__node_id_checker_msg, ())        \
          args_1C
#define decl_2CA(_1C_d2, _1C_d3, I, _1, _2, _3, _1C_1, _1C_2, _1C_3)                                            \
        base_2CA(I##I,                                                                                          \
                 type(_1, typename),                                                                            \
                 type(_2, typename),                                                                            \
                 type(_3, template<bool arg__node_id_is_in_test_array_, v1::dict::Key arg__node_id_> class),    \
                 decl_1C(_1C_d2, _1C_d3, , _1C_1, _1C_2, _1C_3))
#define args_2CA(I, _1, _2, _3, _1C_1, _1C_2, _1C_3) base_2CA(I, _1, _2, _3, args_1C(, _1C_1, _1C_2, _1C_3))
#define prms_2CA(...) decl_2CA((), (), __VA_ARGS__)

        template<decl_2CA(_1C_d2(0),
                          _1C_d3(arg__i == arg__end),
                          I, _, _, _, _, _, _)>
        struct parse_nodes__check_node_ids;

#define base_1E(I, _1, _2, _3, _4, _5, _6) I                    \
        opts  (_1, arg__node_id_test_array__sensor, ())         \
          opts(_2, arg__node_id_test_array__intermediary, ())   \
          opts(_3, arg__node_id_test_array__actuator, ())       \
          opts(_4, arg__node_id_array__sensor, ())              \
          opts(_5, arg__node_id_array__intermediary, ())        \
          opts(_6, arg__node_id_array__actuator, ())
#define decl_1E(I, _1, _2, _3, _4, _5, _6)      \
        base_1E(I##I,                           \
                type(_1, typename),             \
                type(_2, typename),             \
                type(_3, typename),             \
                type(_4, typename),             \
                type(_5, typename),             \
                type(_6, typename))
#define args_1E(I, _1, _2, _3, _4, _5, _6) base_1E(I, _1, _2, _3, _4, _5, _6)
#define prms_1E(...) decl_1E(__VA_ARGS__)

#define base_2E(I, _1, args_1E, args_2B) I                                              \
        opts  (_1, arg__tuple_construct__of__dict_Entry__of__Node__node_dict, ())       \
          args_1E                                                                       \
          args_2B
#define decl_2E(I, _1, _1E_1, _1E_2, _1E_3, _1E_4, _1E_5, _1E_6, _2B_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7)        \
        base_2E(I##I,                                                                                                           \
                type(_1, typename),                                                                                             \
                prms_1E(, _1E_1, _1E_2, _1E_3, _1E_4, _1E_5, _1E_6),                                                            \
                prms_2B(, _2B_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7))
#define args_2E(I, _1, _1E_1, _1E_2, _1E_3, _1E_4, _1E_5, _1E_6, _2B_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7)                        \
        base_2E(I, _1, args_1E(, _1E_1, _1E_2, _1E_3, _1E_4, _1E_5, _1E_6), args_2B(, _2B_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7))
#define prms_2E(...) decl_2E(__VA_ARGS__)

        template<prms_2E(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes__check_result;

#define base_3B(args_1E, args_2B, args_1C)      \
        args_1E                                 \
        args_2B                                 \
        args_1C
#define decl_3B(_1C_d2, _1C_d3,                                                                                                                 \
                I, _1E_1, _1E_2, _1E_3, _1E_4, _1E_5, _1E_6, _2B_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7, _1C_1, _1C_2, _1C_3)       \
        base_3B(prms_1E(I, _1E_1, _1E_2, _1E_3, _1E_4, _1E_5, _1E_6),                                                                           \
                prms_2B(, _2B_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7),                                                              \
                decl_1C(_1C_d2, _1C_d3, , _1C_1, _1C_2, _1C_3))
#define args_3B(I, _1E_1, _1E_2, _1E_3, _1E_4, _1E_5, _1E_6, _2B_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7, _1C_1, _1C_2, _1C_3)       \
        base_3B(args_1E(I, _1E_1, _1E_2, _1E_3, _1E_4, _1E_5, _1E_6), args_2B(, _2B_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7),        \
                args_1C(, _1C_1, _1C_2, _1C_3))
#define prms_3B(...) decl_3B((), (), __VA_ARGS__)

        template<decl_3B(_1C_d2(0),
                         _1C_d3(arg__i == arg__end),
                         I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes__check_totality_and_disjointness;

#define base_2AB(_d1, _d2, _d3, args_1A, _1, _2, _3, args_2B)   \
        args_1A                                                 \
        opts  (_1, arg__tuple_checker_msg, _d1)                 \
          opts(_2, arg__nonempty_tuple_checker_msg, _d2)        \
          opts(_3, arg__first_element_checker_msg, _d3)         \
          args_2B
#define decl_2AB(_1A_d3, _1A_d4, _1A_d5, _1A_d6, _1A_d7, _1A_d8, _1A_d9, _1A_d10, _1A_d11, _d1, _d2, _d3,       \
                 _2B_d1, _1B_d1, _1B_d2, _1B_d3, _1B_d4, _1B_d5, _1B_d6, _1B_d7,                                \
                 I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11, _1, _2, _3,  \
                 _2B_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7)                                        \
        base_2AB(_d1, _d2, _d3,                                                                                 \
                 decl_1A(_1A_d3, _1A_d4, _1A_d5, _1A_d6, _1A_d7, _1A_d8, _1A_d9, _1A_d10, _1A_d11,              \
                         I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11),     \
                 type(_1, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class),      \
                 type(_2, template<bool arg__is_nonempty_tuple_construct_> class),                              \
                 type(_3, template<bool arg__is_Node_, typename arg__actual_parameter_> class),                 \
                 decl_2B(_2B_d1, _1B_d1, _1B_d2, _1B_d3, _1B_d4, _1B_d5, _1B_d6, _1B_d7,                        \
                         , _2B_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7))
#define args_2AB(I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11, _1, _2, _3,                          \
                 _2B_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7)                                                                \
        base_2AB((), (), (), args_1A(I, _1A_1, _1A_2, _1A_3, _1A_4, _1A_5, _1A_6, _1A_7, _1A_8, _1A_9, _1A_10, _1A_11), _1, _2, _3,     \
                 args_2B(, _2B_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1B_6, _1B_7))
#define prms_2AB(...) decl_2AB((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_2AB(_1A_d3 (error::program::parse_nodes::node_arg_2_is_comp_Unit),
                          _1A_d4 (error::program::parse_nodes::node_arg_2_arg_2_is_function_pointer),
                          _1A_d5 (error::program::parse_nodes::node_arg_2_arg_3_is_Ratio),
                          _1A_d6 (error::program::parse_nodes::node_arg_2_arg_3_has_nonzero_denominator),
                          _1A_d7 (error::program::parse_nodes::node_arg_2_arg_3_is_positive),
                          _1A_d8 (error::program::parse_nodes::node_arg_3_is_std_ratio),
                          _1A_d9 (error::program::parse_nodes::node_arg_3_has_nonzero_denominator),
                          _1A_d10(error::program::parse_nodes::node_arg_3_is_greater_than_or_equal_to_node_arg_2_arg_3),
                          _1A_d11(error::program::parse_nodes::nodes_are_distinct),
                          _d1    (error::program::parse_nodes::arg_3_is_tuple_construct),
                          _d2    (error::program::parse_nodes::arg_3_is_nonempty_tuple_construct),
                          _d3    (error::program::parse_nodes::first_element_is_Node),
                          _2B_d1 (error::program::parse_nodes::sizes_of_node_dict_and_test_and_id_arrays_are_equal_and_positive),
                          _1B_d1 (error::program::parse_nodes::node_type_is_either_sensor_or_intermediary_or_actuator),
                          _1B_d2 (error::program::parse_nodes::node_type_is_not_both_sensor_and_intermediary),
                          _1B_d3 (error::program::parse_nodes::node_type_is_not_both_sensor_and_actuator),
                          _1B_d4 (error::program::parse_nodes::node_type_is_not_both_intermediary_and_actuator),
                          _1B_d5 (error::program::parse_nodes::sensor_node_id_is_in_sensor_test_array),
                          _1B_d6 (error::program::parse_nodes::intermediary_node_id_is_in_intermediary_test_array),
                          _1B_d7 (error::program::parse_nodes::actuator_node_id_is_in_actuator_test_array),
                          I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes
        /* {
         *   with template<typename I, typename... args> tuple::construct<I, args...> = arg__tuple_construct__input {
         *     let tuple::construct<I, args...@@[&](std::size_t i, auto arg__element) { return {arg__element~~Node, arg__element}; }> nodeset;
         *     invariant {
         *       tuple::size<I, nodeset>::value > 0;
         *
         *       for (auto i = 0; i < tuple::size<I, nodeset>::value - 1; ++i) {
         *         for (auto j = i + 1; j < tuple::size<I, nodeset>::value; ++j) {
         *           std::is_same<typename tuple::get_pos<I, i + 1, nodeset>::value,
         *                        typename tuple::get_pos<I, j + 1, nodeset>::value>::value == false;
         *         }
         *       }
         *     }
         *
         *     with template<typename I, typename... args__node> tuple::construct<I, args__node...> = nodeset {
         *       let tuple::construct<I, args...@[&](std::size_t i, auto arg__element) {
         *                                 return {i >= sizeof...(args__node),
         *                                         arg__element};
         *                               }> remainder;
         *       let tuple::size<I, remainder> remainder_size;
         *
         *       include parser_remaining_args<I, remainder, remainder_size::value == 0 ? v1::error::Invalid_pos::value : remainder_size::value>;
         *
         *       typedef tuple::construct<I, args__node...@[&](std::size_t i, auto arg__Node__node) {
         *         return {true, dict_Entry<I, i, arg__Node__node>};
         *       }> node_dict;
         *
         *       static constexpr v1::array::Size node_count = tuple::size<I, node_dict>::value;
         *
         *       let tuple::construct<I, args__node...@[&](std::size_t i, auto arg__Node__node) {
         *         return {arg__Node__node::type == v1::node::type::SENSOR, dict_Entry_key<I, i>};
         *       }> sensor_nodeset;
         *       let tuple::construct<I, args__node...@[&](std::size_t i, auto arg__Node__node) {
         *         return {arg__Node__node::type == v1::node::type::INTERMEDIARY, dict_Entry_key<I, i>};
         *       }> intermediary_nodeset;
         *       let tuple::construct<I, args__node...@[&](std::size_t i, auto arg__Node__node) {
         *         return {arg__Node__node::type == v1::node::type::ACTUATOR, dict_Entry_key<I, i>};
         *       }> actuator_nodeset;
         *
         *       typedef node_id_test_array<I, node_count, sensor_nodeset> is_sensor_node;
         *       typedef node_id_test_array<I, node_count, intermediary_nodeset> is_intermediary_node;
         *       typedef node_id_test_array<I, node_count, actuator_nodeset> is_actuator_node;
         *       typedef node_id_array<I, is_sensor_node> sensor_node_ids;
         *       typedef node_id_array<I, is_intermediary_node> intermediary_node_ids;
         *       typedef node_id_array<I, is_actuator_node> actuator_node_ids;
         *
         *       invariant {
         *         (node_count > 0)
         *         && (node_count == is_sensor_node::size)
         *         && (node_count == is_intermediary_node::size)
         *         && (node_count == is_actuator_node::size)
         *         && (node_count == sensor_node_ids::size + intermediary_node_ids::size + actuator_node_ids::size);
         *
         *         for (int i = 0; i < node_count; ++i) {
         *           (is_sensor_node::elems[i] || is_intermediary_node::elems[i] || is_actuator_node::elems[i])
         *           && !(is_sensor_node::elems[i] && is_intermediary_node::elems[i])
         *           && !(is_sensor_node::elems[i] && is_actuator_node::elems[i])
         *           && !(is_intermediary_node::elems[i] && is_actuator_node::elems[i]);
         *         }
         *
         *         for (int i = 0; i < sensor_node_ids::size; ++i) {
         *           is_sensor_node::elems[sensor_node_ids::elems[i]] == true;
         *         }
         *         for (int i = 0; i < intermediary_node_ids::size; ++i) {
         *           is_intermediary_node::elems[intermediary_node_ids::elems[i]] == true;
         *         }
         *         for (int i = 0; i < actuator_node_ids::size; ++i) {
         *           is_actuator_node::elems[actuator_node_ids::elems[i]] == true;
         *         }
         *       }
         *
         *       typedef array::make::from_tuple::value<I, tuple::construct<I, args__node...@[&](std::size_t i, auto arg__Node__node) {
         *         return {true, utility::fp::Double<I, arg__Node__node::period>};
         *       }>> node_period;
         *
         *       typedef array::make::from_tuple::value<I, tuple::construct<I, args__node...@[&](std::size_t i, auto arg__Node__node) {
         *         return {true, utility::fp::Double<I, arg__Node__node::wcet>};
         *       }>> node_wcet;
         *
         *       invariant {
         *         for (int i = 0; i < node_count; ++i) {
         *           ((tuple::get_pos<I, i + 1, node_dict>::value::value::period::num
         *             / (double) tuple::get_pos<I, i + 1, node_dict>::value::value::period::den)
         *            == node_period::elems[i])
         *           && ((tuple::get_pos<I, i + 1, node_dict>::value::value::wcet::num
         *                / (double) tuple::get_pos<I, i + 1, node_dict>::value::value::wcet::den)
         *               == node_wcet::elems[i]);
         *         }
         *       }
         *
         *     }
         *   }
         * }
         */;

        

        template<prms_2AB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes :
          arg__tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct__input> {
        };

        template<prms_2AB(I, _, X, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes<args_2AB(I, _, R(tuple::construct<I>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          arg__nonempty_tuple_checker_msg<!!utility::always_false<I>()> {
        };

        template<prms_2AB(I, _, R(typename arg, typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes<args_2AB(I, _, R(tuple::construct<I, arg, args...>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          arg__first_element_checker_msg<!!utility::always_false<I>(), arg> {
        };

        template<prms_2AB(I, _, R(typename... args__node_arg, typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes<args_2AB(I, _, R(tuple::construct<I, Node<args__node_arg...>, args...>),
                                    _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
#define result parse_nodes__next<args_3AAC(I, _, R(tuple::construct<I, Node<args__node_arg...>, args...>),      \
                                           _, _, _, _, _, _, _, _, _, X, X, X, X, X, X, X, X)>
          result,
          parse_nodes__check_result<args_2E(I, R(typename result::node_dict), R(typename result::is_sensor_node),
                                            R(typename result::is_intermediary_node), R(typename result::is_actuator_node),
                                            R(typename result::sensor_node_ids), R(typename result::intermediary_node_ids),
                                            R(typename result::actuator_node_ids), _, _, _, _, _, _, _, _)> {
        };
#undef result

        

        template<prms_3AAC(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes__next :
          parser_remaining_args<I, arg__tuple_construct__input, (tuple::size<I, arg__tuple_construct__input>::value == 0
                                                                 ? v1::error::Invalid_pos::value
                                                                 : arg__pos)>
        {
          typedef arg__tuple_construct__of__dict_Entry__of__Node__node_dict node_dict;
          static constexpr v1::array::Size node_count {tuple::size<I, node_dict>::value};
          typedef node_id_test_array<I, node_count, arg__tuple_construct__of__dict_Entry_key__sensor_node_id> is_sensor_node;
          typedef node_id_test_array<I, node_count, arg__tuple_construct__of__dict_Entry_key__intermediary_node_id> is_intermediary_node;
          typedef node_id_test_array<I, node_count, arg__tuple_construct__of__dict_Entry_key__actuator_node_id> is_actuator_node;
          typedef node_id_array<I, is_sensor_node> sensor_node_ids;
          typedef node_id_array<I, is_intermediary_node> intermediary_node_ids;
          typedef node_id_array<I, is_actuator_node> actuator_node_ids;
          typedef array::make::from_tuple::value<I, arg__tuple_construct__of__utility_fp_Double__node_period> node_period;
          typedef array::make::from_tuple::value<I, arg__tuple_construct__of__utility_fp_Double__node_wcet> node_wcet;
        };

        template<prms_3AAC(I, _, R(typename... args__node_arg, typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes__next<args_3AAC(I, _, R(tuple::construct<I, Node<args__node_arg...>, args...>),
                                           _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          utility::run_metaprogram<I, node::construct<I, args__node_arg...,
                                                      arg__error_pos_adaptor__node_msgs::template period_validator_msg,
                                                      arg__error_pos_adaptor__node_msgs::template computation_checker_msg,
                                                      arg__error_pos_adaptor__node_msgs::template computation_fn_ptr_checker_msg,
                                                      arg__error_pos_adaptor__node_msgs::template computation_wcet_checker_msg,
                                                      arg__error_pos_adaptor__node_msgs::template computation_wcet_den_checker_msg,
                                                      arg__error_pos_adaptor__node_msgs::template computation_wcet_validator_msg,
                                                      arg__error_pos_adaptor__node_msgs::template period_den_checker_msg,
                                                      arg__error_pos_adaptor__node_msgs::template period_checker_msg>>,
          parse_nodes__node_validated<args_3AAA(I, R(Node<args__node_arg...>), _, R(tuple::construct<I, args...>),
                                                _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
        };

        

        template<prms_3AAA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                           R(typename... args__dict_entry), R(typename... args__period), R(typename... args__wcet))>
        struct parse_nodes__node_validated<args_3AAA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(tuple::construct<I, args__dict_entry...>),
                                                     R(tuple::construct<I, args__period...>), R(tuple::construct<I, args__wcet...>))> :
          utility::check_pairwise_distinctness<I, utility::nodes_pairwise_distinctness<I, arg__distinct_node_checker_msg>::template checker,
                                               arg__pos - sizeof...(args__dict_entry), 1, arg__pos,
                                               arg__Node__valid_node, typename args__dict_entry::value...>,
          parse_nodes__categorize_node<args_3AAB(I, R(typename arg__Node__valid_node::type), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                 R(tuple::construct<I, args__dict_entry..., dict_Entry<I, arg__node_id, arg__Node__valid_node>>),
                                                 R(tuple::construct<H(I, args__period...,
                                                                      utility::fp::Double<I, typename arg__Node__valid_node::period>)>),
                                                 R(tuple::construct<H(I, args__wcet...,
                                                                      utility::fp::Double<I, typename arg__Node__valid_node::wcet>)>))> {
        };

        

        template<prms_3AAB(I, X, _, _, _, _, _, _, _, _, _, _, _, _,
                          R(typename... args__sensor_node_id), _, _, _, _, _)>
        struct parse_nodes__categorize_node<args_3AAB(I, R(v1::node::type::SENSOR), _, _, _, _, _, _, _, _, _, _, _, _,
                                                      R(tuple::construct<I, args__sensor_node_id...>), _, _, _, _, _)> :
          parse_nodes__next<args_3AAC(I, R(arg__pos + 1), _, _, _, _, _, _, _, _, _, _, R(arg__node_id + 1),
                                      R(tuple::construct<I, args__sensor_node_id..., dict_Entry_key<I, arg__node_id>>), _, _, _, _, _, X)> {
        };

        template<prms_3AAB(I, X, _, _, _, _, _, _, _, _, _, _, _, _,
                           _, R(typename... args__intermediary_node_id), _, _, _, _)>
        struct parse_nodes__categorize_node<args_3AAB(I, R(v1::node::type::INTERMEDIARY), _, _, _, _, _, _, _, _, _, _, _, _,
                                                      _, R(tuple::construct<I, args__intermediary_node_id...>), _, _, _, _)> :
          parse_nodes__next<args_3AAC(I, R(arg__pos + 1), _, _, _, _, _, _, _, _, _, _, R(arg__node_id + 1),
                                      _, R(tuple::construct<I, args__intermediary_node_id..., dict_Entry_key<I, arg__node_id>>), _, _, _, _, X)> {
        };

        template<prms_3AAB(I, X, _, _, _, _, _, _, _, _, _, _, _, _,
                           _, _, R(typename... args__actuator_node_id), _, _, _)>
        struct parse_nodes__categorize_node<args_3AAB(I, R(v1::node::type::ACTUATOR), _, _, _, _, _, _, _, _, _, _, _, _,
                                                      _, _, R(tuple::construct<I, args__actuator_node_id...>), _, _, _)> :
          parse_nodes__next<args_3AAC(I, R(arg__pos + 1), _, _, _, _, _, _, _, _, _, _, R(arg__node_id + 1),
                                      _, _, R(tuple::construct<I, args__actuator_node_id..., dict_Entry_key<I, arg__node_id>>), _, _, _, X)> {
        };

        

        template<prms_2E(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes__check_result :
          arg__size_checker_msg<(tuple::size<I, arg__tuple_construct__of__dict_Entry__of__Node__node_dict>::value
                                 == arg__node_id_test_array__sensor::size)
                                && arg__node_id_test_array__sensor::size == arg__node_id_test_array__intermediary::size
                                && arg__node_id_test_array__intermediary::size == arg__node_id_test_array__actuator::size
                                && arg__node_id_test_array__actuator::size == (arg__node_id_array__sensor::size
                                                                               + arg__node_id_array__intermediary::size
                                                                               + arg__node_id_array__actuator::size)
                                && (tuple::size<I, arg__tuple_construct__of__dict_Entry__of__Node__node_dict>::value > 0),
                                tuple::size<I, arg__tuple_construct__of__dict_Entry__of__Node__node_dict>::value,
                                arg__node_id_test_array__sensor::size, arg__node_id_test_array__intermediary::size,
                                arg__node_id_test_array__actuator::size, arg__node_id_array__sensor::size, arg__node_id_array__intermediary::size,
                                arg__node_id_array__actuator::size>,
          parse_nodes__check_totality_and_disjointness<args_3B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                               R(tuple::size<I, arg__tuple_construct__of__dict_Entry__of__Node__node_dict>::value),
                                                               X, X)> {
        };

        

        template<prms_3B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_nodes__check_totality_and_disjointness :
          arg__totality_checker_msg<arg__node_id_test_array__sensor::elems[arg__i] || arg__node_id_test_array__intermediary::elems[arg__i]
                                    || arg__node_id_test_array__actuator::elems[arg__i], arg__i + 1>,
          arg__disjoint_sensor_intermediary_checker_msg<!(arg__node_id_test_array__sensor::elems[arg__i]
                                                          && arg__node_id_test_array__intermediary::elems[arg__i]), arg__i + 1>,
          arg__disjoint_sensor_actuator_checker_msg<!(arg__node_id_test_array__sensor::elems[arg__i]
                                                      && arg__node_id_test_array__actuator::elems[arg__i]), arg__i + 1>,
          arg__disjoint_intermediary_actuator_checker_msg<!(arg__node_id_test_array__intermediary::elems[arg__i]
                                                            && arg__node_id_test_array__actuator::elems[arg__i]), arg__i + 1>,
          parse_nodes__check_totality_and_disjointness<args_3B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(arg__i + 1), X)> {
        };

        template<prms_3B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, X)>
        struct parse_nodes__check_totality_and_disjointness<args_3B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(true))> :
          parse_nodes__check_node_ids<args_2CA(I, R(arg__node_id_test_array__sensor), R(arg__node_id_array__sensor),
                                               R(arg__sensor_node_id_checker_msg), R(arg__node_id_array__sensor::size), X, X)>,
          parse_nodes__check_node_ids<args_2CA(I, R(arg__node_id_test_array__intermediary), R(arg__node_id_array__intermediary),
                                               R(arg__intermediary_node_id_checker_msg), R(arg__node_id_array__intermediary::size), X, X)>,
          parse_nodes__check_node_ids<args_2CA(I, R(arg__node_id_test_array__actuator), R(arg__node_id_array__actuator),
                                               R(arg__actuator_node_id_checker_msg), R(arg__node_id_array__actuator::size), X, X)> {
        };

        

        template<prms_2CA(I, _, _, _, _, _, X)>
        struct parse_nodes__check_node_ids<args_2CA(I, _, _, _, _, _, R(true))> {
        };

        template<prms_2CA(I, _, _, _, _, _, _)>
        struct parse_nodes__check_node_ids :
          arg__node_id_checker_msg<arg__node_id_test_array::elems[arg__node_id_array::elems[arg__i]], arg__i + 1>,
          parse_nodes__check_node_ids<args_2CA(I, _, _, _, _, R(arg__i + 1), X)> {
        };

#include "v1_internals_end.hpp"
      }
      namespace program {

#include "v1_internals_begin.hpp"

#define base_1(_d5, _d6, I, _1, _2, _3, _4, _5, _6) I                                   \
        opts  (_1, arg__holder_pos, ())                                                 \
          opts(_2, arg__node_pos, ())                                                   \
          opts(_3, arg__tuple_construct__of__dict_Entry__of__Node__node_dict, ())       \
          opts(_4, arg__Node__node, ())                                                 \
          opts(_5, arg__unspecified_node_checker_msg, _d5)                              \
        opts  (_6, arg__dict_Entry_of_Node_checker_msg, _d6)
#define decl_1(_d5, _d6, I, _1, _2, _3, _4, _5, _6)                                                                                     \
        base_1(_d5, _d6, I##I,                                                                                                          \
               type(_1, v1::error::Pos),                                                                                                \
               type(_2, v1::error::Pos),                                                                                                \
               type(_3, typename),                                                                                                      \
               type(_4, typename),                                                                                                      \
               type(_5, template<bool arg__has_been_specified_, v1::error::Pos arg__holder_pos_, v1::error::Pos arg__node_pos_> class), \
               type(_6, template<H(bool arg__is_tuple_construct_of_dict_Entry_of_Node_, v1::error::Pos arg__element_pos_,               \
                                   typename arg__actual_parameter_)> class))
#define args_1(I, _1, _2, _3, _4, _5, _6) base_1((), (), I, _1, _2, _3, _4, _5, _6)
#define prms_1(...) decl_1((), (), __VA_ARGS__)

#define base_2A(_d1, _d2, _d3, _d4, args_1, _1, _2, _3, _4)     \
        args_1                                                  \
        opts  (_1, arg__tuple_checker_msg, _d1)                 \
          opts(_2, arg__nonempty_tuple_checker_msg, _d2)        \
          opts(_3, arg__dict_Entry_checker_msg, _d3)            \
          opts(_4, arg__node_checker_msg, _d4)
#define decl_2A(_1_d5, _1_d6, _d1, _d2, _d3, _d4, I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1, _2, _3, _4)                \
        base_2A(_d1, _d2, _d3, _d4,                                                                                     \
                decl_1(_1_d5, _1_d6, I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6),                                            \
                type(_1, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class),               \
                type(_2, template<bool arg__is_nonempty_tuple_construct_> class),                                       \
                type(_3, template<bool arg__is_tuple_construct_of_dict_Entry_, typename arg__actual_parameter_> class), \
                type(_4, template<bool arg__is_Node_, typename arg__actual_parameter_> class))
#define args_2A(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1, _2, _3, _4)                          \
        base_2A((), (), (), (), args_1(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6), _1, _2, _3, _4)
#define prms_2A(...) decl_2A((), (), (), (), (), (), __VA_ARGS__)

        template<decl_2A(_1_d5(error::program::get_node_id::node_has_been_specified),
                         _1_d6(error::program::get_node_id::arg_4_is_tuple_construct_of_dict_Entry_of_Node),
                         _d1  (error::program::get_node_id::arg_4_is_tuple_construct),
                         _d2  (error::program::get_node_id::arg_4_is_nonempty_tuple_construct),
                         _d3  (error::program::get_node_id::arg_4_is_tuple_construct_of_dict_Entry),
                         _d4  (error::program::get_node_id::arg_5_is_Node),
                         I, _, _, _, _, _, _, _, _, _, _)>
        struct get_node_id
        /* {
         *   with template<typename I, typename... args__dict_Entry__of__Node>
         *        tuple::construct<I, args__dict_Entry__of__Node...> = arg__tuple_construct__of__dict_Entry__of__Node__node_dict
         *   {
         *     let tuple::construct<I, args__dict_Entry__of__Node...@[&](std::size_t i, auto arg__dict_Entry) {
         *                               return {std::is_same<typename arg__dict_Entry::value, arg__Node__node>::value,
         *                                       dict_Entry_key<I, arg__dict_Entry::key>};
         *                             }> result;
         *     invariant {
         *       tuple::size<I, result>::value > 0;
         *     }
         *     typedef typename tuple::get_pos<I, 1, result>::value value;
         *   }
         * }
         */;

#define base_2B(_d1, args_1, _1)                \
        args_1                                  \
        opts  (_1, arg__element_pos, _d1)
#define decl_2B(_d1, I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1) \
        base_2B(_d1,                                            \
                prms_1(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6),  \
                type(_1, v1::error::Pos))
#define args_2B(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1) base_2B((), args_1(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6), _1)
#define prms_2B(...) decl_2B((), __VA_ARGS__)

        template<decl_2B(_d1(1),
                         I, _, _, _, _, _, _, _)>
        struct get_node_id__find_node;

        

        template<prms_2A(I, _, _, _, _, _, _, _, _, _, _)>
        struct get_node_id :
          arg__tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct__of__dict_Entry__of__Node__node_dict> {
        };

        template<prms_2A(I, _, _, X, _, _, _, _, _, _, _)>
        struct get_node_id<args_2A(I, _, _, R(tuple::construct<I>), _, _, _, _, _, _, _)> :
          arg__nonempty_tuple_checker_msg<!!utility::always_false<I>()> {
        };

        template<prms_2A(I, _, _, R(typename arg, typename... args), _, _, _, _, _, _, _)>
        struct get_node_id<args_2A(I, _, _, R(tuple::construct<I, arg, args...>), _, _, _, _, _, _, _)> :
          arg__node_checker_msg<!!utility::always_false<I>(), arg__Node__node> {
        };

        template<prms_2A(I, _, _, R(typename arg, typename... args), R(typename... args__node_arg), _, _, _, _, _, _)>
        struct get_node_id<args_2A(I, _, _, R(tuple::construct<I, arg, args...>), R(Node<args__node_arg...>), _, _, _, _, _, _)> :
          get_node_id__find_node<args_2B(I, _, _, R(tuple::construct<I, arg, args...>), R(Node<args__node_arg...>), _, _, X)> {
        };

        

#define prms_2B_1(I, _1_3, _1_4) prms_2B(I, _, _, _1_3, _1_4, _, _, _)
#define args_2B_1(I, _1_3, _1_4) args_2B(I, _, _, _1_3, _1_4, _, _, _)

        template<prms_2B_1(I, R(typename arg, typename... args), _)>
        struct get_node_id__find_node<args_2B_1(I, R(tuple::construct<I, arg, args...>), _)> :
          arg__dict_Entry_of_Node_checker_msg<!!utility::always_false<I>(), arg__element_pos, arg> {
        };

        template<prms_2B_1(I, X, _)>
        struct get_node_id__find_node<args_2B_1(I, R(tuple::construct<I>), _)> :
          arg__unspecified_node_checker_msg<!!utility::always_false<I>(), arg__holder_pos, arg__node_pos> {
        };

        template<prms_2B_1(I, R(v1::dict::Key arg__node_id, typename... args__node_arg, typename... args), _)>
        struct get_node_id__find_node<args_2B_1(I, R(tuple::construct<I, dict_Entry<I, arg__node_id, Node<args__node_arg...>>, args...>), _)> :
          get_node_id__find_node<args_2B_1(I, R(tuple::construct<I, args...>), _)> {
        };

        template<prms_2B_1(I, R(v1::dict::Key arg__node_id, typename... args), R(typename... args__node_arg))>
        struct get_node_id__find_node<args_2B_1(I, R(tuple::construct<I, dict_Entry<I, arg__node_id, Node<args__node_arg...>>, args...>),
                                                R(Node<args__node_arg...>))>
        {
          typedef dict_Entry_key<I, arg__node_id> value;
        };

#include "v1_internals_end.hpp"
      }
      namespace program {

#include "v1_internals_begin.hpp"

#define base_1A(I, _1) I                        \
        opts  (_1, arg__channel_type, ())
#define decl_1A(I, _1)                          \
        base_1A(I##I,                           \
                type(_1, typename))
#define args_1A(I, _1) base_1A(I, _1)
#define prms_1A(...) decl_1A(__VA_ARGS__)

#define base_1B(I, _1, _2) I                    \
        opts  (_1, arg__producer_id, ())        \
          opts(_2, arg__consumer_id, ())
#define decl_1B(I, _1, _2)                      \
        base_1B(I##I,                           \
                type(_1, v1::dict::Key),        \
                type(_2, v1::dict::Key))
#define args_1B(I, _1, _2) base_1B(I, _1, _2)
#define prms_1B(...) decl_1B(__VA_ARGS__)

#define base_2A(args_1A, args_1B)               \
        args_1A                                 \
        args_1B
#define decl_2A(I, _1A_1, _1B_1, _1B_2)         \
        base_2A(prms_1A(I, _1A_1),              \
                prms_1B(, _1B_1, _1B_2))
#define args_2A(I, _1A_1, _1B_1, _1B_2) base_2A(args_1A(I, _1A_1), args_1B(, _1B_1, _1B_2))
#define prms_2A(...) decl_2A(__VA_ARGS__)

        template<prms_2A(I, _, _, _)>
        struct Channel__common;

#define base_2B(_d2, args_1B, _1, _2)                           \
        args_1B                                                 \
        opts  (_1, arg__Chan_inlit_or_Chan__channel, ())        \
          opts(_2, arg__channel_checker_msg, _d2)
#define decl_2B(_d2, I, _1B_1, _1B_2, _1, _2)                                                                   \
        base_2B(_d2,                                                                                            \
                prms_1B(I, _1B_1, _1B_2),                                                                       \
                type(_1, typename),                                                                             \
                type(_2, template<bool arg__is_Chan_inlit_or_Chan_, typename arg__actual_parameter_> class))
#define args_2B(I, _1B_1, _1B_2, _1, _2) base_2B((), args_1B(I, _1B_1, _1B_2), _1, _2)
#define prms_2B(...) decl_2B((), __VA_ARGS__)

        template<decl_2B(_d2(error::program::Channel::arg_3_is_either_Chan_inlit_or_Chan),
                         I, _, _, _, _)>
        struct Channel;

        

        template<prms_2B(I, _, _, _, _)>
        struct Channel :
          arg__channel_checker_msg<!!utility::always_false<I>(), arg__Chan_inlit_or_Chan__channel> {
        };

        template<prms_2B(I, _, _, R(typename arg__channel_type, std::add_pointer_t<std::add_const_t<arg__channel_type>> arg__initial_value_ptr), _)>
        struct Channel<args_2B(I, _, _, R(Chan<arg__channel_type, arg__initial_value_ptr>), _)> :
          utility::run_metaprogram<I, Chan<arg__channel_type, arg__initial_value_ptr>>,
          Channel__common<args_2A(I, R(arg__channel_type), _, _)>
        {
          static constexpr const arg__channel_type &initial_value {*arg__initial_value_ptr};
        };

        template<prms_2B(I, _, _, R(typename arg__channel_type, arg__channel_type arg__initial_value), _)>
        struct Channel<args_2B(I, _, _, R(Chan_inlit<arg__channel_type, arg__initial_value>), _)> :
          utility::run_metaprogram<I, Chan_inlit<arg__channel_type, arg__initial_value>>,
          Channel__common<args_2A(I, R(arg__channel_type), _, _)>
#ifndef TICE_V1_NOGEN
          , code::lvalue<arg__channel_type, arg__initial_value>
#endif // TICE_V1_NOGEN
        {
          static constexpr const arg__channel_type &initial_value {
#ifdef TICE_V1_NOGEN
            arg__initial_value
#else  // TICE_V1_NOGEN
            code::lvalue<arg__channel_type, arg__initial_value>::value
#endif // TICE_V1_NOGEN
          };
        };

        

        template<prms_2A(I, _, _, _)>
        struct Channel__common
        {
          typedef arg__channel_type channel_type;
          static constexpr v1::dict::Key producer_id {arg__producer_id};
          static constexpr v1::dict::Key consumer_id {arg__consumer_id};
        };

#include "v1_internals_end.hpp"
      }
      namespace program {

        template<typename I, v1::array::Idx... args__idx>
        struct Array_idx_list;

      }
      namespace program {

        template<typename I, v1::dict::Key arg__producer_id, v1::dict::Key arg__consumer_id, v1::array::Idx arg__channel_idx>
        struct Arc
        {
          static constexpr v1::dict::Key prod_id {arg__producer_id};
          static constexpr v1::dict::Key cons_id {arg__consumer_id};
          static constexpr v1::array::Idx chan_idx {arg__channel_idx};
        };

      }
      namespace program {
        namespace adjacency_list {

          typedef v1::dict::Invalid_key EOL; // stands for end-of-line (an adjacency list has one or more lines)

        }
      }
      namespace program {

        template<typename I, v1::dict::Key arg__key, v1::dict::Key arg__node_id>
        using dict_Entry_node_id = dict_Entry<I, arg__key, void, std::integral_constant<v1::dict::Key, arg__node_id>>;

      }
      namespace program {

        template<typename I, v1::dict::Key arg__key, v1::array::Idx arg__array_idx>
        using dict_Entry_array_idx = dict_Entry<I, arg__key, void, std::integral_constant<v1::array::Idx, arg__array_idx>>;

      }
      namespace program {

#include "v1_internals_begin.hpp"

#define base_1A(I, _1) I                        \
        opts  (_1, arg__pos, ())
#define decl_1A(I, _1)                          \
        base_1A(I##I,                           \
                type(_1, v1::error::Pos))
#define args_1A(I, _1) base_1A(I, _1)
#define prms_1A(...) decl_1A(__VA_ARGS__)

#define base_2A(args_1A, _1)                                                            \
        args_1A                                                                         \
        opts  (_1, arg__tuple_construct__of__dict_Entry__of__Node__node_dict, ())
#define decl_2A(I, _1A_1, _1)                   \
        base_2A(prms_1A(I, _1A_1),              \
                type(_1, typename))
#define args_2A(I, _1A_1, _1) base_2A(args_1A(I, _1A_1), _1)
#define prms_2A(...) decl_2A(__VA_ARGS__)

#define base_1F(_d1, _d2, I, _1, _2) I                          \
        opts  (_1, arg__node_dict_element_checker_msg, _d1)     \
          opts(_2, arg__node_dict_id_checker_msg, _d2)
#define decl_1F(_d1, _d2, I, _1, _2)                                                                                            \
        base_1F(_d1, _d2, I##I,                                                                                                 \
                type(_1, template<H(bool arg__is_tuple_construct_of_dict_Entry_of_Node_, v1::error::Pos arg__element_pos_,      \
                                    typename arg__actual_parameter_)> class),                                                   \
                type(_2, template<H(bool arg__has_linearly_increasing_node_ids_starting_at_zero_with_gradient_one,              \
                                    v1::error::Pos arg__element_pos, v1::dict::Key arg__actual_node_id_)> class))
#define args_1F(I, _1, _2) base_1F((), (), I, _1, _2)
#define prms_1F(...) decl_1F((), (), __VA_ARGS__)

#define base_2F(args_2A, args_1F)               \
        args_2A                                 \
        args_1F
#define decl_2F(I, _1A_1, _2A_1, _1F_1, _1F_2)  \
        base_2F(prms_2A(I, _1A_1, _2A_1),       \
                prms_1F(, _1F_1, _1F_2))
#define args_2F(I, _1A_1, _2A_1, _1F_1, _1F_2) base_2F(args_2A(I, _1A_1, _2A_1), args_1F(, _1F_1, _1F_2))
#define prms_2F(...) decl_2F(__VA_ARGS__)

        template<prms_2F(I, _, _, _, _)>
        struct parse_feeders__validate_node_dict;

#define base_1B(_d1, I, _1) I                                   \
        opts  (_1, arg__specified_node_checker_msg, _d1)
#define decl_1B(_d1, I, _1)                                                                                             \
        base_1B(_d1, I##I,                                                                                              \
                type(_1, template<H(bool arg__is_already_specified_,                                                    \
                                    v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__node_pos_in_feeder_)> class))
#define args_1B(I, _1) base_1B((), I, _1)
#define prms_1B(...) decl_1B((), __VA_ARGS__)

#define base_1D(_d1, _d2, I, _1, _2) I                          \
        opts  (_1, arg__Array__of__bool__is_producer_node, _d1) \
          opts(_2, arg__arc_count, _d2)
#define decl_1D(_d1, _d2, I, _1, _2)            \
        base_1D(_d1, _d2, I##I,                 \
                type(_1, typename),             \
                type(_2, v1::array::Size))
#define args_1D(I, _1, _2) base_1D((), (), I, _1, _2)
#define prms_1D(...) decl_1D((), (), __VA_ARGS__)

#define base_2D(_d2, _d3, _d4, _d5, _d6, _d7, args_2A, args_1B, args_1D, _1, _2, _3, _4, _5, _6, _7)    \
        args_2A                                                                                         \
        args_1B                                                                                         \
        args_1D                                                                                         \
        opts(_1, arg__tuple_construct__feeder_args, ())                                                 \
        opts(_2, arg__node_pos, _d2)                                                                    \
        opts(_3, arg__tuple_construct__of__dict_Entry_key__producer_id, _d3)                            \
        opts(_4, arg__tuple_construct__of__dict_Entry__of__Chan_inlit_or_Chan__channel, _d4)            \
        opts(_5, arg__producer_id_min, _d5)                                                             \
        opts(_6, arg__producer_id_max, _d6)                                                             \
        opts(_7, arg__Array_idx_list__cons_channel_idx_list, _d7)
#define decl_2D(_d2, _d3, _d4, _d5, _d6, _d7, I, _1A_1, _2A_1, _1B_1, _1D_1, _1D_2, _1, _2, _3, _4, _5, _6, _7) \
        base_2D(_d2, _d3, _d4, _d5, _d6, _d7,                                                                   \
                prms_2A(I, _1A_1, _2A_1),                                                                       \
                prms_1B(, _1B_1),                                                                               \
                prms_1D(, _1D_1, _1D_2),                                                                        \
                type(_1, typename),                                                                             \
                type(_2, v1::error::Pos),                                                                       \
                type(_3, typename),                                                                             \
                type(_4, typename),                                                                             \
                type(_5, v1::dict::Key),                                                                        \
                type(_6, v1::dict::Key),                                                                        \
                type(_7, typename))
#define args_2D(I, _1A_1, _2A_1, _1B_1, _1D_1, _1D_2, _1, _2, _3, _4, _5, _6, _7)                                                               \
        base_2D((), (), (), (), (), (), args_2A(I, _1A_1, _2A_1), args_1B(, _1B_1), args_1D(, _1D_1, _1D_2), _1, _2, _3, _4, _5, _6, _7)
#define prms_2D(...) decl_2D((), (), (), (), (), (), __VA_ARGS__)

        template<decl_2D(_d2(1),
                         _d3(tuple::construct<I>),
                         _d4(tuple::construct<I>),
                         _d5(v1::dict::Max_key::value),
                         _d6(0),
                         _d7(Array_idx_list<I>),
                         I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__check_feeder_nodes;

#define base_1E(_d1, I, _1) I                   \
        opts  (_1, arg__idx, _d1)
#define decl_1E(_d1, I, _1)                     \
        base_1E(_d1, I##I,                      \
                type(_1, v1::array::Idx))
#define args_1E(I, _1) base_1E((), I, _1)
#define prms_1E(...) decl_1E((), __VA_ARGS__)

        template<prms_1E(I, _)>
        struct parse_feeders__next__empty_tuple;

#define base_1J(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18, _d19, _d20, _d21, _d22, _d23,   \
                I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23) I                     \
        opts          (_1 , arg__multiple_edge_checker_msg, _d1)                                                                                   \
                  opts(_2 , arg__loop_checker_msg, _d2)                                                                                            \
        opts          (_3 , arg__node_missing_msg, _d3)                                                                                            \
        opts          (_4 , arg__producer_node_checker_msg, _d4)                                                                                   \
        opts          (_5 , arg__consumer_node_checker_msg, _d5)                                                                                   \
        opts          (_6 , arg__param_count_checker_msg, _d6)                                                                                     \
        opts          (_7 , arg__consumer_invocation_checker_msg, _d7)                                                                             \
        opts          (_8 , arg__channel_missing_msg, _d8)                                                                                         \
        opts          (_9 , arg__channel_checker_msg, _d9)                                                                                         \
        opts          (_10, arg__channel_type_checker_msg, _d10)                                                                                   \
        opts          (_11, arg__channel_cv_checker_msg, _d11)                                                                                     \
        opts          (_12, arg__channel_value_checker_msg, _d12)                                                                                  \
        opts          (_13, arg__producer_return_value_checker_msg, _d13)                                                                          \
        opts          (_14, arg__channel_initval_checker_msg, _d14)                                                                                \
        opts          (_15, arg__channel_and_consumer_param_checker_msg, _d15)                                                                     \
        opts          (_16, arg__node_computation_checker_msg, _d16)                                                                               \
        opts          (_17, arg__node_computation_fn_ptr_checker_msg, _d17)                                                                        \
        opts          (_18, arg__node_computation_wcet_checker_msg, _d18)                                                                          \
        opts          (_19, arg__node_computation_wcet_den_checker_msg, _d19)                                                                      \
        opts          (_20, arg__node_computation_wcet_validator_msg, _d20)                                                                        \
        opts          (_21, arg__node_period_checker_msg, _d21)                                                                                    \
        opts          (_22, arg__node_period_den_checker_msg, _d22)                                                                                \
        opts          (_23, arg__node_period_validator_msg, _d23)
#define decl_1J(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18, _d19, _d20,                     \
                _d21, _d22, _d23, I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23)     \
        base_1J(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18, _d19, _d20, _d21, _d22, _d23,   \
                I##I,                                                                                                                              \
                type(_1, template<H(bool arg__has_no_multiple_edges_, v1::error::Pos arg__feeder_pos_,                                             \
                                    v1::error::Pos arg__producer_1_pos_, v1::error::Pos arg__producer_2_pos_)> class),                             \
                type(_2, template<H(bool arg__has_no_loop, v1::error::Pos arg__feeder_pos_,                                                        \
                                    v1::error::Pos arg__producer_pos, v1::error::Pos arg__consumer_pos_)> class),                                  \
                type(_3, template<bool arg__is_missing_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_> class),                        \
                type(_4, template<H(bool arg__is_producer_node_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                        \
                                    typename arg__actual_parameter_)> class),                                                                      \
                type(_5, template<H(bool arg__is_consumer_node_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                        \
                                    typename arg__actual_parameter_)> class),                                                                      \
                type(_6, template<H(bool arg__is_equal_, v1::error::Pos arg__feeder_pos_, unsigned arg__channel_count_,                            \
                                    unsigned arg__parameter_count_)> class),                                                                       \
                type(_7, template<bool arg__is_callable_, v1::error::Pos arg__feeder_pos_, typename arg__consumer_function_> class),               \
                type(_8, template<bool arg__is_missing_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_> class),                        \
                type(_9, template<H(bool arg__is_channel_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                              \
                                    typename arg__actual_parameter_)> class),                                                                      \
                type(_10, template<H(bool arg__is_object_type_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                         \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_11, template<H(bool arg__is_cv_unqualified_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                      \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_12, template<bool arg__is_not_nullptr_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_> class),                   \
                type(_13, template<H(bool arg__is_assignable_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__producer_pos_,                 \
                                     v1::error::Pos arg__channel_pos_, typename arg__return_type_, typename arg__channel_type_)> class),           \
                type(_14, template<H(bool arg__can_initialize_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__channel_pos_,                 \
                                     typename arg__init_val_type_, typename arg__channel_type_)> class),                                           \
                type(_15, template<H(bool arg__is_compatible_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__channel_pos_,                  \
                                     v1::error::Pos arg__parameter_pos_, typename arg__channel_type_, typename arg__parameter_type_)> class),      \
                type(_16, template<H(bool arg__is_comp_Unit_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                           \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_17, template<H(bool arg__is_function_pointer_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                    \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_18, template<H(bool arg__is_std_ratio_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                           \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_19, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_20, template<bool arg__is_positive_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_, typename arg__wcet_> class), \
                type(_21, template<H(bool arg__is_std_ratio_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                           \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_22, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_23, template<H(bool arg__is_greater_than_or_equal_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,               \
                                     typename arg__period_, typename arg__wcet_)> class))
#define args_1J(I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23)    \
        base_1J((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (),                     \
                I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23)
#define prms_1J(...) decl_1J((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

#define base_2J(args_1A, args_1J)               \
        args_1A                                 \
        args_1J
#define decl_2J(I, _1A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,        \
                _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23)                 \
        base_2J(prms_1A(I, _1A_1),                                                                                      \
                prms_1J(, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,        \
                        _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23))
#define args_2J(I, _1A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,                        \
                _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23)                                 \
        base_2J(args_1A(I, _1A_1), args_1J(, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,     \
                                           _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23))
#define prms_2J(...) decl_2J(__VA_ARGS__)

        template<prms_2J(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__error_pos_adaptor__feeder_msgs;

#define base_2B(_d1, args_1J, _1, args_1B)                      \
        args_1J                                                 \
        opts  (_1, arg__distinct_feeder_checker_msg, _d1)       \
          args_1B
#define decl_2B(_1J_d1, _1J_d2, _1J_d3, _1J_d4, _1J_d5, _1J_d6, _1J_d7, _1J_d8, _1J_d9, _1J_d10, _1J_d11, _1J_d12,                              \
                _1J_d13, _1J_d14, _1J_d15, _1J_d16, _1J_d17, _1J_d18, _1J_d19, _1J_d20, _1J_d21, _1J_d22, _1J_d23, _d1, _1B_d1,                 \
                I, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,                                       \
                _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23, _1, _1B_1)                              \
        base_2B(_d1,                                                                                                                            \
                decl_1J(_1J_d1, _1J_d2, _1J_d3, _1J_d4, _1J_d5, _1J_d6, _1J_d7, _1J_d8, _1J_d9, _1J_d10, _1J_d11, _1J_d12,                      \
                        _1J_d13, _1J_d14, _1J_d15, _1J_d16, _1J_d17, _1J_d18, _1J_d19, _1J_d20,                                                 \
                        _1J_d21, _1J_d22, _1J_d23, I, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,    \
                        _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23),                                \
                type(_1, template<bool arg__are_distinct_, v1::error::Pos arg__feeder_1_pos_, v1::error::Pos arg__feeder_2_pos_> class),        \
                decl_1B(_1B_d1, , _1B_1))
#define args_2B(I, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,                                       \
                _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23, _1, _1B_1)                              \
        base_2B((), args_1J(I, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,                           \
                            _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23), _1, args_1B(, _1B_1))
#define prms_2B(...) decl_2B((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

#define base_3A(args_2A, _1)                            \
        args_2A                                         \
        opts  (_1, arg__tuple_construct__input, ())
#define decl_3A(I, _1A_1, _2A_1, _1)            \
        base_3A(prms_2A(I, _1A_1, _2A_1),       \
                type(_1, typename))
#define args_3A(I, _1A_1, _2A_1, _1) base_3A(args_2A(I, _1A_1, _2A_1), _1)
#define prms_3A(...) decl_3A(__VA_ARGS__)

#define base_3B(_d1, _d2, _d3, _d4, args_3A, args_2B, args_1D, _1, _2, _3, _4)                          \
        args_3A                                                                                         \
        args_2B                                                                                         \
        args_1D                                                                                         \
        opts(_1, arg__tuple_construct__of__tuple_construct__of__dict_Entry_array_idx__arc_list, _d1)    \
        opts(_2, arg__tuple_construct__of__dict_Entry__of__Node__consumer_list, _d2)                    \
        opts(_3, arg__tuple_construct__of__Channel__channel_list, _d3)                                  \
        opts(_4, arg__tuple_construct__of__dict_Entry__of__Array_idx_list__cons_channel_idx_list, _d4)
#define decl_3B(_1D_d1, _1D_d2, _d1, _d2, _d3, _d4,                                                                             \
                I, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,  \
                _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                         \
                _2B_1, _1B_1, _1D_1, _1D_2, _1, _2, _3, _4)                                                                     \
        base_3B(_d1, _d2, _d3, _d4,                                                                                             \
                prms_3A(I, _1A_1, _2A_1, _3A_1),                                                                                \
                prms_2B(, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,                \
                        _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23, _2B_1, _1B_1),  \
                decl_1D(_1D_d1, _1D_d2, , _1D_1, _1D_2),                                                                        \
                type(_1, typename),                                                                                             \
                type(_2, typename),                                                                                             \
                type(_3, typename),                                                                                             \
                type(_4, typename))
#define args_3B(I, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,  \
                _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                         \
                _2B_1, _1B_1, _1D_1, _1D_2, _1, _2, _3, _4)                                                                     \
        base_3B((), (), (), (), args_3A(I, _1A_1, _2A_1, _3A_1),                                                                \
                args_2B(, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,                \
                        _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23, _2B_1, _1B_1),  \
                args_1D(, _1D_1, _1D_2), _1, _2, _3, _4)
#define prms_3B(...) decl_3B((), (), (), (), (), (), __VA_ARGS__)

#define base_4BC(_d1, args_3B, _1)                                              \
        args_3B                                                                 \
        opts  (_1, arg__parse_feeders__error_pos_adaptor__feeder_msgs, _d1)
#define decl_4BC(_1D_d1, _1D_d2, _3B_d1, _3B_d2, _3B_d3, _3B_d4, _d1,                                                                   \
                 I, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,         \
                 _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                                \
                 _2B_1, _1B_1, _1D_1, _1D_2, _3B_1, _3B_2, _3B_3, _3B_4, _1)                                                            \
        base_4BC(_d1,                                                                                                                   \
                 decl_3B(_1D_d1, _1D_d2, _3B_d1, _3B_d2, _3B_d3, _3B_d4,                                                                \
                         I, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12, \
                         _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                        \
                         _2B_1, _1B_1, _1D_1, _1D_2, _3B_1, _3B_2, _3B_3, _3B_4),                                                       \
                 type(_1, typename))
#define args_4BC(I, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,                 \
                 _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                                        \
                 _2B_1, _1B_1, _1D_1, _1D_2, _3B_1, _3B_2, _3B_3, _3B_4, _1)                                                                    \
        base_4BC((), args_3B(I, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,     \
                             _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                            \
                             _2B_1, _1B_1, _1D_1, _1D_2, _3B_1, _3B_2, _3B_3, _3B_4), _1)
#define prms_4BC(...) decl_4BC((), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_4BC(_1D_d1(node_id_test_array<H(I, tuple::size<I, arg__tuple_construct__of__dict_Entry__of__Node__node_dict>::value,
                                                      tuple::construct<I>)>),
                          _1D_d2(0),
                          _3B_d1(typename utility::list::make<H(I, tuple::size<I, arg__tuple_construct__of__dict_Entry__of__Node__node_dict>::value,
                                                                parse_feeders__next__empty_tuple)>::value),
                          _3B_d2(tuple::construct<I>),
                          _3B_d3(tuple::construct<I>),
                          _3B_d4(tuple::construct<I>),
                          _d1   (parse_feeders__error_pos_adaptor__feeder_msgs<args_2J(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                                       _, _, _, _, _)>),
                          I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__next;

#define base_2EC(args_1E, _1, _2)               \
        args_1E                                 \
        opts  (_1, arg__prev_list_element, ())  \
          opts(_2, arg__updater_data, ())
#define decl_2EC(I, _1E_1, _1, _2)              \
        base_2EC(prms_1E(I, _1E_1),             \
                 type(_1, typename),            \
                 type(_2, typename))
#define args_2EC(I, _1E_1, _1, _2) base_2EC(args_1E(I, _1E_1), _1, _2)
#define prms_2EC(...) decl_2EC(__VA_ARGS__)

        template<prms_2EC(I, _, _, _)>
        struct parse_feeders__next__make_adjacency_list;

#define base_3EC(args_2EC)                      \
        args_2EC
#define decl_3EC(I, _1E_1, _2EC_1, _2EC_2)              \
        base_3EC(prms_2EC(I, _1E_1, _2EC_1, _2EC_2))
#define args_3EC(I, _1E_1, _2EC_1, _2EC_2) base_3EC(args_2EC(I, _1E_1, _2EC_1, _2EC_2))
#define prms_3EC(...) decl_3EC(__VA_ARGS__)

        template<prms_3EC(I, _, _, _)>
        struct parse_feeders__next__make_channel_idx_list;

#define base_4BA(I, _1, args_3B) I                      \
        opts  (_1, arg__Feeder__valid_feeder, ())       \
          args_3B
#define decl_4BA(I, _1, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,     \
                 _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                                \
                 _2B_1, _1B_1, _1D_1, _1D_2, _3B_1, _3B_2, _3B_3, _3B_4)                                                                \
        base_4BA(I##I,                                                                                                                  \
                 type(_1, typename),                                                                                                    \
                 prms_3B(, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,  \
                         _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                        \
                         _2B_1, _1B_1, _1D_1, _1D_2, _3B_1, _3B_2, _3B_3, _3B_4))
#define args_4BA(I, _1, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,             \
                 _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                                        \
                 _2B_1, _1B_1, _1D_1, _1D_2, _3B_1, _3B_2, _3B_3, _3B_4)                                                                        \
        base_4BA(I, _1, args_3B(, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,   \
                                _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                         \
                                _2B_1, _1B_1, _1D_1, _1D_2, _3B_1, _3B_2, _3B_3, _3B_4))
#define prms_4BA(...) decl_4BA(__VA_ARGS__)

        template<prms_4BA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__next__feeder_validated;

#define base_4BB(I, _1, args_3B) I              \
        opts  (_1, arg__parse_result, ())       \
          args_3B
#define decl_4BB(I, _1, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,     \
                 _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                                \
                 _2B_1, _1B_1, _1D_1, _1D_2, _3B_1, _3B_2, _3B_3, _3B_4)                                                                \
        base_4BB(I##I,                                                                                                                  \
                 type(_1, typename),                                                                                                    \
                 prms_3B(, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,  \
                         _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                        \
                         _2B_1, _1B_1, _1D_1, _1D_2, _3B_1, _3B_2, _3B_3, _3B_4))
#define args_4BB(I, _1, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,             \
                 _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                                        \
                 _2B_1, _1B_1, _1D_1, _1D_2, _3B_1, _3B_2, _3B_3, _3B_4)                                                                        \
        base_4BB(I, _1, args_3B(, _1A_1, _2A_1, _3A_1, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,   \
                                _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23,                         \
                                _2B_1, _1B_1, _1D_1, _1D_2, _3B_1, _3B_2, _3B_3, _3B_4))
#define prms_4BB(...) decl_4BB(__VA_ARGS__)

        template<prms_4BB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__next__parsed;

#define base_1G(_d5, I, _1, _2, _3, _4, _5) I                                                   \
        opts  (_1, arg__dict_Entry_key__producer_id_min, ())                                    \
          opts(_2, arg__dict_Entry_key__producer_id_max, ())                                    \
          opts(_3, arg__is_less_than_min, ())                                                   \
          opts(_4, arg__tuple_construct__of__Arc__unchecked_for_current_list_element, ())       \
          opts(_5, arg__tuple_construct__of__Arc__checked_for_current_list_element, _d5)
#define decl_1G(_d5, I, _1, _2, _3, _4, _5)     \
        base_1G(_d5, I##I,                      \
                type(_1, typename),             \
                type(_2, typename),             \
                type(_3, bool),                 \
                type(_4, typename),             \
                type(_5, typename))
#define args_1G(I, _1, _2, _3, _4, _5) base_1G((), I, _1, _2, _3, _4, _5)
#define prms_1G(...) decl_1G((), __VA_ARGS__)

        template<decl_1G(_d5(tuple::construct<I>),
                         I, _, _, _, _, _)>
        struct parse_feeders__update_arc_list__data;

#define base_2EA(args_1E, _1, _2)                                                               \
        args_1E                                                                                 \
        opts  (_1, arg__tuple_construct__of__dict_Entry_array_idx__prev_arc_list_element, ())   \
          opts(_2, arg__parse_feeders__update_arc_list__data__update_state, ())
#define decl_2EA(I, _1E_1, _1, _2)              \
        base_2EA(prms_1E(I, _1E_1),             \
                 type(_1, typename),            \
                 type(_2, typename))
#define args_2EA(I, _1E_1, _1, _2) base_2EA(args_1E(I, _1E_1), _1, _2)
#define prms_2EA(...) decl_2EA(__VA_ARGS__)

        template<prms_2EA(I, _, _, _)>
        struct parse_feeders__update_arc_list;

#define base_1C(_d2, I, _1, _2) I               \
        opts  (_1, arg__node_starting_idx, ())  \
          opts(_2, arg__check_cycle_msg, _d2)
#define decl_1C(_d2, I, _1, _2)                                                                                 \
        base_1C(_d2, I##I,                                                                                      \
                type(_1, v1::array::Idx),                                                                       \
                type(_2, template<bool arg__has_no_cycle_, typename arg__node_pos_forming_closed_path_> class))
#define args_1C(I, _1, _2) base_1C((), I, _1, _2)
#define prms_1C(...) decl_1C((), __VA_ARGS__)

#define base_1H(I, _1) I                                                        \
        opts  (_1, arg__Array__of__Array__of__dict_Key__adjacency_list, ())
#define decl_1H(I, _1)                          \
        base_1H(I##I,                           \
                type(_1, typename))
#define args_1H(I, _1) base_1H(I, _1)
#define prms_1H(...) decl_1H(__VA_ARGS__)

#define base_2EB(_d1, args_1E, _1)              \
        args_1E                                 \
        opts  (_1, arg__is_done, _d1)
#define decl_2EB(_1E_d1, _d1, I, _1E_1, _1)     \
        base_2EB(_d1,                           \
                 decl_1E(_1E_d1, I, _1E_1),     \
                 type(_1, bool))
#define args_2EB(I, _1E_1, _1) base_2EB((), args_1E(I, _1E_1), _1)
#define prms_2EB(...) decl_2EB((), (), __VA_ARGS__)

#define base_2CC(_d2, args_1H, args_1C, _1, _2, args_2EB)       \
        args_1H                                                 \
        args_1C                                                 \
        opts  (_1, arg__cycle_starting_node_id, ())             \
          opts(_2, arg__DAG_cycle_forming_node_pos, _d2)        \
          args_2EB
#define decl_2CC(_d2, _1E_d1, _2EB_d1, I, _1H_1, _1C_1, _1C_2, _1, _2, _1E_1, _2EB_1)   \
        base_2CC(_d2,                                                                   \
                 prms_1H(I, _1H_1),                                                     \
                 prms_1C(, _1C_1, _1C_2),                                               \
                 type(_1, v1::dict::Key),                                               \
                 type(_2, typename),                                                    \
                 decl_2EB(_1E_d1, _2EB_d1, , _1E_1, _2EB_1))
#define args_2CC(I, _1H_1, _1C_1, _1C_2, _1, _2, _1E_1, _2EB_1)                                         \
        base_2CC((), args_1H(I, _1H_1), args_1C(, _1C_1, _1C_2), _1, _2, args_2EB(, _1E_1, _2EB_1))
#define prms_2CC(...) decl_2CC((), (), (), __VA_ARGS__)

        template<decl_2CC(_d2    (v1::error::DAG_cycle_forming_node_pos<>),
                          _1E_d1 (arg__cycle_starting_node_id),
                          _2EB_d1(arg__Array__of__Array__of__dict_Key__adjacency_list::elems[arg__idx][0] == arg__cycle_starting_node_id),
                          I, _, _, _, _, _, _, _)>
        struct parse_feeders__report_cycle;

#define base_2H(args_1H, _1)                                            \
        args_1H                                                         \
        opts  (_1, arg__Array__of__dict_Key__source_node_ids, ())
#define decl_2H(I, _1H_1, _1)                   \
        base_2H(prms_1H(I, _1H_1),              \
                type(_1, typename))
#define args_2H(I, _1H_1, _1) base_2H(args_1H(I, _1H_1), _1)
#define prms_2H(...) decl_2H(__VA_ARGS__)

#define base_2CA(I, _1, _2, args_1C) I                          \
        opts  (_1, arg__Array__of__bool__is_producer_node, ())  \
          opts(_2, arg__Array__of__bool__is_sink_node, ())      \
          args_1C
#define decl_2CA(I, _1, _2, _1C_1, _1C_2)       \
        base_2CA(I##I,                          \
                 type(_1, typename),            \
                 type(_2, typename),            \
                 prms_1C(, _1C_1, _1C_2))
#define args_2CA(I, _1, _2, _1C_1, _1C_2) base_2CA(I, _1, _2, args_1C(, _1C_1, _1C_2))
#define prms_2CA(...) decl_2CA(__VA_ARGS__)

#define base_3CA(args_2H, args_2CA, _1)                                         \
        args_2H                                                                 \
        args_2CA                                                                \
        opts  (_1, arg__parse_feeders__producer_and_sink_adjacency_matrix, ())
#define decl_3CA(I, _1H_1, _2H_1, _2CA_1, _2CA_2, _1C_1, _1C_2, _1)     \
        base_3CA(prms_2H(I, _1H_1, _2H_1),                              \
                 prms_2CA(, _2CA_1, _2CA_2, _1C_1, _1C_2),              \
                 type(_1, typename))
#define args_3CA(I, _1H_1, _2H_1, _2CA_1, _2CA_2, _1C_1, _1C_2, _1)                             \
        base_3CA(args_2H(I, _1H_1, _2H_1), args_2CA(, _2CA_1, _2CA_2, _1C_1, _1C_2), _1)
#define prms_3CA(...) decl_3CA(__VA_ARGS__)

        template<prms_3CA(I, _, _, _, _, _, _, _)>
        struct parse_feeders__traverse_graph_depth_first_with_memoization;

#define base_1I(I, _1, _2, _3, _4) I                                    \
        opts  (_1, arg__Array__of__dict_Key__producer_node_ids, ())     \
          opts(_2, arg__Array__of__dict_Key__sink_node_ids, ())         \
          opts(_3, arg__Array__of__bool__is_source_node, ())            \
          opts(_4, arg__Array__of__bool__is_isolated_node, ())
#define decl_1I(I, _1, _2, _3, _4)              \
        base_1I(I##I,                           \
                type(_1, typename),             \
                type(_2, typename),             \
                type(_3, typename),             \
                type(_4, typename))
#define args_1I(I, _1, _2, _3, _4) base_1I(I, _1, _2, _3, _4)
#define prms_1I(...) decl_1I(__VA_ARGS__)

#define base_2I(args_2H, args_1I)               \
        args_2H                                 \
        args_1I
#define decl_2I(I, _1H_1, _2H_1, _1I_1, _1I_2, _1I_3, _1I_4)    \
        base_2I(prms_2H(I, _1H_1, _2H_1),                       \
                prms_1I(, _1I_1, _1I_2, _1I_3, _1I_4))
#define args_2I(I, _1H_1, _2H_1, _1I_1, _1I_2, _1I_3, _1I_4) base_2I(args_2H(I, _1H_1, _2H_1), args_1I(, _1I_1, _1I_2, _1I_3, _1I_4))
#define prms_2I(...) decl_2I(__VA_ARGS__)

        template<prms_2I(I, _, _, _, _, _, _)>
        struct parse_feeders__producer_and_sink_adjacency_matrix;

#define base_3H(I, _1, _2, args_2H, args_2CA, args_1I) I        \
        opts  (_1, arg__has_arc, ())                            \
          opts(_2, arg__has_sink_node, ())                      \
          args_2H                                               \
          args_2CA                                              \
          args_1I
#define decl_3H(I, _1, _2, _1H_1, _2H_1, _2CA_1, _2CA_2, _1C_1, _1C_2, _1I_1, _1I_2, _1I_3, _1I_4)      \
        base_3H(I##I,                                                                                   \
                type(_1, bool),                                                                         \
                type(_2, bool),                                                                         \
                prms_2H(, _1H_1, _2H_1),                                                                \
                prms_2CA(, _2CA_1, _2CA_2, _1C_1, _1C_2),                                               \
                prms_1I(, _1I_1, _1I_2, _1I_3, _1I_4))
#define args_3H(I, _1, _2, _1H_1, _2H_1, _2CA_1, _2CA_2, _1C_1, _1C_2, _1I_1, _1I_2, _1I_3, _1I_4)                                      \
        base_3H(I, _1, _2, args_2H(, _1H_1, _2H_1), args_2CA(, _2CA_1, _2CA_2, _1C_1, _1C_2), args_1I(, _1I_1, _1I_2, _1I_3, _1I_4))
#define prms_3H(...) decl_3H(__VA_ARGS__)

        template<prms_3H(I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__check_graph;

#define base_3EB(_d2, _d3, I, _1, _2, _3, args_2EB) I                   \
        opts  (_1, arg__Array__of__dict_Key__node_ids, ())              \
          opts(_2, arg__idx_to_assign, _d2)                             \
          opts(_3, arg__tuple_construct__of__dict_Entry_node_id, _d3)   \
          args_2EB
#define decl_3EB(_d2, _d3, _1E_d1, _2EB_d1, I, _1, _2, _3, _1E_1, _2EB_1)       \
        base_3EB(_d2, _d3, I##I,                                                \
                 type(_1, typename),                                            \
                 type(_2, v1::array::Idx),                                      \
                 type(_3, typename),                                            \
                 decl_2EB(_1E_d1, _2EB_d1, , _1E_1, _2EB_1))
#define args_3EB(I, _1, _2, _3, _1E_1, _2EB_1) base_3EB((), (), I, _1, _2, _3, args_2EB(, _1E_1, _2EB_1))
#define prms_3EB(...) decl_3EB((), (), (), (), __VA_ARGS__)

        template<decl_3EB(_d2    (0),
                          _d3    (tuple::construct<I>),
                          _1E_d1 (0),
                          _2EB_d1(arg__Array__of__dict_Key__node_ids::size == arg__idx),
                          I, _, _, _, _, _)>
        struct parse_feeders__producer_and_sink_adjacency_matrix__assign_idx;

#define base_2CB(_d2, args_1C, _1, _2, args_2EB)                                                \
        args_1C                                                                                 \
        opts  (_1, arg__ptr_to_ptr_to_error_Pos__cycle_forming_node_pos_in_reverse_order, ())   \
          opts(_2, arg__error_DAG_cycle, _d2)                                                   \
          args_2EB
#define decl_2CB(_d2, _1E_d1, _2EB_d1, I, _1C_1, _1C_2, _1, _2, _1E_1, _2EB_1)  \
        base_2CB(_d2,                                                           \
                 prms_1C(I, _1C_1, _1C_2),                                      \
                 type(_1, const v1::error::Pos *const *),                       \
                 type(_2, typename),                                            \
                 decl_2EB(_1E_d1, _2EB_d1, , _1E_1, _2EB_1))
#define args_2CB(I, _1C_1, _1C_2, _1, _2, _1E_1, _2EB_1) base_2CB((), args_1C(I, _1C_1, _1C_2), _1, _2, args_2EB(, _1E_1, _2EB_1))
#define prms_2CB(...) decl_2CB((), (), (), __VA_ARGS__)

        template<decl_2CB(_d2    (v1::error::DAG_cycle_forming_node_pos<>),
                          _1E_d1 (0),
                          _2EB_d1(arg__ptr_to_ptr_to_error_Pos__cycle_forming_node_pos_in_reverse_order[0][arg__idx]
                                  == v1::error::Invalid_pos::value),
                          I, _, _, _, _, _, _)>
        struct parse_feeders__check_cycle;

#define base_4A(args_3A, args_1C, args_2B)      \
        args_3A                                 \
        args_1C                                 \
        args_2B
#define decl_4A(_1C_d2, _1J_d1, _1J_d2, _1J_d3, _1J_d4, _1J_d5, _1J_d6, _1J_d7, _1J_d8, _1J_d9, _1J_d10, _1J_d11, _1J_d12,                      \
                _1J_d13, _1J_d14, _1J_d15, _1J_d16, _1J_d17, _1J_d18, _1J_d19, _1J_d20, _1J_d21, _1J_d22, _1J_d23, _2B_d1, _1B_d1,              \
                I, _1A_1, _2A_1, _3A_1, _1C_1, _1C_2, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,    \
                _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23, _2B_1, _1B_1)                           \
        base_4A(prms_3A(I, _1A_1, _2A_1, _3A_1),                                                                                                \
                decl_1C(_1C_d2, , _1C_1, _1C_2),                                                                                                \
                decl_2B(_1J_d1, _1J_d2, _1J_d3, _1J_d4, _1J_d5, _1J_d6, _1J_d7, _1J_d8, _1J_d9, _1J_d10, _1J_d11, _1J_d12,                      \
                        _1J_d13, _1J_d14, _1J_d15, _1J_d16, _1J_d17, _1J_d18, _1J_d19, _1J_d20, _1J_d21, _1J_d22, _1J_d23,                      \
                        _2B_d1, _1B_d1, , _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,                \
                        _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23, _2B_1, _1B_1))
#define args_4A(I, _1A_1, _2A_1, _3A_1, _1C_1, _1C_2, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,    \
                _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23, _2B_1, _1B_1)                           \
        base_4A(args_3A(I, _1A_1, _2A_1, _3A_1), args_1C(, _1C_1, _1C_2),                                                                       \
                args_2B(, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,                                \
                        _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23, _2B_1, _1B_1))
#define prms_4A(...) decl_4A((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

        template<prms_4A(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__args_checked;

#define base_5A(_d1, _d2, _d3, args_4A, args_1F, _1, _2, _3)    \
        args_4A                                                 \
        args_1F                                                 \
        opts  (_1, arg__node_dict_tuple_checker_msg, _d1)       \
          opts(_2, arg__node_dict_tuple_nonempty_msg, _d2)      \
          opts(_3, arg__input_tuple_checker_msg, _d3)
#define decl_5A(_1C_d2, _1J_d1, _1J_d2, _1J_d3, _1J_d4, _1J_d5, _1J_d6, _1J_d7, _1J_d8, _1J_d9, _1J_d10, _1J_d11, _1J_d12,                      \
                _1J_d13, _1J_d14, _1J_d15, _1J_d16, _1J_d17, _1J_d18, _1J_d19, _1J_d20, _1J_d21, _1J_d22, _1J_d23, _2B_d1, _1B_d1,              \
                _1F_d1, _1F_d2, _d1, _d2, _d3,                                                                                                  \
                I, _1A_1, _2A_1, _3A_1, _1C_1, _1C_2, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,    \
                _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23, _2B_1, _1B_1, _1F_1, _1F_2, _1, _2, _3) \
        base_5A(_d1, _d2, _d3,                                                                                                                  \
                decl_4A(_1C_d2, _1J_d1, _1J_d2, _1J_d3, _1J_d4, _1J_d5, _1J_d6, _1J_d7, _1J_d8, _1J_d9, _1J_d10, _1J_d11, _1J_d12,              \
                        _1J_d13, _1J_d14, _1J_d15, _1J_d16, _1J_d17, _1J_d18, _1J_d19, _1J_d20, _1J_d21, _1J_d22, _1J_d23, _2B_d1, _1B_d1,      \
                        I, _1A_1, _2A_1, _3A_1, _1C_1, _1C_2, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10,            \
                        _1J_11, _1J_12, _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23, _2B_1, _1B_1),  \
                decl_1F(_1F_d1, _1F_d2, , _1F_1, _1F_2),                                                                                        \
                type(_1, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class),                                       \
                type(_2, template<bool arg__is_nonempty_tuple_construct_> class),                                                               \
                type(_3, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class))
#define args_5A(I, _1A_1, _2A_1, _3A_1, _1C_1, _1C_2, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10, _1J_11, _1J_12,    \
                _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23, _2B_1, _1B_1, _1F_1, _1F_2, _1, _2, _3) \
        base_5A((), (), (),                                                                                                                     \
                args_4A(I, _1A_1, _2A_1, _3A_1, _1C_1, _1C_2, _1J_1, _1J_2, _1J_3, _1J_4, _1J_5, _1J_6, _1J_7, _1J_8, _1J_9, _1J_10,            \
                        _1J_11, _1J_12, _1J_13, _1J_14, _1J_15, _1J_16, _1J_17, _1J_18, _1J_19, _1J_20, _1J_21, _1J_22, _1J_23, _2B_1, _1B_1),  \
                args_1F(, _1F_1, _1F_2), _1, _2, _3)
#define prms_5A(...)                                                                                                                            \
        decl_5A((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (),     \
                __VA_ARGS__)

        template<decl_5A(_1C_d2 (error::program::parse_feeders::graph_has_no_cycle),
                         _1J_d1 (error::program::parse_feeders::graph_has_no_multiple_edges),
                         _1J_d2 (error::program::parse_feeders::graph_has_no_loop),
                         _1J_d3 (error::program::parse_feeders::node_element_is_missing),
                         _1J_d4 (error::program::parse_feeders::element_is_producer_node),
                         _1J_d5 (error::program::parse_feeders::element_is_consumer_node),
                         _1J_d6 (error::program::parse_feeders::channel_and_consumer_parameter_counts_are_equal),
                         _1J_d7 (error::program::parse_feeders::consumer_function_is_callable),
                         _1J_d8 (error::program::parse_feeders::channel_element_is_missing),
                         _1J_d9 (error::program::parse_feeders::element_is_channel),
                         _1J_d10(error::program::parse_feeders::channel_arg_2_is_object_type),
                         _1J_d11(error::program::parse_feeders::channel_arg_2_is_cv_unqualified),
                         _1J_d12(error::program::parse_feeders::channel_arg_3_is_not_nullptr),
                         _1J_d13(error::program::parse_feeders::producer_return_value_is_assignable_to_channel),
                         _1J_d14(error::program::parse_feeders::init_val_can_initialize_channel),
                         _1J_d15(error::program::parse_feeders::channel_and_consumer_parameter_are_compatible),
                         _1J_d16(error::program::parse_feeders::node_arg_2_is_comp_Unit),
                         _1J_d17(error::program::parse_feeders::node_arg_2_arg_2_is_function_pointer),
                         _1J_d18(error::program::parse_feeders::node_arg_2_arg_3_is_Ratio),
                         _1J_d19(error::program::parse_feeders::node_arg_2_arg_3_has_nonzero_denominator),
                         _1J_d20(error::program::parse_feeders::node_arg_2_arg_3_is_positive),
                         _1J_d21(error::program::parse_feeders::node_arg_3_is_std_ratio),
                         _1J_d22(error::program::parse_feeders::node_arg_3_has_nonzero_denominator),
                         _1J_d23(error::program::parse_feeders::node_arg_3_is_greater_than_or_equal_to_node_arg_2_arg_3),
                         _2B_d1 (error::program::parse_feeders::feeders_are_distinct),
                         _1B_d1 (error::program::parse_feeders::node_has_been_specified),
                         _1F_d1 (error::program::parse_feeders::arg_2_is_tuple_construct_of_dict_Entry_of_Node),
                         _1F_d2 (error::program::parse_feeders::arg_2_has_linearly_increasing_node_ids_starting_at_zero_with_gradient_one),
                         _d1    (error::program::parse_feeders::arg_2_is_tuple_construct),
                         _d2    (error::program::parse_feeders::arg_2_is_nonempty_tuple_construct),
                         _d3    (error::program::parse_feeders::arg_3_is_tuple_construct),
                         I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders
        /* {
         *   with template<typename I, typename... args> tuple::construct<I, args...> = arg__tuple_construct__input {
         *     let tuple::construct<I, args...@@[&](std::size_t i, auto arg__element) { return {arg__element~~Feeder, arg__element}; }> feederset;
         *
         *     with template<typename I, typename... args_feeder> tuple::construct<I, args_feeder...> = feederset {
         *       let tuple::size<I, arg__tuple_construct__of__dict_Entry__of__Node__node_dict> node_dict_size;
         *       invariant {
         *         node_dict_size::value > 0;
         *       }
         *       typedef tuple::construct<I, args_feeder...@[&](std::size_t i, auto arg__feeder) {
         *                                     with template<typename... args> Feeder<args...> = arg__feeder {
         *                                       return {true, args...@[&](std::size_t j, auto arg__element) {
         *                                                       return {j % 2 == 1, Channel<I, arg__element>};
         *                                                     }};
         *                                     }
         *                                   }> channel_list;
         *       let tuple::construct<I, args_feeder...@[&](std::size_t i, auto arg__feeder) {
         *                                 with template<typename... args> Feeder<args...> = arg__feeder {
         *                                   return {true, std::integral_constant<v1::array::Size, sizeof...(args) - 1) / 2>};
         *                                 }
         *                               }> feeder_channel_count;
         *       let tuple::construct<I, std::integral_constant<v1::array::Idx, 0>,
         *                            std::integral_constant<v1::array::Idx,
         *                                                   (0
         *                                                    + tuple::get_pos<I, 1, feeder_channel_count>::value::value)>,
         *                            std::integral_constant<v1::array::Idx,
         *                                                   (0
         *                                                    + tuple::get_pos<I, 1, feeder_channel_count>::value::value
         *                                                    + tuple::get_pos<I, 2, feeder_channel_count>::value::value)>,
         *                            ...
         *                            std::integral_constant<v1::array::Idx,
         *                                                   (0
         *                                                    + tuple::get_pos<I, 1, feeder_channel_count>::value::value
         *                                                    + tuple::get_pos<I, 2, feeder_channel_count>::value::value
         *                                                    + ...
         *                                                    + tuple::get_pos<I, tuple::size<I, feeder_channel_count>::value,
         *                                                                     feeder_channel_count>::value::value)>> channel_start_idx;
         *       let tuple::construct<I, args_feeder...@[&](std::size_t i, auto arg__feeder) {
         *                                 with template<typename... args> Feeder<args...> = arg__feeder {
         *                                   let typename tuple::get_pos<I, sizeof...(args), tuple::construct<I, args...>>::value consumer_node;
         *                                   return {true, args...@[&](std::size_t j, auto arg__element) {
         *                                                   return {j % 2 == 0 && !std::is_same<arg__element, consumer_node>::value,
         *                                                           Arc<I,
         *                                                               get_node_id<I, i + 1, j + 1,
         *                                                                           arg__tuple_construct__of__dict_Entry__of__Node__node_dict,
         *                                                                           arg__element>::value::value,
         *                                                               get_node_id<I, i + 1, sizeof...(args),
         *                                                                           arg__tuple_construct__of__dict_Entry__of__Node__node_dict,
         *                                                                           consumer_node>::value::value,
         *                                                               tuple::get_pos<I, i + 1, channel_start_idx>::value::value + j / 2>};
         *                                                 }};
         *                                 }
         *                               }> arcset;
         *
         *       static constexpr v1::array::Size arc_count = tuple::size<I, arcset>::value;
         *
         *       let array::make::value<I, node_dict_size::value, std::integral_constant<bool, false>> false_test_array;
         *
         *       typedef array::update::value<
         *                 I, false_test_array,
         *                 tuple::construct<I, args_feeder...@[&](std::size_t i, auto arg__feeder) {
         *                                       with template<typename... args> Feeder<args...> = arg__feeder {
         *                                         return {true,
         *                                                 dict_Entry<I, get_node_id<I, i + 1, sizeof...(args),
         *                                                                           arg__tuple_construct__of__dict_Entry__of__Node__node_dict,
         *                                                                           typename tuple::get_pos<I, sizeof...(args),
         *                                                                                                   tuple::construct<I, args...>>::value
         *                                                               >::value::value,
         *                                                            true>};
         *                                       }
         *                                     }>> is_consumer_node;
         *       typedef node_id_array<I, is_consumer_node> consumer_node_ids;
         *
         *       with template<typename I, typename... args__arc> tuple::construct<I, args__arc...> = arcset {
         *         typedef array::update::value<I, false_test_array, tuple::construct<I, args_arc...@[&](std::size_t i, auto arg__arc) {
         *                                                                                 return {true, dict_Entry<I, arg__arc::prod_id, true>};
         *                                                                               }>,
         *                                      v1::array::on_dupd_index::CONTINUE_IF_IDENTICAL> is_producer_node;
         *         typedef node_id_array<I, is_producer_node> producer_node_ids;
         *       }
         *
         *       with template<typename I, typename... args__node>
         *            tuple::construct<I, args__node...> = arg__tuple_construct__of__dict_Entry__of__Node__node_dict
         *       {
         *         with template<typename I, typename... args__arc> tuple::construct<I, args__arc...> = arcset {
         *           typedef tuple::construct<I, args__node...@[&](std::size_t i, auto arg__node) {
         *                                         return {
         *                                           true,
         *                                           pair::construct<I,
         *                                             Array_idx_list<I, args_arc...@[&](std::size_t i, auto arg__arc) {
         *                                                                 return {arg__arc::prod_id == args__node::key, arg__arc::chan_idx};
         *                                                               }>,
         *                                             Array_idx_list<I, args_arc...@[&](std::size_t i, auto arg__arc) {
         *                                                                 return {arg__arc::cons_id == args__node::key, arg__arc::chan_idx};
         *                                                               }>
         *                                           >
         *                                         };
         *                                       }> channel_idx_list;
         *         }
         *         typedef array::update::value<I, false_test_array, tuple::construct<I, args__node...@[&](std::size_t i, auto arg__node) {
         *                                                                                 return {true, dict_Entry<I, arg__node::key,
         *                                                                                           !is_producer_node::elems[arg__node::key]
         *                                                                                           && !is_consumer_node::elems[arg__node::key]
         *                                                                                         >};
         *                                                                               }>> is_isolated_node;
         *         typedef node_id_array<I, is_isolated_node> isolated_node_ids;
         *         typedef array::update::value<I, false_test_array, tuple::construct<I, args__node...@[&](std::size_t i, auto arg__node) {
         *                                                                                 return {true, dict_Entry<I, arg__node::key,
         *                                                                                           is_producer_node::elems[arg__node::key]
         *                                                                                           && !is_consumer_node::elems[arg__node::key]
         *                                                                                         >};
         *                                                                               }>> is_source_node;
         *         typedef node_id_array<I, is_source_node> source_node_ids;
         *         typedef array::update::value<I, false_test_array, tuple::construct<I, args__node...@[&](std::size_t i, auto arg__node) {
         *                                                                                 return {true, dict_Entry<I, arg__node::key,
         *                                                                                           !is_producer_node::elems[arg__node::key]
         *                                                                                           && is_consumer_node::elems[arg__node::key]
         *                                                                                         >};
         *                                                                               }>> is_sink_node;
         *         typedef node_id_array<I, is_sink_node> sink_node_ids;
         *       }
         *
         *       if (arc_count == 0) {
         *         with template<typename I, typename... args>
         *           tuple::construct<I, args...> = arg__tuple_construct__of__dict_Entry__of__Node__node_dict
         *         {
         *           typedef array::make::nested::value<I, tuple::construct<I, args...@[&](std::size_t i, auto) {
         *             return {true, array::construct<I, false, v1::dict::Key, tuple::construct<I, adjacency_list::EOL>>};
         *           }>> adjacency_list;
         *         }
         *       } else {
         *         with template<typename I, typename... args__arc> tuple::construct<I, args__arc...> = arcset {
         *           typedef array::make::nested::value<I, tuple::construct<I,
         *             array::construct<I, false, v1::dict::Key,
         *               typename utility::list::append<I,
         *                 tuple::construct<I, args__arc...@[&](std::size_t i, auto arg__arc) {
         *                                       return {0 == arg__arc::prod_id, std::integral_constant<v1::dict::Key, arg__arc::prod_id>};
         *                                     }>,
         *                 adjacency_list::EOL
         *               >::value
         *             >,
         *             array::construct<I, false, v1::dict::Key,
         *               typename utility::list::append<I,
         *                 tuple::construct<I, args__arc...@[&](std::size_t i, auto arg__arc) {
         *                                       return {1 == arg__arc::prod_id, std::integral_constant<v1::dict::Key, arg__arc::prod_id>};
         *                                     }>,
         *                 adjacency_list::EOL
         *               >::value
         *             >,
         *             ...
         *             array::construct<I, false, v1::dict::Key,
         *               typename utility::list::append<I,
         *                 tuple::construct<I, args__arc...@[&](std::size_t i, auto arg__arc) {
         *                                       return {node_dict_size::value - 1 == arg__arc::prod_id, std::integral_constant<v1::dict::Key,
         *                                                                                                                      arg__arc::prod_id>};
         *                                     }>,
         *                 adjacency_list::EOL
         *               >::value
         *             >
         *           >> adjacency_list;
         *         }
         *       }
         *       invariant {
         *         node_dict_size::value == adjacency_list::size;
         *       }
         *
         *       template<typename I_, v1::dict::Key arg__producer_node_id_, v1::dict::Key arg__sink_node_id_,
         *                template<bool arg__is_producer_node_id__, v1::dict::Key arg__node_id__> class arg__producer_node_id_checker_msg_,
         *                template<bool arg__is_sink_node_id__, v1::dict::Key arg__node_id__> class arg__sink_node_id_checker_msg_>
         *       struct producer_and_sink_are_connected :
         *         arg__producer_node_id_checker_msg_<(0 <= arg__producer_node_id_
         *                                             && arg__producer_node_id_ < arg__Array__of__bool__is_producer_node::size
         *                                             && arg__Array__of__bool__is_producer_node::elems[arg__producer_node_id_]),
         *                                            arg__producer_node_id_>,
         *         arg__sink_node_id_checker_msg_<(0 <= arg__sink_node_id_
         *                                         && arg__sink_node_id_ < arg__Array__of__bool__is_sink_node::size
         *                                         && arg__Array__of__bool__is_sink_node::elems[arg__sink_node_id_]),
         *                                        arg__sink_node_id_>,
         *         std::integral_constant<bool, [&]() {
         *                                        if (arc_count == 0) {
         *                                          return false;
         *                                        }
         *
         *                                        v1::dict::Key source_node_id_stack[node_dict_size::value];
         *                                        v1::array::Idx i_stack[node_dict_size::value];
         *                                        v1::array::Idx stack_top = 0;
         *
         *                                        source_node_id_stack[stack_top] = arg__source_node_id_;
         *                                        i_stack[stack_top] = 0;
         *
         *                                        while (true) {
         *                                          v1::dict::Key source_node_id = source_node_id_stack[stack_top];
         *                                          v1::array::Idx i = i_stack[stack_top];
         *                                          v1::dict::Key child_node_id = adjacency_list::elems[source_node_id][i];
         *
         *                                          while (stack_top != 0 && child_node_id == ::adjacency_list::EOL::value) {
         *                                            --stack_top;
         *                                            source_node_id = source_node_id_stack[stack_top];
         *                                            i = i_stack[stack_pop];
         *                                            child_node_id = adjacency_list::elems[source_node_id][i];
         *                                          }
         *                                          if (stack_top == 0 && child_node_id == ::adjacency_list::EOL::value) {
         *                                            return false;
         *                                          }
         *                                          if (child_node_id == arg__sink_node_id_) {
         *                                            return true;
         *                                          }
         *
         *                                          i_stack[stack_top] = i + 1;
         *                                          ++stack_top;
         *
         *                                          i_stack[stack_top] = 0;
         *                                          source_node_id_stack[stack_top] = child_node_id;
         *                                        }
         *                                      }()> {
         *       };
         *     }
         *   }
         * }
         */;

        

#define prms_5A_1(I, _2A_1, _3A_1)                                                                                                      \
        prms_5A(I, _, _2A_1, _3A_1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)
#define args_5A_1(I, _2A_1, _3A_1)                                                                                                      \
        args_5A(I, _, _2A_1, _3A_1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)

        template<prms_5A_1(I, _, _)>
        struct parse_feeders :
          arg__node_dict_tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct__of__dict_Entry__of__Node__node_dict> {
        };

        template<prms_5A_1(I, R(typename... args), _)>
        struct parse_feeders<args_5A_1(I, R(tuple::construct<I, args...>), _)> :
          arg__input_tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct__input> {
        };

        template<prms_5A_1(I, R(typename... args), R(typename... args__input))>
        struct parse_feeders<args_5A_1(I, R(tuple::construct<I, args...>), R(tuple::construct<I, args__input...>))> :
          arg__node_dict_tuple_nonempty_msg<(sizeof...(args) > 0)>,
          parse_feeders__validate_node_dict<args_2F(I, R(1), R(tuple::construct<I, args...>), _, _)>,
          parse_feeders__args_checked<args_4A(I, _, R(tuple::construct<I, args...>), R(tuple::construct<I, args__input...>),
                                              _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
        };

        

        template<prms_2F(I, _, R(typename arg, typename... args), _, _)>
        struct parse_feeders__validate_node_dict<args_2F(I, _, R(tuple::construct<I, arg, args...>), _, _)> :
          arg__node_dict_element_checker_msg<!!utility::always_false<I>(), arg__pos, arg> {
        };

        template<prms_2F(I, _, R(v1::dict::Key arg__node_id, typename... args__node_arg, typename... args), _, _)>
        struct parse_feeders__validate_node_dict<args_2F(I, _,
                                                         R(tuple::construct<I, dict_Entry<I, arg__node_id, Node<args__node_arg...>>, args...>),
                                                         _, _)> :
          arg__node_dict_id_checker_msg<arg__pos == arg__node_id + 1, arg__pos, arg__node_id>,
          parse_feeders__validate_node_dict<args_2F(I, R(arg__pos + 1), R(tuple::construct<I, args...>), _, _)> {
        };

        template<prms_2F(I, _, X, _, _)>
        struct parse_feeders__validate_node_dict<args_2F(I, _, R(tuple::construct<I>), _, _)> {
        };

        

        template<prms_4A(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__args_checked :
#define base_class parse_feeders__next<args_4BC(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,  \
                                                X, X, X, X, X, X, X)>

          base_class,
          parse_feeders__check_graph<args_3H(I, R((base_class::arc_count > 0)), R((base_class::sink_node_ids::size > 0)),
                                             R(typename base_class::adjacency_list), R(typename base_class::source_node_ids),
                                             R(typename base_class::is_producer_node), R(typename base_class::is_sink_node), _, _,
                                             R(typename base_class::producer_node_ids), R(typename base_class::sink_node_ids),
                                             R(typename base_class::is_source_node), R(typename base_class::is_isolated_node))> {
        };
#undef base_class

        

        template<prms_3H(I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__check_graph :
          parse_feeders__traverse_graph_depth_first_with_memoization<args_3CA(I, _, _, _, _, _, _,
                                                                              R(parse_feeders__producer_and_sink_adjacency_matrix<args_2I(I, _, _,
                                                                                                                                          _, _, _,
                                                                                                                                          _)>))> {
        };

        template<prms_3H(I, X, X, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__check_graph<args_3H(I, R(false), R(false), _, _, _, _, _, _, _, _, _, _)>
        {
          template<typename I_, v1::dict::Key arg__producer_node_id_, v1::dict::Key arg__sink_node_id_,
                   template<bool arg__is_producer_node_id__, v1::dict::Key arg__node_id__> class arg__producer_node_id_checker_msg_,
                   template<bool arg__is_sink_node_id__, v1::dict::Key arg__node_id__> class arg__sink_node_id_checker_msg_>
          struct producer_and_sink_are_connected :
#define in_range (0 <= arg__producer_node_id_ && arg__producer_node_id_ < arg__Array__of__bool__is_producer_node::size)
            arg__producer_node_id_checker_msg_<in_range && arg__Array__of__bool__is_producer_node::elems[in_range ? arg__producer_node_id_ : 0],
                                               arg__producer_node_id_>,
#undef in_range
#define in_range (0 <= arg__sink_node_id_ && arg__sink_node_id_ < arg__Array__of__bool__is_sink_node::size)
            arg__sink_node_id_checker_msg_<in_range && arg__Array__of__bool__is_sink_node::elems[in_range ? arg__sink_node_id_ : 0],
                                           arg__sink_node_id_>,
#undef in_range
            std::false_type {
          };
        };

        template<prms_3H(I, _, X, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__check_graph<args_3H(I, _, R(false), _, _, _, _, _, _, _, _, _, _)>
        {
          struct get_cycle_starting_node
          {
            v1::dict::Key node_id;

            constexpr get_cycle_starting_node(const v1::dict::Key &source_node_id) :
              node_id{v1::dict::Invalid_key::value}
            {
              bool already_visited[arg__Array__of__Array__of__dict_Key__adjacency_list::size] = {};
              for (v1::array::Idx i = 0; i < arg__Array__of__Array__of__dict_Key__adjacency_list::size; ++i) {
                already_visited[i] = false;
              }
              v1::dict::Key current_node_id = source_node_id;
              while (!already_visited[current_node_id]) {
                already_visited[current_node_id] = true;
                current_node_id = arg__Array__of__Array__of__dict_Key__adjacency_list::elems[current_node_id][0];
              }
              node_id = current_node_id;
            }
          };

          run_metaprogram(parse_feeders__report_cycle<
                          args_2CC(I, _, _, _, R(get_cycle_starting_node(arg__Array__of__dict_Key__source_node_ids::elems[0]).node_id), X, X, X)>);
        };

        

        template<prms_2CC(I, _, _, _, _, R(v1::error::Pos... args), _, X)>
        struct parse_feeders__report_cycle<args_2CC(I, _, _, _, _, R(v1::error::DAG_cycle_forming_node_pos<args...>), _, R(true))> :
          arg__check_cycle_msg<!!utility::always_false<I>(),
                               v1::error::DAG_cycle_forming_node_pos<args..., arg__node_starting_idx + arg__idx,
                                                                     arg__node_starting_idx + arg__cycle_starting_node_id>> {
        };

        template<prms_2CC(I, _, _, _, _, R(v1::error::Pos... args), _, X)>
        struct parse_feeders__report_cycle<args_2CC(I, _, _, _, _, R(v1::error::DAG_cycle_forming_node_pos<args...>), _, R(false))> :
          parse_feeders__report_cycle<args_2CC(I, _, _, _, _,
                                               R(v1::error::DAG_cycle_forming_node_pos<args..., arg__node_starting_idx + arg__idx>),
                                               R(arg__Array__of__Array__of__dict_Key__adjacency_list::elems[arg__idx][0]), X)> {
        };

        

        template<prms_2I(I, _, _, _, _, _, _)>
        struct parse_feeders__producer_and_sink_adjacency_matrix {
          static constexpr v1::array::Size producer_count {arg__Array__of__dict_Key__producer_node_ids::size};
          static constexpr v1::array::Size sink_count {arg__Array__of__dict_Key__sink_node_ids::size};
          typedef bool type[producer_count][sink_count];
          typedef array::update::value<I, array::make::value<I, arg__Array__of__Array__of__dict_Key__adjacency_list::size,
                                                             std::integral_constant<v1::dict::Key, v1::dict::Invalid_key::value>>,
                                       H(typename utility::list::cat<I,
                                         H(typename parse_feeders__producer_and_sink_adjacency_matrix__assign_idx<
                                           args_3EB(I, R(arg__Array__of__dict_Key__producer_node_ids), X, X, X, X)>::value),
                                         H(typename parse_feeders__producer_and_sink_adjacency_matrix__assign_idx<
                                           args_3EB(I, R(arg__Array__of__dict_Key__sink_node_ids), X, X, X, X)>::value)>::value)> idx;
        };

        

        template<prms_3EB(I, _, _, _, _, _)>
        struct parse_feeders__producer_and_sink_adjacency_matrix__assign_idx {
          typedef arg__tuple_construct__of__dict_Entry_node_id value;
        };

        template<prms_3EB(I, _, _, R(typename... args), _, X)>
        struct parse_feeders__producer_and_sink_adjacency_matrix__assign_idx<args_3EB(I, _, _, R(tuple::construct<I, args...>), _, R(false))> :
          parse_feeders__producer_and_sink_adjacency_matrix__assign_idx<args_3EB(I, _, R(arg__idx_to_assign + 1),
                                                                                 R(tuple::construct<I,
                                                                                   H(args...,
                                                                                     dict_Entry_node_id<I,
                                                                                     H(arg__Array__of__dict_Key__node_ids::elems[arg__idx],
                                                                                       arg__idx_to_assign)
                                                                                     >)
                                                                                   >),
                                                                                 R(arg__idx + 1), X)> {
        };

        

        template<prms_3CA(I, _, _, _, _, _, _, _)>
        struct parse_feeders__traverse_graph_depth_first_with_memoization
        {
        private:
          struct traverse
          {
          private:
            enum class result {
              NO, NO__FROM_SINK, YES__NODE_IN_CYCLE, YES
            };

          public:
            v1::error::Pos cycle_forming_node_pos_in_reverse_order[arg__Array__of__Array__of__dict_Key__adjacency_list::size + 1];
            typename arg__parse_feeders__producer_and_sink_adjacency_matrix::type producer_and_sink_adjacency_matrix;

            constexpr traverse() :
              cycle_forming_node_pos_in_reverse_order{},
              producer_and_sink_adjacency_matrix{}
            {
              v1::error::Pos cycle_starting_node_pos = v1::error::Invalid_pos::value;
              bool any_traversal_starting_at_this_node_has_been_cleared_of_cycle[arg__Array__of__Array__of__dict_Key__adjacency_list::size] = {};
              bool current_traversal_already_visits_this_node[arg__Array__of__Array__of__dict_Key__adjacency_list::size] = {};

              for (v1::array::Idx i = 0; i < arg__Array__of__Array__of__dict_Key__adjacency_list::size; ++i) {
                any_traversal_starting_at_this_node_has_been_cleared_of_cycle[i] = current_traversal_already_visits_this_node[i] = false;
              }

              typename arg__parse_feeders__producer_and_sink_adjacency_matrix::type producer_and_sink_adjacency_matrix = {};
              for (v1::array::Idx i = 0; i < arg__parse_feeders__producer_and_sink_adjacency_matrix::producer_count; ++i) {
                for (v1::array::Idx j = 0; j < arg__parse_feeders__producer_and_sink_adjacency_matrix::sink_count; ++j) {
                  producer_and_sink_adjacency_matrix[i][j] = false;
                }
              }

              v1::array::Size cycle_forming_node_pos_in_reverse_order__size = 0;
              for (v1::array::Idx i = 0; i < arg__Array__of__dict_Key__source_node_ids::size; ++i) {

                v1::dict::Key node_id = arg__Array__of__dict_Key__source_node_ids::elems[i];

                current_traversal_already_visits_this_node[node_id] = true;

                switch (has_cycle(cycle_starting_node_pos, cycle_forming_node_pos_in_reverse_order__size,
                                  any_traversal_starting_at_this_node_has_been_cleared_of_cycle, current_traversal_already_visits_this_node,
                                  arg__Array__of__Array__of__dict_Key__adjacency_list::elems[node_id], node_id,
                                  producer_and_sink_adjacency_matrix)) {
                case result::YES__NODE_IN_CYCLE:
                  cycle_forming_node_pos_in_reverse_order[cycle_forming_node_pos_in_reverse_order__size++] = node_id + 1;
                case result::YES:
                  i = arg__Array__of__dict_Key__source_node_ids::size - 1;
                default:
                  ;
                }

              }
              cycle_forming_node_pos_in_reverse_order[cycle_forming_node_pos_in_reverse_order__size] = v1::error::Invalid_pos::value;

              for (v1::array::Idx i = 0; i < arg__parse_feeders__producer_and_sink_adjacency_matrix::producer_count; ++i) {
                for (v1::array::Idx j = 0; j < arg__parse_feeders__producer_and_sink_adjacency_matrix::sink_count; ++j) {
                  this->producer_and_sink_adjacency_matrix[i][j] = producer_and_sink_adjacency_matrix[i][j];
                }
              }
            }

          private:
            constexpr result has_cycle(v1::error::Pos &cycle_starting_node_pos, v1::array::Size &cycle_forming_node_pos_in_reverse_order__size,
                                       bool any_traversal_starting_at_this_node_has_been_cleared_of_cycle[],
                                       bool current_traversal_already_visits_this_node[],
                                       const v1::dict::Key child_node_ids[], const v1::dict::Key &parent_node_id,
                                       typename
                                       arg__parse_feeders__producer_and_sink_adjacency_matrix::type &producer_and_sink_adjacency_matrix)
            {
              if (child_node_ids[0] == adjacency_list::EOL::value) {
                return result::NO__FROM_SINK;
              }

              for (v1::array::Idx i = 0; child_node_ids[i] != adjacency_list::EOL::value; ++i) {

                v1::dict::Key node_id = child_node_ids[i];

                if (current_traversal_already_visits_this_node[node_id]) {
                  cycle_forming_node_pos_in_reverse_order[cycle_forming_node_pos_in_reverse_order__size++] = cycle_starting_node_pos = node_id + 1;
                  return result::YES__NODE_IN_CYCLE;
                }

                if (any_traversal_starting_at_this_node_has_been_cleared_of_cycle[node_id]) {
                  if (arg__Array__of__bool__is_sink_node::elems[node_id]) {
                    producer_and_sink_adjacency_matrix
                      [arg__parse_feeders__producer_and_sink_adjacency_matrix::idx::elems[parent_node_id]]
                      [arg__parse_feeders__producer_and_sink_adjacency_matrix::idx::elems[node_id]]
                      = true;
                  } else {
                    for (v1::array::Idx j = 0; j < arg__parse_feeders__producer_and_sink_adjacency_matrix::sink_count; ++j) {
                      producer_and_sink_adjacency_matrix[arg__parse_feeders__producer_and_sink_adjacency_matrix::idx::elems[parent_node_id]][j]
                        |= producer_and_sink_adjacency_matrix[arg__parse_feeders__producer_and_sink_adjacency_matrix::idx::elems[node_id]][j];
                    }
                  }

                  continue;
                }

                current_traversal_already_visits_this_node[node_id] = true;

                switch (has_cycle(cycle_starting_node_pos, cycle_forming_node_pos_in_reverse_order__size,
                                  any_traversal_starting_at_this_node_has_been_cleared_of_cycle, current_traversal_already_visits_this_node,
                                  arg__Array__of__Array__of__dict_Key__adjacency_list::elems[node_id], node_id,
                                  producer_and_sink_adjacency_matrix)) {
                case result::YES__NODE_IN_CYCLE:
                  if (cycle_starting_node_pos == node_id + 1) {
                    return result::YES;
                  }
                  cycle_forming_node_pos_in_reverse_order[cycle_forming_node_pos_in_reverse_order__size++] = node_id + 1;
                  return result::YES__NODE_IN_CYCLE;
                case result::YES:
                  return result::YES;
                case result::NO__FROM_SINK:
                  producer_and_sink_adjacency_matrix
                    [arg__parse_feeders__producer_and_sink_adjacency_matrix::idx::elems[parent_node_id]]
                    [arg__parse_feeders__producer_and_sink_adjacency_matrix::idx::elems[node_id]]
                    = true;
                  break;
                default:
                  for (v1::array::Idx j = 0; j < arg__parse_feeders__producer_and_sink_adjacency_matrix::sink_count; ++j) {
                    producer_and_sink_adjacency_matrix[arg__parse_feeders__producer_and_sink_adjacency_matrix::idx::elems[parent_node_id]][j]
                      |= producer_and_sink_adjacency_matrix[arg__parse_feeders__producer_and_sink_adjacency_matrix::idx::elems[node_id]][j];
                  }
                }

                any_traversal_starting_at_this_node_has_been_cleared_of_cycle[node_id] = true;
                current_traversal_already_visits_this_node[node_id] = false;

              }

              return result::NO;
            }
          };

          static constexpr traverse result {};
          static constexpr const v1::error::Pos *cycle_forming_node_pos_in_reverse_order_ptr[1] = {result.cycle_forming_node_pos_in_reverse_order};

          run_metaprogram(parse_feeders__check_cycle<args_2CB(I, _, _, R(cycle_forming_node_pos_in_reverse_order_ptr), X, X, X)>);

          template<typename I_, v1::dict::Key arg__producer_node_id_, v1::dict::Key arg__sink_node_id_>
          struct producer_and_sink_are_connected__test :
            std::integral_constant<bool, H(result.producer_and_sink_adjacency_matrix
                                           [arg__parse_feeders__producer_and_sink_adjacency_matrix::idx::elems[arg__producer_node_id_]]
                                           [arg__parse_feeders__producer_and_sink_adjacency_matrix::idx::elems[arg__sink_node_id_]])> {
          };

        public:
          template<typename I_, v1::dict::Key arg__producer_node_id_, v1::dict::Key arg__sink_node_id_,
                   template<bool arg__is_producer_node_id__, v1::dict::Key arg__node_id__> class arg__producer_node_id_checker_msg_,
                   template<bool arg__is_sink_node_id__, v1::dict::Key arg__node_id__> class arg__sink_node_id_checker_msg_>
          struct producer_and_sink_are_connected :
#define in_range (0 <= arg__producer_node_id_ && arg__producer_node_id_ < arg__Array__of__bool__is_producer_node::size)
            arg__producer_node_id_checker_msg_<in_range && arg__Array__of__bool__is_producer_node::elems[in_range ? arg__producer_node_id_ : 0],
                                               arg__producer_node_id_>,
#undef in_range
#define in_range (0 <= arg__sink_node_id_ && arg__sink_node_id_ < arg__Array__of__bool__is_sink_node::size)
            arg__sink_node_id_checker_msg_<in_range && arg__Array__of__bool__is_sink_node::elems[in_range ? arg__sink_node_id_ : 0],
                                           arg__sink_node_id_>,
#undef in_range
            producer_and_sink_are_connected__test<I_, arg__producer_node_id_, arg__sink_node_id_> {
          };
        };

        

        template<prms_2CB(I, _, _, _, X, _, X)>
        struct parse_feeders__check_cycle<args_2CB(I, _, _, _, R(v1::error::DAG_cycle_forming_node_pos<>), _, R(true))> {
        };

        template<prms_2CB(I, _, _, _, R(v1::error::Pos... args), _, X)>
        struct parse_feeders__check_cycle<args_2CB(I, _, _, _, R(v1::error::DAG_cycle_forming_node_pos<args...>), _, R(false))> :
          parse_feeders__check_cycle<args_2CB(I, _, _, _,
                                              R(v1::error::DAG_cycle_forming_node_pos<
                                                H((arg__node_starting_idx
                                                   + arg__ptr_to_ptr_to_error_Pos__cycle_forming_node_pos_in_reverse_order[0][arg__idx]),
                                                  args...)
                                                >), R(arg__idx + 1), X)> {
        };

        template<prms_2CB(I, _, _, _, R(v1::error::Pos arg, v1::error::Pos... args), _, X)>
        struct parse_feeders__check_cycle<args_2CB(I, _, _, _, R(v1::error::DAG_cycle_forming_node_pos<arg, args...>), _, R(true))> :
          arg__check_cycle_msg<!!utility::always_false<I>(),
                               v1::error::DAG_cycle_forming_node_pos<
                                 H(arg__node_starting_idx + arg__ptr_to_ptr_to_error_Pos__cycle_forming_node_pos_in_reverse_order[0][0],
                                   arg, args...)
                                 >> {
        };

        

#define prms_4BC_1(I, _3A_1) prms_4BC(I, _, _, _3A_1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,       \
                                      R(typename... args__consumer), _, _, _)
#define args_4BC_1(I, _3A_1) args_4BC(I, _, _, _3A_1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,       \
                                      R(tuple::construct<I, args__consumer...>), _, _, _)

        template<prms_4BC_1(I, R(typename... args))>
        struct parse_feeders__next<args_4BC_1(I, R(tuple::construct<I, args...>))> :
          parser_remaining_args<I, tuple::construct<I, args...>, (sizeof...(args) == 0 ? v1::error::Invalid_pos::value : arg__pos)>
        {
        private:
          typedef tuple::size<I, arg__tuple_construct__of__dict_Entry__of__Node__node_dict> node_dict_size;
          typedef tuple::construct<I, dict_Entry_key<I, args__consumer::key>...> consumer_node_dict_Entry_key;

        public:
          typedef array::make::nested::value<I, H(typename utility::list::
                                                  update<H(I, arg__tuple_construct__of__tuple_construct__of__dict_Entry_array_idx__arc_list,
                                                           parse_feeders__next__make_adjacency_list, void)>::value)> adjacency_list;
          typedef arg__tuple_construct__of__Channel__channel_list channel_list;
          typedef typename utility::list::update<I, arg__tuple_construct__of__tuple_construct__of__dict_Entry_array_idx__arc_list,
                                                 parse_feeders__next__make_channel_idx_list,
                                                 pair::construct<I, arg__tuple_construct__of__dict_Entry__of__Array_idx_list__cons_channel_idx_list,
                                                                 tuple::construct<I>>>::value channel_idx_list;
          static constexpr v1::array::Size arc_count {arg__arc_count};

        public:
          typedef node_id_test_array<I, node_dict_size::value, consumer_node_dict_Entry_key> is_consumer_node;
          typedef node_id_array<I, is_consumer_node> consumer_node_ids;
          typedef arg__Array__of__bool__is_producer_node is_producer_node;
          typedef node_id_array<I, is_producer_node> producer_node_ids;

        private:
          template<typename arg__Array__of__bool__is_producer_node_, typename arg__Array__of__bool__is_consumer_node_>
          static constexpr pair::construct<I, bool, bool> producer_node_or_consumer_node_to_false(const v1::array::Idx &idx, const bool &prev_value)
          {
            return {true, (arg__Array__of__bool__is_producer_node_::elems[idx]
                           || arg__Array__of__bool__is_consumer_node_::elems[idx]) ? false : prev_value};
          }

        public:
          typedef array::filter_then_map::value<I, array::make::value<I, node_dict_size::value, std::integral_constant<bool, true>>, bool,
                                                producer_node_or_consumer_node_to_false<is_producer_node, is_consumer_node>> is_isolated_node;
          typedef node_id_array<I, is_isolated_node> isolated_node_ids;

        private:
          template<typename arg__Array__of__bool__is_positive_node_>
          static constexpr pair::construct<I, bool, bool> positive_node_to_false_else_negate(const v1::array::Idx &idx,
                                                                                             const bool &is_isolated_node)
          {
            return {true, arg__Array__of__bool__is_positive_node_::elems[idx] ? false : !is_isolated_node};
          }

        public:
          typedef array::filter_then_map::value<I, is_isolated_node, bool, positive_node_to_false_else_negate<is_consumer_node>> is_source_node;
          typedef node_id_array<I, is_source_node> source_node_ids;
          typedef array::filter_then_map::value<I, is_isolated_node, bool, positive_node_to_false_else_negate<is_producer_node>> is_sink_node;
          typedef node_id_array<I, is_sink_node> sink_node_ids;
        };

        template<prms_4BC_1(I, R(typename... args__feeder_arg, typename... args))>
        struct parse_feeders__next<args_4BC_1(I, R(tuple::construct<I, Feeder<args__feeder_arg...>, args...>))> :
          utility::run_metaprogram<I, feeder::construct<I, tuple::construct<I, args__feeder_arg...>,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        node_missing_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        producer_node_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        consumer_node_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        param_count_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        consumer_invocation_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        channel_missing_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        channel_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        channel_type_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        channel_cv_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        channel_value_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        producer_return_value_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        channel_initval_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        channel_and_consumer_param_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        multiple_edge_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        loop_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        node_computation_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        node_computation_fn_ptr_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        node_computation_wcet_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        node_computation_wcet_den_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        node_computation_wcet_validator_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        node_period_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        node_period_den_checker_msg,
                                                        arg__parse_feeders__error_pos_adaptor__feeder_msgs::template
                                                        node_period_validator_msg>>,
          parse_feeders__next__feeder_validated<args_4BA(I, R(Feeder<args__feeder_arg...>), _, _, R(tuple::construct<I, args...>),
                                                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                         R(tuple::construct<I, args__consumer...>), _, _)> {
        };

        

        template<prms_2EC(I, _, R(typename... args__dict_Entry_array_idx), _)>
        struct parse_feeders__next__make_adjacency_list<args_2EC(I, _, R(tuple::construct<I, args__dict_Entry_array_idx...>), _)>
        {
          static constexpr bool is_complete = false;
          typedef arg__updater_data data;
          typedef array::construct<I, false, v1::dict::Key,
                                   tuple::construct<I, std::integral_constant<v1::dict::Key, args__dict_Entry_array_idx::key>...,
                                                    adjacency_list::EOL>> value;
        };

        

#define prms_3EC_1(I, _2EC_2) prms_3EC(I, _, _, _2EC_2)
#define args_3EC_1(I, _2EC_2) args_3EC(I, _, _, _2EC_2)

        template<prms_3EC_1(I, R(typename... args__checked))>
        struct parse_feeders__next__make_channel_idx_list<args_3EC_1(I, R(pair::construct<I,
                                                                          H(tuple::construct<I>,
                                                                            tuple::construct<I, args__checked...>)>))>
        {
          static constexpr bool is_complete = false;
          typedef pair::construct<I, tuple::construct<I, args__checked...>, tuple::construct<I>> data;
          typedef pair::construct<I, arg__prev_list_element, Array_idx_list<I>> value;
        };

        template<prms_3EC_1(I, R(typename arg__Array_idx_list, typename... args__unchecked, typename... args__checked))>
        struct parse_feeders__next__make_channel_idx_list<args_3EC_1(I, R(pair::construct<I,
                                                                          H(tuple::construct<I,
                                                                            H(dict_Entry<I, arg__idx, arg__Array_idx_list>, args__unchecked...)>,
                                                                            tuple::construct<I, args__checked...>)>))>
        {
          static constexpr bool is_complete = false;
          typedef pair::construct<I, tuple::construct<I, args__checked..., args__unchecked...>, tuple::construct<I>> data;
          typedef pair::construct<I, arg__prev_list_element, arg__Array_idx_list> value;
        };

        template<prms_3EC_1(I, R(v1::dict::Key arg__cons_node_id, typename arg__Array_idx_list,
                                 typename... args__unchecked, typename... args__checked))>
        struct parse_feeders__next__make_channel_idx_list<args_3EC_1(I, R(pair::construct<I,
                                                                          H(tuple::construct<H(I, dict_Entry<H(I, arg__cons_node_id,
                                                                                                               arg__Array_idx_list)>,
                                                                                               args__unchecked...)>,
                                                                            tuple::construct<I, args__checked...>)>))> :
          parse_feeders__next__make_channel_idx_list<args_3EC_1(I,
                                                                R(pair::construct<I,
                                                                  H(tuple::construct<I, args__unchecked...>,
                                                                    tuple::construct<I, args__checked..., dict_Entry<H(I, arg__cons_node_id,
                                                                                                                       arg__Array_idx_list)>>)>))> {
        };

        

        template<prms_2J(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__error_pos_adaptor__feeder_msgs
        {
          template<bool arg__has_no_multiple_edges_, v1::error::Pos arg__producer_1_pos_, v1::error::Pos arg__producer_2_pos_>
          using multiple_edge_checker_msg = arg__multiple_edge_checker_msg<arg__has_no_multiple_edges_, arg__pos,
                                                                           arg__producer_1_pos_, arg__producer_2_pos_>;

          template<bool arg__has_no_loop, v1::error::Pos arg__producer_pos, v1::error::Pos arg__consumer_pos_>
          using loop_checker_msg = arg__loop_checker_msg<arg__has_no_loop, arg__pos, arg__producer_pos, arg__consumer_pos_>;

          template<bool arg__is_missing_, v1::error::Pos arg__pos_>
          using node_missing_msg = arg__node_missing_msg<arg__is_missing_, arg__pos, arg__pos_>;

          template<bool arg__is_producer_node_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
          using producer_node_checker_msg = arg__producer_node_checker_msg<arg__is_producer_node_, arg__pos, arg__pos_, arg__actual_parameter_>;

          template<bool arg__is_consumer_node_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
          using consumer_node_checker_msg = arg__consumer_node_checker_msg<arg__is_consumer_node_, arg__pos, arg__pos_, arg__actual_parameter_>;

          template<bool arg__is_equal_, unsigned arg__channel_count_, unsigned arg__parameter_count_>
          using param_count_checker_msg = arg__param_count_checker_msg<arg__is_equal_, arg__pos, arg__channel_count_, arg__parameter_count_>;

          template<bool arg__is_callable_, typename arg__consumer_function_>
          using consumer_invocation_checker_msg = arg__consumer_invocation_checker_msg<arg__is_callable_, arg__pos, arg__consumer_function_>;

          template<bool arg__is_missing_, v1::error::Pos arg__pos_>
          using channel_missing_msg = arg__channel_missing_msg<arg__is_missing_, arg__pos, arg__pos_>;

          template<bool arg__is_channel_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
          using channel_checker_msg = arg__channel_checker_msg<arg__is_channel_, arg__pos, arg__pos_, arg__actual_parameter_>;

          template<bool arg__is_object_type_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
          using channel_type_checker_msg = arg__channel_type_checker_msg<arg__is_object_type_, arg__pos, arg__pos_, arg__actual_parameter_>;

          template<bool arg__is_cv_unqualified_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
          using channel_cv_checker_msg = arg__channel_cv_checker_msg<arg__is_cv_unqualified_, arg__pos, arg__pos_, arg__actual_parameter_>;

          template<bool arg__is_not_nullptr_, v1::error::Pos arg__pos_>
          using channel_value_checker_msg = arg__channel_value_checker_msg<arg__is_not_nullptr_, arg__pos, arg__pos_>;

          template<bool arg__is_assignable_,
                   v1::error::Pos arg__producer_pos_, v1::error::Pos arg__channel_pos_, typename arg__return_type_, typename arg__channel_type_>
          using producer_return_value_checker_msg = arg__producer_return_value_checker_msg<arg__is_assignable_, arg__pos,
                                                                                           arg__producer_pos_, arg__channel_pos_,
                                                                                           arg__return_type_, arg__channel_type_>;

          template<bool arg__can_initialize_, v1::error::Pos arg__channel_pos_, typename arg__init_val_type_, typename arg__channel_type_>
          using channel_initval_checker_msg = arg__channel_initval_checker_msg<arg__can_initialize_, arg__pos, arg__channel_pos_,
                                                                               arg__init_val_type_, arg__channel_type_>;

          template<bool arg__is_compatible_,
                   v1::error::Pos arg__channel_pos_, v1::error::Pos arg__parameter_pos_, typename arg__channel_type_, typename arg__parameter_type_>
          using channel_and_consumer_param_checker_msg = arg__channel_and_consumer_param_checker_msg<arg__is_compatible_, arg__pos,
                                                                                                     arg__channel_pos_, arg__parameter_pos_,
                                                                                                     arg__channel_type_, arg__parameter_type_>;

          template<bool arg__is_comp_Unit_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
          using node_computation_checker_msg = arg__node_computation_checker_msg<arg__is_comp_Unit_, arg__pos, arg__pos_, arg__actual_parameter_>;

          template<bool arg__is_function_pointer_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
          using node_computation_fn_ptr_checker_msg = arg__node_computation_fn_ptr_checker_msg<arg__is_function_pointer_, arg__pos,
                                                                                               arg__pos_, arg__actual_parameter_>;

          template<bool arg__is_std_ratio_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
          using node_computation_wcet_checker_msg = arg__node_computation_wcet_checker_msg<arg__is_std_ratio_, arg__pos,
                                                                                           arg__pos_, arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
          using node_computation_wcet_den_checker_msg = arg__node_computation_wcet_den_checker_msg<arg__has_nonzero_denominator_, arg__pos,
                                                                                                   arg__pos_, arg__actual_parameter_>;

          template<bool arg__is_positive_, v1::error::Pos arg__pos_, typename arg__wcet_>
          using node_computation_wcet_validator_msg = arg__node_computation_wcet_validator_msg<arg__is_positive_, arg__pos, arg__pos_, arg__wcet_>;

          template<bool arg__is_std_ratio_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
          using node_period_checker_msg = arg__node_period_checker_msg<arg__is_std_ratio_, arg__pos, arg__pos_, arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
          using node_period_den_checker_msg = arg__node_period_den_checker_msg<arg__has_nonzero_denominator_, arg__pos,
                                                                               arg__pos_, arg__actual_parameter_>;

          template<bool arg__is_greater_than_or_equal_, v1::error::Pos arg__pos_, typename arg__period_, typename arg__wcet_>
          using node_period_validator_msg = arg__node_period_validator_msg<arg__is_greater_than_or_equal_, arg__pos,
                                                                           arg__pos_, arg__period_, arg__wcet_>;
        };

        

        template<prms_1E(I, _)>
        struct parse_feeders__next__empty_tuple {
          typedef tuple::construct<I> value;
        };

        

        template<prms_4BA(I, R(typename... args),
                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct parse_feeders__next__feeder_validated<args_4BA(I, R(Feeder<args...>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                              _, _, _, _, _, _, _, _, _, _, _, _, _)> :
#define parse_result parse_feeders__check_feeder_nodes<args_2D(I, _, _, _, _, _, R(tuple::construct<I, args...>), X, X, X, X, X, X)>
          utility::run_metaprogram<I, parse_result>,
          parse_feeders__next__parsed<args_4BB(I, R(parse_result), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                               _, _, _, _, _, _, _)> {
        };
#undef parse_result

        

        template<prms_2D(I, _, _, _, _, _, R(typename arg__consumer), _, R(typename... args__producer_id),
                         R(typename... args__dict_Entry__channel), _, _, _)>
        struct parse_feeders__check_feeder_nodes<args_2D(I, _, _, _, _, _, R(tuple::construct<I, arg__consumer>),
                                                         _, R(tuple::construct<I, args__producer_id...>),
                                                         R(tuple::construct<I, args__dict_Entry__channel...>), _, _, _)>
        {
          typedef dict_Entry_key<I, arg__producer_id_min> producer_id_min;
          typedef dict_Entry_key<I, arg__producer_id_max> producer_id_max;
          typedef dict_Entry<I, get_node_id<I, arg__pos, arg__node_pos, arg__tuple_construct__of__dict_Entry__of__Node__node_dict, arg__consumer,
                                            arg__specified_node_checker_msg>::value::value,
                             arg__consumer> consumer_entry;
          typedef tuple::construct<I, Arc<I, args__producer_id::value, consumer_entry::key, args__dict_Entry__channel::key>...> arcset;
          typedef tuple::construct<I, Channel<I, args__producer_id::value, consumer_entry::key,
                                              typename args__dict_Entry__channel::value>...> channel_list;
          typedef dict_Entry<I, consumer_entry::key, arg__Array_idx_list__cons_channel_idx_list> cons_channel_idx_list;
          typedef array::update::value<I, arg__Array__of__bool__is_producer_node,
                                       tuple::construct<I, dict_Entry_bool<I, args__producer_id::value, true>...>> is_producer_node;
        };

        template<prms_2D(I, _, _, _, _, _,
                         R(typename arg__producer, typename arg__channel, typename... args), _, R(typename... args__producer_id),
                         R(typename... args__dict_Entry__channel), _, _, R(v1::array::Idx... args__cons_channel_idx))>
        struct parse_feeders__check_feeder_nodes<args_2D(I, _, _, _, _, _, R(tuple::construct<I, arg__producer, arg__channel, args...>),
                                                         _, R(tuple::construct<I, args__producer_id...>),
                                                         R(tuple::construct<I, args__dict_Entry__channel...>), _, _,
                                                         R(Array_idx_list<I, args__cons_channel_idx...>))> :
#define producer_id typename get_node_id<H(I, arg__pos, arg__node_pos, arg__tuple_construct__of__dict_Entry__of__Node__node_dict, arg__producer,   \
                                           arg__specified_node_checker_msg)>::value
          parse_feeders__check_feeder_nodes<args_2D(I, _, _, _, _, _, R(tuple::construct<I, args...>),
                                                    R(arg__node_pos + 2), R(tuple::construct<I, args__producer_id..., producer_id>),
                                                    R(tuple::construct<H(I, args__dict_Entry__channel...,
                                                                         dict_Entry<I, (arg__arc_count
                                                                                        + sizeof...(args__dict_Entry__channel)), arg__channel>)>),
                                                    R(utility::min<I, dict_Entry_key<I, arg__producer_id_min>, producer_id>::value::value),
                                                    R(utility::max<I, dict_Entry_key<I, arg__producer_id_max>, producer_id>::value::value),
                                                    R(Array_idx_list<I, args__cons_channel_idx..., (arg__arc_count
                                                                                                    + sizeof...(args__dict_Entry__channel))>))> {
        };
#undef producer_id

        

        template<prms_4BB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                          R(typename... args__consumer), _, _)>
        struct parse_feeders__next__parsed<args_4BB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                    _, R(tuple::construct<I, args__consumer...>), _, _)> :
          utility::check_pairwise_distinctness<I, utility::nodes_pairwise_distinctness<I, arg__distinct_feeder_checker_msg>::template checker,
                                               arg__pos - sizeof...(args__consumer), 1, arg__pos,
                                               typename arg__parse_result::consumer_entry::value, typename args__consumer::value...>,
          parse_feeders__next<args_4BC(I, R(arg__pos + 1), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                       R(typename arg__parse_result::is_producer_node),
                                       R(arg__arc_count + tuple::size<I, typename arg__parse_result::arcset>::value),
                                       R(typename utility::list::
                                         update<H(I, arg__tuple_construct__of__tuple_construct__of__dict_Entry_array_idx__arc_list,
                                                  parse_feeders__update_arc_list,
                                                  parse_feeders__update_arc_list__data<
                                                  H(args_1G(I,
                                                            R(typename arg__parse_result::producer_id_min),
                                                            R(typename arg__parse_result::producer_id_max),
                                                            R(0 < arg__parse_result::producer_id_min::value),
                                                            R(typename arg__parse_result::arcset),
                                                            X))>)>::value),
                                       R(tuple::construct<I, args__consumer..., typename arg__parse_result::consumer_entry>),
                                       R(typename utility::list::cat<H(I, arg__tuple_construct__of__Channel__channel_list,
                                                                       typename arg__parse_result::channel_list)>::value),
                                       R(typename utility::list::append<
                                         H(I, arg__tuple_construct__of__dict_Entry__of__Array_idx_list__cons_channel_idx_list,
                                           typename arg__parse_result::cons_channel_idx_list)>::value),
                                       X)> {
        };

        

        template<prms_2EA(I, _, _, RR(prms_1G(, _, _, X, _, _)))>
        struct parse_feeders__update_arc_list<args_2EA(I, _, _, R(parse_feeders__update_arc_list__data<args_1G(I, _, _, R(true), _, _)>))>
        {
          static constexpr bool is_complete {false};
          typedef parse_feeders__update_arc_list__data<args_1G(I, _, _, R(arg__idx + 1 < arg__dict_Entry_key__producer_id_min::value), _, _)> data;
          typedef arg__tuple_construct__of__dict_Entry_array_idx__prev_arc_list_element value;
        };

#define prms_1G_1(I, _4, _5) prms_1G(I, _, _, X, _4, _5)
#define args_1G_1(I, _4, _5) args_1G(I, _, _, R(false), _4, _5)

#define prms_2EA_1(I, _2) prms_2EA(I, _, R(typename... args__dict_Entry_array_idx__consumer_id_to_channel_idx), _2)
#define args_2EA_1(I, _2) args_2EA(I, _, R(tuple::construct<I, args__dict_Entry_array_idx__consumer_id_to_channel_idx...>), _2)

        template<prms_2EA_1(I, RR(prms_1G_1(, X, R(typename... args__checked_arc))))>
        struct parse_feeders__update_arc_list<args_2EA_1(I, R(parse_feeders__update_arc_list__data<
                                                              args_1G_1(I, R(tuple::construct<I>), // this row needs no updating
                                                                        R(tuple::construct<I, args__checked_arc...>))>))>
        {
          static constexpr bool is_complete {false};
          typedef parse_feeders__update_arc_list__data<args_1G_1(I, R(tuple::construct<I, args__checked_arc...>), X)> data; // put back for next row
          typedef tuple::construct<I, args__dict_Entry_array_idx__consumer_id_to_channel_idx...> value;
        };

        template<prms_2EA_1(I, RR(prms_1G_1(, R(v1::dict::Key arg__consumer_id, v1::array::Idx arg__channel_idx,
                                                typename... args__unchecked_arc), R(typename... args__checked_arc))))>
        struct parse_feeders__update_arc_list<args_2EA_1(I, R(parse_feeders__update_arc_list__data<
                                                              args_1G_1(I, R(tuple::construct<H(I, Arc<H(I, arg__idx, arg__consumer_id,
                                                                                                         arg__channel_idx)>, // update this row
                                                                                                args__unchecked_arc...)>),
                                                                        R(tuple::construct<I, args__checked_arc...>))>))>
        {
          static constexpr bool is_complete {arg__idx == arg__dict_Entry_key__producer_id_max::value};
          typedef parse_feeders__update_arc_list__data<args_1G_1(I, R(tuple::construct<H(I, args__checked_arc...,
                                                                                         Arc<H(I, arg__idx, arg__consumer_id, arg__channel_idx)>,
                                                                                         args__unchecked_arc...)>),
                                                                 X)> data; // put back for next row
          typedef tuple::construct<I, args__dict_Entry_array_idx__consumer_id_to_channel_idx...,
                                   dict_Entry_array_idx<I, arg__consumer_id, arg__channel_idx>> value;
        };

        template<prms_2EA_1(I, RR(prms_1G_1(, R(v1::dict::Key arg__producer_id, v1::dict::Key arg__consumer_id, v1::array::Idx arg__channel_idx,
                                                typename... args__unchecked_arc), R(typename... args__checked_arc))))>
        struct parse_feeders__update_arc_list<args_2EA_1(I, R(parse_feeders__update_arc_list__data<
                                                              args_1G_1(I, R(tuple::construct<H(I, Arc<H(I, arg__producer_id,
                                                                                                         arg__consumer_id,
                                                                                                         arg__channel_idx)>, // keep going
                                                                                                args__unchecked_arc...>)),
                                                                        R(tuple::construct<I, args__checked_arc...>))>))> :
          parse_feeders__update_arc_list<args_2EA_1(I, R(parse_feeders__update_arc_list__data<
                                                         args_1G_1(I, R(tuple::construct<I, args__unchecked_arc...>),
                                                                   R(tuple::construct<H(I, args__checked_arc...,
                                                                                        Arc<H(I, arg__producer_id, arg__consumer_id,
                                                                                              arg__channel_idx)>)>))>))> {
        };

#include "v1_internals_end.hpp"
      }
      namespace program {

        template<typename I, v1::dict::Key arg__key, v1::array::Size arg__size>
        using dict_Entry_array_Size = dict_Entry<I, arg__key, void, std::integral_constant<v1::array::Size, arg__size>>;

      }
      namespace program {

#include "v1_internals_begin.hpp"

#define base_1G(I, _1) I                                                \
        opts  (_1, arg__Array__of__bool__is_intermediary_node, ())
#define decl_1G(I, _1)                          \
        base_1G(I##I,                           \
                type(_1, typename))
#define args_1G(I, _1) base_1G(I, _1)
#define prms_1G(...) decl_1G(__VA_ARGS__)

#define base_2GA(args_1G, _1)                                   \
        args_1G                                                 \
        opts  (_1, arg__Array__of__bool__is_actuator_node, ())
#define decl_2GA(I, _1G_1, _1)                  \
        base_2GA(prms_1G(I, _1G_1),             \
                 type(_1, typename))
#define args_2GA(I, _1G_1, _1) base_2GA(args_1G(I, _1G_1), _1)
#define prms_2GA(...) decl_2GA(__VA_ARGS__)

#define base_1H(_d1, I, _1) I                           \
        opts  (_1, arg__validate_nodes_msg, _d1)
#define decl_1H(_d1, I, _1)                                                                                                     \
        base_1H(_d1, I##I,                                                                                                      \
                type(_1, template<bool arg__is_expected_node_, v1::error::Pos arg__node_pos_, typename arg__node_type_> class))
#define args_1H(I, _1) base_1H((), I, _1)
#define prms_1H(...) decl_1H((), __VA_ARGS__)

#define base_2H(args_1H)                        \
        args_1H
#define decl_2H(I, _1H_1)                       \
        base_2H(prms_1H(I, _1H_1))
#define args_2H(I, _1H_1) base_2H(args_1H(I, _1H_1))
#define prms_2H(...) decl_2H(__VA_ARGS__)

        template<prms_2H(I, _)>
        struct construct__error_pos_adaptor__arg__validate_nodes_msg;

#define base_1P(_d1, I, _1) I                   \
        opts  (_1, arg__i, _d1)
#define decl_1P(_d1, I, _1)                     \
        base_1P(_d1, I##I,                      \
                type(_1, v1::array::Idx))
#define args_1P(I, _1) base_1P((), I, _1)
#define prms_1P(...) decl_1P((), __VA_ARGS__)

#define base_1Ab(_d1, I, _1) I                  \
        opts(_1, arg__is_done, _d1)
#define decl_1Ab(_d1, I, _1)                    \
        base_1Ab(_d1, I##I,                     \
                 type(_1, bool))
#define args_1Ab(I, _1) base_1Ab((), I, _1)
#define prms_1Ab(...) decl_1Ab((), __VA_ARGS__)

#define base_2P(args_1P, args_1Ab)              \
        args_1P                                 \
        args_1Ab
#define decl_2P(_1P_d1, _1Ab_d1, I, _1P_1, _1Ab_1)      \
        base_2P(decl_1P(_1P_d1, I, _1P_1),              \
                decl_1Ab(_1Ab_d1, , _1Ab_1))
#define args_2P(I, _1P_1, _1Ab_1) base_2P(args_1P(I, _1P_1), args_1Ab(, _1Ab_1))
#define prms_2P(...) decl_2P((), (), __VA_ARGS__)

#define base_3PA(I, _1, args_1H, args_2P) I                     \
        opts  (_1, arg__Array__of__dict_Key__node_ids, ())      \
          args_1H                                               \
          args_2P
#define decl_3PA(_1P_d1, _1Ab_d1, I, _1, _1H_1, _1P_1, _1Ab_1)  \
        base_3PA(I##I,                                          \
                 type(_1, typename),                            \
                 prms_1H(, _1H_1),                              \
                 decl_2P(_1P_d1, _1Ab_d1, , _1P_1, _1Ab_1))
#define args_3PA(I, _1, _1H_1, _1P_1, _1Ab_1) base_3PA(I, _1, args_1H(, _1H_1), args_2P(, _1P_1, _1Ab_1))
#define prms_3PA(...) decl_3PA((), (), __VA_ARGS__)

#define base_2GB(args_1G, args_3PA)             \
        args_1G                                 \
        args_3PA
#define decl_2GB(_1P_d1, _1Ab_d1, I, _1G_1, _3PA_1, _1H_1, _1P_1, _1Ab_1)       \
        base_2GB(prms_1G(I, _1G_1),                                             \
                 decl_3PA(_1P_d1, _1Ab_d1, , _3PA_1, _1H_1, _1P_1, _1Ab_1))
#define args_2GB(I, _1G_1, _3PA_1, _1H_1, _1P_1, _1Ab_1) base_2GB(args_1G(I, _1G_1), args_3PA(, _3PA_1, _1H_1, _1P_1, _1Ab_1))
#define prms_2GB(...) decl_2GB((), (), __VA_ARGS__)

        template<decl_2GB(_1P_d1 (0),
                          _1Ab_d1(arg__i == arg__Array__of__dict_Key__node_ids::size),
                          I, _, _, _, _, _)>
        struct construct__front_end__check_all_nodes_are_actuator_nodes;

#define base_3GA(args_2GA, args_3PA)            \
        args_2GA                                \
        args_3PA
#define decl_3GA(_1P_d1, _1Ab_d1, I, _1G_1, _2GA_1, _3PA_1, _1H_1, _1P_1, _1Ab_1)       \
        base_3GA(prms_2GA(I, _1G_1, _2GA_1),                                            \
                 decl_3PA(_1P_d1, _1Ab_d1, , _3PA_1, _1H_1, _1P_1, _1Ab_1))
#define args_3GA(I, _1G_1, _2GA_1, _3PA_1, _1H_1, _1P_1, _1Ab_1) base_3GA(args_2GA(I, _1G_1, _2GA_1), args_3PA(, _3PA_1, _1H_1, _1P_1, _1Ab_1))
#define prms_3GA(...) decl_3GA((), (), __VA_ARGS__)

        template<decl_3GA(_1P_d1 (0),
                          _1Ab_d1(arg__i == arg__Array__of__dict_Key__node_ids::size),
                          I, _, _, _, _, _, _)>
        struct construct__front_end__check_all_nodes_are_sensor_nodes;

#define base_1Y(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18,      \
                _d19, _d20, _d21, _d22, _d23, _d24, _d25, _d26, _d27,                                                   \
                I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18,                     \
                _19, _20, _21, _22, _23, _24, _25, _26, _27) I                                                          \
        opts  (_1 , arg__ete_delay_min_max_checker_msg, _d1)                                                            \
        opts  (_2 , arg__ete_delay_2_checker_msg, _d2)                                                                  \
        opts  (_3 , arg__ete_delay_2_validator_msg, _d3)                                                                \
        opts  (_4 , arg__ete_delay_3_checker_msg, _d4)                                                                  \
        opts  (_5 , arg__ete_delay_3_validator_msg, _d5)                                                                \
        opts  (_6 , arg__ete_delay_4_checker_msg, _d6)                                                                  \
        opts  (_7 , arg__ete_delay_4_den_checker_msg, _d7)                                                              \
        opts  (_8 , arg__ete_delay_4_validator_msg, _d8)                                                                \
        opts  (_9 , arg__ete_delay_5_checker_msg, _d9)                                                                  \
        opts  (_10, arg__ete_delay_5_den_checker_msg, _d10)                                                             \
        opts  (_11, arg__ete_delay_5_validator_msg, _d11)                                                               \
        opts  (_12, arg__ete_delay_2_computation_checker_msg, _d12)                                                     \
        opts  (_13, arg__ete_delay_2_computation_fn_ptr_checker_msg, _d13)                                              \
        opts  (_14, arg__ete_delay_2_computation_wcet_checker_msg, _d14)                                                \
        opts  (_15, arg__ete_delay_2_computation_wcet_den_checker_msg, _d15)                                            \
        opts  (_16, arg__ete_delay_2_computation_wcet_validator_msg, _d16)                                              \
        opts  (_17, arg__ete_delay_2_period_checker_msg, _d17)                                                          \
        opts  (_18, arg__ete_delay_2_period_den_checker_msg, _d18)                                                      \
        opts  (_19, arg__ete_delay_2_period_validator_msg, _d19)                                                        \
        opts  (_20, arg__ete_delay_3_computation_checker_msg, _d20)                                                     \
        opts  (_21, arg__ete_delay_3_computation_fn_ptr_checker_msg, _d21)                                              \
        opts  (_22, arg__ete_delay_3_computation_wcet_checker_msg, _d22)                                                \
        opts  (_23, arg__ete_delay_3_computation_wcet_den_checker_msg, _d23)                                            \
        opts  (_24, arg__ete_delay_3_computation_wcet_validator_msg, _d24)                                              \
        opts  (_25, arg__ete_delay_3_period_checker_msg, _d25)                                                          \
        opts  (_26, arg__ete_delay_3_period_den_checker_msg, _d26)                                                      \
        opts  (_27, arg__ete_delay_3_period_validator_msg, _d27)
#define decl_1Y(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18,                                 \
                _d19, _d20, _d21, _d22, _d23, _d24, _d25, _d26, _d27,                                                                              \
                I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18,                                                \
                _19, _20, _21, _22, _23, _24, _25, _26, _27)                                                                                       \
        base_1Y(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18,                                 \
                _d19, _d20, _d21, _d22, _d23, _d24, _d25, _d26, _d27, I##I,                                                                        \
                type(_1, template<H(bool arg__is_less_than_or_equal_to_max_delay_, v1::error::Pos arg__ete_delay_pos_,                             \
                                    typename arg__min_delay_, typename arg__max_delay_)> class),                                                   \
                type(_2, template<bool arg__is_Node_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),                 \
                type(_3, template<bool arg__is_sensor_node_, v1::error::Pos arg__ete_delay_pos_, typename arg__node_type_> class),                 \
                type(_4, template<bool arg__is_Node_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),                 \
                type(_5, template<bool arg__is_actuator_node_, v1::error::Pos arg__ete_delay_pos_, typename arg__node_type_> class),               \
                type(_6, template<bool arg__is_std_ratio_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),            \
                type(_7, template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class), \
                type(_8, template<bool arg__is_nonnegative_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),          \
                type(_9, template<bool arg__is_std_ratio_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),            \
                type(_10, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__ete_delay_pos_,                                       \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_11, template<bool arg__is_positive_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),            \
                type(_12, template<bool arg__is_comp_Unit_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),           \
                type(_13, template<bool arg__is_function_pointer_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),    \
                type(_14, template<bool arg__is_std_ratio_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),           \
                type(_15, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__ete_delay_pos_,                                       \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_16, template<bool arg__is_positive_, v1::error::Pos arg__ete_delay_pos_, typename arg__wcet_> class),                        \
                type(_17, template<bool arg__is_std_ratio_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),           \
                type(_18, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__ete_delay_pos_,                                       \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_19, template<H(bool arg__is_greater_than_or_equal_, v1::error::Pos arg__ete_delay_pos_,                                      \
                                     typename arg__period_, typename arg__wcet_)> class),                                                          \
                type(_20, template<bool arg__is_comp_Unit_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),           \
                type(_21, template<bool arg__is_function_pointer_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),    \
                type(_22, template<bool arg__is_std_ratio_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),           \
                type(_23, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__ete_delay_pos_,                                       \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_24, template<bool arg__is_positive_, v1::error::Pos arg__ete_delay_pos_, typename arg__wcet_> class),                        \
                type(_25, template<bool arg__is_std_ratio_, v1::error::Pos arg__ete_delay_pos_, typename arg__actual_parameter_> class),           \
                type(_26, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__ete_delay_pos_,                                       \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_27, template<H(bool arg__is_greater_than_or_equal_, v1::error::Pos arg__ete_delay_pos_,                                      \
                                     typename arg__period_, typename arg__wcet_)> class))
#define args_1Y(I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18,                                                \
                _19, _20, _21, _22, _23, _24, _25, _26, _27)                                                                                       \
        base_1Y((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (),                                                            \
                (), (), (), (), (), (), (), (), (),                                                                                                \
                I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27)
#define prms_1Y(...) decl_1Y((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (),        \
                             __VA_ARGS__)

#define base_1Z(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18,                      \
                _d19, _d20, _d21, _d22, _d23, _d24,                                                                                     \
                I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24) I     \
        opts(_1 , arg__correlation_node_checker_msg, _d1)                                                                               \
        opts(_2 , arg__correlation_node_validator_msg, _d2)                                                                             \
        opts(_3 , arg__correlation_distinct_node_checker_msg, _d3)                                                                      \
        opts(_4 , arg__correlation_2_checker_msg, _d4)                                                                                  \
        opts(_5 , arg__correlation_2_validator_msg, _d5)                                                                                \
        opts(_6 , arg__correlation_3_checker_msg, _d6)                                                                                  \
        opts(_7 , arg__correlation_3_den_checker_msg, _d7)                                                                              \
        opts(_8 , arg__correlation_3_validator_msg, _d8)                                                                                \
        opts(_9 , arg__correlation_2_computation_checker_msg, _d9)                                                                      \
        opts(_10, arg__correlation_2_computation_fn_ptr_checker_msg, _d10)                                                              \
        opts(_11, arg__correlation_2_computation_wcet_checker_msg, _d11)                                                                \
        opts(_12, arg__correlation_2_computation_wcet_den_checker_msg, _d12)                                                            \
        opts(_13, arg__correlation_2_computation_wcet_validator_msg, _d13)                                                              \
        opts(_14, arg__correlation_2_period_checker_msg, _d14)                                                                          \
        opts(_15, arg__correlation_2_period_den_checker_msg, _d15)                                                                      \
        opts(_16, arg__correlation_2_period_validator_msg, _d16)                                                                        \
        opts(_17, arg__correlation_node_computation_checker_msg, _d17)                                                                  \
        opts(_18, arg__correlation_node_computation_fn_ptr_checker_msg, _d18)                                                           \
        opts(_19, arg__correlation_node_computation_wcet_checker_msg, _d19)                                                             \
        opts(_20, arg__correlation_node_computation_wcet_den_checker_msg, _d20)                                                         \
        opts(_21, arg__correlation_node_computation_wcet_validator_msg, _d21)                                                           \
        opts(_22, arg__correlation_node_period_checker_msg, _d22)                                                                       \
        opts(_23, arg__correlation_node_period_den_checker_msg, _d23)                                                                   \
        opts(_24, arg__correlation_node_period_validator_msg, _d24)
#define decl_1Z(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18,                                 \
                _d19, _d20, _d21, _d22, _d23, _d24,                                                                                                \
                I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24)                  \
        base_1Z(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18,                                 \
                _d19, _d20, _d21, _d22, _d23, _d24, I##I,                                                                                          \
                type(_1, template<H(bool arg__is_Node_, v1::error::Pos arg__correlation_pos_,                                                      \
                                    v1::error::Pos arg__element_pos_, typename arg__actual_parameter_)> class),                                    \
                type(_2, template<H(bool arg__is_sensor_node_, v1::error::Pos arg__correlation_pos_,                                               \
                                    v1::error::Pos arg__element_pos_, typename arg__node_type_)> class),                                           \
                type(_3, template<H(bool arg__are_distinct_, v1::error::Pos arg__correlation_pos_,                                                 \
                                    v1::error::Pos arg__node_1_pos_, v1::error::Pos arg__node_2_pos_)> class),                                     \
                type(_4, template<bool arg__is_Node_, v1::error::Pos arg__correlation_pos_, typename arg__actual_parameter_> class),               \
                type(_5, template<bool arg__is_actuator_node_, v1::error::Pos arg__correlation_pos_, typename arg__node_type_> class),             \
                type(_6, template<bool arg__is_std_ratio_, v1::error::Pos arg__correlation_pos_, typename arg__actual_parameter_> class),          \
                type(_7, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__correlation_pos_,                                      \
                                    typename arg__actual_parameter_)> class),                                                                      \
                type(_8, template<bool arg__is_nonnegative_, v1::error::Pos arg__correlation_pos_, typename arg__actual_parameter_> class),        \
                type(_9, template<bool arg__is_comp_Unit_, v1::error::Pos arg__correlation_pos_, typename arg__actual_parameter_> class),          \
                type(_10, template<bool arg__is_function_pointer_, v1::error::Pos arg__correlation_pos_, typename arg__actual_parameter_> class),  \
                type(_11, template<bool arg__is_std_ratio_, v1::error::Pos arg__correlation_pos_, typename arg__actual_parameter_> class),         \
                type(_12, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__correlation_pos_,                                     \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_13, template<bool arg__is_positive_, v1::error::Pos arg__correlation_pos_, typename arg__wcet_> class),                      \
                type(_14, template<bool arg__is_std_ratio_, v1::error::Pos arg__correlation_pos_, typename arg__actual_parameter_> class),         \
                type(_15, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__correlation_pos_,                                     \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_16, template<H(bool arg__is_greater_than_or_equal_, v1::error::Pos arg__correlation_pos_,                                    \
                                     typename arg__period_, typename arg__wcet_)> class),                                                          \
                type(_17, template<H(bool arg__is_comp_Unit_, v1::error::Pos arg__correlation_pos_,                                                \
                                     v1::error::Pos arg__element_pos_, typename arg__actual_parameter_)> class),                                   \
                type(_18, template<H(bool arg__is_function_pointer_, v1::error::Pos arg__correlation_pos_,                                         \
                                     v1::error::Pos arg__element_pos_, typename arg__actual_parameter_)> class),                                   \
                type(_19, template<H(bool arg__is_std_ratio_, v1::error::Pos arg__correlation_pos_,                                                \
                                     v1::error::Pos arg__element_pos_, typename arg__actual_parameter_)> class),                                   \
                type(_20, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__correlation_pos_,                                     \
                                     v1::error::Pos arg__element_pos_, typename arg__actual_parameter_)> class),                                   \
                type(_21, template<H(bool arg__is_positive_, v1::error::Pos arg__correlation_pos_,                                                 \
                                     v1::error::Pos arg__element_pos_, typename arg__wcet_)> class),                                               \
                type(_22, template<H(bool arg__is_std_ratio_, v1::error::Pos arg__correlation_pos_,                                                \
                                     v1::error::Pos arg__element_pos_, typename arg__actual_parameter_)> class),                                   \
                type(_23, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__correlation_pos_,                                     \
                                     v1::error::Pos arg__element_pos_, typename arg__actual_parameter_)> class),                                   \
                type(_24, template<H(bool arg__is_greater_than_or_equal_, v1::error::Pos arg__correlation_pos_, v1::error::Pos arg__element_pos_,  \
                                     typename arg__period_, typename arg__wcet_)> class))
#define args_1Z(I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24)       \
        base_1Z((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (),                                                 \
                (), (), (), (), (), (),                                                                                                 \
                I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24)
#define prms_1Z(...) decl_1Z((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

#define base_2YA(args_1Y, args_1Z)              \
        args_1Y                                 \
        args_1Z
#define decl_2YA(_1Y_d1, _1Y_d2, _1Y_d3, _1Y_d4, _1Y_d5, _1Y_d6, _1Y_d7, _1Y_d8, _1Y_d9, _1Y_d10, _1Y_d11, _1Y_d12, _1Y_d13, _1Y_d14,           \
                 _1Y_d15, _1Y_d16, _1Y_d17, _1Y_d18, _1Y_d19, _1Y_d20, _1Y_d21, _1Y_d22, _1Y_d23, _1Y_d24, _1Y_d25, _1Y_d26, _1Y_d27,           \
                 _1Z_d1, _1Z_d2, _1Z_d3, _1Z_d4, _1Z_d5, _1Z_d6, _1Z_d7, _1Z_d8, _1Z_d9, _1Z_d10, _1Z_d11, _1Z_d12, _1Z_d13, _1Z_d14,           \
                 _1Z_d15, _1Z_d16, _1Z_d17, _1Z_d18, _1Z_d19, _1Z_d20, _1Z_d21, _1Z_d22, _1Z_d23, _1Z_d24,                                      \
                 I, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,                      \
                 _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                        \
                 _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                         \
                 _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                                \
        base_2YA(decl_1Y(_1Y_d1, _1Y_d2, _1Y_d3, _1Y_d4, _1Y_d5, _1Y_d6, _1Y_d7, _1Y_d8, _1Y_d9, _1Y_d10, _1Y_d11, _1Y_d12, _1Y_d13, _1Y_d14,   \
                         _1Y_d15, _1Y_d16, _1Y_d17, _1Y_d18, _1Y_d19, _1Y_d20, _1Y_d21, _1Y_d22, _1Y_d23, _1Y_d24, _1Y_d25, _1Y_d26, _1Y_d27,   \
                         I, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,              \
                         _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27),               \
                 decl_1Z(_1Z_d1, _1Z_d2, _1Z_d3, _1Z_d4, _1Z_d5, _1Z_d6, _1Z_d7, _1Z_d8, _1Z_d9, _1Z_d10, _1Z_d11, _1Z_d12, _1Z_d13, _1Z_d14,   \
                         _1Z_d15, _1Z_d16, _1Z_d17, _1Z_d18, _1Z_d19, _1Z_d20, _1Z_d21, _1Z_d22, _1Z_d23, _1Z_d24,                              \
                         , _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,               \
                         _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define args_2YA(I, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,              \
                 _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                \
                 _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                 \
                 _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                        \
        base_2YA(args_1Y(I, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,      \
                         _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27),       \
                 args_1Z(, _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,       \
                         _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define prms_2YA(...) decl_2YA((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (),      \
                               (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

#define base_1I(I, _1) I                        \
        opts  (_1, arg__pos, ())
#define decl_1I(I, _1)                          \
        base_1I(I##I,                           \
                type(_1, v1::error::Pos))
#define args_1I(I, _1) base_1I(I, _1)
#define prms_1I(...) decl_1I(__VA_ARGS__)

#define base_2YB(args_1I, args_1Y)              \
        args_1I                                 \
        args_1Y
#define decl_2YB(I, _1I_1, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,       \
                 _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27)                \
        base_2YB(prms_1I(I, _1I_1),                                                                                                     \
                 prms_1Y(, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,       \
                         _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27))
#define args_2YB(I, _1I_1, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,       \
                 _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27)                \
        base_2YB(args_1I(I, _1I_1),                                                                                                     \
                 args_1Y(, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,       \
                         _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27))
#define prms_2YB(...) decl_2YB(__VA_ARGS__)

        template<prms_2YB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__error_pos_adaptor__ete_delay_msgs;

#define base_2Z(args_1I, args_1Z)               \
        args_1I                                 \
        args_1Z
#define decl_2Z(I, _1I_1, _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,        \
                _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                         \
        base_2Z(prms_1I(I, _1I_1),                                                                                                      \
                prms_1Z(, _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,        \
                        _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define args_2Z(I, _1I_1, _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,        \
                _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                         \
        base_2Z(args_1I(I, _1I_1),                                                                                                      \
                args_1Z(, _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,        \
                        _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define prms_2Z(...) decl_2Z(__VA_ARGS__)

        template<prms_2Z(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__error_pos_adaptor__correlation_msgs;

#define base_2I(args_1I, _1)                            \
        args_1I                                         \
        opts  (_1, arg__tuple_construct__input, ())
#define decl_2I(I, _1I_1, _1)                   \
        base_2I(prms_1I(I, _1I_1),              \
                type(_1, typename))
#define args_2I(I, _1I_1, _1) base_2I(args_1I(I, _1I_1), _1)
#define prms_2I(...) decl_2I(__VA_ARGS__)

#define base_3IA(args_2I, _1)                           \
        args_2I                                         \
        opts  (_1, arg__follower_checker_msg, ())
#define decl_3IA(I, _1I_1, _2I_1, _1)                                                                                           \
        base_3IA(prms_2I(I, _1I_1, _2I_1),                                                                                      \
                 type(_1, template<bool arg__is_follower_correct_, v1::error::Pos arg__pos_, typename arg__follower_> class))
#define args_3IA(I, _1I_1, _2I_1, _1) base_3IA(args_2I(I, _1I_1, _2I_1), _1)
#define prms_3IA(...) decl_3IA(__VA_ARGS__)

        template<prms_3IA(I, _, _, _)>
        struct construct__front_end__done;

#define base_1B(_d1, I, _1) I                                   \
        opts  (_1, arg__Array__of__double__node_period_db, _d1)
#define decl_1B(_d1, I, _1)                     \
        base_1B(_d1, I##I,                      \
                type(_1, typename))
#define args_1B(I, _1) base_1B((), I, _1)
#define prms_1B(...) decl_1B((), __VA_ARGS__)

#define base_2BA(_d1, I, _1, args_1B) I                         \
        opts  (_1, arg__Array__of__double__node_wcet_db, _d1)   \
          args_1B
#define decl_2BA(_d1, _1B_d1, I, _1, _1B_1)     \
        base_2BA(_d1, I##I,                     \
                 type(_1, typename),            \
                 decl_1B(_1B_d1, , _1B_1))
#define args_2BA(I, _1, _1B_1) base_2BA((), I, _1, args_1B(, _1B_1))
#define prms_2BA(...) decl_2BA((), (), __VA_ARGS__)

#define base_1Ai(_d1, I, _1) I                                  \
        opts  (_1, arg__gedf_schedulability_checker_msg, _d1)
#define decl_1Ai(_d1, I, _1)                                                                                                                    \
        base_1Ai(_d1, I##I,                                                                                                                     \
                 type(_1, template<H(bool arg__is_successful_, typename arg__total_utilization_, typename arg__total_utilization_bound_,        \
                                     typename arg__max_utilization_, typename arg__max_utilization_bound_)> class))
#define args_1Ai(I, _1) base_1Ai((), I, _1)
#define prms_1Ai(...) decl_1Ai((), __VA_ARGS__)

#define base_2AiA(args_2BA, _1, args_1Ai)       \
        args_2BA                                \
        opts  (_1, arg__core_count, ())         \
          args_1Ai
#define decl_2AiA(I, _2BA_1, _1B_1, _1, _1Ai_1) \
        base_2AiA(prms_2BA(I, _2BA_1, _1B_1),   \
                  type(_1, v1::array::Size),    \
                  prms_1Ai(, _1Ai_1))
#define args_2AiA(I, _2BA_1, _1B_1, _1, _1Ai_1) base_2AiA(args_2BA(I, _2BA_1, _1B_1), _1, args_1Ai(, _1Ai_1))
#define prms_2AiA(...) decl_2AiA(__VA_ARGS__)

        template<prms_2AiA(I, _, _, _, _)>
        struct construct__schedulability_test__gedf;

#define base_1Ak(_d1, I, _1) I                                                          \
        opts  (_1, arg__tuple_construct__of__dict_Entry__of__Node__node_dict, _d1)
#define decl_1Ak(_d1, I, _1)                    \
        base_1Ak(_d1, I##I,                     \
                 type(_1, typename))
#define args_1Ak(I, _1) base_1Ak((), I, _1)
#define prms_1Ak(...) decl_1Ak((), __VA_ARGS__)

#define base_3BA(args_1Ak, args_2BA)            \
        args_1Ak                                \
        args_2BA
#define decl_3BA(_1Ak_d1, _2BA_d1, _1B_d1, I, _1Ak_1, _2BA_1, _1B_1)    \
        base_3BA(decl_1Ak(_1Ak_d1, I, _1Ak_1),                          \
                 decl_2BA(_2BA_d1, _1B_d1, , _2BA_1, _1B_1))
#define args_3BA(I, _1Ak_1, _2BA_1, _1B_1) base_3BA(args_1Ak(I, _1Ak_1), args_2BA(, _2BA_1, _1B_1))
#define prms_3BA(...) decl_3BA((), (), (), __VA_ARGS__)

#define base_1N(_d1, I, _1) I                                                   \
        opts  (_1, arg__Array__of__Array__of__dict_Key__adjacency_list, _d1)
#define decl_1N(_d1, I, _1)                     \
        base_1N(_d1, I##I,                      \
                type(_1, typename))
#define args_1N(I, _1) base_1N((), I, _1)
#define prms_1N(...) decl_1N((), __VA_ARGS__)

#define base_4BA(args_3BA, args_1N)             \
        args_3BA                                \
        args_1N
#define decl_4BA(_1Ak_d1, _2BA_d1, _1B_d1, _1N_d1, I, _1Ak_1, _2BA_1, _1B_1, _1N_1)     \
        base_4BA(decl_3BA(_1Ak_d1, _2BA_d1, _1B_d1, I, _1Ak_1, _2BA_1, _1B_1),          \
                 decl_1N(_1N_d1, , _1N_1))
#define args_4BA(I, _1Ak_1, _2BA_1, _1B_1, _1N_1) base_4BA(args_3BA(I, _1Ak_1, _2BA_1, _1B_1), args_1N(, _1N_1))
#define prms_4BA(...) decl_4BA((), (), (), (), __VA_ARGS__)

#define base_1L(I, _1) I                                        \
        opts  (_1, arg__Array__of__bool__is_sink_node, ())
#define decl_1L(I, _1)                          \
        base_1L(I##I,                           \
                type(_1, typename))
#define args_1L(I, _1) base_1L(I, _1)
#define prms_1L(...) decl_1L(__VA_ARGS__)

#define base_2L(args_1L, _1)                                    \
        args_1L                                                 \
        opts  (_1, arg__Array__of__bool__is_isolated_node, ())
#define decl_2L(I, _1L_1, _1)                   \
        base_2L(prms_1L(I, _1L_1),              \
                type(_1, typename))
#define args_2L(I, _1L_1, _1) base_2L(args_1L(I, _1L_1), _1)
#define prms_2L(...) decl_2L(__VA_ARGS__)

#define base_1O(I, _1) I                                        \
        opts  (_1, arg__producer_and_sink_are_connected, ())
#define decl_1O(I, _1)                                                                                                                             \
        base_1O(I##I,                                                                                                                              \
                type(_1,                                                                                                                           \
                     template<H(typename I_, v1::dict::Key arg__producer_node_id_, v1::dict::Key arg__sink_node_id_,                               \
                                template<bool arg__is_producer_node_id__, v1::dict::Key arg__node_id__> class arg__producer_node_id_checker_msg_,  \
                                template<bool arg__is_sink_node_id__, v1::dict::Key arg__node_id__> class arg__sink_node_id_checker_msg_)>         \
                     class))
#define args_1O(I, _1) base_1O(I, _1)
#define prms_1O(...) decl_1O(__VA_ARGS__)

#define base_5BA(args_2I, args_4BA, args_2L, args_1O)   \
        args_2I                                         \
        args_4BA                                        \
        args_2L                                         \
        args_1O
#define decl_5BA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1)    \
        base_5BA(prms_2I(I, _1I_1, _2I_1),                                              \
                 prms_4BA(, _1Ak_1, _2BA_1, _1B_1, _1N_1),                              \
                 prms_2L(, _1L_1, _2L_1),                                               \
                 prms_1O(, _1O_1))
#define args_5BA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1)                                            \
        base_5BA(args_2I(I, _1I_1, _2I_1), args_4BA(, _1Ak_1, _2BA_1, _1B_1, _1N_1), args_2L(, _1L_1, _2L_1), args_1O(, _1O_1))
#define prms_5BA(...) decl_5BA(__VA_ARGS__)

#define base_1K(_d1, _d2, I, _1, _2) I                          \
        opts  (_1, arg__producer_node_id_checker_msg, _d1)      \
          opts(_2, arg__sink_node_id_checker_msg, _d2)
#define decl_1K(_d1, _d2, I, _1, _2)                                                                                                    \
        base_1K(_d1, _d2, I##I,                                                                                                         \
                type(_1, template<H(bool arg__is_producer_node_id_, v1::error::Pos arg__ete_delay_or_correlation_pos_,                  \
                                    v1::error::Pos arg__node_pos_in_ete_delay_or_correlation_, v1::dict::Key arg__node_id_)> class),    \
                type(_2, template<H(bool arg__is_sink_node_id_, v1::error::Pos arg__ete_delay_or_correlation_pos_,                      \
                                    v1::error::Pos arg__node_pos_in_ete_delay_or_correlation_, v1::dict::Key arg__node_id_)> class))
#define args_1K(I, _1, _2) base_1K((), (), I, _1, _2)
#define prms_1K(...) decl_1K((), (), __VA_ARGS__)

#define base_2K(I, _1, _2, args_1K) I                                   \
        opts  (_1, arg__ete_delay_or_correlation_pos, ())               \
          opts(_2, arg__node_pos_in_ete_delay_or_correlation, ())       \
          args_1K
#define decl_2K(I, _1, _2, _1K_1, _1K_2)        \
        base_2K(I##I,                           \
                type(_1, v1::error::Pos),       \
                type(_2, v1::error::Pos),       \
                prms_1K(, _1K_1, _1K_2))
#define args_2K(I, _1, _2, _1K_1, _1K_2) base_2K(I, _1, _2, args_1K(, _1K_1, _1K_2))
#define prms_2K(...) decl_2K(__VA_ARGS__)

        template<prms_2K(I, _, _, _, _)>
        struct construct__error_pos_adaptor__producer_and_sink_are_connected_msgs;

#define base_1Q(_d1, I, _1) I                                           \
        opts  (_1, arg__sensing_release_domain_checker_msg, _d1)
#define decl_1Q(_d1, I, _1)                                                                                                                        \
        base_1Q(_d1, I##I,                                                                                                                         \
                type(_1, H(template<bool arg__is_in_sensing_release_domain_, v1::array::Idx arg__confluent_node_release_idx_, typename arg__path_> \
                           class)))
#define args_1Q(I, _1) base_1Q((), I, _1)
#define prms_1Q(...) decl_1Q((), __VA_ARGS__)

#define base_2Q(_d1, args_1Q, _1)                               \
        args_1Q                                                 \
        opts(_1, arg__correlation_threshold_checker_msg, _d1)
#define decl_2Q(_1Q_d1, _d1, I, _1Q_1, _1)                                                                      \
        base_2Q(_d1,                                                                                            \
                decl_1Q(_1Q_d1, I, _1Q_1),                                                                      \
                type(_1, template<H(bool arg__is_within_threshold_, v1::error::Pos arg__constraint_pos_,        \
                                    typename arg__correlation_, typename arg__threshold_,                       \
                                    typename arg__confluent_path_1_, typename arg__confluent_path_2_,           \
                                    v1::error::Pos arg__actuator_k_th_period_)> class))
#define args_2Q(I, _1Q_1, _1) base_2Q((), args_1Q(I, _1Q_1), _1)
#define prms_2Q(...) decl_2Q((), (), __VA_ARGS__)

#define base_1A(_d1, _d2, _d3, _d4, I, _1, _2, args_2Q, _3, _4) I       \
        opts  (_1, arg__specified_node_checker_msg, _d1)                \
          opts(_2, arg__sensor_actuator_connection_checker_msg, _d2)    \
          args_2Q                                                       \
          opts(_3, arg__correlation_follower_checker_msg, _d3)          \
          opts(_4, arg__ete_delay_follower_checker_msg, _d4)
#define decl_1A(_d1, _d2, _1Q_d1, _2Q_d1, _d3, _d4, I, _1, _2, _1Q_1, _2Q_1, _3, _4)                                                    \
        base_1A(_d1, _d2, _d3, _d4, I##I,                                                                                               \
                type(_1, template<H(bool arg__is_already_specified_, v1::error::Pos arg__feeder_or_ete_delay_or_correlation_pos_,       \
                                    v1::error::Pos arg__node_pos_in_feeder_or_ete_delay_or_correlation_)> class),                       \
                type(_2, template<H(bool arg__are_connected_, v1::error::Pos arg__constraint_pos_,                                      \
                                    v1::error::Pos arg__sensor_node_pos, v1::error::Pos arg__actuator_node_pos_)> class),               \
                decl_2Q(_1Q_d1, _2Q_d1, , _1Q_1, _2Q_1),                                                                                \
                type(_3, template<bool arg__is_followed_by_nothing_, v1::error::Pos arg__pos_, typename arg__follower_> class),         \
                type(_4, template<H(bool arg__is_followed_only_by_sequence_of_Correlation_,                                             \
                                    v1::error::Pos arg__pos_, typename arg__follower_)> class))
#define args_1A(I, _1, _2, _1Q_1, _2Q_1, _3, _4) base_1A((), (), (), (), I, _1, _2, args_2Q(, _1Q_1, _2Q_1), _3, _4)
#define prms_1A(...) decl_1A((), (), (), (), (), (), __VA_ARGS__)

#define base_6BAA(args_5BA, args_1A, args_1K)   \
        args_5BA                                \
        args_1A                                 \
        args_1K
#define decl_6BAA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4, _1K_1, _1K_2)   \
        base_6BAA(prms_5BA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1),                                                 \
                  prms_1A(, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4),                                                                          \
                  prms_1K(, _1K_1, _1K_2))
#define args_6BAA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4, _1K_1, _1K_2)   \
        base_6BAA(args_5BA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1),                                                 \
                  args_1A(, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4),                                                                          \
                  args_1K(, _1K_1, _1K_2))
#define prms_6BAA(...) decl_6BAA(__VA_ARGS__)

#define base_7BAAA(_d1, args_6BAA, args_1Z, _1)                                 \
        args_6BAA                                                               \
        args_1Z                                                                 \
        opts  (_1, arg__construct__error_pos_adaptor__correlation_msgs, _d1)
#define decl_7BAAA(_d1,                                                                                                                         \
                   I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4, _1K_1, _1K_2,  \
                   _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                       \
                   _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24, _1)                                          \
        base_7BAAA(_d1,                                                                                                                         \
                   prms_6BAA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,                    \
                             _1A_3, _1A_4, _1K_1, _1K_2),                                                                                       \
                   prms_1Z(, _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,             \
                           _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24),                                     \
                   type(_1, typename))
#define args_7BAAA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4, _1K_1, _1K_2,  \
                   _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                       \
                   _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24, _1)                                          \
        base_7BAAA((),                                                                                                                          \
                   args_6BAA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,                    \
                             _1A_3, _1A_4, _1K_1, _1K_2),                                                                                       \
                   args_1Z(, _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,             \
                           _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24), _1)
#define prms_7BAAA(...) decl_7BAAA((), __VA_ARGS__)

        template<decl_7BAAA(_d1(construct__error_pos_adaptor__correlation_msgs<args_2Z(I,
                                                                                       _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                                       _, _, _, _, _)>),
                            I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _)>
        struct construct__front_end__parse_correlations;

#define base_7BAAB(args_6BAA)                   \
        args_6BAA
#define decl_7BAAB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4, _1K_1, _1K_2)  \
        base_7BAAB(prms_6BAA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,                    \
                             _1A_3, _1A_4, _1K_1, _1K_2))
#define args_7BAAB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4, _1K_1, _1K_2)  \
        base_7BAAB(args_6BAA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,                    \
                             _1A_3, _1A_4, _1K_1, _1K_2))
#define prms_7BAAB(...) decl_7BAAB(__VA_ARGS__)

        template<prms_7BAAB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__validate_correlation;

#define base_1R(_d1, I, _1) I                           \
        opts(_1, arg__ete_delay_checker_msg, _d1)
#define decl_1R(_d1, I, _1)                                                                                                                     \
        base_1R(_d1, I##I,                                                                                                                      \
                type(_1, template<H(bool arg__is_between_min_and_max_delays_, v1::error::Pos arg__constraint_pos_,                              \
                                    typename arg__min_, typename arg__end_to_end_delay_, typename arg__max_, typename arg__end_to_end_path_,    \
                                    v1::error::Pos arg__sensor_k_th_period)> class))
#define args_1R(I, _1) base_1R((), I, _1)
#define prms_1R(...) decl_1R((), __VA_ARGS__)

#define base_2A(_d1, args_1A, _1, args_1R, args_1K)             \
        args_1A                                                 \
        opts  (_1, arg__feeder_follower_checker_msg, _d1)       \
          args_1R                                               \
          args_1K
#define decl_2A(_1A_d1, _1A_d2, _1Q_d1, _2Q_d1, _1A_d3, _1A_d4, _d1, _1R_d1, _1K_d1, _1K_d2,                            \
                I, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4, _1, _1R_1, _1K_1, _1K_2)                                   \
        base_2A(_d1,                                                                                                    \
                decl_1A(_1A_d1, _1A_d2, _1Q_d1, _2Q_d1, _1A_d3, _1A_d4, I, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4),   \
                type(_1, template<H(bool arg__is_followed_only_by_sequence_of_ETE_delay_or_Correlation_,                \
                                    v1::error::Pos arg__pos_, typename arg__follower_)> class),                         \
                decl_1R(_1R_d1, , _1R_1),                                                                               \
                decl_1K(_1K_d1, _1K_d2, , _1K_1, _1K_2))
#define args_2A(I, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4, _1, _1R_1, _1K_1, _1K_2)                                           \
        base_2A((), args_1A(I, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4), _1, args_1R(, _1R_1), args_1K(, _1K_1, _1K_2))
#define prms_2A(...) decl_2A((), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

#define base_6BAB(args_5BA, args_2A)            \
        args_5BA                                \
        args_2A
#define decl_6BAB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,       \
                  _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2)                                                             \
        base_6BAB(prms_5BA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1),                         \
                  prms_2A(, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2))
#define args_6BAB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,       \
                  _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2)                                                             \
        base_6BAB(args_5BA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1),                         \
                  args_2A(, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2))
#define prms_6BAB(...) decl_6BAB(__VA_ARGS__)

#define base_7BABA(args_6BAB, args_2YA)         \
        args_6BAB                               \
        args_2YA
#define decl_7BABA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,                      \
                   _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2,                                                                            \
                   _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,               \
                   _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,              \
                   _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,               \
                   _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                      \
        base_7BABA(prms_6BAB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,            \
                             _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2),                                                                 \
                   prms_2YA(, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,    \
                            _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,     \
                            _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,      \
                            _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define args_7BABA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,                      \
                   _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2,                                                                            \
                   _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,               \
                   _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,              \
                   _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,               \
                   _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                      \
        base_7BABA(args_6BAB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,            \
                             _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2),                                                                 \
                   args_2YA(, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,    \
                            _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,     \
                            _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,      \
                            _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define prms_7BABA(...) decl_7BABA(__VA_ARGS__)

        template<prms_7BABA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__feeders_parsed;

#define base_8BABA(_d1, args_7BABA, _1)                                         \
        args_7BABA                                                              \
        opts  (_1, arg__construct__error_pos_adaptor__ete_delay_msgs, _d1)
#define decl_8BABA(_d1, I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,                 \
                   _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2,                                                                            \
                   _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,               \
                   _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,              \
                   _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,               \
                   _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24, _1)                                  \
        base_8BABA(_d1,                                                                                                                 \
                   prms_7BABA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,           \
                              _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2,                                                                 \
                              _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,    \
                              _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,   \
                              _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,    \
                              _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24),                          \
                   type(_1, typename))
#define args_8BABA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,                              \
                   _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2,                                                                                    \
                   _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,                       \
                   _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                      \
                   _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                       \
                   _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24, _1)                                          \
        base_8BABA((), args_7BABA(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,               \
                                  _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2,                                                                     \
                                  _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,        \
                                  _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,       \
                                  _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,        \
                                  _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24), _1)
#define prms_8BABA(...) decl_8BABA((), __VA_ARGS__)

        template<decl_8BABA(_d1(construct__error_pos_adaptor__ete_delay_msgs<args_2YB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                                      _, _, _, _, _, _, _, _)>),
                            I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__parse_ete_delays;

#define base_7BABB(args_6BAB)                   \
        args_6BAB
#define decl_7BABB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,              \
                   _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2)                                                                    \
        base_7BABB(prms_6BAB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,    \
                             _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2))
#define args_7BABB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,              \
                   _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2)                                                                    \
        base_7BABB(args_6BAB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1L_1, _2L_1, _1O_1, _1A_1, _1A_2, _1Q_1, _2Q_1,    \
                             _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2))
#define prms_7BABB(...) decl_7BABB(__VA_ARGS__)

        template<prms_7BABB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__validate_ete_delay;

#define base_3A(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14,                      \
                _d15, _d16, _d17, _d18, _d19, _d20, _d21, _d22, _d23, _d24, _d25, _d26, _d27, _d28,             \
                I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14,                                 \
                _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, args_2A, args_2YA) I      \
        opts(_1 , arg__check_cycle_msg, _d1)                                                                    \
        opts(_2 , arg__multiple_edge_checker_msg, _d2)                                                          \
        opts(_3 , arg__loop_checker_msg, _d3)                                                                   \
        opts(_4 , arg__node_missing_msg, _d4)                                                                   \
        opts(_5 , arg__producer_node_checker_msg, _d5)                                                          \
        opts(_6 , arg__consumer_node_checker_msg, _d6)                                                          \
        opts(_7 , arg__param_count_checker_msg, _d7)                                                            \
        opts(_8 , arg__consumer_invocation_checker_msg, _d8)                                                    \
        opts(_9 , arg__channel_missing_msg, _d9)                                                                \
        opts(_10, arg__channel_checker_msg, _d10)                                                               \
        opts(_11, arg__channel_type_checker_msg, _d11)                                                          \
        opts(_12, arg__channel_cv_checker_msg, _d12)                                                            \
        opts(_13, arg__channel_value_checker_msg, _d13)                                                         \
        opts(_14, arg__producer_return_value_checker_msg, _d14)                                                 \
        opts(_15, arg__channel_initval_checker_msg, _d15)                                                       \
        opts(_16, arg__channel_and_consumer_param_checker_msg, _d16)                                            \
        opts(_17, arg__feeder_node_computation_checker_msg, _d17)                                               \
        opts(_18, arg__feeder_node_computation_fn_ptr_checker_msg, _d18)                                        \
        opts(_19, arg__feeder_node_computation_wcet_checker_msg, _d19)                                          \
        opts(_20, arg__feeder_node_computation_wcet_den_checker_msg, _d20)                                      \
        opts(_21, arg__feeder_node_computation_wcet_validator_msg, _d21)                                        \
        opts(_22, arg__feeder_node_period_checker_msg, _d22)                                                    \
        opts(_23, arg__feeder_node_period_den_checker_msg, _d23)                                                \
        opts(_24, arg__feeder_node_period_validator_msg, _d24)                                                  \
        opts(_25, arg__distinct_feeder_checker_msg, _d25)                                                       \
        opts(_26, arg__validate_isolated_nodes_msg, _d26)                                                       \
        opts(_27, arg__validate_source_nodes_msg, _d27)                                                         \
        opts(_28, arg__validate_sink_nodes_msg, _d28)                                                           \
        args_2A                                                                                                 \
        args_2YA
#define decl_3A(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14,                                                         \
                _d15, _d16, _d17, _d18, _d19, _d20, _d21, _d22, _d23, _d24, _d25, _d26, _d27, _d28,                                                \
                _1A_d1, _1A_d2, _1Q_d1, _2Q_d1, _1A_d3, _1A_d4, _2A_d1, _1R_d1, _1K_d1, _1K_d2,                                                    \
                _1Y_d1, _1Y_d2, _1Y_d3, _1Y_d4, _1Y_d5, _1Y_d6, _1Y_d7, _1Y_d8, _1Y_d9, _1Y_d10, _1Y_d11, _1Y_d12, _1Y_d13, _1Y_d14,               \
                _1Y_d15, _1Y_d16, _1Y_d17, _1Y_d18, _1Y_d19, _1Y_d20, _1Y_d21, _1Y_d22, _1Y_d23, _1Y_d24, _1Y_d25, _1Y_d26, _1Y_d27,               \
                _1Z_d1, _1Z_d2, _1Z_d3, _1Z_d4, _1Z_d5, _1Z_d6, _1Z_d7, _1Z_d8, _1Z_d9, _1Z_d10, _1Z_d11, _1Z_d12, _1Z_d13, _1Z_d14,               \
                _1Z_d15, _1Z_d16, _1Z_d17, _1Z_d18, _1Z_d19, _1Z_d20, _1Z_d21, _1Z_d22, _1Z_d23, _1Z_d24,                                          \
                I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14,                                                                    \
                _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _1A_1, _1A_2, _1Q_1, _2Q_1,                                  \
                _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                                  \
                _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                                     \
                _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                            \
                _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                             \
                _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                                    \
        base_3A(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14,                                                         \
                _d15, _d16, _d17, _d18, _d19, _d20, _d21, _d22, _d23, _d24, _d25, _d26, _d27, _d28, I##I,                                          \
                type(_1, template<bool arg__has_no_cycle_, typename arg__node_pos_forming_closed_path_> class),                                    \
                type(_2, template<H(bool arg__has_no_multiple_edges_, v1::error::Pos arg__feeder_pos_,                                             \
                                    v1::error::Pos arg__producer_1_pos_, v1::error::Pos arg__producer_2_pos_)> class),                             \
                type(_3, template<H(bool arg__has_no_loop, v1::error::Pos arg__feeder_pos_,                                                        \
                                    v1::error::Pos arg__producer_pos, v1::error::Pos arg__consumer_pos_)> class),                                  \
                type(_4, template<bool arg__is_missing_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_> class),                        \
                type(_5, template<H(bool arg__is_producer_node_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                        \
                                    typename arg__actual_parameter_)> class),                                                                      \
                type(_6, template<H(bool arg__is_consumer_node_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                        \
                                    typename arg__actual_parameter_)> class),                                                                      \
                type(_7, template<H(bool arg__is_equal_, v1::error::Pos arg__feeder_pos_, unsigned arg__channel_count_,                            \
                                    unsigned arg__parameter_count_)> class),                                                                       \
                type(_8, template<bool arg__is_callable_, v1::error::Pos arg__feeder_pos_, typename arg__consumer_function_> class),               \
                type(_9, template<bool arg__is_missing_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_> class),                        \
                type(_10, template<H(bool arg__is_channel_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                             \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_11, template<H(bool arg__is_object_type_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                         \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_12, template<H(bool arg__is_cv_unqualified_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                      \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_13, template<bool arg__is_not_nullptr_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_> class),                   \
                type(_14, template<H(bool arg__is_assignable_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__producer_pos_,                 \
                                     v1::error::Pos arg__channel_pos_, typename arg__return_type_, typename arg__channel_type_)> class),           \
                type(_15, template<H(bool arg__can_initialize_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__channel_pos_,                 \
                                     typename arg__init_val_type_, typename arg__channel_type_)> class),                                           \
                type(_16, template<H(bool arg__is_compatible_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__channel_pos_,                  \
                                     v1::error::Pos arg__parameter_pos_, typename arg__channel_type_, typename arg__parameter_type_)> class),      \
                type(_17, template<H(bool arg__is_comp_Unit_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                           \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_18, template<H(bool arg__is_function_pointer_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                    \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_19, template<H(bool arg__is_std_ratio_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                           \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_20, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_21, template<bool arg__is_positive_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_, typename arg__wcet_> class), \
                type(_22, template<H(bool arg__is_std_ratio_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                           \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_23, template<H(bool arg__has_nonzero_denominator_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,                \
                                     typename arg__actual_parameter_)> class),                                                                     \
                type(_24, template<H(bool arg__is_greater_than_or_equal_, v1::error::Pos arg__feeder_pos_, v1::error::Pos arg__pos_,               \
                                     typename arg__period_, typename arg__wcet_)> class),                                                          \
                type(_25, template<bool arg__are_distinct_, v1::error::Pos arg__feeder_1_pos_, v1::error::Pos arg__feeder_2_pos_> class),          \
                type(_26, template<bool arg__is_sensor_node_, v1::error::Pos arg__node_pos_, typename arg__node_type_> class),                     \
                type(_27, template<bool arg__is_sensor_node_, v1::error::Pos arg__node_pos_, typename arg__node_type_> class),                     \
                type(_28, template<bool arg__is_actuator_node_, v1::error::Pos arg__node_pos_, typename arg__node_type_> class),                   \
                decl_2A(_1A_d1, _1A_d2, _1Q_d1, _2Q_d1, _1A_d3, _1A_d4, _2A_d1, _1R_d1, _1K_d1, _1K_d2,                                            \
                        , _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2),                                                   \
                decl_2YA(_1Y_d1, _1Y_d2, _1Y_d3, _1Y_d4, _1Y_d5, _1Y_d6, _1Y_d7, _1Y_d8, _1Y_d9, _1Y_d10, _1Y_d11, _1Y_d12, _1Y_d13, _1Y_d14,      \
                         _1Y_d15, _1Y_d16, _1Y_d17, _1Y_d18, _1Y_d19, _1Y_d20, _1Y_d21, _1Y_d22, _1Y_d23, _1Y_d24, _1Y_d25, _1Y_d26, _1Y_d27,      \
                         _1Z_d1, _1Z_d2, _1Z_d3, _1Z_d4, _1Z_d5, _1Z_d6, _1Z_d7, _1Z_d8, _1Z_d9, _1Z_d10, _1Z_d11, _1Z_d12, _1Z_d13, _1Z_d14,      \
                         _1Z_d15, _1Z_d16, _1Z_d17, _1Z_d18, _1Z_d19, _1Z_d20, _1Z_d21, _1Z_d22, _1Z_d23, _1Z_d24,                                 \
                         , _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,                  \
                         _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                   \
                         _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                    \
                         _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define args_3A(I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14,                                                         \
                _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _1A_1, _1A_2, _1Q_1, _2Q_1,                       \
                _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                       \
                _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                          \
                _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                 \
                _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                  \
                _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                         \
        base_3A((), (), (), (), (), (), (), (), (), (), (), (), (), (),                                                                 \
                (), (), (), (), (), (), (), (), (), (), (), (), (), (),                                                                 \
                I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14,                                                         \
                _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28,                                                   \
                args_2A(, _1A_1, _1A_2, _1Q_1, _2Q_1, _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2),                                        \
                args_2YA(, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8, _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14,       \
                         _1Y_15, _1Y_16, _1Y_17, _1Y_18, _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,        \
                         _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,         \
                         _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define prms_3A(...) decl_3A((), (), (), (), (), (), (), (), (), (), (), (), (), (),    \
                             (), (), (), (), (), (), (), (), (), (), (), (), (), (),    \
                             (), (), (), (), (), (), (), (), (), (),                    \
                             (), (), (), (), (), (), (), (), (), (), (), (), (), (),    \
                             (), (), (), (), (), (), (), (), (), (), (), (), (),        \
                             (), (), (), (), (), (), (), (), (), (), (), (), (), (),    \
                             (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

#define base_1M(_d1, I, _1) I                   \
        opts  (_1, args__node_id, _d1)
#define decl_1M(I, _1)                          \
        base_1M((), I##I,                       \
                type(_1, v1::dict::Key...))
#define args_1M(I, _1) base_1M((...), I, _1)
#define prms_1M(...) decl_1M(__VA_ARGS__)

        template<prms_1M(I, _)>
        struct construct__path;

#define base_1V(I, _1, _2) I                            \
        opts  (_1, arg__construct__path__lhs, ())       \
          opts(_2, arg__construct__path__rhs, ())
#define decl_1V(I, _1, _2)                      \
        base_1V(I##I,                           \
                type(_1, typename),             \
                type(_2, typename))
#define args_1V(I, _1, _2) base_1V(I, _1, _2)
#define prms_1V(...) decl_1V(__VA_ARGS__)

        template<prms_1V(I, _, _)>
        struct construct__two_paths_concatenated;

#define base_1X(_d1, I, _1) I                   \
        opts  (_1, arg__construct__path, _d1)
#define decl_1X(_d1, I, _1)                     \
        base_1X(_d1, I##I,                      \
                type(_1, typename))
#define args_1X(I, _1) base_1X((), I, _1)
#define prms_1X(...) decl_1X((), __VA_ARGS__)

#define base_2BB(args_1B, args_1X)              \
        args_1B                                 \
        args_1X
#define decl_2BB(_1X_d1, I, _1B_1, _1X_1)       \
        base_2BB(prms_1B(I, _1B_1),             \
                 decl_1X(_1X_d1, , _1X_1))
#define args_2BB(I, _1B_1, _1X_1) base_2BB(args_1B(I, _1B_1), args_1X(, _1X_1))
#define prms_2BB(...) decl_2BB((), __VA_ARGS__)

        template<prms_2BB(I, _, _)>
        struct construct__hyperperiod;

        enum class construct__hyperperiod_as_release_idx__owner {
          FIRST_NODE,
          LAST_NODE
        };

#define base_3BB(args_2BB, _1)                  \
        args_2BB                                \
        opts  (_1, arg__whose_release_idx, ())
#define decl_3BB(I, _1B_1, _1X_1, _1)                                           \
        base_3BB(prms_2BB(I, _1B_1, _1X_1),                                     \
                 type(_1, construct__hyperperiod_as_release_idx__owner))
#define args_3BB(I, _1B_1, _1X_1, _1) base_3BB(args_2BB(I, _1B_1, _1X_1), _1)
#define prms_3BB(...) decl_3BB(__VA_ARGS__)

        template<prms_3BB(I, _, _, _)>
        struct construct__hyperperiod_as_release_idx;

#define base_1U(I, _1) I                        \
        opts  (_1, arg__prod_release_idx, ())
#define decl_1U(I, _1)                          \
        base_1U(I##I,                           \
                type(_1, v1::array::Idx))
#define args_1U(I, _1) base_1U(I, _1)
#define prms_1U(...) decl_1U(__VA_ARGS__)

#define base_2U(args_1U, args_2BB)              \
        args_1U                                 \
        args_2BB
#define decl_2U(I, _1U_1, _1B_1, _1X_1)         \
        base_2U(prms_1U(I, _1U_1),              \
                prms_2BB(, _1B_1, _1X_1))
#define args_2U(I, _1U_1, _1B_1, _1X_1) base_2U(args_1U(I, _1U_1), args_2BB(, _1B_1, _1X_1))
#define prms_2U(...) decl_2U(__VA_ARGS__)

        template<prms_2U(I, _, _, _)>
        struct construct__ete_delay;

#define base_3U(_d1, args_2U, _1)                               \
        args_2U                                                 \
        opts  (_1, arg__construct__reading_timeset, _d1)
#define decl_3U(_d1, I, _1U_1, _1B_1, _1X_1, _1)        \
        base_3U(_d1,                                    \
                prms_2U(I, _1U_1, _1B_1, _1X_1),        \
                type(_1, typename))
#define args_3U(I, _1U_1, _1B_1, _1X_1, _1) base_3U((), args_2U(I, _1U_1, _1B_1, _1X_1), _1)
#define prms_3U(...) decl_3U((), __VA_ARGS__)

#define base_4UA(_d1, args_3U, _1)                      \
        args_3U                                         \
        opts  (_1, arg__reading_timeset_is_empty, _d1)
#define decl_4UA(_d1, I, _1U_1, _1B_1, _1X_1, _3U_1, _1)        \
        base_4UA(_d1,                                           \
                 prms_3U(I, _1U_1, _1B_1, _1X_1, _3U_1),        \
                 type(_1, bool))
#define args_4UA(I, _1U_1, _1B_1, _1X_1, _3U_1, _1) base_4UA((), args_3U(I, _1U_1, _1B_1, _1X_1, _3U_1), _1)
#define prms_4UA(...) decl_4UA((), __VA_ARGS__)

        template<decl_4UA(_d1(arg__construct__reading_timeset::release_idx_begin == arg__construct__reading_timeset::release_idx_end),
                          I, _, _, _, _, _)>
        struct construct__ete_delay__reading_timeset;

#define base_1T(I, _1) I                        \
        opts  (_1, arg__prod_node_id, ())
#define decl_1T(I, _1)                          \
        base_1T(I##I,                           \
                type(_1, v1::dict::Key))
#define args_1T(I, _1) base_1T(I, _1)
#define prms_1T(...) decl_1T(__VA_ARGS__)

#define base_2TA(args_1T, _1)                   \
        args_1T                                 \
        opts  (_1, arg__cons_node_id, ())
#define decl_2TA(I, _1T_1, _1)                  \
        base_2TA(prms_1T(I, _1T_1),             \
                 type(_1, v1::dict::Key))
#define args_2TA(I, _1T_1, _1) base_2TA(args_1T(I, _1T_1), _1)
#define prms_2TA(...) decl_2TA(__VA_ARGS__)

#define base_2BC(args_1B, args_2TA, args_1U)    \
        args_1B                                 \
        args_2TA                                \
        args_1U
#define decl_2BC(I, _1B_1, _1T_1, _2TA_1, _1U_1)        \
        base_2BC(prms_1B(I, _1B_1),                     \
                 prms_2TA(, _1T_1, _2TA_1),             \
                 prms_1U(, _1U_1))
#define args_2BC(I, _1B_1, _1T_1, _2TA_1, _1U_1) base_2BC(args_1B(I, _1B_1), args_2TA(, _1T_1, _2TA_1), args_1U(, _1U_1))
#define prms_2BC(...) decl_2BC(__VA_ARGS__)

        template<prms_2BC(I, _, _, _, _)>
        struct construct__reading_timeset;

#define base_3T(args_2U, args_2TA, _1)          \
        args_2U                                 \
        args_2TA                                \
        opts  (_1, arg__cons_release_idx, ())
#define decl_3T(I, _1U_1, _1B_1, _1X_1, _1T_1, _2TA_1, _1)      \
        base_3T(prms_2U(I, _1U_1, _1B_1, _1X_1),                \
                prms_2TA(, _1T_1, _2TA_1),                      \
                type(_1, v1::array::Idx))
#define args_3T(I, _1U_1, _1B_1, _1X_1, _1T_1, _2TA_1, _1) base_3T(args_2U(I, _1U_1, _1B_1, _1X_1), args_2TA(, _1T_1, _2TA_1), _1)
#define prms_3T(...) decl_3T(__VA_ARGS__)

        template<prms_3T(I, _, _, _, _, _, _)>
        struct construct__ete_delay__at_reading_point;

#define base_4UB(_d1, _d2, _d3, args_3U, args_2TA, _1, args_2P, _2, _3) \
        args_3U                                                         \
        args_2TA                                                        \
        opts  (_1, arg__min_ete_delay, _d1)                             \
          args_2P                                                       \
          opts(_2, arg__construct__ete_delay__at_reading_point, _d2)    \
          opts(_3, arg__min_ete_delay_update_is_needed, _d3)
#define decl_4UB(_d1, _1P_d1, _1Ab_d1, _d2, _d3, I, _1U_1, _1B_1, _1X_1, _3U_1, _1T_1, _2TA_1, _1, _1P_1, _1Ab_1, _2, _3)       \
        base_4UB(_d1, _d2, _d3,                                                                                                 \
                 prms_3U(I, _1U_1, _1B_1, _1X_1, _3U_1),                                                                        \
                 prms_2TA(, _1T_1, _2TA_1),                                                                                     \
                 type(_1, typename),                                                                                            \
                 decl_2P(_1P_d1, _1Ab_d1, , _1P_1, _1Ab_1),                                                                     \
                 type(_2, typename),                                                                                            \
                 type(_3, bool))
#define args_4UB(I, _1U_1, _1B_1, _1X_1, _3U_1, _1T_1, _2TA_1, _1, _1P_1, _1Ab_1, _2, _3)                                               \
        base_4UB((), (), (), args_3U(I, _1U_1, _1B_1, _1X_1, _3U_1), args_2TA(, _1T_1, _2TA_1), _1, args_2P(, _1P_1, _1Ab_1), _2, _3)
#define prms_4UB(...) decl_4UB((), (), (), (), (), __VA_ARGS__)

        template<decl_4UB(_d1    (utility::fp::positive_infinity),
                          _1P_d1 (arg__construct__reading_timeset::release_idx_begin),
                          _1Ab_d1(arg__i + 1 == arg__construct__reading_timeset::release_idx_end),
                          _d2    (construct__ete_delay__at_reading_point<args_3T(I, _, _, _, _, _, R(arg__i))>),
                          _d3    (utility::fp::lt(arg__construct__ete_delay__at_reading_point::value, arg__min_ete_delay::value)),
                          I, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__ete_delay__iterate_reading_timeset;

#define base_1S(I, _1) I                        \
        opts  (_1, arg__actuator_node_id, ())
#define decl_1S(I, _1)                          \
        base_1S(I##I,                           \
                type(_1, v1::dict::Key))
#define args_1S(I, _1) base_1S(I, _1)
#define prms_1S(...) decl_1S(__VA_ARGS__)

#define base_2BD(args_1B, args_1S, _1)                  \
        args_1B                                         \
        args_1S                                         \
        opts  (_1, arg__construct__ete_delay__max, ())
#define decl_2BD(I, _1B_1, _1S_1, _1)           \
        base_2BD(prms_1B(I, _1B_1),             \
                 prms_1S(, _1S_1),              \
                 type(_1, typename))
#define args_2BD(I, _1B_1, _1S_1, _1) base_2BD(args_1B(I, _1B_1), args_1S(, _1S_1), _1)
#define prms_2BD(...) decl_2BD(__VA_ARGS__)

        template<prms_2BD(I, _, _, _)>
        struct construct__ete_delay__min;

#define base_2R(args_1R, _1, _2)                                \
        args_1R                                                 \
        opts  (_1, arg__utility_fp_Double__lower_bound, ())     \
          opts(_2, arg__utility_fp_Double__upper_bound, ())
#define decl_2R(I, _1R_1, _1, _2)               \
        base_2R(prms_1R(I, _1R_1),              \
                type(_1, typename),             \
                type(_2, typename))
#define args_2R(I, _1R_1, _1, _2) base_2R(args_1R(I, _1R_1), _1, _2)
#define prms_2R(...) decl_2R(__VA_ARGS__)

#define base_2S(args_1S, args_1I)               \
        args_1S                                 \
        args_1I
#define decl_2S(I, _1S_1, _1I_1)                \
        base_2S(prms_1S(I, _1S_1),              \
                prms_1I(, _1I_1))
#define args_2S(I, _1S_1, _1I_1) base_2S(args_1S(I, _1S_1), args_1I(, _1I_1))
#define prms_2S(...) decl_2S(__VA_ARGS__)

#define base_2N(args_2S, args_1N, args_1O, args_1K, args_1L)    \
        args_2S                                                 \
        args_1N                                                 \
        args_1O                                                 \
        args_1K                                                 \
        args_1L
#define decl_2N(I, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1)     \
        base_2N(prms_2S(I, _1S_1, _1I_1),                               \
                prms_1N(, _1N_1),                                       \
                prms_1O(, _1O_1),                                       \
                prms_1K(, _1K_1, _1K_2),                                \
                prms_1L(, _1L_1))
#define args_2N(I, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1)                                                             \
        base_2N(args_2S(I, _1S_1, _1I_1), args_1N(, _1N_1), args_1O(, _1O_1), args_1K(, _1K_1, _1K_2), args_1L(, _1L_1))
#define prms_2N(...) decl_2N(__VA_ARGS__)

#define base_3NA(args_1T, args_2N, args_2BB, args_1P)   \
        args_1T                                         \
        args_2N                                         \
        args_2BB                                        \
        args_1P
#define decl_3NA(_1X_d1, _1P_d1, I, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1)        \
        base_3NA(prms_1T(I, _1T_1),                                                                                     \
                 prms_2N(, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1),                                            \
                 decl_2BB(_1X_d1, , _1B_1, _1X_1),                                                                      \
                 decl_1P(_1P_d1, , _1P_1))
#define args_3NA(I, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1)                                                \
        base_3NA(args_1T(I, _1T_1), args_2N(, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1), args_2BB(, _1B_1, _1X_1), args_1P(, _1P_1))
#define prms_3NA(...) decl_3NA((), (), __VA_ARGS__)

#define base_2Ab(_d1, _d2, _d3, _d4, I, _1, args_1Ab, _2, _3, _4) I     \
        opts  (_1, arg__child_at_i, _d1)                                \
          args_1Ab                                                      \
          opts(_2, arg__prod_and_actuator_are_adjacent, _d2)            \
          opts(_3, arg__child_at_i_is_sink, _d3)                        \
          opts(_4, arg__child_at_i_and_actuator_are_connected, _d4)
#define decl_2Ab(_d1, _1Ab_d1, _d2, _d3, _d4, I, _1, _1Ab_1, _2, _3, _4)        \
        base_2Ab(_d1, _d2, _d3, _d4, I##I,                                      \
                 type(_1, v1::dict::Key),                                       \
                 decl_1Ab(_1Ab_d1, , _1Ab_1),                                   \
                 type(_2, bool),                                                \
                 type(_3, bool),                                                \
                 type(_4, bool))
#define args_2Ab(I, _1, _1Ab_1, _2, _3, _4) base_2Ab((), (), (), (), I, _1, args_1Ab(, _1Ab_1), _2, _3, _4)
#define prms_2Ab(...) decl_2Ab((), (), (), (), (), __VA_ARGS__)

#define base_3Ab(args_3NA, args_2Ab)            \
        args_3NA                                \
        args_2Ab
#define decl_3Ab(_1X_d1, _1P_d1, _2Ab_d1, _1Ab_d1, _2Ab_d2, _2Ab_d3, _2Ab_d4,                                                                   \
                 I, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)        \
        base_3Ab(decl_3NA(_1X_d1, _1P_d1, I, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1),                      \
                 decl_2Ab(_2Ab_d1, _1Ab_d1, _2Ab_d2, _2Ab_d3, _2Ab_d4, , _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4))
#define args_3Ab(I, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)        \
        base_3Ab(args_3NA(I, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1),                                      \
                 args_2Ab(, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4))
#define prms_3Ab(...) decl_3Ab((), (), (), (), (), (), (), __VA_ARGS__)

#define base_3RA(args_2R, args_3Ab)             \
        args_2R                                 \
        args_3Ab
#define decl_3RA(_1X_d1, _1P_d1, _2Ab_d1, _1Ab_d1, _2Ab_d2, _2Ab_d3, _2Ab_d4,                                                                      \
                 I, _1R_1, _2R_1, _2R_2, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1,                              \
                 _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)                                                                                           \
        base_3RA(prms_2R(I, _1R_1, _2R_1, _2R_2),                                                                                                  \
                 decl_3Ab(_1X_d1, _1P_d1, _2Ab_d1, _1Ab_d1, _2Ab_d2, _2Ab_d3, _2Ab_d4,                                                             \
                          , _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4))
#define args_3RA(I, _1R_1, _2R_1, _2R_2, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1,                              \
                 _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)                                                                                           \
        base_3RA(args_2R(I, _1R_1, _2R_1, _2R_2),                                                                                                  \
                 args_3Ab(, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4))
#define prms_3RA(...) decl_3RA((), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_3RA(_1X_d1 (construct__path<args_1M(I, X)>),
                          _1P_d1 (0),
                          _2Ab_d1(arg__Array__of__Array__of__dict_Key__adjacency_list::elems[arg__prod_node_id][arg__i]),
                          _1Ab_d1(arg__child_at_i == adjacency_list::EOL::value),
                          _2Ab_d2(arg__child_at_i == arg__actuator_node_id),
                          _2Ab_d3(arg__Array__of__bool__is_sink_node::elems[arg__is_done ? arg__prod_node_id : arg__child_at_i]),
                          _2Ab_d4(!arg__child_at_i_is_sink
                                  && arg__producer_and_sink_are_connected<H(I, (arg__is_done || arg__child_at_i_is_sink
                                                                                ? arg__prod_node_id : arg__child_at_i),
                                                                            arg__actuator_node_id,
                                                                            H(construct__error_pos_adaptor__producer_and_sink_are_connected_msgs<
                                                                              args_2K(I, R(arg__pos), R(v1::error::Invalid_pos::value), _, _)>::
                                                                              template producer_node_id_checker_msg),
                                                                            H(construct__error_pos_adaptor__producer_and_sink_are_connected_msgs<
                                                                              args_2K(I, R(arg__pos), R(2), _, _)>::
                                                                              template sink_node_id_checker_msg))>::value),
                          I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__validate_ete_delay__run;

        enum class construct__ete_delayset__member_violation {
          NONE,
          MIN,
          MAX
        };

#define base_3RB(_d1, _d2, args_2S, args_2R, args_2BB, args_2P, _1, _2) \
        args_2S                                                         \
        args_2R                                                         \
        args_2BB                                                        \
        args_2P                                                         \
        opts  (_1, arg__construct__ete_delay__at_i, _d1)                \
          opts(_2, arg__member_violation, _d2)
#define decl_3RB(_1P_d1, _1Ab_d1, _d1, _d2, I, _1S_1, _1I_1, _1R_1, _2R_1, _2R_2, _1B_1, _1X_1, _1P_1, _1Ab_1, _1, _2)  \
        base_3RB(_d1, _d2,                                                                                              \
                 prms_2S(I, _1S_1, _1I_1),                                                                              \
                 prms_2R(, _1R_1, _2R_1, _2R_2),                                                                        \
                 prms_2BB(, _1B_1, _1X_1),                                                                              \
                 decl_2P(_1P_d1, _1Ab_d1, , _1P_1, _1Ab_1),                                                             \
                 type(_1, typename),                                                                                    \
                 type(_2, construct__ete_delayset__member_violation))
#define args_3RB(I, _1S_1, _1I_1, _1R_1, _2R_1, _2R_2, _1B_1, _1X_1, _1P_1, _1Ab_1, _1, _2)                                                     \
        base_3RB((), (), args_2S(I, _1S_1, _1I_1), args_2R(, _1R_1, _2R_1, _2R_2), args_2BB(, _1B_1, _1X_1), args_2P(, _1P_1, _1Ab_1), _1, _2)
#define prms_3RB(...) decl_3RB((), (), (), (), __VA_ARGS__)

        template<decl_3RB(_1P_d1 (0),
                          _1Ab_d1(arg__i + 1 == (construct__hyperperiod_as_release_idx<
                                                 H(args_3BB(I, _, _, R(construct__hyperperiod_as_release_idx__owner::FIRST_NODE)))>::value)),
                          _d1    (construct__ete_delay<args_2U(I, R(arg__i), _, _)>),
                          _d2    (arg__construct__ete_delay__at_i::value == utility::fp::positive_infinity::value
                                  ? construct__ete_delayset__member_violation::NONE
                                  : utility::fp::lt(construct__ete_delay__min<args_2BD(I, _, _, R(arg__construct__ete_delay__at_i))>::value,
                                                    arg__utility_fp_Double__lower_bound::value)
                                  ? construct__ete_delayset__member_violation::MIN
                                  : utility::fp::gt(arg__construct__ete_delay__at_i::value, arg__utility_fp_Double__upper_bound::value)
                                  ? construct__ete_delayset__member_violation::MAX
                                  : construct__ete_delayset__member_violation::NONE),
                          I, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__ete_delayset__check_member;

#define base_3Q(args_2Q, _1)                                    \
        args_2Q                                                 \
        opts  (_1, arg__utility_fp_Double__threshold, ())
#define decl_3Q(I, _1Q_1, _2Q_1, _1)            \
        base_3Q(prms_2Q(I, _1Q_1, _2Q_1),       \
                type(_1, typename))
#define args_3Q(I, _1Q_1, _2Q_1, _1) base_3Q(args_2Q(I, _1Q_1, _2Q_1), _1)
#define prms_3Q(...) decl_3Q(__VA_ARGS__)

#define base_3NB(args_2N, args_1B, args_3Q)     \
        args_2N                                 \
        args_1B                                 \
        args_3Q
#define decl_3NB(I, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1Q_1, _2Q_1, _3Q_1)        \
        base_3NB(prms_2N(I, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1),                           \
                 prms_1B(, _1B_1),                                                                      \
                 prms_3Q(, _1Q_1, _2Q_1, _3Q_1))
#define args_3NB(I, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1Q_1, _2Q_1, _3Q_1)                                \
        base_3NB(args_2N(I, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1), args_1B(, _1B_1), args_3Q(, _1Q_1, _2Q_1, _3Q_1))
#define prms_3NB(...) decl_3NB(__VA_ARGS__)

#define base_4NB(I, _1, args_3NB) I                                                     \
        opts  (_1, arg__tuple_construct__of__dict_Entry_key__sensor_node_ids, ())       \
          args_3NB
#define decl_4NB(I, _1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1Q_1, _2Q_1, _3Q_1)            \
        base_4NB(I##I,                                                                                          \
                 type(_1, typename),                                                                            \
                 prms_3NB(, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1Q_1, _2Q_1, _3Q_1))
#define args_4NB(I, _1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1Q_1, _2Q_1, _3Q_1)                    \
        base_4NB(I, _1, args_3NB(, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1Q_1, _2Q_1, _3Q_1))
#define prms_4NB(...) decl_4NB(__VA_ARGS__)

        template<prms_4NB(I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__two_paths;

#define base_1Ag(_d1, I, _1) I                                          \
        opts  (_1, arg__construct__backtracked_first_path__size, _d1)
#define decl_1Ag(_d1, I, _1)                    \
        base_1Ag(_d1, I##I,                     \
                 type(_1, v1::array::Size))
#define args_1Ag(I, _1) base_1Ag((), I, _1)
#define prms_1Ag(...) decl_1Ag((), __VA_ARGS__)

#define base_2TB(_d1, args_1T, _1, args_1Ag)    \
        args_1T                                 \
        opts  (_1, arg__child_to_take, _d1)     \
          args_1Ag
#define decl_2TB(_d1, _1Ag_d1, I, _1T_1, _1, _1Ag_1)    \
        base_2TB(_d1,                                   \
                 prms_1T(I, _1T_1),                     \
                 type(_1, v1::array::Idx),              \
                 decl_1Ag(_1Ag_d1, , _1Ag_1))
#define args_2TB(I, _1T_1, _1, _1Ag_1) base_2TB((), args_1T(I, _1T_1), _1, args_1Ag(, _1Ag_1))
#define prms_2TB(...) decl_2TB((), (), __VA_ARGS__)

        template<decl_2TB(_d1    (0),
                          _1Ag_d1(0),
                          I, _, _, _)>
        struct construct__branching_node;

#define base_1Ae(I, _1) I                                                               \
        opts  (_1, arg__tuple_construct__of__construct__branching_node__stack, ())
#define decl_1Ae(I, _1)                         \
        base_1Ae(I##I,                          \
                 type(_1, typename))
#define args_1Ae(I, _1) base_1Ae(I, _1)
#define prms_1Ae(...) decl_1Ae(__VA_ARGS__)

#define base_4NA(args_1Ae, args_3Q, args_3NA)   \
        args_1Ae                                \
        args_3Q                                 \
        args_3NA
#define decl_4NA(_1X_d1, _1P_d1, I, _1Ae_1, _1Q_1, _2Q_1, _3Q_1, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1)   \
        base_4NA(prms_1Ae(I, _1Ae_1),                                                                                                           \
                 prms_3Q(, _1Q_1, _2Q_1, _3Q_1),                                                                                                \
                 decl_3NA(_1X_d1, _1P_d1, , _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1))
#define args_4NA(I, _1Ae_1, _1Q_1, _2Q_1, _3Q_1, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1)   \
        base_4NA(args_1Ae(I, _1Ae_1), args_3Q(, _1Q_1, _2Q_1, _3Q_1),                                                           \
                 args_3NA(, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1))
#define prms_4NA(...) decl_4NA((), (), __VA_ARGS__)

#define base_5NAA(args_4NA, args_2Ab)           \
        args_4NA                                \
        args_2Ab
#define decl_5NAA(_1X_d1, _1P_d1, _2Ab_d1, _1Ab_d1, _2Ab_d2, _2Ab_d3, _2Ab_d4,                                                                  \
                  I, _1Ae_1, _1Q_1, _2Q_1, _3Q_1, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1,                  \
                  _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)                                                                                       \
        base_5NAA(decl_4NA(_1X_d1, _1P_d1,                                                                                                      \
                           I, _1Ae_1, _1Q_1, _2Q_1, _3Q_1, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1),        \
                  decl_2Ab(_2Ab_d1, _1Ab_d1, _2Ab_d2, _2Ab_d3, _2Ab_d4, , _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4))
#define args_5NAA(I, _1Ae_1, _1Q_1, _2Q_1, _3Q_1, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1,                  \
                  _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)                                                                                       \
        base_5NAA(args_4NA(I, _1Ae_1, _1Q_1, _2Q_1, _3Q_1, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1),        \
                  args_2Ab(, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4))
#define prms_5NAA(...) decl_5NAA((), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_5NAA(_1X_d1 (construct__path<args_1M(I, X)>),
                           _1P_d1 (0),
                           _2Ab_d1(arg__Array__of__Array__of__dict_Key__adjacency_list::elems[arg__prod_node_id][arg__i]),
                           _1Ab_d1(arg__child_at_i == adjacency_list::EOL::value),
                           _2Ab_d2(arg__child_at_i == arg__actuator_node_id),
                           _2Ab_d3(arg__Array__of__bool__is_sink_node::elems[arg__is_done ? arg__prod_node_id : arg__child_at_i]),
                           _2Ab_d4(!arg__child_at_i_is_sink
                                   && arg__producer_and_sink_are_connected<H(I, (arg__is_done || arg__child_at_i_is_sink
                                                                                 ? arg__prod_node_id : arg__child_at_i),
                                                                             arg__actuator_node_id,
                                                                             H(construct__error_pos_adaptor__producer_and_sink_are_connected_msgs<
                                                                               args_2K(I, R(arg__pos), R(v1::error::Invalid_pos::value), _, _)>::
                                                                               template producer_node_id_checker_msg),
                                                                             H(construct__error_pos_adaptor__producer_and_sink_are_connected_msgs<
                                                                               args_2K(I, R(arg__pos), R(1), _, _)>::
                                                                               template sink_node_id_checker_msg))>::value),
                           I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__two_paths__first_path;

#define base_5NAB(_d1, args_4NA, args_1Ab, _1)                  \
        args_4NA                                                \
        args_1Ab                                                \
        opts  (_1, arg__prod_node_is_branching_node, _d1)
#define decl_5NAB(_d1, I, _1Ae_1, _1Q_1, _2Q_1, _3Q_1, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1, _1Ab_1, _1) \
        base_5NAB(_d1,                                                                                                                          \
                  prms_4NA(I, _1Ae_1, _1Q_1, _2Q_1, _3Q_1, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1),        \
                  prms_1Ab(, _1Ab_1),                                                                                                           \
                  type(_1, bool))
#define args_5NAB(I, _1Ae_1, _1Q_1, _2Q_1, _3Q_1, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1, _1Ab_1, _1)      \
        base_5NAB((), args_4NA(I, _1Ae_1, _1Q_1, _2Q_1, _3Q_1, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1),    \
                  args_1Ab(, _1Ab_1), _1)
#define prms_5NAB(...) decl_5NAB((), __VA_ARGS__)

        template<decl_5NAB(_d1(arg__Array__of__Array__of__dict_Key__adjacency_list::elems[arg__prod_node_id][arg__i + 1]
                               != adjacency_list::EOL::value),
                           I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__two_paths__first_path__push_to_backtrack_stack_as_needed;

#define base_1Aa(I, _1) I                       \
        opts  (_1, arg__construct__path__1, ())
#define decl_1Aa(I, _1)                         \
        base_1Aa(I##I,                          \
                 type(_1, typename))
#define args_1Aa(I, _1) base_1Aa(I, _1)
#define prms_1Aa(...) decl_1Aa(__VA_ARGS__)

#define base_2Ag(_d1, args_1Aa, args_1Ag, _1, args_2P)  \
        args_1Aa                                        \
        args_1Ag                                        \
        opts  (_1, arg__construct__path__result, _d1)   \
          args_2P
#define decl_2Ag(_d1, _1P_d1, _1Ab_d1, I, _1Aa_1, _1Ag_1, _1, _1P_1, _1Ab_1)    \
        base_2Ag(_d1,                                                           \
                 prms_1Aa(I, _1Aa_1),                                           \
                 prms_1Ag(, _1Ag_1),                                            \
                 type(_1, typename),                                            \
                 decl_2P(_1P_d1, _1Ab_d1, , _1P_1, _1Ab_1))
#define args_2Ag(I, _1Aa_1, _1Ag_1, _1, _1P_1, _1Ab_1) base_2Ag((), args_1Aa(I, _1Aa_1), args_1Ag(, _1Ag_1), _1, args_2P(, _1P_1, _1Ab_1))
#define prms_2Ag(...) decl_2Ag((), (), (), __VA_ARGS__)

        template<decl_2Ag(_d1    (construct__path<args_1M(I, X)>),
                          _1P_d1 (0),
                          _1Ab_d1(arg__i == arg__construct__backtracked_first_path__size),
                          I, _, _, _, _, _)>
        struct construct__backtracked_first_path;

#define base_4Ab(args_1Aa, args_3Q, args_3Ab)   \
        args_1Aa                                \
        args_3Q                                 \
        args_3Ab
#define decl_4Ab(_1X_d1, _1P_d1, _2Ab_d1, _1Ab_d1, _2Ab_d2, _2Ab_d3, _2Ab_d4,                                                                      \
                 I, _1Aa_1, _1Q_1, _2Q_1, _3Q_1, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1,                      \
                 _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)                                                                                           \
        base_4Ab(prms_1Aa(I, _1Aa_1),                                                                                                              \
                 prms_3Q(, _1Q_1, _2Q_1, _3Q_1),                                                                                                   \
                 decl_3Ab(_1X_d1, _1P_d1, _2Ab_d1, _1Ab_d1, _2Ab_d2, _2Ab_d3, _2Ab_d4,                                                             \
                          , _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4))
#define args_4Ab(I, _1Aa_1, _1Q_1, _2Q_1, _3Q_1, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1,                      \
                 _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)                                                                                           \
        base_4Ab(args_1Aa(I, _1Aa_1), args_3Q(, _1Q_1, _2Q_1, _3Q_1),                                                                              \
                 args_3Ab(, _1T_1, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1X_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4))
#define prms_4Ab(...) decl_4Ab((), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_4Ab(_1X_d1 (construct__path<args_1M(I, X)>),
                          _1P_d1 (0),
                          _2Ab_d1(arg__Array__of__Array__of__dict_Key__adjacency_list::elems[arg__prod_node_id][arg__i]),
                          _1Ab_d1(arg__child_at_i == adjacency_list::EOL::value),
                          _2Ab_d2(arg__child_at_i == arg__actuator_node_id),
                          _2Ab_d3(arg__Array__of__bool__is_sink_node::elems[arg__is_done ? arg__prod_node_id : arg__child_at_i]),
                          _2Ab_d4(!arg__child_at_i_is_sink
                                  && arg__producer_and_sink_are_connected<H(I, (arg__is_done || arg__child_at_i_is_sink
                                                                                ? arg__prod_node_id : arg__child_at_i),
                                                                            arg__actuator_node_id,
                                                                            H(construct__error_pos_adaptor__producer_and_sink_are_connected_msgs<
                                                                              args_2K(I, R(arg__pos), R(v1::error::Invalid_pos::value), _, _)>::
                                                                              template producer_node_id_checker_msg),
                                                                            H(construct__error_pos_adaptor__producer_and_sink_are_connected_msgs<
                                                                              args_2K(I, R(arg__pos), R(1), _, _)>::
                                                                              template sink_node_id_checker_msg))>::value),
                          I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__two_paths__second_path;

#define base_2Ae(args_3NB, args_1Ae, args_1Aa)  \
        args_3NB                                \
        args_1Ae                                \
        args_1Aa
#define decl_2Ae(I, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1Q_1, _2Q_1, _3Q_1, _1Ae_1, _1Aa_1)        \
        base_2Ae(prms_3NB(I, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1Q_1, _2Q_1, _3Q_1),              \
                 prms_1Ae(, _1Ae_1),                                                                                    \
                 prms_1Aa(, _1Aa_1))
#define args_2Ae(I, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1Q_1, _2Q_1, _3Q_1, _1Ae_1, _1Aa_1)                                   \
        base_2Ae(args_3NB(I, _1S_1, _1I_1, _1N_1, _1O_1, _1K_1, _1K_2, _1L_1, _1B_1, _1Q_1, _2Q_1, _3Q_1), args_1Ae(, _1Ae_1), args_1Aa(, _1Aa_1))
#define prms_2Ae(...) decl_2Ae(__VA_ARGS__)

        template<prms_2Ae(I, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__two_paths__backtrack;

#define base_2Aa(_d1, args_1Aa, _1)                     \
        args_1Aa                                        \
        opts  (_1, arg__construct__path__2, _d1)
#define decl_2Aa(_d1, I, _1Aa_1, _1)            \
        base_2Aa(_d1,                           \
                 prms_1Aa(I, _1Aa_1),           \
                 type(_1, typename))
#define args_2Aa(I, _1Aa_1, _1) base_2Aa((), args_1Aa(I, _1Aa_1), _1)
#define prms_2Aa(...) decl_2Aa((), __VA_ARGS__)

#define base_1Ac(_d1, I, _1) I                  \
        opts  (_1, arg__confluent_node_id, _d1)
#define decl_1Ac(_d1, I, _1)                    \
        base_1Ac(_d1, I##I,                     \
                 type(_1, v1::dict::Key))
#define args_1Ac(I, _1) base_1Ac((), I, _1)
#define prms_1Ac(...) decl_1Ac((), __VA_ARGS__)

#define base_4Q(args_1I, args_1B, args_3Q, args_2Aa, args_1Ac)  \
        args_1I                                                 \
        args_1B                                                 \
        args_3Q                                                 \
        args_2Aa                                                \
        args_1Ac
#define decl_4Q(_2Aa_d1, _1Ac_d1, I, _1I_1, _1B_1, _1Q_1, _2Q_1, _3Q_1, _1Aa_1, _2Aa_1, _1Ac_1) \
        base_4Q(prms_1I(I, _1I_1),                                                              \
                prms_1B(, _1B_1),                                                               \
                prms_3Q(, _1Q_1, _2Q_1, _3Q_1),                                                 \
                decl_2Aa(_2Aa_d1, , _1Aa_1, _2Aa_1),                                            \
                decl_1Ac(_1Ac_d1, , _1Ac_1))
#define args_4Q(I, _1I_1, _1B_1, _1Q_1, _2Q_1, _3Q_1, _1Aa_1, _2Aa_1, _1Ac_1)                                                           \
        base_4Q(args_1I(I, _1I_1), args_1B(, _1B_1), args_3Q(, _1Q_1, _2Q_1, _3Q_1), args_2Aa(, _1Aa_1, _2Aa_1), args_1Ac(, _1Ac_1))
#define prms_4Q(...) decl_4Q((), (), __VA_ARGS__)

#define base_5QA(_d2, _d3, args_1X, _1, args_4Q, _2, _3)        \
        args_1X                                                 \
        opts  (_1, arg__Array__of__array_Size__path_1_pos, ())  \
          args_4Q                                               \
          opts(_2, arg__path_2_previous_node_is_in_path_1, _d2) \
          opts(_3, arg__path_2_current_node_is_in_path_1, _d3)
#define decl_5QA(_2Aa_d1, _1Ac_d1, _d2, _d3, I, _1X_1, _1, _1I_1, _1B_1, _1Q_1, _2Q_1, _3Q_1, _1Aa_1, _2Aa_1, _1Ac_1, _2, _3)   \
        base_5QA(_d2, _d3,                                                                                                      \
                 prms_1X(I, _1X_1),                                                                                             \
                 type(_1, typename),                                                                                            \
                 decl_4Q(_2Aa_d1, _1Ac_d1, , _1I_1, _1B_1, _1Q_1, _2Q_1, _3Q_1, _1Aa_1, _2Aa_1, _1Ac_1),                        \
                 type(_2, bool),                                                                                                \
                 type(_3, bool))
#define args_5QA(I, _1X_1, _1, _1I_1, _1B_1, _1Q_1, _2Q_1, _3Q_1, _1Aa_1, _2Aa_1, _1Ac_1, _2, _3)                               \
        base_5QA((), (), args_1X(I, _1X_1), _1, args_4Q(, _1I_1, _1B_1, _1Q_1, _2Q_1, _3Q_1, _1Aa_1, _2Aa_1, _1Ac_1), _2, _3)
#define prms_5QA(...) decl_5QA((), (), (), (), __VA_ARGS__)

        template<decl_5QA(_2Aa_d1(construct__path<args_1M(I, X)>),
                          _1Ac_d1(v1::dict::Invalid_key::value),
                          _d2    (true),
                          _d3    (true),
                          I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__two_paths__iterate_confluent_nodeset;

#define base_1Ah(I, _1, _2, _3) I                       \
        opts  (_1, arg__idx, ())                        \
          opts(_2, arg__dict_Entry_key__node_id, ())    \
          opts(_3, arg__data, ())
#define decl_1Ah(I, _1, _2, _3)                 \
        base_1Ah(I##I,                          \
                 type(_1, v1::array::Idx),      \
                 type(_2, typename),            \
                 type(_3, typename))
#define args_1Ah(I, _1, _2, _3) base_1Ah(I, _1, _2, _3)
#define prms_1Ah(...) decl_1Ah(__VA_ARGS__)

        template<prms_1Ah(I, _, _, _)>
        struct construct__node_id_to_path_pos;

#define base_1W(I, _1) I                                \
        opts  (_1, arg__confluent_node_release_idx, ())
#define decl_1W(I, _1)                          \
        base_1W(I##I,                           \
                type(_1, v1::array::Idx))
#define args_1W(I, _1) base_1W(I, _1)
#define prms_1W(...) decl_1W(__VA_ARGS__)

#define base_2W(args_1W, args_1Q)               \
        args_1W                                 \
        args_1Q
#define decl_2W(I, _1W_1, _1Q_1)                \
        base_2W(prms_1W(I, _1W_1),              \
                prms_1Q(, _1Q_1))
#define args_2W(I, _1W_1, _1Q_1) base_2W(args_1W(I, _1W_1), args_1Q(, _1Q_1))
#define prms_2W(...) decl_2W(__VA_ARGS__)

#define base_3WA(_d1, args_2W, args_2BB, _1)                    \
        args_2W                                                 \
        args_2BB                                                \
        opts  (_1, arg__construct__path__remainder, _d1)
#define decl_3WA(_d1, I, _1W_1, _1Q_1, _1B_1, _1X_1, _1)        \
        base_3WA(_d1,                                           \
                 prms_2W(I, _1W_1, _1Q_1),                      \
                 prms_2BB(, _1B_1, _1X_1),                      \
                 type(_1, typename))
#define args_3WA(I, _1W_1, _1Q_1, _1B_1, _1X_1, _1) base_3WA((), args_2W(I, _1W_1, _1Q_1), args_2BB(, _1B_1, _1X_1), _1)
#define prms_3WA(...) decl_3WA((), __VA_ARGS__)

        template<decl_3WA(_d1(arg__construct__path),
                          I, _, _, _, _, _)>
        struct construct__correlation__sensing_release_idx;

#define base_3WB(args_2W, args_2Aa, args_1B)    \
        args_2W                                 \
        args_2Aa                                \
        args_1B
#define decl_3WB(I, _1W_1, _1Q_1, _1Aa_1, _2Aa_1, _1B_1)        \
        base_3WB(prms_2W(I, _1W_1, _1Q_1),                      \
                 prms_2Aa(, _1Aa_1, _2Aa_1),                    \
                 prms_1B(, _1B_1))
#define args_3WB(I, _1W_1, _1Q_1, _1Aa_1, _2Aa_1, _1B_1) base_3WB(args_2W(I, _1W_1, _1Q_1), args_2Aa(, _1Aa_1, _2Aa_1), args_1B(, _1B_1))
#define prms_3WB(...) decl_3WB(__VA_ARGS__)

        template<prms_3WB(I, _, _, _, _, _)>
        struct construct__correlation__at_release_point;

#define base_2Ac(_d1, args_1Ac, args_2BB, args_1P, _1, args_1Ab)        \
        args_1Ac                                                        \
        args_2BB                                                        \
        args_1P                                                         \
        opts(_1, arg__construct__ete_delay__at_i, _d1)                  \
          args_1Ab
#define decl_2Ac(_1P_d1, _d1, _1Ab_d1, I, _1Ac_1, _1B_1, _1X_1, _1P_1, _1, _1Ab_1)      \
        base_2Ac(_d1,                                                                   \
                 prms_1Ac(I, _1Ac_1),                                                   \
                 prms_2BB(, _1B_1, _1X_1),                                              \
                 decl_1P(_1P_d1, , _1P_1),                                              \
                 type(_1, typename),                                                    \
                 decl_1Ab(_1Ab_d1, , _1Ab_1))
#define args_2Ac(I, _1Ac_1, _1B_1, _1X_1, _1P_1, _1, _1Ab_1)                                                    \
        base_2Ac((), args_1Ac(I, _1Ac_1), args_2BB(, _1B_1, _1X_1), args_1P(, _1P_1), _1, args_1Ab(, _1Ab_1))
#define prms_2Ac(...) decl_2Ac((), (), (), __VA_ARGS__)

        template<decl_2Ac(_1P_d1 (0),
                          _d1    (construct__ete_delay<args_2U(I, R(arg__i), _, _)>),
                          _1Ab_d1(arg__construct__ete_delay__at_i::value != utility::fp::positive_infinity::value),
                          I, _, _, _, _, _, _)>
        struct construct__sensing_release_domain__min;

#define base_5QB(_d1, _d2, _d3, _d4, args_4Q, _1, _2, args_2P, _3, _4)  \
        args_4Q                                                         \
        opts  (_1, arg__i_begin, _d1)                                   \
          opts(_2, arg__i_end, _d2)                                     \
          args_2P                                                       \
          opts(_3, arg__construct__correlation__at_i, _d3)              \
          opts(_4, arg__threshold_is_respected, _d4)
#define decl_5QB(_d1, _d2, _1P_d1, _1Ab_d1, _d3, _d4, I, _1I_1, _1B_1, _1Q_1, _2Q_1, _3Q_1, _1Aa_1, _2Aa_1, _1Ac_1, _1, _2, _1P_1, _1Ab_1, _3, _4) \
        base_5QB(_d1, _d2, _d3, _d4,                                                                                                               \
                 prms_4Q(I, _1I_1, _1B_1, _1Q_1, _2Q_1, _3Q_1, _1Aa_1, _2Aa_1, _1Ac_1),                                                            \
                 type(_1, v1::array::Idx),                                                                                                         \
                 type(_2, v1::array::Idx),                                                                                                         \
                 decl_2P(_1P_d1, _1Ab_d1, , _1P_1, _1Ab_1),                                                                                        \
                 type(_3, typename),                                                                                                               \
                 type(_4, bool))
#define args_5QB(I, _1I_1, _1B_1, _1Q_1, _2Q_1, _3Q_1, _1Aa_1, _2Aa_1, _1Ac_1, _1, _2, _1P_1, _1Ab_1, _3, _4)                                      \
        base_5QB((), (), (), (), args_4Q(I, _1I_1, _1B_1, _1Q_1, _2Q_1, _3Q_1, _1Aa_1, _2Aa_1, _1Ac_1), _1, _2, args_2P(, _1P_1, _1Ab_1), _3, _4)
#define prms_5QB(...) decl_5QB((), (), (), (), (), (), __VA_ARGS__)

        template<decl_5QB(_d1    (utility::max<H(I, construct__sensing_release_domain__min<args_2Ac(I, _, _, R(arg__construct__path__1), X, X, X)>,
                                                 construct__sensing_release_domain__min<args_2Ac(I, _, _, R(arg__construct__path__2), X, X, X)>
                                                 )>::value::value),
                          _d2    (arg__i_begin
                                  + (construct__hyperperiod_as_release_idx<
                                     H(args_3BB(I, _,
                                                R(typename construct__two_paths_concatenated<args_1V(I, R(arg__construct__path__1),
                                                                                                     R(arg__construct__path__2))>::value),
                                                R(construct__hyperperiod_as_release_idx__owner::LAST_NODE)))>::value)),
                          _1P_d1 (arg__i_begin),
                          _1Ab_d1(arg__i + 1 == arg__i_end),
                          _d3    (construct__correlation__at_release_point<args_3WB(I, R(arg__i), _, _, _, _)>),
                          _d4    (!utility::fp::gt(arg__construct__correlation__at_i::value,
                                                   arg__utility_fp_Double__threshold::value)),
                          I, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__correlation__check_threshold;

#define base_3IB(args_2I, args_3BA, args_2GA, args_3A)  \
        args_2I                                         \
        args_3BA                                        \
        args_2GA                                        \
        args_3A
#define decl_3IB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1G_1, _2GA_1,                                                                         \
                 _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                         \
                 _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,                \
                 _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                                    \
                 _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                              \
                 _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                                 \
                 _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                        \
                 _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                         \
                 _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                                \
        base_3IB(prms_2I(I, _1I_1, _2I_1),                                                                                                      \
                 prms_3BA(, _1Ak_1, _2BA_1, _1B_1),                                                                                             \
                 prms_2GA(, _1G_1, _2GA_1),                                                                                                     \
                 prms_3A(, _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,               \
                         _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,        \
                         _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                            \
                         _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                      \
                         _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                         \
                         _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                \
                         _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                 \
                         _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define args_3IB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1G_1, _2GA_1,                                                                         \
                 _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                         \
                 _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,                \
                 _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                                    \
                 _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                              \
                 _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                                 \
                 _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                        \
                 _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                         \
                 _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                                \
        base_3IB(args_2I(I, _1I_1, _2I_1), args_3BA(, _1Ak_1, _2BA_1, _1B_1), args_2GA(, _1G_1, _2GA_1),                                        \
                 args_3A(, _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,               \
                         _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,        \
                         _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                            \
                         _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                      \
                         _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                         \
                         _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                \
                         _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                 \
                         _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define prms_3IB(...) decl_3IB(__VA_ARGS__)

        template<prms_3IB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__parse_feeders;

#define base_1J(_d1, I, _1) I                           \
        opts  (_1, arg__node_follower_checker_msg, _d1)
#define decl_1J(_d1, I, _1)                                                                                                                     \
        base_1J(_d1, I##I,                                                                                                                      \
                type(_1, template<bool arg__is_followed_by_sequence_of_Feeder_, v1::error::Pos arg__pos_, typename arg__follower_> class))
#define args_1J(I, _1) base_1J((), I, _1)
#define prms_1J(...) decl_1J((), __VA_ARGS__)

#define base_2J(args_3IB, args_1J)              \
        args_3IB                                \
        args_1J
#define decl_2J(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1G_1, _2GA_1,                                                                          \
                _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                          \
                _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,                 \
                _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                                     \
                _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                               \
                _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                                  \
                _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                         \
                _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                          \
                _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24, _1J_1)                                          \
        base_2J(prms_3IB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1G_1, _2GA_1,                                                                 \
                         _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                 \
                         _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,        \
                         _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                            \
                         _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                      \
                         _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                         \
                         _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                \
                         _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                 \
                         _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24),                                       \
                prms_1J(, _1J_1))
#define args_2J(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1G_1, _2GA_1,                                                                          \
                _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                          \
                _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,                 \
                _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                                     \
                _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                               \
                _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                                  \
                _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                         \
                _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                          \
                _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24, _1J_1)                                          \
        base_2J(args_3IB(I, _1I_1, _2I_1, _1Ak_1, _2BA_1, _1B_1, _1G_1, _2GA_1,                                                                 \
                         _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                 \
                         _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,        \
                         _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                            \
                         _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                      \
                         _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                         \
                         _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                \
                         _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                 \
                         _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24), args_1J(, _1J_1))
#define prms_2J(...) decl_2J(__VA_ARGS__)

        template<prms_2J(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__nodes_parsed;

#define base_4A(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, I, _1, _2, _3, _4, _5, _6, _7, _8, _9, args_1J, args_3A) I \
        opts  (_1, arg__node_computation_checker_msg, _d1)                                                              \
          opts(_2, arg__node_computation_fn_ptr_checker_msg, _d2)                                                       \
          opts(_3, arg__node_computation_wcet_checker_msg, _d3)                                                         \
          opts(_4, arg__node_computation_wcet_den_checker_msg, _d4)                                                     \
        opts  (_5, arg__node_computation_wcet_validator_msg, _d5)                                                       \
        opts  (_6, arg__node_period_checker_msg, _d6)                                                                   \
        opts  (_7, arg__node_period_den_checker_msg, _d7)                                                               \
        opts  (_8, arg__node_period_validator_msg, _d8)                                                                 \
        opts  (_9, arg__distinct_node_checker_msg, _d9)                                                                 \
        args_1J                                                                                                         \
        args_3A
#define decl_4A(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _1J_d1,                                                                            \
                _3A_d1, _3A_d2, _3A_d3, _3A_d4, _3A_d5, _3A_d6, _3A_d7, _3A_d8, _3A_d9, _3A_d10, _3A_d11, _3A_d12, _3A_d13, _3A_d14,            \
                _3A_d15, _3A_d16, _3A_d17, _3A_d18, _3A_d19, _3A_d20, _3A_d21, _3A_d22, _3A_d23, _3A_d24, _3A_d25, _3A_d26, _3A_d27, _3A_d28,   \
                _1A_d1, _1A_d2, _1Q_d1, _2Q_d1, _1A_d3, _1A_d4, _2A_d1, _1R_d1, _1K_d1, _1K_d2,                                                 \
                _1Y_d1, _1Y_d2, _1Y_d3, _1Y_d4, _1Y_d5, _1Y_d6, _1Y_d7, _1Y_d8, _1Y_d9, _1Y_d10, _1Y_d11, _1Y_d12, _1Y_d13, _1Y_d14,            \
                _1Y_d15, _1Y_d16, _1Y_d17, _1Y_d18, _1Y_d19, _1Y_d20, _1Y_d21, _1Y_d22, _1Y_d23, _1Y_d24, _1Y_d25, _1Y_d26, _1Y_d27,            \
                _1Z_d1, _1Z_d2, _1Z_d3, _1Z_d4, _1Z_d5, _1Z_d6, _1Z_d7, _1Z_d8, _1Z_d9, _1Z_d10, _1Z_d11, _1Z_d12, _1Z_d13, _1Z_d14,            \
                _1Z_d15, _1Z_d16, _1Z_d17, _1Z_d18, _1Z_d19, _1Z_d20, _1Z_d21, _1Z_d22, _1Z_d23, _1Z_d24,                                       \
                I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _1J_1,                                                                                   \
                _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                          \
                _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,                 \
                _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                                     \
                _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                               \
                _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                                  \
                _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                         \
                _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                          \
                _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                                 \
        base_4A(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, I##I,                                                                              \
                type(_1, template<bool arg__is_comp_Unit_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),                   \
                type(_2, template<bool arg__is_function_pointer_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),            \
                type(_3, template<bool arg__is_std_ratio_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),                   \
                type(_4, template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),        \
                type(_5, template<bool arg__is_positive_, v1::error::Pos arg__pos_, typename arg__wcet_> class),                                \
                type(_6, template<bool arg__is_std_ratio_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),                   \
                type(_7, template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),        \
                type(_8,  template<H(bool arg__is_greater_than_or_equal_, v1::error::Pos arg__pos_, typename arg__period_, typename arg__wcet_> \
                                     class)),                                                                                                   \
                type(_9, template<bool arg__are_distinct_, v1::error::Pos arg__node_1_pos_, v1::error::Pos arg__node_2_pos_> class),            \
                decl_1J(_1J_d1, , _1J_1),                                                                                                       \
                decl_3A(_3A_d1, _3A_d2, _3A_d3, _3A_d4, _3A_d5, _3A_d6, _3A_d7, _3A_d8, _3A_d9, _3A_d10, _3A_d11, _3A_d12, _3A_d13, _3A_d14,    \
                        _3A_d15, _3A_d16, _3A_d17, _3A_d18, _3A_d19, _3A_d20, _3A_d21, _3A_d22, _3A_d23, _3A_d24, _3A_d25, _3A_d26, _3A_d27,    \
                        _3A_d28, _1A_d1, _1A_d2, _1Q_d1, _2Q_d1, _1A_d3, _1A_d4, _2A_d1, _1R_d1, _1K_d1, _1K_d2,                                \
                        _1Y_d1, _1Y_d2, _1Y_d3, _1Y_d4, _1Y_d5, _1Y_d6, _1Y_d7, _1Y_d8, _1Y_d9, _1Y_d10, _1Y_d11, _1Y_d12, _1Y_d13, _1Y_d14,    \
                        _1Y_d15, _1Y_d16, _1Y_d17, _1Y_d18, _1Y_d19, _1Y_d20, _1Y_d21, _1Y_d22, _1Y_d23, _1Y_d24, _1Y_d25, _1Y_d26, _1Y_d27,    \
                        _1Z_d1, _1Z_d2, _1Z_d3, _1Z_d4, _1Z_d5, _1Z_d6, _1Z_d7, _1Z_d8, _1Z_d9, _1Z_d10, _1Z_d11, _1Z_d12, _1Z_d13, _1Z_d14,    \
                        _1Z_d15, _1Z_d16, _1Z_d17, _1Z_d18, _1Z_d19, _1Z_d20, _1Z_d21, _1Z_d22, _1Z_d23, _1Z_d24,                               \
                        , _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                \
                        _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,         \
                        _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                             \
                        _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                       \
                        _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                          \
                        _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                 \
                        _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                  \
                        _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define args_4A(I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _1J_1,                                                                           \
                _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                  \
                _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,         \
                _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                             \
                _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                       \
                _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                          \
                _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                 \
                _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                  \
                _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                         \
        base_4A((), (), (), (), (), (), (), (), (), I, _1, _2, _3, _4, _5, _6, _7, _8, _9, args_1J(, _1J_1),                            \
                args_3A(, _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,        \
                        _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28, \
                        _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                     \
                        _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,               \
                        _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                  \
                        _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                         \
                        _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,          \
                        _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define prms_4A(...) decl_4A((), (), (), (), (), (), (), (), (), (),                    \
                             (), (), (), (), (), (), (), (), (), (), (), (), (), (),    \
                             (), (), (), (), (), (), (), (), (), (), (), (), (), (),    \
                             (), (), (), (), (), (), (), (), (), (),                    \
                             (), (), (), (), (), (), (), (), (), (), (), (), (), (),    \
                             (), (), (), (), (), (), (), (), (), (), (), (), (),        \
                             (), (), (), (), (), (), (), (), (), (), (), (), (), (),    \
                             (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

#define base_5A(args_2I, args_4A)               \
        args_2I                                 \
        args_4A
#define decl_5A(I, _1I_1, _2I_1, _4A_1, _4A_2, _4A_3, _4A_4, _4A_5, _4A_6, _4A_7, _4A_8, _4A_9, _1J_1,                                  \
                _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                  \
                _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,         \
                _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                             \
                _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                       \
                _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                          \
                _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                 \
                _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                  \
                _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                         \
        base_5A(prms_2I(I, _1I_1, _2I_1),                                                                                               \
                prms_4A(, _4A_1, _4A_2, _4A_3, _4A_4, _4A_5, _4A_6, _4A_7, _4A_8, _4A_9, _1J_1,                                         \
                        _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,          \
                        _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28, \
                        _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                     \
                        _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,               \
                        _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                  \
                        _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                         \
                        _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,          \
                        _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define args_5A(I, _1I_1, _2I_1, _4A_1, _4A_2, _4A_3, _4A_4, _4A_5, _4A_6, _4A_7, _4A_8, _4A_9, _1J_1,                                  \
                _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                  \
                _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,         \
                _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                             \
                _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                       \
                _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                          \
                _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                 \
                _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                  \
                _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                         \
        base_5A(args_2I(I, _1I_1, _2I_1),                                                                                               \
                args_4A(, _4A_1, _4A_2, _4A_3, _4A_4, _4A_5, _4A_6, _4A_7, _4A_8, _4A_9, _1J_1,                                         \
                        _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,          \
                        _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28, \
                        _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                     \
                        _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,               \
                        _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                  \
                        _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                         \
                        _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,          \
                        _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define prms_5A(...) decl_5A(__VA_ARGS__)

        template<prms_5A(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end;

#define base_1C(I, _1) I                        \
        opts  (_1, arg__HW__hw_desc, ())
#define decl_1C(I, _1)                          \
        base_1C(I##I,                           \
                type(_1, typename))
#define args_1C(I, _1) base_1C(I, _1)
#define prms_1C(...) decl_1C(__VA_ARGS__)

#define base_2AiB(_d1, I, _1, args_1Ai) I                       \
        opts  (_1, arg__backend_availability_checker_msg, _d1)  \
          args_1Ai
#define decl_2AiB(_d1, _1Ai_d1, I, _1, _1Ai_1)                                                          \
        base_2AiB(_d1, I##I,                                                                            \
                  type(_1, template<bool arg__is_available_, typename arg__HW__hw_desc_> class),        \
                  decl_1Ai(_1Ai_d1, , _1Ai_1))
#define args_2AiB(I, _1, _1Ai_1) base_2AiB((), I, _1, args_1Ai(, _1Ai_1))
#define prms_2AiB(...) decl_2AiB((), (), __VA_ARGS__)

#define base_2C(args_1C, args_2AiB)             \
        args_1C                                 \
        args_2AiB
#define decl_2C(I, _1C_1, _2AiB_1, _1Ai_1)      \
        base_2C(prms_1C(I, _1C_1),              \
                prms_2AiB(, _2AiB_1, _1Ai_1))
#define args_2C(I, _1C_1, _2AiB_1, _1Ai_1) base_2C(args_1C(I, _1C_1), args_2AiB(, _2AiB_1, _1Ai_1))
#define prms_2C(...) decl_2C(__VA_ARGS__)

#define base_3IC(args_2C, args_2I, args_4A)     \
        args_2C                                 \
        args_2I                                 \
        args_4A
#define decl_3IC(I, _1C_1, _2AiB_1, _1Ai_1, _1I_1, _2I_1, _4A_1, _4A_2, _4A_3, _4A_4, _4A_5, _4A_6, _4A_7, _4A_8, _4A_9, _1J_1,                 \
                 _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                         \
                 _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,                \
                 _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                                    \
                 _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                              \
                 _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                                 \
                 _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                        \
                 _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                         \
                 _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                                \
        base_3IC(prms_2C(I, _1C_1, _2AiB_1, _1Ai_1),                                                                                            \
                 prms_2I(, _1I_1, _2I_1),                                                                                                       \
                 prms_4A(, _4A_1, _4A_2, _4A_3, _4A_4, _4A_5, _4A_6, _4A_7, _4A_8, _4A_9, _1J_1,                                                \
                         _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                 \
                         _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,        \
                         _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                            \
                         _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                      \
                         _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                         \
                         _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                \
                         _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                 \
                         _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define args_3IC(I, _1C_1, _2AiB_1, _1Ai_1, _1I_1, _2I_1, _4A_1, _4A_2, _4A_3, _4A_4, _4A_5, _4A_6, _4A_7, _4A_8, _4A_9, _1J_1,                 \
                 _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                         \
                 _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,                \
                 _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                                    \
                 _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                              \
                 _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                                 \
                 _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                        \
                 _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                         \
                 _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24)                                                \
        base_3IC(args_2C(I, _1C_1, _2AiB_1, _1Ai_1), args_2I(, _1I_1, _2I_1),                                                                   \
                 args_4A(, _4A_1, _4A_2, _4A_3, _4A_4, _4A_5, _4A_6, _4A_7, _4A_8, _4A_9, _1J_1,                                                \
                         _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                 \
                         _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,        \
                         _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                            \
                         _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                      \
                         _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                         \
                         _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                \
                         _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                 \
                         _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24))
#define prms_3IC(...) decl_3IC(__VA_ARGS__)

        template<prms_3IC(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__args_validated;

#define base_1Aj(_d1, _d2, I, _1, _2) I                                                                                                         \
        opts  (_1, arg__tuple_construct__of__Channel__channel_list, _d1)                                                                        \
          opts(_2, arg__tuple_construct__of__pair_construct__of__tuple_dict_Entry_array_idx__and__Array_idx_list__channel_idx_list, _d2)
#define decl_1Aj(_d1, _d2, I, _1, _2)           \
        base_1Aj(_d1, _d2, I##I,                \
                 type(_1, typename),            \
                 type(_2, typename))
#define args_1Aj(I, _1, _2) base_1Aj((), (), I, _1, _2)
#define prms_1Aj(...) decl_1Aj((), (), __VA_ARGS__)

#define base_1Am(_d1, I, _1) I                          \
        opts  (_1, arg__actuator_node_count, _d1)
#define decl_1Am(_d1, I, _1)                    \
        base_1Am(_d1, I##I,                     \
                 type(_1, v1::array::Size))
#define args_1Am(I, _1) base_1Am((), I, _1)
#define prms_1Am(...) decl_1Am((), __VA_ARGS__)

#define base_2Ak(_d1, args_1Ak, _1)                     \
        args_1Ak                                        \
        opts(_1, args__Channel__channel_list, _d1)
#define decl_2Ak(I, _1Ak_1, _1)                 \
        base_2Ak((),                            \
                 prms_1Ak(I, _1Ak_1),           \
                 type(_1, typename...))
#define args_2Ak(I, _1Ak_1, _1) base_2Ak((...), args_1Ak(I, _1Ak_1), _1)
#define prms_2Ak(...) decl_2Ak(__VA_ARGS__)

        namespace {
          template<prms_2Ak(I, _, _)>
          struct construct__back_end__gedf__shared_data;
        }

#define base_3C(args_2C, args_4BA, args_1Aj)    \
        args_2C                                 \
        args_4BA                                \
        args_1Aj
#define decl_3C(_1Ak_d1, _2BA_d1, _1B_d1, _1N_d1, _1Aj_d1, _1Aj_d2, I, _1C_1, _2AiB_1, _1Ai_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1Aj_1, _1Aj_2)    \
        base_3C(prms_2C(I, _1C_1, _2AiB_1, _1Ai_1),                                                                                             \
                decl_4BA(_1Ak_d1, _2BA_d1, _1B_d1, _1N_d1, , _1Ak_1, _2BA_1, _1B_1, _1N_1),                                                     \
                decl_1Aj(_1Aj_d1, _1Aj_d2, , _1Aj_1, _1Aj_2))
#define args_3C(I, _1C_1, _2AiB_1, _1Ai_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1Aj_1, _1Aj_2)                                        \
        base_3C(args_2C(I, _1C_1, _2AiB_1, _1Ai_1), args_4BA(, _1Ak_1, _2BA_1, _1B_1, _1N_1), args_1Aj(, _1Aj_1, _1Aj_2))
#define prms_3C(...) decl_3C((), (), (), (), (), (), __VA_ARGS__)

#define base_4CB(args_3C, args_1Am)             \
        args_3C                                 \
        args_1Am
#define decl_4CB(_1Ak_d1, _2BA_d1, _1B_d1, _1N_d1, _1Aj_d1, _1Aj_d2, _1Am_d1,                           \
                 I, _1C_1, _2AiB_1, _1Ai_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1Aj_1, _1Aj_2, _1Am_1)       \
        base_4CB(decl_3C(_1Ak_d1, _2BA_d1, _1B_d1, _1N_d1, _1Aj_d1, _1Aj_d2,                            \
                         I, _1C_1, _2AiB_1, _1Ai_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1Aj_1, _1Aj_2),      \
                 decl_1Am(_1Am_d1, , _1Am_1))
#define args_4CB(I, _1C_1, _2AiB_1, _1Ai_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1Aj_1, _1Aj_2, _1Am_1)                       \
        base_4CB(args_3C(I, _1C_1, _2AiB_1, _1Ai_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1Aj_1, _1Aj_2), args_1Am(, _1Am_1))
#define prms_4CB(...) decl_4CB((), (), (), (), (), (), (), __VA_ARGS__)

        template<prms_4CB(I, _, _, _, _, _, _, _, _, _, _)>
        struct construct__back_end__generate;

#define base_4CA(I, _1, args_4CB) I                     \
        opts  (_1, arg__construct__front_end, ())       \
          args_4CB
#define decl_4CA(_1Ak_d1, _2BA_d1, _1B_d1, _1N_d1, _1Aj_d1, _1Aj_d2, _1Am_d1,                                   \
                 I, _1, _1C_1, _2AiB_1, _1Ai_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1Aj_1, _1Aj_2, _1Am_1)           \
        base_4CA(I##I,                                                                                          \
                 type(_1, typename),                                                                            \
                 decl_4CB(_1Ak_d1, _2BA_d1, _1B_d1, _1N_d1, _1Aj_d1, _1Aj_d2, _1Am_d1,                          \
                          , _1C_1, _2AiB_1, _1Ai_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1Aj_1, _1Aj_2, _1Am_1))
#define args_4CA(I, _1, _1C_1, _2AiB_1, _1Ai_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1Aj_1, _1Aj_2, _1Am_1)                   \
        base_4CA(I, _1, args_4CB(, _1C_1, _2AiB_1, _1Ai_1, _1Ak_1, _2BA_1, _1B_1, _1N_1, _1Aj_1, _1Aj_2, _1Am_1))
#define prms_4CA(...) decl_4CA((), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_4CA(_1Ak_d1(typename arg__construct__front_end::node_dict),
                          _2BA_d1(typename arg__construct__front_end::node_wcet),
                          _1B_d1 (typename arg__construct__front_end::node_period),
                          _1N_d1 (typename arg__construct__front_end::adjacency_list),
                          _1Aj_d1(typename arg__construct__front_end::channel_list),
                          _1Aj_d2(typename arg__construct__front_end::channel_idx_list),
                          _1Am_d1(arg__construct__front_end::sink_node_ids::size),
                          I, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__back_end;

#define base_1D(_d1, _d2, _d3, I, _1, _2, _3) I                 \
        opts  (_1, arg__hw_checker_msg, _d1)                    \
          opts(_2, arg__hw_core_ids_checker_msg, _d2)           \
          opts(_3, arg__hw_core_ids_dup_checker_msg, _d3)
#define decl_1D(_d1, _d2, _d3, I, _1, _2, _3)                                                                                                   \
        base_1D(_d1, _d2, _d3, I##I,                                                                                                            \
                type(_1, template<bool arg__is_HW_, typename arg__actual_parameter_> class),                                                    \
                type(_2, template<bool arg__is_Core_ids_, typename arg__actual_parameter_> class),                                              \
                type(_3, template<H(bool arg__are_not_duplicated_, v1::error::Pos arg__core_id_1_pos_, v1::error::Pos arg__core_id_2_pos_,      \
                                    int arg__core_id_)> class))
#define args_1D(I, _1, _2, _3) base_1D((), (), (), I, _1, _2, _3)
#define prms_1D(...) decl_1D((), (), (), __VA_ARGS__)

#define base_2D(args_1C, args_1D)               \
        args_1C                                 \
        args_1D
#define decl_2D(I, _1C_1, _1D_1, _1D_2, _1D_3)  \
        base_2D(prms_1C(I, _1C_1),              \
                prms_1D(, _1D_1, _1D_2, _1D_3))
#define args_2D(I, _1C_1, _1D_1, _1D_2, _1D_3) base_2D(args_1C(I, _1C_1), args_1D(, _1D_1, _1D_2, _1D_3))
#define prms_2D(...) decl_2D(__VA_ARGS__)

        template<prms_2D(I, _, _, _, _)>
        struct construct__check_hw;

#define base_1E(I, _1) I                        \
        opts  (_1, arg__Node__node, ())
#define decl_1E(I, _1)                          \
        base_1E(I##I,                           \
                type(_1, typename))
#define args_1E(I, _1) base_1E(I, _1)
#define prms_1E(...) decl_1E(__VA_ARGS__)

#define base_1F(_d1, I, _1) I                   \
        opts  (_1, arg__node_checker_msg, _d1)
#define decl_1F(_d1, I, _1)                                                                     \
        base_1F(_d1, I##I,                                                                      \
                type(_1, template<bool arg__is_Node_, typename arg__actual_parameter_> class))
#define args_1F(I, _1) base_1F((), I, _1)
#define prms_1F(...) decl_1F((), __VA_ARGS__)

#define base_2F(args_1E, args_1F)               \
        args_1E                                 \
        args_1F
#define decl_2F(I, _1E_1, _1F_1)                \
        base_2F(prms_1E(I, _1E_1),              \
                prms_1F(, _1F_1))
#define args_2F(I, _1E_1, _1F_1) base_2F(args_1E(I, _1E_1), args_1F(, _1F_1))
#define prms_2F(...) decl_2F(__VA_ARGS__)

        template<prms_2F(I, _, _)>
        struct construct__check_node;

#define base_2E(_d2, args_1C, args_1E, _1, args_1D, args_1F, args_4A, args_2AiB, _2)    \
        args_1C                                                                         \
        args_1E                                                                         \
        opts(_1, arg__tuple_construct__args, ())                                        \
          args_1D                                                                       \
          args_1F                                                                       \
        args_4A                                                                         \
        args_2AiB                                                                       \
        opts(_2, arg__tuple_checker_msg, _d2)
#define decl_2E(_1D_d1, _1D_d2, _1D_d3, _1F_d1, _4A_d1, _4A_d2, _4A_d3, _4A_d4, _4A_d5, _4A_d6, _4A_d7, _4A_d8, _4A_d9, _1J_d1,                 \
                _3A_d1, _3A_d2, _3A_d3, _3A_d4, _3A_d5, _3A_d6, _3A_d7, _3A_d8, _3A_d9, _3A_d10, _3A_d11, _3A_d12, _3A_d13, _3A_d14,            \
                _3A_d15, _3A_d16, _3A_d17, _3A_d18, _3A_d19, _3A_d20, _3A_d21, _3A_d22, _3A_d23, _3A_d24, _3A_d25, _3A_d26, _3A_d27, _3A_d28,   \
                _1A_d1, _1A_d2, _1Q_d1, _2Q_d1, _1A_d3, _1A_d4, _2A_d1, _1R_d1, _1K_d1, _1K_d2,                                                 \
                _1Y_d1, _1Y_d2, _1Y_d3, _1Y_d4, _1Y_d5, _1Y_d6, _1Y_d7, _1Y_d8, _1Y_d9, _1Y_d10, _1Y_d11, _1Y_d12, _1Y_d13, _1Y_d14,            \
                _1Y_d15, _1Y_d16, _1Y_d17, _1Y_d18, _1Y_d19, _1Y_d20, _1Y_d21, _1Y_d22, _1Y_d23, _1Y_d24, _1Y_d25, _1Y_d26, _1Y_d27,            \
                _1Z_d1, _1Z_d2, _1Z_d3, _1Z_d4, _1Z_d5, _1Z_d6, _1Z_d7, _1Z_d8, _1Z_d9, _1Z_d10, _1Z_d11, _1Z_d12, _1Z_d13, _1Z_d14,            \
                _1Z_d15, _1Z_d16, _1Z_d17, _1Z_d18, _1Z_d19, _1Z_d20, _1Z_d21, _1Z_d22, _1Z_d23, _1Z_d24, _2AiB_d1, _1Ai_d1, _d2,               \
                I, _1C_1, _1E_1, _1, _1D_1, _1D_2, _1D_3, _1F_1, _4A_1, _4A_2, _4A_3, _4A_4, _4A_5, _4A_6, _4A_7, _4A_8, _4A_9, _1J_1,          \
                _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                          \
                _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,                 \
                _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                                     \
                _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                               \
                _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                                  \
                _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                         \
                _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                          \
                _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24, _2AiB_1, _1Ai_1, _2)                            \
        base_2E(_d2,                                                                                                                            \
                prms_1C(I, _1C_1),                                                                                                              \
                prms_1E(, _1E_1),                                                                                                               \
                type(_1, typename),                                                                                                             \
                decl_1D(_1D_d1, _1D_d2, _1D_d3, , _1D_1, _1D_2, _1D_3),                                                                         \
                decl_1F(_1F_d1, , _1F_1),                                                                                                       \
                decl_4A(_4A_d1, _4A_d2, _4A_d3, _4A_d4, _4A_d5, _4A_d6, _4A_d7, _4A_d8, _4A_d9, _1J_d1,                                         \
                        _3A_d1, _3A_d2, _3A_d3, _3A_d4, _3A_d5, _3A_d6, _3A_d7, _3A_d8, _3A_d9, _3A_d10, _3A_d11, _3A_d12, _3A_d13, _3A_d14,    \
                        _3A_d15, _3A_d16, _3A_d17, _3A_d18, _3A_d19, _3A_d20, _3A_d21, _3A_d22, _3A_d23, _3A_d24, _3A_d25, _3A_d26, _3A_d27,    \
                        _3A_d28, _1A_d1, _1A_d2, _1Q_d1, _2Q_d1, _1A_d3, _1A_d4, _2A_d1, _1R_d1, _1K_d1, _1K_d2,                                \
                        _1Y_d1, _1Y_d2, _1Y_d3, _1Y_d4, _1Y_d5, _1Y_d6, _1Y_d7, _1Y_d8, _1Y_d9, _1Y_d10, _1Y_d11, _1Y_d12, _1Y_d13, _1Y_d14,    \
                        _1Y_d15, _1Y_d16, _1Y_d17, _1Y_d18, _1Y_d19, _1Y_d20, _1Y_d21, _1Y_d22, _1Y_d23, _1Y_d24, _1Y_d25, _1Y_d26, _1Y_d27,    \
                        _1Z_d1, _1Z_d2, _1Z_d3, _1Z_d4, _1Z_d5, _1Z_d6, _1Z_d7, _1Z_d8, _1Z_d9, _1Z_d10, _1Z_d11, _1Z_d12, _1Z_d13, _1Z_d14,    \
                        _1Z_d15, _1Z_d16, _1Z_d17, _1Z_d18, _1Z_d19, _1Z_d20, _1Z_d21, _1Z_d22, _1Z_d23, _1Z_d24,                               \
                        , _4A_1, _4A_2, _4A_3, _4A_4, _4A_5, _4A_6, _4A_7, _4A_8, _4A_9, _1J_1,                                                 \
                        _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                  \
                        _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,         \
                        _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                             \
                        _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                       \
                        _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                          \
                        _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                 \
                        _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                  \
                        _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24),                                        \
                decl_2AiB(_2AiB_d1, _1Ai_d1, , _2AiB_1, _1Ai_1),                                                                                \
                type(_2, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class))
#define args_2E(I, _1C_1, _1E_1, _1, _1D_1, _1D_2, _1D_3, _1F_1, _4A_1, _4A_2, _4A_3, _4A_4, _4A_5, _4A_6, _4A_7, _4A_8, _4A_9, _1J_1,          \
                _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                          \
                _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,                 \
                _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                                     \
                _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                               \
                _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                                  \
                _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                         \
                _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                          \
                _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24, _2AiB_1, _1Ai_1, _2)                            \
        base_2E((), args_1C(I, _1C_1), args_1E(, _1E_1), _1, args_1D(, _1D_1, _1D_2, _1D_3), args_1F(, _1F_1),                                  \
                args_4A(, _4A_1, _4A_2, _4A_3, _4A_4, _4A_5, _4A_6, _4A_7, _4A_8, _4A_9, _1J_1,                                                 \
                        _3A_1, _3A_2, _3A_3, _3A_4, _3A_5, _3A_6, _3A_7, _3A_8, _3A_9, _3A_10, _3A_11, _3A_12, _3A_13, _3A_14,                  \
                        _3A_15, _3A_16, _3A_17, _3A_18, _3A_19, _3A_20, _3A_21, _3A_22, _3A_23, _3A_24, _3A_25, _3A_26, _3A_27, _3A_28,         \
                        _1A_1, _1A_2, _1Q_1, _2Q_1,                                                                                             \
                        _1A_3, _1A_4, _2A_1, _1R_1, _1K_1, _1K_2, _1Y_1, _1Y_2, _1Y_3, _1Y_4, _1Y_5, _1Y_6, _1Y_7, _1Y_8,                       \
                        _1Y_9, _1Y_10, _1Y_11, _1Y_12, _1Y_13, _1Y_14, _1Y_15, _1Y_16, _1Y_17, _1Y_18,                                          \
                        _1Y_19, _1Y_20, _1Y_21, _1Y_22, _1Y_23, _1Y_24, _1Y_25, _1Y_26, _1Y_27,                                                 \
                        _1Z_1, _1Z_2, _1Z_3, _1Z_4, _1Z_5, _1Z_6, _1Z_7, _1Z_8, _1Z_9, _1Z_10, _1Z_11, _1Z_12, _1Z_13, _1Z_14,                  \
                        _1Z_15, _1Z_16, _1Z_17, _1Z_18, _1Z_19, _1Z_20, _1Z_21, _1Z_22, _1Z_23, _1Z_24), args_2AiB(, _2AiB_1, _1Ai_1), _2)
#define prms_2E(...) decl_2E((), (), (), (), (), (), (), (), (), (), (), (), (), (),            \
                             (), (), (), (), (), (), (), (), (), (), (), (), (), (),            \
                             (), (), (), (), (), (), (), (), (), (), (), (), (), (),            \
                             (), (), (), (), (), (), (), (), (), (),                            \
                             (), (), (), (), (), (), (), (), (), (), (), (), (), (),            \
                             (), (), (), (), (), (), (), (), (), (), (), (), (),                \
                             (), (), (), (), (), (), (), (), (), (), (), (), (), (),            \
                             (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_2E(_1D_d1  (error::program::construct::arg_2_is_HW),
                         _1D_d2  (error::program::construct::arg_2_arg_2_is_Core_ids),
                         _1D_d3  (error::program::construct::arg_2_arg_2_core_ids_are_not_duplicated),
                         _1F_d1  (error::program::construct::arg_3_is_Node),
                         _4A_d1  (error::program::construct::node_arg_2_is_comp_Unit),
                         _4A_d2  (error::program::construct::node_arg_2_arg_2_is_function_pointer),
                         _4A_d3  (error::program::construct::node_arg_2_arg_3_is_Ratio),
                         _4A_d4  (error::program::construct::node_arg_2_arg_3_has_nonzero_denominator),
                         _4A_d5  (error::program::construct::node_arg_2_arg_3_is_positive),
                         _4A_d6  (error::program::construct::node_arg_3_is_std_ratio),
                         _4A_d7  (error::program::construct::node_arg_3_has_nonzero_denominator),
                         _4A_d8  (error::program::construct::node_arg_3_is_greater_than_or_equal_to_node_arg_2_arg_3),
                         _4A_d9  (error::program::construct::nodes_are_distinct),
                         _1J_d1  (error::program::construct::sequence_of_Node_is_followed_only_by_sequence_of_Feeder),
                         _3A_d1  (error::program::construct::graph_has_no_cycle),
                         _3A_d2  (error::program::construct::graph_has_no_multiple_edges),
                         _3A_d3  (error::program::construct::graph_has_no_loop),
                         _3A_d4  (error::program::construct::feeder_node_element_is_missing),
                         _3A_d5  (error::program::construct::feeder_element_is_producer_node),
                         _3A_d6  (error::program::construct::feeder_element_is_consumer_node),
                         _3A_d7  (error::program::construct::feeder_channel_and_consumer_parameter_counts_are_equal),
                         _3A_d8  (error::program::construct::feeder_consumer_function_is_callable),
                         _3A_d9  (error::program::construct::feeder_channel_element_is_missing),
                         _3A_d10 (error::program::construct::feeder_element_is_channel),
                         _3A_d11 (error::program::construct::feeder_channel_arg_2_is_object_type),
                         _3A_d12 (error::program::construct::feeder_channel_arg_2_is_cv_unqualified),
                         _3A_d13 (error::program::construct::feeder_channel_arg_3_is_not_nullptr),
                         _3A_d14 (error::program::construct::feeder_producer_return_value_is_assignable_to_channel),
                         _3A_d15 (error::program::construct::feeder_init_val_can_initialize_channel),
                         _3A_d16 (error::program::construct::feeder_channel_and_consumer_parameter_are_compatible),
                         _3A_d17 (error::program::construct::feeder_node_arg_2_is_comp_Unit),
                         _3A_d18 (error::program::construct::feeder_node_arg_2_arg_2_is_function_pointer),
                         _3A_d19 (error::program::construct::feeder_node_arg_2_arg_3_is_Ratio),
                         _3A_d20 (error::program::construct::feeder_node_arg_2_arg_3_has_nonzero_denominator),
                         _3A_d21 (error::program::construct::feeder_node_arg_2_arg_3_is_positive),
                         _3A_d22 (error::program::construct::feeder_node_arg_3_is_std_ratio),
                         _3A_d23 (error::program::construct::feeder_node_arg_3_has_nonzero_denominator),
                         _3A_d24 (error::program::construct::feeder_node_arg_3_is_greater_than_or_equal_to_node_arg_2_arg_3),
                         _3A_d25 (error::program::construct::feeders_are_distinct),
                         _3A_d26 (error::program::construct::isolated_node_is_sensor_node),
                         _3A_d27 (error::program::construct::source_node_is_sensor_node),
                         _3A_d28 (error::program::construct::sink_node_is_actuator_node),
                         _1A_d1  (error::program::construct::node_is_already_specified),
                         _1A_d2  (error::program::construct::sensor_and_actuator_nodes_are_connected),
                         _1Q_d1  (error::program::construct::confluent_node_release_idx_is_in_sensing_release_domain),
                         _2Q_d1  (error::program::construct::correlation_is_within_threshold),
                         _1A_d3  (error::program::construct::sequence_of_Correlation_is_followed_by_nothing),
                         _1A_d4  (error::program::construct::sequence_of_ETE_delay_is_followed_only_by_sequence_of_Correlation),
                         _2A_d1  (error::program::construct::sequence_of_Feeder_is_followed_only_by_sequence_of_ETE_delay_or_Correlation),
                         _1R_d1  (error::program::construct::end_to_end_delay_is_between_min_and_max_delays),
                         _1K_d1  (error::program::construct::node_id_is_producer_node_id),
                         _1K_d2  (error::program::construct::node_id_is_sink_node_id),
                         _1Y_d1  (error::program::construct::ete_delay_arg_4_is_less_than_or_equal_to_ete_delay_arg_5),
                         _1Y_d2  (error::program::construct::ete_delay_arg_2_is_Node),
                         _1Y_d3  (error::program::construct::ete_delay_arg_2_is_sensor_node),
                         _1Y_d4  (error::program::construct::ete_delay_arg_3_is_Node),
                         _1Y_d5  (error::program::construct::ete_delay_arg_3_is_actuator_node),
                         _1Y_d6  (error::program::construct::ete_delay_arg_4_is_std_ratio),
                         _1Y_d7  (error::program::construct::ete_delay_arg_4_has_nonzero_denominator),
                         _1Y_d8  (error::program::construct::ete_delay_arg_4_is_nonnegative),
                         _1Y_d9  (error::program::construct::ete_delay_arg_5_is_std_ratio),
                         _1Y_d10 (error::program::construct::ete_delay_arg_5_has_nonzero_denominator),
                         _1Y_d11 (error::program::construct::ete_delay_arg_5_is_positive),
                         _1Y_d12 (error::program::construct::ete_delay_arg_2_arg_2_is_comp_Unit),
                         _1Y_d13 (error::program::construct::ete_delay_arg_2_arg_2_arg_2_is_function_pointer),
                         _1Y_d14 (error::program::construct::ete_delay_arg_2_arg_2_arg_3_is_Ratio),
                         _1Y_d15 (error::program::construct::ete_delay_arg_2_arg_2_arg_3_has_nonzero_denominator),
                         _1Y_d16 (error::program::construct::ete_delay_arg_2_arg_2_arg_3_is_positive),
                         _1Y_d17 (error::program::construct::ete_delay_arg_2_arg_3_is_std_ratio),
                         _1Y_d18 (error::program::construct::ete_delay_arg_2_arg_3_has_nonzero_denominator),
                         _1Y_d19 (error::program::construct::ete_delay_arg_2_arg_3_is_greater_than_or_equal_to_ete_delay_arg_2_arg_2_arg_3),
                         _1Y_d20 (error::program::construct::ete_delay_arg_3_arg_2_is_comp_Unit),
                         _1Y_d21 (error::program::construct::ete_delay_arg_3_arg_2_arg_2_is_function_pointer),
                         _1Y_d22 (error::program::construct::ete_delay_arg_3_arg_2_arg_3_is_Ratio),
                         _1Y_d23 (error::program::construct::ete_delay_arg_3_arg_2_arg_3_has_nonzero_denominator),
                         _1Y_d24 (error::program::construct::ete_delay_arg_3_arg_2_arg_3_is_positive),
                         _1Y_d25 (error::program::construct::ete_delay_arg_3_arg_3_is_std_ratio),
                         _1Y_d26 (error::program::construct::ete_delay_arg_3_arg_3_has_nonzero_denominator),
                         _1Y_d27 (error::program::construct::ete_delay_arg_3_arg_3_is_greater_than_or_equal_to_ete_delay_arg_3_arg_2_arg_3),
                         _1Z_d1  (error::program::construct::correlation_arg_4_element_is_Node),
                         _1Z_d2  (error::program::construct::correlation_arg_4_element_is_sensor_node),
                         _1Z_d3  (error::program::construct::correlation_nodes_are_distinct),
                         _1Z_d4  (error::program::construct::correlation_arg_2_is_Node),
                         _1Z_d5  (error::program::construct::correlation_arg_2_is_actuator_node),
                         _1Z_d6  (error::program::construct::correlation_arg_3_is_std_ratio),
                         _1Z_d7  (error::program::construct::correlation_arg_3_has_nonzero_denominator),
                         _1Z_d8  (error::program::construct::correlation_arg_3_is_nonnegative),
                         _1Z_d9  (error::program::construct::correlation_arg_2_arg_2_is_comp_Unit),
                         _1Z_d10 (error::program::construct::correlation_arg_2_arg_2_arg_2_is_function_pointer),
                         _1Z_d11 (error::program::construct::correlation_arg_2_arg_2_arg_3_is_Ratio),
                         _1Z_d12 (error::program::construct::correlation_arg_2_arg_2_arg_3_has_nonzero_denominator),
                         _1Z_d13 (error::program::construct::correlation_arg_2_arg_2_arg_3_is_positive),
                         _1Z_d14 (error::program::construct::correlation_arg_2_arg_3_is_std_ratio),
                         _1Z_d15 (error::program::construct::correlation_arg_2_arg_3_has_nonzero_denominator),
                         _1Z_d16 (error::program::construct::correlation_arg_2_arg_3_is_greater_than_or_equal_to_correlation_arg_2_arg_2_arg_3),
                         _1Z_d17 (error::program::construct::correlation_arg_4_element_arg_2_is_comp_Unit),
                         _1Z_d18 (error::program::construct::correlation_arg_4_element_arg_2_arg_2_is_function_pointer),
                         _1Z_d19 (error::program::construct::correlation_arg_4_element_arg_2_arg_3_is_Ratio),
                         _1Z_d20 (error::program::construct::correlation_arg_4_element_arg_2_arg_3_has_nonzero_denominator),
                         _1Z_d21 (error::program::construct::correlation_arg_4_element_arg_2_arg_3_is_positive),
                         _1Z_d22 (error::program::construct::correlation_arg_4_element_arg_3_is_std_ratio),
                         _1Z_d23 (error::program::construct::correlation_arg_4_element_arg_3_has_nonzero_denominator),
                         _1Z_d24 (error::program::construct::
                                  correlation_arg_4_element_arg_3_is_greater_than_or_equal_to_correlation_arg_4_element_arg_2_arg_3),
                         _2AiB_d1(error::program::construct::backend_is_available),
                         _1Ai_d1 (error::program::construct::gedf_schedulability_test_is_successful),
                         _d2     (error::program::construct::arg_4_is_tuple_construct),
                         I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct
        /* {
         *   with template<typename I, typename... args> tuple::construct<I, args...> = arg__tuple_construct__args {
         *     let parse_nodes<I, 2, tuple::construct<I, arg__Node__node, args...>> parsed_nodes;
         *     include parsed_nodes;
         *     let parse_feeders<I, parsed_nodes::remaining_args_pos, typename parsed_nodes::node_dict,
         *                       typename parsed_nodes::remaining_args> parsed_feeders;
         *     include parsed_feeders;
         *   }
         *
         *   typedef arg__HW__hw_desc hw_desc;
         * }
         */;

        

        template<prms_2E(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct :
          arg__tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct__args> {
        };

        template<prms_2E(I, _, _, R(typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct<args_2E(I, _, _, R(tuple::construct<I, args...>),
                                 _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                 _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                 _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          construct__check_hw<args_2D(I, _, _, _, _)>,
          construct__check_node<args_2F(I, _, _)>,
          construct__args_validated<args_3IC(I, _, _, _, R(2), R(tuple::construct<I, arg__Node__node, args...>), _, _, _, _, _, _, _, _, _, _, _, _,
                                             _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                             R(construct__error_pos_adaptor__arg__validate_nodes_msg<args_2H(I,
                                                                                                             R(arg__validate_isolated_nodes_msg))>::
                                               template msg),
                                             R(construct__error_pos_adaptor__arg__validate_nodes_msg<args_2H(I,
                                                                                                             R(arg__validate_source_nodes_msg))>::
                                               template msg),
                                             R(construct__error_pos_adaptor__arg__validate_nodes_msg<args_2H(I,
                                                                                                             R(arg__validate_sink_nodes_msg))>::
                                               template msg),
                                             _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                             _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
        };

        

        template<prms_2H(I, _)>
        struct construct__error_pos_adaptor__arg__validate_nodes_msg
        {
          template<bool arg__is_expected_node_, v1::error::Pos arg__node_pos_, typename arg__node_type_>
          using msg = arg__validate_nodes_msg<arg__is_expected_node_, arg__node_pos_ + 1, arg__node_type_>;
        };

        

        template<prms_3IC(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__args_validated :
#define FE construct__front_end<args_5A(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,   \
                                        _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,   \
                                        _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
          utility::run_metaprogram<I, FE>,
          construct__back_end<args_4CA(I, R(FE), _, _, _, X, X, X, X, X, X, X)>
        {
          typedef FE front_end;
        };
#undef front_end

        

        template<prms_2D(I, _, _, _, _)>
        struct construct__check_hw :
          arg__hw_checker_msg<!!utility::always_false<I>(), arg__HW__hw_desc> {
        };

        template<prms_2D(I, R(typename... args), _, _, _)>
        struct construct__check_hw<args_2D(I, R(v1::HW<args...>), _, _, _)> :
          utility::run_metaprogram<I, hw::construct<I, args..., arg__hw_core_ids_checker_msg, arg__hw_core_ids_dup_checker_msg>> {
        };

        

        template<prms_2F(I, _, _)>
        struct construct__check_node :
          arg__node_checker_msg<!!utility::always_false<I>(), arg__Node__node> {
        };

        template<prms_2F(I, R(typename... args), _)>
        struct construct__check_node<args_2F(I, R(v1::Node<args...>), _)> {
        };

        

        template<prms_5A(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end :
#define result parse_nodes<I, arg__pos, arg__tuple_construct__input, arg__node_computation_checker_msg, arg__node_computation_fn_ptr_checker_msg,  \
                           arg__node_computation_wcet_checker_msg, arg__node_computation_wcet_den_checker_msg,                                     \
                           arg__node_computation_wcet_validator_msg, arg__node_period_checker_msg, arg__node_period_den_checker_msg,               \
                           arg__node_period_validator_msg, arg__distinct_node_checker_msg>

          result,
          construct__front_end__nodes_parsed<args_2J(I, R(result::remaining_args_pos), R(typename result::remaining_args),
                                                     R(typename result::node_dict), R(typename result::node_wcet), R(typename result::node_period),
                                                     R(typename result::is_intermediary_node), R(typename result::is_actuator_node),
                                                     _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                     _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                     _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
        };
#undef result

        

        template<prms_2J(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__nodes_parsed :
          construct__front_end__done<args_3IA(I, _, _, R(arg__node_follower_checker_msg))> {
        };

        template<prms_2J(I, _, X, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__nodes_parsed<args_2J(I, _, R(tuple::construct<I>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          construct__front_end__parse_feeders<args_3IB(I, _, R(tuple::construct<I>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                       _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                       _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                       _, _, _, _, _, _, _, _, _, _, _)> {
        };

        template<prms_2J(I, _, R(typename... args__feeder_arg, typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__nodes_parsed<args_2J(I, _, R(tuple::construct<I, Feeder<args__feeder_arg...>, args...>),
                                                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                          _, _, _, _, _)> :
          construct__front_end__parse_feeders<args_3IB(I, _, R(tuple::construct<I, Feeder<args__feeder_arg...>, args...>), _, _, _, _, _, _, _, _,
                                                       _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                       _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                       _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
        };

        

        template<prms_3IB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__parse_feeders :
#define result parse_feeders<I, arg__pos, arg__tuple_construct__of__dict_Entry__of__Node__node_dict, arg__tuple_construct__input,                  \
                             arg__pos - tuple::size<I, arg__tuple_construct__of__dict_Entry__of__Node__node_dict>::value - 1,                      \
                             arg__check_cycle_msg, arg__multiple_edge_checker_msg, arg__loop_checker_msg, arg__node_missing_msg,                   \
                             arg__producer_node_checker_msg, arg__consumer_node_checker_msg, arg__param_count_checker_msg,                         \
                             arg__consumer_invocation_checker_msg, arg__channel_missing_msg, arg__channel_checker_msg,                             \
                             arg__channel_type_checker_msg, arg__channel_cv_checker_msg, arg__channel_value_checker_msg,                           \
                             arg__producer_return_value_checker_msg, arg__channel_initval_checker_msg,                                             \
                             arg__channel_and_consumer_param_checker_msg, arg__feeder_node_computation_checker_msg,                                \
                             arg__feeder_node_computation_fn_ptr_checker_msg, arg__feeder_node_computation_wcet_checker_msg,                       \
                             arg__feeder_node_computation_wcet_den_checker_msg, arg__feeder_node_computation_wcet_validator_msg,                   \
                             arg__feeder_node_period_checker_msg, arg__feeder_node_period_den_checker_msg, arg__feeder_node_period_validator_msg,  \
                             arg__distinct_feeder_checker_msg, arg__specified_node_checker_msg>
          result,
          construct__front_end__check_all_nodes_are_sensor_nodes<args_3GA(I, _, _, R(typename result::isolated_node_ids),
                                                                          R(arg__validate_isolated_nodes_msg), X, X)>,
          construct__front_end__check_all_nodes_are_sensor_nodes<args_3GA(I, _, _, R(typename result::source_node_ids),
                                                                          R(arg__validate_source_nodes_msg), X, X)>,
          construct__front_end__check_all_nodes_are_actuator_nodes<args_2GB(I, _, R(typename result::sink_node_ids),
                                                                            R(arg__validate_sink_nodes_msg), X, X)>,
          construct__front_end__feeders_parsed<args_7BABA(I, R(result::remaining_args_pos), R(typename result::remaining_args), _, _, _,
                                                          R(typename result::adjacency_list),
                                                          R(typename result::is_sink_node), R(typename result::is_isolated_node),
                                                          R(result::template producer_and_sink_are_connected), _, _, _, _, _, _, _, _, _, _, _, _,
                                                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
        };
#undef result

        

        template<prms_7BABA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__feeders_parsed :
          construct__front_end__done<args_3IA(I, _, _, R(arg__feeder_follower_checker_msg))> {
        };

        template<prms_7BABA(I, _, R(typename... args__ete_delay_arg, typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _, _, _, _, _, _)>
        struct construct__front_end__feeders_parsed<args_7BABA(I, _, R(tuple::construct<I, ETE_delay<args__ete_delay_arg...>, args...>),
                                                               _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                               _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                               _, _, _, _, _, _, _, _, _, _, _, _)> :
          construct__front_end__parse_ete_delays<args_8BABA(I, _, R(tuple::construct<I, ETE_delay<args__ete_delay_arg...>, args...>),
                                                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                            _, _, _, _, _, _,  _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                            _, _, _, _, _, _, _, _, _, _, X)> {
        };

        template<prms_7BABA(I, _, R(typename... args__correlation_arg, typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__feeders_parsed<args_7BABA(I, _, R(tuple::construct<I, Correlation<args__correlation_arg...>, args...>),
                                                               _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                               _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                               _, _, _, _, _, _, _, _, _, _, _, _)> :
          construct__front_end__parse_correlations<args_7BAAA(I, _, R(tuple::construct<I, Correlation<args__correlation_arg...>, args...>),
                                                              _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                              _, _, _, _, _, _, _, _, _, _, X)> {
        };

        

        template<prms_3GA(I, _, _, _, _, _, X)>
        struct construct__front_end__check_all_nodes_are_sensor_nodes<args_3GA(I, _, _, _, _, _, R(true))> {
        };

        template<prms_3GA(I, _, _, _, _, _, _)>
        struct construct__front_end__check_all_nodes_are_sensor_nodes :
          arg__validate_nodes_msg<!arg__Array__of__bool__is_intermediary_node::elems[arg__Array__of__dict_Key__node_ids::elems[arg__i]],
                                  arg__Array__of__dict_Key__node_ids::elems[arg__i] + 1, v1::node::type::INTERMEDIARY>,
          arg__validate_nodes_msg<!arg__Array__of__bool__is_actuator_node::elems[arg__Array__of__dict_Key__node_ids::elems[arg__i]],
                                  arg__Array__of__dict_Key__node_ids::elems[arg__i] + 1, v1::node::type::ACTUATOR>,
          construct__front_end__check_all_nodes_are_sensor_nodes<args_3GA(I, _, _, _, _, R(arg__i + 1), X)> {
        };

        

        template<prms_2GB(I, _, _, _, _, X)>
        struct construct__front_end__check_all_nodes_are_actuator_nodes<args_2GB(I, _, _, _, _, R(true))> {
        };

        template<prms_2GB(I, _, _, _, _, _)>
        struct construct__front_end__check_all_nodes_are_actuator_nodes :
          arg__validate_nodes_msg<!arg__Array__of__bool__is_intermediary_node::elems[arg__Array__of__dict_Key__node_ids::elems[arg__i]],
                                  arg__Array__of__dict_Key__node_ids::elems[arg__i] + 1, v1::node::type::INTERMEDIARY>,
          construct__front_end__check_all_nodes_are_actuator_nodes<args_2GB(I, _, _, _, R(arg__i + 1), X)> {
        };

        

        template<prms_8BABA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__parse_ete_delays :
          construct__front_end__done<args_3IA(I, _, _, R(arg__ete_delay_follower_checker_msg))> {
        };

        template<prms_8BABA(I, _, R(typename... args__correlation_arg, typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,  _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__parse_ete_delays<args_8BABA(I, _, R(tuple::construct<I, Correlation<args__correlation_arg...>, args...>),
                                                                 _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                 _, _, _, _, _, _, _, _,  _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                 _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          construct__front_end__parse_correlations<args_7BAAA(I, _, R(tuple::construct<I, Correlation<args__correlation_arg...>, args...>), _,
                                                              _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                              _, _, _, _, _, _, _, _, _, X)> {
        };

        template<prms_8BABA(I, _, R(typename... args__delay_arg, typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,  _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _, _, _, _, _, _)>
        struct construct__front_end__parse_ete_delays<args_8BABA(I, _, R(tuple::construct<I, ETE_delay<args__delay_arg...>, args...>),
                                                                 _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                 _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                 _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          utility::run_metaprogram<I, ete_delay::construct<I, args__delay_arg...,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template min_max_checker_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_2_checker_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_2_validator_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_3_checker_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_3_validator_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_4_checker_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_4_den_checker_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_4_validator_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_5_checker_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_5_den_checker_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_5_validator_msg,
                                                           H(arg__construct__error_pos_adaptor__ete_delay_msgs::template
                                                             arg_2_computation_checker_msg),
                                                           H(arg__construct__error_pos_adaptor__ete_delay_msgs::template
                                                             arg_2_computation_fn_ptr_checker_msg),
                                                           H(arg__construct__error_pos_adaptor__ete_delay_msgs::template
                                                             arg_2_computation_wcet_checker_msg),
                                                           H(arg__construct__error_pos_adaptor__ete_delay_msgs::template
                                                             arg_2_computation_wcet_den_checker_msg),
                                                           H(arg__construct__error_pos_adaptor__ete_delay_msgs::template
                                                             arg_2_computation_wcet_validator_msg),
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_2_period_checker_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_2_period_den_checker_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_2_period_validator_msg,
                                                           H(arg__construct__error_pos_adaptor__ete_delay_msgs::template
                                                             arg_3_computation_checker_msg),
                                                           H(arg__construct__error_pos_adaptor__ete_delay_msgs::template
                                                             arg_3_computation_fn_ptr_checker_msg),
                                                           H(arg__construct__error_pos_adaptor__ete_delay_msgs::template
                                                             arg_3_computation_wcet_checker_msg),
                                                           H(arg__construct__error_pos_adaptor__ete_delay_msgs::template
                                                             arg_3_computation_wcet_den_checker_msg),
                                                           H(arg__construct__error_pos_adaptor__ete_delay_msgs::template
                                                             arg_3_computation_wcet_validator_msg),
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_3_period_checker_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_3_period_den_checker_msg,
                                                           arg__construct__error_pos_adaptor__ete_delay_msgs::template arg_3_period_validator_msg>>,
          construct__front_end__validate_ete_delay<args_7BABB(I, _, R(tuple::construct<I, args__delay_arg...>),
                                                              _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>,
          construct__front_end__parse_ete_delays<args_8BABA(I,
                                                            R(arg__pos + 1), R(tuple::construct<I, args...>), _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,  _, _, _, _, _, _, _, _, _,
                                                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, X)> {
        };

        

        template<prms_2YB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__error_pos_adaptor__ete_delay_msgs
        {
          template<bool arg__is_less_than_or_equal_to_max_delay_, typename arg__min_delay_, typename arg__max_delay_>
          using min_max_checker_msg = arg__ete_delay_min_max_checker_msg<arg__is_less_than_or_equal_to_max_delay_, arg__pos,
                                                                         arg__min_delay_, arg__max_delay_>;

          template<bool arg__is_Node_, typename arg__actual_parameter_>
          using arg_2_checker_msg = arg__ete_delay_2_checker_msg<arg__is_Node_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_sensor_node_, typename arg__node_type_>
          using arg_2_validator_msg = arg__ete_delay_2_validator_msg<arg__is_sensor_node_, arg__pos, arg__node_type_>;

          template<bool arg__is_Node_, typename arg__actual_parameter_>
          using arg_3_checker_msg = arg__ete_delay_3_checker_msg<arg__is_Node_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_actuator_node_, typename arg__node_type_>
          using arg_3_validator_msg = arg__ete_delay_3_validator_msg<arg__is_actuator_node_, arg__pos, arg__node_type_>;

          template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
          using arg_4_checker_msg = arg__ete_delay_4_checker_msg<arg__is_std_ratio_, arg__pos, arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
          using arg_4_den_checker_msg = arg__ete_delay_4_den_checker_msg<arg__has_nonzero_denominator_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_nonnegative_, typename arg__actual_parameter_>
          using arg_4_validator_msg = arg__ete_delay_4_validator_msg<arg__is_nonnegative_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
          using arg_5_checker_msg = arg__ete_delay_5_checker_msg<arg__is_std_ratio_, arg__pos, arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
          using arg_5_den_checker_msg = arg__ete_delay_5_den_checker_msg<arg__has_nonzero_denominator_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_positive_, typename arg__actual_parameter_>
          using arg_5_validator_msg = arg__ete_delay_5_validator_msg<arg__is_positive_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_comp_Unit_, typename arg__actual_parameter_>
          using arg_2_computation_checker_msg = arg__ete_delay_2_computation_checker_msg<arg__is_comp_Unit_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_function_pointer_, typename arg__actual_parameter_>
          using arg_2_computation_fn_ptr_checker_msg = arg__ete_delay_2_computation_fn_ptr_checker_msg<arg__is_function_pointer_, arg__pos,
                                                                                                       arg__actual_parameter_>;

          template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
          using arg_2_computation_wcet_checker_msg = arg__ete_delay_2_computation_wcet_checker_msg<arg__is_std_ratio_, arg__pos,
                                                                                                   arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
          using arg_2_computation_wcet_den_checker_msg = arg__ete_delay_2_computation_wcet_den_checker_msg<arg__has_nonzero_denominator_, arg__pos,
                                                                                                           arg__actual_parameter_>;

          template<bool arg__is_positive_, typename arg__wcet_>
          using arg_2_computation_wcet_validator_msg = arg__ete_delay_2_computation_wcet_validator_msg<arg__is_positive_, arg__pos, arg__wcet_>;

          template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
          using arg_2_period_checker_msg = arg__ete_delay_2_period_checker_msg<arg__is_std_ratio_, arg__pos, arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
          using arg_2_period_den_checker_msg = arg__ete_delay_2_period_den_checker_msg<arg__has_nonzero_denominator_, arg__pos,
                                                                                       arg__actual_parameter_>;

          template<bool arg__is_greater_than_or_equal_, typename arg__period_, typename arg__wcet_>
          using arg_2_period_validator_msg = arg__ete_delay_2_period_validator_msg<arg__is_greater_than_or_equal_, arg__pos, arg__period_,
                                                                                   arg__wcet_>;

          template<bool arg__is_comp_Unit_, typename arg__actual_parameter_>
          using arg_3_computation_checker_msg = arg__ete_delay_3_computation_checker_msg<arg__is_comp_Unit_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_function_pointer_, typename arg__actual_parameter_>
          using arg_3_computation_fn_ptr_checker_msg = arg__ete_delay_3_computation_fn_ptr_checker_msg<arg__is_function_pointer_, arg__pos,
                                                                                                       arg__actual_parameter_>;

          template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
          using arg_3_computation_wcet_checker_msg = arg__ete_delay_3_computation_wcet_checker_msg<arg__is_std_ratio_, arg__pos,
                                                                                                   arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
          using arg_3_computation_wcet_den_checker_msg = arg__ete_delay_3_computation_wcet_den_checker_msg<arg__has_nonzero_denominator_, arg__pos,
                                                                                                           arg__actual_parameter_>;

          template<bool arg__is_positive_, typename arg__wcet_>
          using arg_3_computation_wcet_validator_msg = arg__ete_delay_3_computation_wcet_validator_msg<arg__is_positive_, arg__pos, arg__wcet_>;

          template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
          using arg_3_period_checker_msg = arg__ete_delay_3_period_checker_msg<arg__is_std_ratio_, arg__pos, arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
          using arg_3_period_den_checker_msg = arg__ete_delay_3_period_den_checker_msg<arg__has_nonzero_denominator_, arg__pos,
                                                                                       arg__actual_parameter_>;

          template<bool arg__is_greater_than_or_equal_, typename arg__period_, typename arg__wcet_>
          using arg_3_period_validator_msg = arg__ete_delay_3_period_validator_msg<arg__is_greater_than_or_equal_, arg__pos,
                                                                                   arg__period_, arg__wcet_>;
        };

        

        template<prms_7BABB(I, _, R(typename arg__Node__sensor, typename arg__Node__actuator,
                                    typename arg__Ratio__min_delay, typename arg__Ratio__max_delay),
                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__validate_ete_delay<args_7BABB(I, _, R(tuple::construct<H(I, arg__Node__sensor, arg__Node__actuator,
                                                                                              arg__Ratio__min_delay, arg__Ratio__max_delay)>),
                                                                   _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
#define sensor_node_id get_node_id<H(I, arg__pos, 1, arg__tuple_construct__of__dict_Entry__of__Node__node_dict, arg__Node__sensor,      \
                                     arg__specified_node_checker_msg)>::value
#define actuator_node_id get_node_id<H(I, arg__pos, 2, arg__tuple_construct__of__dict_Entry__of__Node__node_dict, arg__Node__actuator,  \
                                       arg__specified_node_checker_msg)>::value
          arg__sensor_actuator_connection_checker_msg<
          H(arg__producer_and_sink_are_connected<I,
            H(utility::run_metaprogram<I,
              H(arg__sensor_actuator_connection_checker_msg<!arg__Array__of__bool__is_isolated_node::elems[sensor_node_id::value], arg__pos, 1, 2>,
                typename sensor_node_id)>::value::value,
              actuator_node_id::value,
              construct__error_pos_adaptor__producer_and_sink_are_connected_msgs<args_2K(I, R(arg__pos),
                                                                                         R(1), _, _)>::template producer_node_id_checker_msg,
              construct__error_pos_adaptor__producer_and_sink_are_connected_msgs<args_2K(I, R(arg__pos),
                                                                                         R(2), _, _)>::template sink_node_id_checker_msg>::value),
            arg__pos, 1, 2)>,
          construct__front_end__validate_ete_delay__run<args_3RA(I, _,
                                                                 R(utility::fp::Double<I, arg__Ratio__min_delay>),
                                                                 R(utility::fp::Double<I, arg__Ratio__max_delay>),
                                                                 R(sensor_node_id::value), R(actuator_node_id::value), _, _, _, _, _, _, _,
                                                                 X, X, X, X, X, X, X)> {
        };
#undef sensor_node_id
#undef actuator_node_id

        

        template<prms_2K(I, _, _, _, _)>
        struct construct__error_pos_adaptor__producer_and_sink_are_connected_msgs
        {
          template<bool arg__is_producer_node_id_, v1::dict::Key arg__node_id_>
          using producer_node_id_checker_msg = arg__producer_node_id_checker_msg<arg__is_producer_node_id_, arg__ete_delay_or_correlation_pos,
                                                                                 arg__node_pos_in_ete_delay_or_correlation, arg__node_id_>;
          template<bool arg__is_sink_node_id_, v1::dict::Key arg__node_id_>
          using sink_node_id_checker_msg = arg__sink_node_id_checker_msg<arg__is_sink_node_id_, arg__ete_delay_or_correlation_pos,
                                                                         arg__node_pos_in_ete_delay_or_correlation, arg__node_id_>;
        };

        

        //\begin{depth-first traversal has visited all possible end-to-end paths}
        template<prms_3RA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__validate_ete_delay__run {
        };
        //\end{depth-first traversal has visited all possible end-to-end paths}
        //\begin{depth-first traversal has not visited all possible end-to-end paths}
#define prms_3RA_1(I, _1T_1, _X1_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)                                      \
        prms_3RA(I, _, _, _, _1T_1, _, _, _, _, _, _, _, _, _X1_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)
#define args_3RA_1(I, _1T_1, _X1_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)                                      \
        args_3RA(I, _, _, _, _1T_1, _, _, _, _, _, _, _, _, _X1_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)

#define prms_3RA_2A(I, _2Ab_2, _2Ab_4) prms_3RA_1(I, _, RR(prms_1M(, _)), _, _, X, _2Ab_2, _, _2Ab_4)
#define args_3RA_2A(I, _2Ab_2, _2Ab_4) args_3RA_1(I, _, R(construct__path<args_1M(I, _)>), _, _, R(false), _2Ab_2, _, _2Ab_4)

#define prms_3RA_2B(I, _1T_1, _X1_1, _1P_1) prms_3RA_1(I, _1T_1, _X1_1, _1P_1, X, X, X, X, X)
#define args_3RA_2B(I, _1T_1, _X1_1, _1P_1) args_3RA_1(I, _1T_1, _X1_1, _1P_1, X, X, X, X, X)

        //  \begin{depth-first traversal is obtaining one end-to-end path}
        //    \begin{child under inspection and constrained sink are not connected}
        template<prms_3RA_2A(I, X, X)>
        struct construct__front_end__validate_ete_delay__run<args_3RA_2A(I, R(false), R(false))> :
          construct__front_end__validate_ete_delay__run<args_3RA_2B(I, _, R(construct__path<args_1M(I, _)>), R(arg__i + 1))> { // inspect next child
        };
        //    \end{child under inspection and constrained sink are not connected}
        //    \begin{child under inspection and constrained sink are connected}
        template<prms_3RA_2A(I, X, X)>
        struct construct__front_end__validate_ete_delay__run<args_3RA_2A(I, R(false), R(true))> :
          construct__front_end__validate_ete_delay__run<args_3RA_2B(I, R(arg__Array__of__Array__of__dict_Key__adjacency_list::
                                                                         elems[arg__prod_node_id][arg__i]),
                                                                    R(construct__path<args_1M(I, R(args__node_id..., arg__prod_node_id))>),
                                                                    X)>,  // visit this child
          construct__front_end__validate_ete_delay__run<args_3RA_2B(I, _, R(construct__path<args_1M(I, _)>), R(arg__i + 1))> { // inspect next child
        };
        //    \end{child under inspection and constrained sink are connected}
        //  \end{depth-first traversal is obtaining one end-to-end path}
        //  \begin{depth-first traversal has obtained one end-to-end path}
        template<prms_3RA_2A(I, X, _)>
        struct construct__front_end__validate_ete_delay__run<args_3RA_2A(I, R(true), _)> :
          construct__ete_delayset__check_member<args_3RB(I, _, _, _, _, _, _,
                                                         R(construct__path<args_1M(I,
                                                                                   R(args__node_id..., arg__prod_node_id, arg__actuator_node_id))>),
                                                         X, X, X, X)>, // process this path
          construct__front_end__validate_ete_delay__run<args_3RA_2B(I, _, R(construct__path<args_1M(I, _)>), R(arg__i + 1))> { // inspect next child
        };
        //  \end{depth-first traversal has obtained one end-to-end path}
        //\end{depth-first traversal has not visited all possible end-to-end paths}

        

        template<prms_3RB(I, _, _, _, _, _, _, _, _, _, _, X)>
        struct construct__ete_delayset__check_member<args_3RB(I,
                                                              _, _, _, _, _, _, _, _, _, _, R(construct__ete_delayset__member_violation::NONE))> {
        };

        template<prms_3RB(I, _, _, _, _, _, _, _, _, X, _, X)>
        struct construct__ete_delayset__check_member<args_3RB(I, _, _, _, _, _, _, _, _,
                                                              R(false), _, R(construct__ete_delayset__member_violation::NONE))> :
          construct__ete_delayset__check_member<args_3RB(I, _, _, _, _, _, _, _, R(arg__i + 1), X, X, X)> {
        };

        template<prms_3RB(I, _, _, _, _, _, _, RR(prms_1M(, _)), _, _, _, X)>
        struct construct__ete_delayset__check_member<args_3RB(I, _, _, _, _, _, _, R(construct__path<args_1M(I, _)>), _, _, _,
                                                              R(construct__ete_delayset__member_violation::MIN))> :
          arg__ete_delay_checker_msg<!!utility::always_false<I>(), arg__pos, typename arg__utility_fp_Double__lower_bound::rational_value,
                                     typename utility::fp::Ratio<I, &construct__ete_delay__min<args_2BD(I, _, _,
                                                                                                        R(arg__construct__ete_delay__at_i))>::value,
                                                                 std::ratio<1, 1000000>,
                                                                 utility::fp::floor<I, true>>::value,
                                     typename arg__utility_fp_Double__upper_bound::rational_value,
                                     v1::error::Path_forming_node_pos<(args__node_id + 2)...>, 1 + arg__i> {
        };

        template<prms_3RB(I, _, _, _, _, _, _, RR(prms_1M(, _)), _, _, _, X)>
        struct construct__ete_delayset__check_member<args_3RB(I, _, _, _, _, _, _, R(construct__path<args_1M(I, _)>), _, _, _,
                                                              R(construct__ete_delayset__member_violation::MAX))> :
          arg__ete_delay_checker_msg<!!utility::always_false<I>(), arg__pos, typename arg__utility_fp_Double__lower_bound::rational_value,
                                     typename utility::fp::Ratio<I, &arg__construct__ete_delay__at_i::value,
                                                                 std::ratio<1, 1000000>,
                                                                 utility::fp::ceil<I, true>>::value,
                                     typename arg__utility_fp_Double__upper_bound::rational_value,
                                     v1::error::Path_forming_node_pos<(args__node_id + 2)...>, 1 + arg__i> {
        };

        

        template<prms_2BD(I, _, _, _)>
        struct construct__ete_delay__min {
          static constexpr double value {(arg__construct__ete_delay__max::value == utility::fp::positive_infinity::value
                                          ? utility::fp::positive_infinity::value
                                          : (arg__construct__ete_delay__max::value
                                             - arg__Array__of__double__node_period_db::elems[arg__actuator_node_id]))};
        };

        

        template<prms_2BB(I, _,
                          RR(prms_1M(, R(v1::dict::Key arg__sensor_node_id, v1::dict::Key arg__cons_node_id, v1::dict::Key... args__node_id))))>
        struct construct__hyperperiod<args_2BB(I, _, R(construct__path<args_1M(I, R(arg__sensor_node_id, arg__cons_node_id, args__node_id...))>))>
        {
          double value;
          v1::dict::Key first_node_id;
          v1::dict::Key last_node_id;

        private:
          static constexpr double update_hyperperiod(double hyperperiod, double period) {
            if (utility::fp::lt(hyperperiod, period)) {
              double tmp = hyperperiod;
              hyperperiod = period;
              period = tmp;
            }
            v1::array::Idx j = 0;
            while (!utility::fp::is_integral(hyperperiod * ++j / period));
            return hyperperiod * j;
          }

        public:
          constexpr construct__hyperperiod() :
            value(0), first_node_id(arg__sensor_node_id), last_node_id(v1::dict::Invalid_key::value)
          {
            const v1::dict::Key path_nodes[] = {arg__sensor_node_id, arg__cons_node_id, args__node_id...};
            double hyperperiod = arg__Array__of__double__node_period_db::elems[arg__sensor_node_id];
            for (v1::array::Idx i = 1, i_end = 2 + sizeof...(args__node_id); i < i_end; ++i) {
              double period = arg__Array__of__double__node_period_db::elems[path_nodes[i]];
              if (!utility::fp::eq(hyperperiod, period)) {
                hyperperiod = update_hyperperiod(hyperperiod, period);
              }
            }
            value = hyperperiod;
            last_node_id = path_nodes[sizeof...(args__node_id) + 1];
          }
        };

        

        template<prms_3BB(I, _, _, X)>
        struct construct__hyperperiod_as_release_idx<args_3BB(I, _, _, R(construct__hyperperiod_as_release_idx__owner::FIRST_NODE))>
        {
        private:
          static constexpr construct__hyperperiod<args_2BB(I, _, _)> result {};

        public:
          static constexpr v1::array::Idx value {
            static_cast<v1::array::Idx>(result.value / arg__Array__of__double__node_period_db::elems[result.first_node_id])
          };
        };

        template<prms_3BB(I, _, _, X)>
        struct construct__hyperperiod_as_release_idx<args_3BB(I, _, _, R(construct__hyperperiod_as_release_idx__owner::LAST_NODE))>
        {
        private:
          static constexpr construct__hyperperiod<args_2BB(I, _, _)> result {};

        public:
          static constexpr v1::array::Idx value {
            static_cast<v1::array::Idx>(result.value / arg__Array__of__double__node_period_db::elems[result.last_node_id])
          };
        };

        

        template<prms_2U(I, _, _,
                         RR(prms_1M(, R(v1::dict::Key arg__prod_node_id, v1::dict::Key arg__cons_node_id, v1::dict::Key... args__node_id))))>
        struct construct__ete_delay<args_2U(I, _, _, R(construct__path<args_1M(I, R(arg__prod_node_id, arg__cons_node_id, args__node_id...))>))> :
          construct__ete_delay__reading_timeset<args_4UA(I, _, _,
                                                         R(construct__path<args_1M(I, R(arg__prod_node_id, arg__cons_node_id, args__node_id...))>),
                                                         R(construct__reading_timeset<args_2BC(I, _, R(arg__prod_node_id), R(arg__cons_node_id),
                                                                                               R(arg__prod_release_idx + 1))>), X)> {
        };

        

        template<prms_4UA(I, _, _, _, _, _)>
        struct construct__ete_delay__reading_timeset
        {
          static constexpr double value {utility::fp::positive_infinity::value};
        };

#define prms_4UA_1(I, _X1_1) prms_4UA(I, _, _, _X1_1, _, X)
#define args_4UA_1(I, _X1_1) args_4UA(I, _, _, _X1_1, _, R(false))

        template<prms_4UA_1(I, RR(prms_1M(, R(v1::dict::Key arg__prod_node_id, v1::dict::Key arg__actuator_node_id))))>
        struct construct__ete_delay__reading_timeset<args_4UA_1(I, R(construct__path<args_1M(I, R(arg__prod_node_id, arg__actuator_node_id))>))>
        {
          static constexpr double value {((arg__construct__reading_timeset::release_idx_begin + 1)
                                          * arg__Array__of__double__node_period_db::elems[arg__actuator_node_id]
                                          - arg__prod_release_idx * arg__Array__of__double__node_period_db::elems[arg__prod_node_id])};
        };

        template<prms_4UA_1(I, RR(prms_1M(, R(v1::dict::Key arg__prod_node_id, v1::dict::Key arg__cons_node_id, v1::dict::Key... args__node_id))))>
        struct construct__ete_delay__reading_timeset<args_4UA_1(I, R(construct__path<args_1M(I, R(arg__prod_node_id, arg__cons_node_id,
                                                                                                  args__node_id...))>))> :
          construct__ete_delay__iterate_reading_timeset<args_4UB(I, _, _, R(construct__path<args_1M(I, R(arg__cons_node_id, args__node_id...))>),
                                                                 _, R(arg__prod_node_id), R(arg__cons_node_id), X, X, X, X, X)> {
        };

        

#define prms_4UB_1(I, _1, _1P_1, _1Ab_1, _2, _3) prms_4UB(I, _, _, _, _, _, _, _1, _1P_1, _1Ab_1, _2, _3)
#define args_4UB_1(I, _1, _1P_1, _1Ab_1, _2, _3) args_4UB(I, _, _, _, _, _, _, _1, _1P_1, _1Ab_1, _2, _3)

#define prms_4UB_2A(I, _1Ab_1, _3) prms_4UB_1(I, _, _, _1Ab_1, _, _3)
#define args_4UB_2A(I, _1Ab_1, _3) args_4UB_1(I, _, _, _1Ab_1, _, _3)

#define prms_4UB_2B(I, _1) prms_4UB_1(I, _1, X, X, X, X)
#define args_4UB_2B(I, _1) args_4UB_1(I, _1, R(arg__i + 1), X, X, X)

        template<prms_4UB_2A(I, X, X)>
        struct construct__ete_delay__iterate_reading_timeset<args_4UB_2A(I, R(false), R(false))> :
          construct__ete_delay__iterate_reading_timeset<args_4UB_2B(I, _)> {
        };

        template<prms_4UB_2A(I, X, X)>
        struct construct__ete_delay__iterate_reading_timeset<args_4UB_2A(I, R(false), R(true))> :
          construct__ete_delay__iterate_reading_timeset<args_4UB_2B(I, R(arg__construct__ete_delay__at_reading_point))> {
        };

        template<prms_4UB_2A(I, X, X)>
        struct construct__ete_delay__iterate_reading_timeset<args_4UB_2A(I, R(true), R(false))> :
          arg__min_ete_delay {
        };

        template<prms_4UB_2A(I, X, X)>
        struct construct__ete_delay__iterate_reading_timeset<args_4UB_2A(I, R(true), R(true))> :
          arg__construct__ete_delay__at_reading_point {
        };

        

        template<prms_3T(I, _, _, _, _, _, _)>
        struct construct__ete_delay__at_reading_point
        {
#define ete_delay construct__ete_delay<args_2U(I, R(arg__cons_release_idx), _, _)>

          static constexpr double value {(ete_delay::value == utility::fp::positive_infinity::value
                                          ? utility::fp::positive_infinity::value
                                          : (arg__cons_release_idx * arg__Array__of__double__node_period_db::elems[arg__cons_node_id]
                                             + ete_delay::value
                                             - arg__prod_release_idx * arg__Array__of__double__node_period_db::elems[arg__prod_node_id]))};
#undef ete_delay
        };

        

        template<prms_2BC(I, _, _, _, _)>
        struct construct__reading_timeset {
          static constexpr v1::array::Idx release_idx_begin {
            utility::fp::ceil<I, true>(arg__prod_release_idx
                                       * arg__Array__of__double__node_period_db::elems[arg__prod_node_id]
                                       / arg__Array__of__double__node_period_db::elems[arg__cons_node_id])
          };
          static constexpr v1::array::Idx release_idx_end {
            utility::fp::ceil<I, true>((arg__prod_release_idx + 1)
                                       * arg__Array__of__double__node_period_db::elems[arg__prod_node_id]
                                       / arg__Array__of__double__node_period_db::elems[arg__cons_node_id])
          };
        };

        

        template<prms_7BAAA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                            _, _, _)>
        struct construct__front_end__parse_correlations :
          construct__front_end__done<args_3IA(I, _, _, R(arg__correlation_follower_checker_msg))> {
        };

        template<prms_7BAAA(I, _, R(typename arg__correlation_arg_1, typename arg__correlation_arg_2,
                                    typename... args__correlation_arg, typename... args),
                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__front_end__parse_correlations<args_7BAAA(I, _, R(tuple::construct<I, Correlation<H(arg__correlation_arg_1,
                                                                                                             arg__correlation_arg_2,
                                                                                                             args__correlation_arg...)>, args...>),
                                                                   _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                   _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          utility::run_metaprogram<I, correlation::construct<I, arg__correlation_arg_1, arg__correlation_arg_2,
                                                             tuple::construct<I, args__correlation_arg...>, 3,
                                                             arg__construct__error_pos_adaptor__correlation_msgs::template node_checker_msg,
                                                             arg__construct__error_pos_adaptor__correlation_msgs::template node_validator_msg,
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               distinct_node_checker_msg),
                                                             arg__construct__error_pos_adaptor__correlation_msgs::template arg_2_checker_msg,
                                                             arg__construct__error_pos_adaptor__correlation_msgs::template arg_2_validator_msg,
                                                             arg__construct__error_pos_adaptor__correlation_msgs::template arg_3_checker_msg,
                                                             arg__construct__error_pos_adaptor__correlation_msgs::template arg_3_den_checker_msg,
                                                             arg__construct__error_pos_adaptor__correlation_msgs::template arg_3_validator_msg,
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               arg_2_computation_checker_msg),
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               arg_2_computation_fn_ptr_checker_msg),
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               arg_2_computation_wcet_checker_msg),
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               arg_2_computation_wcet_den_checker_msg),
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               arg_2_computation_wcet_validator_msg),
                                                             arg__construct__error_pos_adaptor__correlation_msgs::template arg_2_period_checker_msg,
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               arg_2_period_den_checker_msg),
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               arg_2_period_validator_msg),
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               node_computation_checker_msg),
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               node_computation_fn_ptr_checker_msg),
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               node_computation_wcet_checker_msg),
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               node_computation_wcet_den_checker_msg),
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               node_computation_wcet_validator_msg),
                                                             arg__construct__error_pos_adaptor__correlation_msgs::template node_period_checker_msg,
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               node_period_den_checker_msg),
                                                             H(arg__construct__error_pos_adaptor__correlation_msgs::template
                                                               node_period_validator_msg)>>,
          construct__front_end__validate_correlation<args_7BAAB(I, _, R(tuple::construct<H(I, arg__correlation_arg_1, arg__correlation_arg_2,
                                                                                           args__correlation_arg...)>),
                                                                _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>,
          construct__front_end__parse_correlations<args_7BAAA(I, R(arg__pos + 1), R(tuple::construct<I, args...>), _, _, _, _, _, _, _, _, _, _, _,
                                                              _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                              X)> {
        };

        

        template<prms_2Z(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__error_pos_adaptor__correlation_msgs
        {
          template<bool arg__is_Node_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_>
          using node_checker_msg = arg__correlation_node_checker_msg<arg__is_Node_, arg__pos, arg__element_pos_, arg__actual_parameter_>;

          template<bool arg__is_sensor_node_, v1::error::Pos arg__element_pos_, typename arg__node_type_>
          using node_validator_msg = arg__correlation_node_validator_msg<arg__is_sensor_node_, arg__pos, arg__element_pos_, arg__node_type_>;

          template<bool arg__are_distinct_, v1::error::Pos arg__node_1_pos_, v1::error::Pos arg__node_2_pos_>
          using distinct_node_checker_msg = arg__correlation_distinct_node_checker_msg<arg__are_distinct_, arg__pos,
                                                                                       arg__node_1_pos_, arg__node_2_pos_>;

          template<bool arg__is_Node_, typename arg__actual_parameter_>
          using arg_2_checker_msg = arg__correlation_2_checker_msg<arg__is_Node_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_actuator_node_, typename arg__node_type_>
          using arg_2_validator_msg = arg__correlation_2_validator_msg<arg__is_actuator_node_, arg__pos, arg__node_type_>;

          template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
          using arg_3_checker_msg = arg__correlation_3_checker_msg<arg__is_std_ratio_, arg__pos, arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
          using arg_3_den_checker_msg = arg__correlation_3_den_checker_msg<arg__has_nonzero_denominator_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_nonnegative_, typename arg__actual_parameter_>
          using arg_3_validator_msg = arg__correlation_3_validator_msg<arg__is_nonnegative_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_comp_Unit_, typename arg__actual_parameter_>
          using arg_2_computation_checker_msg = arg__correlation_2_computation_checker_msg<arg__is_comp_Unit_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_function_pointer_, typename arg__actual_parameter_>
          using arg_2_computation_fn_ptr_checker_msg = arg__correlation_2_computation_fn_ptr_checker_msg<arg__is_function_pointer_, arg__pos,
                                                                                                         arg__actual_parameter_>;

          template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
          using arg_2_computation_wcet_checker_msg = arg__correlation_2_computation_wcet_checker_msg<arg__is_std_ratio_, arg__pos,
                                                                                                     arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
          using arg_2_computation_wcet_den_checker_msg = arg__correlation_2_computation_wcet_den_checker_msg<arg__has_nonzero_denominator_,
                                                                                                             arg__pos, arg__actual_parameter_>;

          template<bool arg__is_positive_, typename arg__wcet_>
          using arg_2_computation_wcet_validator_msg = arg__correlation_2_computation_wcet_validator_msg<arg__is_positive_, arg__pos, arg__wcet_>;

          template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
          using arg_2_period_checker_msg = arg__correlation_2_period_checker_msg<arg__is_std_ratio_, arg__pos, arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
          using arg_2_period_den_checker_msg = arg__correlation_2_period_den_checker_msg<arg__has_nonzero_denominator_, arg__pos,
                                                                                         arg__actual_parameter_>;

          template<bool arg__is_greater_than_or_equal_, typename arg__period_, typename arg__wcet_>
          using arg_2_period_validator_msg = arg__correlation_2_period_validator_msg<arg__is_greater_than_or_equal_, arg__pos,
                                                                                     arg__period_, arg__wcet_>;

          template<bool arg__is_comp_Unit_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_>
          using node_computation_checker_msg = arg__correlation_node_computation_checker_msg<arg__is_comp_Unit_, arg__pos, arg__element_pos_,
                                                                                             arg__actual_parameter_>;

          template<bool arg__is_function_pointer_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_>
          using node_computation_fn_ptr_checker_msg = arg__correlation_node_computation_fn_ptr_checker_msg<arg__is_function_pointer_, arg__pos,
                                                                                                           arg__element_pos_,
                                                                                                           arg__actual_parameter_>;

          template<bool arg__is_std_ratio_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_>
          using node_computation_wcet_checker_msg = arg__correlation_node_computation_wcet_checker_msg<arg__is_std_ratio_, arg__pos,
                                                                                                       arg__element_pos_,
                                                                                                       arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_>
          using node_computation_wcet_den_checker_msg = arg__correlation_node_computation_wcet_den_checker_msg<arg__has_nonzero_denominator_,
                                                                                                               arg__pos, arg__element_pos_,
                                                                                                               arg__actual_parameter_>;

          template<bool arg__is_positive_, v1::error::Pos arg__element_pos_, typename arg__wcet_>
          using node_computation_wcet_validator_msg = arg__correlation_node_computation_wcet_validator_msg<arg__is_positive_, arg__pos,
                                                                                                           arg__element_pos_, arg__wcet_>;

          template<bool arg__is_std_ratio_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_>
          using node_period_checker_msg = arg__correlation_node_period_checker_msg<arg__is_std_ratio_, arg__pos, arg__element_pos_,
                                                                                   arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_>
          using node_period_den_checker_msg = arg__correlation_node_period_den_checker_msg<arg__has_nonzero_denominator_, arg__pos,
                                                                                           arg__element_pos_, arg__actual_parameter_>;

          template<bool arg__is_greater_than_or_equal_, v1::error::Pos arg__element_pos_, typename arg__period_, typename arg__wcet_>
          using node_period_validator_msg = arg__correlation_node_period_validator_msg<arg__is_greater_than_or_equal_, arg__pos, arg__element_pos_,
                                                                                       arg__period_, arg__wcet_>;
        };

        

#define prms_7BAAB_1(I, _2I_1) prms_7BAAB(I, _, _2I_1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)
#define args_7BAAB_1(I, _2I_1) args_7BAAB(I, _, _2I_1, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)

        template<prms_7BAAB_1(I, R(typename arg__Node__actuator, typename arg__Ratio__threshold, typename arg__Node__sensor,
                                   typename... args__Node__sensor))>
        struct construct__front_end__validate_correlation<args_7BAAB_1(I, R(tuple::construct<H(I, arg__Node__actuator, arg__Ratio__threshold,
                                                                                               arg__Node__sensor, args__Node__sensor...)>))> :
          construct__front_end__validate_correlation<args_7BAAB_1(I,
                                                                  R(tuple::construct<I,
                                                                    H(typename
                                                                      get_node_id<H(I, arg__pos, 1,
                                                                                    arg__tuple_construct__of__dict_Entry__of__Node__node_dict,
                                                                                    arg__Node__actuator,
                                                                                    arg__specified_node_checker_msg)>::value,
                                                                      arg__Ratio__threshold, tuple::construct<I>,
                                                                      arg__Node__sensor, args__Node__sensor...)>))> {
        };

        template<prms_7BAAB_1(I, R(typename arg__dict_Entry_key__actuator_node_id, typename arg__Ratio__threshold, typename arg__Node__sensor,
                                   typename... args__Node__sensor, typename... args__dict_Entry_key__sensor_node_id))>
        struct construct__front_end__validate_correlation<
          H(args_7BAAB_1(I, R(tuple::construct<H(I, arg__dict_Entry_key__actuator_node_id, arg__Ratio__threshold,
                                                 tuple::construct<I, args__dict_Entry_key__sensor_node_id...>,
                                                 arg__Node__sensor, args__Node__sensor...)>)))> :
#define sensor_node_pos (3 + sizeof...(args__dict_Entry_key__sensor_node_id))
#define sensor_node_id get_node_id<H(I, arg__pos, sensor_node_pos,                                                      \
                                     arg__tuple_construct__of__dict_Entry__of__Node__node_dict, arg__Node__sensor,      \
                                     arg__specified_node_checker_msg)>::value
          arg__sensor_actuator_connection_checker_msg<
          H(arg__producer_and_sink_are_connected<I,
            H(utility::run_metaprogram<I,
              H(arg__sensor_actuator_connection_checker_msg<H(!arg__Array__of__bool__is_isolated_node::elems[sensor_node_id::value],
                                                              arg__pos, sensor_node_pos, 1)>,
                typename sensor_node_id)>::value::value,
              arg__dict_Entry_key__actuator_node_id::value,
              construct__error_pos_adaptor__producer_and_sink_are_connected_msgs<args_2K(I, R(arg__pos), R(sensor_node_pos),
                                                                                         _, _)>::template producer_node_id_checker_msg,
              construct__error_pos_adaptor__producer_and_sink_are_connected_msgs<args_2K(I, R(arg__pos), R(1),
                                                                                         _, _)>::template sink_node_id_checker_msg>::value),
            arg__pos, sensor_node_pos, 1)>,
          construct__front_end__validate_correlation<args_7BAAB_1(I,
                                                                  R(tuple::construct<H(I,
                                                                                       arg__dict_Entry_key__actuator_node_id, arg__Ratio__threshold,
                                                                                       tuple::construct<H(I,
                                                                                                          args__dict_Entry_key__sensor_node_id...,
                                                                                                          typename sensor_node_id)>,
                                                                                       args__Node__sensor...)>))> {
        };
#undef sensor_node_pos
#undef sensor_node_id

        template<prms_7BAAB_1(I, R(typename arg__dict_Entry_key__actuator_node_id, typename arg__Ratio__threshold,
                                   typename... args__dict_Entry_key__sensor_node_id))>
        struct construct__front_end__validate_correlation<
          H(args_7BAAB_1(I, R(tuple::construct<H(I, arg__dict_Entry_key__actuator_node_id, arg__Ratio__threshold,
                                                 tuple::construct<I, args__dict_Entry_key__sensor_node_id...>)>)))> :
          construct__two_paths<args_4NB(I, R(tuple::construct<I, args__dict_Entry_key__sensor_node_id...>),
                                        R(arg__dict_Entry_key__actuator_node_id::value),
                                        _, _, _, _, _, _, _, _, _, R(utility::fp::Double<I, arg__Ratio__threshold>))> {
        };

        

        template<prms_4NB(I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__two_paths {
        };

        template<prms_4NB(I, R(typename arg__sensor_node_id, typename... args__sensor_node_id), _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__two_paths<args_4NB(I, R(tuple::construct<I, arg__sensor_node_id, args__sensor_node_id...>),
                                             _, _, _, _, _, _, _, _, _, _, _)> :
          construct__two_paths__first_path<args_5NAA(I, R(tuple::construct<I, construct__branching_node<args_2TB(I, R(args__sensor_node_id::value),
                                                                                                                 X, X)>...>),
                                                     _, _, _, R(arg__sensor_node_id::value), _, _, _, _, _, _, _, _, X, X, X, X, X, X, X)>,
          construct__two_paths<args_4NB(I, R(tuple::construct<I, args__sensor_node_id...>), _, _, _, _, _, _, _, _, _, _, _)> {
        };

        

        //\begin{depth-first traversal has visited all possible end-to-end paths}
        template<prms_5NAA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__two_paths__first_path {
        };
        //\end{depth-first traversal has visited all possible end-to-end paths}
        //\begin{depth-first traversal has not visited all possible end-to-end paths}
#define prms_5NAA_1(I, _1T_1, _X1_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)                                     \
        prms_5NAA(I, _, _, _, _, _1T_1, _, _, _, _, _, _, _, _, _X1_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)
#define args_5NAA_1(I, _1T_1, _X1_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)                                     \
        args_5NAA(I, _, _, _, _, _1T_1, _, _, _, _, _, _, _, _, _X1_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)

#define prms_5NAA_2A(I, _2Ab_2, _2Ab_4) prms_5NAA_1(I, _, RR(prms_1M(, _)), _, _, X, _2Ab_2, _, _2Ab_4)
#define args_5NAA_2A(I, _2Ab_2, _2Ab_4) args_5NAA_1(I, _, R(construct__path<args_1M(I, _)>), _, _, R(false), _2Ab_2, _, _2Ab_4)

#define prms_5NAA_2B(I, _1T_1, _X1_1, _1P_1) prms_5NAA_1(I, _1T_1, _X1_1, _1P_1, X, X, X, X, X)
#define args_5NAA_2B(I, _1T_1, _X1_1, _1P_1) args_5NAA_1(I, _1T_1, _X1_1, _1P_1, X, X, X, X, X)

        //  \begin{depth-first traversal is obtaining one end-to-end path}
        //    \begin{child under inspection and constrained sink are not connected}
        template<prms_5NAA_2A(I, X, X)>
        struct construct__two_paths__first_path<args_5NAA_2A(I, R(false), R(false))> :
          construct__two_paths__first_path<args_5NAA_2B(I, _, R(construct__path<args_1M(I, _)>), R(arg__i + 1))> { // inspect next child
        };
        //    \end{child under inspection and constrained sink are not connected}
        //    \begin{child under inspection and constrained sink are connected}
        template<prms_5NAA_2A(I, X, X)>
        struct construct__two_paths__first_path<args_5NAA_2A(I, R(false), R(true))> :
          construct__two_paths__first_path__push_to_backtrack_stack_as_needed<args_5NAB(I, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                                        R(construct__path<args_1M(I, R(args__node_id...,
                                                                                                                       arg__prod_node_id))>), _,
                                                                                        R(false), X)>, // visit this child
          construct__two_paths__first_path<args_5NAA_2B(I, _, R(construct__path<args_1M(I, _)>), R(arg__i + 1))> { // inspect next child
        };
        //    \end{child under inspection and constrained sink are connected}
        //  \end{depth-first traversal is obtaining one end-to-end path}
        //  \begin{depth-first traversal has obtained one end-to-end path}
        template<prms_5NAA_2A(I, X, _)>
        struct construct__two_paths__first_path<args_5NAA_2A(I, R(true), _)> :
          construct__two_paths__first_path__push_to_backtrack_stack_as_needed<args_5NAB(I, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                                        R(construct__path<args_1M(I, R(args__node_id...,
                                                                                                                       arg__prod_node_id,
                                                                                                                       arg__actuator_node_id))>), _,
                                                                                        R(true), X)>, // process this path
          construct__two_paths__first_path<args_5NAA_2B(I, _, R(construct__path<args_1M(I, _)>), R(arg__i + 1))> { // inspect next child
        };
        //  \end{depth-first traversal has obtained one end-to-end path}
        //\end{depth-first traversal has not visited all possible end-to-end paths}

        

        template<prms_5NAB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, X, X)>
        struct construct__two_paths__first_path__push_to_backtrack_stack_as_needed<args_5NAB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                                             R(false), R(false))> :
          construct__two_paths__first_path<args_5NAA(I, _, _, _, _,
                                                     R(arg__Array__of__Array__of__dict_Key__adjacency_list::elems[arg__prod_node_id][arg__i]),
                                                     _, _, _, _, _, _, _, _, _, X, X, X, X, X, X)> {
        };

        template<prms_5NAB(I, R(typename... args__branching_node), _, _, _, _, _, _, _, _, _, _, _, _, RR(prms_1M(, _)), _, X, X)>
        struct construct__two_paths__first_path__push_to_backtrack_stack_as_needed<args_5NAB(I, R(tuple::construct<I, args__branching_node...>),
                                                                                             _, _, _, _, _, _, _, _, _, _, _, _,
                                                                                             R(construct__path<args_1M(I, _)>), _,
                                                                                             R(false), R(true))> :
          construct__two_paths__first_path<args_5NAA(I,
                                                     R(tuple::construct<H(I, construct__branching_node<args_2TB(I, _, R(arg__i + 1),
                                                                                                                R(sizeof...(args__node_id) - 1))>,
                                                                          args__branching_node...)>), _, _, _,
                                                     R(arg__Array__of__Array__of__dict_Key__adjacency_list::elems[arg__prod_node_id][arg__i]),
                                                     _, _, _, _, _, _, _, _, R(construct__path<args_1M(I, _)>), X, X, X, X, X, X)> {
        };

        template<prms_5NAB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, X, X)>
        struct construct__two_paths__first_path__push_to_backtrack_stack_as_needed<args_5NAB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                                             R(true), R(false))> :
          construct__two_paths__backtrack<args_2Ae(I, _, _, _, _, _, _, _, _, _, _, _, _, R(arg__construct__path))> {
        };

        template<prms_5NAB(I, R(typename... args__branching_node), _, _, _, _, _, _, _, _, _, _, _, _, RR(prms_1M(, _)), _, X, X)>
        struct construct__two_paths__first_path__push_to_backtrack_stack_as_needed<args_5NAB(I, R(tuple::construct<I, args__branching_node...>),
                                                                                             _, _, _, _, _, _, _, _, _, _, _, _,
                                                                                             R(construct__path<args_1M(I, _)>), _,
                                                                                             R(true), R(true))> :
          construct__two_paths__backtrack<args_2Ae(I, _, _, _, _, _, _, _, _, _, _, _,
                                                   R(tuple::construct<H(I, construct__branching_node<args_2TB(I, _, R(arg__i + 1),
                                                                                                              R(sizeof...(args__node_id) - 2))>,
                                                                        args__branching_node...)>), R(construct__path<args_1M(I, _)>))> {
        };

        

        template<prms_2Ae(I, _, _, _, _, _, _, _, _, _, _, _, X, _)>
        struct construct__two_paths__backtrack<args_2Ae(I, _, _, _, _, _, _, _, _, _, _, _, R(tuple::construct<I>), _)> {
        };

        template<prms_2Ae(I, _, _, _, _, _, _, _, _, _, _, _, RR(prms_2TB(, _, _, _), typename... args__branching_node), _)>
        struct construct__two_paths__backtrack<args_2Ae(I, _, _, _, _, _, _, _, _, _, _, _,
                                                        R(tuple::construct<H(I, construct__branching_node<args_2TB(I, _, _, _)>,
                                                                             args__branching_node...)>), _)> :
          construct__two_paths__second_path<args_4Ab(I, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                     R(typename construct__backtracked_first_path<args_2Ag(I, _, _, X, X, X)>::value),
                                                     R(arg__child_to_take), X, X, X, X, X)>,
          construct__two_paths__backtrack<args_2Ae(I, _, _, _, _, _, _, _, _, _, _, _, R(tuple::construct<I, args__branching_node...>), _)> {
        };

        

        //\begin{depth-first traversal has visited all possible end-to-end paths}
        template<prms_4Ab(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__two_paths__second_path {
        };
        //\end{depth-first traversal has visited all possible end-to-end paths}
        //\begin{depth-first traversal has not visited all possible end-to-end paths}
#define prms_4Ab_1(I, _1T_1, _X1_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)                                                              \
        prms_4Ab(I, RR(prms_1M(, R(v1::dict::Key... args__node_id_1))), _, _, _, _1T_1, _, _, _, _, _, _, _, _, _X1_1, _1P_1, _2Ab_1, _1Ab_1,   \
                 _2Ab_2, _2Ab_3, _2Ab_4)
#define args_4Ab_1(I, _1T_1, _X1_1, _1P_1, _2Ab_1, _1Ab_1, _2Ab_2, _2Ab_3, _2Ab_4)                                                                 \
        args_4Ab(I, R(construct__path<args_1M(I, R(args__node_id_1...))>), _, _, _, _1T_1, _, _, _, _, _, _, _, _, _X1_1, _1P_1, _2Ab_1, _1Ab_1,   \
                 _2Ab_2, _2Ab_3, _2Ab_4)

#define prms_4Ab_2A(I, _2Ab_2, _2Ab_4) prms_4Ab_1(I, _, RR(prms_1M(, _)), _, _, X, _2Ab_2, _, _2Ab_4)
#define args_4Ab_2A(I, _2Ab_2, _2Ab_4) args_4Ab_1(I, _, R(construct__path<args_1M(I, _)>), _, _, R(false), _2Ab_2, _, _2Ab_4)

#define prms_4Ab_2B(I, _1T_1, _X1_1, _1P_1) prms_4Ab_1(I, _1T_1, _X1_1, _1P_1, X, X, X, X, X)
#define args_4Ab_2B(I, _1T_1, _X1_1, _1P_1) args_4Ab_1(I, _1T_1, _X1_1, _1P_1, X, X, X, X, X)

        //  \begin{depth-first traversal is obtaining one end-to-end path}
        //    \begin{child under inspection and constrained sink are not connected}
        template<prms_4Ab_2A(I, X, X)>
        struct construct__two_paths__second_path<args_4Ab_2A(I, R(false), R(false))> :
          construct__two_paths__second_path<args_4Ab_2B(I, _, R(construct__path<args_1M(I, _)>), R(arg__i + 1))> { // inspect next child
        };
        //    \end{child under inspection and constrained sink are not connected}
        //    \begin{child under inspection and constrained sink are connected}
        template<prms_4Ab_2A(I, X, X)>
        struct construct__two_paths__second_path<args_4Ab_2A(I, R(false), R(true))> :
          construct__two_paths__second_path<args_4Ab_2B(I, R(arg__Array__of__Array__of__dict_Key__adjacency_list::elems[arg__prod_node_id][arg__i]),
                                                        R(construct__path<args_1M(I, R(args__node_id..., arg__prod_node_id))>),
                                                        X)>, // visit this child
          construct__two_paths__second_path<args_4Ab_2B(I, _, R(construct__path<args_1M(I, _)>), R(arg__i + 1))> { // inspect next child
        };
        //    \end{child under inspection and constrained sink are connected}
        //  \end{depth-first traversal is obtaining one end-to-end path}
        //  \begin{depth-first traversal has obtained one end-to-end path}
        template<prms_4Ab_2A(I, X, _)>
        struct construct__two_paths__second_path<args_4Ab_2A(I, R(true), _)> :
          construct__two_paths__iterate_confluent_nodeset<
          H(args_5QA(I, R(construct__path<args_1M(I, R(args__node_id..., arg__prod_node_id, arg__actuator_node_id))>),
                     R(array::update::value<H(I, array::make::value<H(I, arg__Array__of__Array__of__dict_Key__adjacency_list::size,
                                                                      std::integral_constant<v1::array::Size, 0>, v1::array::Size)>,
                                              typename utility::list::update<H(I, tuple::construct<I, dict_Entry_key<I, args__node_id_1>...>,
                                                                               construct__node_id_to_path_pos, void)>::value)>),
                     _, _, _, _, _, R(construct__path<args_1M(I, R(args__node_id_1...))>), X, X, X, X))>, // process this path
          construct__two_paths__second_path<args_4Ab_2B(I, _, R(construct__path<args_1M(I, _)>), R(arg__i + 1))> { // inspect next child
        };
        //  \end{depth-first traversal has obtained one end-to-end path}
        //\end{depth-first traversal has not visited all possible end-to-end paths}

        

        template<prms_1Ah(I, _, _, _)>
        struct construct__node_id_to_path_pos
        {
          static constexpr bool is_complete {false};
          typedef arg__data data;
          typedef dict_Entry_array_Size<I, arg__dict_Entry_key__node_id::value, arg__idx + 1> value;
        };

        

#define prms_5QA_1(I, _1X_1, _2Aa_1, _1Ac_1, _2, _3) prms_5QA(I, _1X_1, _, _, _, _, _, _, _, _2Aa_1, _1Ac_1, _2, _3)
#define args_5QA_1(I, _1X_1, _2Aa_1, _1Ac_1, _2, _3) args_5QA(I, _1X_1, _, _, _, _, _, _, _, _2Aa_1, _1Ac_1, _2, _3)

#define prms_5QA_2C(I, _1X_1, _2Aa_1, _2) prms_5QA_1(I, _1X_1, _2Aa_1, X, _2, X)
#define args_5QA_2C(I, _1X_1, _2Aa_1, _2) args_5QA_1(I, _1X_1, _2Aa_1,                                                          \
                                                     R(arg__Array__of__array_Size__path_1_pos::elems[arg__node_id_2] == 0       \
                                                       ? v1::dict::Invalid_key::value                                           \
                                                       : arg__node_id_2),                                                       \
                                                     _2, R(arg__Array__of__array_Size__path_1_pos::elems[arg__node_id_2] != 0))

        template<prms_5QA_1(I, RR(prms_1M(, R(v1::dict::Key arg__sensor_node_id_2, v1::dict::Key arg__node_id_2,
                                              v1::dict::Key... args__node_id_2))), RR(prms_1M(, X)), _, _, _)>
        struct construct__two_paths__iterate_confluent_nodeset<args_5QA_1(I,
                                                                          R(construct__path<args_1M(I, R(arg__sensor_node_id_2, arg__node_id_2,
                                                                                                         args__node_id_2...))>),
                                                                          R(construct__path<args_1M(I, X)>), _, _, _)> :
          construct__two_paths__iterate_confluent_nodeset<args_5QA_2C(I, R(construct__path<args_1M(I, R(args__node_id_2...))>),
                                                                      R(construct__path<args_1M(I, R(arg__sensor_node_id_2, arg__node_id_2))>),
                                                                      R(arg__Array__of__array_Size__path_1_pos::elems[arg__sensor_node_id_2]
                                                                        != 0))> {
        };

#define prms_5QA_2A(I, _2, _3) prms_5QA_1(I, RR(prms_1M(, X)), RR(prms_1M(, _)), _, _2, _3)
#define args_5QA_2A(I, _2, _3) args_5QA_1(I, R(construct__path<args_1M(I, X)>), R(construct__path<args_1M(I, _)>), _, _2, _3)

        template<prms_5QA_2A(I, _, _)>
        struct construct__two_paths__iterate_confluent_nodeset<args_5QA_2A(I, _, _)> {
        };

        template<prms_5QA_2A(I, X, X)>
        struct construct__two_paths__iterate_confluent_nodeset<args_5QA_2A(I, R(false), R(true))> :
          construct__correlation__check_threshold<args_5QB(I, _, _, _, _, _,
                                                           R(typename construct__backtracked_first_path<
                                                             H(args_2Ag(I, _, R(arg__Array__of__array_Size__path_1_pos::
                                                                                elems[arg__confluent_node_id]), X, X, X))>::value),
                                                           R(construct__path<args_1M(I, _)>), _, X, X, X, X, X, X)> {
        };

#define prms_5QA_2B(I, _2, _3) prms_5QA_1(I, RR(prms_1M(, R(v1::dict::Key arg__node_id_2, v1::dict::Key... args__node_id_2))),                  \
                                          RR(prms_1M(, R(v1::dict::Key arg__sensor_node_id, v1::dict::Key... args__node_id))), _, _2, _3)
#define args_5QA_2B(I, _2, _3) args_5QA_1(I, R(construct__path<args_1M(I, R(arg__node_id_2, args__node_id_2...))>),             \
                                          R(construct__path<args_1M(I, R(arg__sensor_node_id, args__node_id...))>), _, _2, _3)

#define prms_5QA_3C(I, _2) prms_5QA_2C(I, RR(prms_1M(, R(v1::dict::Key arg__node_id_2, v1::dict::Key... args__node_id_2))),     \
                                       RR(prms_1M(, R(v1::dict::Key arg__sensor_node_id, v1::dict::Key... args__node_id))), _2)
#define args_5QA_3C(I, _2) args_5QA_2C(I, R(construct__path<args_1M(I, R(args__node_id_2...))>),                                        \
                                       R(construct__path<args_1M(I, R(arg__sensor_node_id, args__node_id..., arg__node_id_2))>), _2)

        template<prms_5QA_2B(I, _, _)>
        struct construct__two_paths__iterate_confluent_nodeset<args_5QA_2B(I, _, _)> :
          construct__two_paths__iterate_confluent_nodeset<args_5QA_3C(I, R(arg__path_2_current_node_is_in_path_1))> {
        };

        template<prms_5QA_2B(I, X, X)>
        struct construct__two_paths__iterate_confluent_nodeset<args_5QA_2B(I, R(false), R(true))> :
          construct__correlation__check_threshold<args_5QB(I, _, _, _, _, _,
                                                           R(typename construct__backtracked_first_path<
                                                             H(args_2Ag(I, _, R(arg__Array__of__array_Size__path_1_pos::
                                                                                elems[arg__confluent_node_id]), X, X, X))>::value),
                                                           R(construct__path<args_1M(I, R(arg__sensor_node_id, args__node_id...))>), _,
                                                           X, X, X, X, X, X)>,
          construct__two_paths__iterate_confluent_nodeset<args_5QA_3C(I, R(true))> {
        };

        

        template<prms_2Ac(I, _, _, RR(prms_1M(, R(v1::dict::Key arg__sensor_node_id, v1::dict::Key... args__node_id))), _, _, X)>
        struct construct__sensing_release_domain__min<args_2Ac(I, _, _, R(construct__path<args_1M(I, R(arg__sensor_node_id, args__node_id...))>),
                                                               _, _, R(true))>
        {
          static constexpr v1::array::Idx value {
            utility::fp::round<I, true>((arg__i * arg__Array__of__double__node_period_db::elems[arg__sensor_node_id]
                                         + arg__construct__ete_delay__at_i::value)
                                        / arg__Array__of__double__node_period_db::elems[arg__confluent_node_id]) - 1
          };
        };

        template<prms_2Ac(I, _, _, _, _, _, X)>
        struct construct__sensing_release_domain__min<args_2Ac(I, _, _, _, _, _, R(false))> :
          construct__sensing_release_domain__min<args_2Ac(I, _, _, _, R(arg__i + 1), X, X)> {
        };

        

        template<prms_2Ag(I, _, _, _, _, X)>
        struct construct__backtracked_first_path<args_2Ag(I, _, _, _, _, R(true))>
        {
          typedef arg__construct__path__result value;
        };

        template<prms_2Ag(I, RR(prms_1M(, R(v1::dict::Key arg__node_id, v1::dict::Key... args__node_id))), _,
                          RR(prms_1M(, R(v1::dict::Key... args__resulting_node_id))), _, X)>
        struct construct__backtracked_first_path<args_2Ag(I, R(construct__path<args_1M(I, R(arg__node_id, args__node_id...))>), _,
                                                          R(construct__path<args_1M(I, R(args__resulting_node_id...))>), _, R(false))> :
          construct__backtracked_first_path<args_2Ag(I, R(construct__path<args_1M(I, R(args__node_id...))>), _,
                                                     R(construct__path<args_1M(I, R(args__resulting_node_id..., arg__node_id))>), R(arg__i + 1),
                                                     X)> {
        };

        

        template<prms_5QB(I, _, _, _, _, _, _, _, _, _, _, _, X, _, X)>
        struct construct__correlation__check_threshold<args_5QB(I, _, _, _, _, _, _, _, _, _, _, _, R(true), _, R(true))> {
        };

        template<prms_5QB(I, _, _, _, _, _, _, _, _, _, _, _, X, _, X)>
        struct construct__correlation__check_threshold<args_5QB(I, _, _, _, _, _, _, _, _, _, _, _, R(false), _, R(true))> :
          construct__correlation__check_threshold<args_5QB(I, _, _, _, _, _, _, _, _, _, _, R(arg__i + 1), X, X, X)> {
        };

        template<prms_5QB(I, _, _, _, _, _,
                          RR(prms_1M(, R(v1::dict::Key... args__node_id_1))), RR(prms_1M(, R(v1::dict::Key... args__node_id_2))), _, _, _, _, _, _,
                          X)>
        struct construct__correlation__check_threshold<args_5QB(I, _, _, _, _, _,
                                                                R(construct__path<args_1M(I, R(args__node_id_1...))>),
                                                                R(construct__path<args_1M(I, R(args__node_id_2...))>),
                                                                _, _, _, _, _, _, R(false))> :
          arg__correlation_threshold_checker_msg<!!utility::always_false<I>(), arg__pos,
                                                 typename utility::fp::Ratio<I, &arg__construct__correlation__at_i::value,
                                                                             std::ratio<1, 1000000>,
                                                                             utility::fp::ceil<I, true>>::value,
                                                 typename arg__utility_fp_Double__threshold::rational_value,
                                                 v1::error::Path_forming_node_pos<(args__node_id_1 + 2)...>,
                                                 v1::error::Path_forming_node_pos<(args__node_id_2 + 2)...>, 1 + arg__i> {
        };

        

        template<prms_3WB(I, _, _, RR(prms_1M(, R(v1::dict::Key arg__sensor_node_id_1, v1::dict::Key... args__node_id_1))),
                          RR(prms_1M(, R(v1::dict::Key arg__sensor_node_id_2, v1::dict::Key... args__node_id_2))), _)>
        struct construct__correlation__at_release_point<args_3WB(I, _, _,
                                                                 R(construct__path<args_1M(I, R(arg__sensor_node_id_1, args__node_id_1...))>),
                                                                 R(construct__path<args_1M(I, R(arg__sensor_node_id_2, args__node_id_2...))>), _)>
        {
          static constexpr double value {
            utility::fp::abs((construct__correlation__sensing_release_idx<args_3WA(I, _, _, _,
                                                                                   R(construct__path<args_1M(I, R(arg__sensor_node_id_1,
                                                                                                                  args__node_id_1...))>), X)>::value
                              * arg__Array__of__double__node_period_db::elems[arg__sensor_node_id_1])
                             - (construct__correlation__sensing_release_idx<args_3WA(I, _, _, _,
                                                                                     R(construct__path<args_1M(I,
                                                                                                               R(arg__sensor_node_id_2,
                                                                                                                 args__node_id_2...))>), X)>::value
                                * arg__Array__of__double__node_period_db::elems[arg__sensor_node_id_2]))
          };
        };

        

        template<prms_1V(I, RR(prms_1M(, R(v1::dict::Key... args__lhs_node_id))), RR(prms_1M(, R(v1::dict::Key... args__rhs_node_id))))>
        struct construct__two_paths_concatenated<args_1V(I, R(construct__path<args_1M(I, R(args__lhs_node_id...))>),
                                                         R(construct__path<args_1M(I, R(args__rhs_node_id...))>))>
        {
          typedef construct__path<args_1M(I, R(args__lhs_node_id..., args__rhs_node_id...))> value;
        };

        

        template<prms_3WA(I, _, _, _, _, RR(prms_1M(, R(v1::dict::Key arg__prod_node_id, v1::dict::Key arg__cons_node_id))))>
        struct construct__correlation__sensing_release_idx<args_3WA(I, _, _, _, _,
                                                                    R(construct__path<args_1M(I, R(arg__prod_node_id, arg__cons_node_id))>))>
        {
          static constexpr std::intmax_t value {(utility::fp::floor<I, true>(arg__confluent_node_release_idx
                                                                             * arg__Array__of__double__node_period_db::elems[arg__cons_node_id]
                                                                             / arg__Array__of__double__node_period_db::elems[arg__prod_node_id])
                                                 - 1)};
          run_metaprogram(arg__sensing_release_domain_checker_msg<(value >= 0), arg__confluent_node_release_idx, arg__construct__path>);
        };

        template<prms_3WA(I, _, _, _, _, RR(prms_1M(, R(v1::dict::Key arg__prod_node_id, v1::dict::Key arg__cons_node_id,
                                                        v1::dict::Key... args__node_id))))>
        struct construct__correlation__sensing_release_idx<args_3WA(I, _, _, _, _,
                                                                    R(construct__path<args_1M(I, R(arg__prod_node_id, arg__cons_node_id,
                                                                                                   args__node_id...))>))>
        {
          static constexpr std::intmax_t value {(utility::fp::floor<I, true>(construct__correlation__sensing_release_idx<
                                                                             H(args_3WA(I, _, _, _, _,
                                                                                        R(construct__path<args_1M(I, R(arg__cons_node_id,
                                                                                                                       args__node_id...))>)))
                                                                             >::value
                                                                             * arg__Array__of__double__node_period_db::elems[arg__cons_node_id]
                                                                             / arg__Array__of__double__node_period_db::elems[arg__prod_node_id])
                                                 - 1)};
          run_metaprogram(arg__sensing_release_domain_checker_msg<(value >= 0), arg__confluent_node_release_idx, arg__construct__path>);
        };

        

        template<prms_3IA(I, _, R(typename arg, typename... args), _)>
        struct construct__front_end__done<args_3IA(I, _, R(tuple::construct<I, arg, args...>), _)> :
          arg__follower_checker_msg<!!utility::always_false<I>(), arg__pos, arg> {
        };

        template<prms_3IA(I, _, X, _)>
        struct construct__front_end__done<args_3IA(I, _, R(tuple::construct<I>), _)> :
          parser_remaining_args<I> {
        };

        

        template<prms_4CA(I, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__back_end :
          utility::run_metaprogram<I, arg__HW__hw_desc>,
          construct__back_end__generate<args_4CB(I, _, _, _, _, _, _, _, _, _, _)>
        {
          typedef arg__HW__hw_desc hw_desc;
        };

        

        template<prms_4CB(I, _, _, _, _, _, _, _, _, _, _)>
        struct construct__back_end__generate :
          arg__backend_availability_checker_msg<!!utility::always_false<I>(), arg__HW__hw_desc> {
        };

        template<prms_4CB(I, X, _, _, _, _, _, _, _, _, _)>
        struct construct__back_end__generate<args_4CB(I, R(HW<Core_ids<>>), _, _, _, _, _, _, _, _, _)>
        {
#ifndef TICE_V1_NOGEN
          typedef code::empty::setup code_setup;
#endif // TICE_V1_NOGEN
        };

        template<prms_4CB(I, R(int arg__core_id, int... args__core_id), _, _, _, _, _, _, _, _, _)>
        struct construct__back_end__generate<args_4CB(I, R(HW<Core_ids<arg__core_id, args__core_id...>>), _, _, _, _, _, _, _, _, _)> :
          utility::run_metaprogram<I, construct__schedulability_test__gedf<args_2AiA(I, _, _, R(1 + sizeof...(args__core_id)), _)>>
        {
#ifndef TICE_V1_NOGEN
          typedef code::gedf::setup<Core_ids<arg__core_id, args__core_id...>, arg__tuple_construct__of__dict_Entry__of__Node__node_dict,
                                    arg__tuple_construct__of__Channel__channel_list,
                                    arg__tuple_construct__of__pair_construct__of__tuple_dict_Entry_array_idx__and__Array_idx_list__channel_idx_list>
          code_setup;
#endif // TICE_V1_NOGEN
        };

        

        template<prms_2AiA(I, _, _, _, _)>
        struct construct__schedulability_test__gedf
        {
        private:
          struct test_result {
            bool schedulable;
            double total_utilization;
            double total_utilization_bound;
            double max_utilization;
            double max_utilization_bound;
          };
          static constexpr test_result compute_sufficient_condition() {
            test_result result = {H(false,
                                    0, H(arg__core_count * arg__core_count
                                         / (2.0 * arg__core_count - 1)),
                                    0, H(arg__core_count
                                         / (2.0 * arg__core_count - 1)))};

            for (v1::array::Idx i = 0; i < arg__Array__of__double__node_wcet_db::size; ++i) {
              double task_utilization = arg__Array__of__double__node_wcet_db::elems[i] / arg__Array__of__double__node_period_db::elems[i];
              if (result.max_utilization < task_utilization) {
                result.max_utilization = task_utilization;
              }
              result.total_utilization += task_utilization;
            }

            result.schedulable = (result.total_utilization <= result.total_utilization_bound
                                  && result.max_utilization <= result.max_utilization_bound);

            return result;
          }

          static constexpr test_result result {compute_sufficient_condition()};
          static constexpr double total_utilization {result.total_utilization};
          static constexpr double total_utilization_bound {result.total_utilization_bound};
          static constexpr double max_utilization {result.max_utilization};
          static constexpr double max_utilization_bound {result.max_utilization_bound};

          run_metaprogram(arg__gedf_schedulability_checker_msg<H(result.schedulable,
                                                                 typename utility::fp::Ratio<H(I, &total_utilization,
                                                                                               std::ratio<1, 1000000>,
                                                                                               utility::fp::ceil<I, true>)>::value,
                                                                 typename utility::fp::Ratio<H(I, &total_utilization_bound,
                                                                                               std::ratio<1, 1000000>,
                                                                                               utility::fp::ceil<I, true>)>::value,
                                                                 typename utility::fp::Ratio<H(I, &max_utilization,
                                                                                               std::ratio<1, 1000000>,
                                                                                               utility::fp::ceil<I, true>)>::value,
                                                                 typename utility::fp::Ratio<H(I, &max_utilization_bound,
                                                                                               std::ratio<1, 1000000>,
                                                                                               utility::fp::ceil<I, true>)>::value)>);
        };

#include "v1_internals_end.hpp"
      }
    }
  }
}

namespace tice {
  namespace v1 {

    template<typename arg__HW__hw_desc, typename arg__Node__node, typename... args__Node__then__Feeder__then__ETE_delay__then__Correlation>
    class Program :
      public internals::program::construct<I, arg__HW__hw_desc, arg__Node__node,
                                           internals::tuple::construct<I, args__Node__then__Feeder__then__ETE_delay__then__Correlation...>,
                                           error::program::arg_1_is_HW,
                                           error::program::arg_1_arg_1_is_Core_ids,
                                           error::program::arg_1_arg_1_core_ids_are_not_duplicated,
                                           error::program::arg_2_is_Node,
                                           error::program::node_arg_1_is_Comp,
                                           error::program::node_arg_1_arg_1_is_function_pointer,
                                           error::program::node_arg_1_arg_2_is_Ratio,
                                           error::program::node_arg_1_arg_2_has_nonzero_denominator,
                                           error::program::node_arg_1_arg_2_is_positive,
                                           error::program::node_arg_2_is_Ratio,
                                           error::program::node_arg_2_has_nonzero_denominator,
                                           error::program::node_arg_2_is_greater_than_or_equal_to_node_arg_1_arg_2,
                                           error::program::nodes_are_distinct,
                                           error::program::sequence_of_Node_is_followed_only_by_sequence_of_Feeder,
                                           error::program::graph_has_no_cycle,
                                           error::program::graph_has_no_multiple_edges,
                                           error::program::graph_has_no_loop,
                                           error::program::feeder_node_arg_is_missing,
                                           error::program::feeder_arg_is_producer_node,
                                           error::program::feeder_arg_is_consumer_node,
                                           error::program::feeder_channel_and_consumer_parameter_counts_are_equal,
                                           error::program::feeder_consumer_function_is_callable,
                                           error::program::feeder_channel_arg_is_missing,
                                           error::program::feeder_arg_is_channel,
                                           error::program::feeder_channel_arg_1_is_object_type,
                                           error::program::feeder_channel_arg_1_is_cv_unqualified,
                                           error::program::feeder_channel_arg_2_is_not_nullptr,
                                           error::program::feeder_producer_return_value_is_assignable_to_channel,
                                           error::program::feeder_init_val_can_initialize_channel,
                                           error::program::feeder_channel_and_consumer_parameter_are_compatible,
                                           error::program::feeder_node_arg_1_is_Comp,
                                           error::program::feeder_node_arg_1_arg_1_is_function_pointer,
                                           error::program::feeder_node_arg_1_arg_2_is_Ratio,
                                           error::program::feeder_node_arg_1_arg_2_has_nonzero_denominator,
                                           error::program::feeder_node_arg_1_arg_2_is_positive,
                                           error::program::feeder_node_arg_2_is_Ratio,
                                           error::program::feeder_node_arg_2_has_nonzero_denominator,
                                           error::program::feeder_node_arg_2_is_greater_than_or_equal_to_feeder_node_arg_1_arg_2,
                                           error::program::feeders_are_distinct,
                                           error::program::isolated_node_is_sensor_node,
                                           error::program::source_node_is_sensor_node,
                                           error::program::sink_node_is_actuator_node,
                                           error::program::node_is_already_specified,
                                           error::program::sensor_and_actuator_nodes_are_connected,
                                           internals::error::program::construct::confluent_node_release_idx_is_in_sensing_release_domain,
                                           error::program::correlation_is_within_threshold,
                                           error::program::sequence_of_Correlation_is_followed_by_nothing,
                                           error::program::sequence_of_ETE_delay_is_followed_only_by_sequence_of_Correlation,
                                           error::program::sequence_of_Feeder_is_followed_only_by_sequence_of_ETE_delay_or_Correlation,
                                           error::program::end_to_end_delay_is_between_min_and_max_delays,
                                           internals::error::program::construct::node_id_is_producer_node_id,
                                           internals::error::program::construct::node_id_is_sink_node_id,
                                           error::program::ete_delay_arg_3_is_less_than_or_equal_to_ete_delay_arg_4,
                                           error::program::ete_delay_arg_1_is_Node,
                                           error::program::ete_delay_arg_1_is_sensor_node,
                                           error::program::ete_delay_arg_2_is_Node,
                                           error::program::ete_delay_arg_2_is_actuator_node,
                                           error::program::ete_delay_arg_3_is_Ratio,
                                           error::program::ete_delay_arg_3_has_nonzero_denominator,
                                           error::program::ete_delay_arg_3_is_nonnegative,
                                           error::program::ete_delay_arg_4_is_Ratio,
                                           error::program::ete_delay_arg_4_has_nonzero_denominator,
                                           error::program::ete_delay_arg_4_is_positive,
                                           error::program::ete_delay_arg_1_arg_1_is_Comp,
                                           error::program::ete_delay_arg_1_arg_1_arg_1_is_function_pointer,
                                           error::program::ete_delay_arg_1_arg_1_arg_2_is_Ratio,
                                           error::program::ete_delay_arg_1_arg_1_arg_2_has_nonzero_denominator,
                                           error::program::ete_delay_arg_1_arg_1_arg_2_is_positive,
                                           error::program::ete_delay_arg_1_arg_2_is_Ratio,
                                           error::program::ete_delay_arg_1_arg_2_has_nonzero_denominator,
                                           error::program::ete_delay_arg_1_arg_2_is_greater_than_or_equal_to_ete_delay_arg_1_arg_1_arg_2,
                                           error::program::ete_delay_arg_2_arg_1_is_Comp,
                                           error::program::ete_delay_arg_2_arg_1_arg_1_is_function_pointer,
                                           error::program::ete_delay_arg_2_arg_1_arg_2_is_Ratio,
                                           error::program::ete_delay_arg_2_arg_1_arg_2_has_nonzero_denominator,
                                           error::program::ete_delay_arg_2_arg_1_arg_2_is_positive,
                                           error::program::ete_delay_arg_2_arg_2_is_Ratio,
                                           error::program::ete_delay_arg_2_arg_2_has_nonzero_denominator,
                                           error::program::ete_delay_arg_2_arg_2_is_greater_than_or_equal_to_ete_delay_arg_2_arg_1_arg_2,
                                           error::program::correlation_remaining_arg_is_Node,
                                           error::program::correlation_remaining_arg_is_sensor_node,
                                           error::program::correlation_nodes_are_distinct,
                                           error::program::correlation_arg_1_is_Node,
                                           error::program::correlation_arg_1_is_actuator_node,
                                           error::program::correlation_arg_2_is_Ratio,
                                           error::program::correlation_arg_2_has_nonzero_denominator,
                                           error::program::correlation_arg_2_is_nonnegative,
                                           error::program::correlation_arg_1_arg_1_is_Comp,
                                           error::program::correlation_arg_1_arg_1_arg_1_is_function_pointer,
                                           error::program::correlation_arg_1_arg_1_arg_2_is_Ratio,
                                           error::program::correlation_arg_1_arg_1_arg_2_has_nonzero_denominator,
                                           error::program::correlation_arg_1_arg_1_arg_2_is_positive,
                                           error::program::correlation_arg_1_arg_2_is_Ratio,
                                           error::program::correlation_arg_1_arg_2_has_nonzero_denominator,
                                           error::program::correlation_arg_1_arg_2_is_greater_than_or_equal_to_correlation_arg_1_arg_1_arg_2,
                                           error::program::correlation_node_arg_1_is_Comp,
                                           error::program::correlation_node_arg_1_arg_1_is_function_pointer,
                                           error::program::correlation_node_arg_1_arg_2_is_Ratio,
                                           error::program::correlation_node_arg_1_arg_2_has_nonzero_denominator,
                                           error::program::correlation_node_arg_1_arg_2_is_positive,
                                           error::program::correlation_node_arg_2_is_Ratio,
                                           error::program::correlation_node_arg_2_has_nonzero_denominator,
                                           error::program::correlation_node_arg_2_is_greater_than_or_equal_to_correlation_node_arg_1_arg_2,
                                           error::program::backend_is_available,
                                           error::program::gedf_schedulability_test_is_successful> {
    };

  }
}
