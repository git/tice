/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace error {
        namespace correlation {
          namespace construct {

            template<bool arg__is_Node, typename arg__actual_parameter>
            struct arg_2_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Arg 2 is not class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, typename arg__actual_parameter>
            struct arg_2_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, typename arg__actual_parameter>
            struct arg_2_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, typename arg__actual_parameter>
            struct arg_2_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_2_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, typename arg__wcet>
            struct arg_2_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, typename arg__actual_parameter>
            struct arg_2_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, typename arg__period, typename arg__wcet>
            struct arg_2_arg_3_is_greater_than_or_equal_to_arg_2_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__is_actuator_node, typename arg__node_type>
            struct arg_2_is_actuator_node {
              static_assert(arg__is_actuator_node,
                            "[DEBUG] Arg 2 is not actuator node");
            };

            template<bool arg__is_std_ratio, typename arg__actual_parameter>
            struct arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Arg 3 is not class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_nonnegative, typename arg__actual_parameter>
            struct arg_3_is_nonnegative {
              static_assert(arg__is_nonnegative,
                            "[DEBUG] Correlation threshold is not nonnegative");
            };

            template<bool arg__is_Node, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct arg_4_element_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Arg 4's element is not class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct arg_4_element_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct arg_4_element_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct arg_4_element_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct arg_4_element_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, v1::error::Pos arg__element_pos, typename arg__wcet>
            struct arg_4_element_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct arg_4_element_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
            struct arg_4_element_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, v1::error::Pos arg__element_pos, typename arg__period, typename arg__wcet>
            struct arg_4_element_arg_3_is_greater_than_or_equal_to_arg_4_element_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__is_sensor_node, v1::error::Pos arg__element_pos, typename arg__node_type>
            struct arg_4_element_is_sensor_node {
              static_assert(arg__is_sensor_node,
                            "[DEBUG] Arg 4's element is not sensor node");
            };

            template<bool arg__is_tuple_construct, typename arg__actual_parameter>
            struct arg_4_is_tuple_construct {
              static_assert(arg__is_tuple_construct,
                            "[DEBUG] Arg 4 is not class template tice::v1::internals::tuple::construct");
            };

            template<bool arg__is_nonempty_tuple_construct>
            struct arg_4_is_nonempty_tuple_construct {
              static_assert(arg__is_nonempty_tuple_construct,
                            "[DEBUG] Arg 4 is not tice::v1::internals::tuple::construct with at least one element");
            };

            template<bool arg__are_distinct, v1::error::Pos arg__node_1_pos, v1::error::Pos arg__node_2_pos>
            struct nodes_are_distinct {
              static_assert(arg__are_distinct,
                            "[DEBUG] Duplicated nodes are not allowed");
            };

          }
        }
      }
      namespace correlation {

#include "v1_internals_begin.hpp"

#define base_1A(_d1, _d2, _d3, _d4, I, _1, _2, _3, _4) I        \
        opts  (_1, arg__starting_pos_of_sensor_nodes, _d1)      \
          opts(_2, arg__node_checker_msg, _d2)                  \
          opts(_3, arg__node_validator_msg, _d3)                \
          opts(_4, arg__distinct_node_checker_msg, _d4)
#define decl_1A(_d1, _d2, _d3, _d4, I, _1, _2, _3, _4)                                                                                  \
        base_1A(_d1, _d2, _d3, _d4, I##I,                                                                                               \
                type(_1, v1::error::Pos),                                                                                               \
                type(_2, template<bool arg__is_Node_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_> class),        \
                type(_3, template<bool arg__is_sensor_node_, v1::error::Pos arg__element_pos_, typename arg__node_type_> class),        \
                type(_4, template<bool arg__are_distinct_, v1::error::Pos arg__node_1_pos_, v1::error::Pos arg__node_2_pos_> class))
#define args_1A(I, _1, _2, _3, _4) base_1A((), (), (), (), I, _1, _2, _3, _4)
#define prms_1A(...) decl_1A((), (), (), (), __VA_ARGS__)

#define base_1C(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, I, _1, _2, _3, _4, _5, _6, _7, _8) I    \
        opts  (_1, arg__node_computation_checker_msg, _d1)                                      \
          opts(_2, arg__node_computation_fn_ptr_checker_msg, _d2)                               \
          opts(_3, arg__node_computation_wcet_checker_msg, _d3)                                 \
          opts(_4, arg__node_computation_wcet_den_checker_msg, _d4)                             \
          opts(_5, arg__node_computation_wcet_validator_msg, _d5)                               \
        opts  (_6, arg__node_period_checker_msg, _d6)                                           \
        opts  (_7, arg__node_period_den_checker_msg, _d7)                                       \
        opts  (_8, arg__node_period_validator_msg, _d8)
#define decl_1C(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, I, _1, _2, _3, _4, _5, _6, _7, _8)                                                         \
        base_1C(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, I##I,                                                                                      \
                type(_1, template<bool arg__is_comp_Unit_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_> class),              \
                type(_2, template<bool arg__is_function_pointer_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_> class),       \
                type(_3, template<bool arg__is_std_ratio_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_> class),              \
                type(_4, template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_> class),   \
                type(_5, template<bool arg__is_positive_, v1::error::Pos arg__element_pos_, typename arg__wcet_> class),                           \
                type(_6, template<bool arg__is_std_ratio_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_> class),              \
                type(_7, template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__element_pos_, typename arg__actual_parameter_> class),   \
                type(_8, template<H(bool arg__is_greater_than_or_equal_, v1::error::Pos arg__element_pos_,                                         \
                                    typename arg__period_, typename arg__wcet_)> class))
#define args_1C(I, _1, _2, _3, _4, _5, _6, _7, _8) base_1C((), (), (), (), (), (), (), (), I, _1, _2, _3, _4, _5, _6, _7, _8)
#define prms_1C(...) decl_1C((), (), (), (), (), (), (), (), __VA_ARGS__)

#define base_2AA(_d1, args_1A, args_1C, _1)                     \
        args_1A                                                 \
        args_1C                                                 \
        opts  (_1, arg__tuple_construct__sensor_nodes, _d1)
#define decl_2AA(_d1, I, _1A_1, _1A_2, _1A_3, _1A_4, _1C_1, _1C_2, _1C_3, _1C_4, _1C_5, _1C_6, _1C_7, _1C_8, _1)        \
        base_2AA(_d1,                                                                                                   \
                 prms_1A(I, _1A_1, _1A_2, _1A_3, _1A_4),                                                                \
                 prms_1C(, _1C_1, _1C_2, _1C_3, _1C_4, _1C_5, _1C_6, _1C_7, _1C_8),                                     \
                 type(_1, typename))
#define args_2AA(I, _1A_1, _1A_2, _1A_3, _1A_4, _1C_1, _1C_2, _1C_3, _1C_4, _1C_5, _1C_6, _1C_7, _1C_8, _1)                             \
        base_2AA((), args_1A(I, _1A_1, _1A_2, _1A_3, _1A_4), args_1C(, _1C_1, _1C_2, _1C_3, _1C_4, _1C_5, _1C_6, _1C_7, _1C_8), _1)
#define prms_2AA(...) decl_2AA((), __VA_ARGS__)

        template<decl_2AA(_d1(tuple::construct<I>),
                          I, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__sensor_checker_data;

#define base_2AB(_d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18,                            \
                 I, _1, _2, _3, args_1A, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, args_1C, _17, _18) I        \
        opts           (_1 , arg__Node__actuator, ())                                                                           \
                   opts(_2 , arg__std_ratio__threshold, ())                                                                     \
        opts           (_3 , args__tuple_construct__of__Node__sensor, ())                                                       \
        args_1A                                                                                                                 \
        opts           (_4 , arg__2_checker_msg, _d4)                                                                           \
        opts           (_5 , arg__2_validator_msg, _d5)                                                                         \
        opts           (_6 , arg__3_checker_msg, _d6)                                                                           \
        opts           (_7 , arg__3_den_checker_msg, _d7)                                                                       \
        opts           (_8 , arg__3_validator_msg, _d8)                                                                         \
        opts           (_9 , arg__2_computation_checker_msg, _d9)                                                               \
        opts           (_10, arg__2_computation_fn_ptr_checker_msg, _d10)                                                       \
        opts           (_11, arg__2_computation_wcet_checker_msg, _d11)                                                         \
        opts           (_12, arg__2_computation_wcet_den_checker_msg, _d12)                                                     \
        opts           (_13, arg__2_computation_wcet_validator_msg, _d13)                                                       \
        opts           (_14, arg__2_period_checker_msg, _d14)                                                                   \
        opts           (_15, arg__2_period_den_checker_msg, _d15)                                                               \
        opts           (_16, arg__2_period_validator_msg, _d16)                                                                 \
        args_1C                                                                                                                 \
        opts           (_17, arg__tuple_checker_msg, _d17)                                                                      \
        opts           (_18, arg__nonempty_tuple_checker_msg, _d18)
#define decl_2AB(_1A_d1, _1A_d2, _1A_d3, _1A_d4, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16,        \
                 _1C_d1, _1C_d2, _1C_d3, _1C_d4, _1C_d5, _1C_d6, _1C_d7, _1C_d8, _d17, _d18,                                    \
                 I, _1, _2, _3, _1A_1, _1A_2, _1A_3, _1A_4, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16,          \
                 _1C_1, _1C_2, _1C_3, _1C_4, _1C_5, _1C_6, _1C_7, _1C_8, _17, _18)                                              \
        base_2AB(_d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18, I##I,                      \
                 type(_1, typename),                                                                                            \
                 type(_2, typename),                                                                                            \
                 type(_3, typename),                                                                                            \
                 decl_1A(_1A_d1, _1A_d2, _1A_d3, _1A_d4, , _1A_1, _1A_2, _1A_3, _1A_4),                                         \
                 type(_4, template<bool arg__is_Node_, typename arg__actual_parameter_> class),                                 \
                 type(_5, template<bool arg__is_actuator_node_, typename arg__node_type_> class),                               \
                 type(_6, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                            \
                 type(_7, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                 \
                 type(_8, template<bool arg__is_nonnegative_, typename arg__actual_parameter_> class),                          \
                 type(_9, template<bool arg__is_comp_Unit_, typename arg__actual_parameter_> class),                            \
                 type(_10, template<bool arg__is_function_pointer_, typename arg__actual_parameter_> class),                    \
                 type(_11, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                           \
                 type(_12, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                \
                 type(_13, template<bool arg__is_positive_, typename arg__wcet_> class),                                        \
                 type(_14, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                           \
                 type(_15, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                \
                 type(_16, template<bool arg__is_greater_than_or_equal_, typename arg__period_, typename arg__wcet_> class),    \
                 decl_1C(_1C_d1, _1C_d2, _1C_d3, _1C_d4, _1C_d5, _1C_d6, _1C_d7, _1C_d8,                                        \
                         , _1C_1, _1C_2, _1C_3, _1C_4, _1C_5, _1C_6, _1C_7, _1C_8),                                             \
                 type(_17, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class),                     \
                 type(_18,  template<bool arg__is_nonempty_tuple_construct_> class))
#define args_2AB(I, _1, _2, _3, _1A_1, _1A_2, _1A_3, _1A_4, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16,                  \
                 _1C_1, _1C_2, _1C_3, _1C_4, _1C_5, _1C_6, _1C_7, _1C_8, _17, _18)                                                      \
        base_2AB((), (), (), (), (), (), (), (), (), (), (), (), (), (), (),                                                            \
                 I, _1, _2, _3, args_1A(, _1A_1, _1A_2, _1A_3, _1A_4), _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16,       \
                 args_1C(, _1C_1, _1C_2, _1C_3, _1C_4, _1C_5, _1C_6, _1C_7, _1C_8), _17, _18)
#define prms_2AB(...)                                                                                                                           \
        decl_2AB((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_2AB(_1A_d1(1),
                          _1A_d2(error::correlation::construct::arg_4_element_is_Node),
                          _1A_d3(error::correlation::construct::arg_4_element_is_sensor_node),
                          _1A_d4(error::correlation::construct::nodes_are_distinct),
                          _d4   (error::correlation::construct::arg_2_is_Node),
                          _d5   (error::correlation::construct::arg_2_is_actuator_node),
                          _d6   (error::correlation::construct::arg_3_is_std_ratio),
                          _d7   (error::correlation::construct::arg_3_has_nonzero_denominator),
                          _d8   (error::correlation::construct::arg_3_is_nonnegative),
                          _d9   (error::correlation::construct::arg_2_arg_2_is_comp_Unit),
                          _d10  (error::correlation::construct::arg_2_arg_2_arg_2_is_function_pointer),
                          _d11  (error::correlation::construct::arg_2_arg_2_arg_3_is_Ratio),
                          _d12  (error::correlation::construct::arg_2_arg_2_arg_3_has_nonzero_denominator),
                          _d13  (error::correlation::construct::arg_2_arg_2_arg_3_is_positive),
                          _d14  (error::correlation::construct::arg_2_arg_3_is_std_ratio),
                          _d15  (error::correlation::construct::arg_2_arg_3_has_nonzero_denominator),
                          _d16  (error::correlation::construct::arg_2_arg_3_is_greater_than_or_equal_to_arg_2_arg_2_arg_3),
                          _1C_d1(error::correlation::construct::arg_4_element_arg_2_is_comp_Unit),
                          _1C_d2(error::correlation::construct::arg_4_element_arg_2_arg_2_is_function_pointer),
                          _1C_d3(error::correlation::construct::arg_4_element_arg_2_arg_3_is_Ratio),
                          _1C_d4(error::correlation::construct::arg_4_element_arg_2_arg_3_has_nonzero_denominator),
                          _1C_d5(error::correlation::construct::arg_4_element_arg_2_arg_3_is_positive),
                          _1C_d6(error::correlation::construct::arg_4_element_arg_3_is_std_ratio),
                          _1C_d7(error::correlation::construct::arg_4_element_arg_3_has_nonzero_denominator),
                          _1C_d8(error::correlation::construct::arg_4_element_arg_3_is_greater_than_or_equal_to_arg_4_element_arg_2_arg_3),
                          _d17  (error::correlation::construct::arg_4_is_tuple_construct),
                          _d18  (error::correlation::construct::arg_4_is_nonempty_tuple_construct),
                          I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct
        /* {
         *   typedef arg__Node__actuator actuator;
         *   typedef arg__std_ratio__threshold threshold;
         *   with template<typename I, typename... args> tuple::construct<I, args...> = args__tuple_construct__of__Node__sensor {
         *     typedef tuple::construct<I, arg__std__Node__sensor, args...> sensors;
         *   }
         * }
         */;

#define base_1B(I, _1, _2, _3) I                                \
        opts  (_1, arg__idx, ())                                \
          opts(_2, arg__element, ())                            \
          opts(_3, arg__construct__sensor_checker_data, ())
#define decl_1B(I, _1, _2, _3)                  \
        base_1B(I##I,                           \
                type(_1, v1::array::Idx),       \
                type(_2, typename),             \
                type(_3, typename))
#define args_1B(I, _1, _2, _3) base_1B(I, _1, _2, _3)
#define prms_1B(...) decl_1B(__VA_ARGS__)

        template<prms_1B(I, _, _, _)>
        struct construct__sensor_checker;

        

        template<prms_2AB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct :
          arg__tuple_checker_msg<!!utility::always_false<I>(), args__tuple_construct__of__Node__sensor> {
        };

        template<prms_2AB(I, _, _, R(typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct<args_2AB(I, _, _,
                                  R(tuple::construct<I, args...>),
                                  _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          arg__nonempty_tuple_checker_msg<!!utility::always_false<I>()> {
        };

        template<prms_2AB(I, _, _,
                          R(typename arg, typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct<args_2AB(I, _, _,
                                  R(tuple::construct<I, arg, args...>),
                                  _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          node::validate_type<I, arg__Node__actuator, v1::node::type::ACTUATOR, arg__2_validator_msg, arg__2_checker_msg,
                              arg__2_computation_checker_msg, arg__2_computation_fn_ptr_checker_msg, arg__2_computation_wcet_checker_msg,
                              arg__2_computation_wcet_den_checker_msg, arg__2_computation_wcet_validator_msg, arg__2_period_checker_msg,
                              arg__2_period_den_checker_msg, arg__2_period_validator_msg>,
          utility::ratio::validate_nonnegative<I, arg__std_ratio__threshold, arg__3_validator_msg, arg__3_den_checker_msg, arg__3_checker_msg>,
          utility::list::iterate<I, tuple::construct<I, arg, args...>, construct__sensor_checker,
                                 construct__sensor_checker_data<args_2AA(I, _, _, _, _, _, _, _, _, _, _, _, _, X)>>::value
        {
          typedef arg__Node__actuator actuator;
          typedef arg__std_ratio__threshold threshold;
          typedef tuple::construct<I, arg, args...> sensors;
        };

        

        template<prms_1B(I, _, _, RR(prms_2AA(, _, _, _, _, _, _, _, _, _, _, _, _, R(typename... args))))>
        struct construct__sensor_checker<args_1B(I, _, _, R(construct__sensor_checker_data<args_2AA(I, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                                                    R(tuple::construct<I, args...>))>))>
        {
          template<bool arg__is_Node_, typename arg__actual_parameter_>
          using arg__node_checker_msg_ = arg__node_checker_msg<arg__is_Node_, arg__starting_pos_of_sensor_nodes + arg__idx, arg__actual_parameter_>;

          template<bool arg__is_sensor_node_, typename arg__node_type_>
          using arg__node_validator_msg_ = arg__node_validator_msg<arg__is_sensor_node_, arg__starting_pos_of_sensor_nodes + arg__idx,
                                                                   arg__node_type_>;

          template<bool arg__is_comp_Unit_, typename arg__actual_parameter_>
          using computation_checker_msg_ = arg__node_computation_checker_msg<arg__is_comp_Unit_, arg__starting_pos_of_sensor_nodes + arg__idx,
                                                                             arg__actual_parameter_>;

          template<bool arg__is_function_pointer_, typename arg__actual_parameter_>
          using computation_fn_ptr_checker_msg_ = arg__node_computation_fn_ptr_checker_msg<H(arg__is_function_pointer_,
                                                                                             arg__starting_pos_of_sensor_nodes + arg__idx,
                                                                                             arg__actual_parameter_)>;

          template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
          using computation_wcet_checker_msg_ = arg__node_computation_wcet_checker_msg<arg__is_std_ratio_,
                                                                                       arg__starting_pos_of_sensor_nodes + arg__idx,
                                                                                       arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
          using computation_wcet_den_checker_msg_ = arg__node_computation_wcet_den_checker_msg<H(arg__has_nonzero_denominator_,
                                                                                                 arg__starting_pos_of_sensor_nodes + arg__idx,
                                                                                                 arg__actual_parameter_)>;

          template<bool arg__is_positive_, typename arg__wcet_>
          using computation_wcet_validator_msg_ = arg__node_computation_wcet_validator_msg<arg__is_positive_,
                                                                                           arg__starting_pos_of_sensor_nodes + arg__idx,
                                                                                           arg__wcet_>;

          template<bool arg__is_std_ratio_, typename arg__actual_parameter_>
          using period_checker_msg_ = arg__node_period_checker_msg<arg__is_std_ratio_, arg__starting_pos_of_sensor_nodes + arg__idx,
                                                                   arg__actual_parameter_>;

          template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_>
          using period_den_checker_msg_ = arg__node_period_den_checker_msg<arg__has_nonzero_denominator_,
                                                                           arg__starting_pos_of_sensor_nodes + arg__idx, arg__actual_parameter_>;

          template<bool arg__is_greater_than_or_equal_, typename arg__period_, typename arg__wcet_>
          using period_validator_msg_ = arg__node_period_validator_msg<arg__is_greater_than_or_equal_, arg__starting_pos_of_sensor_nodes + arg__idx,
                                                                       arg__period_, arg__wcet_>;

          run_metaprogram(node::validate_type<H(I, arg__element, v1::node::type::SENSOR,
                                                arg__node_validator_msg_, arg__node_checker_msg_,
                                                computation_checker_msg_, computation_fn_ptr_checker_msg_, computation_wcet_checker_msg_,
                                                computation_wcet_den_checker_msg_, computation_wcet_validator_msg_, period_checker_msg_,
                                                period_den_checker_msg_, period_validator_msg_)>);

          run_metaprogram(utility::check_pairwise_distinctness<H(I,
                                                                 utility::nodes_pairwise_distinctness<H(I, arg__distinct_node_checker_msg,
                                                                                                        arg__node_checker_msg,
                                                                                                        arg__node_checker_msg)>::template checker,
                                                                 arg__starting_pos_of_sensor_nodes + arg__idx - sizeof...(args), 1,
                                                                 arg__starting_pos_of_sensor_nodes + arg__idx, arg__element, args...)>);

          typedef construct__sensor_checker_data<args_2AA(I, _, _, _, _, _, _, _, _, _, _, _, _,
                                                          R(tuple::construct<I, args..., arg__element>))> value;
        };

        

        template<prms_2AA(I, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__sensor_checker_data {
        };

#include "v1_internals_end.hpp"
      }
    }
  }
}

namespace tice {
  namespace v1 {

    template<typename arg__Node__actuator, typename arg__Ratio__threshold, typename arg__Node__sensor,
             typename... args__Node__sensor>
    class Correlation :
      public internals::correlation::construct<I, arg__Node__actuator, arg__Ratio__threshold, internals::tuple::construct<I, arg__Node__sensor,
                                                                                                                          args__Node__sensor...>,
                                               3,
                                               error::correlation::remaining_arg_is_Node,
                                               error::correlation::remaining_arg_is_sensor_node,
                                               error::correlation::nodes_are_distinct,
                                               error::correlation::arg_1_is_Node,
                                               error::correlation::arg_1_is_actuator_node,
                                               error::correlation::arg_2_is_Ratio,
                                               error::correlation::arg_2_has_nonzero_denominator,
                                               error::correlation::arg_2_is_nonnegative,
                                               error::correlation::arg_1_arg_1_is_Comp,
                                               error::correlation::arg_1_arg_1_arg_1_is_function_pointer,
                                               error::correlation::arg_1_arg_1_arg_2_is_Ratio,
                                               error::correlation::arg_1_arg_1_arg_2_has_nonzero_denominator,
                                               error::correlation::arg_1_arg_1_arg_2_is_positive,
                                               error::correlation::arg_1_arg_2_is_Ratio,
                                               error::correlation::arg_1_arg_2_has_nonzero_denominator,
                                               error::correlation::arg_1_arg_2_is_greater_than_or_equal_to_arg_1_arg_1_arg_2,
                                               error::correlation::node_arg_1_is_Comp,
                                               error::correlation::node_arg_1_arg_1_is_function_pointer,
                                               error::correlation::node_arg_1_arg_2_is_Ratio,
                                               error::correlation::node_arg_1_arg_2_has_nonzero_denominator,
                                               error::correlation::node_arg_1_arg_2_is_positive,
                                               error::correlation::node_arg_2_is_Ratio,
                                               error::correlation::node_arg_2_has_nonzero_denominator,
                                               error::correlation::node_arg_2_is_greater_than_or_equal_to_node_arg_1_arg_2> {
    };

  }
}
