/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace error {
        namespace dict {
          namespace construct_entry {

            template<bool arg__is_correct>
            struct usage_is_correct {
              static_assert(arg__is_correct,
                            "[DEBUG] Template tice::v1::internals::dict::construct_entry is instantiated with"
                            " neither two arguments for carrying no data"
                            " nor three arguments with `void' as the second for carrying some data");
            };

          }
        }
      }
      namespace dict {

        template<typename I, v1::dict::Key arg__key>
        struct construct_entry_key {
          typedef v1::dict::Key value_type;
          static constexpr value_type value {arg__key};
        };

      }
      namespace dict {

#include "v1_internals_begin.hpp"

#define base_1(I, _1, _2) I                     \
        opts  (_1, arg__value, ())              \
          opts(_2, arg__specialization, ())
#define decl_1(I, _1, _2)                       \
        base_1(I##I,                            \
               type(_1, typename),              \
               type(_2, typename))
#define args_1(I, _1, _2) base_1(I, _1, _2)
#define prms_1(...) decl_1(__VA_ARGS__)

#define base_2A(_d2, I, _1, args_1, _2) I               \
        opts  (_1, arg__key, ())                        \
          args_1                                        \
          opts(_2, arg__usage_validator_msg, _d2)
#define decl_2A(_d2, I, _1, _1_1, _1_2, _2)                             \
        base_2A(_d2, I##I,                                              \
                type(_1, v1::dict::Key),                                \
                prms_1(, _1_1, _1_2),                                   \
                type(_2, template<bool arg__is_correct_> class))
#define args_2A(I, _1, _1_1, _1_2, _2) base_2A((), I, _1, args_1(, _1_1, _1_2), _2)
#define prms_2A(...) decl_2A((), __VA_ARGS__)

        template<decl_2A(_d2(error::dict::construct_entry::usage_is_correct),
                         I, _, _, _, _)>
        struct construct_entry
        /* {
         *   static constexpr v1::dict::Key key {arg__key};
         *   if (arg__specialization == void) {
         *     typedef arg__value value;
         *   } else {
         *     include arg__specialization;
         *   }
         * }
         */;

#define base_2B(I, _1, args_1) I                                \
        opts  (_1, arg__is_data_carrying_version_selected, ())  \
          args_1
#define decl_2B(I, _1, _1_1, _1_2)              \
        base_2B(I##I,                           \
                type(_1, bool),                 \
                prms_1(, _1_1, _1_2))
#define args_2B(I, _1, _1_1, _1_2) base_2B(I, _1, args_1(, _1_1, _1_2))
#define prms_2B(...) decl_2B(__VA_ARGS__)

        template<prms_2B(I, _, _, _)>
        struct construct_entry__base;

        

        template<prms_2A(I, _, _, _, _)>
        struct construct_entry :
          arg__usage_validator_msg<(std::is_same<arg__value, void>::value ^ std::is_same<arg__specialization, void>::value) == 1
                                   && (std::is_same<arg__value, void>::value || std::is_same<arg__specialization, void>::value)>,
          construct_entry__base<args_2B(I, R(std::is_same<arg__value, void>::value), _, _)>
        {
          static constexpr v1::dict::Key key {arg__key};
        };

        

        template<prms_2B(I, _, _, _)>
        struct construct_entry__base {
          typedef arg__value value;
        };

        template<prms_2B(I, X, _, _)>
        struct construct_entry__base<args_2B(I, R(true), _, _)> :
          arg__specialization {
        };

#include "v1_internals_end.hpp"
      }
    }
  }
}

namespace tice {
  namespace v1 {
    namespace dict {

      template<Key arg__key>
      class Entry_key :
        public internals::dict::construct_entry_key<I, arg__key> {
      };

      template<Key arg__key, typename arg__value, typename arg__specialization>
      class Entry :
        public internals::dict::construct_entry<I, arg__key, arg__value, arg__specialization,
                                                error::dict::usage_is_correct> {
      };

    }
  }
}
