/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace error {
        namespace feeder {
          namespace construct {

            template<bool arg__is_tuple_construct_with_three_or_more_elements, typename arg__actual_parameter>
            struct arg_2_is_tuple_construct_with_three_or_more_elements {
              static_assert(arg__is_tuple_construct_with_three_or_more_elements,
                            "[DEBUG] Arg 2 is either not specified using class template tice::v1::internals::tuple::construct"
                            " or has less than three elements");
            };

            template<bool arg__is_missing, v1::error::Pos arg__pos>
            struct node_element_is_missing {
              static_assert(arg__is_missing,
                            "[DEBUG] Tuple ends when an element of type tice::v1::Node is expected");
            };

            template<bool arg__is_producer_node, v1::error::Pos arg__pos, typename arg__actual_element>
            struct element_is_producer_node {
              static_assert(arg__is_producer_node,
                            "[DEBUG] Producer node element is not specified using class template tice::v1::Node");
            };

            template<bool arg__is_consumer_node, v1::error::Pos arg__pos, typename arg__actual_element>
            struct element_is_consumer_node {
              static_assert(arg__is_consumer_node,
                            "[DEBUG] Consumer node element is not specified using class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, v1::error::Pos arg__pos, typename arg__wcet>
            struct node_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct node_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, v1::error::Pos arg__pos, typename arg__period, typename arg__wcet>
            struct node_arg_3_is_greater_than_or_equal_to_node_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__is_equal, unsigned arg__channel_count, unsigned arg__parameter_count>
            struct channel_and_consumer_parameter_counts_are_equal {
              static_assert(arg__is_equal,
                            "[DEBUG] Consumer's parameter count and incoming arc count differ");
            };

            template<bool arg__is_callable, typename arg__consumer_function>
            struct consumer_function_is_callable {
              static_assert(arg__is_callable,
                            "[DEBUG] Consumer function is not callable");
            };

            template<bool arg__is_missing, v1::error::Pos arg__pos>
            struct channel_element_is_missing {
              static_assert(arg__is_missing,
                            "[DEBUG] Tuple ends when an element of type tice::v1::Chan_inlit or tice::v1::Chan is expected");
            };

            template<bool arg__is_channel, v1::error::Pos arg__pos, typename arg__actual_element>
            struct element_is_channel {
              static_assert(arg__is_channel,
                            "[DEBUG] Channel element is not specified using class template tice::v1::Chan_inlit or tice::v1::Chan");
            };

            template<bool arg__is_object_type, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct channel_arg_2_is_object_type {
              static_assert(arg__is_object_type,
                            "[DEBUG] Channel type is not an object type (neither void nor reference type nor function type)");
            };

            template<bool arg__is_cv_unqualified, v1::error::Pos arg__pos, typename arg__actual_parameter>
            struct channel_arg_2_is_cv_unqualified {
              static_assert(arg__is_cv_unqualified,
                            "[DEBUG] Channel type cannot be cv-qualified (qualified with either const or volatile or both)");
            };

            template<bool arg__is_not_nullptr, v1::error::Pos arg__pos>
            struct channel_arg_3_is_not_nullptr {
              static_assert(arg__is_not_nullptr,
                            "[DEBUG] Channel initial value pointer cannot be null");
            };

            template<bool arg__is_assignable, v1::error::Pos arg__producer_pos, v1::error::Pos arg__channel_pos,
                     typename arg__return_type, typename arg__channel_type>
            struct producer_return_value_is_assignable_to_channel {
              static_assert(arg__is_assignable,
                            "[DEBUG] Producer's return value is not assignable to channel");
            };

            template<bool arg__can_initialize, v1::error::Pos arg__channel_pos, typename arg__init_val_type, typename arg__channel_type>
            struct init_val_can_initialize_channel {
              static_assert(arg__can_initialize,
                            "[DEBUG] Initial value cannot initialize channel");
            };

            template<bool arg__is_compatible, v1::error::Pos arg__channel_pos, v1::error::Pos arg__parameter_pos,
                     typename arg__channel_type, typename arg__parameter_type>
            struct channel_and_consumer_parameter_are_compatible {
              static_assert(arg__is_compatible,
                            "[DEBUG] Channel cannot initialize consumer's corresponding parameter");
            };

            template<bool arg__has_no_multiple_edges, v1::error::Pos arg__producer_1_pos, v1::error::Pos arg__producer_2_pos>
            struct graph_has_no_multiple_edges {
              static_assert(arg__has_no_multiple_edges,
                            "[DEBUG] Tice graph cannot have multiple edges");
            };

            template<bool arg__has_no_loop, v1::error::Pos arg__producer_pos, v1::error::Pos arg__consumer_pos>
            struct graph_has_no_loop {
              static_assert(arg__has_no_loop,
                            "[DEBUG] Tice graph cannot have a loop");
            };

          }
        }
      }
      namespace feeder {

#include "v1_internals_begin.hpp"

#define base_1A(I, _1) I                                        \
        opts(_1, arg__tuple_construct__channel_types, ())
#define decl_1A(I, _1)                          \
        base_1A(I##I,                           \
                type(_1, typename))
#define args_1A(I, _1) base_1A(I, _1)
#define prms_1A(...) decl_1A(__VA_ARGS__)

#define base_1B(_d1, I, _1) I                   \
        opts  (_1, args, _d1)
#define decl_1B(I, _1)                          \
        base_1B((), I##I,                       \
                type(_1, typename...))
#define args_1B(I, _1) base_1B((...), I, _1)
#define prms_1B(...) decl_1B(__VA_ARGS__)

#define base_1H(I, _1) I                        \
        opts  (_1, arg__pos, ())
#define decl_1H(I, _1)                          \
        base_1H(I##I,                           \
                type(_1, v1::error::Pos))
#define args_1H(I, _1) base_1H(I, _1)
#define prms_1H(...) decl_1H(__VA_ARGS__)

#define base_1G(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, I, _1, _2, _3, _4, _5, _6, _7, _8) I    \
        opts  (_1, arg__node_computation_checker_msg, _d1)                                      \
          opts(_2, arg__node_computation_fn_ptr_checker_msg, _d2)                               \
          opts(_3, arg__node_computation_wcet_checker_msg, _d3)                                 \
          opts(_4, arg__node_computation_wcet_den_checker_msg, _d4)                             \
          opts(_5, arg__node_computation_wcet_validator_msg, _d5)                               \
        opts  (_6, arg__node_period_checker_msg, _d6)                                           \
        opts  (_7, arg__node_period_den_checker_msg, _d7)                                       \
        opts  (_8, arg__node_period_validator_msg, _d8)
#define decl_1G(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, I, _1, _2, _3, _4, _5, _6, _7, _8)                                                         \
        base_1G(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, I##I,                                                                                      \
                type(_1, template<bool arg__is_comp_Unit_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),                      \
                type(_2, template<bool arg__is_function_pointer_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),               \
                type(_3, template<bool arg__is_std_ratio_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),                      \
                type(_4, template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),           \
                type(_5, template<bool arg__is_positive_, v1::error::Pos arg__pos_, typename arg__wcet_> class),                                   \
                type(_6, template<bool arg__is_std_ratio_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),                      \
                type(_7, template<bool arg__has_nonzero_denominator_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),           \
                type(_8, template<bool arg__is_greater_than_or_equal_, v1::error::Pos arg__pos_, typename arg__period_, typename arg__wcet_> class))
#define args_1G(I, _1, _2, _3, _4, _5, _6, _7, _8) base_1G((), (), (), (), (), (), (), (), I, _1, _2, _3, _4, _5, _6, _7, _8)
#define prms_1G(...) decl_1G((), (), (), (), (), (), (), (), __VA_ARGS__)

        //\begin{import utility::error_pos_adaptor::node_msgs}
#define base_2G(args_1H, args_1G)               \
        args_1H                                 \
        args_1G
#define decl_2G(I, _1H_1, _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8)       \
        base_2G(prms_1H(I, _1H_1),                                                      \
                prms_1G(, _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8))
#define args_2G(I, _1H_1, _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8)                       \
        base_2G(args_1H(I, _1H_1), args_1G(, _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8))
#define prms_2G(...) decl_2G(__VA_ARGS__)
        //\end{import utility::error_pos_adaptor::node_msgs}

#define base_1C(_d1, _d2, _d3, I, _1, _2, _3) I         \
        opts  (_1, arg__channel_type_checker_msg, _d1)  \
          opts(_2, arg__channel_cv_checker_msg, _d2)    \
          opts(_3, arg__channel_value_checker_msg, _d3)
#define decl_1C(_d1, _d2, _d3, I, _1, _2, _3)                                                                                           \
        base_1C(_d1, _d2, _d3, I##I,                                                                                                    \
                type(_1, template<bool arg__is_object_type_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),         \
                type(_2, template<bool arg__is_cv_unqualified_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),      \
                type(_3, template<bool arg__is_not_nullptr_, v1::error::Pos arg__pos_> class))
#define args_1C(I, _1, _2, _3) base_1C((), (), (), I, _1, _2, _3)
#define prms_1C(...) decl_1C((), (), (), __VA_ARGS__)

#define base_2H(args_1H, args_1C)               \
        args_1H                                 \
        args_1C
#define decl_2H(I, _1H_1, _1C_1, _1C_2, _1C_3)  \
        base_2H(prms_1H(I, _1H_1),              \
                prms_1C(, _1C_1, _1C_2, _1C_3))
#define args_2H(I, _1H_1, _1C_1, _1C_2, _1C_3) base_2H(args_1H(I, _1H_1), args_1C(, _1C_1, _1C_2, _1C_3))
#define prms_2H(...) decl_2H(__VA_ARGS__)

        template<prms_2H(I, _, _, _, _)>
        struct construct__error_pos_adaptor__channel_msgs;

#define base_2C(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, I, _1, _2, _3, _4, _5, _6, _7, args_1C, _8, _9) I  \
        opts  (_1, arg__node_missing_msg, _d1)                                                                  \
          opts(_2, arg__producer_node_checker_msg, _d2)                                                         \
          opts(_3, arg__consumer_node_checker_msg, _d3)                                                         \
          opts(_4, arg__param_count_checker_msg, _d4)                                                           \
        opts  (_5, arg__consumer_invocation_checker_msg, _d5)                                                   \
        opts  (_6, arg__channel_missing_msg, _d6)                                                               \
        opts  (_7, arg__channel_checker_msg, _d7)                                                               \
        args_1C                                                                                                 \
        opts  (_8, arg__producer_return_value_checker_msg, _d8)                                                 \
        opts  (_9, arg__channel_initval_checker_msg, _d9)
#define decl_2C(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _1C_d1, _1C_d2, _1C_d3, _d8, _d9, I, _1, _2, _3, _4, _5, _6, _7, _1C_1, _1C_2, _1C_3, _8, _9)   \
        base_2C(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, I##I,                                                                                 \
                type(_1, template<bool arg__is_missing_, v1::error::Pos arg__pos_> class),                                                         \
                type(_2, template<bool arg__is_producer_node_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),                  \
                type(_3, template<bool arg__is_consumer_node_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),                  \
                type(_4, template<bool arg__is_equal_, unsigned arg__channel_count_, unsigned arg__parameter_count_> class),                       \
                type(_5, template<bool arg__is_callable_, typename arg__consumer_function_> class),                                                \
                type(_6, template<bool arg__is_missing_, v1::error::Pos arg__pos_> class),                                                         \
                type(_7, template<bool arg__is_channel_, v1::error::Pos arg__pos_, typename arg__actual_parameter_> class),                        \
                decl_1C(_1C_d1, _1C_d2, _1C_d3, , _1C_1, _1C_2, _1C_3),                                                                            \
                type(_8, template<H(bool arg__is_assignable_, v1::error::Pos arg__producer_pos_, v1::error::Pos arg__channel_pos_,                 \
                                    typename arg__return_type_, typename arg__channel_type_)> class),                                              \
                type(_9, template<H(bool arg__can_initialize_, v1::error::Pos arg__channel_pos_,                                                   \
                                    typename arg__init_val_type_, typename arg__channel_type_)> class))
#define args_2C(I, _1, _2, _3, _4, _5, _6, _7, _1C_1, _1C_2, _1C_3, _8, _9)                                                     \
        base_2C((), (), (), (), (), (), (), (), (), I, _1, _2, _3, _4, _5, _6, _7, args_1C(, _1C_1, _1C_2, _1C_3), _8, _9)
#define prms_2C(...) decl_2C((), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

#define base_1E(_d1, I, _1) I                                           \
        opts  (_1, arg__channel_and_consumer_param_checker_msg, _d1)
#define decl_1E(_d1, I, _1)                                                                                                             \
        base_1E(_d1, I##I,                                                                                                              \
                type(_1, template<H(bool arg__is_compatible_, v1::error::Pos arg__channel_pos_, v1::error::Pos arg__parameter_pos_,     \
                                    typename arg__channel_type_, typename arg__parameter_type_)> class))
#define args_1E(I, _1) base_1E((), I, _1)
#define prms_1E(...) decl_1E((), __VA_ARGS__)

#define base_2A(args_1H, args_1A, args_2C, args_1E, args_1G, args_1B)   \
        args_1H                                                         \
        args_1A                                                         \
        args_2C                                                         \
        args_1E                                                         \
        args_1G                                                         \
        args_1B
#define decl_2A(I, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,     \
                _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                  \
        base_2A(prms_1H(I, _1H_1),                                                                                              \
                prms_1A(, _1A_1),                                                                                               \
                prms_2C(, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9),                  \
                prms_1E(, _1E_1),                                                                                               \
                prms_1G(, _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8),                                              \
                prms_1B(, _1B_1))
#define args_2A(I, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,             \
                _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                          \
        base_2A(args_1H(I, _1H_1), args_1A(, _1A_1),                                                                                    \
                args_2C(, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9), args_1E(, _1E_1),        \
                args_1G(, _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8), args_1B(, _1B_1))
#define prms_2A(...) decl_2A(__VA_ARGS__)

#define base_3AA(I, _1, args_2A) I                              \
        opts  (_1, arg__error_pos_adaptor__node_msgs, ())       \
          args_2A
#define decl_3AA(I, _1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,        \
                 _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                         \
        base_3AA(I##I,                                                                                                                  \
                 type(_1, typename),                                                                                                    \
                 prms_2A(, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,     \
                         _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1))
#define args_3AA(I, _1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,                \
                 _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                                 \
        base_3AA(I, _1, args_2A(, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,      \
                                _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1))
#define prms_3AA(...) decl_3AA(__VA_ARGS__)

        template<prms_3AA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__check_odd_arg;

#define base_3AB(I, _1, args_2A) I                      \
        opts  (_1, arg__Node__producer_or_consumer, ()) \
          args_2A
#define decl_3AB(I, _1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,        \
                 _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                         \
        base_3AB(I##I,                                                                                                                  \
                 type(_1, typename),                                                                                                    \
                 prms_2A(, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,     \
                         _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1))
#define args_3AB(I, _1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,                \
                 _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                                 \
        base_3AB(I, _1, args_2A(, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,      \
                                _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1))
#define prms_3AB(...) decl_3AB(__VA_ARGS__)

        template<prms_3AB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__check_odd_arg__valid_node;

#define base_3AC(I, _1, args_2A) I                      \
        opts  (_1, arg__producer_return_type, ())       \
          args_2A
#define decl_3AC(I, _1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,        \
                 _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                         \
        base_3AC(I##I,                                                                                                                  \
                 type(_1, typename),                                                                                                    \
                 prms_2A(, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,     \
                         _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1))
#define args_3AC(I, _1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,                \
                 _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                                 \
        base_3AC(I, _1, args_2A(, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,      \
                                _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1))
#define prms_3AC(...) decl_3AC(__VA_ARGS__)

#define base_4ACA(I, _1, args_3AC) I                                    \
        opts  (_1, arg__construct__error_pos_adaptor__channel_msgs, ()) \
          args_3AC
#define decl_4ACA(I, _1, _3AC_1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,       \
                  _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                                \
        base_4ACA(I##I,                                                                                                                         \
                  type(_1, typename),                                                                                                           \
                  prms_3AC(, _3AC_1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,   \
                           _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1))
#define args_4ACA(I, _1, _3AC_1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,       \
                  _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                                \
        base_4ACA(I, _1,                                                                                                                        \
                  args_3AC(, _3AC_1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,   \
                           _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1))
#define prms_4ACA(...) decl_4ACA(__VA_ARGS__)

        template<prms_4ACA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__check_even_arg;

#define base_4ACB(I, _1, args_3AC) I                            \
        opts  (_1, arg__Chan_inlit_or_Chan__channel, ())        \
          args_3AC
#define decl_4ACB(I, _1, _3AC_1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,       \
                  _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                                \
        base_4ACB(I##I,                                                                                                                         \
                  type(_1, typename),                                                                                                           \
                  prms_3AC(, _3AC_1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,   \
                           _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1))
#define args_4ACB(I, _1, _3AC_1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,       \
                  _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                                \
        base_4ACB(I, _1,                                                                                                                        \
                  args_3AC(, _3AC_1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1,   \
                           _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1))
#define prms_4ACB(...) decl_4ACB(__VA_ARGS__)

        template<prms_4ACB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__check_even_arg__valid_initval;

#define base_5ACB(I, _1, args_4ACB) I                   \
        opts  (_1, arg__initial_value_is_ptr, ())       \
          args_4ACB
#define decl_5ACB(I, _1, _4ACB_1, _3AC_1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1, \
                  _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                                   \
        base_5ACB(I##I,                                                                                                                            \
                  type(_1, bool),                                                                                                                  \
                  prms_4ACB(, _4ACB_1, _3AC_1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9,   \
                            _1E_1, _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1))
#define args_5ACB(I, _1, _4ACB_1, _3AC_1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1, \
                  _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1)                                                                   \
        base_5ACB(I, _1,                                                                                                                           \
                  args_4ACB(, _4ACB_1, _3AC_1, _1H_1, _1A_1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9,   \
                            _1E_1, _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _1B_1))
#define prms_5ACB(...) decl_5ACB(__VA_ARGS__)

        template<prms_5ACB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__check_even_arg__valid_channel;

#define base_1D(I, _1, args_1A, _2) I                           \
        opts  (_1, arg__parameter_pos, ())                      \
          args_1A                                               \
          opts(_2, arg__tuple_construct__parameter_types, ())
#define decl_1D(I, _1, _1A_1, _2)               \
        base_1D(I##I,                           \
                type(_1, v1::error::Pos),             \
                prms_1A(, _1A_1),                     \
                type(_2, typename))
#define args_1D(I, _1, _1A_1, _2) base_1D(I, _1, args_1A(, _1A_1), _2)
#define prms_1D(...) decl_1D(__VA_ARGS__)

#define base_2E(args_1D, args_1E)               \
        args_1D                                 \
        args_1E
#define decl_2E(I, _1D_1, _1A_1, _1D_2, _1E_1)                \
        base_2E(prms_1D(I, _1D_1, _1A_1, _1D_2),              \
                prms_1E(, _1E_1))
#define args_2E(I, _1D_1, _1A_1, _1D_2, _1E_1) base_2E(args_1D(I, _1D_1, _1A_1, _1D_2), args_1E(, _1E_1))
#define prms_2E(...) decl_2E(__VA_ARGS__)

        template<prms_2E(I, _, _, _, _)>
        struct construct__check_channels_and_consumer_parameters_compatibility;

#define base_1F(_d1, _d2, I, _1, _2) I                  \
        opts  (_1, arg__multiple_edge_checker_msg, _d1) \
          opts(_2, arg__loop_checker_msg, _d2)
#define decl_1F(_d1, _d2, I, _1, _2)                                                                                                            \
        base_1F(_d1, _d2, I##I,                                                                                                                 \
                type(_1, template<H(bool arg__has_no_multiple_edges_,                                                                           \
                                    v1::error::Pos arg__producer_1_pos_, v1::error::Pos arg__producer_2_pos_)> class),                          \
                type(_2, template<H(bool arg__has_no_loop, v1::error::Pos arg__producer_pos, v1::error::Pos arg__consumer_pos_)> class))
#define args_1F(I, _1, _2) base_1F((), (), I, _1, _2)
#define prms_1F(...) decl_1F((), (), __VA_ARGS__)

#define base_2F(I, _1, _2, args_1F, args_1B) I                          \
        opts  (_1, arg__head_pos, ())                                   \
          opts(_2, arg__tuple_construct__of__Node__producers, ())       \
          args_1F                                                       \
          args_1B
#define decl_2F(I, _1, _2, _1F_1, _1F_2, _1B_1) \
        base_2F(I##I,                           \
                type(_1, v1::error::Pos),       \
                type(_2, typename),             \
                prms_1F(, _1F_1, _1F_2),        \
                prms_1B(, _1B_1))
#define args_2F(I, _1, _2, _1F_1, _1F_2, _1B_1) base_2F(I, _1, _2, args_1F(, _1F_1, _1F_2), args_1B(, _1B_1))
#define prms_2F(...) decl_2F(__VA_ARGS__)

        template<prms_2F(I, _, _, _, _, _)>
        struct construct__check_simple_graph;

#define base_2B(I, _1, _2, _3, args_1B) I                                       \
        opts  (_1, arg__tuple_construct__of__Node__producers, ())               \
          opts(_2, arg__tuple_construct__of__Chan_inlit_or_Chan__channels, ())  \
          opts(_3, arg__Node__producer_or_consumer, ())                         \
          args_1B
#define decl_2B(I, _1, _2, _3, _1B_1)           \
        base_2B(I##I,                           \
                type(_1, typename),             \
                type(_2, typename),             \
                type(_3, typename),             \
                prms_1B(, _1B_1))
#define args_2B(I, _1, _2, _3, _1B_1) base_2B(I, _1, _2, _3, args_1B(, _1B_1))
#define prms_2B(...) decl_2B(__VA_ARGS__)

        template<prms_2B(I, _, _, _, _)>
        struct construct__make_arcs;

#define base_3C(_d2, I, _1, args_2C, args_1E, args_1F, args_1G, _2) I                                   \
        opts  (_1, arg__tuple_construct__of__Node__comma__Chan_inlit_or_Chan__ellipsis__Node, ())       \
          args_2C                                                                                       \
          args_1E                                                                                       \
          args_1F                                                                                       \
        args_1G                                                                                         \
        opts  (_2, arg__tuple_checker_msg, _d2)
#define decl_3C(_2C_d1, _2C_d2, _2C_d3, _2C_d4, _2C_d5, _2C_d6, _2C_d7, _1C_d1, _1C_d2, _1C_d3, _2C_d8, _2C_d9, _1E_d1, _1F_d1, _1F_d2,            \
                _1G_d1, _1G_d2, _1G_d3, _1G_d4, _1G_d5, _1G_d6, _1G_d7, _1G_d8, _d2,                                                               \
                I, _1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1, _1F_1, _1F_2,                    \
                _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _2)                                                                        \
        base_3C(_d2, I##I,                                                                                                                         \
                type(_1, typename),                                                                                                                \
                decl_2C(_2C_d1, _2C_d2, _2C_d3, _2C_d4, _2C_d5, _2C_d6, _2C_d7, _1C_d1, _1C_d2, _1C_d3, _2C_d8, _2C_d9,                            \
                        , _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9),                                     \
                decl_1E(_1E_d1, , _1E_1),                                                                                                          \
                decl_1F(_1F_d1, _1F_d2, , _1F_1, _1F_2),                                                                                           \
                decl_1G(_1G_d1, _1G_d2, _1G_d3, _1G_d4, _1G_d5, _1G_d6, _1G_d7, _1G_d8, , _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8), \
                type(_2, template<bool arg__is_tuple_construct_with_three_or_more_elements_, typename arg__actual_parameter_> class))
#define args_3C(I, _1, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9, _1E_1, _1F_1, _1F_2,                 \
                _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8, _2)                                                                     \
        base_3C((), I, _1, args_2C(, _2C_1, _2C_2, _2C_3, _2C_4, _2C_5, _2C_6, _2C_7, _1C_1, _1C_2, _1C_3, _2C_8, _2C_9), args_1E(, _1E_1),     \
                args_1F(, _1F_1, _1F_2), args_1G(, _1G_1, _1G_2, _1G_3, _1G_4, _1G_5, _1G_6, _1G_7, _1G_8), _2)
#define prms_3C(...) decl_3C((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_3C(_2C_d1(error::feeder::construct::node_element_is_missing),
                         _2C_d2(error::feeder::construct::element_is_producer_node),
                         _2C_d3(error::feeder::construct::element_is_consumer_node),
                         _2C_d4(error::feeder::construct::channel_and_consumer_parameter_counts_are_equal),
                         _2C_d5(error::feeder::construct::consumer_function_is_callable),
                         _2C_d6(error::feeder::construct::channel_element_is_missing),
                         _2C_d7(error::feeder::construct::element_is_channel),
                         _1C_d1(error::feeder::construct::channel_arg_2_is_object_type),
                         _1C_d2(error::feeder::construct::channel_arg_2_is_cv_unqualified),
                         _1C_d3(error::feeder::construct::channel_arg_3_is_not_nullptr),
                         _2C_d8(error::feeder::construct::producer_return_value_is_assignable_to_channel),
                         _2C_d9(error::feeder::construct::init_val_can_initialize_channel),
                         _1E_d1(error::feeder::construct::channel_and_consumer_parameter_are_compatible),
                         _1F_d1(error::feeder::construct::graph_has_no_multiple_edges),
                         _1F_d2(error::feeder::construct::graph_has_no_loop),
                         _1G_d1(error::feeder::construct::node_arg_2_is_comp_Unit),
                         _1G_d2(error::feeder::construct::node_arg_2_arg_2_is_function_pointer),
                         _1G_d3(error::feeder::construct::node_arg_2_arg_3_is_Ratio),
                         _1G_d4(error::feeder::construct::node_arg_2_arg_3_has_nonzero_denominator),
                         _1G_d5(error::feeder::construct::node_arg_2_arg_3_is_positive),
                         _1G_d6(error::feeder::construct::node_arg_3_is_std_ratio),
                         _1G_d7(error::feeder::construct::node_arg_3_has_nonzero_denominator),
                         _1G_d8(error::feeder::construct::node_arg_3_is_greater_than_or_equal_to_node_arg_2_arg_3),
                         _d2   (error::feeder::construct::arg_2_is_tuple_construct_with_three_or_more_elements),
                         I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct
        /* {
         *   with template<typename I,
         *                 (typename, typename)... (args__Node__producer, args__Chan_inlit_or_Chan__channel), typename arg__Node__consumer>
         *        tuple::construct<I, (args__Node__producer, args__Chan_inlit_or_Chan__channel)..., arg__Node__consumer>
         *        = arg__tuple_construct__of__Node__comma__Chan_inlit_or_Chan__ellipsis__Node
         *   {
         *     typedef tuple::construct<I,
         *                              pair::construct<I, pair::construct<I, args__Node__producer, arg__Node__consumer>,
         *                                              args__Chan_inlit_or_Chan__channel>...> arcs;
         *   }
         * }
         */;

        

        template<prms_3C(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct :
          arg__tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct__of__Node__comma__Chan_inlit_or_Chan__ellipsis__Node> {
        };

        template<prms_3C(I, R(typename arg__Node__producer, typename arg__Chan_inlit_or_Chan__channel, typename arg__Node__producer_or_consumer,
                              typename... args__Chan_inlit_or_Chan__channel__comma__Node__producer_or_consumer),
                         _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct<args_3C(I, R(tuple::construct<H(I, arg__Node__producer, arg__Chan_inlit_or_Chan__channel, arg__Node__producer_or_consumer,
                                                         args__Chan_inlit_or_Chan__channel__comma__Node__producer_or_consumer...)>),
                                 _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          construct__check_odd_arg<args_3AA(I, R(utility::error_pos_adaptor::node_msgs<args_2G(I, R(1), _, _, _, _, _, _, _, _)>), R(1),
                                            R(tuple::construct<I>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                            R(arg__Node__producer, arg__Chan_inlit_or_Chan__channel, arg__Node__producer_or_consumer,
                                              args__Chan_inlit_or_Chan__channel__comma__Node__producer_or_consumer...))>,
          construct__check_simple_graph<args_2F(I, R(1), R(tuple::construct<I, arg__Node__producer>), _, _,
                                                R(arg__Chan_inlit_or_Chan__channel, arg__Node__producer_or_consumer,
                                                  args__Chan_inlit_or_Chan__channel__comma__Node__producer_or_consumer...))>,
          construct__make_arcs<args_2B(I, R(tuple::construct<I, arg__Node__producer>), R(tuple::construct<I, arg__Chan_inlit_or_Chan__channel>), _,
                                       R(args__Chan_inlit_or_Chan__channel__comma__Node__producer_or_consumer...))> {
        };

        

        template<prms_3AA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__check_odd_arg :
          arg__node_missing_msg<!!utility::always_false<I>(), arg__pos> {
        };

        template<prms_3AA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(typename arg, typename... args))>
        struct construct__check_odd_arg<args_3AA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(arg, args...))> :
          arg__producer_node_checker_msg<sizeof...(args) == 0, arg__pos, arg>,
          arg__consumer_node_checker_msg<sizeof...(args) != 0, arg__pos, arg> {
        };

        template<prms_3AA(I,
                          _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(typename... args__node_arg, typename... args))>
        struct construct__check_odd_arg<args_3AA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                 R(v1::Node<args__node_arg...>, args...))> :
          utility::run_metaprogram<I, node::construct<I, args__node_arg...,
                                                      arg__error_pos_adaptor__node_msgs::template period_validator_msg,
                                                      arg__error_pos_adaptor__node_msgs::template computation_checker_msg,
                                                      arg__error_pos_adaptor__node_msgs::template computation_fn_ptr_checker_msg,
                                                      arg__error_pos_adaptor__node_msgs::template computation_wcet_checker_msg,
                                                      arg__error_pos_adaptor__node_msgs::template computation_wcet_den_checker_msg,
                                                      arg__error_pos_adaptor__node_msgs::template computation_wcet_validator_msg,
                                                      arg__error_pos_adaptor__node_msgs::template period_den_checker_msg,
                                                      arg__error_pos_adaptor__node_msgs::template period_checker_msg>>,
          construct__check_odd_arg__valid_node<args_3AB(I, R(v1::Node<args__node_arg...>),
                                                        _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(args...))> {
        };

        

        template<prms_3AB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, X)>
        struct construct__check_odd_arg__valid_node<args_3AB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, X)> :
          arg__param_count_checker_msg<(tuple::size<I, arg__tuple_construct__channel_types>::value
                                        == tuple::size<I, typename arg__Node__producer_or_consumer::fn_info::param_types>::value),
                                       tuple::size<I, arg__tuple_construct__channel_types>::value,
                                       tuple::size<I, typename arg__Node__producer_or_consumer::fn_info::param_types>::value>,
          construct__check_channels_and_consumer_parameters_compatibility<args_2E(I, R(1), _,
                                                                                  R(typename arg__Node__producer_or_consumer::fn_info::param_types),
                                                                                  _)>,
          arg__consumer_invocation_checker_msg<H(utility::is_callable<H(I, typename arg__Node__producer_or_consumer::fn_info::fn_ptr::value_type,
                                                                        arg__tuple_construct__channel_types)>::value,
                                                 typename arg__Node__producer_or_consumer::fn_info::fn_ptr::value_type)> {
        };

        template<prms_3AB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__check_odd_arg__valid_node :
          construct__check_even_arg<args_4ACA(I, R(construct__error_pos_adaptor__channel_msgs<args_2H(I, R(arg__pos + 1), _, _, _)>),
                                              R(typename arg__Node__producer_or_consumer::fn_info::return_type), R(arg__pos + 1),
                                              _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
        };

        

        template<prms_2H(I, _, _, _, _)>
        struct construct__error_pos_adaptor__channel_msgs
        {
          template<bool arg__is_object_type_, typename arg__actual_parameter_>
          using type_checker_msg = arg__channel_type_checker_msg<arg__is_object_type_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_cv_unqualified_, typename arg__actual_parameter_>
          using cv_checker_msg = arg__channel_cv_checker_msg<arg__is_cv_unqualified_, arg__pos, arg__actual_parameter_>;

          template<bool arg__is_not_nullptr_>
          using value_checker_msg = arg__channel_value_checker_msg<arg__is_not_nullptr_, arg__pos>;
        };

        

        template<prms_4ACA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__check_even_arg :
          arg__channel_missing_msg<!!utility::always_false<I>(), arg__pos> {
        };

        template<prms_4ACA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(typename arg, typename... args))>
        struct construct__check_even_arg<args_4ACA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(arg, args...))> :
          arg__channel_checker_msg<!!utility::always_false<I>(), arg__pos, arg> {
        };

#define generate(chan_ns, chan_t, initval_t, init_val_is_ptr, optional_msg)                                                                     \
        template<prms_4ACA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,                                        \
                           R(typename arg__channel_type, initval_t arg__channel_initval, typename... args))>                                    \
        struct construct__check_even_arg<args_4ACA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,                \
                                                   R(chan_t<arg__channel_type, arg__channel_initval>, args...))> :                              \
          utility::run_metaprogram<I, chan_ns::construct<I, arg__channel_type, arg__channel_initval,                                            \
                                                         arg__construct__error_pos_adaptor__channel_msgs::template type_checker_msg,            \
                                                         arg__construct__error_pos_adaptor__channel_msgs::template cv_checker_msg               \
                                                         optional_msg>>,                                                                        \
          construct__check_even_arg__valid_channel<args_5ACB(I, R(init_val_is_ptr), R(chan_t<arg__channel_type, arg__channel_initval>),         \
                                                             _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {      \
        }

        generate(chan_inlit, v1::Chan_inlit, arg__channel_type, false, H());
        generate(chan, v1::Chan, std::add_pointer_t<std::add_const_t<arg__channel_type>>, true,
                 H(, arg__construct__error_pos_adaptor__channel_msgs::template value_checker_msg));

#undef generate

        

        template<prms_5ACB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__check_even_arg__valid_channel :
          arg__channel_initval_checker_msg<std::is_constructible<typename arg__Chan_inlit_or_Chan__channel::type,
                                                                 decltype(arg__Chan_inlit_or_Chan__channel::initial_value::value)>::value,
                                           arg__pos, decltype(arg__Chan_inlit_or_Chan__channel::initial_value::value),
                                           typename arg__Chan_inlit_or_Chan__channel::type>,
          construct__check_even_arg__valid_initval<args_4ACB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
        };

        template<prms_5ACB(I, X, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__check_even_arg__valid_channel<args_5ACB(I, R(true),
                                                                  _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          arg__channel_initval_checker_msg<std::is_constructible<typename arg__Chan_inlit_or_Chan__channel::type,
                                                                 decltype(*arg__Chan_inlit_or_Chan__channel::initial_value::value)>::value,
                                           arg__pos, decltype(*arg__Chan_inlit_or_Chan__channel::initial_value::value),
                                           typename arg__Chan_inlit_or_Chan__channel::type>,
          construct__check_even_arg__valid_initval<args_4ACB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
        };

        

        template<prms_4ACB(I, _, _, _, R(typename... args__element), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct construct__check_even_arg__valid_initval<args_4ACB(I, _, _, _, R(tuple::construct<I, args__element...>),
                                                                  _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
          arg__producer_return_value_checker_msg<utility::simple_assignment_ok<H(I, typename arg__Chan_inlit_or_Chan__channel::type,
                                                                                 arg__producer_return_type>::value,
                                                                                 arg__pos - 1, arg__pos,
                                                                                 arg__producer_return_type,
                                                                                 typename arg__Chan_inlit_or_Chan__channel::type)>,
          construct__check_odd_arg<args_3AA(I, R(utility::error_pos_adaptor::node_msgs<args_2G(I, R(arg__pos + 1), _, _, _, _, _, _, _, _)>),
                                            R(arg__pos + 1),
                                            R(tuple::construct<I, args__element..., typename arg__Chan_inlit_or_Chan__channel::type>),
                                            _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
        };

        

        template<prms_2E(I, _, X, X, _)>
        struct construct__check_channels_and_consumer_parameters_compatibility<args_2E(I, _, R(tuple::construct<I>), R(tuple::construct<I>), _)> {
        };

        template<prms_2E(I, _,
                         R(typename arg__channel_type, typename... args__channel_type),
                         R(typename arg__parameter_type, typename... args__parameter_type), _)>
        struct construct__check_channels_and_consumer_parameters_compatibility<args_2E(I, _,
                                                                                       R(tuple::construct<H(I, arg__channel_type,
                                                                                                            args__channel_type...)>),
                                                                                       R(tuple::construct<H(I, arg__parameter_type,
                                                                                                            args__parameter_type...)>),
                                                                                       _)> :
          arg__channel_and_consumer_param_checker_msg<std::is_constructible<arg__parameter_type, arg__channel_type>::value,
                                                      2 * arg__parameter_pos, arg__parameter_pos, arg__channel_type, arg__parameter_type>,
          construct__check_channels_and_consumer_parameters_compatibility<args_2E(I, R(arg__parameter_pos + 1),
                                                                                  R(tuple::construct<I, args__channel_type...>),
                                                                                  R(tuple::construct<I, args__parameter_type...>), _)> {
        };

        

        template<prms_2F(I, _, R(typename... args__Node__producer), _, _,
                         R(typename arg__Chan_inlit_or_Chan__channel, typename arg__Node__tail))>
        struct construct__check_simple_graph<args_2F(I, _, R(tuple::construct<I, args__Node__producer...>), _, _,
                                                     R(arg__Chan_inlit_or_Chan__channel, arg__Node__tail))> :
          utility::check_pairwise_distinctness<I, utility::nodes_pairwise_distinctness<I, arg__loop_checker_msg>::template checker,
                                               arg__head_pos, 2, arg__head_pos + 2 * sizeof...(args__Node__producer),
                                               arg__Node__tail, args__Node__producer...> {
        };

        template<prms_2F(I, _, R(typename... args__Node__producer), _, _,
                         R(typename arg__Chan_inlit_or_Chan__channel, typename arg__Node__tail, typename... args))>
        struct construct__check_simple_graph<args_2F(I, _, R(tuple::construct<I, args__Node__producer...>), _, _,
                                                     R(arg__Chan_inlit_or_Chan__channel, arg__Node__tail, args...))> :
          utility::check_pairwise_distinctness<I, utility::nodes_pairwise_distinctness<I, arg__multiple_edge_checker_msg>::template checker,
                                               arg__head_pos, 2, arg__head_pos + 2 * sizeof...(args__Node__producer),
                                               arg__Node__tail, args__Node__producer...>,
          construct__check_simple_graph<args_2F(I, _, R(tuple::construct<I, args__Node__producer..., arg__Node__tail>), _, _, _)> {
        };

        

        template<prms_2B(I, R(typename... args__Node__producer), R(typename... args__Chan_inlit_or_Chan__channel), _, X)>
        struct construct__make_arcs<args_2B(I, R(tuple::construct<I, args__Node__producer...>),
                                            R(tuple::construct<I, args__Chan_inlit_or_Chan__channel...>), _, X)> {
          typedef tuple::construct<I, pair::construct<I, pair::construct<I, args__Node__producer,
                                                                         arg__Node__producer_or_consumer>,
                                                      args__Chan_inlit_or_Chan__channel>...> arcs;
        };

        template<prms_2B(I, R(typename... args__Node__producer), R(typename... args__Chan_inlit_or_Chan__channel), _,
                         R(typename arg__Chan_inlit_or_Chan__channel, typename... args))>
        struct construct__make_arcs<args_2B(I, R(tuple::construct<I, args__Node__producer...>),
                                            R(tuple::construct<I, args__Chan_inlit_or_Chan__channel...>), _,
                                            R(arg__Chan_inlit_or_Chan__channel, args...))> :
          construct__make_arcs<args_2B(I, R(tuple::construct<I, args__Node__producer..., arg__Node__producer_or_consumer>),
                                       R(tuple::construct<I, args__Chan_inlit_or_Chan__channel..., arg__Chan_inlit_or_Chan__channel>), X, _)> {
        };

#include "v1_internals_end.hpp"
      }
    }
  }
}

namespace tice {
  namespace v1 {

    template<typename arg__Node__producer, typename arg__Chan_inlit_or_Chan__channel, typename
             _,                                                                   /*     |                         */
             typename... args__Node__producer__comma__Chan_inlit_or_Chan__channel /*, ___V____ arg__Node__consumer */>
    class Feeder :
      public internals::feeder::construct<I, internals::tuple::construct<I, arg__Node__producer, arg__Chan_inlit_or_Chan__channel, _,
                                                                         args__Node__producer__comma__Chan_inlit_or_Chan__channel...>,
                                          error::feeder::node_arg_is_missing,
                                          error::feeder::arg_is_producer_node,
                                          error::feeder::arg_is_consumer_node,
                                          error::feeder::channel_and_consumer_parameter_counts_are_equal,
                                          error::feeder::consumer_function_is_callable,
                                          error::feeder::channel_arg_is_missing,
                                          error::feeder::arg_is_channel,
                                          error::feeder::channel_arg_1_is_object_type,
                                          error::feeder::channel_arg_1_is_cv_unqualified,
                                          error::feeder::channel_arg_2_is_not_nullptr,
                                          error::feeder::producer_return_value_is_assignable_to_channel,
                                          error::feeder::init_val_can_initialize_channel,
                                          error::feeder::channel_and_consumer_parameter_are_compatible,
                                          error::feeder::graph_has_no_multiple_edges,
                                          error::feeder::graph_has_no_loop,
                                          error::feeder::node_arg_1_is_Comp,
                                          error::feeder::node_arg_1_arg_1_is_function_pointer,
                                          error::feeder::node_arg_1_arg_2_is_Ratio,
                                          error::feeder::node_arg_1_arg_2_has_nonzero_denominator,
                                          error::feeder::node_arg_1_arg_2_is_positive,
                                          error::feeder::node_arg_2_is_Ratio,
                                          error::feeder::node_arg_2_has_nonzero_denominator,
                                          error::feeder::node_arg_2_is_greater_than_or_equal_to_node_arg_1_arg_2> {
    };

  }
}
