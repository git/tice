/******************************************************************************
 * Copyright (C) 2018-2020  Tadeus Prastowo <0x66726565@gmail.com>            *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#ifdef TICE_V1_NOGEN

namespace tice {
  namespace v1 {
    namespace code {

      #define tice_v1_internals_gen(arg__generated_class_identifier, /* arg__Program__program */ ...) \
        class arg__generated_class_identifier                                                         \
        {                                                                                             \
          static_assert(sizeof(__VA_ARGS__) >= 0,                                                     \
                        "Executing the C++ metaprogram of "#arg__generated_class_identifier);         \
        public:                                                                                       \
          constexpr arg__generated_class_identifier() {                                               \
          }                                                                                           \
        }

    }
  }
}

#else // TICE_V1_NOGEN

#include <thread>
#include <chrono>
#include <mutex>
#include <cstring>
#include <atomic>
#include <condition_variable>
#include <cerrno>
#include <cstdio>

//\begin{platform-specific header files}
#include <sched.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <linux/sched.h>
#include <sys/mount.h>
//\end{platform-specific header files}

namespace tice {
  namespace v1 {
    namespace code {

      template<typename arg__type, arg__type arg__value>
      struct lvalue {
        static constexpr arg__type value {arg__value};
      };

      template<typename arg__type, arg__type arg__value>
      constexpr arg__type lvalue<arg__type, arg__value>::value;

    }
    namespace code {

      template<v1::dict::Key... args__id>
      struct Ids {
      };

    }
    namespace code {

      template<typename... args>
      struct List {
      };

      template<typename arg>
      struct List<arg> {
        arg element;
      };

      template<typename arg, typename... args>
      struct List<arg, args...> {
        arg element;
        List<args...> next;
      };

      namespace list {

        template<v1::array::Idx arg__idx, typename arg__List__list>
        struct get_type;

        template<v1::array::Idx arg__idx, typename arg__type, typename... args__type>
        struct get_type<arg__idx, List<arg__type, args__type...>> :
          get_type<arg__idx - 1, List<args__type...>> {
        };

        template<typename arg__type, typename... args__type>
        struct get_type<0, List<arg__type, args__type...>> {
          typedef arg__type value;
        };

      }
      namespace list {

        template<typename arg__return_type, v1::array::Idx arg__idx, v1::array::Idx arg__i = 0, typename arg__List__list>
        constexpr arg__return_type &get_element(arg__List__list &list, typename std::enable_if<arg__i == arg__idx>::type * = nullptr) {
          return list.element;
        }

        template<typename arg__return_type, v1::array::Idx arg__idx, v1::array::Idx arg__i = 0, typename arg__List__list>
        constexpr arg__return_type &get_element(arg__List__list &list, typename std::enable_if<arg__i != arg__idx>::type * = nullptr) {
          return get_element<arg__return_type, arg__idx, arg__i + 1>(list.next);
        }

      }

    }
    namespace code {

      typedef std::mutex task_sync_lock_type;

      struct Task_sync
      {
        std::atomic<bool> not_stopped;
        long error_code;
        task_sync_lock_type start_lock;
        task_sync_lock_type ready_to_start_lock;
        std::condition_variable ready_to_start;
      };

    }
    namespace code {

      class Periodic_task
      {
        Task_sync *synchronizer;

        std::chrono::nanoseconds wcet;
        std::chrono::nanoseconds period;
        long (*scheduler_setup)(const std::chrono::nanoseconds &wcet, const std::chrono::nanoseconds &period);
        void (*computation)(v1::array::Idx release_idx);

        std::thread task;

        inline bool wait_for_start_signal(const long &scheduler_setup_error_code)
        {
          std::unique_lock<task_sync_lock_type> notification_lock(synchronizer->ready_to_start_lock);
          synchronizer->error_code = scheduler_setup_error_code;
          synchronizer->ready_to_start.notify_one();
          notification_lock.unlock();

          std::lock_guard<task_sync_lock_type> wait(synchronizer->start_lock);
          return !synchronizer->error_code;
        }

        inline void run_periodically_until_stopped()
        {
          typedef std::chrono::high_resolution_clock clock;
          const clock::duration delta = period;
          std::chrono::time_point<clock> release_time(clock::now());
          v1::array::Idx release_idx = 0;
          while (std::atomic_load_explicit(&synchronizer->not_stopped, std::memory_order::memory_order_relaxed)) {
            computation(release_idx++);
            std::this_thread::sleep_until(release_time += delta);
          }
        }

        void run_periodic_task()
        {
          if (wait_for_start_signal(scheduler_setup(wcet, period))) {
            run_periodically_until_stopped();
          }
        }

      public:
        void setup(Task_sync *synchronizer)
        {
          Periodic_task::synchronizer = synchronizer;
        }

        void setup(const std::chrono::nanoseconds &wcet, const std::chrono::nanoseconds &period,
                   long (*scheduler_setup)(const std::chrono::nanoseconds &wcet, const std::chrono::nanoseconds &period),
                   void (*computation)(v1::array::Idx release_idx))
        {
          Periodic_task::wcet = wcet;
          Periodic_task::period = period;
          Periodic_task::scheduler_setup = scheduler_setup;
          Periodic_task::computation = computation;
        }

        void run()
        {
          task = std::thread(&Periodic_task::run_periodic_task, this);
        }

        void wait()
        {
          if (task.joinable()) {
            task.join();
          }
        }

      };

    }
    namespace code {

      template<v1::array::Size arg__task_count>
      class Task_manager;

      template<>
      class Task_manager<0>
      {
      public:
        typedef int data_propagator_array;

      private:
        long error_code;

      public:
        template<typename arg__setup_>
        Task_manager(arg__setup_ &&, typename arg__setup_::channels &, const data_propagator_array &, bool &first_object_exists) :
          error_code(0)
        {
          if (first_object_exists) {
            error_code = EBUSY;
          } else {
            first_object_exists = true;
          }
        }

        void run() {
        }

        long get_error_code() const {
          return error_code;
        }

        void stop() {
        }

        void wait() {
        }

      };

      template<v1::array::Size arg__task_count>
      class Task_manager
      {
      public:
        static constexpr v1::array::Size task_count {arg__task_count};
        typedef Periodic_task tasks_t[task_count];
        typedef long (*scheduler_setup_t)();
        struct data_propagator_array {
          void (*elems[task_count])(v1::array::Idx release_idx);
        };

      private:
        Task_sync synchronizer;
        tasks_t tasks;
        scheduler_setup_t scheduler_setup;
        bool the_active_object;
        bool tasks_are_running;

      public:
        template<typename arg__setup_>
        Task_manager(arg__setup_ &&, typename arg__setup_::channels &channels, const data_propagator_array &propagator_array,
                     bool &first_object_exists) :
          synchronizer{{true}}, tasks{}, scheduler_setup(nullptr), the_active_object(false), tasks_are_running(false)
        {
          if (first_object_exists) {
            synchronizer.error_code = EBUSY;
            return;
          } else {
            the_active_object = first_object_exists = true;
          }

          for (v1::array::Idx i = 0; i < task_count; ++i) {
            tasks[i].setup(&synchronizer);
          }
          arg__setup_(channels, tasks, scheduler_setup, propagator_array);
        }

        void run()
        {
          if (!the_active_object || tasks_are_running || (synchronizer.error_code = scheduler_setup())) {
            return;
          }
          tasks_are_running = true;

          std::unique_lock<task_sync_lock_type> barrier(synchronizer.start_lock);

          for (v1::array::Idx i = 0; i < task_count; ++i) {
            std::unique_lock<task_sync_lock_type> notification_lock(synchronizer.ready_to_start_lock);
            tasks[i].run();
            synchronizer.ready_to_start.wait(notification_lock);
            if (synchronizer.error_code) {
              stop();
              barrier.unlock();
              wait();
              break;
            }
          }
        }

        long get_error_code() const
        {
          return synchronizer.error_code;
        }

        void stop()
        {
          std::atomic_store_explicit(&synchronizer.not_stopped, false, std::memory_order::memory_order_relaxed);
        }

        void wait()
        {
          if (!tasks_are_running || std::atomic_load_explicit(&synchronizer.not_stopped, std::memory_order::memory_order_relaxed)) {
            return;
          }

          for (v1::array::Idx i = 0; i < task_count; ++i) {
            tasks[i].wait();
          }
          tasks_are_running = false;
        }

      };

    }
    namespace code {

      namespace {

        template<typename arg__channel_type>
        struct Channel
        {
          std::mutex buffer_lock;
          char communication_buffer[3][sizeof(arg__channel_type)];
          arg__channel_type *communication_array[3];
          v1::array::Idx usage_array[3];
          double producer_period;
          double consumer_period;
        };

      }

      namespace channel {

        template<typename arg__channel_type>
        void setup(Channel<arg__channel_type> &channel,
                   const arg__channel_type &initial_value, const double &producer_period, const double &consumer_period)
        {
          for (int i = 0; i < 3; ++i) {
            channel.communication_array[i] = new(channel.communication_buffer[i]) arg__channel_type(initial_value);
          }

          channel.producer_period = producer_period;
          channel.consumer_period = consumer_period;
          channel.usage_array[2] = v1::internals::utility::fp::ceil<I, true>(producer_period / consumer_period);
        }

      }
      namespace channel {

        template<typename arg__channel_type>
        arg__channel_type *get_buffer_to_write(Channel<arg__channel_type> &channel, v1::array::Idx producer_release_idx)
        {
          ++producer_release_idx;

          const v1::array::Idx consumer_end_release_idx
            = v1::internals::utility::fp::ceil<I, true>(channel.producer_period * (producer_release_idx + 1)
                                                        / channel.consumer_period);

          if (v1::internals::utility::fp::lt(channel.consumer_period * (consumer_end_release_idx - 1),
                                             channel.producer_period * producer_release_idx)) {
            return nullptr;
          }

          const v1::array::Idx consumer_start_release_idx
            = v1::internals::utility::fp::floor<I, true>(channel.producer_period * producer_release_idx
                                                         / channel.consumer_period);

          v1::array::Idx i = 2;
          if (channel.usage_array[0] <= consumer_start_release_idx) {
            if (channel.usage_array[1] <= consumer_start_release_idx && channel.usage_array[1] < channel.usage_array[0]) {
              i = 1;
            } else if (!(channel.usage_array[2] <= consumer_start_release_idx && channel.usage_array[2] < channel.usage_array[0])) {
              i = 0;
            }
          } else if (channel.usage_array[1] <= consumer_start_release_idx) {
            if (!(channel.usage_array[2] <= consumer_start_release_idx && channel.usage_array[2] < channel.usage_array[1])) {
              i = 1;
            }
          }

          std::lock_guard<std::mutex> critical_region(channel.buffer_lock);
          channel.usage_array[i] = consumer_end_release_idx;
          return channel.communication_array[i];
        }

      }
      namespace channel {

        template<typename arg__channel_type>
        arg__channel_type &get_buffer_to_read(Channel<arg__channel_type> &channel, const v1::array::Idx &consumer_release_idx)
        {
          std::lock_guard<std::mutex> critical_region(channel.buffer_lock);

          if (channel.usage_array[0] > consumer_release_idx) {

            if (channel.usage_array[1] > consumer_release_idx && channel.usage_array[1] < channel.usage_array[0]) {
              return *channel.communication_array[1];
            }
            if (channel.usage_array[2] > consumer_release_idx && channel.usage_array[2] < channel.usage_array[0]) {
              return *channel.communication_array[2];
            }

            return *channel.communication_array[0];
          }

          if (channel.usage_array[1] > consumer_release_idx) {

            if (channel.usage_array[2] > consumer_release_idx && channel.usage_array[2] < channel.usage_array[1]) {
              return *channel.communication_array[2];
            }

            return *channel.communication_array[1];
          }

          return *channel.communication_array[2];
        }

      }
      namespace channel {

        template<typename arg__channel_type>
        struct setup_data
        {
          const arg__channel_type &initial_value;
          const double &producer_period;
          const double &consumer_period;
        };

      }
      namespace channel {

        template<typename arg__List__channel_list, typename arg__channel_setup_data>
        inline void setup_list(arg__List__channel_list &channel_list, const arg__channel_setup_data &data)
        {
          setup(channel_list.element, data.initial_value, data.producer_period, data.consumer_period);
        }

        template<typename arg__List__channel_list, typename arg__channel_setup_data, typename... args__channel_setup_data>
        inline void setup_list(arg__List__channel_list &channel_list, const arg__channel_setup_data &data,
                               args__channel_setup_data... further_data)
        {
          setup_list(channel_list, data);
          setup_list(channel_list.next, further_data...);
        }

        template<typename arg__List__channel_list>
        inline void setup_list(arg__List__channel_list &channel_list) {
        }

      }

    }
    namespace code {

      template<typename arg__dict_Entry__of__Node__node, typename arg__tuple_construct__of__dict_construct_entry__consumer_id_to_channel_index,
               typename arg__program_Array_idx_list__producer_id>
      struct Propagator;

      template<typename arg__dict_Entry__of__Node__node, typename I, typename... args__dict_construct_entry__consumer_id_to_channel_index,
               v1::array::Idx... args__producer_id>
      struct Propagator<arg__dict_Entry__of__Node__node,
                        v1::internals::tuple::construct<I, args__dict_construct_entry__consumer_id_to_channel_index...>,
                        v1::internals::program::Array_idx_list<I, args__producer_id...>>
      {
        template<typename... args__channel_type_>
        inline void update_channels(const typename arg__dict_Entry__of__Node__node::value::fn_info::return_type &producer_output,
                                    args__channel_type_ *... channels)
        {
          [](auto...){}((channels && (*channels = producer_output, false))...);
        }

        template<typename arg__List__channels_>
        void propagate(arg__List__channels_ &channels, v1::array::Idx release_idx)
        {
          typedef typename arg__dict_Entry__of__Node__node::value::fn_info::fn_ptr fn;

          update_channels(fn::value(channel::get_buffer_to_read(list::get_element<H(typename
                                                                                    list::get_type<args__producer_id, arg__List__channels_>::value,
                                                                                    args__producer_id)>(channels), release_idx)...),
                          channel::get_buffer_to_write(list::get_element<H(typename list::
                                                                           get_type<H(H(args__dict_construct_entry__consumer_id_to_channel_index::
                                                                                        value),
                                                                                      arg__List__channels_)>::value,
                                                                           H(args__dict_construct_entry__consumer_id_to_channel_index::
                                                                             value))>(channels),
                                                       release_idx)...);
        }
      };

      template<typename arg__dict_Entry__of__Node__node, typename I, v1::array::Idx... args__producer_id>
      struct Propagator<arg__dict_Entry__of__Node__node, v1::internals::tuple::construct<I>,
                        v1::internals::program::Array_idx_list<I, args__producer_id...>>
      {
        template<typename arg__List__channels_>
        void propagate(arg__List__channels_ &channels, v1::array::Idx release_idx)
        {
          typedef typename arg__dict_Entry__of__Node__node::value::fn_info::fn_ptr fn;

          fn::value(channel::get_buffer_to_read(list::get_element<H(typename list::get_type<args__producer_id, arg__List__channels_>::value,
                                                                    args__producer_id)>(channels), release_idx)...);
        }
      };

    }
    namespace code {

      namespace empty {

        class setup
        {
        public:
          typedef Ids<> node_ids;
          typedef Task_manager<0> task_manager;
          typedef List<> channels;
          typedef List<> propagators;

          constexpr setup() {
          }
        };

      }

    }
    namespace code {

      namespace gedf {

        template<int... args__core_id>
        inline long setup_scheduler()
        {
          //\begin{platform-specific region}
          int core_ids[] = {args__core_id...};
          cpu_set_t coreset;
          CPU_ZERO(&coreset);
          for (v1::array::Idx i = 0, i_end = sizeof...(args__core_id); i < i_end; ++i) {
            CPU_SET(core_ids[i], &coreset);
          }
          if (sched_setaffinity(0, sizeof(coreset), &coreset)) {
            return errno;
          }

          char one_line[256];
          bool cpuset_is_mounted = false;
          {
            std::FILE *mountinfo_file = std::fopen("/proc/self/mountinfo", "rb");
            if (!mountinfo_file) {
              return errno;
            }
            while (std::fgets(one_line, sizeof(one_line), mountinfo_file)) {
              if (std::strstr(one_line, " cpuset ")) {
                cpuset_is_mounted = true;
                break;
              }
            }
            std::fclose(mountinfo_file);
          }

          char cpuset_mount_point[128];
          if (cpuset_is_mounted) {
            char *start_ptr = std::strstr(one_line, " /");
            if (!start_ptr) {
              return ENOENT;
            }
            start_ptr = std::strstr(start_ptr + 1, " /");
            if (!start_ptr) {
              return ENOENT;
            }
            ++start_ptr;

            char *end_ptr = std::strstr(start_ptr, " - ");
            if (!end_ptr) {
              return ENOENT;
            }
            for (v1::array::Idx i = 0; i < 2; ++i) {
              *end_ptr = '\0';
              end_ptr = std::strrchr(start_ptr, ' ');
              if (!end_ptr) {
                return ENOENT;
              }
            }
            *end_ptr = '\0';

            if (end_ptr - start_ptr >= sizeof(cpuset_mount_point)) {
              return ENAMETOOLONG;
            }
            std::strcpy(cpuset_mount_point, start_ptr);
          } else {
#define default_cpuset_mount_point "/dev/cpuset"
            if (sizeof(default_cpuset_mount_point) > sizeof(cpuset_mount_point)) {
              return ENAMETOOLONG;
            }
            std::strcpy(cpuset_mount_point, default_cpuset_mount_point);
#undef default_cpuset_mount_point

            struct stat stat_result;
            if (stat(cpuset_mount_point, &stat_result)) {
              if (errno == ENOENT) {
                if (mkdir(cpuset_mount_point, 0755)) {
                  return errno;
                }
              } else {
                return errno;
              }
            } else {
              return EEXIST;
            }

            if (mount("cpuset", cpuset_mount_point, "cgroup", 0, "cpuset")) {
              return errno;
            }
          }

          char cpuset_dir_path[128];
          for (v1::array::Idx i = 1; i <= v1::array::Max_idx::value; ++i) {
            if (std::snprintf(cpuset_dir_path, sizeof(cpuset_dir_path), "%s/tice-%llu",
                              cpuset_mount_point, static_cast<long long unsigned>(i)) >= sizeof(cpuset_dir_path)) {
              return ENAMETOOLONG;
            }
            struct stat stat_result;
            if (stat(cpuset_dir_path, &stat_result)) {
              if (errno != ENOENT) {
                continue;
              }
            } else {
              if (std::remove(cpuset_dir_path)) {
                continue;
              }
            }
            break;
          }
          if (mkdir(cpuset_dir_path, 0755)) {
            return errno;
          }

          char load_balancing_file_path[128];
          char cpuset_file_path[128];
          char task_file_path[128];
          if ((std::snprintf(load_balancing_file_path, sizeof(load_balancing_file_path), "%s/cpuset.sched_load_balance",
                             cpuset_mount_point) >= sizeof(load_balancing_file_path))
              || (std::snprintf(cpuset_file_path, sizeof(cpuset_file_path), "%s/cpuset.cpus",
                                cpuset_dir_path) >= sizeof(cpuset_file_path))
              || (std::snprintf(task_file_path, sizeof(task_file_path), "%s/tasks",
                                cpuset_dir_path) >= sizeof(task_file_path))) {
            return ENAMETOOLONG;
          }

          struct stat stat_result;
          if (stat(load_balancing_file_path, &stat_result)
              || stat(cpuset_file_path, &stat_result)
              || stat(task_file_path, &stat_result)) {
            return errno;
          }

          std::FILE *load_balancing_file = std::fopen(load_balancing_file_path, "rb");
          if (!load_balancing_file) {
            return errno;
          }
          int load_balancing_is_active;
          if (std::fscanf(load_balancing_file, "%d", &load_balancing_is_active) != 1) {
            auto error_code = errno;
            std::fclose(load_balancing_file);
            return error_code;
          }
          if (std::fclose(load_balancing_file)) {
            return errno;
          }
          if (load_balancing_is_active) {
            load_balancing_file = std::fopen(load_balancing_file_path, "wb");
            if (load_balancing_file) {
              std::fprintf(load_balancing_file, "0\n");
              if (std::fclose(load_balancing_file)) {
                return errno;
              }
            } else {
              return errno;
            }
          }

          std::FILE *cpuset_file = std::fopen(cpuset_file_path, "wb");
          if (cpuset_file) {
            std::fprintf(cpuset_file, "%d", core_ids[0]);
            for (v1::array::Idx i = 1, i_end = sizeof...(args__core_id); i < i_end; ++i) {
              std::fprintf(cpuset_file, ",%d", core_ids[i]);
            }
            std::fprintf(cpuset_file, "\n");
            if (std::fclose(cpuset_file)) {
              return errno;
            }
          } else {
            return errno;
          }

          std::FILE *task_file = std::fopen(task_file_path, "wb");
          if (task_file) {
            std::fprintf(task_file, "%llu\n", static_cast<long long unsigned>(getpid()));
            if (std::fclose(task_file)) {
              return errno;
            }
          } else {
            return errno;
          }
          //\end{platform-specific region}

          return 0;
        }

      }
      namespace gedf {

        inline long setup_per_task_scheduler(const std::chrono::nanoseconds &wcet, const std::chrono::nanoseconds &period)
        {
          //\begin{platform-specific region}
          struct {
            uint32_t size;
            uint32_t sched_policy;
            uint64_t sched_flags;
            int32_t sched_nice;
            uint32_t sched_priority;
            uint64_t sched_runtime;
            uint64_t sched_deadline;
            uint64_t sched_period;
          } scheduling_attributes = {0};
          scheduling_attributes.size = sizeof(scheduling_attributes);
          scheduling_attributes.sched_policy = SCHED_DEADLINE;
          scheduling_attributes.sched_runtime = wcet.count();
          scheduling_attributes.sched_deadline = period.count();
          return syscall(SYS_sched_setattr, 0, &scheduling_attributes, 0) ? errno : 0;
          //\end{platform-specific region}
        }

      }
      namespace gedf {

        template<typename arg__Core_ids__core_ids,
                 typename arg__tuple_construct__of__dict_Entry__of__Node__node_dict,
                 typename arg__tuple_construct__of__Channel__channel_list,
                 typename arg__tuple_construct__of__pair_construct__of__tuple_dict_Entry_array_idx__and__Array_idx_list__channel_idx_list>
        class setup;

        template<int... args__core_ids, typename I, typename... args__dict_Entry__of__Node, typename... args__Channel,
                 typename... args__tuple_dict_Entry_array_idx, typename... args__Array_idx_list>
        class setup<Core_ids<args__core_ids...>, v1::internals::tuple::construct<I, args__dict_Entry__of__Node...>,
                    v1::internals::tuple::construct<I, args__Channel...>,
                    v1::internals::tuple::construct<I,
                                                    v1::internals::pair::construct<I, args__tuple_dict_Entry_array_idx, args__Array_idx_list>...>>
        {
        public:
          typedef Ids<args__dict_Entry__of__Node::key...> node_ids;
          typedef Task_manager<sizeof...(args__dict_Entry__of__Node)> task_manager;
          typedef List<Channel<typename args__Channel::channel_type>...> channels;
          typedef List<Propagator<args__dict_Entry__of__Node, args__tuple_dict_Entry_array_idx, args__Array_idx_list>...> propagators;

        public:
          constexpr setup() {
          }

          setup(channels &channels, typename task_manager::tasks_t &tasks, typename task_manager::scheduler_setup_t &scheduler_setup,
                const typename task_manager::data_propagator_array &propagator_array)
          {
            typedef std::chrono::nanoseconds duration_t;
            constexpr duration_t wcet_array[] = {
              std::chrono::duration_cast<duration_t>(std::chrono::duration<H(typename duration_t::rep,
                                                                             typename args__dict_Entry__of__Node::value::wcet)>(1))...
            };
            constexpr duration_t period_array[] = {
              std::chrono::duration_cast<duration_t>(std::chrono::duration<H(typename std::chrono::nanoseconds::rep,
                                                                             typename args__dict_Entry__of__Node::value::period)>(1))...
            };
            for (v1::array::Idx i = 0; i < task_manager::task_count; ++i) {
              tasks[i].setup(wcet_array[i], period_array[i], &setup_per_task_scheduler, propagator_array.elems[i]);
            }

            scheduler_setup = &setup_scheduler<args__core_ids...>;

            typedef double period_t;
            constexpr period_t periods[] = {
              v1::internals::utility::fp::Double<I, typename args__dict_Entry__of__Node::value::period>::value...
            };
            channel::setup_list(channels, channel::setup_data<typename args__Channel::channel_type>{args__Channel::initial_value,
                                                                                                    periods[args__Channel::producer_id],
                                                                                                    periods[args__Channel::consumer_id]}...);
          }
        };

      }

    }
    namespace code {

      #define tice_v1_internals_gen(arg__generated_class_identifier, /* arg__Program__program */ ...)                                        \
        class arg__generated_class_identifier :                                                                                              \
          public __VA_ARGS__::code_setup::task_manager                                                                                       \
        {                                                                                                                                    \
          static bool first_object_exists;                                                                                                   \
          static typename __VA_ARGS__::code_setup::channels channels;                                                                        \
          static typename __VA_ARGS__::code_setup::propagators propagators;                                                                  \
                                                                                                                                             \
          template<tice::v1::array::Idx arg__i_>                                                                                             \
          static void f(tice::v1::array::Idx release_idx)                                                                                    \
          {                                                                                                                                  \
            tice::v1::code::list::get_element<typename tice::v1::code::list::get_type<arg__i_,                                               \
                                                                                      typename __VA_ARGS__::code_setup::propagators>::value, \
                                              arg__i_>(propagators).propagate(channels, release_idx);                                        \
          }                                                                                                                                  \
                                                                                                                                             \
          template<tice::v1::dict::Key... args__node_id_>                                                                                    \
          constexpr typename __VA_ARGS__::code_setup::task_manager::data_propagator_array                                                    \
          make_propagators(tice::v1::code::Ids<args__node_id_...> &&)                                                                        \
          {                                                                                                                                  \
            return {f<args__node_id_>...};                                                                                                   \
          }                                                                                                                                  \
                                                                                                                                             \
        public:                                                                                                                              \
          arg__generated_class_identifier() :                                                                                                \
            __VA_ARGS__::code_setup::task_manager(typename __VA_ARGS__::code_setup(), channels,                                              \
                                                  make_propagators(typename __VA_ARGS__::code_setup::node_ids()), first_object_exists) {     \
          }                                                                                                                                  \
        };                                                                                                                                   \
        bool arg__generated_class_identifier::first_object_exists;                                                                           \
        typename __VA_ARGS__::code_setup::channels arg__generated_class_identifier::channels;                                                \
        typename __VA_ARGS__::code_setup::propagators arg__generated_class_identifier::propagators

    }
  }
}

#endif // TICE_V1_NOGEN
