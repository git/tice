/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace debug {

        template<typename arg__template, bool arg__is_breakpoint = true>
        class print_template_args
        {
          static_assert(arg__is_breakpoint == false,
                        "[DEBUG] Template args printed");
        };

      }
      namespace debug {

        template<typename arg__template>
        class print_template_args_if_arg_1_is_incomplete
        {
          static_assert(sizeof(arg__template) >= 0,
                        "[DEBUG] Template args printed");
        };

      }
      namespace debug {

        template<typename arg__template, bool arg__is_breakpoint = true>
        class print_template_args_in_expression :
          arg__template
        {
          static_assert(arg__is_breakpoint == false,
                        "[DEBUG] Template args printed");
        };

      }
    }
  }
}
