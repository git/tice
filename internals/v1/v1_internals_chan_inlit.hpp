/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace error {
        namespace chan_inlit {
          namespace construct {

            template<bool arg__is_object_type, typename arg__actual_parameter>
            struct arg_2_is_object_type {
              static_assert(arg__is_object_type,
                            "[DEBUG] Channel type is not an object type (neither void nor reference type nor function type)");
            };

            template<bool arg__is_cv_unqualified, typename arg__actual_parameter>
            struct arg_2_is_cv_unqualified {
              static_assert(arg__is_cv_unqualified,
                            "[DEBUG] Channel type cannot be cv-qualified (qualified with either const or volatile or both)");
            };

          }
        }
      }
      namespace chan_inlit {

#include "v1_internals_begin.hpp"

#define base_1(_d3, _d4, I, _1, _2, _3, _4) I                   \
        opts  (_1, arg__channel_type, ())                       \
          opts(_2, arg__initial_value, ())                      \
          opts(_3, arg__channel_type_validator_msg, _d3)        \
          opts(_4, arg__channel_type_cv_validator_msg, _d4)
#define decl_1(_d3, _d4, I, _1, _2, _3, _4)                                                             \
        base_1(_d3, _d4, I##I,                                                                          \
               type(_1, typename),                                                                      \
               type(_2, arg__channel_type),                                                             \
               type(_3, template<bool arg__is_object_type_, typename arg__actual_parameter_> class),    \
               type(_4, template<bool arg__is_cv_unqualified_, typename arg__actual_parameter_> class))
#define args_1(I, _1, _2, _3, _4) base_1((), (), I, _1, _2, _3, _4)
#define prms_1(...) decl_1((), (), __VA_ARGS__)

        template<decl_1(_d3(error::chan_inlit::construct::arg_2_is_object_type),
                        _d4(error::chan_inlit::construct::arg_2_is_cv_unqualified),
                        I, _, _, _, _)>
        struct construct
        /* {
         *   typedef arg__channel_type type;
         *   typedef std::integral_constant<arg__channel_type, arg__initial_value> initial_value;
         * }
         */;

        

        template<prms_1(I, _, _, _, _)>
        struct construct :
          arg__channel_type_validator_msg<std::is_object<arg__channel_type>::value, arg__channel_type>,
          arg__channel_type_cv_validator_msg<!std::is_const<arg__channel_type>::value
                                             && !std::is_volatile<arg__channel_type>::value, arg__channel_type>
        {
          typedef arg__channel_type type;
          typedef std::integral_constant<arg__channel_type, arg__initial_value> initial_value;
        };

#include "v1_internals_end.hpp"
      }
    }
  }
}

namespace tice {
  namespace v1 {

    template<typename arg__channel_type, arg__channel_type arg__initial_value>
    class Chan_inlit :
      public internals::chan_inlit::construct<I, arg__channel_type, arg__initial_value,
                                              error::chan_inlit::arg_1_is_object_type,
                                              error::chan_inlit::arg_1_is_cv_unqualified> {
    };

  }
}
