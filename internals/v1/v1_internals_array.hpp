/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace error {
        namespace array {
          namespace construct {

            template<bool arg__is_valid_array_element_type, typename arg__actual_parameter>
            struct arg_3_is_valid_array_element_type {
              static_assert(arg__is_valid_array_element_type,
                            "[DEBUG] Arg 3 is not valid array element type"
                            " (Arg 3 is either an incomplete type or `void' or a reference type or a function type or an abstract class)");
            };

            template<bool arg__is_tuple_construct, typename arg__actual_parameter>
            struct arg_4_is_tuple_construct {
              static_assert(arg__is_tuple_construct,
                            "[DEBUG] Arg 4 is not class template tice::v1::internals::tuple::construct");
            };

            template<bool arg__is_tuple_construct__of__std_integral_constant, typename arg__actual_parameter>
            struct arg_4_is_tuple_construct__of__std_integral_constant {
              static_assert(arg__is_tuple_construct__of__std_integral_constant,
                            "[DEBUG] Arg 4 is not class template tice::v1::internals::tuple::construct"
                            " whose elements are class template std::integral_constant");
            };

            template<bool arg__is_tuple_construct_with_positive_size>
            struct arg_4_is_tuple_construct_with_positive_size {
              static_assert(arg__is_tuple_construct_with_positive_size,
                            "[DEBUG] Arg 4 is not class template tice::v1::internals::tuple::construct with at least one element");
            };

          }
          namespace is_empty {

            template<bool arg__has_element_type_as_member_typedef, typename arg__actual_parameter>
            struct arg_2_has_element_type_as_member_typedef {
              static_assert(arg__has_element_type_as_member_typedef,
                            "[DEBUG] Arg 2 has no member typedef `element_type'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_size_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_size_as_constexpr_copyable_data_member {
              static_assert(arg__has_size_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_type_as_member_typedef, typename arg__actual_parameter>
            struct arg_2_has_type_as_member_typedef {
              static_assert(arg__has_type_as_member_typedef,
                            "[DEBUG] Arg 2 has no member typedef `type'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_elems_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_elems_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_elems_ptr_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__element_type>
            struct arg_2_element_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's member typedef `element_type' is not valid array element type"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__size_type>
            struct arg_2_size_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's data member `size' is not `const tice::v1::array::Idx'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__type>
            struct arg_2_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's member typedef `type' is not `element_type[size]'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_type>
            struct arg_2_elems_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's data member `elems' is not `const element_type[size]'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_ptr_type>
            struct arg_2_elems_ptr_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's data member `elems_ptr' is not `const element_type *[]'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
            struct arg_2_elems_ptr_points_to_elems {
              static_assert(arg__points_to_elems,
                            "[DEBUG] Arg 2's data member `elems_ptr' fails to point to data member `elems' at index 0"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

          }
          namespace make {
            namespace value {

              template<bool arg__is_positive, v1::array::Size arg__size>
              struct arg_2_is_positive {
                static_assert(arg__is_positive,
                              "[DEBUG] Arg 2 is not positive");
              };

              template<bool arg__has_value_as_constexpr_copyable_data_member, typename arg__actual_parameter>
              struct arg_3_has_value_as_constexpr_copyable_data_member {
                static_assert(arg__has_value_as_constexpr_copyable_data_member,
                              "[DEBUG] Arg 3 has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `value'");
              };

              template<bool arg__is_convertible_to_array_element_type, typename arg__value_type, typename arg__array_element_type>
              struct value_is_convertible_to_array_element_type {
                static_assert(arg__is_convertible_to_array_element_type,
                              "[DEBUG] Arg 3 has its `value' nonconvertible to array element type");
              };

            }
            namespace from_tuple {
              namespace value {

                template<bool arg__is_tuple_construct, typename arg__actual_parameter>
                struct arg_2_is_tuple_construct {
                  static_assert(arg__is_tuple_construct,
                                "[DEBUG] Arg 2 is not class template tice::v1::internals::tuple::construct");
                };

                template<bool arg__is_nonempty>
                struct arg_2_is_nonempty {
                  static_assert(arg__is_nonempty,
                                "[DEBUG] Tuple has no element");
                };

                template<bool arg__has_value_as_constexpr_copyable_data_member, v1::error::Pos arg__element_pos, typename arg__element>
                struct element_has_value_as_constexpr_copyable_data_member {
                  static_assert(arg__has_value_as_constexpr_copyable_data_member,
                                "[DEBUG] Element has no constexpr (not static or not constexpr)"
                                " copyable (copy constructor maybe deleted or not constexpr) data member `value'");
                };

                template<bool arg__is_convertible_to_array_element_type, v1::error::Pos arg__element_pos,
                         typename arg__element_value_type, typename arg__array_element_type>
                struct element_value_is_convertible_to_array_element_type {
                  static_assert(arg__is_convertible_to_array_element_type,
                                "[DEBUG] Element has its `value' nonconvertible to array element type");
                };

              }
            }
            namespace nested {
              namespace value {

                template<bool arg__is_tuple_construct, typename arg__actual_parameter>
                struct arg_2_is_tuple_construct {
                  static_assert(arg__is_tuple_construct,
                                "[DEBUG] Arg 2 is not class template tice::v1::internals::tuple::construct");
                };

                template<bool arg__is_nonempty>
                struct arg_2_is_nonempty {
                  static_assert(arg__is_nonempty,
                                "[DEBUG] Tuple has no element");
                };

                template<bool arg__is_uniform, v1::error::Pos arg__pos, typename arg__array_element_type , typename arg__first_array_element_type>
                struct nested_array_element_type_is_uniform {
                  static_assert(arg__is_uniform,
                                "[DEBUG] N-th array element type is not that of the first array");
                };

                template<bool arg__is_positive, v1::error::Pos arg__pos, v1::array::Size arg__size>
                struct nested_array_size_is_positive {
                  static_assert(arg__is_positive,
                                "[DEBUG] N-th array size is not positive");
                };

                template<bool arg__has_element_type_as_member_typedef, v1::error::Pos arg__pos, typename arg__actual_parameter>
                struct nested_array_has_element_type_as_member_typedef {
                  static_assert(arg__has_element_type_as_member_typedef,
                                "[DEBUG] N-th array has no member typedef `element_type'"
                                " (maybe not using tice::v1::internals::array::construct in the first place)");
                };

                template<bool arg__has_size_as_constexpr_copyable_data_member, v1::error::Pos arg__pos, typename arg__actual_parameter>
                struct nested_array_has_size_as_constexpr_copyable_data_member {
                  static_assert(arg__has_size_as_constexpr_copyable_data_member,
                                "[DEBUG] N-th array has no constexpr (not static or not constexpr)"
                                " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                                " (maybe not using tice::v1::internals::array::construct in the first place)");
                };

                template<bool arg__has_type_as_member_typedef, v1::error::Pos arg__pos, typename arg__actual_parameter>
                struct nested_array_has_type_as_member_typedef {
                  static_assert(arg__has_type_as_member_typedef,
                                "[DEBUG] N-th array has no member typedef `type'"
                                " (maybe not using tice::v1::internals::array::construct in the first place)");
                };

                template<bool arg__has_elems_as_constexpr_copyable_data_member, v1::error::Pos arg__pos, typename arg__actual_parameter>
                struct nested_array_has_elems_as_constexpr_copyable_data_member {
                  static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                                "[DEBUG] N-th array has no constexpr (not static or not constexpr)"
                                " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                                " (maybe not using tice::v1::internals::array::construct in the first place)");
                };

                template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, v1::error::Pos arg__pos, typename arg__actual_parameter>
                struct nested_array_has_elems_ptr_as_constexpr_copyable_data_member {
                  static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                                "[DEBUG] N-th array has no constexpr (not static or not constexpr)"
                                " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                                " (maybe not using tice::v1::internals::array::construct in the first place)");
                };

                template<bool arg__is_valid, v1::error::Pos arg__pos, typename arg__element_type>
                struct nested_array_element_type_is_valid {
                  static_assert(arg__is_valid,
                                "[DEBUG] Type of N-th array's member typedef `element_type' is not valid array element type"
                                " (maybe not using tice::v1::internals::array::construct in the first place)");
                };

                template<bool arg__is_valid, v1::error::Pos arg__pos, typename arg__size_type>
                struct nested_array_size_type_is_valid {
                  static_assert(arg__is_valid,
                                "[DEBUG] Type of N-th array's data member `size' is not `const tice::v1::array::Idx'"
                                " (maybe not using tice::v1::internals::array::construct in the first place)");
                };

                template<bool arg__is_valid, v1::error::Pos arg__pos, typename arg__type>
                struct nested_array_type_is_valid {
                  static_assert(arg__is_valid,
                                "[DEBUG] Type of N-th array's member typedef `type' is not `element_type[size]'"
                                " (maybe not using tice::v1::internals::array::construct in the first place)");
                };

                template<bool arg__is_valid, v1::error::Pos arg__pos, typename arg__elems_type>
                struct nested_array_elems_type_is_valid {
                  static_assert(arg__is_valid,
                                "[DEBUG] Type of N-th array's data member `elems' is not `const element_type[size]'"
                                " (maybe not using tice::v1::internals::array::construct in the first place)");
                };

                template<bool arg__is_valid, v1::error::Pos arg__pos, typename arg__elems_ptr_type>
                struct nested_array_elems_ptr_type_is_valid {
                  static_assert(arg__is_valid,
                                "[DEBUG] Type of N-th array's data member `elems_ptr' is not `const element_type *[]'"
                                " (maybe not using tice::v1::internals::array::construct in the first place)");
                };

                template<bool arg__points_to_elems, v1::error::Pos arg__pos, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
                struct nested_array_elems_ptr_points_to_elems {
                  static_assert(arg__points_to_elems,
                                "[DEBUG] N-th array's data member `elems_ptr' fails to point to data member `elems' at index 0"
                                " (maybe not using tice::v1::internals::array::construct in the first place)");
                };

              }
            }
          }
          namespace update {
            namespace value {

              template<bool arg__has_element_type_as_member_typedef, typename arg__actual_parameter>
              struct arg_2_has_element_type_as_member_typedef {
                static_assert(arg__has_element_type_as_member_typedef,
                              "[DEBUG] Arg 2 has no member typedef `element_type'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__has_size_as_constexpr_copyable_data_member, typename arg__actual_parameter>
              struct arg_2_has_size_as_constexpr_copyable_data_member {
                static_assert(arg__has_size_as_constexpr_copyable_data_member,
                              "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__has_type_as_member_typedef, typename arg__actual_parameter>
              struct arg_2_has_type_as_member_typedef {
                static_assert(arg__has_type_as_member_typedef,
                              "[DEBUG] Arg 2 has no member typedef `type'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__has_elems_as_constexpr_copyable_data_member, typename arg__actual_parameter>
              struct arg_2_has_elems_as_constexpr_copyable_data_member {
                static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                              "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, typename arg__actual_parameter>
              struct arg_2_has_elems_ptr_as_constexpr_copyable_data_member {
                static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                              "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__is_valid, typename arg__element_type>
              struct arg_2_element_type_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Type of arg 2's member typedef `element_type' is not valid array element type"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__is_valid, typename arg__size_type>
              struct arg_2_size_type_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Type of arg 2's data member `size' is not `const tice::v1::array::Idx'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__is_valid, typename arg__type>
              struct arg_2_type_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Type of arg 2's member typedef `type' is not `element_type[size]'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__is_valid, typename arg__elems_type>
              struct arg_2_elems_type_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Type of arg 2's data member `elems' is not `const element_type[size]'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__is_valid, typename arg__elems_ptr_type>
              struct arg_2_elems_ptr_type_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Type of arg 2's data member `elems_ptr' is not `const element_type *[]'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
              struct arg_2_elems_ptr_points_to_elems {
                static_assert(arg__points_to_elems,
                              "[DEBUG] Arg 2's data member `elems_ptr' fails to point to data member `elems' at index 0"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__is_tuple_construct, typename arg__actual_parameter>
              struct arg_3_is_tuple_construct {
                static_assert(arg__is_tuple_construct,
                              "[DEBUG] Arg 3 is not class template tice::v1::internals::tuple::construct");
              };

              template<bool arg__is_member_of_on_dupd_index, typename arg__actual_parameter>
              struct arg_4_is_member_of_on_dupd_index {
                static_assert(arg__is_member_of_on_dupd_index,
                              "[DEBUG] Arg 4 is not member of namespace tice::v1::array::update::on_dupd_index");
              };

              template<bool arg__is_member_of_on_stray_index, typename arg__actual_parameter>
              struct arg_5_is_member_of_on_stray_index {
                static_assert(arg__is_member_of_on_stray_index,
                              "[DEBUG] Arg 5 is not member of namespace tice::v1::array::update::on_stray_index");
              };

              template<bool arg__is_within_array_bound, v1::error::Pos arg__element_pos, v1::dict::Key arg__key>
              struct update_list_key_is_within_array_bound {
                static_assert(arg__is_within_array_bound,
                              "[DEBUG] Update list key is outside array bound");
              };

              template<bool arg__has_no_duplicate, v1::error::Pos arg__element_1_pos, v1::error::Pos arg__element_2_pos, v1::dict::Key arg__key>
              struct update_list_key_has_no_duplicate {
                static_assert(arg__has_no_duplicate,
                              "[DEBUG] Update list key is duplicated");
              };

              template<bool arg__are_identical, v1::error::Pos arg__element_1_pos, v1::error::Pos arg__element_2_pos, v1::dict::Key arg__key>
              struct update_list_key_duplicates_are_identical {
                static_assert(arg__are_identical,
                              "[DEBUG] Update list key duplicates have non-identical associated values");
              };

              template<bool arg__has_key_as_constexpr_copyable_data_member, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
              struct update_list_element_has_key_as_constexpr_copyable_data_member {
                static_assert(arg__has_key_as_constexpr_copyable_data_member,
                              "[DEBUG] Element has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `key'"
                              " (maybe not using tice::v1::internals::dict::construct_entry)");
              };

              template<bool arg__has_value_as_constexpr_copyable_data_member, v1::error::Pos arg__element_pos, typename arg__actual_parameter>
              struct update_list_element_has_value_as_constexpr_copyable_data_member {
                static_assert(arg__has_value_as_constexpr_copyable_data_member,
                              "[DEBUG] Element has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `value'"
                              " (maybe not using tice::v1::internals::dict::construct_entry in data-carrying mode)");
              };

              template<bool arg__is_valid, v1::error::Pos arg__element_pos, typename arg__element_key_type>
              struct update_list_element_key_type_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Type of element's data member `key' is not tice::v1::dict::Key"
                              " (maybe not using tice::v1::internals::dict::construct_entry)");
              };

              template<bool arg__is_convertible_to_array_element_type, v1::error::Pos arg__element_pos,
                       typename arg__element_value_type, typename arg__array_element_type>
              struct update_list_element_value_is_convertible_to_array_element_type {
                static_assert(arg__is_convertible_to_array_element_type,
                              "[DEBUG] Element has its `value' nonconvertible to array element type");
              };

            }
          }
          namespace filter_then_map {
            namespace value {

              template<bool arg__has_element_type_as_member_typedef, typename arg__actual_parameter>
              struct arg_2_has_element_type_as_member_typedef {
                static_assert(arg__has_element_type_as_member_typedef,
                              "[DEBUG] Arg 2 has no member typedef `element_type'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__has_size_as_constexpr_copyable_data_member, typename arg__actual_parameter>
              struct arg_2_has_size_as_constexpr_copyable_data_member {
                static_assert(arg__has_size_as_constexpr_copyable_data_member,
                              "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__has_type_as_member_typedef, typename arg__actual_parameter>
              struct arg_2_has_type_as_member_typedef {
                static_assert(arg__has_type_as_member_typedef,
                              "[DEBUG] Arg 2 has no member typedef `type'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__has_elems_as_constexpr_copyable_data_member, typename arg__actual_parameter>
              struct arg_2_has_elems_as_constexpr_copyable_data_member {
                static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                              "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, typename arg__actual_parameter>
              struct arg_2_has_elems_ptr_as_constexpr_copyable_data_member {
                static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                              "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__is_valid, typename arg__element_type>
              struct arg_2_element_type_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Type of arg 2's member typedef `element_type' is not valid array element type"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__is_valid, typename arg__size_type>
              struct arg_2_size_type_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Type of arg 2's data member `size' is not `const tice::v1::array::Idx'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__is_valid, typename arg__type>
              struct arg_2_type_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Type of arg 2's member typedef `type' is not `element_type[size]'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__is_valid, typename arg__elems_type>
              struct arg_2_elems_type_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Type of arg 2's data member `elems' is not `const element_type[size]'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__is_valid, typename arg__elems_ptr_type>
              struct arg_2_elems_ptr_type_is_valid {
                static_assert(arg__is_valid,
                              "[DEBUG] Type of arg 2's data member `elems_ptr' is not `const element_type *[]'"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
              struct arg_2_elems_ptr_points_to_elems {
                static_assert(arg__points_to_elems,
                              "[DEBUG] Arg 2's data member `elems_ptr' fails to point to data member `elems' at index 0"
                              " (maybe not using tice::v1::internals::array::construct in the first place)");
              };

              template<bool arg__is_valid_array_element_type, typename arg__actual_parameter>
              struct arg_3_is_valid_array_element_type {
                static_assert(arg__is_valid_array_element_type,
                              "[DEBUG] Arg 3 is not valid array element type"
                              " (Arg 3 is either an incomplete type or `void' or a reference type or a function type or an abstract class)");
              };

              template<bool arg__is_not_nullptr>
              struct arg_4_is_not_nullptr {
                static_assert(arg__is_not_nullptr,
                              "[DEBUG] Arg 4 is a nullptr");
              };

            }
          }
          namespace elements_are_pairwise_distinct {

            template<bool arg__has_element_type_as_member_typedef, typename arg__actual_parameter>
            struct arg_2_has_element_type_as_member_typedef {
              static_assert(arg__has_element_type_as_member_typedef,
                            "[DEBUG] Arg 2 has no member typedef `element_type'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_size_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_size_as_constexpr_copyable_data_member {
              static_assert(arg__has_size_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_type_as_member_typedef, typename arg__actual_parameter>
            struct arg_2_has_type_as_member_typedef {
              static_assert(arg__has_type_as_member_typedef,
                            "[DEBUG] Arg 2 has no member typedef `type'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_elems_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_elems_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_elems_ptr_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__element_type>
            struct arg_2_element_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's member typedef `element_type' is not valid array element type"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__size_type>
            struct arg_2_size_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's data member `size' is not `const tice::v1::array::Idx'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__type>
            struct arg_2_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's member typedef `type' is not `element_type[size]'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_type>
            struct arg_2_elems_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's data member `elems' is not `const element_type[size]'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_ptr_type>
            struct arg_2_elems_ptr_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's data member `elems_ptr' is not `const element_type *[]'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
            struct arg_2_elems_ptr_points_to_elems {
              static_assert(arg__points_to_elems,
                            "[DEBUG] Arg 2's data member `elems_ptr' fails to point to data member `elems' at index 0"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

          }
          namespace are_elementwise_equal {

            template<bool arg__has_element_type_as_member_typedef, typename arg__actual_parameter>
            struct arg_2_has_element_type_as_member_typedef {
              static_assert(arg__has_element_type_as_member_typedef,
                            "[DEBUG] Arg 2 has no member typedef `element_type'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_size_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_size_as_constexpr_copyable_data_member {
              static_assert(arg__has_size_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_type_as_member_typedef, typename arg__actual_parameter>
            struct arg_2_has_type_as_member_typedef {
              static_assert(arg__has_type_as_member_typedef,
                            "[DEBUG] Arg 2 has no member typedef `type'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_elems_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_elems_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_elems_ptr_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__element_type>
            struct arg_2_element_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's member typedef `element_type' is not valid array element type"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__size_type>
            struct arg_2_size_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's data member `size' is not `const tice::v1::array::Idx'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__type>
            struct arg_2_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's member typedef `type' is not `element_type[size]'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_type>
            struct arg_2_elems_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's data member `elems' is not `const element_type[size]'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_ptr_type>
            struct arg_2_elems_ptr_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 2's data member `elems_ptr' is not `const element_type *[]'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
            struct arg_2_elems_ptr_points_to_elems {
              static_assert(arg__points_to_elems,
                            "[DEBUG] Arg 2's data member `elems_ptr' fails to point to data member `elems' at index 0"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_element_type_as_member_typedef>
            struct arg_3_has_element_type_as_member_typedef {
              static_assert(arg__has_element_type_as_member_typedef,
                            "[DEBUG] Arg 3 has no member typedef `element_type'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_size_as_constexpr_copyable_data_member>
            struct arg_3_has_size_as_constexpr_copyable_data_member {
              static_assert(arg__has_size_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 3 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_type_as_member_typedef>
            struct arg_3_has_type_as_member_typedef {
              static_assert(arg__has_type_as_member_typedef,
                            "[DEBUG] Arg 3 has no member typedef `type'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_elems_as_constexpr_copyable_data_member>
            struct arg_3_has_elems_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 3 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member>
            struct arg_3_has_elems_ptr_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                            "[DEBUG] Arg 3 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__element_type>
            struct arg_3_element_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 3's member typedef `element_type' is not valid array element type"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__size_type>
            struct arg_3_size_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 3's data member `size' is not `const tice::v1::array::Idx'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__type>
            struct arg_3_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 3's member typedef `type' is not `element_type[size]'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_type>
            struct arg_3_elems_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 3's data member `elems' is not `const element_type[size]'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_ptr_type>
            struct arg_3_elems_ptr_type_is_valid {
              static_assert(arg__is_valid,
                            "[DEBUG] Type of arg 3's data member `elems_ptr' is not `const element_type *[]'"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
            struct arg_3_elems_ptr_points_to_elems {
              static_assert(arg__points_to_elems,
                            "[DEBUG] Arg 3's data member `elems_ptr' fails to point to data member `elems' at index 0"
                            " (maybe not using tice::v1::internals::array::construct in the first place)");
            };

            template<bool arg__are_of_equal_element_type, typename arg__lhs_element_type, typename arg__rhs_element_type>
            struct arrays_are_of_equal_element_type {
              static_assert(arg__are_of_equal_element_type,
                            "[DEBUG] Arrays are not equal in element type");
            };

            template<bool arg__are_of_equal_size, v1::array::Size arg__lhs_size, v1::array::Size arg__rhs_size>
            struct arrays_are_of_equal_size {
              static_assert(arg__are_of_equal_size,
                            "[DEBUG] Arrays are not equal in size");
            };

          }
        }
      }
      namespace array {

#include "v1_internals_begin.hpp"

#define base_1(I, _1, _2) I                     \
        opts  (_1, arg__array_element_type, ()) \
          opts(_2, arg__N, ())
#define decl_1(I, _1, _2)                       \
        base_1(I##I,                            \
               type(_1, typename),              \
               type(_2, v1::array::Size))
#define args_1(I, _1, _2) base_1(I, _1, _2)
#define prms_1(...) decl_1(__VA_ARGS__)

        template<prms_1(I, _, _)>
        struct array_common__base;

#define base_2(args_1)                          \
        args_1
#define decl_2(I, _1_1, _1_2)                   \
        base_2(prms_1(I, _1_1, _1_2))
#define args_2(I, _1_1, _1_2) base_2(args_1(I, _1_1, _1_2))
#define prms_2(...) decl_2(__VA_ARGS__)

        template<prms_2(I, _, _)>
        struct array_common
        /* {
         *   typedef arg__array_element_type element_type;
         *   static constexpr v1::array::Size size {arg__N};
         *   if (arg__N > 0) {
         *     typedef element_type type[size];
         *   }
         * }
         */;

        

        template<prms_2(I, _, X)>
        struct array_common<args_2(I, _, R(0))> :
          array_common__base<args_1(I, _, R(0))> {
        };

        template<prms_2(I, _, _)>
        struct array_common :
#define base_class array_common__base<args_1(I, _, _)>
          base_class
        {
          typedef typename base_class::element_type type[base_class::size
                                                         /* > 0 as stipulated in ISO-IEC 14882:2014
                                                          * (C++14 standard) [dcl.array]p{1}
                                                          */             ];
        };
#undef base_class

        

        template<prms_1(I, _, _)>
        struct array_common__base {
          typedef arg__array_element_type element_type;
          static constexpr v1::array::Size size {arg__N};
        };

#include "v1_internals_end.hpp"
      }
      namespace array {

        template<typename I, typename arg>
        using element_type_is_valid = std::integral_constant<bool, H(std::is_object<arg>::value && !utility::is_abstract<I, arg>::value
                                                                     /* As stipulated in ISO-IEC 14882:2014 (C++14 standard)
                                                                      * [dcl.array]p{1}
                                                                      */                                                               )>;

      }
      namespace array {

#include "v1_internals_begin.hpp"

#define base_1A(_d3, I, _1, _2, _3) I                                                                           \
        opts  (_1, arg__is_empty_array, ())                                                                     \
          opts(_2, arg__array_element_type, ())                                                                 \
          opts(_3, arg__tuple_construct__of__std_integral_constant__of__array_element_type__elements, _d3)
#define decl_1A(_d3, I, _1, _2, _3)             \
        base_1A(_d3, I##I,                      \
                type(_1, bool),                 \
                type(_2, typename),             \
                type(_3, typename))
#define args_1A(I, _1, _2, _3) base_1A((), I, _1, _2, _3)
#define prms_1A(...) decl_1A((), __VA_ARGS__)

#define base_1B(_d1, _d2, _d3, I, _1, _2, _3) I         \
        opts  (_1, arg__size_checker_msg, _d1)          \
          opts(_2, arg__tuple_element_checker_msg, _d2) \
          opts(_3, arg__tuple_checker_msg, _d3)
#define decl_1B(_d1, _d2, _d3, I, _1, _2, _3)                                                                                           \
        base_1B(_d1, _d2, _d3, I##I,                                                                                                    \
                type(_1, template<bool arg__is_positive_> class),                                                                       \
                type(_2, template<bool arg__is_tuple_construct__of__std_integral_constant_, typename arg__actual_parameter_> class),    \
                type(_3, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class))
#define args_1B(I, _1, _2, _3) base_1B((), (), (), I, _1, _2, _3)
#define prms_1B(...) decl_1B((), (), (), __VA_ARGS__)

#define base_2AA(args_1A, args_1B)              \
        args_1A                                 \
        args_1B
#define decl_2AA(I, _1A_1, _1A_2, _1A_3, _1B_1, _1B_2, _1B_3)   \
        base_2AA(prms_1A(I, _1A_1, _1A_2, _1A_3),               \
                 prms_1B(, _1B_1, _1B_2, _1B_3))
#define args_2AA(I, _1A_1, _1A_2, _1A_3, _1B_1, _1B_2, _1B_3) base_2AA(args_1A(I, _1A_1, _1A_2, _1A_3), args_1B(, _1B_1, _1B_2, _1B_3))
#define prms_2AA(...) decl_2AA(__VA_ARGS__)

        template<prms_2AA(I, _, _, _, _, _, _)>
        struct construct__type_checked;

#define base_2AB(_d1, args_1A, _1, args_1B)                     \
        args_1A                                                 \
        opts  (_1, arg__array_element_type_checker_msg, _d1)    \
          args_1B
#define decl_2AB(_1A_d3, _d1, _1B_d1, _1B_d2, _1B_d3, I, _1A_1, _1A_2, _1A_3, _1, _1B_1, _1B_2, _1B_3)                  \
        base_2AB(_d1,                                                                                                   \
                 decl_1A(_1A_d3, I, _1A_1, _1A_2, _1A_3),                                                               \
                 type(_1, template<bool arg__is_valid_array_element_type_, typename arg__actual_parameter_> class),     \
                 decl_1B(_1B_d1, _1B_d2, _1B_d3, , _1B_1, _1B_2, _1B_3))
#define args_2AB(I, _1A_1, _1A_2, _1A_3, _1, _1B_1, _1B_2, _1B_3) base_2AB((), args_1A(I, _1A_1, _1A_2, _1A_3), _1, args_1B(, _1B_1, _1B_2, _1B_3))
#define prms_2AB(...) decl_2AB((), (), (), (), (), __VA_ARGS__)

        template<decl_2AB(_1A_d3(tuple::construct<I>),
                          _d1   (error::array::construct::arg_3_is_valid_array_element_type),
                          _1B_d1(error::array::construct::arg_4_is_tuple_construct_with_positive_size),
                          _1B_d2(error::array::construct::arg_4_is_tuple_construct__of__std_integral_constant),
                          _1B_d3(error::array::construct::arg_4_is_tuple_construct),
                          I, _, _, _, _, _, _, _)>
        struct construct
        /* {
         *   include array_common<I,
         *                        arg__array_element_type,
         *                        (arg__is_empty_array
         *                         ? 0
         *                         : tuple::size<I, arg__tuple_construct__of__std_integral_constant__of__array_element_type__elements>::value)>;
         *   if (!arg__is_empty_array) {
         *     with template<typename I, typename arg__element_type, arg__element_type... args__element>
         *          tuple::construct<I, std::integral_constant<arg__element_type, args__element>...>
         *          = arg__tuple_construct__of__std_integral_constant__of__array_element_type__elements
         *     {
         *       static constexpr typename self::type elems = {args__element...};
         *       static constexpr const typename self::element_type *elems_ptr[1] = {elems};
         *     }
         *   }
         * }
         */;

        

        template<prms_2AB(I, _, _, _, _, _, _, _)>
        struct construct :
          arg__array_element_type_checker_msg<element_type_is_valid<I, arg__array_element_type>::value, arg__array_element_type>,
          construct__type_checked<args_2AA(I, _, _, _, _, _, _)> {
        };

        

        template<prms_2AA(I, X, _, _, _, _, _)>
        struct construct__type_checked<args_2AA(I, R(true), _, _, _, _, _)> :
          array_common<I, arg__array_element_type, 0> {
        };

#define prms_2AA_1(I, _1A_3) prms_2AA(I, X, _, _1A_3, _, _, _)
#define args_2AA_1(I, _1A_3) args_2AA(I, R(false), _, _1A_3, _, _, _)

        template<prms_2AA_1(I, _)>
        struct construct__type_checked<args_2AA_1(I, _)> :
          arg__tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct__of__std_integral_constant__of__array_element_type__elements> {
        };

        template<prms_2AA_1(I, R(typename... args__element))>
        struct construct__type_checked<args_2AA_1(I, R(tuple::construct<I, args__element...>))> :
          arg__tuple_element_checker_msg<!!utility::always_false<I>(), tuple::construct<I, args__element...>> {
        };

        template<prms_2AA_1(I, R(arg__array_element_type... args__element))>
        struct construct__type_checked<args_2AA_1(I, R(tuple::construct<I, std::integral_constant<arg__array_element_type, args__element>...>))> :
          arg__size_checker_msg<(sizeof...(args__element) > 0)>,
#define base_class array_common<I, arg__array_element_type, sizeof...(args__element)>
          base_class
        {
          static constexpr typename base_class::type elems {args__element...};
          static constexpr const typename base_class::element_type *elems_ptr[1] {elems};
        };
#undef base_class

#include "v1_internals_end.hpp"
      }
      namespace array {
        namespace make {

#include "v1_internals_begin.hpp"

#define base_1A(_d3, I, _1, _2, _3) I                                                   \
          opts  (_1, arg__N, ())                                                        \
            opts(_2, arg__init_val_with_value_as_constexpr_copyable_data_member, ())    \
            opts(_3, arg__array_element_type, _d3)
#define decl_1A(_d3, I, _1, _2, _3)             \
          base_1A(_d3, I##I,                    \
                  type(_1, v1::array::Size),    \
                  type(_2, typename),           \
                  type(_3, typename))
#define args_1A(I, _1, _2, _3) base_1A((), I, _1, _2, _3)
#define prms_1A(...) decl_1A((), __VA_ARGS__)

#define base_1B(_d1, I, _1) I                           \
          opts  (_1, arg__type_checker_msg, _d1)
#define decl_1B(_d1, I, _1)                                                                                     \
          base_1B(_d1, I##I,                                                                                    \
                  type(_1, template<H(bool arg__is_convertible_to_array_element_type_,                          \
                                      typename arg__value_type_, typename arg__array_element_type_)> class))
#define args_1B(I, _1) base_1B((), I, _1)
#define prms_1B(...) decl_1B((), __VA_ARGS__)

#define base_2B(args_1A, args_1B)               \
          args_1A                               \
          args_1B
#define decl_2B(I, _1A_1, _1A_2, _1A_3, _1B_1)          \
          base_2B(prms_1A(I, _1A_1, _1A_2, _1A_3),      \
                  prms_1B(, _1B_1))
#define args_2B(I, _1A_1, _1A_2, _1A_3, _1B_1) base_2B(args_1A(I, _1A_1, _1A_2, _1A_3), args_1B(, _1B_1))
#define prms_2B(...) decl_2B(__VA_ARGS__)

          template<prms_2B(I, _, _, _, _)>
          struct value__with_value;

#define base_2AA(_d1, _d2, args_1A, _1, args_1B, _2)    \
          args_1A                                       \
          opts  (_1, arg__size_checker_msg, _d1)        \
            args_1B                                     \
            opts(_2, arg__value_checker_msg, _d2)
#define decl_2AA(_1A_d3, _d1, _1B_d1, _d2, I, _1A_1, _1A_2, _1A_3, _1, _1B_1, _2)                                                       \
          base_2AA(_d1, _d2,                                                                                                            \
                   decl_1A(_1A_d3, I, _1A_1, _1A_2, _1A_3),                                                                             \
                   type(_1, template<bool arg__is_positive_, v1::array::Size arg__size_> class),                                        \
                   decl_1B(_1B_d1, , _1B_1),                                                                                            \
                   type(_2, template<bool arg__has_value_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class))
#define args_2AA(I, _1A_1, _1A_2, _1A_3, _1, _1B_1, _2) base_2AA((), (), args_1A(I, _1A_1, _1A_2, _1A_3), _1, args_1B(, _1B_1), _2)
#define prms_2AA(...) decl_2AA((), (), (), (), __VA_ARGS__)

          template<decl_2AA(_1A_d3(tice::v1::array::make::init_val_value_type),
                            _d1   (error::array::make::value::arg_2_is_positive),
                            _1B_d1(error::array::make::value::value_is_convertible_to_array_element_type),
                            _d2   (error::array::make::value::arg_3_has_value_as_constexpr_copyable_data_member),
                            I, _, _, _, _, _, _)>
          struct value
          /* {
           *   invariant {
           *     arg__N > 0;
           *   }
           *   include array_common<I,
           *                        (arg__array_element_type == tice::v1::array::make::init_val_value_type
           *                         ? std::remove_const_t<
           *                             std::remove_reference_t<decltype(arg__init_val_with_value_as_constexpr_copyable_data_member::value)>>
           *                         : arg__array_element_type),
           *                        arg__N>;
           *   static constexpr typename self::type elems = {
           *     arg__init_val_with_value_as_constexpr_copyable_data_member::value, // #0
           *     arg__init_val_with_value_as_constexpr_copyable_data_member::value, // #1
           *     ...
           *     arg__init_val_with_value_as_constexpr_copyable_data_member::value  // #(arg__N - 1)
           *   };
           *   static constexpr const typename self::element_type *elems_ptr[1] = {elems};
           * }
           */;

#define base_2AB(_d2, I, _1, args_1A, _2) I     \
          opts  (_1, arg__i_reversed, ())       \
            args_1A                             \
            opts(_2, args__element, _d2)
#define decl_2AB(I, _1, _1A_1, _1A_2, _1A_3, _2)        \
          base_2AB((), I##I,                            \
                   type(_1, v1::array::Idx),            \
                   prms_1A(, _1A_1, _1A_2, _1A_3),      \
                   type(_2, typename...))
#define args_2AB(I, _1, _1A_1, _1A_2, _1A_3, _2) base_2AB((...), I, _1, args_1A(, _1A_1, _1A_2, _1A_3), _2)
#define prms_2AB(...) decl_2AB(__VA_ARGS__)

          template<prms_2AB(I, _, _, _, _, _)>
          struct value__make;

          

          template<prms_2AA(I, _, _, _, _, _, _)>
          struct value :
            arg__size_checker_msg<(arg__N > 0), arg__N>,
            arg__value_checker_msg<utility::has_value_as_constexpr_copyable_data_member<
                                     H(I, arg__init_val_with_value_as_constexpr_copyable_data_member)>::value,
                                   arg__init_val_with_value_as_constexpr_copyable_data_member>,
            value__with_value<args_2B(I, _, _, _, _)> {
          };

          

          template<prms_2B(I, _, _, _, _)>
          struct value__with_value :
            arg__type_checker_msg<std::is_convertible<H(std::remove_reference_t<
                                                        decltype(arg__init_val_with_value_as_constexpr_copyable_data_member::value)>),
                                                      arg__array_element_type>::value,
                                  std::remove_reference_t<decltype(arg__init_val_with_value_as_constexpr_copyable_data_member::value)>,
                                  arg__array_element_type>,
            value__make<args_2AB(I, R(arg__N), _, _, _, X)> {
          };

          template<prms_2B(I, _, _, X, _)>
          struct value__with_value<args_2B(I, _, _, R(v1::array::make::init_val_value_type), _)> :
            value__with_value<args_2B(I, _, _, R(std::remove_const_t<
                                                 H(std::remove_reference_t<
                                                   H(decltype(arg__init_val_with_value_as_constexpr_copyable_data_member::value))>)>), _)> {
          };

          

          template<prms_2AB(I, X, _, _, _, _)>
          struct value__make<args_2AB(I, R(0), _, _, _, _)> :
#define base_class array_common<I, arg__array_element_type, sizeof...(args__element)>
            base_class
          {
            static constexpr typename base_class::type elems {static_cast<typename base_class::element_type>(args__element::value)...};
            // The `static_cast' is used to allow narrowing (e.g., `double' to `int'), which passes the `std::is_convertible' test.
            // The `static_cast', however, does not work around the case where an explicit conversion is required because
            // such case fails the `std::is_convertible' test.
            static constexpr const typename base_class::element_type *elems_ptr[1] {elems};
          };
#undef base_class

          template<prms_2AB(I, _, _, _, _, _)>
          struct value__make :
            value__make<args_2AB(I, R(arg__i_reversed - 1), _, _, _,
                                 R(arg__init_val_with_value_as_constexpr_copyable_data_member, args__element...))> {
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace array {
        namespace make {
          namespace from_tuple {

#include "v1_internals_begin.hpp"

#define base_1(_d2, _d3, _d4, I, _1, _2, _3, _4) I                                                              \
            opts  (_1, arg__tuple_construct /* __of__type_with_value_as_constexpr_copyable_data_member */, ())  \
              opts(_2, arg__array_element_type, _d2)                                                            \
              opts(_3, arg__value_checker_msg, _d3)                                                             \
              opts(_4, arg__type_checker_msg, _d4)
#define decl_1(_d2, _d3, _d4, I, _1, _2, _3, _4)                                                                                        \
            base_1(_d2, _d3, _d4, I##I,                                                                                                 \
                   type(_1, typename),                                                                                                  \
                   type(_2, typename),                                                                                                  \
                   type(_3, template<H(bool arg__has_value_as_constexpr_copyable_data_member_, v1::error::Pos arg__element_pos_,        \
                                       typename arg__element_)> class),                                                                 \
                   type(_4, template<H(bool arg__is_convertible_to_array_element_type_, v1::error::Pos arg__element_pos_,               \
                                       typename arg__element_value_type_, typename arg__array_element_type_)> class))
#define args_1(I, _1, _2, _3, _4) base_1((), (), (), I, _1, _2, _3, _4)
#define prms_1(...) decl_1((), (), (), __VA_ARGS__)

#define base_2A(_d1, _d2, args_1, _1, _2)               \
            args_1                                      \
            opts  (_1, arg__tuple_checker_msg, _d1)     \
              opts(_2, arg__size_checker_msg, _d2)
#define decl_2A(_1_d2, _1_d3, _1_d4, _d1, _d2, I, _1_1, _1_2, _1_3, _1_4, _1, _2)                       \
            base_2A(_d1, _d2,                                                                           \
                    decl_1(_1_d2, _1_d3, _1_d4, I, _1_1, _1_2, _1_3, _1_4),                             \
                    type(_1, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class), \
                    type(_2, template<bool arg__is_nonempty_> class))
#define args_2A(I, _1_1, _1_2, _1_3, _1_4, _1, _2) base_2A((), (), args_1(I, _1_1, _1_2, _1_3, _1_4), _1, _2)
#define prms_2A(...) decl_2A((), (), (), (), (), __VA_ARGS__)

            template<decl_2A(_1_d2(v1::array::make::from_tuple::first_element_value_type),
                             _1_d3(error::array::make::from_tuple::value::element_has_value_as_constexpr_copyable_data_member),
                             _1_d4(error::array::make::from_tuple::value::element_value_is_convertible_to_array_element_type),
                             _d1  (error::array::make::from_tuple::value::arg_2_is_tuple_construct),
                             _d2  (error::array::make::from_tuple::value::arg_2_is_nonempty),
                             I, _, _, _, _, _, _)>
            struct value
            /* {
             *   invariant {
             *     tuple::size<I, arg__tuple_construct>::value > 0;
             *   }
             *   include array_common<I,
             *                        (arg__array_element_type == v1::array::make::from_tuple::first_element_value_type
             *                         ? std::remove_const_t<std::remove_reference_t<decltype(tuple::get_pos<I, 1,
             *                                                                                               arg__tuple_construct>::value::value)>>
             *                         : arg__array_element_type),
             *                        tuple::size<I, arg__tuple_construct>::value>;
             *   with template<typename I, typename... args__element>
             *        tuple::construct<I, args__element...> = arg__tuple_construct
             *   {
             *     static constexpr typename self::type elems = {args__element::value...};
             *     static constexpr const typename self::element_type *elems_ptr[1] = {elems};
             *   }
             * }
             */;

#define base_2B(_d1, args_1, _1)                \
            args_1                              \
            opts  (_1, args__element, _d1)
#define decl_2B(I, _1_1, _1_2, _1_3, _1_4, _1)          \
            base_2B((),                                 \
                    prms_1(I, _1_1, _1_2, _1_3, _1_4),  \
                    type(_1, typename...))
#define args_2B(I, _1_1, _1_2, _1_3, _1_4, _1) base_2B((...), args_1(I, _1_1, _1_2, _1_3, _1_4), _1)
#define prms_2B(...) decl_2B(__VA_ARGS__)

            template<prms_2B(I, _, _, _, _, _)>
            struct value__next;

#define base_3B(I, _1, _2, args_2B) I                   \
            opts  (_1, arg__has_value, ())              \
              opts(_2, arg__element_to_append, ())      \
              args_2B
#define decl_3B(I, _1, _2, _1_1, _1_2, _1_3, _1_4, _2B_1)       \
            base_3B(I##I,                                       \
                    type(_1, bool),                             \
                    type(_2, typename),                         \
                    prms_2B(, _1_1, _1_2, _1_3, _1_4, _2B_1))
#define args_3B(I, _1, _2, _1_1, _1_2, _1_3, _1_4, _2B_1) base_3B(I, _1, _2, args_2B(, _1_1, _1_2, _1_3, _1_4, _2B_1))
#define prms_3B(...) decl_3B(__VA_ARGS__)

            template<prms_3B(I, _, _, _, _, _, _, _)>
            struct value__append;

            

            template<prms_2A(I, _, _, _, _, _, _)>
            struct value :
              arg__tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct> {
            };

#define prms_2A_1(I, _1_1) prms_2A(I, _1_1, _, _, _, _, _)
#define args_2A_1(I, _1_1) args_2A(I, _1_1, _, _, _, _, _)

            template<prms_2A_1(I, R(typename... args__element))>
            struct value<args_2A_1(I, R(Tuple<args__element...>))> :
              value<args_2A_1(I, R(tuple::construct<I, args__element...>))> {
            };

            template<prms_2A_1(I, X)>
            struct value<args_2A_1(I, R(tuple::construct<I>))> :
              arg__size_checker_msg<!!utility::always_false<I>()> {
            };

            template<prms_2A_1(I, R(typename arg__element, typename... args__remaining_element))>
            struct value<args_2A_1(I, R(tuple::construct<I, arg__element, args__remaining_element...>))> :
              value__next<args_2B(I, R(tuple::construct<I, arg__element, args__remaining_element...>), _, _, _, X)> {
            };

            

            template<prms_2B(I, _, _, _, _, _)>
            struct value__next :
#define base_class array_common<I, arg__array_element_type, sizeof...(args__element)>
              base_class
            {
              static constexpr typename base_class::type elems {static_cast<typename base_class::element_type>(args__element::value)...};
              // The `static_cast' is used to allow narrowing (e.g., `double' to `int'), which passes the `std::is_convertible' test.
              // The `static_cast', however, does not work around the case where an explicit conversion is required because
              // such case fails the `std::is_convertible' test.
              static constexpr const typename base_class::element_type *elems_ptr[1] {elems};
            };
#undef base_class

            template<prms_2B(I, R(typename arg__element, typename... args__remaining_element), _, _, _, _)>
            struct value__next<args_2B(I, R(tuple::construct<I, arg__element, args__remaining_element...>), _, _, _, _)> :
              value__append<args_3B(I,
                                    R(utility::has_value_as_constexpr_copyable_data_member<I, arg__element>::value),
                                    R(arg__element), R(tuple::construct<I, args__remaining_element...>), _, _, _, _)> {
            };

            

            template<prms_3B(I, _, _, _, _, _, _, _)>
            struct value__append :
              arg__value_checker_msg<arg__has_value, sizeof...(args__element) + 1, arg__element_to_append> {
            };

#define prms_3B_1(I, _1_2) prms_3B(I, X, _, _, _1_2, _, _, _)
#define args_3B_1(I, _1_2) args_3B(I, R(true), _, _, _1_2, _, _, _)

            template<prms_3B_1(I, X)>
            struct value__append<args_3B_1(I, R(v1::array::make::from_tuple::first_element_value_type))> :
              value__append<args_3B_1(I, R(std::remove_const_t<std::remove_reference_t<decltype(arg__element_to_append::value)>>))> {
            };

            template<prms_3B_1(I, _)>
            struct value__append<args_3B_1(I, _)> :
              arg__type_checker_msg<H(std::is_convertible<H(std::remove_reference_t<decltype(arg__element_to_append::value)>,
                                                            arg__array_element_type)>::value,
                                      sizeof...(args__element) + 1,
                                      std::remove_reference_t<decltype(arg__element_to_append::value)>, arg__array_element_type)>,
              value__next<args_2B(I, _, _, _, _, R(args__element..., arg__element_to_append))> {
            };

#include "v1_internals_end.hpp"
          }
        }
      }
      namespace array {

#include "v1_internals_begin.hpp"

#define base_1(I, _1) I                         \
        opts  (_1, arg, ())
#define decl_1(I, _1)                           \
        base_1(I##I,                            \
               type(_1, typename))
#define args_1(I, _1) base_1(I, _1)
#define prms_1(...) decl_1(__VA_ARGS__)

        template<prms_1(I, _)>
        struct validate__base_class;

#define base_2(args_1, _1, _2, _3, _4)                          \
        args_1                                                  \
        opts  (_1, arg__type_validator_msg, ())                 \
          opts(_2, arg__elems_type_validator_msg, ())           \
          opts(_3, arg__elems_ptr_type_validator_msg, ())       \
          opts(_4, arg__elems_ptr_value_checker_msg, ())
#define decl_2(I, _1_1, _1, _2, _3, _4)                                                                                                 \
        base_2(prms_1(I, _1_1),                                                                                                         \
               type(_1, template<bool arg__is_valid_, typename arg__type_> class),                                                      \
               type(_2, template<bool arg__is_valid_, typename arg__elems_type_> class),                                                \
               type(_3, template<bool arg__is_valid_, typename arg__elems_ptr_type_> class),                                            \
               type(_4, template<bool arg__is_valid_, typename arg__elems_ptr_type_, arg__elems_ptr_type_ arg__elems_ptr_> class))
#define args_2(I, _1_1, _1, _2, _3, _4) base_2(args_1(I, _1_1), _1, _2, _3, _4)
#define prms_2(...) decl_2(__VA_ARGS__)

        template<prms_2(I, _, _, _, _, _)>
        struct validate__elems;

#define base_3(args_2, _1, _2, _3)                      \
        args_2                                          \
        opts  (_1, arg__type_checker_msg, ())           \
          opts(_2, arg__elems_checker_msg, ())          \
          opts(_3, arg__elems_ptr_checker_msg, ())
#define decl_3(I, _1_1, _2_1, _2_2, _2_3, _2_4, _1, _2, _3)                                                                             \
        base_3(prms_2(I, _1_1, _2_1, _2_2, _2_3, _2_4),                                                                                 \
               type(_1, template<bool arg__has_type_as_member_typedef_, typename arg__actual_parameter_> class),                        \
               type(_2, template<bool arg__has_elems_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),       \
               type(_3, template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class))
#define args_3(I, _1_1, _2_1, _2_2, _2_3, _2_4, _1, _2, _3) base_3(args_2(I, _1_1, _2_1, _2_2, _2_3, _2_4), _1, _2, _3)
#define prms_3(...) decl_3(__VA_ARGS__)

        template<prms_3(I, _, _, _, _, _, _, _, _)>
        struct validate__emptiness_check;

#define base_4A(args_3, _1)                     \
        args_3                                  \
        opts  (_1, arg__array_is_empty, ())
#define decl_4A(I, _1_1, _2_1, _2_2, _2_3, _2_4, _3_1, _3_2, _3_3, _1)            \
        base_4A(prms_3(I, _1_1, _2_1, _2_2, _2_3, _2_4, _3_1, _3_2, _3_3),        \
                type(_1, bool))
#define args_4A(I, _1_1, _2_1, _2_2, _2_3, _2_4, _3_1, _3_2, _3_3, _1) base_4A(args_3(I, _1_1, _2_1, _2_2, _2_3, _2_4, _3_1, _3_2, _3_3), _1)
#define prms_4A(...) decl_4A(__VA_ARGS__)

        template<prms_4A(I, _, _, _, _, _, _, _, _, _)>
        struct validate__empty_array;

#define base_4B(args_3, _1, _2)                         \
        args_3                                          \
        opts  (_1, arg__element_type_validator_msg, ()) \
          opts(_2, arg__size_type_validator_msg, ())
#define decl_4B(I, _1_1, _2_1, _2_2, _2_3, _2_4, _3_1, _3_2, _3_3, _1, _2)                      \
        base_4B(decl_3(I, _1_1, _2_1, _2_2, _2_3, _2_4, _3_1, _3_2, _3_3),                      \
                type(_1, template<bool arg__is_valid_, typename arg__element_type_> class),     \
                type(_2, template<bool arg__is_valid_, typename arg__size_type_> class))
#define args_4B(I, _1_1, _2_1, _2_2, _2_3, _2_4, _3_1, _3_2, _3_3, _1, _2)              \
        base_4B(args_3(I, _1_1, _2_1, _2_2, _2_3, _2_4, _3_1, _3_2, _3_3), _1, _2)
#define prms_4B(...) decl_4B(__VA_ARGS__)

        template<prms_4B(I, _, _, _, _, _, _, _, _, _, _)>
        struct validate__element_type__and__size;

#define base_5B(args_4B, _1, _2)                        \
        args_4B                                         \
        opts  (_1, arg__element_type_checker_msg, ())   \
          opts(_2, arg__size_checker_msg, ())
#define decl_5B(I, _1_1, _2_1, _2_2, _2_3, _2_4, _3_1, _3_2, _3_3, _4B_1, _4B_2, _1, _2)                                                \
        base_5B(decl_4B(I, _1_1, _2_1, _2_2, _2_3, _2_4, _3_1, _3_2, _3_3, _4B_1, _4B_2),                                               \
                type(_1, template<bool arg__has_element_type_as_member_typedef_, typename arg__actual_parameter_> class),               \
                type(_2, template<bool arg__has_size_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class))
#define args_5B(I, _1_1, _2_1, _2_2, _2_3, _2_4, _3_1, _3_2, _3_3, _4B_1, _4B_2, _1, _2)                \
        base_5B(args_4B(I, _1_1, _2_1, _2_2, _2_3, _2_4, _3_1, _3_2, _3_3, _4B_1, _4B_2), _1, _2)
#define prms_5B(...) decl_5B(__VA_ARGS__)

        template<prms_5B(I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct validate;

        

        template<prms_5B(I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct validate :
          arg__element_type_checker_msg<utility::has_element_type_as_member_typedef<I, arg>::value, arg>,
          arg__size_checker_msg<utility::has_size_as_constexpr_copyable_data_member<I, arg>::value, arg>,
          validate__element_type__and__size<args_4B(I, _, _, _, _, _, _, _, _, _, _)> {
        };

        

        template<prms_4B(I, _, _, _, _, _, _, _, _, _, _)>
        struct validate__element_type__and__size :
          arg__element_type_validator_msg<element_type_is_valid<I, typename arg::element_type>::value, typename arg::element_type>,
          arg__size_type_validator_msg<std::is_same<std::add_const_t<v1::array::Idx>, decltype(arg::size)>::value, decltype(arg::size)>,
          validate__emptiness_check<args_3(I, _, _, _, _, _, _, _, _)> {
        };

        

        template<prms_3(I, _, _, _, _, _, _, _, _)>
        struct validate__emptiness_check :
          validate__empty_array<args_4A(I, _, _, _, _, _, _, _, _, R(arg::size == 0))> {
        };

        

        template<prms_4A(I, _, _, _, _, _, _, _, _, X)>
        struct validate__empty_array<args_4A(I, _, _, _, _, _, _, _, _, R(true))> {
        };

        template<prms_4A(I, _, _, _, _, _, _, _, _, _)>
        struct validate__empty_array :
          arg__type_checker_msg<utility::has_type_as_member_typedef<I, arg>::value, arg>,
          arg__elems_checker_msg<utility::has_elems_as_constexpr_copyable_data_member<I, arg>::value, arg>,
          arg__elems_ptr_checker_msg<utility::has_elems_ptr_as_constexpr_copyable_data_member<I, arg>::value, arg>,
          validate__elems<args_2(I, _, _, _, _, _)> {
        };

        

        template<prms_2(I, _, _, _, _, _)>
        struct validate__elems :
          arg__type_validator_msg<std::is_same<typename arg::element_type[arg::size], typename arg::type>::value, typename arg::type>,
          arg__elems_type_validator_msg<std::is_same<const typename arg::element_type[arg::size],
                                                     std::remove_reference_t<decltype(arg::elems)>>::value,
                                        std::remove_reference_t<decltype(arg::elems)>>,
          arg__elems_ptr_type_validator_msg<std::is_array<decltype(arg::elems_ptr)>::value
                                            && std::is_same<const typename arg::element_type *const *,
                                                            std::decay_t<decltype(arg::elems_ptr)>>::value,
                                            decltype(arg::elems_ptr)>,
#define base_class validate__base_class<args_1(I, _)>
          base_class
        {
          run_metaprogram(arg__elems_ptr_value_checker_msg<H(utility::arrays_are_point_equal<H(I, decltype(arg::elems_ptr),
                                                                                               decltype(base_class::expected_elems_ptr),
                                                                                               &arg::elems_ptr,
                                                                                               &base_class::expected_elems_ptr,
                                                                                               0)>::value,
                                                             const typename arg::element_type *const *, arg::elems_ptr)>);
        };
#undef base_class

        

        template<prms_1(I, _)>
        struct validate__base_class
        {
        protected:
          static constexpr const typename arg::element_type *expected_elems_ptr[1] {arg::elems};
        };

#include "v1_internals_end.hpp"
      }
      namespace array {

        template<typename I, v1::error::Pos arg__pos, typename arg,
                 H(template<bool arg__is_valid_, v1::error::Pos arg__pos_, typename arg__type_>
                   class arg__array_type_validator_msg),
                 H(template<bool arg__is_valid_, v1::error::Pos arg__pos_, typename arg__elems_type_>
                   class arg__array_elems_type_validator_msg),
                 H(template<bool arg__is_valid_, v1::error::Pos arg__pos_, typename arg__elems_ptr_type_>
                   class arg__array_elems_ptr_validator_msg),
                 H(template<H(bool arg__points_to_elems_, v1::error::Pos arg__pos_,
                              typename arg__elems_ptr_type_, arg__elems_ptr_type_ arg__elems_ptr_)>
                   class arg__array_elems_ptr_value_checker_msg),
                 H(template<bool arg__has_type_as_member_typedef_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
                   class arg__array_type_checker_msg),
                 H(template<bool arg__has_elems_as_constexpr_copyable_data_member_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
                   class arg__array_elems_checker_msg),
                 H(template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
                   class arg__array_elems_ptr_checker_msg),
                 H(template<bool arg__is_valid_, v1::error::Pos arg__pos_, typename arg__element_type_>
                   class arg__array_element_type_validator_msg),
                 H(template<bool arg__is_valid_, v1::error::Pos arg__pos_, typename arg__size_type_>
                   class arg__array_size_validator_msg),
                 H(template<bool arg__has_element_type_as_member_typedef_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
                   class arg__array_element_type_checker_msg),
                 H(template<bool arg__has_size_as_constexpr_copyable_data_member_, v1::error::Pos arg__pos_, typename arg__actual_parameter_>
                   class arg__array_size_checker_msg)>
        struct validate_with_pos {
          template<bool arg__is_valid_, typename arg__type_>
          using arg__array_type_validator_msg_ = arg__array_type_validator_msg<arg__is_valid_, arg__pos, arg__type_>;

          template<bool arg__is_valid_, typename arg__elems_type_>
          using arg__array_elems_type_validator_msg_ = arg__array_elems_type_validator_msg<arg__is_valid_, arg__pos, arg__elems_type_>;

          template<bool arg__is_valid_, typename arg__elems_ptr_type_>
          using arg__array_elems_ptr_validator_msg_ = arg__array_elems_ptr_validator_msg<arg__is_valid_, arg__pos, arg__elems_ptr_type_>;

          template<bool arg__points_to_elems_, typename arg__elems_ptr_type_, arg__elems_ptr_type_ arg__elems_ptr_>
          using arg__array_elems_ptr_value_checker_msg_ = arg__array_elems_ptr_value_checker_msg<arg__points_to_elems_, arg__pos,
                                                                                                 arg__elems_ptr_type_, arg__elems_ptr_>;

          template<bool arg__has_type_as_member_typedef_, typename arg__actual_parameter_>
          using arg__array_type_checker_msg_ = arg__array_type_checker_msg<arg__has_type_as_member_typedef_, arg__pos, arg__actual_parameter_>;

          template<bool arg__has_elems_as_constexpr_copyable_data_member_, typename arg__actual_parameter_>
          using arg__array_elems_checker_msg_ = arg__array_elems_checker_msg<arg__has_elems_as_constexpr_copyable_data_member_, arg__pos,
                                                                             arg__actual_parameter_>;

          template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member_, typename arg__actual_parameter_>
          using arg__array_elems_ptr_checker_msg_ = arg__array_elems_ptr_checker_msg<arg__has_elems_ptr_as_constexpr_copyable_data_member_,
                                                                                     arg__pos, arg__actual_parameter_>;

          template<bool arg__is_valid_, typename arg__element_type_>
          using arg__array_element_type_validator_msg_ = arg__array_element_type_validator_msg<arg__is_valid_, arg__pos, arg__element_type_>;

          template<bool arg__is_valid_, typename arg__size_type_>
          using arg__array_size_validator_msg_ = arg__array_size_validator_msg<arg__is_valid_, arg__pos, arg__size_type_>;

          template<bool arg__has_element_type_as_member_typedef_, typename arg__actual_parameter_>
          using arg__array_element_type_checker_msg_ = arg__array_element_type_checker_msg<arg__has_element_type_as_member_typedef_, arg__pos,
                                                                                           arg__actual_parameter_>;

          template<bool arg__has_size_as_constexpr_copyable_data_member_, typename arg__actual_parameter_>
          using arg__array_size_checker_msg_ = arg__array_size_checker_msg<arg__has_size_as_constexpr_copyable_data_member_, arg__pos,
                                                                           arg__actual_parameter_>;

          run_metaprogram(validate<H(I, arg, arg__array_type_validator_msg_, arg__array_elems_type_validator_msg_,
                                     arg__array_elems_ptr_validator_msg_, arg__array_elems_ptr_value_checker_msg_,
                                     arg__array_type_checker_msg_, arg__array_elems_checker_msg_, arg__array_elems_ptr_checker_msg_,
                                     arg__array_element_type_validator_msg_, arg__array_size_validator_msg_,
                                     arg__array_element_type_checker_msg_, arg__array_size_checker_msg_)>);
        };

      }
      namespace array {
        namespace make {
          namespace nested {

#include "v1_internals_begin.hpp"

#define base_1(_d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14,    \
               I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14) I        \
            opts     (_1 , arg__tuple_construct__of__Array, ())                         \
                 opts(_2 , arg__nested_array_uniform_element_type_checker_msg, _d2)     \
                 opts(_3 , arg__nested_array_positive_size_checker_msg, _d3)            \
                 opts(_4 , arg__nested_array_element_type_checker_msg, _d4)             \
            opts     (_5 , arg__nested_array_size_checker_msg, _d5)                     \
            opts     (_6 , arg__nested_array_type_checker_msg, _d6)                     \
            opts     (_7 , arg__nested_array_elems_checker_msg, _d7)                    \
            opts     (_8 , arg__nested_array_elems_ptr_checker_msg, _d8)                \
            opts     (_9 , arg__nested_array_element_type_validator_msg, _d9)           \
            opts     (_10, arg__nested_array_size_validator_msg, _d10)                  \
            opts     (_11, arg__nested_array_type_validator_msg, _d11)                  \
            opts     (_12, arg__nested_array_elems_type_validator_msg, _d12)            \
            opts     (_13, arg__nested_array_elems_ptr_validator_msg, _d13)             \
            opts     (_14, arg__nested_array_elems_ptr_value_checker_msg, _d14)
#define decl_1(_d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14,                                            \
               I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14)                                                  \
            base_1(_d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, I##I,                                  \
                   type(_1, typename),                                                                                          \
                   type(_2, template<H(bool arg__is_uniform_, v1::error::Pos arg__pos_,                                         \
                                       typename arg__array_element_type_ , typename arg__first_array_element_type_)> class),    \
                   type(_3, template<bool arg__is_positive_, v1::error::Pos arg__pos_, v1::array::Size arg__size_> class),      \
                   type(_4, template<H(bool arg__has_element_type_as_member_typedef_, v1::error::Pos arg__pos_,                 \
                                       typename arg__actual_parameter_)> class),                                                \
                   type(_5, template<H(bool arg__has_size_as_constexpr_copyable_data_member_, v1::error::Pos arg__pos_,         \
                                       typename arg__actual_parameter_)> class),                                                \
                   type(_6, template<H(bool arg__has_type_as_member_typedef_, v1::error::Pos arg__pos_,                         \
                                       typename arg__actual_parameter_)> class),                                                \
                   type(_7, template<H(bool arg__has_elems_as_constexpr_copyable_data_member_, v1::error::Pos arg__pos_,        \
                                       typename arg__actual_parameter_)> class),                                                \
                   type(_8, template<H(bool arg__has_elems_ptr_as_constexpr_copyable_data_member_, v1::error::Pos arg__pos_,    \
                                       typename arg__actual_parameter_)> class),                                                \
                   type(_9, template<bool arg__is_valid_, v1::error::Pos arg__pos_, typename arg__element_type_> class),        \
                   type(_10, template<bool arg__is_valid_, v1::error::Pos arg__pos_, typename arg__size_type_> class),          \
                   type(_11, template<bool arg__is_valid_, v1::error::Pos arg__pos_, typename arg__type_> class),               \
                   type(_12, template<bool arg__is_valid_, v1::error::Pos arg__pos_, typename arg__elems_type_> class),         \
                   type(_13, template<bool arg__is_valid_, v1::error::Pos arg__pos_, typename arg__elems_ptr_type_> class),     \
                   type(_14, template<H(bool arg__points_to_elems_, v1::error::Pos arg__pos_,                                   \
                                        typename arg__elems_ptr_type_, arg__elems_ptr_type_ arg__elems_ptr_)> class))
#define args_1(I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14)                                                          \
            base_1((), (), (), (), (), (), (), (), (), (), (), (), (), I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14)
#define prms_1(...) decl_1((), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

#define base_2A(_d1, _d2, args_1, _1, _2)                       \
            args_1                                              \
            opts  (_1, arg__nonempty_tuple_checker_msg, _d1)    \
              opts(_2, arg__tuple_checker_msg, _d2)
#define decl_2A(_1_d2, _1_d3, _1_d4, _1_d5, _1_d6, _1_d7, _1_d8, _1_d9, _1_d10, _1_d11, _1_d12, _1_d13, _1_d14, _d1, _d2,       \
                I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14, _1, _2)             \
            base_2A(_d1, _d2,                                                                                                   \
                    decl_1(_1_d2, _1_d3, _1_d4, _1_d5, _1_d6, _1_d7, _1_d8, _1_d9, _1_d10, _1_d11, _1_d12, _1_d13, _1_d14,      \
                           I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14),         \
                    type(_1, template<bool arg__is_nonempty_> class),                                                           \
                    type(_2, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class))
#define args_2A(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14, _1, _2)                     \
            base_2A((), (), args_1(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14), _1, _2)
#define prms_2A(...) decl_2A((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

            template<decl_2A(_1_d2 (error::array::make::nested::value::nested_array_element_type_is_uniform),
                             _1_d3 (error::array::make::nested::value::nested_array_size_is_positive),
                             _1_d4 (error::array::make::nested::value::nested_array_has_element_type_as_member_typedef),
                             _1_d5 (error::array::make::nested::value::nested_array_has_size_as_constexpr_copyable_data_member),
                             _1_d6 (error::array::make::nested::value::nested_array_has_type_as_member_typedef),
                             _1_d7 (error::array::make::nested::value::nested_array_has_elems_as_constexpr_copyable_data_member),
                             _1_d8 (error::array::make::nested::value::nested_array_has_elems_ptr_as_constexpr_copyable_data_member),
                             _1_d9 (error::array::make::nested::value::nested_array_element_type_is_valid),
                             _1_d10(error::array::make::nested::value::nested_array_size_type_is_valid),
                             _1_d11(error::array::make::nested::value::nested_array_type_is_valid),
                             _1_d12(error::array::make::nested::value::nested_array_elems_type_is_valid),
                             _1_d13(error::array::make::nested::value::nested_array_elems_ptr_type_is_valid),
                             _1_d14(error::array::make::nested::value::nested_array_elems_ptr_points_to_elems),
                             _d1   (error::array::make::nested::value::arg_2_is_nonempty),
                             _d2   (error::array::make::nested::value::arg_2_is_tuple_construct),
                             I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
            struct value
            /* {
             *   with template<typename I, typename arg__Array, typename... args__Array>
             *        tuple::construct<I, arg__Array, args__Array...> = arg__tuple_construct__of__Array {
             *     invariant {
             *       arg__Array::size > 0;
             *       args__Array...@[&](std::size_t i, auto arg__nested_array) {
             *         arg__nested_array::size > 0;
             *         std::is_same<typename arg__Array::element_type, typename arg__nested_array::element_type>::value == true;
             *       }
             *     }
             *     include construct<I, false, const typename arg__Array::element_type *,
             *                       tuple::construct<I, arg__Array::elems, args__Array::elems...>>;
             *   }
             * }
             */;

#define base_2B(_d1, _d2, args_1, _1, _2)                                               \
            args_1                                                                      \
            opts  (_1, arg__first_array_element_type, _d1)                              \
              opts(_2, arg__tuple_construct__of__Array__valid_array, _d2)
#define decl_2B(_d1, _d2, I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14, _1, _2)   \
            base_2B(_d1, _d2,                                                                                                   \
                    prms_1(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14),         \
                    type(_1, typename),                                                                                         \
                    type(_2, typename))
#define args_2B(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14, _1, _2)                     \
            base_2B((), (), args_1(I, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14), _1, _2)
#define prms_2B(...) decl_2B((), (), __VA_ARGS__)

            template<decl_2B(_d1(I),
                             _d2(tuple::construct<I>),
                             I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
            struct value__next;

#define base_3B(I, _1, args_2B) I                       \
            opts  (_1, arg__Array__nested_array, ())    \
              args_2B
#define decl_3B(I, _1, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14, _2B_1, _2B_2)           \
            base_3B(I##I,                                                                                                               \
                    type(_1, typename),                                                                                                 \
                    prms_2B(, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14, _2B_1, _2B_2))
#define args_3B(I, _1, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14, _2B_1, _2B_2)                   \
            base_3B(I, _1, args_2B(, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14, _2B_1, _2B_2))
#define prms_3B(...) decl_3B(__VA_ARGS__)

            template<prms_3B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
            struct value__nested_array_validated;

#define base_4B(args_3B)                        \
            args_3B
#define decl_4B(I, _3B_1, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14, _2B_1, _2B_2)                \
            base_4B(prms_3B(I, _3B_1, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14, _2B_1, _2B_2))
#define args_4B(I, _3B_1, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14, _2B_1, _2B_2)                \
            base_4B(args_3B(I, _3B_1, _1_1, _1_2, _1_3, _1_4, _1_5, _1_6, _1_7, _1_8, _1_9, _1_10, _1_11, _1_12, _1_13, _1_14, _2B_1, _2B_2))
#define prms_4B(...) decl_4B(__VA_ARGS__)

            template<prms_4B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
            struct value__nested_array_validated_and_uniform;

            

            template<prms_2A(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
            struct value :
              arg__tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct__of__Array> {
            };

            template<prms_2A(I, R(typename... args__element), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
            struct value<args_2A(I, R(Tuple<args__element...>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
              value<args_2A(I, R(tuple::construct<I, args__element...>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
            };

            template<prms_2A(I, X, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
            struct value<args_2A(I, R(tuple::construct<I>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
              arg__nonempty_tuple_checker_msg<!!utility::always_false<I>()> {
            };

            template<prms_2A(I, R(typename arg, typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
            struct value<args_2A(I, R(tuple::construct<I, arg, args...>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
              value__next<args_2B(I, R(tuple::construct<I, arg, args...>), _, _, _, _, _, _, _, _, _, _, _, _, _, X, X)> {
            };

            

            template<prms_2B(I, X, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(typename... args))>
            struct value__next<args_2B(I, R(tuple::construct<I>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(tuple::construct<I, args...>))> :
#define base_class array_common<I, const arg__first_array_element_type *, sizeof...(args)>
              base_class
            {
              static constexpr typename base_class::type elems {args::elems_ptr[0]...};
              static constexpr const typename base_class::element_type *elems_ptr[1] {elems};
            };
#undef base_class

            template<prms_2B(I, R(typename arg, typename... args), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
            struct value__next<args_2B(I, R(tuple::construct<I, arg, args...>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
              array::validate_with_pos<I, tuple::size<I, arg__tuple_construct__of__Array__valid_array>::value + 1,
                                       arg, arg__nested_array_type_validator_msg, arg__nested_array_elems_type_validator_msg,
                                       arg__nested_array_elems_ptr_validator_msg, arg__nested_array_elems_ptr_value_checker_msg,
                                       arg__nested_array_type_checker_msg, arg__nested_array_elems_checker_msg,
                                       arg__nested_array_elems_ptr_checker_msg,
                                       arg__nested_array_element_type_validator_msg, arg__nested_array_size_validator_msg,
                                       arg__nested_array_element_type_checker_msg, arg__nested_array_size_checker_msg>,
              value__nested_array_validated<args_3B(I, R(arg), R(tuple::construct<I, args...>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>{
            };

            

            template<prms_3B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, X, _)>
            struct value__nested_array_validated<args_3B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(I), _)> :
              value__nested_array_validated_and_uniform<args_4B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                R(typename arg__Array__nested_array::element_type), _)> {
            };

            template<prms_3B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
            struct value__nested_array_validated :
              arg__nested_array_uniform_element_type_checker_msg<std::is_same<typename arg__Array__nested_array::element_type,
                                                                              arg__first_array_element_type>::value,
                                                                 tuple::size<I, arg__tuple_construct__of__Array__valid_array>::value + 1,
                                                                 typename arg__Array__nested_array::element_type, arg__first_array_element_type>,
              value__nested_array_validated_and_uniform<args_4B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
            };

            

            template<prms_4B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(typename... args))>
            struct value__nested_array_validated_and_uniform<args_4B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
                                                                     R(tuple::construct<I, args...>))> :
              arg__nested_array_positive_size_checker_msg<(arg__Array__nested_array::size > 0), sizeof...(args) + 1, arg__Array__nested_array::size>,
              value__next<args_2B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, R(tuple::construct<I, args..., arg__Array__nested_array>))> {
            };

#include "v1_internals_end.hpp"
          }
        }
      }
      namespace array {
        namespace update {

#include "v1_internals_begin.hpp"

#define base_1(I, _1, _2, _3, _4, _5) I                 \
          opts  (_1, arg__array_element_type, ())       \
            opts(_2, arg__key_checker_msg, ())          \
            opts(_3, arg__value_checker_msg, ())        \
            opts(_4, arg__key_type_checker_msg, ())     \
            opts(_5, arg__type_checker_msg, ())
#define decl_1(I, _1, _2, _3, _4, _5)                                                                                                   \
          base_1(I##I,                                                                                                                  \
                 type(_1, typename),                                                                                                    \
                 type(_2, template<H(bool arg__has_key_as_constexpr_copyable_data_member_, v1::error::Pos arg__element_pos_,            \
                                     typename arg__actual_parameter_)> class),                                                          \
                 type(_3, template<H(bool arg__has_value_as_constexpr_copyable_data_member_, v1::error::Pos arg__element_pos_,          \
                                     typename arg__actual_parameter_)> class),                                                          \
                 type(_4, template<bool arg__is_valid_, v1::error::Pos arg__element_pos_, typename arg__element_key_type_> class),      \
                 type(_5, template<H(bool arg__is_convertible_to_array_element_type_, v1::error::Pos arg__element_pos_,                 \
                                     typename arg__element_value_type_, typename arg__array_element_type_)> class))
#define args_1(I, _1, _2, _3, _4, _5) base_1(I, _1, _2, _3, _4, _5)
#define prms_1(...) decl_1(__VA_ARGS__)

          template<prms_1(I, _, _, _, _, _)>
          struct update_list_element_to_pair_mapping_data;

#include "v1_internals_end.hpp"
        }
      }
      namespace array {
        namespace update {

#include "v1_internals_begin.hpp"

          //\begin{import update_list_element_to_pair_mapping_data}
#define base_1A(I, _1, _2, _3, _4, _5) I                \
          opts  (_1, arg__array_element_type, ())       \
            opts(_2, arg__key_checker_msg, ())          \
            opts(_3, arg__value_checker_msg, ())        \
            opts(_4, arg__key_type_checker_msg, ())     \
            opts(_5, arg__type_checker_msg, ())
#define decl_1A(I, _1, _2, _3, _4, _5)                                                                                                  \
          base_1A(I##I,                                                                                                                 \
                  type(_1, typename),                                                                                                   \
                  type(_2, template<H(bool arg__has_key_as_constexpr_copyable_data_member_, v1::error::Pos arg__element_pos_,           \
                                      typename arg__actual_parameter_)> class),                                                         \
                  type(_3, template<H(bool arg__has_value_as_constexpr_copyable_data_member_, v1::error::Pos arg__element_pos_,         \
                                      typename arg__actual_parameter_)> class),                                                         \
                  type(_4, template<bool arg__is_valid_, v1::error::Pos arg__element_pos_, typename arg__element_key_type_> class),     \
                  type(_5, template<H(bool arg__is_convertible_to_array_element_type_, v1::error::Pos arg__element_pos_,                \
                                      typename arg__element_value_type_, typename arg__array_element_type_)> class))
#define args_1A(I, _1, _2, _3, _4, _5) base_1A(I, _1, _2, _3, _4, _5)
#define prms_1A(...) decl_1A(__VA_ARGS__)
          //\end{import update_list_element_to_pair_mapping_data}

#define base_1B(I, _1, _2, _3) I                                        \
          opts  (_1, arg__idx, ())                                      \
            opts(_2, arg__dict_Entry__element, ())                      \
            opts(_3, arg__update_list_element_to_pair_mapping_data, ())
#define decl_1B(I, _1, _2, _3)                  \
          base_1B(I##I,                         \
                  type(_1, v1::array::Idx),     \
                  type(_2, typename),           \
                  type(_3, typename))
#define args_1B(I, _1, _2, _3) base_1B(I, _1, _2, _3)
#define prms_1B(...) decl_1B(__VA_ARGS__)

          template<prms_1B(I, _, _, _)>
          struct update_list_element_to_pair_mapping
          /* {
           *   static constexpr bool is_complete {false};
           *   typedef arg__update_list_element_to_pair_mapping_data data;
           *   typedef struct {
           *     typedef pair::construct<I, v1::dict::Key, data$$arg__array_element_type> value_type;
           *     static constexpr value_type value {arg__dict_Entry__element::key, arg__dict_Entry__element::value};
           *   } value;
           * }
           */;

#define base_2B(args_1B)                        \
          args_1B
#define decl_2B(I, _1_1, _1_2, _1_3)            \
          base_2B(prms_1B(I, _1_1, _1_2, _1_3))
#define args_2B(I, _1_1, _1_2, _1_3) base_2B(args_1B(I, _1_1, _1_2, _1_3))
#define prms_2B(...) decl_2B(__VA_ARGS__)

          template<prms_2B(I, _, _, _)>
          struct update_list_element_to_pair_mapping__with_key_and_value;

#define base_3B(args_2B)                        \
          args_2B
#define decl_3B(I, _1_1, _1_2, _1_3)            \
          base_2B(prms_2B(I, _1_1, _1_2, _1_3))
#define args_3B(I, _1_1, _1_2, _1_3) base_3B(args_2B(I, _1_1, _1_2, _1_3))
#define prms_3B(...) decl_3B(__VA_ARGS__)

          template<prms_3B(I, _, _, _)>
          struct update_list_element_to_pair_mapping__with_convertible_value;

          

          template<prms_1B(I, _, _, RR(prms_1A(, _, _, _, _, _)))>
          struct update_list_element_to_pair_mapping<args_1B(I, _, _, R(update_list_element_to_pair_mapping_data<args_1A(I, _, _, _, _, _)>))> :
            arg__key_checker_msg<utility::has_key_as_constexpr_copyable_data_member<I, arg__dict_Entry__element>::value, arg__idx + 1,
                                 arg__dict_Entry__element>,
            arg__value_checker_msg<H(utility::has_value_as_constexpr_copyable_data_member<I, arg__dict_Entry__element>::value, arg__idx + 1,
                                     arg__dict_Entry__element)>,
            update_list_element_to_pair_mapping__with_key_and_value<args_2B(I, _, _,
                                                                            R(update_list_element_to_pair_mapping_data<args_1A(I, _, _, _,
                                                                                                                               _, _)>))> {
          };

          

          template<prms_2B(I, _, _, RR(prms_1A(, _, _, _, _, _)))>
          struct update_list_element_to_pair_mapping__with_key_and_value<args_2B(I, _, _,
                                                                                 R(update_list_element_to_pair_mapping_data<args_1A(I, _, _, _,
                                                                                                                                    _, _)>))> :
            arg__key_type_checker_msg<std::is_same<std::remove_const_t<decltype(arg__dict_Entry__element::key)>, v1::dict::Key>::value,
                                      arg__idx + 1, std::remove_const_t<decltype(arg__dict_Entry__element::key)>>,
            arg__type_checker_msg<H(std::is_convertible<decltype(arg__dict_Entry__element::value), arg__array_element_type>::value, arg__idx + 1,
                                    decltype(arg__dict_Entry__element::value), arg__array_element_type)>,
            update_list_element_to_pair_mapping__with_convertible_value<args_3B(I, _, _,
                                                                                R(update_list_element_to_pair_mapping_data<args_1A(I, _, _, _,
                                                                                                                                   _, _)>))> {
          };

          

          template<prms_3B(I, _, _, RR(prms_1A(, _, _, _, _, _)))>
          struct update_list_element_to_pair_mapping__with_convertible_value<args_3B(I, _, _,
                                                                                     R(update_list_element_to_pair_mapping_data<args_1A(I, _, _, _,
                                                                                                                                        _, _)>))> {
          private:
            struct pair {
              typedef internals::pair::construct<I, v1::dict::Key, arg__array_element_type> value_type;
              static constexpr value_type value {H(arg__dict_Entry__element::key,
                                                   static_cast<arg__array_element_type>(arg__dict_Entry__element::value))};
              // The `static_cast' is used to allow narrowing (e.g., `double' to `int'), which passes the `std::is_convertible' test.
              // The `static_cast', however, does not work around the case where an explicit conversion is required because
              // such case fails the `std::is_convertible' test.
            };
          public:
            static constexpr bool is_complete {false};
            typedef update_list_element_to_pair_mapping_data<args_1A(I, _, _, _, _, _)> data;
            typedef pair value;
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace array {
        namespace update {

          enum class Error { NONE, STRAY_INDEX, DUPD, DUPD_NOT_IDENTICAL };

        }
      }
      namespace array {
        namespace update {

#include "v1_internals_begin.hpp"

#define base_1(I, _1) I                                 \
          opts  (_1, arg__Array__prev_array, ())
#define decl_1(I, _1)                           \
          base_1(I##I,                          \
                 type(_1, typename))
#define args_1(I, _1) base_1(I, _1)
#define prms_1(...) decl_1(__VA_ARGS__)

#define base_2(_d2, _d3, _d4, _d5, _d6, _d7, args_1, _1, _2, _3, _4, _5, _6, _7)                                \
          args_1                                                                                                \
          opts  (_1, arg__tuple_construct__of__dict_Entry__of__prev_array_element_type__update_list, ())        \
            opts(_2, arg__on_dupd_index, _d2)                                                                   \
            opts(_3, arg__on_stray_index, _d3)                                                                  \
          opts  (_4, arg__key_checker_msg, _d4)                                                                 \
          opts  (_5, arg__value_checker_msg, _d5)                                                               \
          opts  (_6, arg__key_type_checker_msg, _d6)                                                            \
          opts  (_7, arg__value_type_checker_msg, _d7)
#define decl_2(_d2, _d3, _d4, _d5, _d6, _d7, I, _1_1, _1, _2, _3, _4, _5, _6, _7)                                                       \
          base_2(_d2, _d3, _d4, _d5, _d6, _d7,                                                                                          \
                 prms_1(I, _1_1),                                                                                                       \
                 type(_1, typename),                                                                                                    \
                 type(_2, typename),                                                                                                    \
                 type(_3, typename),                                                                                                    \
                 type(_4, template<H(bool arg__has_key_as_constexpr_copyable_data_member_, v1::error::Pos arg__element_pos_,            \
                                     typename arg__actual_parameter_)> class),                                                          \
                 type(_5, template<H(bool arg__has_value_as_constexpr_copyable_data_member_, v1::error::Pos arg__element_pos_,          \
                                     typename arg__actual_parameter_)> class),                                                          \
                 type(_6, template<bool arg__is_valid_, v1::error::Pos arg__element_pos_, typename arg__element_key_type_> class),      \
                 type(_7, template<H(bool arg__is_convertible_to_array_element_type_, v1::error::Pos arg__element_pos_,                 \
                                     typename arg__element_value_type_, typename arg__array_element_type_)> class))
#define args_2(I, _1_1, _1, _2, _3, _4, _5, _6, _7) base_2((), (), (), (), (), (), args_1(I, _1_1), _1, _2, _3, _4, _5, _6, _7)
#define prms_2(...) decl_2((), (), (), (), (), (), __VA_ARGS__)

          template<prms_2(I, _, _, _, _, _, _, _, _)>
          struct value__run;

#define base_3(_d1, _d2, _d3, args_2, _1, _2, _3)       \
          args_2                                        \
          opts  (_1, arg__bound_checker_msg, _d1)       \
            opts(_2, arg__no_dup_msg, _d2)              \
            opts(_3, arg__identical_dup_msg, _d3)
#define decl_3(_2_d2, _2_d3, _2_d4, _2_d5, _2_d6, _2_d7, _d1, _d2, _d3, I, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _1, _2, _3)          \
          base_3(_d1, _d2, _d3,                                                                                                                 \
                 decl_2(_2_d2, _2_d3, _2_d4, _2_d5, _2_d6, _2_d7, I, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7),                           \
                 type(_1, template<bool arg__is_within_array_bound_, v1::error::Pos arg__element_pos_, v1::dict::Key arg__key_> class),         \
                 type(_2, template<H(bool arg__has_no_duplicate_, v1::error::Pos arg__element_1_pos_, v1::error::Pos arg__element_2_pos_,       \
                                     v1::dict::Key arg__key_)> class),                                                                          \
                 type(_3, template<H(bool arg__are_identical_, v1::error::Pos arg__element_1_pos_, v1::error::Pos arg__element_2_pos_,          \
                                     v1::dict::Key arg__key_)> class))
#define args_3(I, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _1, _2, _3)                           \
          base_3((), (), (), args_2(I, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7), _1, _2, _3)
#define prms_3(...) decl_3((), (), (), (), (), (), (), (), (), __VA_ARGS__)

          template<prms_3(I, _, _, _, _, _, _, _, _, _, _, _)>
          struct value__result;

#define base_4(args_3)                          \
          args_3
#define decl_4(_2_d2, _2_d3, _2_d4, _2_d5, _2_d6, _2_d7, _3_d1, _3_d2, _3_d3, I, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _3_1, _3_2, _3_3) \
          base_4(decl_3(_2_d2, _2_d3, _2_d4, _2_d5, _2_d6, _2_d7, _3_d1, _3_d2, _3_d3,                                                             \
                        I, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _3_1, _3_2, _3_3))
#define args_4(I, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _3_1, _3_2, _3_3)             \
          base_4(args_3(I, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _3_1, _3_2, _3_3))
#define prms_4(...) decl_4((), (), (), (), (), (), (), (), (), __VA_ARGS__)

          template<prms_4(I, _, _, _, _, _, _, _, _, _, _, _)>
          struct value__args_checked;

#define base_5A(I, _1, args_4) I                        \
          opts  (_1, arg__prev_array_is_empty, ())      \
            args_4
#define decl_5A(_2_d2, _2_d3, _2_d4, _2_d5, _2_d6, _2_d7, _3_d1, _3_d2, _3_d3,                  \
                I, _1, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _3_1, _3_2, _3_3)        \
          base_5A(I##I,                                                                         \
                  type(_1, bool),                                                               \
                  decl_4(_2_d2, _2_d3, _2_d4, _2_d5, _2_d6, _2_d7, _3_d1, _3_d2, _3_d3,         \
                         , _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _3_1, _3_2, _3_3))
#define args_5A(I, _1, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _3_1, _3_2, _3_3)                \
          base_5A(I, _1, args_4(, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _3_1, _3_2, _3_3))
#define prms_5A(...) decl_5A((), (), (), (), (), (), (), (), (), __VA_ARGS__)

          template<prms_5A(I, _, _, _, _, _, _, _, _, _, _, _, _)>
          struct value__empty_array_or_tuple_check;

#define base_5B(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14,      \
                args_4, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14)            \
          args_4                                                                                \
          opts  (_1,  arg__2_element_type_checker_msg, _d1)                                     \
            opts(_2,  arg__2_size_checker_msg, _d2)                                             \
            opts(_3,  arg__2_type_checker_msg, _d3)                                             \
            opts(_4,  arg__2_elems_checker_msg, _d4)                                            \
            opts(_5,  arg__2_elems_ptr_checker_msg, _d5)                                        \
          opts  (_6,  arg__2_element_type_validator_msg, _d6)                                   \
          opts  (_7,  arg__2_size_type_validator_msg, _d7)                                      \
          opts  (_8,  arg__2_type_validator_msg, _d8)                                           \
          opts  (_9,  arg__2_elems_type_validator_msg, _d9)                                     \
          opts  (_10, arg__2_elems_ptr_type_validator_msg, _d10)                                \
          opts  (_11, arg__2_elems_ptr_value_checker_msg, _d11)                                 \
          opts  (_12, arg__3_tuple_checker_msg, _d12)                                           \
          opts  (_13, arg__4_type_checker_msg, _d13)                                            \
          opts  (_14, arg__5_type_checker_msg, _d14)
#define decl_5B(_2_d2, _2_d3, _2_d4, _2_d5, _2_d6, _2_d7, _3_d1, _3_d2, _3_d3,                                                                     \
                _d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14,                                                         \
                I, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _3_1, _3_2, _3_3, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14)  \
          base_5B(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14,                                                       \
                  decl_4(_2_d2, _2_d3, _2_d4, _2_d5, _2_d6, _2_d7, _3_d1, _3_d2, _3_d3,                                                            \
                         I, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _3_1, _3_2, _3_3),                                                     \
                  type(_1,  template<bool arg__has_element_type_as_member_typedef_, typename arg__actual_parameter_> class),                       \
                  type(_2,  template<bool arg__has_size_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),               \
                  type(_3,  template<bool arg__has_type_as_member_typedef_, typename arg__actual_parameter_> class),                               \
                  type(_4,  template<bool arg__has_elems_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),              \
                  type(_5,  template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),          \
                  type(_6,  template<bool arg__is_valid_, typename arg__element_type_> class),                                                     \
                  type(_7,  template<bool arg__is_valid_, typename arg__size_type_> class),                                                        \
                  type(_8,  template<bool arg__is_valid_, typename arg__type_> class),                                                             \
                  type(_9,  template<bool arg__is_valid_, typename arg__elems_type_> class),                                                       \
                  type(_10, template<bool arg__is_valid_, typename arg__elems_ptr_type_> class),                                                   \
                  type(_11, template<bool arg__is_valid_, typename arg__elems_ptr_type_, arg__elems_ptr_type_ arg__elems_ptr_> class),             \
                  type(_12, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class),                                       \
                  type(_13, template<bool arg__is_member_of_on_dupd_index_, typename arg__actual_parameter_> class),                               \
                  type(_14, template<bool arg__is_member_of_on_stray_index_, typename arg__actual_parameter_> class))
#define args_5B(I, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _3_1, _3_2, _3_3, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14)  \
          base_5B((), (), (), (), (), (), (), (), (), (), (), (), (), (),                                                                          \
                  args_4(I, _1_1, _2_1, _2_2, _2_3, _2_4, _2_5, _2_6, _2_7, _3_1, _3_2, _3_3),                                                     \
                  _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14)
#define prms_5B(...) decl_5B((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

          template<decl_5B(_2_d2(v1::array::update::on_dupd_index::BREAK),
                           _2_d3(v1::array::update::on_stray_index::BREAK),
                           _2_d4(error::array::update::value::update_list_element_has_key_as_constexpr_copyable_data_member),
                           _2_d5(error::array::update::value::update_list_element_has_value_as_constexpr_copyable_data_member),
                           _2_d6(error::array::update::value::update_list_element_key_type_is_valid),
                           _2_d7(error::array::update::value::update_list_element_value_is_convertible_to_array_element_type),
                           _3_d1(error::array::update::value::update_list_key_is_within_array_bound),
                           _3_d2(error::array::update::value::update_list_key_has_no_duplicate),
                           _3_d3(error::array::update::value::update_list_key_duplicates_are_identical),
                           _d1  (error::array::update::value::arg_2_has_element_type_as_member_typedef),
                           _d2  (error::array::update::value::arg_2_has_size_as_constexpr_copyable_data_member),
                           _d3  (error::array::update::value::arg_2_has_type_as_member_typedef),
                           _d4  (error::array::update::value::arg_2_has_elems_as_constexpr_copyable_data_member),
                           _d5  (error::array::update::value::arg_2_has_elems_ptr_as_constexpr_copyable_data_member),
                           _d6  (error::array::update::value::arg_2_element_type_is_valid),
                           _d7  (error::array::update::value::arg_2_size_type_is_valid),
                           _d8  (error::array::update::value::arg_2_type_is_valid),
                           _d9  (error::array::update::value::arg_2_elems_type_is_valid),
                           _d10 (error::array::update::value::arg_2_elems_ptr_type_is_valid),
                           _d11 (error::array::update::value::arg_2_elems_ptr_points_to_elems),
                           _d12 (error::array::update::value::arg_3_is_tuple_construct),
                           _d13 (error::array::update::value::arg_4_is_member_of_on_dupd_index),
                           _d14 (error::array::update::value::arg_5_is_member_of_on_stray_index),
                           I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
          struct value
          /* {
           *   if (arg__Array__prev_array::size == 0) {
           *     include construct<I, true, typename arg__Array__prev_array::element_type>;
           *   } else {
           *     if (tuple::size<I, arg__tuple_construct__of__dict_Entry__of__prev_array_element_type__update_list>::value == 0) {
           *       include arg__Array__prev_array;
           *     } else {
           *       include array_common<I, typename arg__Array__prev_array::element_type, arg__Array__prev_array::size>;
           *       with template<self::element_type... args__array_element>
           *            {args__array_element...} = arg__Array__prev_array::elems
           *       {
           *         static constexpr typename self::type elems = {args__array_element...@[&](std::size_t i, auto arg__array_element) {
           *           if (arg__on_dupd_index == v1::array::update::on_dupd_index::CONTINUE_KEEP_LAST) {
           *             for (auto j = tuple::size<I, arg__tuple_construct__of__dict_Entry__of__prev_array_element_type__update_list>::value;
           *                  j > 0; --j)
           *             {
           *               if (tuple::get_pos<I, j,
           *                                  arg__tuple_construct__of__dict_Entry__of__prev_array_element_type__update_list>::value::key == i) {
           *                 return {true,
           *                         tuple::get_pos<I, j,
           *                                        arg__tuple_construct__of__dict_Entry__of__prev_array_element_type__update_list>::value::value};
           *               }
           *             }
           *           } else {
           *             for (auto j = 0;
           *                  j < tuple::size<I, arg__tuple_construct__of__dict_Entry__of__prev_array_element_type__update_list>::value; ++j)
           *             {
           *               if (tuple::get_pos<I, j - 1,
           *                                  arg__tuple_construct__of__dict_Entry__of__prev_array_element_type__update_list>::value::key == i) {
           *                 return {true,
           *                         tuple::get_pos<I, j - 1,
           *                                        arg__tuple_construct__of__dict_Entry__of__prev_array_element_type__update_list>::value};
           *               }
           *             }
           *           }
           *           return {true, arg__array_element};
           *         }};
           *         static constexpr const typename self::element_type *elems_ptr[1] = {elems};
           *       }
           *     }
           *   }
           * }
           */;

          

          template<prms_5B(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
          struct value :
            arg__3_tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct__of__dict_Entry__of__prev_array_element_type__update_list> {
          };

          template<prms_5B(I, _, R(typename... args__element), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
          struct value<args_5B(I, _, R(Tuple<args__element...>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
            value<args_5B(I, _, R(tuple::construct<I, args__element...>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> {
          };

          template<prms_5B(I, _, R(typename... args__element), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
          struct value<args_5B(I,
                               _, R(tuple::construct<I, args__element...>), _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)> :
            array::validate<I, arg__Array__prev_array, arg__2_type_validator_msg, arg__2_elems_type_validator_msg,
                            arg__2_elems_ptr_type_validator_msg, arg__2_elems_ptr_value_checker_msg,
                            arg__2_type_checker_msg, arg__2_elems_checker_msg, arg__2_elems_ptr_checker_msg,
                            arg__2_element_type_validator_msg, arg__2_size_type_validator_msg,
                            arg__2_element_type_checker_msg, arg__2_size_checker_msg>,
            arg__4_type_checker_msg<   std::is_same<arg__on_dupd_index, v1::array::update::on_dupd_index::BREAK                >::value
                                    || std::is_same<arg__on_dupd_index, v1::array::update::on_dupd_index::CONTINUE_IF_IDENTICAL>::value
                                    || std::is_same<arg__on_dupd_index, v1::array::update::on_dupd_index::CONTINUE_KEEP_FIRST  >::value
                                    || std::is_same<arg__on_dupd_index, v1::array::update::on_dupd_index::CONTINUE_KEEP_LAST   >::value,
                                    arg__on_dupd_index>,
            arg__5_type_checker_msg<   std::is_same<arg__on_stray_index, v1::array::update::on_stray_index::BREAK   >::value
                                    || std::is_same<arg__on_stray_index, v1::array::update::on_stray_index::CONTINUE>::value, arg__on_stray_index>,
            value__args_checked<args_4(I, _, R(tuple::construct<I, args__element...>), _, _, _, _, _, _, _, _, _)> {
          };

          

          template<prms_4(I, _, _, _, _, _, _, _, _, _, _, _)>
          struct value__args_checked :
            value__empty_array_or_tuple_check<args_5A(I, R(arg__Array__prev_array::size == 0), _, _, _, _, _, _, _, _, _, _, _)> {
          };

          

#define prms_5A_1(I, _1, _2_1)                                  \
          prms_5A(I, _1, _, _2_1, _, _, _, _, _, _, _, _, _)
#define args_5A_1(I, _1, _2_1)                                  \
          args_5A(I, _1, _, _2_1, _, _, _, _, _, _, _, _, _)

          template<prms_5A_1(I, X, _)>
          struct value__empty_array_or_tuple_check<args_5A_1(I, R(true), _)> :
            construct<I, true, typename arg__Array__prev_array::element_type> {
          };

#define prms_5A_2(I, _2_1)                      \
          prms_5A_1(I, X, _2_1)
#define args_5A_2(I, _2_1)                      \
          args_5A_1(I, R(false), _2_1)

#define base_definition(arg__elems)                                                             \
          static constexpr const typename base_class::type &elems {arg__elems};                 \
          static constexpr const typename base_class::element_type *elems_ptr[1] {elems}

          template<prms_5A_2(I, X)>
          struct value__empty_array_or_tuple_check<args_5A_2(I, R(tuple::construct<I>))> :
#define base_class array_common<I, typename arg__Array__prev_array::element_type, arg__Array__prev_array::size>
            base_class
          {
            base_definition(arg__Array__prev_array::elems);
          };
#undef base_class

          template<prms_5A_2(I, _)>
          struct value__empty_array_or_tuple_check<args_5A_2(I, _)> :
            value__result<args_3(I, _, _, _, _, _, _, _, _, _, _, _)> {
          };

          

          template<prms_3(I, _, _, _, _, _, _, _, _, _, _, _)>
          struct value__result :
#define base_class array_common<I, typename arg__Array__prev_array::element_type, arg__Array__prev_array::size>
            base_class
          {
          private:
            static constexpr value__run<args_2(I, _, _, _, _, _, _, _, _)> result {};
            run_metaprogram(arg__bound_checker_msg<H(result.error != internals::array::update::Error::STRAY_INDEX, result.error_pos,
                                                     result.error_key)>);
            run_metaprogram(arg__no_dup_msg<H(result.error != internals::array::update::Error::DUPD,
                                              result.error_first_pos, result.error_pos, result.error_key)>);
            run_metaprogram(arg__identical_dup_msg<H(result.error != internals::array::update::Error::DUPD_NOT_IDENTICAL,
                                                     result.error_first_pos, result.error_pos, result.error_key)>);
          public:
            base_definition(result.elems);
          };
#undef base_class

#undef base_definition

          

          template<prms_2(I, _, _, _, _, _, _, _, _)>
          class value__run :
#define base_class array_common<I, typename arg__Array__prev_array::element_type, arg__Array__prev_array::size>
            public base_class
          {
            typedef update_list_element_to_pair_mapping_data<I, typename arg__Array__prev_array::element_type,
                                                             arg__key_checker_msg, arg__value_checker_msg,
                                                             arg__key_type_checker_msg, arg__value_type_checker_msg> mapping_data;

            typedef make::from_tuple::value<I,
                                            typename utility::list::update<I,
                                                                           arg__tuple_construct__of__dict_Entry__of__prev_array_element_type__update_list,
                                                                           update_list_element_to_pair_mapping,
                                                                           mapping_data>::value> updates;
          public:
            typename base_class::type elems;
            Error error;
            v1::error::Pos error_pos;
            v1::error::Pos error_first_pos;
            v1::dict::Key error_key;

            constexpr value__run() :
              elems{}, error(Error::NONE), error_pos(v1::error::Invalid_pos::value), error_first_pos(v1::error::Invalid_pos::value),
              error_key(v1::dict::Invalid_key::value)
            {
              typename base_class::type elems = {};
              bool dupd[arg__Array__prev_array::size] = {};
              v1::error::Pos dupd_first_pos[arg__Array__prev_array::size] = {};

              for (v1::array::Idx i = 0; i < arg__Array__prev_array::size; ++i) {
                this->elems[i] = elems[i] = arg__Array__prev_array::elems[i];
                dupd[i] = false;
                dupd_first_pos[i] = v1::error::Invalid_pos::value;
              }

              for (v1::array::Idx i = 0; i < updates::size; ++i) {
                const auto &idx {updates::elems[i].first};
                const auto &new_val {updates::elems[i].second};

                if (idx >= arg__Array__prev_array::size || idx < 0) {
                  if (std::is_same<arg__on_stray_index, v1::array::update::on_stray_index::BREAK>::value) {
                    this->error = Error::STRAY_INDEX;
                    this->error_pos = i + 1;
                    this->error_key = idx;
                    return;
                  } else {
                    continue;
                  }
                }

                if (dupd[idx]) {
                  if (std::is_same<arg__on_dupd_index, v1::array::update::on_dupd_index::BREAK>::value) {
                    this->error = Error::DUPD;
                    this->error_first_pos = dupd_first_pos[idx];
                    this->error_pos = i + 1;
                    this->error_key = idx;
                    return;
                  } else if (std::is_same<arg__on_dupd_index, v1::array::update::on_dupd_index::CONTINUE_IF_IDENTICAL>::value
                             && elems[idx] != new_val) {
                    this->error = Error::DUPD_NOT_IDENTICAL;
                    this->error_first_pos = dupd_first_pos[idx];
                    this->error_pos = i + 1;
                    this->error_key = idx;
                    return;
                  } else if (std::is_same<arg__on_dupd_index, v1::array::update::on_dupd_index::CONTINUE_KEEP_FIRST>::value) {
                    continue;
                  }
                } else {
                  dupd[idx] = true;
                  dupd_first_pos[idx] = i + 1;
                }

                this->elems[idx] = elems[idx] = new_val;
              }
            }
          };
#undef base_class

#include "v1_internals_end.hpp"
        }
      }
      namespace array {
        namespace filter_then_map {

#include "v1_internals_begin.hpp"

#define base_1(I, _1, _2, _3) I                         \
          opts  (_1, arg__Array__prev_array, ())        \
            opts(_2, arg__array_element_type, ())       \
            opts(_3, arg__filter_and_mapper, ())
#define decl_1(I, _1, _2, _3)                                                                                                           \
          base_1(I##I,                                                                                                                  \
                 type(_1, typename),                                                                                                    \
                 type(_2, typename),                                                                                                    \
                 typr(_3, H(pair::construct<Ii, bool, arg__array_element_type>                                                          \
                            (*arg__filter_and_mapper)(const v1::array::Idx &idx,                                                        \
                                                      const typename arg__Array__prev_array::element_type &prev_array_element))))
#define args_1(I, _1, _2, _3) base_1(I, _1, _2, _3)
#define prms_1(...) decl_1(__VA_ARGS__)

          template<prms_1(I, _, _,
                          R(Pair<bool, arg__array_element_type>
                            (*arg__filter_and_mapper)(const v1::array::Idx &idx,
                                                      const typename arg__Array__prev_array::element_type &prev_array_element)))>
          constexpr internals::pair::construct<I, bool, arg__array_element_type>
          filter_and_mapper_adaptor(const v1::array::Idx &idx, const typename arg__Array__prev_array::element_type &prev_array_element)
          {
            constexpr int arg_4_is_not_nullptr = utility::nontype_nontemplate_args_eq<I, decltype(arg__filter_and_mapper),
                                                                                      decltype(arg__filter_and_mapper),
                                                                                      arg__filter_and_mapper, nullptr>::value ? -1 : 1;
            const char *compile_time_error[arg_4_is_not_nullptr] = {"[DEBUG] Arg 4 is a nullptr"};

            Pair<bool, arg__array_element_type> result(arg__filter_and_mapper(idx, prev_array_element));
            return {result.first, result.second};
          }

#include "v1_internals_end.hpp"
        }
      }
      namespace array {
        namespace filter_then_map {

#include "v1_internals_begin.hpp"

#define base_1A(_d2, _d3, I, _1, _2, _3) I      \
          opts  (_1, arg__result, ())           \
            opts(_2, arg__size, _d2)            \
            opts(_3, args__element_index, _d3)
#define decl_1A(_d2, I, _1, _2, _3)             \
          base_1A(_d2, (), I##I,                \
                  type(_1, typename),           \
                  type(_2, v1::array::Size),    \
                  type(_3, v1::array::Idx...))
#define args_1A(I, _1, _2, _3) base_1A((), (...), I, _1, _2, _3)
#define prms_1A(...) decl_1A((), __VA_ARGS__)

          template<decl_1A(_d2(arg__result::size),
                           I, _, _, _)>
          struct value__base_class;

          //\begin{import filter_and_mapper_adaptor}
#define base_1B(I, _1, _2, _3) I                        \
          opts  (_1, arg__Array__prev_array, ())        \
            opts(_2, arg__array_element_type, ())       \
            opts(_3, arg__filter_and_mapper, ())
#define decl_1B(I, _1, _2, _3)                                                                                                          \
          base_1B(I##I,                                                                                                                 \
                  type(_1, typename),                                                                                                   \
                  type(_2, typename),                                                                                                   \
                  typr(_3, H(pair::construct<Ii, bool, arg__array_element_type>                                                         \
                             (*arg__filter_and_mapper)(const v1::array::Idx &idx,                                                       \
                                                       const typename arg__Array__prev_array::element_type &prev_array_element))))
#define args_1B(I, _1, _2, _3) base_1B(I, _1, _2, _3)
#define prms_1B(...) decl_1B(__VA_ARGS__)
          //\end{import filter_and_mapper_adaptor}

          template<prms_1B(I, _, _, _)>
          struct value__args_checked;

#define base_2BA(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13,   \
                 args_1B, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13)       \
          args_1B                                                                       \
          opts  (_1 ,  arg__element_type_checker_msg, _d1)                              \
            opts(_2 ,  arg__size_checker_msg, _d2)                                      \
            opts(_3 ,  arg__type_checker_msg, _d3)                                      \
          opts  (_4 ,  arg__elems_checker_msg, _d4)                                     \
          opts  (_5 ,  arg__elems_ptr_checker_msg, _d5)                                 \
          opts  (_6 ,  arg__element_type_validator_msg, _d6)                            \
          opts  (_7 ,  arg__size_type_validator_msg, _d7)                               \
          opts  (_8 ,  arg__type_validator_msg, _d8)                                    \
          opts  (_9 ,  arg__elems_type_validator_msg, _d9)                              \
          opts  (_10, arg__elems_ptr_type_validator_msg, _d10)                          \
          opts  (_11, arg__elems_ptr_value_checker_msg, _d11)                           \
          opts  (_12, arg__array_element_type_validator_msg, _d12)                      \
          opts  (_13, arg__nullptr_checker_msg, _d13)
#define decl_2BA(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13,                                                           \
                 I, _1B_1, _1B_2, _1B_3, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13)                                                \
          base_2BA(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13,                                                         \
                   prms_1B(I, _1B_1, _1B_2, _1B_3),                                                                                             \
                   type(_1,  template<bool arg__has_element_type_as_member_typedef_, typename arg__actual_parameter_> class),                   \
                   type(_2,  template<bool arg__has_size_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),           \
                   type(_3,  template<bool arg__has_type_as_member_typedef_, typename arg__actual_parameter_> class),                           \
                   type(_4,  template<bool arg__has_elems_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),          \
                   type(_5,  template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),      \
                   type(_6,  template<bool arg__is_valid_, typename arg__element_type_> class),                                                 \
                   type(_7,  template<bool arg__is_valid_, typename arg__size_type_> class),                                                    \
                   type(_8,  template<bool arg__is_valid_, typename arg__type_> class),                                                         \
                   type(_9,  template<bool arg__is_valid_, typename arg__elems_type_> class),                                                   \
                   type(_10, template<bool arg__is_valid_, typename arg__elems_ptr_type_> class),                                               \
                   type(_11, template<bool arg__is_valid_, typename arg__elems_ptr_type_, arg__elems_ptr_type_ arg__elems_ptr_> class),         \
                   type(_12, template<bool arg__is_valid_array_element_type_, typename arg__actual_parameter_> class),                          \
                   type(_13, template<bool arg__is_not_nullptr_> class))
#define args_2BA(I, _1B_1, _1B_2, _1B_3, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13)                \
          base_2BA((), (), (), (), (), (), (), (), (), (), (), (), (),                                          \
                   args_1B(I, _1B_1, _1B_2, _1B_3), _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13)
#define prms_2BA(...) decl_2BA((), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

          template<decl_2BA(_d1 (error::array::filter_then_map::value::arg_2_has_element_type_as_member_typedef),
                            _d2 (error::array::filter_then_map::value::arg_2_has_size_as_constexpr_copyable_data_member),
                            _d3 (error::array::filter_then_map::value::arg_2_has_type_as_member_typedef),
                            _d4 (error::array::filter_then_map::value::arg_2_has_elems_as_constexpr_copyable_data_member),
                            _d5 (error::array::filter_then_map::value::arg_2_has_elems_ptr_as_constexpr_copyable_data_member),
                            _d6 (error::array::filter_then_map::value::arg_2_element_type_is_valid),
                            _d7 (error::array::filter_then_map::value::arg_2_size_type_is_valid),
                            _d8 (error::array::filter_then_map::value::arg_2_type_is_valid),
                            _d9 (error::array::filter_then_map::value::arg_2_elems_type_is_valid),
                            _d10(error::array::filter_then_map::value::arg_2_elems_ptr_type_is_valid),
                            _d11(error::array::filter_then_map::value::arg_2_elems_ptr_points_to_elems),
                            _d12(error::array::filter_then_map::value::arg_3_is_valid_array_element_type),
                            _d13(error::array::filter_then_map::value::arg_4_is_not_nullptr),
                            I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
          struct value
          /* {
           *   if (arg__Array__prev_array::size == 0) {
           *     include construct<I, true, arg__array_element_type>;
           *   } else {
           *     with template<arg__Array__prev_array::element_type... args__array_element>
           *          {args__array_element...} = arg__Array__prev_array::elems
           *     {
           *       if (sizeof...@[&](std::size_t i, auto arg__array_element) { return arg__filter_and_mapper(i, arg__array_element); }
           *                    (args__array_element) == 0)
           *       {
           *         include construct<I, true, arg__array_element_type>;
           *       }
           *       else
           *       {
           *         static constexpr arg__array_element_type elems[] = {args__array_element...@[&](std::size_t i, auto arg__array_element) {
           *           return arg__filter_and_mapper(i, arg__array_element);
           *         }};
           *         static constexpr const arg__array_element_type *elems_ptr[1] = {elems};
           *         include array_common<I, arg__array_element_type, sizeof(elems)/sizeof(elems[0])>;
           *       }
           *     }
           *   }
           * }
           */;

#define base_2BB(args_1B)                       \
          args_1B
#define decl_2BB(I, _1B_1, _1B_2, _1B_3)                \
          base_2BB(prms_1B(I, _1B_1, _1B_2, _1B_3))
#define args_2BB(I, _1B_1, _1B_2, _1B_3) base_2BB(args_1B(I, _1B_1, _1B_2, _1B_3))
#define prms_2BB(...) decl_2BB(__VA_ARGS__)

          template<prms_2BB(I, _, _, _)>
          struct value__result;

#define base_3BB(args_2BB)                      \
          args_2BB
#define decl_3BB(I, _1B_1, _1B_2, _1B_3)                \
          base_3BB(prms_2BB(I, _1B_1, _1B_2, _1B_3))
#define args_3BB(I, _1B_1, _1B_2, _1B_3) base_3BB(args_2BB(I, _1B_1, _1B_2, _1B_3))
#define prms_3BB(...) decl_3BB(__VA_ARGS__)

          template<prms_3BB(I, _, _, _)>
          struct value__run;

#define base_2BC(I, _1, args_1B) I              \
          opts  (_1, arg__prev_array_size, ())  \
            args_1B
#define decl_2BC(I, _1, _1B_1, _1B_2, _1B_3)            \
          base_2BC(I##I,                                \
                   type(_1, v1::array::Size),           \
                   prms_1B(, _1B_1, _1B_2, _1B_3))
#define args_2BC(I, _1, _1B_1, _1B_2, _1B_3) base_2BC(I, _1, args_1B(, _1B_1, _1B_2, _1B_3))
#define prms_2BC(...) decl_2BC(__VA_ARGS__)

          template<prms_2BC(I, _, _, _, _)>
          struct value__empty_array_check;

          

          template<prms_2BA(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
          struct value :
            array::validate<I, arg__Array__prev_array, arg__type_validator_msg, arg__elems_type_validator_msg,
                            arg__elems_ptr_type_validator_msg, arg__elems_ptr_value_checker_msg,
                            arg__type_checker_msg, arg__elems_checker_msg, arg__elems_ptr_checker_msg,
                            arg__element_type_validator_msg, arg__size_type_validator_msg,
                            arg__element_type_checker_msg, arg__size_checker_msg>,
            arg__array_element_type_validator_msg<element_type_is_valid<I, arg__array_element_type>::value, arg__array_element_type>,
            arg__nullptr_checker_msg<arg__filter_and_mapper != nullptr>,
            value__args_checked<args_1B(I, _, _, _)> {
          };

          

          template<prms_1B(I, _, _, _)>
          struct value__args_checked :
            value__empty_array_check<args_2BC(I, R(arg__Array__prev_array::size), _, _, _)> {
          };

          

          template<prms_2BC(I, X, _, _, _)>
          struct value__empty_array_check<args_2BC(I, R(0), _, _, _)> :
            construct<I, true, arg__array_element_type> {
          };

          template<prms_2BC(I, _, _, _, _)>
          struct value__empty_array_check :
            value__base_class<args_1A(I, R(value__result<args_2BB(I, _, _, _)>), X, X)> {
          };

          

          template<prms_1A(I, _, _, _)>
          struct value__base_class :
            value__base_class<args_1A(I, _, R(arg__size - 1), R(arg__size - 1, args__element_index...))> {
          };

          template<prms_1A(I, _, X, R(v1::array::Idx arg__element_index, v1::array::Idx... args__element_index))>
          struct value__base_class<args_1A(I, _, R(0), R(arg__element_index, args__element_index...))> :
#define base_class array_common<I, typename arg__result::element_type, arg__result::size>
            base_class
          {
            static constexpr typename base_class::type elems {arg__result::elems[arg__element_index], arg__result::elems[args__element_index]...};
            static constexpr const typename base_class::element_type *elems_ptr[1] {elems};
          };
#undef base_class

          template<prms_1A(I, _, X, X)>
          struct value__base_class<args_1A(I, _, R(0), X)> :
            construct<I, true, typename arg__result::element_type> {
          };

          

          template<prms_2BB(I, _, _, _)>
          struct value__result {
          private:
            static constexpr value__run<args_3BB(I, _, _, _)> result {};
          public:
            typedef arg__array_element_type element_type;
            static constexpr v1::array::Size size {result.size};
            static constexpr const element_type *elems {result.elems};
          };

          

          template<prms_3BB(I, _, _, _)>
          struct value__run {
            arg__array_element_type elems[arg__Array__prev_array::size];
            v1::array::Size size;

            constexpr value__run() : elems{}, size{0} {
              v1::array::Idx j = 0;
              for (v1::array::Idx i = 0; i < arg__Array__prev_array::size; ++i) {
                pair::construct<I, bool, arg__array_element_type> result {arg__filter_and_mapper(i, arg__Array__prev_array::elems[i])};
                if (result.first) {
                  elems[j++] = result.second;
                }
              }
              size = j;
            }
          };

#include "v1_internals_end.hpp"
        }
      }
      namespace array {

#include "v1_internals_begin.hpp"

#define base_1(_d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12) I        \
        opts  (_1, arg__Array__array, ())                                                                                               \
          opts(_2,  arg__element_type_checker_msg, _d2)                                                                                 \
          opts(_3,  arg__size_checker_msg, _d3)                                                                                         \
          opts(_4,  arg__type_checker_msg, _d4)                                                                                         \
        opts  (_5,  arg__elems_checker_msg, _d5)                                                                                        \
        opts  (_6,  arg__elems_ptr_checker_msg, _d6)                                                                                    \
        opts  (_7,  arg__element_type_validator_msg, _d7)                                                                               \
        opts  (_8,  arg__size_type_validator_msg, _d8)                                                                                  \
        opts  (_9,  arg__type_validator_msg, _d9)                                                                                       \
        opts  (_10, arg__elems_type_validator_msg, _d10)                                                                                \
        opts  (_11, arg__elems_ptr_type_validator_msg, _d11)                                                                            \
        opts  (_12, arg__elems_ptr_value_checker_msg, _d12)
#define decl_1(_d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12)          \
        base_1(_d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, I##I,                                                          \
               type(_1, typename),                                                                                                      \
               type(_2,  template<bool arg__has_element_type_as_member_typedef_, typename arg__actual_parameter_> class),               \
               type(_3,  template<bool arg__has_size_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),       \
               type(_4,  template<bool arg__has_type_as_member_typedef_, typename arg__actual_parameter_> class),                       \
               type(_5,  template<bool arg__has_elems_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),      \
               type(_6,  template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),  \
               type(_7,  template<bool arg__is_valid_, typename arg__element_type_> class),                                             \
               type(_8,  template<bool arg__is_valid_, typename arg__size_type_> class),                                                \
               type(_9,  template<bool arg__is_valid_, typename arg__type_> class),                                                     \
               type(_10, template<bool arg__is_valid_, typename arg__elems_type_> class),                                               \
               type(_11, template<bool arg__is_valid_, typename arg__elems_ptr_type_> class),                                           \
               type(_12, template<bool arg__is_valid_, typename arg__elems_ptr_type_, arg__elems_ptr_type_ arg__elems_ptr_> class))
#define args_1(I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12)                                            \
        base_1((), (), (), (), (), (), (), (), (), (), (), _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12)
#define prms_1(...) decl_1((), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_1(_d2 (error::array::is_empty::arg_2_has_element_type_as_member_typedef),
                        _d3 (error::array::is_empty::arg_2_has_size_as_constexpr_copyable_data_member),
                        _d4 (error::array::is_empty::arg_2_has_type_as_member_typedef),
                        _d5 (error::array::is_empty::arg_2_has_elems_as_constexpr_copyable_data_member),
                        _d6 (error::array::is_empty::arg_2_has_elems_ptr_as_constexpr_copyable_data_member),
                        _d7 (error::array::is_empty::arg_2_element_type_is_valid),
                        _d8 (error::array::is_empty::arg_2_size_type_is_valid),
                        _d9 (error::array::is_empty::arg_2_type_is_valid),
                        _d10(error::array::is_empty::arg_2_elems_type_is_valid),
                        _d11(error::array::is_empty::arg_2_elems_ptr_type_is_valid),
                        _d12(error::array::is_empty::arg_2_elems_ptr_points_to_elems),
                        I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct is_empty
        /* {
         *   include std::integral_constant<bool, arg__Array__array::size == 0>;
         * }
         */;

        

        template<prms_1(I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct is_empty :
          array::validate<I, arg__Array__array, arg__type_validator_msg, arg__elems_type_validator_msg,
                          arg__elems_ptr_type_validator_msg, arg__elems_ptr_value_checker_msg,
                          arg__type_checker_msg, arg__elems_checker_msg, arg__elems_ptr_checker_msg,
                          arg__element_type_validator_msg, arg__size_type_validator_msg,
                          arg__element_type_checker_msg, arg__size_checker_msg>,
          utility::negation<I, utility::has_type_as_member_typedef<I, arg__Array__array>> {
        };

#include "v1_internals_end.hpp"
      }
      namespace array {

#include "v1_internals_begin.hpp"

#define base_1A(_d2, _d3, I, _1, _2, _3) I      \
        opts  (_1, arg__result, ())             \
          opts(_2, arg__lhs_idx, _d2)           \
          opts(_3, arg__rhs_idx, _d3)
#define decl_1A(_d2, _d3, I, _1, _2, _3)        \
        base_1A(_d2, _d3, I##I,                 \
                type(_1, typename),             \
                type(_2, v1::array::Idx),       \
                type(_3, v1::array::Idx))
#define args_1A(I, _1, _2, _3) base_1A((), (), I, _1, _2, _3)
#define prms_1A(...) decl_1A((), (), __VA_ARGS__)

        template<decl_1A(_d2(v1::array::Invalid_idx::value),
                         _d3(v1::array::Invalid_idx::value),
                         I, _, _, _)>
        struct elements_are_pairwise_distinct__base_class;

#define base_1B(I, _1) I                         \
        opts  (_1, arg__Array__array, ())
#define decl_1B(I, _1)                           \
        base_1B(I##I,                            \
               type(_1, typename))
#define args_1B(I, _1) base_1B(I, _1)
#define prms_1B(...) decl_1B(__VA_ARGS__)

#define base_2BA(_d1, _d2, _d3, _d4, args_1B, _1, _2, _3, _4)   \
        args_1B                                                 \
        opts  (_1, arg__i, _d1)                                 \
          opts(_2, arg__j, _d2)                                 \
          opts(_3, arg__is_not_equal, _d3)                      \
          opts(_4, arg__is_not_end_of_test, _d4)
#define decl_2BA(_d1, _d2, _d3, _d4, I, _1B_1, _1, _2, _3, _4)  \
        base_2BA(_d1, _d2, _d3, _d4,                            \
                 prms_1B(I, _1B_1),                             \
                 type(_1, v1::array::Idx),                      \
                 type(_2, v1::array::Idx),                      \
                 type(_3, bool),                                \
                 type(_4, bool))
#define args_2BA(I, _1B_1, _1, _2, _3, _4) base_2BA((), (), (), (), args_1B(I, _1B_1), _1, _2, _3, _4)
#define prms_2BA(...) decl_2BA((), (), (), (), __VA_ARGS__)

        template<decl_2BA(_d1(0),
                          _d2(1),
                          _d3(!utility::nontype_nontemplate_args_eq<H(I,
                                                                      const typename arg__Array__array::element_type *const *,
                                                                      const typename arg__Array__array::element_type *const *,
                                                                      arg__Array__array::elems_ptr, arg__Array__array::elems_ptr,
                                                                      0, 0, arg__i, arg__j)>::value),
                          _d4(arg__i + 2 < arg__Array__array::size),
                          I, _, _, _, _, _)>
        struct elements_are_pairwise_distinct__run;

#define base_2BB(args_1B)                       \
        args_1B
#define decl_2BB(I, _1B_1)                      \
        base_2BB(prms_1B(I, _1B_1))
#define args_2BB(I, _1B_1) base_2BB(args_1B(I, _1B_1))
#define prms_2BB(...) decl_2BB(__VA_ARGS__)

        template<prms_2BB(I, _)>
        struct elements_are_pairwise_distinct__array_checked;

#define base_3BBA(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, args_2BB, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)      \
        args_2BB                                                                                                                        \
        opts  (_1,  arg__element_type_checker_msg, _d1)                                                                                 \
          opts(_2,  arg__size_checker_msg, _d2)                                                                                         \
          opts(_3,  arg__type_checker_msg, _d3)                                                                                         \
        opts  (_4,  arg__elems_checker_msg, _d4)                                                                                        \
        opts  (_5,  arg__elems_ptr_checker_msg, _d5)                                                                                    \
        opts  (_6,  arg__element_type_validator_msg, _d6)                                                                               \
        opts  (_7,  arg__size_type_validator_msg, _d7)                                                                                  \
        opts  (_8,  arg__type_validator_msg, _d8)                                                                                       \
        opts  (_9,  arg__elems_type_validator_msg, _d9)                                                                                 \
        opts  (_10, arg__elems_ptr_type_validator_msg, _d10)                                                                            \
        opts  (_11, arg__elems_ptr_value_checker_msg, _d11)
#define decl_3BBA(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, I, _1B_1, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)              \
        base_3BBA(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11,                                                                      \
                  prms_2BB(I, _1B_1),                                                                                                           \
                  type(_1,  template<bool arg__has_element_type_as_member_typedef_, typename arg__actual_parameter_> class),                    \
                  type(_2,  template<bool arg__has_size_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),            \
                  type(_3,  template<bool arg__has_type_as_member_typedef_, typename arg__actual_parameter_> class),                            \
                  type(_4,  template<bool arg__has_elems_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),           \
                  type(_5,  template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),       \
                  type(_6,  template<bool arg__is_valid_, typename arg__element_type_> class),                                                  \
                  type(_7,  template<bool arg__is_valid_, typename arg__size_type_> class),                                                     \
                  type(_8,  template<bool arg__is_valid_, typename arg__type_> class),                                                          \
                  type(_9,  template<bool arg__is_valid_, typename arg__elems_type_> class),                                                    \
                  type(_10, template<bool arg__is_valid_, typename arg__elems_ptr_type_> class),                                                \
                  type(_11, template<bool arg__is_valid_, typename arg__elems_ptr_type_, arg__elems_ptr_type_ arg__elems_ptr_> class))
#define args_3BBA(I, _1B_1, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)                                                       \
        base_3BBA((), (), (), (), (), (), (), (), (), (), (), args_2BB(I, _1B_1), _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11)
#define prms_3BBA(...) decl_3BBA((), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_3BBA(_d1 (error::array::elements_are_pairwise_distinct::arg_2_has_element_type_as_member_typedef),
                           _d2 (error::array::elements_are_pairwise_distinct::arg_2_has_size_as_constexpr_copyable_data_member),
                           _d3 (error::array::elements_are_pairwise_distinct::arg_2_has_type_as_member_typedef),
                           _d4 (error::array::elements_are_pairwise_distinct::arg_2_has_elems_as_constexpr_copyable_data_member),
                           _d5 (error::array::elements_are_pairwise_distinct::arg_2_has_elems_ptr_as_constexpr_copyable_data_member),
                           _d6 (error::array::elements_are_pairwise_distinct::arg_2_element_type_is_valid),
                           _d7 (error::array::elements_are_pairwise_distinct::arg_2_size_type_is_valid),
                           _d8 (error::array::elements_are_pairwise_distinct::arg_2_type_is_valid),
                           _d9 (error::array::elements_are_pairwise_distinct::arg_2_elems_type_is_valid),
                           _d10(error::array::elements_are_pairwise_distinct::arg_2_elems_ptr_type_is_valid),
                           _d11(error::array::elements_are_pairwise_distinct::arg_2_elems_ptr_points_to_elems),
                           I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct elements_are_pairwise_distinct
        /* {
         *   include std::integral_constant<bool, [&]() {
         *     for (auto i = 0; i < arg__Array__array::size - 1; ++i) {
         *       for (auto j = i + 1; j < arg__Array__array::size; ++j) {
         *         if (arg__Array__array::elems[i] == arg__Array__array::elems[j]) {
         *           return false;
         *         }
         *       }
         *     }
         *     return true;
         *   }()>;
         *   static constexpr v1::array::Idx lhs_idx;
         *   static constexpr v1::array::Idx rhs_idx;
         *   invariant {
         *     if (self::value == false) {
         *       lhs_idx < rhs_idx;
         *       self[lhs_idx] != self[rhs_idx];
         *     }
         *   }
         * }
         */;

#define base_3BBB(I, _1, args_2BB) I                            \
        opts  (_1, arg__array_is_empty_or_singleton, ())        \
          args_2BB
#define decl_3BBB(I, _1, _1B_1)                 \
        base_3BBB(I##I,                         \
                  type(_1, bool),               \
                  prms_2BB(, _1B_1))
#define args_3BBB(I, _1, _1B_1) base_3BBB(I, _1, args_2BB(, _1B_1))
#define prms_3BBB(...) decl_3BBB(__VA_ARGS__)

        template<prms_3BBB(I, _, _)>
        struct elements_are_pairwise_distinct__result;

        

        template<prms_3BBA(I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct elements_are_pairwise_distinct :
          array::validate<I, arg__Array__array, arg__type_validator_msg, arg__elems_type_validator_msg,
                          arg__elems_ptr_type_validator_msg, arg__elems_ptr_value_checker_msg,
                          arg__type_checker_msg, arg__elems_checker_msg, arg__elems_ptr_checker_msg,
                          arg__element_type_validator_msg, arg__size_type_validator_msg,
                          arg__element_type_checker_msg, arg__size_checker_msg>,
          elements_are_pairwise_distinct__array_checked<args_2BB(I, _)> {
        };

        

        template<prms_2BB(I, _)>
        struct elements_are_pairwise_distinct__array_checked :
          elements_are_pairwise_distinct__result<args_3BBB(I, R(arg__Array__array::size <= 1), _)> {
        };

        

        template<prms_3BBB(I, X, _)>
        struct elements_are_pairwise_distinct__result<args_3BBB(I, R(true), _)> :
          elements_are_pairwise_distinct__base_class<args_1A(I, R(std::true_type), X, X)> {
        };

        template<prms_3BBB(I, _, _)>
        struct elements_are_pairwise_distinct__result :
          elements_are_pairwise_distinct__run<args_2BA(I, _, X, X, X, X)> {
        };

        

        template<prms_2BA(I, _, _, _, X, _)>
        struct elements_are_pairwise_distinct__run<args_2BA(I, _, _, _, R(false), _)> :
          elements_are_pairwise_distinct__base_class<args_1A(I, R(std::false_type), R(arg__i), R(arg__j))> {
        };

        template<prms_2BA(I, _, _, _, X, X)>
        struct elements_are_pairwise_distinct__run<args_2BA(I, _, _, _, R(true), R(false))> :
          elements_are_pairwise_distinct__base_class<args_1A(I, R(std::true_type), X, X)> {
        };

        template<prms_2BA(I, _, _, _, X, X)>
        struct elements_are_pairwise_distinct__run<args_2BA(I, _, _, _, R(true), R(true))> :
          elements_are_pairwise_distinct__run<args_2BA(I, _,
                                                       R(arg__i + (arg__j + 1 < arg__Array__array::size ? 0          :          1)),
                                                       R(          arg__j + 1 < arg__Array__array::size ? arg__j + 1 : arg__i + 2 ), X, X)> {
        };

        

        template<prms_1A(I, _, _, _)>
        struct elements_are_pairwise_distinct__base_class :
          arg__result
        {
          static constexpr v1::array::Idx lhs_idx {arg__lhs_idx};
          static constexpr v1::array::Idx rhs_idx {arg__rhs_idx};
        };

#include "v1_internals_end.hpp"
      }
      namespace array {

#include "v1_internals_begin.hpp"

#define base_1A(_d2, I, _1, _2) I               \
        opts  (_1, arg__result, ())             \
          opts(_2, arg__idx, _d2)
#define decl_1A(_d2, I, _1, _2)                 \
        base_1A(_d2, I##I,                      \
                type(_1, typename),             \
                type(_2, v1::array::Idx))
#define args_1A(I, _1, _2) base_1A((), I, _1, _2)
#define prms_1A(...) decl_1A((), __VA_ARGS__)

        template<decl_1A(_d2(v1::array::Invalid_idx::value),
                         I, _, _)>
        struct are_elementwise_equal__base_class;

#define base_1B(I, _1, _2) I                    \
        opts  (_1, arg__Array__lhs_array, ())   \
          opts(_2, arg__Array__rhs_array, ())
#define decl_1B(I, _1, _2)                      \
        base_1B(I##I,                           \
                type(_1, typename),             \
                type(_2, typename))
#define args_1B(I, _1, _2) base_1B(I, _1, _2)
#define prms_1B(...) decl_1B(__VA_ARGS__)

#define base_2BC(_d1, _d2, _d3, args_1B, _1, _2, _3)    \
        args_1B                                         \
        opts  (_1, arg__i, _d1)                         \
          opts(_2, arg__is_equal, _d2)                  \
          opts(_3, arg__is_not_end_of_array, _d3)
#define decl_2BC(_d1, _d2, _d3, I, _1B_1, _1B_2, _1, _2, _3)    \
        base_2BC(_d1, _d2, _d3,                                 \
                 prms_1B(I, _1B_1, _1B_2),                      \
                 type(_1, v1::array::Idx),                      \
                 type(_2, bool),                                \
                 type(_3, bool))
#define args_2BC(I, _1B_1, _1B_2, _1, _2, _3) base_2BC((), (), (), args_1B(I, _1B_1, _1B_2), _1, _2, _3)
#define prms_2BC(...) decl_2BC((), (), (), __VA_ARGS__)

        template<decl_2BC(_d1(0),
                          _d2(utility::nontype_nontemplate_args_eq<H(I,
                                                                     const typename arg__Array__lhs_array::element_type *const *,
                                                                     const typename arg__Array__rhs_array::element_type *const *,
                                                                     arg__Array__lhs_array::elems_ptr, arg__Array__rhs_array::elems_ptr,
                                                                     0, 0, arg__i, arg__i)>::value),
                          _d3(arg__i + 1 < arg__Array__lhs_array::size),
                          I, _, _, _, _, _)>
        struct are_elementwise_equal__run;

#define base_2BA(I, _1, args_1B) I                      \
        opts  (_1, arg__lhs_and_rhs_are_empty, ())      \
          args_1B
#define decl_2BA(I, _1, _1B_1, _1B_2)           \
        base_2BA(I##I,                          \
                 type(_1, bool),                \
                 prms_1B(, _1B_1, _1B_2))
#define args_2BA(I, _1, _1B_1, _1B_2) base_2BA(I, _1, args_1B(, _1B_1, _1B_2))
#define prms_2BA(...) decl_2BA(__VA_ARGS__)

        template<prms_2BA(I, _, _, _)>
        struct are_elementwise_equal__result;

#define base_2BB(_d1, _d2, args_1B, _1, _2)                     \
        args_1B                                                 \
        opts  (_1, arg__equal_element_type_checker_msg, _d1)    \
          opts(_2, arg__equal_size_checker_msg, _d2)
#define decl_2BB(_d1, _d2, I, _1B_1, _1B_2, _1, _2)                                                                                                \
        base_2BB(_d1, _d2,                                                                                                                         \
                 prms_1B(I, _1B_1, _1B_2),                                                                                                         \
                 type(_1, template<bool arg__are_of_equal_element_type_, typename arg__lhs_element_type_, typename arg__rhs_element_type_> class), \
                 type(_2, template<bool arg__are_of_equal_size_, v1::array::Size arg__lhs_size_, v1::array::Size arg__rhs_size_> class))
#define args_2BB(I, _1B_1, _1B_2, _1, _2) base_2BB((), (), args_1B(I, _1B_1, _1B_2), _1, _2)
#define prms_2BB(...) decl_2BB((), (), __VA_ARGS__)

        template<prms_2BB(I, _, _, _, _)>
        struct are_elementwise_equal__array_checked;

#define base_3BB(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18, _d19, _d20, _d21, _d22,     \
                 args_2BB, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22)                 \
        args_2BB                                                                                                                                \
        opts  (_1,  arg__2_element_type_checker_msg, _d1)                                                                                       \
          opts(_2,  arg__2_size_checker_msg, _d2)                                                                                               \
          opts(_3,  arg__2_type_checker_msg, _d3)                                                                                               \
        opts  (_4,  arg__2_elems_checker_msg, _d4)                                                                                              \
        opts  (_5,  arg__2_elems_ptr_checker_msg, _d5)                                                                                          \
        opts  (_6,  arg__2_element_type_validator_msg, _d6)                                                                                     \
        opts  (_7,  arg__2_size_type_validator_msg, _d7)                                                                                        \
        opts  (_8,  arg__2_type_validator_msg, _d8)                                                                                             \
        opts  (_9,  arg__2_elems_type_validator_msg, _d9)                                                                                       \
        opts  (_10, arg__2_elems_ptr_type_validator_msg, _d10)                                                                                  \
        opts  (_11, arg__2_elems_ptr_value_checker_msg, _d11)                                                                                   \
        opts  (_12,  arg__3_element_type_checker_msg, _d12)                                                                                     \
        opts  (_13,  arg__3_size_checker_msg, _d13)                                                                                             \
        opts  (_14,  arg__3_type_checker_msg, _d14)                                                                                             \
        opts  (_15,  arg__3_elems_checker_msg, _d15)                                                                                            \
        opts  (_16,  arg__3_elems_ptr_checker_msg, _d16)                                                                                        \
        opts  (_17,  arg__3_element_type_validator_msg, _d17)                                                                                   \
        opts  (_18,  arg__3_size_type_validator_msg, _d18)                                                                                      \
        opts  (_19,  arg__3_type_validator_msg, _d19)                                                                                           \
        opts  (_20,  arg__3_elems_type_validator_msg, _d20)                                                                                     \
        opts  (_21, arg__3_elems_ptr_type_validator_msg, _d21)                                                                                  \
        opts  (_22, arg__3_elems_ptr_value_checker_msg, _d22)
#define decl_3BB(_2BB_d1, _2BB_d2,                                                                                                              \
                 _d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18, _d19, _d20, _d21, _d22,     \
                 I, _1B_1, _1B_2, _2BB_1, _2BB_2,                                                                                               \
                 _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22)                           \
        base_3BB(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, _d11, _d12, _d13, _d14, _d15, _d16, _d17, _d18, _d19, _d20, _d21, _d22,     \
                 decl_2BB(_2BB_d1, _2BB_d2, I, _1B_1, _1B_2, _2BB_1, _2BB_2),                                                                   \
                 type(_1,  template<bool arg__has_element_type_as_member_typedef_, typename arg__actual_parameter_> class),                     \
                 type(_2,  template<bool arg__has_size_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),             \
                 type(_3,  template<bool arg__has_type_as_member_typedef_, typename arg__actual_parameter_> class),                             \
                 type(_4,  template<bool arg__has_elems_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),            \
                 type(_5,  template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),        \
                 type(_6,  template<bool arg__is_valid_, typename arg__element_type_> class),                                                   \
                 type(_7,  template<bool arg__is_valid_, typename arg__size_type_> class),                                                      \
                 type(_8,  template<bool arg__is_valid_, typename arg__type_> class),                                                           \
                 type(_9,  template<bool arg__is_valid_, typename arg__elems_type_> class),                                                     \
                 type(_10, template<bool arg__is_valid_, typename arg__elems_ptr_type_> class),                                                 \
                 type(_11, template<bool arg__is_valid_, typename arg__elems_ptr_type_, arg__elems_ptr_type_ arg__elems_ptr_> class),           \
                 type(_12,  template<bool arg__has_element_type_as_member_typedef_, typename arg__actual_parameter_> class),                    \
                 type(_13,  template<bool arg__has_size_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),            \
                 type(_14,  template<bool arg__has_type_as_member_typedef_, typename arg__actual_parameter_> class),                            \
                 type(_15,  template<bool arg__has_elems_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),           \
                 type(_16,  template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member_, typename arg__actual_parameter_> class),       \
                 type(_17,  template<bool arg__is_valid_, typename arg__element_type_> class),                                                  \
                 type(_18,  template<bool arg__is_valid_, typename arg__size_type_> class),                                                     \
                 type(_19,  template<bool arg__is_valid_, typename arg__type_> class),                                                          \
                 type(_20,  template<bool arg__is_valid_, typename arg__elems_type_> class),                                                    \
                 type(_21, template<bool arg__is_valid_, typename arg__elems_ptr_type_> class),                                                 \
                 type(_22, template<bool arg__is_valid_, typename arg__elems_ptr_type_, arg__elems_ptr_type_ arg__elems_ptr_> class))
#define args_3BB(I, _1B_1, _1B_2, _2BB_1, _2BB_2,                                                                       \
                 _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22)   \
        base_3BB((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (),        \
                 args_2BB(I, _1B_1, _1B_2, _2BB_1, _2BB_2),                                                             \
                 _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22)
#define prms_3BB(...) decl_3BB((), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_3BB(_2BB_d1(error::array::are_elementwise_equal::arrays_are_of_equal_element_type),
                          _2BB_d2(error::array::are_elementwise_equal::arrays_are_of_equal_size),
                          _d1    (error::array::are_elementwise_equal::arg_2_has_element_type_as_member_typedef),
                          _d2    (error::array::are_elementwise_equal::arg_2_has_size_as_constexpr_copyable_data_member),
                          _d3    (error::array::are_elementwise_equal::arg_2_has_type_as_member_typedef),
                          _d4    (error::array::are_elementwise_equal::arg_2_has_elems_as_constexpr_copyable_data_member),
                          _d5    (error::array::are_elementwise_equal::arg_2_has_elems_ptr_as_constexpr_copyable_data_member),
                          _d6    (error::array::are_elementwise_equal::arg_2_element_type_is_valid),
                          _d7    (error::array::are_elementwise_equal::arg_2_size_type_is_valid),
                          _d8    (error::array::are_elementwise_equal::arg_2_type_is_valid),
                          _d9    (error::array::are_elementwise_equal::arg_2_elems_type_is_valid),
                          _d10   (error::array::are_elementwise_equal::arg_2_elems_ptr_type_is_valid),
                          _d11   (error::array::are_elementwise_equal::arg_2_elems_ptr_points_to_elems),
                          _d12   (error::array::are_elementwise_equal::arg_3_has_element_type_as_member_typedef),
                          _d13   (error::array::are_elementwise_equal::arg_3_has_size_as_constexpr_copyable_data_member),
                          _d14   (error::array::are_elementwise_equal::arg_3_has_type_as_member_typedef),
                          _d15   (error::array::are_elementwise_equal::arg_3_has_elems_as_constexpr_copyable_data_member),
                          _d16   (error::array::are_elementwise_equal::arg_3_has_elems_ptr_as_constexpr_copyable_data_member),
                          _d17   (error::array::are_elementwise_equal::arg_3_element_type_is_valid),
                          _d18   (error::array::are_elementwise_equal::arg_3_size_type_is_valid),
                          _d19   (error::array::are_elementwise_equal::arg_3_type_is_valid),
                          _d20   (error::array::are_elementwise_equal::arg_3_elems_type_is_valid),
                          _d21   (error::array::are_elementwise_equal::arg_3_elems_ptr_type_is_valid),
                          _d22   (error::array::are_elementwise_equal::arg_3_elems_ptr_points_to_elems),
                          I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct are_elementwise_equal
        /* {
         *   invariant {
         *     arg__Array__lhs_array::size == arg__Array__rhs_array::size;
         *   }
         *   include std::integral_constant<bool, [&]() {
         *     for (auto i = 0; i < arg__Array__lhs_array::size; ++i) {
         *       for (auto j = 0; j < arg__Array__rhs_array::size; ++j) {
         *         if (arg__Array__lhs_array[i] != arg__Array__rhs_array[j]) {
         *           return false;
         *         }
         *       }
         *     }
         *     return true;
         *   }()>;
         *   static constexpr v1::array::Idx idx;
         *   invariant {
         *     if (self::value == false) {
         *       arg__Array__lhs_array[idx] != arg__Array__rhs_array[idx];
         *     }
         *   }
         * }
         */;

        

        template<prms_3BB(I, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct are_elementwise_equal :
          array::validate<I, arg__Array__lhs_array, arg__2_type_validator_msg, arg__2_elems_type_validator_msg,
                          arg__2_elems_ptr_type_validator_msg, arg__2_elems_ptr_value_checker_msg,
                          arg__2_type_checker_msg, arg__2_elems_checker_msg, arg__2_elems_ptr_checker_msg,
                          arg__2_element_type_validator_msg, arg__2_size_type_validator_msg,
                          arg__2_element_type_checker_msg, arg__2_size_checker_msg>,
          array::validate<I, arg__Array__rhs_array, arg__3_type_validator_msg, arg__3_elems_type_validator_msg,
                          arg__3_elems_ptr_type_validator_msg, arg__3_elems_ptr_value_checker_msg,
                          arg__3_type_checker_msg, arg__3_elems_checker_msg, arg__3_elems_ptr_checker_msg,
                          arg__3_element_type_validator_msg, arg__3_size_type_validator_msg,
                          arg__3_element_type_checker_msg, arg__3_size_checker_msg>,
          are_elementwise_equal__array_checked<args_2BB(I, _, _, _, _)> {
        };

        

        template<prms_2BB(I, _, _, _, _)>
        struct are_elementwise_equal__array_checked :
          arg__equal_element_type_checker_msg<H(std::is_same<H(typename arg__Array__lhs_array::element_type,
                                                               typename arg__Array__rhs_array::element_type)>::value,
                                                typename arg__Array__lhs_array::element_type, typename arg__Array__rhs_array::element_type)>,
          arg__equal_size_checker_msg<H(arg__Array__lhs_array::size == arg__Array__rhs_array::size,
                                        arg__Array__lhs_array::size, arg__Array__rhs_array::size)>,
          are_elementwise_equal__result<args_2BA(I, R(arg__Array__lhs_array::size == 0), _, _)> {
        };

        

        template<prms_2BA(I, X, _, _)>
        struct are_elementwise_equal__result<args_2BA(I, R(true), _, _)> :
          are_elementwise_equal__base_class<args_1A(I, R(std::true_type), X)> {
        };

        template<prms_2BA(I, _, _, _)>
        struct are_elementwise_equal__result :
          are_elementwise_equal__run<args_2BC(I, _, _, X, X, X)> {
        };

        

        template<prms_2BC(I, _, _, _, X, X)>
        struct are_elementwise_equal__run<args_2BC(I, _, _, _, R(true), R(true))> :
          are_elementwise_equal__run<args_2BC(I, _, _, R(arg__i + 1), X, X)> {
        };

        template<prms_2BC(I, _, _, _, X, X)>
        struct are_elementwise_equal__run<args_2BC(I, _, _, _, R(true), R(false))> :
          are_elementwise_equal__base_class<args_1A(I, R(std::true_type), X)> {
        };

        template<prms_2BC(I, _, _, _, X, _)>
        struct are_elementwise_equal__run<args_2BC(I, _, _, _, R(false), _)> :
          are_elementwise_equal__base_class<args_1A(I, R(std::false_type), R(arg__i))> {
        };

        

        template<prms_1A(I, _, _)>
        struct are_elementwise_equal__base_class :
          arg__result
        {
          static constexpr v1::array::Idx idx {arg__idx};
        };

#include "v1_internals_end.hpp"
      }
    }
  }
}

namespace tice {
  namespace v1 {

    template<typename arg__array_element_type, arg__array_element_type... args__element>
    class Array :
      public internals::array::construct<I, false, arg__array_element_type,
                                         internals::tuple::construct<I, std::integral_constant<arg__array_element_type, args__element>...>,
                                         error::array::arg_1_is_valid_array_element_type,
                                         error::array::array_size_is_positive> {
    };

    namespace array {

      template<typename arg>
      class element_type_is_valid :
        public internals::array::element_type_is_valid<I, arg> {
      };

      template<typename arg__array_element_type>
      class Empty :
        public internals::array::construct<I, true, arg__array_element_type, internals::tuple::construct<I>,
                                           error::array::empty::arg_1_is_valid_array_element_type> {
      };

      template<typename arg__Array__array>
      class is_empty :
        public internals::array::is_empty<I, arg__Array__array,
                                          error::array::is_empty::arg_1_has_element_type_as_member_typedef,
                                          error::array::is_empty::arg_1_has_size_as_constexpr_copyable_data_member,
                                          error::array::is_empty::arg_1_has_type_as_member_typedef,
                                          error::array::is_empty::arg_1_has_elems_as_constexpr_copyable_data_member,
                                          error::array::is_empty::arg_1_has_elems_ptr_as_constexpr_copyable_data_member,
                                          error::array::is_empty::arg_1_element_type_is_valid,
                                          error::array::is_empty::arg_1_size_type_is_valid,
                                          error::array::is_empty::arg_1_type_is_valid,
                                          error::array::is_empty::arg_1_elems_type_is_valid,
                                          error::array::is_empty::arg_1_elems_ptr_type_is_valid,
                                          error::array::is_empty::arg_1_elems_ptr_points_to_elems> {
      };

      namespace make {

        template<Size arg__N, typename arg__init_val_with_value_as_constexpr_copyable_data_member, typename arg__array_element_type>
        class value :
          public internals::array::make::value<I, arg__N, arg__init_val_with_value_as_constexpr_copyable_data_member, arg__array_element_type,
                                               error::array::make::value::arg_1_is_positive,
                                               error::array::make::value::value_is_convertible_to_array_element_type,
                                               error::array::make::value::arg_2_has_value_as_constexpr_copyable_data_member> {
        };

        namespace from_tuple {

          template<typename arg__Tuple__of__type_with_value_as_constexpr_copyable_data_member, typename arg__array_element_type>
          class value :
            public internals::array::make::from_tuple::value<I,
                                                             arg__Tuple__of__type_with_value_as_constexpr_copyable_data_member,
                                                             arg__array_element_type,
                                                             H(error::array::make::from_tuple::value::
                                                               element_has_value_as_constexpr_copyable_data_member),
                                                             H(error::array::make::from_tuple::value::
                                                               element_value_is_convertible_to_array_element_type),
                                                             H(error::array::make::from_tuple::value::
                                                               arg_1_is_Tuple),
                                                             H(error::array::make::from_tuple::value::
                                                               arg_1_is_nonempty)> {
          };

        }

        namespace nested {

          template<typename... args__Array>
          class value :
            public internals::array::make::nested::value<I, internals::tuple::construct<I, args__Array...>,
                                                         error::array::make::nested::value::nested_array_element_type_is_uniform,
                                                         error::array::make::nested::value::nested_array_size_is_positive,
                                                         error::array::make::nested::value::nested_array_has_element_type_as_member_typedef,
                                                         error::array::make::nested::value::nested_array_has_size_as_constexpr_copyable_data_member,
                                                         error::array::make::nested::value::nested_array_has_type_as_member_typedef,
                                                         H(error::array::make::nested::value::
                                                           nested_array_has_elems_as_constexpr_copyable_data_member),
                                                         H(error::array::make::nested::value::
                                                           nested_array_has_elems_ptr_as_constexpr_copyable_data_member),
                                                         error::array::make::nested::value::nested_array_element_type_is_valid,
                                                         error::array::make::nested::value::nested_array_size_type_is_valid,
                                                         error::array::make::nested::value::nested_array_type_is_valid,
                                                         error::array::make::nested::value::nested_array_elems_type_is_valid,
                                                         error::array::make::nested::value::nested_array_elems_ptr_type_is_valid,
                                                         error::array::make::nested::value::nested_array_elems_ptr_points_to_elems,
                                                         error::array::make::nested::value::array_is_nonempty> {
          };

          namespace from_tuple {

            template<typename arg__Tuple__of__Array>
            class value :
              public internals::array::make::nested::value<I, arg__Tuple__of__Array,
                                                           error::array::make::nested::from_tuple::value::nested_array_element_type_is_uniform,
                                                           error::array::make::nested::from_tuple::value::nested_array_size_is_positive,
                                                           H(error::array::make::nested::from_tuple::value::
                                                             nested_array_has_element_type_as_member_typedef),
                                                           H(error::array::make::nested::from_tuple::value::
                                                             nested_array_has_size_as_constexpr_copyable_data_member),
                                                           error::array::make::nested::from_tuple::value::nested_array_has_type_as_member_typedef,
                                                           H(error::array::make::nested::from_tuple::value::
                                                             nested_array_has_elems_as_constexpr_copyable_data_member),
                                                           H(error::array::make::nested::from_tuple::value::
                                                             nested_array_has_elems_ptr_as_constexpr_copyable_data_member),
                                                           error::array::make::nested::from_tuple::value::nested_array_element_type_is_valid,
                                                           error::array::make::nested::from_tuple::value::nested_array_size_type_is_valid,
                                                           error::array::make::nested::from_tuple::value::nested_array_type_is_valid,
                                                           error::array::make::nested::from_tuple::value::nested_array_elems_type_is_valid,
                                                           error::array::make::nested::from_tuple::value::nested_array_elems_ptr_type_is_valid,
                                                           error::array::make::nested::from_tuple::value::nested_array_elems_ptr_points_to_elems,
                                                           error::array::make::nested::from_tuple::value::arg_1_is_nonempty,
                                                           error::array::make::nested::from_tuple::value::arg_1_is_Tuple> {
            };

          }

        }

      }

      namespace update {

        template<typename arg__Array__prev_array, typename arg__Tuple__of__dict_Entry__of__prev_array_element_type__update_list,
                 typename arg__on_dupd_index, typename arg__on_stray_index>
        class value :
          public internals::array::update::value<I, arg__Array__prev_array, arg__Tuple__of__dict_Entry__of__prev_array_element_type__update_list,
                                                 arg__on_dupd_index, arg__on_stray_index,
                                                 error::array::update::value::update_list_element_has_key_as_constexpr_copyable_data_member,
                                                 error::array::update::value::update_list_element_has_value_as_constexpr_copyable_data_member,
                                                 error::array::update::value::update_list_element_key_type_is_valid,
                                                 error::array::update::value::update_list_element_value_is_convertible_to_array_element_type,
                                                 error::array::update::value::update_list_key_is_within_array_bound,
                                                 error::array::update::value::update_list_key_has_no_duplicate,
                                                 error::array::update::value::update_list_key_duplicates_are_identical,
                                                 error::array::update::value::arg_1_has_element_type_as_member_typedef,
                                                 error::array::update::value::arg_1_has_size_as_constexpr_copyable_data_member,
                                                 error::array::update::value::arg_1_has_type_as_member_typedef,
                                                 error::array::update::value::arg_1_has_elems_as_constexpr_copyable_data_member,
                                                 error::array::update::value::arg_1_has_elems_ptr_as_constexpr_copyable_data_member,
                                                 error::array::update::value::arg_1_element_type_is_valid,
                                                 error::array::update::value::arg_1_size_type_is_valid,
                                                 error::array::update::value::arg_1_type_is_valid,
                                                 error::array::update::value::arg_1_elems_type_is_valid,
                                                 error::array::update::value::arg_1_elems_ptr_type_is_valid,
                                                 error::array::update::value::arg_1_elems_ptr_points_to_elems,
                                                 error::array::update::value::arg_2_is_Tuple,
                                                 error::array::update::value::arg_3_is_member_of_on_dupd_index,
                                                 error::array::update::value::arg_4_is_member_of_on_stray_index> {
        };

      }

      namespace filter_then_map {

        template<typename arg__Array__prev_array, typename arg__array_element_type,
                 Pair<bool,
                      arg__array_element_type> (*arg__filter_and_mapper)(const Idx &idx,
                                                                         const typename arg__Array__prev_array::element_type &prev_array_element)>
        class value :
          public internals::array::filter_then_map::value<I, arg__Array__prev_array, arg__array_element_type,
                                                          &internals::array::filter_then_map::filter_and_mapper_adaptor<I, arg__Array__prev_array,
                                                                                                                        arg__array_element_type,
                                                                                                                        arg__filter_and_mapper>,
                                                          H(error::array::filter_then_map::value::
                                                            arg_1_has_element_type_as_member_typedef),
                                                          H(error::array::filter_then_map::value::
                                                            arg_1_has_size_as_constexpr_copyable_data_member),
                                                          H(error::array::filter_then_map::value::
                                                            arg_1_has_type_as_member_typedef),
                                                          H(error::array::filter_then_map::value::
                                                            arg_1_has_elems_as_constexpr_copyable_data_member),
                                                          H(error::array::filter_then_map::value::
                                                            arg_1_has_elems_ptr_as_constexpr_copyable_data_member),
                                                          H(error::array::filter_then_map::value::
                                                            arg_1_element_type_is_valid),
                                                          H(error::array::filter_then_map::value::
                                                            arg_1_size_type_is_valid),
                                                          H(error::array::filter_then_map::value::
                                                            arg_1_type_is_valid),
                                                          H(error::array::filter_then_map::value::
                                                            arg_1_elems_type_is_valid),
                                                          H(error::array::filter_then_map::value::
                                                            arg_1_elems_ptr_type_is_valid),
                                                          H(error::array::filter_then_map::value::
                                                            arg_1_elems_ptr_points_to_elems),
                                                          H(error::array::filter_then_map::value::
                                                            arg_2_is_valid_array_element_type),
                                                          H(error::array::filter_then_map::value::
                                                            arg_3_is_not_nullptr)> {
        };

      }

      template<typename arg__Array__array>
      class elements_are_pairwise_distinct :
        public internals::array::elements_are_pairwise_distinct<I, arg__Array__array,
                                                                H(error::array::elements_are_pairwise_distinct::
                                                                  arg_1_has_element_type_as_member_typedef),
                                                                H(error::array::elements_are_pairwise_distinct::
                                                                  arg_1_has_size_as_constexpr_copyable_data_member),
                                                                H(error::array::elements_are_pairwise_distinct::
                                                                  arg_1_has_type_as_member_typedef),
                                                                H(error::array::elements_are_pairwise_distinct::
                                                                  arg_1_has_elems_as_constexpr_copyable_data_member),
                                                                H(error::array::elements_are_pairwise_distinct::
                                                                  arg_1_has_elems_ptr_as_constexpr_copyable_data_member),
                                                                H(error::array::elements_are_pairwise_distinct::
                                                                  arg_1_element_type_is_valid),
                                                                H(error::array::elements_are_pairwise_distinct::
                                                                  arg_1_size_type_is_valid),
                                                                H(error::array::elements_are_pairwise_distinct::
                                                                  arg_1_type_is_valid),
                                                                H(error::array::elements_are_pairwise_distinct::
                                                                  arg_1_elems_type_is_valid),
                                                                H(error::array::elements_are_pairwise_distinct::
                                                                  arg_1_elems_ptr_type_is_valid),
                                                                H(error::array::elements_are_pairwise_distinct::
                                                                  arg_1_elems_ptr_points_to_elems)> {
      };

      template<typename arg__Array__lhs_array, typename arg__Array__rhs_array>
      class are_elementwise_equal :
        public internals::array::are_elementwise_equal<I, arg__Array__lhs_array, arg__Array__rhs_array,
                                                       error::array::are_elementwise_equal::arrays_are_of_equal_element_type,
                                                       error::array::are_elementwise_equal::arrays_are_of_equal_size,
                                                       error::array::are_elementwise_equal::arg_1_has_element_type_as_member_typedef,
                                                       error::array::are_elementwise_equal::arg_1_has_size_as_constexpr_copyable_data_member,
                                                       error::array::are_elementwise_equal::arg_1_has_type_as_member_typedef,
                                                       error::array::are_elementwise_equal::arg_1_has_elems_as_constexpr_copyable_data_member,
                                                       error::array::are_elementwise_equal::arg_1_has_elems_ptr_as_constexpr_copyable_data_member,
                                                       error::array::are_elementwise_equal::arg_1_element_type_is_valid,
                                                       error::array::are_elementwise_equal::arg_1_size_type_is_valid,
                                                       error::array::are_elementwise_equal::arg_1_type_is_valid,
                                                       error::array::are_elementwise_equal::arg_1_elems_type_is_valid,
                                                       error::array::are_elementwise_equal::arg_1_elems_ptr_type_is_valid,
                                                       error::array::are_elementwise_equal::arg_1_elems_ptr_points_to_elems,
                                                       error::array::are_elementwise_equal::arg_2_has_element_type_as_member_typedef,
                                                       error::array::are_elementwise_equal::arg_2_has_size_as_constexpr_copyable_data_member,
                                                       error::array::are_elementwise_equal::arg_2_has_type_as_member_typedef,
                                                       error::array::are_elementwise_equal::arg_2_has_elems_as_constexpr_copyable_data_member,
                                                       error::array::are_elementwise_equal::arg_2_has_elems_ptr_as_constexpr_copyable_data_member,
                                                       error::array::are_elementwise_equal::arg_2_element_type_is_valid,
                                                       error::array::are_elementwise_equal::arg_2_size_type_is_valid,
                                                       error::array::are_elementwise_equal::arg_2_type_is_valid,
                                                       error::array::are_elementwise_equal::arg_2_elems_type_is_valid,
                                                       error::array::are_elementwise_equal::arg_2_elems_ptr_type_is_valid,
                                                       error::array::are_elementwise_equal::arg_2_elems_ptr_points_to_elems> {
      };

    }
  }
}
