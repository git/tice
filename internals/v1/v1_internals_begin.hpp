/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

//\begin{frequently maintained part}
#ifdef _1_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d1(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _1_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d2(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _1_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d3(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _1_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d4(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _1_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d5(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _1_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d6(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _1_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d7(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _1_d8
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d8(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _1_d9
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d9(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _1_d10
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d10(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1_d11
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d11(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1_d12
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d12(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1_d13
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d13(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1_d14
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1_d14(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1A_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1A_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1A_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1A_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1A_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1A_d3(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1A_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1A_d4(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1A_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1A_d5(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1A_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1A_d6(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1A_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1A_d7(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1A_d8
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1A_d8(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1A_d9
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1A_d9(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1A_d10
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1A_d10(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1A_d11
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1A_d11(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Ab_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Ab_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Ac_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Ac_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Ag_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Ag_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Ai_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Ai_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Aj_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Aj_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Aj_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Aj_d2(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Ak_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Ak_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Am_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Am_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1B_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1B_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1B_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1B_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1B_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1B_d3(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1B_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1B_d4(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1B_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1B_d5(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1B_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1B_d6(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1B_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1B_d7(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1C_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1C_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1C_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1C_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1C_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1C_d3(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1C_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1C_d4(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1C_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1C_d5(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1C_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1C_d6(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1C_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1C_d7(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1C_d8
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1C_d8(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1D_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1D_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1D_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1D_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1D_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1D_d3(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1E_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1E_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1F_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1F_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1F_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1F_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1G_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1G_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1G_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1G_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1G_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1G_d3(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1G_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1G_d4(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1G_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1G_d5(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1G_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1G_d6(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1G_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1G_d7(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1G_d8
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1G_d8(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d3(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d4(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d5(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d6(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d7(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d8
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d8(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d9
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d9(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d10
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d10(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d11
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d11(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d12
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d12(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d13
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d13(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d14
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d14(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d15
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d15(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d16
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d16(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d17
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d17(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d18
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d18(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d19
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d19(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d20
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d20(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d21
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d21(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d22
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d22(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1J_d23
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1J_d23(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1K_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1K_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1K_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1K_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1N_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1N_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1P_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1P_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Q_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Q_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1R_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1R_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1X_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1X_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d3(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d4(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d5(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d6(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d7(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d8
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d8(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d9
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d9(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d10
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d10(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d11
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d11(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d12
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d12(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d13
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d13(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d14
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d14(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d15
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d15(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d16
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d16(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d17
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d17(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d18
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d18(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d19
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d19(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d20
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d20(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d21
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d21(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d22
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d22(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d23
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d23(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d24
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d24(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d25
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d25(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d26
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d26(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Y_d27
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Y_d27(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d3(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d4(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d5(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d6(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d7(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d8
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d8(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d9
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d9(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d10
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d10(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d11
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d11(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d12
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d12(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d13
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d13(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d14
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d14(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d15
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d15(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d16
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d16(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d17
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d17(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d18
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d18(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d19
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d19(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d20
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d20(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d21
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d21(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d22
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d22(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d23
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d23(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _1Z_d24
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _1Z_d24(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2_d2(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _2_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2_d3(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _2_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2_d4(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _2_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2_d5(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _2_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2_d6(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _2_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2_d7(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _2A_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2A_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _2A_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2A_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _2AA_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2AA_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2AA_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2AA_d2(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2AA_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2AA_d3(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2AA_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2AA_d4(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2AA_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2AA_d5(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2AA_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2AA_d6(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2AA_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2AA_d7(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2Aa_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2Aa_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2Ab_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2Ab_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2Ab_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2Ab_d2(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2Ab_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2Ab_d3(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2Ab_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2Ab_d4(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2AiB_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2AiB_d1(...)                                                                                                                              \
  (= __VA_ARGS__)
#endif
#ifdef _2B_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2B_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _2BA_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2BA_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2BB_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2BB_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2BB_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2BB_d2(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2C_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2C_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _2C_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2C_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _2C_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2C_d3(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _2C_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2C_d4(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _2C_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2C_d5(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _2C_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2C_d6(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _2C_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2C_d7(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _2C_d8
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2C_d8(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _2C_d9
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2C_d9(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _2EB_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2EB_d1(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _2Q_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _2Q_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3_d1(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _3_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3_d2(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _3_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3_d3(...)                                                                                                                                 \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d3(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d4(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d5(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d6(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d7(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d8
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d8(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d9
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d9(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d10
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d10(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d11
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d11(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d12
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d12(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d13
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d13(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d14
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d14(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d15
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d15(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d16
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d16(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d17
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d17(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d18
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d18(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d19
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d19(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d20
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d20(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d21
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d21(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d22
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d22(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d23
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d23(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d24
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d24(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d25
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d25(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d26
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d26(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d27
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d27(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3A_d28
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3A_d28(...)                                                                                                                               \
  (= __VA_ARGS__)
#endif
#ifdef _3B_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3B_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3B_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3B_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3B_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3B_d3(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _3B_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _3B_d4(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _4A_d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _4A_d1(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _4A_d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _4A_d2(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _4A_d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _4A_d3(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _4A_d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _4A_d4(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _4A_d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _4A_d5(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _4A_d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _4A_d6(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _4A_d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _4A_d7(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _4A_d8
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _4A_d8(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _4A_d9
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _4A_d9(...)                                                                                                                                \
  (= __VA_ARGS__)
#endif
#ifdef _d1
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d1(...)                                                                                                                                   \
  (= __VA_ARGS__)
#endif
#ifdef _d2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d2(...)                                                                                                                                   \
  (= __VA_ARGS__)
#endif
#ifdef _d3
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d3(...)                                                                                                                                   \
  (= __VA_ARGS__)
#endif
#ifdef _d4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d4(...)                                                                                                                                   \
  (= __VA_ARGS__)
#endif
#ifdef _d5
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d5(...)                                                                                                                                   \
  (= __VA_ARGS__)
#endif
#ifdef _d6
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d6(...)                                                                                                                                   \
  (= __VA_ARGS__)
#endif
#ifdef _d7
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d7(...)                                                                                                                                   \
  (= __VA_ARGS__)
#endif
#ifdef _d8
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d8(...)                                                                                                                                   \
  (= __VA_ARGS__)
#endif
#ifdef _d9
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d9(...)                                                                                                                                   \
  (= __VA_ARGS__)
#endif
#ifdef _d10
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d10(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d11
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d11(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d12
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d12(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d13
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d13(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d14
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d14(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d15
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d15(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d16
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d16(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d17
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d17(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d18
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d18(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d19
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d19(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d20
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d20(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d21
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d21(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d22
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d22(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d23
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d23(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d24
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d24(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d25
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d25(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d26
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d26(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d27
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d27(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif
#ifdef _d28
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _d28(...)                                                                                                                                  \
  (= __VA_ARGS__)
#endif

#if (false                                                                                                                                         \
     || defined(base_1) || defined(decl_1) || defined(args_1) || defined (prms_1)                                                                  \
     || defined(args_1_1) || defined (prms_1_1)                                                                                                    \
     || defined(base_1A) || defined(decl_1A) || defined(args_1A) || defined (prms_1A)                                                              \
     || defined(base_1Aa) || defined(decl_1Aa) || defined(args_1Aa) || defined (prms_1Aa)                                                          \
     || defined(base_1Ab) || defined(decl_1Ab) || defined(args_1Ab) || defined (prms_1Ab)                                                          \
     || defined(base_1Ac) || defined(decl_1Ac) || defined(args_1Ac) || defined (prms_1Ac)                                                          \
     || defined(base_1Ae) || defined(decl_1Ae) || defined(args_1Ae) || defined (prms_1Ae)                                                          \
     || defined(base_1Ag) || defined(decl_1Ag) || defined(args_1Ag) || defined (prms_1Ag)                                                          \
     || defined(base_1Ah) || defined(decl_1Ah) || defined(args_1Ah) || defined (prms_1Ah)                                                          \
     || defined(base_1Ai) || defined(decl_1Ai) || defined(args_1Ai) || defined (prms_1Ai)                                                          \
     || defined(base_1Aj) || defined(decl_1Aj) || defined(args_1Aj) || defined (prms_1Aj)                                                          \
     || defined(base_1Ak) || defined(decl_1Ak) || defined(args_1Ak) || defined (prms_1Ak)                                                          \
     || defined(base_1Am) || defined(decl_1Am) || defined(args_1Am) || defined (prms_1Am)                                                          \
     || defined(base_1B) || defined(decl_1B) || defined(args_1B) || defined (prms_1B)                                                              \
     || defined(args_1B_1) || defined (prms_1B_1)                                                                                                  \
     || defined(base_1C) || defined(decl_1C) || defined(args_1C) || defined (prms_1C)                                                              \
     || defined(base_1D) || defined(decl_1D) || defined(args_1D) || defined (prms_1D)                                                              \
     || defined(base_1E) || defined(decl_1E) || defined(args_1E) || defined (prms_1E)                                                              \
     || defined(base_1F) || defined(decl_1F) || defined(args_1F) || defined (prms_1F)                                                              \
     || defined(base_1G) || defined(decl_1G) || defined(args_1G) || defined (prms_1G)                                                              \
     || defined(args_1G_1) || defined (prms_1G_1)                                                                                                  \
     || defined(base_1H) || defined(decl_1H) || defined(args_1H) || defined (prms_1H)                                                              \
     || defined(base_1I) || defined(decl_1I) || defined(args_1I) || defined (prms_1I)                                                              \
     || defined(base_1J) || defined(decl_1J) || defined(args_1J) || defined (prms_1J)                                                              \
     || defined(base_1K) || defined(decl_1K) || defined(args_1K) || defined (prms_1K)                                                              \
     || defined(base_1L) || defined(decl_1L) || defined(args_1L) || defined (prms_1L)                                                              \
     || defined(base_1M) || defined(decl_1M) || defined(args_1M) || defined (prms_1M)                                                              \
     || defined(base_1N) || defined(decl_1N) || defined(args_1N) || defined (prms_1N)                                                              \
     || defined(base_1O) || defined(decl_1O) || defined(args_1O) || defined (prms_1O)                                                              \
     || defined(base_1P) || defined(decl_1P) || defined(args_1P) || defined (prms_1P)                                                              \
     || defined(base_1Q) || defined(decl_1Q) || defined(args_1Q) || defined (prms_1Q)                                                              \
     || defined(base_1R) || defined(decl_1R) || defined(args_1R) || defined (prms_1R)                                                              \
     || defined(base_1S) || defined(decl_1S) || defined(args_1S) || defined (prms_1S)                                                              \
     || defined(base_1T) || defined(decl_1T) || defined(args_1T) || defined (prms_1T)                                                              \
     || defined(base_1U) || defined(decl_1U) || defined(args_1U) || defined (prms_1U)                                                              \
     || defined(base_1V) || defined(decl_1V) || defined(args_1V) || defined (prms_1V)                                                              \
     || defined(base_1W) || defined(decl_1W) || defined(args_1W) || defined (prms_1W)                                                              \
     || defined(base_1X) || defined(decl_1X) || defined(args_1X) || defined (prms_1X)                                                              \
     || defined(base_1Y) || defined(decl_1Y) || defined(args_1Y) || defined (prms_1Y)                                                              \
     || defined(base_1Z) || defined(decl_1Z) || defined(args_1Z) || defined (prms_1Z)                                                              \
     || defined(base_2) || defined(decl_2) || defined(args_2) || defined (prms_2)                                                                  \
     || defined(base_2A) || defined(decl_2A) || defined(args_2A) || defined (prms_2A)                                                              \
     || defined(args_2A_1) || defined (prms_2A_1)                                                                                                  \
     || defined(base_2AA) || defined(decl_2AA) || defined(args_2AA) || defined (prms_2AA)                                                          \
     || defined(args_2AA_1) || defined (prms_2AA_1)                                                                                                \
     || defined(base_2AB) || defined(decl_2AB) || defined(args_2AB) || defined (prms_2AB)                                                          \
     || defined(base_2Aa) || defined(decl_2Aa) || defined(args_2Aa) || defined (prms_2Aa)                                                          \
     || defined(base_2Ab) || defined(decl_2Ab) || defined(args_2Ab) || defined (prms_2Ab)                                                          \
     || defined(base_2Ac) || defined(decl_2Ac) || defined(args_2Ac) || defined (prms_2Ac)                                                          \
     || defined(base_2Ae) || defined(decl_2Ae) || defined(args_2Ae) || defined (prms_2Ae)                                                          \
     || defined(base_2Ag) || defined(decl_2Ag) || defined(args_2Ag) || defined (prms_2Ag)                                                          \
     || defined(base_2AiA) || defined(decl_2AiA) || defined(args_2AiA) || defined (prms_2AiA)                                                      \
     || defined(base_2AiB) || defined(decl_2AiB) || defined(args_2AiB) || defined (prms_2AiB)                                                      \
     || defined(base_2Ak) || defined(decl_2Ak) || defined(args_2Ak) || defined (prms_2Ak)                                                          \
     || defined(base_2B) || defined(decl_2B) || defined(args_2B) || defined (prms_2B)                                                              \
     || defined(args_2B_1) || defined (prms_2B_1)                                                                                                  \
     || defined(base_2BA) || defined(decl_2BA) || defined(args_2BA) || defined (prms_2BA)                                                          \
     || defined(base_2BB) || defined(decl_2BB) || defined(args_2BB) || defined (prms_2BB)                                                          \
     || defined(base_2BC) || defined(decl_2BC) || defined(args_2BC) || defined (prms_2BC)                                                          \
     || defined(base_2BD) || defined(decl_2BD) || defined(args_2BD) || defined (prms_2BD)                                                          \
     || defined(base_2C) || defined(decl_2C) || defined(args_2C) || defined (prms_2C)                                                              \
     || defined(base_2CA) || defined(decl_2CA) || defined(args_2CA) || defined (prms_2CA)                                                          \
     || defined(base_2CB) || defined(decl_2CB) || defined(args_2CB) || defined (prms_2CB)                                                          \
     || defined(base_2CC) || defined(decl_2CC) || defined(args_2CC) || defined (prms_2CC)                                                          \
     || defined(base_2D) || defined(decl_2D) || defined(args_2D) || defined (prms_2D)                                                              \
     || defined(base_2E) || defined(decl_2E) || defined(args_2E) || defined (prms_2E)                                                              \
     || defined(base_2EA) || defined(decl_2EA) || defined(args_2EA) || defined (prms_2EA)                                                          \
     || defined(args_2EA_1) || defined (prms_2EA_1)                                                                                                \
     || defined(base_2EB) || defined(decl_2EB) || defined(args_2EB) || defined (prms_2EB)                                                          \
     || defined(base_2EC) || defined(decl_2EC) || defined(args_2EC) || defined (prms_2EC)                                                          \
     || defined(base_2F) || defined(decl_2F) || defined(args_2F) || defined (prms_2F)                                                              \
     || defined(base_2G) || defined(decl_2G) || defined(args_2G) || defined (prms_2G)                                                              \
     || defined(base_2GA) || defined(decl_2GA) || defined(args_2GA) || defined (prms_2GA)                                                          \
     || defined(base_2GB) || defined(decl_2GB) || defined(args_2GB) || defined (prms_2GB)                                                          \
     || defined(base_2H) || defined(decl_2H) || defined(args_2H) || defined (prms_2H)                                                              \
     || defined(base_2I) || defined(decl_2I) || defined(args_2I) || defined (prms_2I)                                                              \
     || defined(base_2J) || defined(decl_2J) || defined(args_2J) || defined (prms_2J)                                                              \
     || defined(base_2K) || defined(decl_2K) || defined(args_2K) || defined (prms_2K)                                                              \
     || defined(base_2L) || defined(decl_2L) || defined(args_2L) || defined (prms_2L)                                                              \
     || defined(base_2N) || defined(decl_2N) || defined(args_2N) || defined (prms_2N)                                                              \
     || defined(base_2P) || defined(decl_2P) || defined(args_2P) || defined (prms_2P)                                                              \
     || defined(base_2Q) || defined(decl_2Q) || defined(args_2Q) || defined (prms_2Q)                                                              \
     || defined(base_2R) || defined(decl_2R) || defined(args_2R) || defined (prms_2R)                                                              \
     || defined(base_2S) || defined(decl_2S) || defined(args_2S) || defined (prms_2S)                                                              \
     || defined(base_2TA) || defined(decl_2TA) || defined(args_2TA) || defined (prms_2TA)                                                          \
     || defined(base_2TB) || defined(decl_2TB) || defined(args_2TB) || defined (prms_2TB)                                                          \
     || defined(base_2U) || defined(decl_2U) || defined(args_2U) || defined (prms_2U)                                                              \
     || defined(base_2W) || defined(decl_2W) || defined(args_2W) || defined (prms_2W)                                                              \
     || defined(base_2YA) || defined(decl_2YA) || defined(args_2YA) || defined (prms_2YA)                                                          \
     || defined(base_2YB) || defined(decl_2YB) || defined(args_2YB) || defined (prms_2YB)                                                          \
     || defined(base_2Z) || defined(decl_2Z) || defined(args_2Z) || defined (prms_2Z)                                                              \
     || defined(base_3) || defined(decl_3) || defined(args_3) || defined (prms_3)                                                                  \
     || defined(args_3_1) || defined (prms_3_1)                                                                                                    \
     || defined(base_3A) || defined(decl_3A) || defined(args_3A) || defined (prms_3A)                                                              \
     || defined(base_3AA) || defined(decl_3AA) || defined(args_3AA) || defined (prms_3AA)                                                          \
     || defined(base_3AAA) || defined(decl_3AAA) || defined(args_3AAA) || defined (prms_3AAA)                                                      \
     || defined(base_3AAB) || defined(decl_3AAB) || defined(args_3AAB) || defined (prms_3AAB)                                                      \
     || defined(base_3AAC) || defined(decl_3AAC) || defined(args_3AAC) || defined (prms_3AAC)                                                      \
     || defined(base_3AB) || defined(decl_3AB) || defined(args_3AB) || defined (prms_3AB)                                                          \
     || defined(base_3AC) || defined(decl_3AC) || defined(args_3AC) || defined (prms_3AC)                                                          \
     || defined(base_3Ab) || defined(decl_3Ab) || defined(args_3Ab) || defined (prms_3Ab)                                                          \
     || defined(base_3B) || defined(decl_3B) || defined(args_3B) || defined (prms_3B)                                                              \
     || defined(args_3B_1) || defined (prms_3B_1)                                                                                                  \
     || defined(base_3BA) || defined(decl_3BA) || defined(args_3BA) || defined (prms_3BA)                                                          \
     || defined(base_3BB) || defined(decl_3BB) || defined(args_3BB) || defined (prms_3BB)                                                          \
     || defined(base_3BBA) || defined(decl_3BBA) || defined(args_3BBA) || defined (prms_3BBA)                                                      \
     || defined(base_3BBB) || defined(decl_3BBB) || defined(args_3BBB) || defined (prms_3BBB)                                                      \
     || defined(base_3C) || defined(decl_3C) || defined(args_3C) || defined (prms_3C)                                                              \
     || defined(base_3CA) || defined(decl_3CA) || defined(args_3CA) || defined (prms_3CA)                                                          \
     || defined(base_3EB) || defined(decl_3EB) || defined(args_3EB) || defined (prms_3EB)                                                          \
     || defined(base_3EC) || defined(decl_3EC) || defined(args_3EC) || defined (prms_3EC)                                                          \
     || defined(args_3EC_1) || defined (prms_3EC_1)                                                                                                \
     || defined(base_3GA) || defined(decl_3GA) || defined(args_3GA) || defined (prms_3GA)                                                          \
     || defined(base_3H) || defined(decl_3H) || defined(args_3H) || defined (prms_3H)                                                              \
     || defined(base_3IA) || defined(decl_3IA) || defined(args_3IA) || defined (prms_3IA)                                                          \
     || defined(base_3IB) || defined(decl_3IB) || defined(args_3IB) || defined (prms_3IB)                                                          \
     || defined(base_3IC) || defined(decl_3IC) || defined(args_3IC) || defined (prms_3IC)                                                          \
     || defined(base_3NA) || defined(decl_3NA) || defined(args_3NA) || defined (prms_3NA)                                                          \
     || defined(base_3NB) || defined(decl_3NB) || defined(args_3NB) || defined (prms_3NB)                                                          \
     || defined(base_3PA) || defined(decl_3PA) || defined(args_3PA) || defined (prms_3PA)                                                          \
     || defined(base_3Q) || defined(decl_3Q) || defined(args_3Q) || defined (prms_3Q)                                                              \
     || defined(base_3RA) || defined(decl_3RA) || defined(args_3RA) || defined (prms_3RA)                                                          \
     || defined(args_3RA_1) || defined (prms_3RA_1)                                                                                                \
     || defined(args_3RA_2A) || defined (prms_3RA_2A)                                                                                              \
     || defined(args_3RA_2B) || defined (prms_3RA_2B)                                                                                              \
     || defined(base_3RB) || defined(decl_3RB) || defined(args_3RB) || defined (prms_3RB)                                                          \
     || defined(base_3T) || defined(decl_3T) || defined(args_3T) || defined (prms_3T)                                                              \
     || defined(base_3U) || defined(decl_3U) || defined(args_3U) || defined (prms_3U)                                                              \
     || defined(base_3WA) || defined(decl_3WA) || defined(args_3WA) || defined (prms_3WA)                                                          \
     || defined(base_3WB) || defined(decl_3WB) || defined(args_3WB) || defined (prms_3WB)                                                          \
     || defined(base_4) || defined(decl_4) || defined(args_4) || defined (prms_4)                                                                  \
     || defined(base_4A) || defined(decl_4A) || defined(args_4A) || defined (prms_4A)                                                              \
     || defined(base_4ACA) || defined(decl_4ACA) || defined(args_4ACA) || defined (prms_4ACA)                                                      \
     || defined(base_4ACB) || defined(decl_4ACB) || defined(args_4ACB) || defined (prms_4ACB)                                                      \
     || defined(base_4Ab) || defined(decl_4Ab) || defined(args_4Ab) || defined (prms_4Ab)                                                          \
     || defined(args_4Ab_1) || defined (prms_4Ab_1)                                                                                                \
     || defined(args_4Ab_2A) || defined (prms_4Ab_2A)                                                                                              \
     || defined(args_4Ab_2B) || defined (prms_4Ab_2B)                                                                                              \
     || defined(base_4B) || defined(decl_4B) || defined(args_4B) || defined (prms_4B)                                                              \
     || defined(base_4BA) || defined(decl_4BA) || defined(args_4BA) || defined (prms_4BA)                                                          \
     || defined(args_4BA_1) || defined (prms_4BA_1)                                                                                                \
     || defined(base_4BB) || defined(decl_4BB) || defined(args_4BB) || defined (prms_4BB)                                                          \
     || defined(base_4BC) || defined(decl_4BC) || defined(args_4BC) || defined (prms_4BC)                                                          \
     || defined(args_4BC_1) || defined (prms_4BC_1)                                                                                                \
     || defined(base_4CA) || defined(decl_4CA) || defined(args_4CA) || defined (prms_4CA)                                                          \
     || defined(base_4CB) || defined(decl_4CB) || defined(args_4CB) || defined (prms_4CB)                                                          \
     || defined(base_4NA) || defined(decl_4NA) || defined(args_4NA) || defined (prms_4NA)                                                          \
     || defined(base_4NB) || defined(decl_4NB) || defined(args_4NB) || defined (prms_4NB)                                                          \
     || defined(base_4Q) || defined(decl_4Q) || defined(args_4Q) || defined (prms_4Q)                                                              \
     || defined(base_4UA) || defined(decl_4UA) || defined(args_4UA) || defined (prms_4UA)                                                          \
     || defined(args_4UA_1) || defined (prms_4UA_1)                                                                                                \
     || defined(base_4UB) || defined(decl_4UB) || defined(args_4UB) || defined (prms_4UB)                                                          \
     || defined(args_4UB_1) || defined (prms_4UB_1)                                                                                                \
     || defined(args_4UB_2A) || defined (prms_4UB_2A)                                                                                              \
     || defined(args_4UB_2B) || defined (prms_4UB_2B)                                                                                              \
     || defined(base_5A) || defined(decl_5A) || defined(args_5A) || defined (prms_5A)                                                              \
     || defined(args_5A_1) || defined (prms_5A_1)                                                                                                  \
     || defined(args_5A_2) || defined (prms_5A_2)                                                                                                  \
     || defined(base_5ACB) || defined(decl_5ACB) || defined(args_5ACB) || defined (prms_5ACB)                                                      \
     || defined(base_5B) || defined(decl_5B) || defined(args_5B) || defined (prms_5B)                                                              \
     || defined(base_5BA) || defined(decl_5BA) || defined(args_5BA) || defined (prms_5BA)                                                          \
     || defined(base_5BB) || defined(decl_5BB) || defined(args_5BB) || defined (prms_5BB)                                                          \
     || defined(base_5NAA) || defined(decl_5NAA) || defined(args_5NAA) || defined (prms_5NAA)                                                      \
     || defined(args_5NAA_1) || defined (prms_5NAA_1)                                                                                              \
     || defined(args_5NAA_2A) || defined (prms_5NAA_2A)                                                                                            \
     || defined(args_5NAA_2B) || defined (prms_5NAA_2B)                                                                                            \
     || defined(base_5NAB) || defined(decl_5NAB) || defined(args_5NAB) || defined (prms_5NAB)                                                      \
     || defined(base_5QA) || defined(decl_5QA) || defined(args_5QA) || defined (prms_5QA)                                                          \
     || defined(args_5QA_1) || defined (prms_5QA_1)                                                                                                \
     || defined(args_5QA_2A) || defined (prms_5QA_2A)                                                                                              \
     || defined(args_5QA_2B) || defined (prms_5QA_2B)                                                                                              \
     || defined(args_5QA_2C) || defined (prms_5QA_2C)                                                                                              \
     || defined(args_5QA_3C) || defined (prms_5QA_3C)                                                                                              \
     || defined(base_5QB) || defined(decl_5QB) || defined(args_5QB) || defined (prms_5QB)                                                          \
     || defined(base_6BAA) || defined(decl_6BAA) || defined(args_6BAA) || defined (prms_6BAA)                                                      \
     || defined(base_6BAB) || defined(decl_6BAB) || defined(args_6BAB) || defined (prms_6BAB)                                                      \
     || defined(base_7BAAA) || defined(decl_7BAAA) || defined(args_7BAAA) || defined (prms_7BAAA)                                                  \
     || defined(base_7BAAB) || defined(decl_7BAAB) || defined(args_7BAAB) || defined (prms_7BAAB)                                                  \
     || defined(args_7BAAB_1) || defined (prms_7BAAB_1)                                                                                            \
     || defined(base_7BABA) || defined(decl_7BABA) || defined(args_7BABA) || defined (prms_7BABA)                                                  \
     || defined(base_7BABB) || defined(decl_7BABB) || defined(args_7BABB) || defined (prms_7BABB)                                                  \
     || defined(base_8BABA) || defined(decl_8BABA) || defined(args_8BABA) || defined (prms_8BABA)                                                  \
     )
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#endif
//\end{frequently maintained part}

#ifdef evaluate
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define evaluate(...)                           \
  __VA_ARGS__
#endif

#ifdef wrap
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define wrap(...)                               \

#endif
#ifdef unwrap_
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define unwrap_(...)                            \
  __VA_ARGS__
#endif
#ifdef unwrap
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define unwrap(...)                             \
  __VA_ARGS__
#endif
#ifdef unwrap_2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define unwrap_2(...)                           \
  unwrap unwrap((__VA_ARGS__))
#endif
#ifdef unwrap_2_unwrap_2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define unwrap_2_unwrap_2(...)                  \
  unwrap unwrap unwrap_2(((__VA_ARGS__)))
#endif
#ifdef unwrap_4
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define unwrap_4(...)                                   \
  unwrap unwrap unwrap unwrap((((__VA_ARGS__))))
#endif
#ifdef unwrap_4_unwrap_2
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define unwrap_4_unwrap_2(...)                  \
  unwrap unwrap unwrap_4(((__VA_ARGS__)))
#endif

#ifdef comma
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define comma                                   \
  unwrap(,)
#endif

#ifdef select
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define select(...)                             \
  selected(__VA_ARGS__,)
#endif
#ifdef selected
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define selected(arg__selected, ...)            \
  arg__selected
#endif

#ifdef opts
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define opts(arg__to_be_selected, arg__name, arg__default)                                              \
  evaluate(unwrap select(select(arg__to_be_selected (, unwrap_)) (arg__name) unwrap_2 arg__default))
#endif
#ifdef type
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define type(arg__to_be_selected, /* arg__type */ ...)                                  \
  arg__to_be_selected ## _ ## unwrap_2(, __VA_ARGS__ unwrap_) unwrap unwrap((,))
#endif
#ifdef typr
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define typr(arg__to_be_selected, /* arg__type */ ...)                          \
  arg__to_be_selected ## _ ## unwrap_2(, __VA_ARGS__ wrap) unwrap unwrap((,))
#endif
#ifdef II
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define II                                      \
  typename I
#endif
#ifdef Ii
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define Ii                                      \
  I
#endif
#ifdef I_I_
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define I_I_                                    \
  typename I_
#endif

#ifdef _
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define _                                       \
  () unwrap_4
#endif
#ifdef X
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define X                                       \
  () unwrap_2
#endif
#ifdef R
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define R(/* arg__replacement */ ...)           \
  (, __VA_ARGS__) unwrap_2
#endif
#ifdef RR
#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation
#else
#define RR(/* arg__replacement */ ...)           \
  (__VA_ARGS__) unwrap_2
#endif
