/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace error {
        namespace tuple {
          namespace size {

            template<bool arg__is_tuple_construct, typename arg__actual_parameter>
            struct arg_2_is_tuple_construct {
              static_assert(arg__is_tuple_construct,
                            "[DEBUG] Arg 2 is not class template tice::v1::internals::tuple::construct");
            };

          }
          namespace get_pos {

            template<bool arg__is_positive, v1::tuple::Element_pos arg__element_pos>
            struct arg_2_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Arg 2 is not positive");
            };

            template<bool arg__is_tuple_construct, typename arg__actual_parameter>
            struct arg_3_is_tuple_construct {
              static_assert(arg__is_tuple_construct,
                            "[DEBUG] Arg 3 is not class template tice::v1::internals::tuple::construct");
            };

            template<bool arg__is_less_than_or_equal_to_arg_3_size, v1::tuple::Element_pos arg__element_pos, v1::tuple::Size arg__tuple_size>
            struct arg_2_is_less_than_or_equal_to_arg_3_size {
              static_assert(arg__is_less_than_or_equal_to_arg_3_size,
                            "[DEBUG] Arg 2 is not less than or equal to Arg 3's size");
            };

          }
        }
      }
      namespace tuple {

        template<typename I, typename... args__element>
        struct construct {
        };

      }
      namespace tuple {

#include "v1_internals_begin.hpp"

#define base_1(_d2, I, _1, _2) I                \
        opts  (_1, arg__tuple_construct, ())    \
          opts(_2, arg__tuple_checker_msg, _d2)
#define decl_1(_d2, I, _1, _2)                                                                                  \
        base_1(_d2, I##I,                                                                                       \
               type(_1, typename),                                                                              \
               type(_2, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class))
#define args_1(I, _1, _2) base_1((), I, _1, _2)
#define prms_1(...) decl_1((), __VA_ARGS__)

        template<decl_1(_d2(error::tuple::size::arg_2_is_tuple_construct),
                        I, _, _)>
        struct size;

        

        template<prms_1(I, _, _)>
        struct size :
          arg__tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct> {
        };

        template<prms_1(I, R(typename... args__element), _)>
        struct size<args_1(I, R(Tuple<args__element...>), _)> :
          size<args_1(I, R(tuple::construct<I, args__element...>), _)> {
        };

        template<prms_1(I, R(typename... args__element), _)>
        struct size<args_1(I, R(tuple::construct<I, args__element...>), _)>
        {
          static constexpr v1::tuple::Size value {sizeof...(args__element)};
        };

#include "v1_internals_end.hpp"
      }
      namespace tuple {

#include "v1_internals_begin.hpp"

#define base_1(I, _1, _2) I                     \
        opts  (_1, arg__pos, ())                \
          opts(_2, arg__tuple_construct, ())
#define decl_1(I, _1, _2)                               \
        base_1(I##I,                                    \
               type(_1, v1::tuple::Element_pos),        \
               type(_2, typename))
#define args_1(I, _1, _2) base_1(I, _1, _2)
#define prms_1(...) decl_1(__VA_ARGS__)

#define base_2A(_d1, args_1, _1)                \
        args_1                                  \
        opts(_1, arg__is_done, _d1)
#define decl_2A(_d1, I, _1_1, _1_2, _1)         \
        base_2A(_d1,                            \
                prms_1(I, _1_1, _1_2),          \
                type(_1, bool))
#define args_2A(I, _1_1, _1_2, _1) base_2A((), args_1(I, _1_1, _1_2), _1)
#define prms_2A(...) decl_2A((), __VA_ARGS__)

        template<decl_2A(_d1(arg__pos == 1),
                         I, _, _, _)>
        struct get_pos__run;

#define base_2B(_d1, _d2, _d3, args_1, _1, _2, _3)      \
        args_1                                          \
        opts  (_1, arg__min_pos_checker_msg, _d1)       \
          opts(_2, arg__tuple_checker_msg, _d2)         \
          opts(_3, arg__max_pos_checker_msg, _d3)
#define decl_2B(_d1, _d2, _d3, I, _1_1, _1_2, _1, _2, _3)                                                               \
        base_2B(_d1, _d2, _d3,                                                                                          \
                prms_1(I, _1_1, _1_2),                                                                                  \
                type(_1, template<bool arg__is_positive_, v1::tuple::Element_pos arg__element_pos_> class),             \
                type(_2, template<bool arg__is_tuple_construct_, typename arg__actual_parameter_> class),               \
                type(_3, template<bool arg__is_less_than_or_equal_to_arg_3_size_,                                       \
                     /**/         v1::tuple::Element_pos arg__element_pos_, v1::tuple::Size arg__tuple_size_> class))
#define args_2B(I, _1_1, _1_2, _1, _2, _3) base_2B((), (), (), args_1(I, _1_1, _1_2), _1, _2, _3)
#define prms_2B(...) decl_2B((), (), (), __VA_ARGS__)

        template<decl_2B(_d1(error::tuple::get_pos::arg_2_is_positive),
                         _d2(error::tuple::get_pos::arg_3_is_tuple_construct),
                         _d3(error::tuple::get_pos::arg_2_is_less_than_or_equal_to_arg_3_size),
                         I, _, _, _, _, _)>
        struct get_pos;

        

        template<prms_2B(I, _, _, _, _, _)>
        struct get_pos :
          arg__tuple_checker_msg<!!utility::always_false<I>(), arg__tuple_construct> {
        };

        template<prms_2B(I, _, R(typename... args__element), _, _, _)>
        struct get_pos<args_2B(I, _, R(Tuple<args__element...>), _, _, _)> :
          get_pos<args_2B(I, _, R(tuple::construct<I, args__element...>), _, _, _)> {
        };

        template<prms_2B(I, _, R(typename... args__element), _, _, _)>
        struct get_pos<args_2B(I, _, R(tuple::construct<I, args__element...>), _, _, _)> :
          arg__min_pos_checker_msg<(arg__pos > 0), arg__pos>,
          arg__max_pos_checker_msg<(arg__pos <= sizeof...(args__element)), arg__pos, sizeof...(args__element)>,
          get_pos__run<args_2A(I, _, R(tuple::construct<I, args__element...>), X)> {
        };

        

        template<prms_2A(I, _, R(typename arg__element, typename... args__element), X)>
        struct get_pos__run<args_2A(I, _, R(tuple::construct<I, arg__element, args__element...>), R(true))>
        {
          typedef arg__element value;
        };

        template<prms_2A(I, _, R(typename arg__element, typename... args__element), X)>
        struct get_pos__run<args_2A(I, _, R(tuple::construct<I, arg__element, args__element...>), R(false))> :
          get_pos__run<args_2A(I, R(arg__pos - 1), R(tuple::construct<I, args__element...>), X)> {
        };

#include "v1_internals_end.hpp"
      }
    }
  }
}

namespace tice {
  namespace v1 {

    template<typename... args__element>
    class Tuple :
      public internals::tuple::construct<I, args__element...> {
    };

    namespace tuple {

      template<typename arg__Tuple>
      class size :
        public internals::tuple::size<I, arg__Tuple,
                                      error::tuple::size::arg_1_is_Tuple> {
      };

      template<Element_pos arg__pos, typename arg__Tuple>
      class get_pos :
        public internals::tuple::get_pos<I, arg__pos, arg__Tuple,
                                         error::tuple::get_pos::arg_1_is_positive,
                                         error::tuple::get_pos::arg_2_is_Tuple,
                                         error::tuple::get_pos::arg_1_is_less_than_or_equal_to_arg_2_size> {
      };

    }
  }
}
