/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

//\begin{frequently maintained part}
#undef _1_d1
#undef _1_d2
#undef _1_d3
#undef _1_d4
#undef _1_d5
#undef _1_d6
#undef _1_d7
#undef _1_d8
#undef _1_d9
#undef _1_d10
#undef _1_d11
#undef _1_d12
#undef _1_d13
#undef _1_d14
#undef _1A_d1
#undef _1A_d2
#undef _1A_d3
#undef _1A_d4
#undef _1A_d5
#undef _1A_d6
#undef _1A_d7
#undef _1A_d8
#undef _1A_d9
#undef _1A_d10
#undef _1A_d11
#undef _1Ab_d1
#undef _1Ac_d1
#undef _1Ag_d1
#undef _1Ai_d1
#undef _1Aj_d1
#undef _1Aj_d2
#undef _1Ak_d1
#undef _1Am_d1
#undef _1B_d1
#undef _1B_d2
#undef _1B_d3
#undef _1B_d4
#undef _1B_d5
#undef _1B_d6
#undef _1B_d7
#undef _1C_d1
#undef _1C_d2
#undef _1C_d3
#undef _1C_d4
#undef _1C_d5
#undef _1C_d6
#undef _1C_d7
#undef _1C_d8
#undef _1D_d1
#undef _1D_d2
#undef _1D_d3
#undef _1E_d1
#undef _1F_d1
#undef _1F_d2
#undef _1G_d1
#undef _1G_d2
#undef _1G_d3
#undef _1G_d4
#undef _1G_d5
#undef _1G_d6
#undef _1G_d7
#undef _1G_d8
#undef _1J_d1
#undef _1J_d2
#undef _1J_d3
#undef _1J_d4
#undef _1J_d5
#undef _1J_d6
#undef _1J_d7
#undef _1J_d8
#undef _1J_d9
#undef _1J_d10
#undef _1J_d11
#undef _1J_d12
#undef _1J_d13
#undef _1J_d14
#undef _1J_d15
#undef _1J_d16
#undef _1J_d17
#undef _1J_d18
#undef _1J_d19
#undef _1J_d20
#undef _1J_d21
#undef _1J_d22
#undef _1J_d23
#undef _1K_d1
#undef _1K_d2
#undef _1N_d1
#undef _1P_d1
#undef _1Q_d1
#undef _1R_d1
#undef _1X_d1
#undef _1Y_d1
#undef _1Y_d2
#undef _1Y_d3
#undef _1Y_d4
#undef _1Y_d5
#undef _1Y_d6
#undef _1Y_d7
#undef _1Y_d8
#undef _1Y_d9
#undef _1Y_d10
#undef _1Y_d11
#undef _1Y_d12
#undef _1Y_d13
#undef _1Y_d14
#undef _1Y_d15
#undef _1Y_d16
#undef _1Y_d17
#undef _1Y_d18
#undef _1Y_d19
#undef _1Y_d20
#undef _1Y_d21
#undef _1Y_d22
#undef _1Y_d23
#undef _1Y_d24
#undef _1Y_d25
#undef _1Y_d26
#undef _1Y_d27
#undef _1Z_d1
#undef _1Z_d2
#undef _1Z_d3
#undef _1Z_d4
#undef _1Z_d5
#undef _1Z_d6
#undef _1Z_d7
#undef _1Z_d8
#undef _1Z_d9
#undef _1Z_d10
#undef _1Z_d11
#undef _1Z_d12
#undef _1Z_d13
#undef _1Z_d14
#undef _1Z_d15
#undef _1Z_d16
#undef _1Z_d17
#undef _1Z_d18
#undef _1Z_d19
#undef _1Z_d20
#undef _1Z_d21
#undef _1Z_d22
#undef _1Z_d23
#undef _1Z_d24
#undef _2_d2
#undef _2_d3
#undef _2_d4
#undef _2_d5
#undef _2_d6
#undef _2_d7
#undef _2A_d1
#undef _2A_d2
#undef _2AA_d1
#undef _2AA_d2
#undef _2AA_d3
#undef _2AA_d4
#undef _2AA_d5
#undef _2AA_d6
#undef _2AA_d7
#undef _2Aa_d1
#undef _2Ab_d1
#undef _2Ab_d2
#undef _2Ab_d3
#undef _2Ab_d4
#undef _2AiB_d1
#undef _2B_d1
#undef _2BA_d1
#undef _2BB_d1
#undef _2BB_d2
#undef _2C_d1
#undef _2C_d2
#undef _2C_d3
#undef _2C_d4
#undef _2C_d5
#undef _2C_d6
#undef _2C_d7
#undef _2C_d8
#undef _2C_d9
#undef _2EB_d1
#undef _2Q_d1
#undef _3_d1
#undef _3_d2
#undef _3_d3
#undef _3A_d1
#undef _3A_d2
#undef _3A_d3
#undef _3A_d4
#undef _3A_d5
#undef _3A_d6
#undef _3A_d7
#undef _3A_d8
#undef _3A_d9
#undef _3A_d10
#undef _3A_d11
#undef _3A_d12
#undef _3A_d13
#undef _3A_d14
#undef _3A_d15
#undef _3A_d16
#undef _3A_d17
#undef _3A_d18
#undef _3A_d19
#undef _3A_d20
#undef _3A_d21
#undef _3A_d22
#undef _3A_d23
#undef _3A_d24
#undef _3A_d25
#undef _3A_d26
#undef _3A_d27
#undef _3A_d28
#undef _3B_d1
#undef _3B_d2
#undef _3B_d3
#undef _3B_d4
#undef _4A_d1
#undef _4A_d2
#undef _4A_d3
#undef _4A_d4
#undef _4A_d5
#undef _4A_d6
#undef _4A_d7
#undef _4A_d8
#undef _4A_d9
#undef _d1
#undef _d2
#undef _d3
#undef _d4
#undef _d5
#undef _d6
#undef _d7
#undef _d8
#undef _d9
#undef _d10
#undef _d11
#undef _d12
#undef _d13
#undef _d14
#undef _d15
#undef _d16
#undef _d17
#undef _d18
#undef _d19
#undef _d20
#undef _d21
#undef _d22
#undef _d23
#undef _d24
#undef _d25
#undef _d26
#undef _d27
#undef _d28

#undef base_1
#undef decl_1
#undef args_1
#undef prms_1

#undef args_1_1
#undef prms_1_1

#undef base_1A
#undef decl_1A
#undef args_1A
#undef prms_1A

#undef base_1Aa
#undef decl_1Aa
#undef args_1Aa
#undef prms_1Aa

#undef base_1Ab
#undef decl_1Ab
#undef args_1Ab
#undef prms_1Ab

#undef base_1Ac
#undef decl_1Ac
#undef args_1Ac
#undef prms_1Ac

#undef base_1Ae
#undef decl_1Ae
#undef args_1Ae
#undef prms_1Ae

#undef base_1Ag
#undef decl_1Ag
#undef args_1Ag
#undef prms_1Ag

#undef base_1Ah
#undef decl_1Ah
#undef args_1Ah
#undef prms_1Ah

#undef base_1Ai
#undef decl_1Ai
#undef args_1Ai
#undef prms_1Ai

#undef base_1Aj
#undef decl_1Aj
#undef args_1Aj
#undef prms_1Aj

#undef base_1Ak
#undef decl_1Ak
#undef args_1Ak
#undef prms_1Ak

#undef base_1Am
#undef decl_1Am
#undef args_1Am
#undef prms_1Am

#undef base_1B
#undef decl_1B
#undef args_1B
#undef prms_1B

#undef args_1B_1
#undef prms_1B_1

#undef base_1C
#undef decl_1C
#undef args_1C
#undef prms_1C

#undef base_1D
#undef decl_1D
#undef args_1D
#undef prms_1D

#undef base_1E
#undef decl_1E
#undef args_1E
#undef prms_1E

#undef base_1F
#undef decl_1F
#undef args_1F
#undef prms_1F

#undef base_1G
#undef decl_1G
#undef args_1G
#undef prms_1G

#undef args_1G_1
#undef prms_1G_1

#undef base_1H
#undef decl_1H
#undef args_1H
#undef prms_1H

#undef base_1I
#undef decl_1I
#undef args_1I
#undef prms_1I

#undef base_1J
#undef decl_1J
#undef args_1J
#undef prms_1J

#undef base_1K
#undef decl_1K
#undef args_1K
#undef prms_1K

#undef base_1L
#undef decl_1L
#undef args_1L
#undef prms_1L

#undef base_1M
#undef decl_1M
#undef args_1M
#undef prms_1M

#undef base_1N
#undef decl_1N
#undef args_1N
#undef prms_1N

#undef base_1O
#undef decl_1O
#undef args_1O
#undef prms_1O

#undef base_1P
#undef decl_1P
#undef args_1P
#undef prms_1P

#undef base_1Q
#undef decl_1Q
#undef args_1Q
#undef prms_1Q

#undef base_1R
#undef decl_1R
#undef args_1R
#undef prms_1R

#undef base_1S
#undef decl_1S
#undef args_1S
#undef prms_1S

#undef base_1T
#undef decl_1T
#undef args_1T
#undef prms_1T

#undef base_1U
#undef decl_1U
#undef args_1U
#undef prms_1U

#undef base_1V
#undef decl_1V
#undef args_1V
#undef prms_1V

#undef base_1W
#undef decl_1W
#undef args_1W
#undef prms_1W

#undef base_1X
#undef decl_1X
#undef args_1X
#undef prms_1X

#undef base_1Y
#undef decl_1Y
#undef args_1Y
#undef prms_1Y

#undef base_1Z
#undef decl_1Z
#undef args_1Z
#undef prms_1Z

#undef base_2
#undef decl_2
#undef args_2
#undef prms_2

#undef base_2A
#undef decl_2A
#undef args_2A
#undef prms_2A

#undef args_2A_1
#undef prms_2A_1

#undef base_2AA
#undef decl_2AA
#undef args_2AA
#undef prms_2AA

#undef args_2AA_1
#undef prms_2AA_1

#undef base_2AB
#undef decl_2AB
#undef args_2AB
#undef prms_2AB

#undef base_2Aa
#undef decl_2Aa
#undef args_2Aa
#undef prms_2Aa

#undef base_2Ab
#undef decl_2Ab
#undef args_2Ab
#undef prms_2Ab

#undef base_2Ac
#undef decl_2Ac
#undef args_2Ac
#undef prms_2Ac

#undef base_2Ae
#undef decl_2Ae
#undef args_2Ae
#undef prms_2Ae

#undef base_2Ag
#undef decl_2Ag
#undef args_2Ag
#undef prms_2Ag

#undef base_2AiA
#undef decl_2AiA
#undef args_2AiA
#undef prms_2AiA

#undef base_2AiB
#undef decl_2AiB
#undef args_2AiB
#undef prms_2AiB

#undef base_2Ak
#undef decl_2Ak
#undef args_2Ak
#undef prms_2Ak

#undef base_2B
#undef decl_2B
#undef args_2B
#undef prms_2B

#undef args_2B_1
#undef prms_2B_1

#undef base_2BA
#undef decl_2BA
#undef args_2BA
#undef prms_2BA

#undef base_2BB
#undef decl_2BB
#undef args_2BB
#undef prms_2BB

#undef base_2BC
#undef decl_2BC
#undef args_2BC
#undef prms_2BC

#undef base_2BD
#undef decl_2BD
#undef args_2BD
#undef prms_2BD

#undef base_2C
#undef decl_2C
#undef args_2C
#undef prms_2C

#undef base_2CA
#undef decl_2CA
#undef args_2CA
#undef prms_2CA

#undef base_2CB
#undef decl_2CB
#undef args_2CB
#undef prms_2CB

#undef base_2CC
#undef decl_2CC
#undef args_2CC
#undef prms_2CC

#undef base_2D
#undef decl_2D
#undef args_2D
#undef prms_2D

#undef base_2E
#undef decl_2E
#undef args_2E
#undef prms_2E

#undef base_2EA
#undef decl_2EA
#undef args_2EA
#undef prms_2EA

#undef args_2EA_1
#undef prms_2EA_1

#undef base_2EB
#undef decl_2EB
#undef args_2EB
#undef prms_2EB

#undef base_2EC
#undef decl_2EC
#undef args_2EC
#undef prms_2EC

#undef base_2F
#undef decl_2F
#undef args_2F
#undef prms_2F

#undef base_2G
#undef decl_2G
#undef args_2G
#undef prms_2G

#undef base_2GA
#undef decl_2GA
#undef args_2GA
#undef prms_2GA

#undef base_2GB
#undef decl_2GB
#undef args_2GB
#undef prms_2GB

#undef base_2H
#undef decl_2H
#undef args_2H
#undef prms_2H

#undef base_2I
#undef decl_2I
#undef args_2I
#undef prms_2I

#undef base_2J
#undef decl_2J
#undef args_2J
#undef prms_2J

#undef base_2K
#undef decl_2K
#undef args_2K
#undef prms_2K

#undef base_2L
#undef decl_2L
#undef args_2L
#undef prms_2L

#undef base_2N
#undef decl_2N
#undef args_2N
#undef prms_2N

#undef base_2P
#undef decl_2P
#undef args_2P
#undef prms_2P

#undef base_2Q
#undef decl_2Q
#undef args_2Q
#undef prms_2Q

#undef base_2R
#undef decl_2R
#undef args_2R
#undef prms_2R

#undef base_2S
#undef decl_2S
#undef args_2S
#undef prms_2S

#undef base_2TA
#undef decl_2TA
#undef args_2TA
#undef prms_2TA

#undef base_2TB
#undef decl_2TB
#undef args_2TB
#undef prms_2TB

#undef base_2U
#undef decl_2U
#undef args_2U
#undef prms_2U

#undef base_2W
#undef decl_2W
#undef args_2W
#undef prms_2W

#undef base_2YA
#undef decl_2YA
#undef args_2YA
#undef prms_2YA

#undef base_2YB
#undef decl_2YB
#undef args_2YB
#undef prms_2YB

#undef base_2Z
#undef decl_2Z
#undef args_2Z
#undef prms_2Z

#undef base_3
#undef decl_3
#undef args_3
#undef prms_3

#undef args_3_1
#undef prms_3_1

#undef base_3A
#undef decl_3A
#undef args_3A
#undef prms_3A

#undef base_3AA
#undef decl_3AA
#undef args_3AA
#undef prms_3AA

#undef base_3AAA
#undef decl_3AAA
#undef args_3AAA
#undef prms_3AAA

#undef base_3AAB
#undef decl_3AAB
#undef args_3AAB
#undef prms_3AAB

#undef base_3AAC
#undef decl_3AAC
#undef args_3AAC
#undef prms_3AAC

#undef base_3AB
#undef decl_3AB
#undef args_3AB
#undef prms_3AB

#undef base_3AC
#undef decl_3AC
#undef args_3AC
#undef prms_3AC

#undef base_3Ab
#undef decl_3Ab
#undef args_3Ab
#undef prms_3Ab

#undef base_3B
#undef decl_3B
#undef args_3B
#undef prms_3B

#undef args_3B_1
#undef prms_3B_1

#undef base_3BA
#undef decl_3BA
#undef args_3BA
#undef prms_3BA

#undef base_3BB
#undef decl_3BB
#undef args_3BB
#undef prms_3BB

#undef base_3BBA
#undef decl_3BBA
#undef args_3BBA
#undef prms_3BBA

#undef base_3BBB
#undef decl_3BBB
#undef args_3BBB
#undef prms_3BBB

#undef base_3C
#undef decl_3C
#undef args_3C
#undef prms_3C

#undef base_3CA
#undef decl_3CA
#undef args_3CA
#undef prms_3CA

#undef base_3EB
#undef decl_3EB
#undef args_3EB
#undef prms_3EB

#undef base_3EC
#undef decl_3EC
#undef args_3EC
#undef prms_3EC

#undef args_3EC_1
#undef prms_3EC_1

#undef base_3GA
#undef decl_3GA
#undef args_3GA
#undef prms_3GA

#undef base_3H
#undef decl_3H
#undef args_3H
#undef prms_3H

#undef base_3IA
#undef decl_3IA
#undef args_3IA
#undef prms_3IA

#undef base_3IB
#undef decl_3IB
#undef args_3IB
#undef prms_3IB

#undef base_3IC
#undef decl_3IC
#undef args_3IC
#undef prms_3IC

#undef base_3NA
#undef decl_3NA
#undef args_3NA
#undef prms_3NA

#undef base_3NB
#undef decl_3NB
#undef args_3NB
#undef prms_3NB

#undef base_3PA
#undef decl_3PA
#undef args_3PA
#undef prms_3PA

#undef base_3Q
#undef decl_3Q
#undef args_3Q
#undef prms_3Q

#undef base_3RA
#undef decl_3RA
#undef args_3RA
#undef prms_3RA

#undef args_3RA_1
#undef prms_3RA_1

#undef args_3RA_2A
#undef prms_3RA_2A

#undef args_3RA_2B
#undef prms_3RA_2B

#undef base_3RB
#undef decl_3RB
#undef args_3RB
#undef prms_3RB

#undef base_3T
#undef decl_3T
#undef args_3T
#undef prms_3T

#undef base_3U
#undef decl_3U
#undef args_3U
#undef prms_3U

#undef base_3WA
#undef decl_3WA
#undef args_3WA
#undef prms_3WA

#undef base_3WB
#undef decl_3WB
#undef args_3WB
#undef prms_3WB

#undef base_4
#undef decl_4
#undef args_4
#undef prms_4

#undef base_4A
#undef decl_4A
#undef args_4A
#undef prms_4A

#undef base_4ACA
#undef decl_4ACA
#undef args_4ACA
#undef prms_4ACA

#undef base_4ACB
#undef decl_4ACB
#undef args_4ACB
#undef prms_4ACB

#undef base_4Ab
#undef decl_4Ab
#undef args_4Ab
#undef prms_4Ab

#undef args_4Ab_1
#undef prms_4Ab_1

#undef args_4Ab_2A
#undef prms_4Ab_2A

#undef args_4Ab_2B
#undef prms_4Ab_2B

#undef base_4B
#undef decl_4B
#undef args_4B
#undef prms_4B

#undef base_4BA
#undef decl_4BA
#undef args_4BA
#undef prms_4BA

#undef args_4BA_1
#undef prms_4BA_1

#undef base_4BB
#undef decl_4BB
#undef args_4BB
#undef prms_4BB

#undef base_4BC
#undef decl_4BC
#undef args_4BC
#undef prms_4BC

#undef args_4BC_1
#undef prms_4BC_1

#undef base_4CA
#undef decl_4CA
#undef args_4CA
#undef prms_4CA

#undef base_4CB
#undef decl_4CB
#undef args_4CB
#undef prms_4CB

#undef base_4NA
#undef decl_4NA
#undef args_4NA
#undef prms_4NA

#undef base_4NB
#undef decl_4NB
#undef args_4NB
#undef prms_4NB

#undef base_4Q
#undef decl_4Q
#undef args_4Q
#undef prms_4Q

#undef base_4UA
#undef decl_4UA
#undef args_4UA
#undef prms_4UA

#undef args_4UA_1
#undef prms_4UA_1

#undef base_4UB
#undef decl_4UB
#undef args_4UB
#undef prms_4UB

#undef args_4UB_1
#undef prms_4UB_1

#undef args_4UB_2A
#undef prms_4UB_2A

#undef args_4UB_2B
#undef prms_4UB_2B

#undef base_5A
#undef decl_5A
#undef args_5A
#undef prms_5A

#undef args_5A_1
#undef prms_5A_1

#undef args_5A_2
#undef prms_5A_2

#undef base_5ACB
#undef decl_5ACB
#undef args_5ACB
#undef prms_5ACB

#undef base_5B
#undef decl_5B
#undef args_5B
#undef prms_5B

#undef base_5BA
#undef decl_5BA
#undef args_5BA
#undef prms_5BA

#undef base_5BB
#undef decl_5BB
#undef args_5BB
#undef prms_5BB

#undef base_5NAA
#undef decl_5NAA
#undef args_5NAA
#undef prms_5NAA

#undef args_5NAA_1
#undef prms_5NAA_1

#undef args_5NAA_2A
#undef prms_5NAA_2A

#undef args_5NAA_2B
#undef prms_5NAA_2B

#undef base_5NAB
#undef decl_5NAB
#undef args_5NAB
#undef prms_5NAB

#undef base_5QA
#undef decl_5QA
#undef args_5QA
#undef prms_5QA

#undef args_5QA_1
#undef prms_5QA_1

#undef args_5QA_2A
#undef prms_5QA_2A

#undef args_5QA_2B
#undef prms_5QA_2B

#undef args_5QA_2C
#undef prms_5QA_2C

#undef args_5QA_3C
#undef prms_5QA_3C

#undef base_5QB
#undef decl_5QB
#undef args_5QB
#undef prms_5QB

#undef base_6BAA
#undef decl_6BAA
#undef args_6BAA
#undef prms_6BAA

#undef base_6BAB
#undef decl_6BAB
#undef args_6BAB
#undef prms_6BAB

#undef base_7BAAA
#undef decl_7BAAA
#undef args_7BAAA
#undef prms_7BAAA

#undef base_7BAAB
#undef decl_7BAAB
#undef args_7BAAB
#undef prms_7BAAB

#undef args_7BAAB_1
#undef prms_7BAAB_1

#undef base_7BABA
#undef decl_7BABA
#undef args_7BABA
#undef prms_7BABA

#undef base_7BABB
#undef decl_7BABB
#undef args_7BABB
#undef prms_7BABB

#undef base_8BABA
#undef decl_8BABA
#undef args_8BABA
#undef prms_8BABA
//\end{frequently maintained part}

#undef evaluate

#undef wrap
#undef unwrap
#undef unwrap_
#undef unwrap_2
#undef unwrap_2_unwrap_2
#undef unwrap_4
#undef unwrap_4_unwrap_2

#undef comma

#undef select
#undef selected

#undef opts
#undef type
#undef typr
#undef II
#undef Ii
#undef I_I_

#undef _
#undef X
#undef R
#undef RR
