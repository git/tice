/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace pair {

        template<typename I, typename arg__element_1, typename arg__element_2>
        struct construct
        {
          typedef arg__element_1 first_type;
          typedef arg__element_2 second_type;
          arg__element_1 first;
          arg__element_2 second;

          construct(const construct &) = default;

          construct(construct &&) = default;

          constexpr construct() :
            first(), second() {
          }

          constexpr construct(const arg__element_1 &first, const arg__element_2 &second) :
            first(first), second(second) {
          }

        };

      }
    }
  }
}

namespace tice {
  namespace v1 {

    template<typename arg__element_1, typename arg__element_2>
    class Pair :
      public internals::pair::construct<I, arg__element_1, arg__element_2>
    {
    public:
      using internals::pair::construct<I, arg__element_1, arg__element_2>::construct;
    };

  }
}
