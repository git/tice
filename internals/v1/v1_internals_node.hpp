/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

namespace tice {
  namespace v1 {
    namespace internals {
      namespace error {
        namespace node {
          namespace construct {

            template<bool arg__is_comp_Unit, typename arg__actual_parameter>
            struct arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, typename arg__actual_parameter>
            struct arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, typename arg__actual_parameter>
            struct arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, typename arg__wcet>
            struct arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, typename arg__actual_parameter>
            struct arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, typename arg__period, typename arg__wcet>
            struct arg_3_is_greater_than_or_equal_to_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

          }
          namespace validate {

            template<bool arg__is_Node, typename arg__actual_parameter>
            struct arg_2_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Arg 2 is not class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, typename arg__actual_parameter>
            struct arg_2_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, typename arg__actual_parameter>
            struct arg_2_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, typename arg__actual_parameter>
            struct arg_2_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_2_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, typename arg__wcet>
            struct arg_2_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, typename arg__actual_parameter>
            struct arg_2_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, typename arg__period, typename arg__wcet>
            struct arg_2_arg_3_is_greater_than_or_equal_to_arg_2_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

          }
          namespace validate_type {

            template<bool arg__is_Node, typename arg__actual_parameter>
            struct arg_2_is_Node {
              static_assert(arg__is_Node,
                            "[DEBUG] Arg 2 is not class template tice::v1::Node");
            };

            template<bool arg__is_comp_Unit, typename arg__actual_parameter>
            struct arg_2_arg_2_is_comp_Unit {
              static_assert(arg__is_comp_Unit,
                            "[DEBUG] Computation is not class template tice::v1::comp::Unit");
            };

            template<bool arg__is_function_pointer, typename arg__actual_parameter>
            struct arg_2_arg_2_arg_2_is_function_pointer {
              static_assert(arg__is_function_pointer,
                            "[DEBUG] Computation is not passed as a function pointer");
            };

            template<bool arg__is_Ratio, typename arg__actual_parameter>
            struct arg_2_arg_2_arg_3_is_Ratio {
              static_assert(arg__is_Ratio,
                            "[DEBUG] Computation WCET is not specified using class template tice::v1::Ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_2_arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
            };

            template<bool arg__is_positive, typename arg__wcet>
            struct arg_2_arg_2_arg_3_is_positive {
              static_assert(arg__is_positive,
                            "[DEBUG] Computation WCET is not positive");
            };

            template<bool arg__is_std_ratio, typename arg__actual_parameter>
            struct arg_2_arg_3_is_std_ratio {
              static_assert(arg__is_std_ratio,
                            "[DEBUG] Period is not specified using class template std::ratio");
            };

            template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
            struct arg_2_arg_3_has_nonzero_denominator {
              static_assert(arg__has_nonzero_denominator,
                            "[DEBUG] Arg 3 is not std::ratio instance with nonzero denominator");
            };

            template<bool arg__is_greater_than_or_equal, typename arg__period, typename arg__wcet>
            struct arg_2_arg_3_is_greater_than_or_equal_to_arg_2_arg_2_arg_3 {
              static_assert(arg__is_greater_than_or_equal,
                            "[DEBUG] Period is less than computation WCET");
            };

            template<bool arg__has_correct_type, typename arg__node_type>
            struct arg_2_has_correct_type {
              static_assert(arg__has_correct_type,
                            "[DEBUG] Arg 2's node type is not correct");
            };

          }
        }
      }
      namespace node {

#include "v1_internals_begin.hpp"

#define base_1A(I, _1) I                                \
        opts  (_1, arg__comp_Unit__computation, ())
#define decl_1A(I, _1)                          \
        base_1A(I##I,                           \
                type(_1, typename))
#define args_1A(I, _1) base_1A(I, _1)
#define prms_1A(...) decl_1A(__VA_ARGS__)

#define base_1B(_d1, _d2, _d3, _d4, _d5, I, _1, _2, _3, _4, _5) I       \
        opts  (_1, arg__computation_validator_msg, _d1)                 \
          opts(_2, arg__computation_function_ptr_checker_msg, _d2)      \
          opts(_3, arg__computation_wcet_checker_msg, _d3)              \
          opts(_4, arg__computation_wcet_den_checker_msg, _d4)          \
          opts(_5, arg__computation_wcet_validator_msg, _d5)
#define decl_1B(_d1, _d2, _d3, _d4, _d5, I, _1, _2, _3, _4, _5)                                                 \
        base_1B(_d1, _d2, _d3, _d4, _d5, I##I,                                                                  \
                type(_1, template<bool arg__is_comp_Unit_, typename arg__actual_parameter_> class),             \
                type(_2, template<bool arg__is_function_pointer_, typename arg__actual_parameter_> class),      \
                type(_3, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),             \
                type(_4, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),  \
                type(_5, template<bool arg__is_positive_, typename arg__wcet_> class))
#define args_1B(I, _1, _2, _3, _4, _5) base_1B((), (), (), (), (), I, _1, _2, _3, _4, _5)
#define prms_1B(...) decl_1B((), (), (), (), (), __VA_ARGS__)

#define base_2B(args_1A, args_1B)               \
        args_1A                                 \
        args_1B
#define decl_2B(I, _1A_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5)    \
        base_2B(prms_1A(I, _1A_1),                              \
                prms_1B(, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5))
#define args_2B(I, _1A_1, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5) base_2B(args_1A(I, _1A_1), args_1B(, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5))
#define prms_2B(...) decl_2B(__VA_ARGS__)

        template<prms_2B(I, _, _, _, _, _, _)>
        struct construct__parse_arg_1;

#define base_2A(_d2, args_1A, _1, _2)                           \
        args_1A                                                 \
        opts  (_1, arg__std_ratio__period, ())                  \
          opts(_2, arg__period_and_wcet_validator_msg, _d2)
#define decl_2A(_d2, I, _1A_1, _1, _2)                                                                                          \
        base_2A(_d2,                                                                                                            \
                prms_1A(I, _1A_1),                                                                                              \
                type(_1, typename),                                                                                             \
                type(_2, template<bool arg__is_greater_than_or_equal_, typename arg__period_, typename arg__wcet_> class))
#define args_2A(I, _1A_1, _1, _2) base_2A((), args_1A(I, _1A_1), _1, _2)
#define prms_2A(...) decl_2A((), __VA_ARGS__)

        template<prms_2A(I, _, _, _)>
        struct construct__result;

#define base_3A(_d1, _d2, args_2A, args_1B, _1, _2)     \
        args_2A                                         \
        args_1B                                         \
        opts  (_1, arg__period_den_checker_msg, _d1)    \
          opts(_2, arg__period_checker_msg, _d2)
#define decl_3A(_2A_d2, _1B_d1, _1B_d2, _1B_d3, _1B_d4, _1B_d5, _d1, _d2, I, _1A_1, _2A_1, _2A_2, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1, _2)    \
        base_3A(_d1, _d2,                                                                                                                       \
                decl_2A(_2A_d2, I, _1A_1, _2A_1, _2A_2),                                                                                        \
                decl_1B(_1B_d1, _1B_d2, _1B_d3, _1B_d4, _1B_d5, , _1B_1, _1B_2, _1B_3, _1B_4, _1B_5),                                           \
                type(_1, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                                  \
                type(_2, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class))
#define args_3A(I, _1A_1, _2A_1, _2A_2, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5, _1, _2)                              \
        base_3A((), (), args_2A(I, _1A_1, _2A_1, _2A_2), args_1B(, _1B_1, _1B_2, _1B_3, _1B_4, _1B_5), _1, _2)
#define prms_3A(...) decl_3A((), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_3A(_2A_d2(error::node::construct::arg_3_is_greater_than_or_equal_to_arg_2_arg_3),
                         _1B_d1(error::node::construct::arg_2_is_comp_Unit),
                         _1B_d2(error::node::construct::arg_2_arg_2_is_function_pointer),
                         _1B_d3(error::node::construct::arg_2_arg_3_is_Ratio),
                         _1B_d4(error::node::construct::arg_2_arg_3_has_nonzero_denominator),
                         _1B_d5(error::node::construct::arg_2_arg_3_is_positive),
                         _d1   (error::node::construct::arg_3_has_nonzero_denominator),
                         _d2   (error::node::construct::arg_3_is_std_ratio),
                         I, _, _, _, _, _, _, _, _, _, _)>
        struct construct
        /* {
         *   typedef typename arg__comp_Unit__computation::type type;
         *   typedef arg__std_ratio__period period;
         *   typedef typename arg__comp_Unit__computation::wcet wcet;
         *   typedef typename arg__comp_Unit__computation::fn_info fn_info;
         * }
         */;

        

        template<prms_3A(I, _, _, _, _, _, _, _, _, _, _)>
        struct construct :
          construct__parse_arg_1<args_2B(I, _, _, _, _, _, _)>,
          utility::ratio::validate<I, arg__std_ratio__period, arg__period_den_checker_msg, arg__period_checker_msg>,
          construct__result<args_2A(I, _, _, _)> {
        };

        

        template<prms_2B(I, _, _, _, _, _, _)>
        struct construct__parse_arg_1 :
          arg__computation_validator_msg<!!utility::always_false<I>(), arg__comp_Unit__computation> {
        };

        template<prms_2B(I, R(typename... args__comp_arg), _, _, _, _, _)>
        struct construct__parse_arg_1<args_2B(I, R(v1::comp::Unit<args__comp_arg...>), _, _, _, _, _)> :
          utility::run_metaprogram<I, comp::construct<I, args__comp_arg...,
                                                      arg__computation_function_ptr_checker_msg, arg__computation_wcet_checker_msg,
                                                      arg__computation_wcet_den_checker_msg, arg__computation_wcet_validator_msg>> {
        };

        

        template<prms_2A(I, _, _, _)>
        struct construct__result :
          arg__period_and_wcet_validator_msg<std::ratio_greater_equal<arg__std_ratio__period,
                                                                      typename arg__comp_Unit__computation::wcet>::value,
                                             arg__std_ratio__period, typename arg__comp_Unit__computation::wcet>
        {
          typedef typename arg__comp_Unit__computation::type type;
          typedef arg__std_ratio__period period;
          typedef typename arg__comp_Unit__computation::wcet wcet;
          typedef typename arg__comp_Unit__computation::fn_info fn_info;
        };

#include "v1_internals_end.hpp"
      }
      namespace node {

#include "v1_internals_begin.hpp"

#define base_1(_d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10) I      \
        opts  (_1 , arg, ())                                                                                    \
          opts(_2 , arg__node_checker_msg, _d2)                                                                 \
          opts(_3 , arg__node_computation_checker_msg, _d3)                                                     \
          opts(_4 , arg__node_computation_fn_ptr_checker_msg, _d4)                                              \
        opts(_5 , arg__node_computation_wcet_checker_msg, _d5)                                                  \
        opts  (_6 , arg__node_computation_wcet_den_checker_msg, _d6)                                            \
        opts  (_7 , arg__node_computation_wcet_validator_msg, _d7)                                              \
        opts  (_8 , arg__node_period_checker_msg, _d8)                                                          \
        opts  (_9 , arg__node_period_den_checker_msg, _d9)                                                      \
        opts  (_10, arg__node_period_validator_msg, _d10)
#define decl_1(_d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10)                        \
        base_1(_d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, _d10, I##I,                                                              \
               type(_1, typename),                                                                                              \
               type(_2, template<bool arg__is_Node_, typename arg__actual_parameter_> class),                                   \
               type(_3, template<bool arg__is_comp_Unit_, typename arg__actual_parameter_> class),                              \
               type(_4, template<bool arg__is_function_pointer_, typename arg__actual_parameter_> class),                       \
               type(_5, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                              \
               type(_6, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                   \
               type(_7, template<bool arg__is_positive_, typename arg__wcet_> class),                                           \
               type(_8, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                              \
               type(_9, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                   \
               type(_10, template<bool arg__is_greater_than_or_equal_, typename arg__period_, typename arg__wcet_> class))
#define args_1(I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10) base_1((), (), (), (), (), (), (), (), (), I, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10)
#define prms_1(...) decl_1((), (), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_1(_d2 (error::node::validate::arg_2_is_Node),
                        _d3 (error::node::validate::arg_2_arg_2_is_comp_Unit),
                        _d4 (error::node::validate::arg_2_arg_2_arg_2_is_function_pointer),
                        _d5 (error::node::validate::arg_2_arg_2_arg_3_is_Ratio),
                        _d6 (error::node::validate::arg_2_arg_2_arg_3_has_nonzero_denominator),
                        _d7 (error::node::validate::arg_2_arg_2_arg_3_is_positive),
                        _d8 (error::node::validate::arg_2_arg_3_is_std_ratio),
                        _d9 (error::node::validate::arg_2_arg_3_has_nonzero_denominator),
                        _d10(error::node::validate::arg_2_arg_3_is_greater_than_or_equal_to_arg_2_arg_2_arg_3),
                        I, _, _, _, _, _, _, _, _, _, _)>
        struct validate;

        

        template<prms_1(I, _, _, _, _, _, _, _, _, _, _)>
        struct validate :
          arg__node_checker_msg<!!utility::always_false<I>(), arg> {
        };

        template<prms_1(I, R(typename... args), _, _, _, _, _, _, _, _, _)>
        struct validate<args_1(I, R(v1::Node<args...>), _, _, _, _, _, _, _, _, _)> :
          utility::run_metaprogram<I, construct<I, args..., arg__node_period_validator_msg, arg__node_computation_checker_msg,
                                                arg__node_computation_fn_ptr_checker_msg, arg__node_computation_wcet_checker_msg,
                                                arg__node_computation_wcet_den_checker_msg, arg__node_computation_wcet_validator_msg,
                                                arg__node_period_den_checker_msg, arg__node_period_checker_msg>> {
        };

#include "v1_internals_end.hpp"
      }
      namespace node {

#include "v1_internals_begin.hpp"

#define base_1(_d3, I, _1, _2, _3) I                    \
        opts  (_1, arg, ())                             \
          opts(_2, arg__node_type, ())                  \
          opts(_3, arg__node_type_checker_msg, _d3)
#define decl_1(_d3, I, _1, _2, _3)                                                              \
        base_1(_d3, I##I,                                                                       \
               type(_1, typename),                                                              \
               type(_2, typename),                                                              \
               type(_3, template<bool arg__has_correct_type_, typename arg__node_type_> class))
#define args_1(I, _1, _2, _3) base_1((), I, _1, _2, _3)
#define prms_1(...) decl_1((), __VA_ARGS__)

        template<prms_1(I, _, _, _)>
        struct validate_type__check_type;

#define base_2(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, args_1, _1, _2, _3, _4, _5, _6, _7, _8, _9) \
        args_1                                                                                          \
        opts  (_1, arg__node_checker_msg, _d1)                                                          \
          opts(_2, arg__node_computation_checker_msg, _d2)                                              \
          opts(_3, arg__node_computation_fn_ptr_checker_msg, _d3)                                       \
        opts(_4, arg__node_computation_wcet_checker_msg, _d4)                                           \
        opts  (_5, arg__node_computation_wcet_den_checker_msg, _d5)                                     \
        opts  (_6, arg__node_computation_wcet_validator_msg, _d6)                                       \
        opts  (_7, arg__node_period_checker_msg, _d7)                                                   \
        opts  (_8, arg__node_period_den_checker_msg, _d8)                                               \
        opts  (_9, arg__node_period_validator_msg, _d9)
#define decl_2(_1_d3, _d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9, I, _1_1, _1_2, _1_3, _1, _2, _3, _4, _5, _6, _7, _8, _9)     \
        base_2(_d1, _d2, _d3, _d4, _d5, _d6, _d7, _d8, _d9,                                                                     \
               decl_1(_1_d3, I, _1_1, _1_2, _1_3),                                                                              \
               type(_1, template<bool arg__is_Node_, typename arg__actual_parameter_> class),                                   \
               type(_2, template<bool arg__is_comp_Unit_, typename arg__actual_parameter_> class),                              \
               type(_3, template<bool arg__is_function_pointer_, typename arg__actual_parameter_> class),                       \
               type(_4, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                              \
               type(_5, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                   \
               type(_6, template<bool arg__is_positive_, typename arg__wcet_> class),                                           \
               type(_7, template<bool arg__is_std_ratio_, typename arg__actual_parameter_> class),                              \
               type(_8, template<bool arg__has_nonzero_denominator_, typename arg__actual_parameter_> class),                   \
               type(_9, template<bool arg__is_greater_than_or_equal_, typename arg__period_, typename arg__wcet_> class))
#define args_2(I, _1_1, _1_2, _1_3, _1, _2, _3, _4, _5, _6, _7, _8, _9)                                                 \
        base_2((), (), (), (), (), (), (), (), (), args_1(I, _1_1, _1_2, _1_3), _1, _2, _3, _4, _5, _6, _7, _8, _9)
#define prms_2(...) decl_2((), (), (), (), (), (), (), (), (), (), __VA_ARGS__)

        template<decl_2(_1_d3(error::node::validate_type::arg_2_has_correct_type),
                        _d1  (error::node::validate_type::arg_2_is_Node),
                        _d2  (error::node::validate_type::arg_2_arg_2_is_comp_Unit),
                        _d3  (error::node::validate_type::arg_2_arg_2_arg_2_is_function_pointer),
                        _d4  (error::node::validate_type::arg_2_arg_2_arg_3_is_Ratio),
                        _d5  (error::node::validate_type::arg_2_arg_2_arg_3_has_nonzero_denominator),
                        _d6  (error::node::validate_type::arg_2_arg_2_arg_3_is_positive),
                        _d7  (error::node::validate_type::arg_2_arg_3_is_std_ratio),
                        _d8  (error::node::validate_type::arg_2_arg_3_has_nonzero_denominator),
                        _d9  (error::node::validate_type::arg_2_arg_3_is_greater_than_or_equal_to_arg_2_arg_2_arg_3),
                        I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct validate_type;

        

        template<prms_2(I, _, _, _, _, _, _, _, _, _, _, _, _)>
        struct validate_type :
          validate<I, arg, arg__node_checker_msg, arg__node_computation_checker_msg, arg__node_computation_fn_ptr_checker_msg,
                   arg__node_computation_wcet_checker_msg, arg__node_computation_wcet_den_checker_msg, arg__node_computation_wcet_validator_msg,
                   arg__node_period_checker_msg, arg__node_period_den_checker_msg, arg__node_period_validator_msg>,
          validate_type__check_type<args_1(I, _, _, _)> {
        };

        

        template<prms_1(I, _, _, _)>
        struct validate_type__check_type :
          arg__node_type_checker_msg<std::is_same<typename arg::type, arg__node_type>::value, typename arg::type> {
        };

#include "v1_internals_end.hpp"
      }
    }
  }
}

namespace tice {
  namespace v1 {

    template<typename arg__Comp__computation, typename arg__Ratio__period>
    class Node :
      public internals::node::construct<I, arg__Comp__computation, arg__Ratio__period,
                                        error::node::arg_2_is_greater_than_or_equal_to_arg_1_arg_2,
                                        error::node::arg_1_is_Comp,
                                        error::node::arg_1_arg_1_is_function_pointer,
                                        error::node::arg_1_arg_2_is_Ratio,
                                        error::node::arg_1_arg_2_has_nonzero_denominator,
                                        error::node::arg_1_arg_2_is_positive,
                                        error::node::arg_2_has_nonzero_denominator,
                                        error::node::arg_2_is_Ratio> {
    };

  }
}
