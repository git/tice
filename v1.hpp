/******************************************************************************
 * Copyright (C) 2018, 2019  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#ifndef TICE_V1_HPP
#define TICE_V1_HPP

#include <limits>
#include <type_traits>
#include <ratio>
#include <cstdint>

namespace tice {
  namespace v1 {

    //\begin{data structures}

    template<std::intmax_t arg__numerator, std::intmax_t arg__denominator = 1>
    using Ratio = std::ratio<arg__numerator, arg__denominator>;

    template<typename arg__element_1, typename arg__element_2>
    class Pair;

    template<typename arg__type, arg__type arg__value>
    using Value = std::integral_constant<arg__type, arg__value>;

    template<typename... args__element>
    class Tuple;
    namespace tuple {

      typedef std::size_t Size;

      typedef Size Element_pos;

      typedef Value<Element_pos, 0> Invalid_element_pos;

      template<typename arg__Tuple>
      class size;

      template<Element_pos arg__pos, typename arg__Tuple>
      class get_pos;

    }

    template<int... args__core_id>
    class Core_ids;

    template<typename arg__array_element_type, arg__array_element_type... args__element>
    class Array;
    namespace array {

      template<typename arg>
      class element_type_is_valid;

      template<typename arg__array_element_type>
      class Empty;

      template<typename arg__Array__array>
      class is_empty;

      typedef tuple::Size Idx;

      typedef Value<Idx, std::numeric_limits<Idx>::max()> Invalid_idx;

      typedef Value<Idx, std::numeric_limits<Idx>::max() - 1> Max_idx;

      typedef Idx Size;

      namespace make {

        class init_val_value_type;

        template<Size arg__N /* must be positive */, typename arg__init_val_with_value_as_constexpr_copyable_data_member,
                 typename arg__array_element_type = init_val_value_type>
        class value;

        namespace from_tuple {

          class first_element_value_type;

          template<typename arg__Tuple__of__type_with_value_as_constexpr_copyable_data_member /* must be a nonempty tuple */,
                   typename arg__array_element_type = first_element_value_type>
          class value;

        }

        namespace nested {

          template<typename... args__Array /* must be nonempty */>
          class value;

          namespace from_tuple {

            template<typename arg__Tuple__of__Array /* must be a nonempty tuple */>
            class value;

          }

        }

      }

      namespace update {

        namespace on_stray_index {
          class BREAK {
          };
          class CONTINUE {
          };
        }

        namespace on_dupd_index {
          class BREAK {
          };
          class CONTINUE_IF_IDENTICAL {
          };
          class CONTINUE_KEEP_FIRST {
          };
          class CONTINUE_KEEP_LAST {
          };
        }

        template<typename arg__Array__prev_array /* can be an empty array */,
                 typename arg__Tuple__of__dict_Entry__of__prev_array_element_type__update_list /* can be an empty tuple */,
                 typename arg__on_dupd_index = on_dupd_index::BREAK, typename arg__on_stray_index = on_stray_index::BREAK>
        class value;

      }

      namespace filter_then_map {

        template<typename arg__Array__prev_array /* can be an empty array */,
                 typename arg__array_element_type /* = typename arg__Array__prev_array::element_type */,
                 tice::v1::Pair<bool /* true iff the element passes the filter
                                      * (an element is included in the resulting array iff it passess the filter)
                                      */,
                                arg__array_element_type /* mapping result */>
                 /**/ (*arg__filter_and_mapper /* must be non-NULL */)(const tice::v1::array::Idx &idx,
                                                                       const typename arg__Array__prev_array::element_type &prev_array_element)>
        class value;

      }

      template<typename arg__Array__array>
      class elements_are_pairwise_distinct;

      template<typename arg__Array__lhs_array, typename arg__Array__rhs_array>
      class are_elementwise_equal;

    }

    namespace dict {

      typedef array::Idx Key;

      typedef array::Invalid_idx Invalid_key;

      typedef array::Max_idx Max_key;

      template<Key arg__key>
      class Entry_key;

      template<Key arg__key, typename arg__value, typename arg__specialization = void>
      class Entry;

      template<Key arg__key, bool arg__value>
      using Entry_bool = Entry<arg__key, void, Value<bool, arg__value>>;

      template<Key arg__key, int arg__value>
      using Entry_int = Entry<arg__key, void, Value<int, arg__value>>;

    }

    //\end{data structures}

    //\begin{utilities}
    namespace utility {

      // Given two lvalue id-expressions whose addresses can be taken by applying operator `&' directly to each of the expressions without first
      // enclosing the expression within parentheses, returns the result of comparing the expressions using operator `==' in a constexpr context.
      // In accordance with ISO-IEC 14882:2014 (C++14 standard) [temp.arg.nontype]p{1}, both id-expressions cannot name a reference.
      #define equal_lvalues(arg__id_expression__lhs_lvalue, arg__id_expression__rhs_lvalue)                                               \
        tice::v1::utility::Lvalue_comparison<decltype(arg__id_expression__lhs_lvalue), decltype(arg__id_expression__rhs_lvalue),          \
                                             &arg__id_expression__lhs_lvalue, &arg__id_expression__rhs_lvalue>::value

      // This is `equal_lvalues' that is designed to work when both id-expressions name arrays and returns the result of comparing the elements of
      // the arrays at the given index using operator `=='.  The `arg__idx' must be an expression of type `tice::v1::array::Idx'.
      #define point_equal_lvalues(arg__id_expression__lhs_array, arg__id_expression__rhs_array, arg__idx)                                 \
        tice::v1::utility::Array_comparison_at_a_point<decltype(arg__id_expression__lhs_array), decltype(arg__id_expression__rhs_array),  \
                                                       &arg__id_expression__lhs_array, &arg__id_expression__rhs_array, (arg__idx)>::value

      // This is `point_equal_lvalues' that allows the elements of the two arrays to be indexed independently.
      // Both `arg__lhs_idx' and `arg__rhs_idx' must be expressions of type `tice::v1::array::Idx'.
      #define pair_equal_lvalues(arg__id_expression__lhs_array, arg__id_expression__rhs_array, arg__lhs_idx, arg__rhs_idx)                \
        tice::v1::utility::Array_comparison_at_a_pair<decltype(arg__id_expression__lhs_array), decltype(arg__id_expression__rhs_array),   \
                                                      &arg__id_expression__lhs_array, &arg__id_expression__rhs_array,                     \
                                                      (arg__lhs_idx), (arg__rhs_idx)>::value

    }
    //\end{utilities}

    //\begin{architecture-dependent components}

    // Hardware description.
    template<typename arg__Core_ids__core_ids>
    class HW
    /* {
     *   // Tice back-end executes if and only if this is true.
     *   static constexpr bool backend_is_enabled;
     * }
     */;

    // Computation.  No computation may share an arg__function_ptr since
    // the pointer is used internally as a node ID to prevent duplicated nodes.
    #define Comp(arg__function_ptr, /* arg__Ratio__wcet */ ...) \
      tice::v1::comp::Unit<std::integral_constant<decltype(arg__function_ptr), arg__function_ptr>, __VA_ARGS__>

    //\end{architecture-dependent components}



    //\begin{architecture-independent components}

    // A node in a Tice graph.
    template<typename arg__Comp__computation, typename arg__Ratio__period>
    class Node;
    namespace node {

      namespace type {
        class SENSOR {
        };
        class INTERMEDIARY {
        };
        class ACTUATOR {
        };
      }

    }

    // A channel whose initial value is specified inline.
    template<typename arg__channel_type, arg__channel_type arg__initial_value>
    class Chan_inlit;

    // A channel whose initial value cannot be or is not specified inline.
    // E.g., type double cannot have its value specified as a nontype nontemplate argument.
    template<typename arg__channel_type, std::add_pointer_t<std::add_const_t<arg__channel_type>> arg__initial_value_ptr>
    class Chan;

    // A set of all of the incoming arcs of a node, which is called a consumer, in a Tice graph.
    template<typename arg__Node__producer, typename arg__Chan_inlit_or_Chan__channel, typename
             ,                                                                    /*     |                         */
             typename... args__Node__producer__comma__Chan_inlit_or_Chan__channel /*, ___V____ arg__Node__consumer */>
    class Feeder;

    // An end-to-end delay constraint decorating a Tice graph.
    template<typename arg__Node__sensor, typename arg__Node__actuator,
             typename arg__Ratio__min_delay, typename arg__Ratio__max_delay>
    class ETE_delay;

    // A correlation constraint decorating a Tice graph.
    template<typename arg__Node__actuator, typename arg__Ratio__threshold, typename arg__Node__sensor,
             typename... args__Node__sensor>
    class Correlation;

    // A program implementing a Tice graph.
    template<typename arg__HW__hw_desc, typename arg__Node__node, typename... args__Node__then__Feeder__then__ETE_delay__then__Correlation>
    class Program;

    //\end{architecture-independent components}



    //\begin{code generation}
    namespace code {

      // This macro is to be called from either the global scope or within some namespace scope to generate a class identified by
      // `arg__generated_class_identifier' from the Tice program specified by `arg__Program__program'.  The class is an empty class with an empty
      // default constructor if macro `TICE_V1_NOGEN' is defined when this macro is called.  Otherwise, the class is a singleton class with a
      // default constructor and four member functions:
      //   void run();
      //     This member function is used to run the generated Tice program.
      //   long get_error_code() const;
      //     This member function returns zero if the member function `run' has not been called and the class object can be constructed successfully
      //     and if the last call to the member function `run' is successful.  Otherwise, some system-dependent error code will be returned.  In
      //     particular, since the class is a singleton, non-zero will always be returned for any object instantiated from the generated class other
      //     than the first.
      //   void stop();
      //     This member function is used to stop the generated Tice program graciously.
      //   void wait();
      //     This member function is used to wait for the generated Tice program to stop completely before the class object can be destroyed
      //     graciously.
      //
      // It is safe to call any of the member functions in any order and in any number of times.  The normal usage, however, is to first construct
      // the class object and calls `get_error_code' on the object to check whether the construction is successful.  If successful, `run' is then
      // called on the object to run the Tice program followed by another call to `get_error_code' on the object to check whether the Tice program
      // can be run successfully.  If successful, the Tice program can be stopped by calling `stop' on the object followed by calling `wait' on the
      // object, after which the object can be safely destroyed or re-run by calling `run' on the object.
      //
      // Constructing an object of the generated class in parallel is a race condition.  Similarly, using any of the four member functions in
      // parallel is also a race condition.  Destroying any object of the generated class in parallel, however, is not a race condition.
      #define tice_v1_gen(arg__generated_class_identifier, /* arg__Program__program */ ...) \
        tice_v1_internals_gen(arg__generated_class_identifier, __VA_ARGS__)

    }
    //\end{code generation}



    //\begin{compile-time error messages}
    namespace error {

      typedef std::size_t Pos;

      typedef Value<Pos, 0> Invalid_pos;

      template<Pos... args__cycle_forming_node_pos>
      struct DAG_cycle_forming_node_pos;

      template<Pos... args__path_forming_node_pos>
      struct Path_forming_node_pos;

      namespace tuple {

        namespace size {

          template<bool arg__is_Tuple, typename arg__actual_parameter>
          struct arg_1_is_Tuple {
            static_assert(arg__is_Tuple,
                          "Arg 1 is not class template tice::v1::Tuple");
          };

        }

        namespace get_pos {

          template<bool arg__is_positive, v1::tuple::Element_pos arg__element_pos>
          struct arg_1_is_positive {
            static_assert(arg__is_positive,
                          "Arg 1 is not positive");
          };

          template<bool arg__is_Tuple, typename arg__actual_parameter>
          struct arg_2_is_Tuple {
            static_assert(arg__is_Tuple,
                          "Arg 2 is not class template tice::v1::Tuple");
          };

          template<bool arg__is_less_than_or_equal_to_arg_2_size, v1::tuple::Element_pos arg__element_pos, v1::tuple::Size arg__tuple_size>
          struct arg_1_is_less_than_or_equal_to_arg_2_size {
            static_assert(arg__is_less_than_or_equal_to_arg_2_size,
                          "Arg 1 is not less than or equal to Arg 2's size");
          };

        }

      }

      namespace array {

        template<bool arg__is_positive>
        struct array_size_is_positive {
          static_assert(arg__is_positive,
                        "Array size is not positive");
        };

        template<bool arg__is_valid_array_element_type, typename arg__actual_parameter>
        struct arg_1_is_valid_array_element_type {
          static_assert(arg__is_valid_array_element_type,
                        "Arg 1 is not valid array element type"
                        " (Arg 1 is either an incomplete type or `void' or a reference type or a function type or an abstract class)");
        };

        namespace empty {

          template<bool arg__is_valid_array_element_type, typename arg__actual_parameter>
          struct arg_1_is_valid_array_element_type {
            static_assert(arg__is_valid_array_element_type,
                          "Arg 1 is not valid array element type"
                          " (Arg 1 is either an incomplete type or `void' or a reference type or a function type or an abstract class)");
          };

        }

        namespace is_empty {

          template<bool arg__has_element_type_as_member_typedef, typename arg__actual_parameter>
          struct arg_1_has_element_type_as_member_typedef {
            static_assert(arg__has_element_type_as_member_typedef,
                          "Arg 1 has no member typedef `element_type'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_size_as_constexpr_copyable_data_member, typename arg__actual_parameter>
          struct arg_1_has_size_as_constexpr_copyable_data_member {
            static_assert(arg__has_size_as_constexpr_copyable_data_member,
                          "Arg 1 has no constexpr (not static or not constexpr)"
                          " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_type_as_member_typedef, typename arg__actual_parameter>
          struct arg_1_has_type_as_member_typedef {
            static_assert(arg__has_type_as_member_typedef,
                          "Arg 1 has no member typedef `type'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_elems_as_constexpr_copyable_data_member, typename arg__actual_parameter>
          struct arg_1_has_elems_as_constexpr_copyable_data_member {
            static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                          "Arg 1 has no constexpr (not static or not constexpr)"
                          " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, typename arg__actual_parameter>
          struct arg_1_has_elems_ptr_as_constexpr_copyable_data_member {
            static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                          "Arg 1 has no constexpr (not static or not constexpr)"
                          " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__element_type>
          struct arg_1_element_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's member typedef `element_type' is not valid array element type"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__size_type>
          struct arg_1_size_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's data member `size' is not `const tice::v1::array::Idx'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__type>
          struct arg_1_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's member typedef `type' is not `element_type[size]'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__elems_type>
          struct arg_1_elems_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's data member `elems' is not `const element_type[size]'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__elems_ptr_type>
          struct arg_1_elems_ptr_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's data member `elems_ptr' is not `const element_type *[]'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
          struct arg_1_elems_ptr_points_to_elems {
            static_assert(arg__points_to_elems,
                          "Arg 1's data member `elems_ptr' fails to point to data member `elems' at index 0"
                          " (maybe not using tice::v1::Array in the first place)");
          };

        }

        namespace make {

          namespace value {

            template<bool arg__is_positive, v1::array::Size arg__size>
            struct arg_1_is_positive {
              static_assert(arg__is_positive,
                            "Arg 1 is not positive");
            };

            template<bool arg__has_value_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_2_has_value_as_constexpr_copyable_data_member {
              static_assert(arg__has_value_as_constexpr_copyable_data_member,
                            "Arg 2 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `value'");
            };

            template<bool arg__is_convertible_to_array_element_type, typename arg__value_type, typename arg__array_element_type>
            struct value_is_convertible_to_array_element_type {
              static_assert(arg__is_convertible_to_array_element_type,
                            "Arg 2 has its `value' nonconvertible to array element type");
            };

          }

          namespace from_tuple {

            namespace value {

              template<bool arg__is_Tuple, typename arg__actual_parameter>
              struct arg_1_is_Tuple {
                static_assert(arg__is_Tuple,
                              "Arg 1 is not class template tice::v1::Tuple");
              };

              template<bool arg__is_nonempty>
              struct arg_1_is_nonempty {
                static_assert(arg__is_nonempty,
                              "Tuple has no element");
              };

              template<bool arg__has_value_as_constexpr_copyable_data_member, Pos arg__element_pos, typename arg__element>
              struct element_has_value_as_constexpr_copyable_data_member {
                static_assert(arg__has_value_as_constexpr_copyable_data_member,
                              "Element has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `value'");
              };

              template<bool arg__is_convertible_to_array_element_type, Pos arg__element_pos,
                       typename arg__element_value_type, typename arg__array_element_type>
              struct element_value_is_convertible_to_array_element_type {
                static_assert(arg__is_convertible_to_array_element_type,
                              "Element has its `value' nonconvertible to array element type");
              };

            }

          }

          namespace nested {

            namespace value {

              template<bool arg__is_nonempty>
              struct array_is_nonempty {
                static_assert(arg__is_nonempty,
                              "No nested array is specified");
              };

              template<bool arg__is_uniform, Pos arg__pos, typename arg__array_element_type , typename arg__first_array_element_type>
              struct nested_array_element_type_is_uniform {
                static_assert(arg__is_uniform,
                              "N-th array element type is not that of the first array");
              };

              template<bool arg__is_positive, Pos arg__pos, v1::array::Size arg__size>
              struct nested_array_size_is_positive {
                static_assert(arg__is_positive,
                              "N-th array size is not positive");
              };

              template<bool arg__has_element_type_as_member_typedef, Pos arg__pos, typename arg__actual_parameter>
              struct nested_array_has_element_type_as_member_typedef {
                static_assert(arg__has_element_type_as_member_typedef,
                              "N-th array has no member typedef `element_type'"
                              " (maybe not using tice::v1::Array in the first place)");
              };

              template<bool arg__has_size_as_constexpr_copyable_data_member, Pos arg__pos, typename arg__actual_parameter>
              struct nested_array_has_size_as_constexpr_copyable_data_member {
                static_assert(arg__has_size_as_constexpr_copyable_data_member,
                              "N-th array has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                              " (maybe not using tice::v1::Array in the first place)");
              };

              template<bool arg__has_type_as_member_typedef, Pos arg__pos, typename arg__actual_parameter>
              struct nested_array_has_type_as_member_typedef {
                static_assert(arg__has_type_as_member_typedef,
                              "N-th array has no member typedef `type'"
                              " (maybe not using tice::v1::Array in the first place)");
              };

              template<bool arg__has_elems_as_constexpr_copyable_data_member, Pos arg__pos, typename arg__actual_parameter>
              struct nested_array_has_elems_as_constexpr_copyable_data_member {
                static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                              "N-th array has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                              " (maybe not using tice::v1::Array in the first place)");
              };

              template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, Pos arg__pos, typename arg__actual_parameter>
              struct nested_array_has_elems_ptr_as_constexpr_copyable_data_member {
                static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                              "N-th array has no constexpr (not static or not constexpr)"
                              " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                              " (maybe not using tice::v1::Array in the first place)");
              };

              template<bool arg__is_valid, Pos arg__pos, typename arg__element_type>
              struct nested_array_element_type_is_valid {
                static_assert(arg__is_valid,
                              "Type of N-th array's member typedef `element_type' is not valid array element type"
                              " (maybe not using tice::v1::Array in the first place)");
              };

              template<bool arg__is_valid, Pos arg__pos, typename arg__size_type>
              struct nested_array_size_type_is_valid {
                static_assert(arg__is_valid,
                              "Type of N-th array's data member `size' is not `const tice::v1::array::Idx'"
                              " (maybe not using tice::v1::Array in the first place)");
              };

              template<bool arg__is_valid, Pos arg__pos, typename arg__type>
              struct nested_array_type_is_valid {
                static_assert(arg__is_valid,
                              "Type of N-th array's member typedef `type' is not `element_type[size]'"
                              " (maybe not using tice::v1::Array in the first place)");
              };

              template<bool arg__is_valid, Pos arg__pos, typename arg__elems_type>
              struct nested_array_elems_type_is_valid {
                static_assert(arg__is_valid,
                              "Type of N-th array's data member `elems' is not `const element_type[size]'"
                              " (maybe not using tice::v1::Array in the first place)");
              };

              template<bool arg__is_valid, Pos arg__pos, typename arg__elems_ptr_type>
              struct nested_array_elems_ptr_type_is_valid {
                static_assert(arg__is_valid,
                              "Type of N-th array's data member `elems_ptr' is not `const element_type *[]'"
                              " (maybe not using tice::v1::Array in the first place)");
              };

              template<bool arg__points_to_elems, Pos arg__pos, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
              struct nested_array_elems_ptr_points_to_elems {
                static_assert(arg__points_to_elems,
                              "N-th array's data member `elems_ptr' fails to point to data member `elems' at index 0"
                              " (maybe not using tice::v1::Array in the first place)");
              };

            }

            namespace from_tuple {

              namespace value {

                template<bool arg__is_Tuple, typename arg__actual_parameter>
                struct arg_1_is_Tuple {
                  static_assert(arg__is_Tuple,
                                "Arg 1 is not class template tice::v1::Tuple");
                };

                template<bool arg__is_nonempty>
                struct arg_1_is_nonempty {
                  static_assert(arg__is_nonempty,
                                "Tuple has no element");
                };

                template<bool arg__is_uniform, Pos arg__pos, typename arg__array_element_type , typename arg__first_array_element_type>
                struct nested_array_element_type_is_uniform {
                  static_assert(arg__is_uniform,
                                "N-th array element type is not that of the first array");
                };

                template<bool arg__is_positive, Pos arg__pos, v1::array::Size arg__size>
                struct nested_array_size_is_positive {
                  static_assert(arg__is_positive,
                                "N-th array size is not positive");
                };

                template<bool arg__has_element_type_as_member_typedef, Pos arg__pos, typename arg__actual_parameter>
                struct nested_array_has_element_type_as_member_typedef {
                  static_assert(arg__has_element_type_as_member_typedef,
                                "N-th array has no member typedef `element_type'"
                                " (maybe not using tice::v1::Array in the first place)");
                };

                template<bool arg__has_size_as_constexpr_copyable_data_member, Pos arg__pos, typename arg__actual_parameter>
                struct nested_array_has_size_as_constexpr_copyable_data_member {
                  static_assert(arg__has_size_as_constexpr_copyable_data_member,
                                "N-th array has no constexpr (not static or not constexpr)"
                                " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                                " (maybe not using tice::v1::Array in the first place)");
                };

                template<bool arg__has_type_as_member_typedef, Pos arg__pos, typename arg__actual_parameter>
                struct nested_array_has_type_as_member_typedef {
                  static_assert(arg__has_type_as_member_typedef,
                                "N-th array has no member typedef `type'"
                                " (maybe not using tice::v1::Array in the first place)");
                };

                template<bool arg__has_elems_as_constexpr_copyable_data_member, Pos arg__pos, typename arg__actual_parameter>
                struct nested_array_has_elems_as_constexpr_copyable_data_member {
                  static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                                "N-th array has no constexpr (not static or not constexpr)"
                                " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                                " (maybe not using tice::v1::Array in the first place)");
                };

                template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, Pos arg__pos, typename arg__actual_parameter>
                struct nested_array_has_elems_ptr_as_constexpr_copyable_data_member {
                  static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                                "N-th array has no constexpr (not static or not constexpr)"
                                " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                                " (maybe not using tice::v1::Array in the first place)");
                };

                template<bool arg__is_valid, Pos arg__pos, typename arg__element_type>
                struct nested_array_element_type_is_valid {
                  static_assert(arg__is_valid,
                                "Type of N-th array's member typedef `element_type' is not valid array element type"
                                " (maybe not using tice::v1::Array in the first place)");
                };

                template<bool arg__is_valid, Pos arg__pos, typename arg__size_type>
                struct nested_array_size_type_is_valid {
                  static_assert(arg__is_valid,
                                "Type of N-th array's data member `size' is not `const tice::v1::array::Idx'"
                                " (maybe not using tice::v1::Array in the first place)");
                };

                template<bool arg__is_valid, Pos arg__pos, typename arg__type>
                struct nested_array_type_is_valid {
                  static_assert(arg__is_valid,
                                "Type of N-th array's member typedef `type' is not `element_type[size]'"
                                " (maybe not using tice::v1::Array in the first place)");
                };

                template<bool arg__is_valid, Pos arg__pos, typename arg__elems_type>
                struct nested_array_elems_type_is_valid {
                  static_assert(arg__is_valid,
                                "Type of N-th array's data member `elems' is not `const element_type[size]'"
                                " (maybe not using tice::v1::Array in the first place)");
                };

                template<bool arg__is_valid, Pos arg__pos, typename arg__elems_ptr_type>
                struct nested_array_elems_ptr_type_is_valid {
                  static_assert(arg__is_valid,
                                "Type of N-th array's data member `elems_ptr' is not `const element_type *[]'"
                                " (maybe not using tice::v1::Array in the first place)");
                };

                template<bool arg__points_to_elems, Pos arg__pos, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
                struct nested_array_elems_ptr_points_to_elems {
                  static_assert(arg__points_to_elems,
                                "N-th array's data member `elems_ptr' fails to point to data member `elems' at index 0"
                                " (maybe not using tice::v1::Array in the first place)");
                };

              }

            }

          }

        }

        namespace update {

          namespace value {

            template<bool arg__has_element_type_as_member_typedef, typename arg__actual_parameter>
            struct arg_1_has_element_type_as_member_typedef {
              static_assert(arg__has_element_type_as_member_typedef,
                            "Arg 1 has no member typedef `element_type'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__has_size_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_1_has_size_as_constexpr_copyable_data_member {
              static_assert(arg__has_size_as_constexpr_copyable_data_member,
                            "Arg 1 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__has_type_as_member_typedef, typename arg__actual_parameter>
            struct arg_1_has_type_as_member_typedef {
              static_assert(arg__has_type_as_member_typedef,
                            "Arg 1 has no member typedef `type'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__has_elems_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_1_has_elems_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                            "Arg 1 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_1_has_elems_ptr_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                            "Arg 1 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__is_valid, typename arg__element_type>
            struct arg_1_element_type_is_valid {
              static_assert(arg__is_valid,
                            "Type of arg 1's member typedef `element_type' is not valid array element type"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__is_valid, typename arg__size_type>
            struct arg_1_size_type_is_valid {
              static_assert(arg__is_valid,
                            "Type of arg 1's data member `size' is not `const tice::v1::array::Idx'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__is_valid, typename arg__type>
            struct arg_1_type_is_valid {
              static_assert(arg__is_valid,
                            "Type of arg 1's member typedef `type' is not `element_type[size]'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_type>
            struct arg_1_elems_type_is_valid {
              static_assert(arg__is_valid,
                            "Type of arg 1's data member `elems' is not `const element_type[size]'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_ptr_type>
            struct arg_1_elems_ptr_type_is_valid {
              static_assert(arg__is_valid,
                            "Type of arg 1's data member `elems_ptr' is not `const element_type *[]'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
            struct arg_1_elems_ptr_points_to_elems {
              static_assert(arg__points_to_elems,
                            "Arg 1's data member `elems_ptr' fails to point to data member `elems' at index 0"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__is_Tuple, typename arg__actual_parameter>
            struct arg_2_is_Tuple {
              static_assert(arg__is_Tuple,
                            "Arg 2 is not class template tice::v1::Tuple");
            };

            template<bool arg__is_member_of_on_dupd_index, typename arg__actual_parameter>
            struct arg_3_is_member_of_on_dupd_index {
              static_assert(arg__is_member_of_on_dupd_index,
                            "Arg 3 is not member of namespace tice::v1::array::update::on_dupd_index");
            };

            template<bool arg__is_member_of_on_stray_index, typename arg__actual_parameter>
            struct arg_4_is_member_of_on_stray_index {
              static_assert(arg__is_member_of_on_stray_index,
                            "Arg 4 is not member of namespace tice::v1::array::update::on_stray_index");
            };

            template<bool arg__is_within_array_bound, Pos arg__element_pos, dict::Key arg__key>
            struct update_list_key_is_within_array_bound {
              static_assert(arg__is_within_array_bound,
                            "Update list key is outside array bound");
            };

            template<bool arg__has_no_duplicate, Pos arg__element_1_pos, Pos arg__element_2_pos, dict::Key arg__key>
            struct update_list_key_has_no_duplicate {
              static_assert(arg__has_no_duplicate,
                            "Update list key is duplicated");
            };

            template<bool arg__are_identical, Pos arg__element_1_pos, Pos arg__element_2_pos, dict::Key arg__key>
            struct update_list_key_duplicates_are_identical {
              static_assert(arg__are_identical,
                            "Update list key duplicates have non-identical associated values");
            };

            template<bool arg__has_key_as_constexpr_copyable_data_member, Pos arg__element_pos, typename arg__actual_parameter>
            struct update_list_element_has_key_as_constexpr_copyable_data_member {
              static_assert(arg__has_key_as_constexpr_copyable_data_member,
                            "Element has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `key'"
                            " (maybe not using tice::v1::dict::Entry)");
            };

            template<bool arg__has_value_as_constexpr_copyable_data_member, Pos arg__element_pos, typename arg__actual_parameter>
            struct update_list_element_has_value_as_constexpr_copyable_data_member {
              static_assert(arg__has_value_as_constexpr_copyable_data_member,
                            "Element has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `value'"
                            " (maybe not using the data-carrying version of tice::v1::dict::Entry)");
            };

            template<bool arg__is_valid, Pos arg__element_pos, typename arg__element_key_type>
            struct update_list_element_key_type_is_valid {
              static_assert(arg__is_valid,
                            "Type of element's data member `key' is not tice::v1::dict::Key"
                            " (maybe not using tice::v1::dict::Entry)");
            };

            template<bool arg__is_convertible_to_array_element_type, Pos arg__element_pos,
                     typename arg__element_value_type, typename arg__array_element_type>
            struct update_list_element_value_is_convertible_to_array_element_type {
              static_assert(arg__is_convertible_to_array_element_type,
                            "Element has its `value' nonconvertible to array element type");
            };

          }

        }

        namespace filter_then_map {

          namespace value {

            template<bool arg__has_element_type_as_member_typedef, typename arg__actual_parameter>
            struct arg_1_has_element_type_as_member_typedef {
              static_assert(arg__has_element_type_as_member_typedef,
                            "Arg 1 has no member typedef `element_type'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__has_size_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_1_has_size_as_constexpr_copyable_data_member {
              static_assert(arg__has_size_as_constexpr_copyable_data_member,
                            "Arg 1 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__has_type_as_member_typedef, typename arg__actual_parameter>
            struct arg_1_has_type_as_member_typedef {
              static_assert(arg__has_type_as_member_typedef,
                            "Arg 1 has no member typedef `type'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__has_elems_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_1_has_elems_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                            "Arg 1 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, typename arg__actual_parameter>
            struct arg_1_has_elems_ptr_as_constexpr_copyable_data_member {
              static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                            "Arg 1 has no constexpr (not static or not constexpr)"
                            " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__is_valid, typename arg__element_type>
            struct arg_1_element_type_is_valid {
              static_assert(arg__is_valid,
                            "Type of arg 1's member typedef `element_type' is not valid array element type"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__is_valid, typename arg__size_type>
            struct arg_1_size_type_is_valid {
              static_assert(arg__is_valid,
                            "Type of arg 1's data member `size' is not `const tice::v1::array::Idx'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__is_valid, typename arg__type>
            struct arg_1_type_is_valid {
              static_assert(arg__is_valid,
                            "Type of arg 1's member typedef `type' is not `element_type[size]'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_type>
            struct arg_1_elems_type_is_valid {
              static_assert(arg__is_valid,
                            "Type of arg 1's data member `elems' is not `const element_type[size]'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__is_valid, typename arg__elems_ptr_type>
            struct arg_1_elems_ptr_type_is_valid {
              static_assert(arg__is_valid,
                            "Type of arg 1's data member `elems_ptr' is not `const element_type *[]'"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
            struct arg_1_elems_ptr_points_to_elems {
              static_assert(arg__points_to_elems,
                            "Arg 1's data member `elems_ptr' fails to point to data member `elems' at index 0"
                            " (maybe not using tice::v1::Array in the first place)");
            };

            template<bool arg__is_valid_array_element_type, typename arg__actual_parameter>
            struct arg_2_is_valid_array_element_type {
              static_assert(arg__is_valid_array_element_type,
                            "Arg 2 is not valid array element type"
                            " (Arg 2 is either an incomplete type or `void' or a reference type or a function type or an abstract class)");
            };

            template<bool arg__is_not_nullptr>
            struct arg_3_is_not_nullptr {
              static_assert(arg__is_not_nullptr,
                            "Arg 3 is a nullptr");
            };

          }

        }

        namespace elements_are_pairwise_distinct {

          template<bool arg__has_element_type_as_member_typedef, typename arg__actual_parameter>
          struct arg_1_has_element_type_as_member_typedef {
            static_assert(arg__has_element_type_as_member_typedef,
                          "Arg 1 has no member typedef `element_type'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_size_as_constexpr_copyable_data_member, typename arg__actual_parameter>
          struct arg_1_has_size_as_constexpr_copyable_data_member {
            static_assert(arg__has_size_as_constexpr_copyable_data_member,
                          "Arg 1 has no constexpr (not static or not constexpr)"
                          " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_type_as_member_typedef, typename arg__actual_parameter>
          struct arg_1_has_type_as_member_typedef {
            static_assert(arg__has_type_as_member_typedef,
                          "Arg 1 has no member typedef `type'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_elems_as_constexpr_copyable_data_member, typename arg__actual_parameter>
          struct arg_1_has_elems_as_constexpr_copyable_data_member {
            static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                          "Arg 1 has no constexpr (not static or not constexpr)"
                          " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, typename arg__actual_parameter>
          struct arg_1_has_elems_ptr_as_constexpr_copyable_data_member {
            static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                          "Arg 1 has no constexpr (not static or not constexpr)"
                          " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__element_type>
          struct arg_1_element_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's member typedef `element_type' is not valid array element type"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__size_type>
          struct arg_1_size_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's data member `size' is not `const tice::v1::array::Idx'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__type>
          struct arg_1_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's member typedef `type' is not `element_type[size]'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__elems_type>
          struct arg_1_elems_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's data member `elems' is not `const element_type[size]'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__elems_ptr_type>
          struct arg_1_elems_ptr_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's data member `elems_ptr' is not `const element_type *[]'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
          struct arg_1_elems_ptr_points_to_elems {
            static_assert(arg__points_to_elems,
                          "Arg 1's data member `elems_ptr' fails to point to data member `elems' at index 0"
                          " (maybe not using tice::v1::Array in the first place)");
          };

        }

        namespace are_elementwise_equal {

          template<bool arg__has_element_type_as_member_typedef, typename arg__actual_parameter>
          struct arg_1_has_element_type_as_member_typedef {
            static_assert(arg__has_element_type_as_member_typedef,
                          "Arg 1 has no member typedef `element_type'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_size_as_constexpr_copyable_data_member, typename arg__actual_parameter>
          struct arg_1_has_size_as_constexpr_copyable_data_member {
            static_assert(arg__has_size_as_constexpr_copyable_data_member,
                          "Arg 1 has no constexpr (not static or not constexpr)"
                          " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_type_as_member_typedef, typename arg__actual_parameter>
          struct arg_1_has_type_as_member_typedef {
            static_assert(arg__has_type_as_member_typedef,
                          "Arg 1 has no member typedef `type'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_elems_as_constexpr_copyable_data_member, typename arg__actual_parameter>
          struct arg_1_has_elems_as_constexpr_copyable_data_member {
            static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                          "Arg 1 has no constexpr (not static or not constexpr)"
                          " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, typename arg__actual_parameter>
          struct arg_1_has_elems_ptr_as_constexpr_copyable_data_member {
            static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                          "Arg 1 has no constexpr (not static or not constexpr)"
                          " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__element_type>
          struct arg_1_element_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's member typedef `element_type' is not valid array element type"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__size_type>
          struct arg_1_size_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's data member `size' is not `const tice::v1::array::Idx'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__type>
          struct arg_1_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's member typedef `type' is not `element_type[size]'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__elems_type>
          struct arg_1_elems_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's data member `elems' is not `const element_type[size]'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__elems_ptr_type>
          struct arg_1_elems_ptr_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 1's data member `elems_ptr' is not `const element_type *[]'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
          struct arg_1_elems_ptr_points_to_elems {
            static_assert(arg__points_to_elems,
                          "Arg 1's data member `elems_ptr' fails to point to data member `elems' at index 0"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_element_type_as_member_typedef, typename arg__actual_parameter>
          struct arg_2_has_element_type_as_member_typedef {
            static_assert(arg__has_element_type_as_member_typedef,
                          "Arg 2 has no member typedef `element_type'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_size_as_constexpr_copyable_data_member, typename arg__actual_parameter>
          struct arg_2_has_size_as_constexpr_copyable_data_member {
            static_assert(arg__has_size_as_constexpr_copyable_data_member,
                          "Arg 2 has no constexpr (not static or not constexpr)"
                          " copyable (copy constructor maybe deleted or not constexpr) data member `size'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_type_as_member_typedef, typename arg__actual_parameter>
          struct arg_2_has_type_as_member_typedef {
            static_assert(arg__has_type_as_member_typedef,
                          "Arg 2 has no member typedef `type'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_elems_as_constexpr_copyable_data_member, typename arg__actual_parameter>
          struct arg_2_has_elems_as_constexpr_copyable_data_member {
            static_assert(arg__has_elems_as_constexpr_copyable_data_member,
                          "Arg 2 has no constexpr (not static or not constexpr)"
                          " copyable (copy constructor maybe deleted or not constexpr) data member `elems'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__has_elems_ptr_as_constexpr_copyable_data_member, typename arg__actual_parameter>
          struct arg_2_has_elems_ptr_as_constexpr_copyable_data_member {
            static_assert(arg__has_elems_ptr_as_constexpr_copyable_data_member,
                          "Arg 2 has no constexpr (not static or not constexpr)"
                          " copyable (copy constructor maybe deleted or not constexpr) data member `elems_ptr'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__element_type>
          struct arg_2_element_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 2's member typedef `element_type' is not valid array element type"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__size_type>
          struct arg_2_size_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 2's data member `size' is not `const tice::v1::array::Idx'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__type>
          struct arg_2_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 2's member typedef `type' is not `element_type[size]'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__elems_type>
          struct arg_2_elems_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 2's data member `elems' is not `const element_type[size]'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__is_valid, typename arg__elems_ptr_type>
          struct arg_2_elems_ptr_type_is_valid {
            static_assert(arg__is_valid,
                          "Type of arg 2's data member `elems_ptr' is not `const element_type *[]'"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__points_to_elems, typename arg__elems_ptr_type, arg__elems_ptr_type arg__elems_ptr>
          struct arg_2_elems_ptr_points_to_elems {
            static_assert(arg__points_to_elems,
                          "Arg 2's data member `elems_ptr' fails to point to data member `elems' at index 0"
                          " (maybe not using tice::v1::Array in the first place)");
          };

          template<bool arg__are_of_equal_element_type, typename arg__lhs_element_type, typename arg__rhs_element_type>
          struct arrays_are_of_equal_element_type {
            static_assert(arg__are_of_equal_element_type,
                          "Arrays are not equal in element type");
          };

          template<bool arg__are_of_equal_size, v1::array::Size arg__lhs_size, v1::array::Size arg__rhs_size>
          struct arrays_are_of_equal_size {
            static_assert(arg__are_of_equal_size,
                          "Arrays are not equal in size");
          };

        }

      }

      namespace dict {

        template<bool arg__is_correct>
        struct usage_is_correct {
          static_assert(arg__is_correct,
                        "Template tice::v1::dict::Entry is instantiated with neither two arguments for carrying no data"
                        " nor three arguments with `void' as the second for carrying some data");
        };

      }

      namespace utility {

        namespace point_equal_lvalues {

          template<bool arg__is_array_type, typename arg__actual_parameter>
          struct arg_1_is_array_type {
            static_assert(arg__is_array_type,
                          "Arg 1 cannot decay to a non-function pointer type"
                          " (e.g., both `T[]' and `T[n]' decay to `T *' for any valid type `T' and size `n')");
          };

          template<bool arg__is_array_type, typename arg__actual_parameter>
          struct arg_2_is_array_type {
            static_assert(arg__is_array_type,
                          "Arg 2 cannot decay to a non-function pointer type"
                          " (e.g., both `T[]' and `T[n]' decay to `T *' for any valid type `T' and size `n')");
          };

          template<bool arg__is_valid_idx, v1::array::Idx arg__idx>
          struct arg_3_is_valid_idx {
            static_assert(arg__is_valid_idx,
                          "Arg 3 is an invalid array index");
          };

        }

        namespace pair_equal_lvalues {

          template<bool arg__is_array_type, typename arg__actual_parameter>
          struct arg_1_is_array_type {
            static_assert(arg__is_array_type,
                          "Arg 1 cannot decay to a non-function pointer type"
                          " (e.g., both `T[]' and `T[n]' decay to `T *' for any valid type `T' and size `n')");
          };

          template<bool arg__is_array_type, typename arg__actual_parameter>
          struct arg_2_is_array_type {
            static_assert(arg__is_array_type,
                          "Arg 2 cannot decay to a non-function pointer type"
                          " (e.g., both `T[]' and `T[n]' decay to `T *' for any valid type `T' and size `n')");
          };

          template<bool arg__is_valid_idx, v1::array::Idx arg__idx>
          struct arg_3_is_valid_idx {
            static_assert(arg__is_valid_idx,
                          "Arg 3 is an invalid array index");
          };

          template<bool arg__is_valid_idx, v1::array::Idx arg__idx>
          struct arg_4_is_valid_idx {
            static_assert(arg__is_valid_idx,
                          "Arg 4 is an invalid array index");
          };

        }

      }

      namespace hw {

        template<bool arg__is_Core_ids, typename arg__actual_parameter>
        struct arg_1_is_Core_ids {
          static_assert(arg__is_Core_ids,
                        "Processor core IDs are not specified using class template tice::v1::Core_ids");
        };

        template<bool arg__are_not_duplicated, Pos arg__core_id_1_pos, Pos arg__core_id_2_pos, int arg__core_id>
        struct arg_1_core_ids_are_not_duplicated {
          static_assert(arg__are_not_duplicated,
                        "Processor core IDs are not pairwise distinct");
        };

      }

      namespace comp {

        template<bool arg__is_function_pointer, typename arg__actual_parameter>
        struct arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, typename arg__actual_parameter>
        struct arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
        struct arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, typename arg__wcet>
        struct arg_2_is_positive {
          static_assert(arg__is_positive,
                        "WCET is not positive");
        };

      }

      namespace node {

        template<bool arg__is_Comp, typename arg__actual_parameter>
        struct arg_1_is_Comp {
          static_assert(arg__is_Comp,
                        "Computation is not specified using macro Comp in tice/v1.hpp");
        };

        template<bool arg__is_function_pointer, typename arg__actual_parameter>
        struct arg_1_arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, typename arg__actual_parameter>
        struct arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Computation WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
        struct arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Computation WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, typename arg__wcet>
        struct arg_1_arg_2_is_positive {
          static_assert(arg__is_positive,
                        "Computation WCET is not positive");
        };

        template<bool arg__is_Ratio, typename arg__actual_parameter>
        struct arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Period is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
        struct arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Period is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_greater_than_or_equal, typename arg__period, typename arg__wcet>
        struct arg_2_is_greater_than_or_equal_to_arg_1_arg_2 {
          static_assert(arg__is_greater_than_or_equal,
                        "Period is less than computation WCET");
        };

      }

      namespace chan_inlit {

        template<bool arg__is_object_type, typename arg__actual_parameter>
        struct arg_1_is_object_type {
          static_assert(arg__is_object_type,
                        "Channel type is not an object type (neither void nor reference type nor function type)");
        };

        template<bool arg__is_cv_unqualified, typename arg__actual_parameter>
        struct arg_1_is_cv_unqualified {
          static_assert(arg__is_cv_unqualified,
                        "Channel type cannot be cv-qualified (qualified with either const or volatile or both)");
        };

      }

      namespace chan {

        template<bool arg__is_object_type, typename arg__actual_parameter>
        struct arg_1_is_object_type {
          static_assert(arg__is_object_type,
                        "Channel type is not an object type (neither void nor reference type nor function type)");
        };

        template<bool arg__is_cv_unqualified, typename arg__actual_parameter>
        struct arg_1_is_cv_unqualified {
          static_assert(arg__is_cv_unqualified,
                        "Channel type cannot be cv-qualified (qualified with either const or volatile or both)");
        };

        template<bool arg__is_not_nullptr>
        struct arg_2_is_not_nullptr {
          static_assert(arg__is_not_nullptr,
                        "Channel initial value pointer cannot be null");
        };

      }

      namespace feeder {

        template<bool arg__is_equal, unsigned arg__channel_count, unsigned arg__parameter_count>
        struct channel_and_consumer_parameter_counts_are_equal {
          static_assert(arg__is_equal,
                        "Consumer's parameter count and incoming arc count differ");
        };

        template<bool arg__is_compatible, Pos arg__channel_pos, Pos arg__parameter_pos, typename arg__channel_type, typename arg__parameter_type>
        struct channel_and_consumer_parameter_are_compatible {
          static_assert(arg__is_compatible,
                        "Channel cannot initialize consumer's corresponding parameter");
        };

        template<bool arg__is_callable, typename arg__consumer_function>
        struct consumer_function_is_callable {
          static_assert(arg__is_callable,
                        "Consumer function is not callable");
        };

        template<bool arg__is_assignable, Pos arg__producer_pos, Pos arg__channel_pos, typename arg__return_type, typename arg__channel_type>
        struct producer_return_value_is_assignable_to_channel {
          static_assert(arg__is_assignable,
                        "Producer's return value is not assignable to channel");
        };

        template<bool arg__can_initialize, Pos arg__channel_pos, typename arg__init_val_type, typename arg__channel_type>
        struct init_val_can_initialize_channel {
          static_assert(arg__can_initialize,
                        "Initial value cannot initialize channel");
        };

        template<bool arg__is_missing, Pos arg__pos>
        struct node_arg_is_missing {
          static_assert(arg__is_missing,
                        "Missing argument whose type is tice::v1::Node");
        };

        template<bool arg__is_missing, Pos arg__pos>
        struct channel_arg_is_missing {
          static_assert(arg__is_missing,
                        "Missing argument whose type is either tice::v1::Chan_inlit or tice::v1::Chan");
        };

        template<bool arg__is_producer_node, Pos arg__pos, typename arg__actual_parameter>
        struct arg_is_producer_node {
          static_assert(arg__is_producer_node,
                        "Producer node is not specified using class template tice::v1::Node");
        };

        template<bool arg__is_consumer_node, Pos arg__pos, typename arg__actual_parameter>
        struct arg_is_consumer_node {
          static_assert(arg__is_consumer_node,
                        "Consumer node is not specified using class template tice::v1::Node");
        };

        template<bool arg__is_Comp, Pos arg__pos, typename arg__actual_parameter>
        struct node_arg_1_is_Comp {
          static_assert(arg__is_Comp,
                        "Node's computation is not specified using macro Comp in tice/v1.hpp");
        };

        template<bool arg__is_function_pointer, Pos arg__pos, typename arg__actual_parameter>
        struct node_arg_1_arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Node's computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, Pos arg__pos, typename arg__actual_parameter>
        struct node_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's computation WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__pos, typename arg__actual_parameter>
        struct node_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's computation WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, Pos arg__pos, typename arg__wcet>
        struct node_arg_1_arg_2_is_positive {
          static_assert(arg__is_positive,
                        "Node's computation WCET is not positive");
        };

        template<bool arg__is_Ratio, Pos arg__pos, typename arg__actual_parameter>
        struct node_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's period is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__pos, typename arg__actual_parameter>
        struct node_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's period is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_greater_than_or_equal, Pos arg__pos, typename arg__period, typename arg__wcet>
        struct node_arg_2_is_greater_than_or_equal_to_node_arg_1_arg_2 {
          static_assert(arg__is_greater_than_or_equal,
                        "Node's period is less than its computation WCET");
        };

        template<bool arg__is_channel, Pos arg__pos, typename arg__actual_parameter>
        struct arg_is_channel {
          static_assert(arg__is_channel,
                        "Channel is not specified using class template tice::v1::Chan_inlit or tice::v1::Chan");
        };

        template<bool arg__is_object_type, Pos arg__pos, typename arg__actual_parameter>
        struct channel_arg_1_is_object_type {
          static_assert(arg__is_object_type,
                        "Channel type is not an object type (neither void nor reference type nor function type)");
        };

        template<bool arg__is_cv_unqualified, Pos arg__pos, typename arg__actual_parameter>
        struct channel_arg_1_is_cv_unqualified {
          static_assert(arg__is_cv_unqualified,
                        "Channel type cannot be cv-qualified (qualified with either const or volatile or both)");
        };

        template<bool arg__is_not_nullptr, Pos arg__pos>
        struct channel_arg_2_is_not_nullptr {
          static_assert(arg__is_not_nullptr,
                        "Channel initial value pointer cannot be null");
        };

        template<bool arg__has_no_loop, Pos arg__producer_pos, Pos arg__consumer_pos>
        struct graph_has_no_loop {
          static_assert(arg__has_no_loop,
                        "Tice graph cannot have a loop");
        };

        template<bool arg__has_no_multiple_edges, Pos arg__producer_1_pos, Pos arg__producer_2_pos>
        struct graph_has_no_multiple_edges {
          static_assert(arg__has_no_multiple_edges,
                        "Tice graph cannot have multiple edges");
        };

      }

      namespace ete_delay {

        template<bool arg__is_Node, typename arg__actual_parameter>
        struct arg_1_is_Node {
          static_assert(arg__is_Node,
                        "Arg 1 is not class template tice::v1::Node");
        };

        template<bool arg__is_Comp, typename arg__actual_parameter>
        struct arg_1_arg_1_is_Comp {
          static_assert(arg__is_Comp,
                        "Node's computation is not specified using macro Comp in tice/v1.hpp");
        };

        template<bool arg__is_function_pointer, typename arg__actual_parameter>
        struct arg_1_arg_1_arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Node's computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, typename arg__actual_parameter>
        struct arg_1_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's computation WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
        struct arg_1_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's computation WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, typename arg__wcet>
        struct arg_1_arg_1_arg_2_is_positive {
          static_assert(arg__is_positive,
                        "Node's computation WCET is not positive");
        };

        template<bool arg__is_Ratio, typename arg__actual_parameter>
        struct arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's period is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
        struct arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's period is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_greater_than_or_equal, typename arg__period, typename arg__wcet>
        struct arg_1_arg_2_is_greater_than_or_equal_to_arg_1_arg_1_arg_2 {
          static_assert(arg__is_greater_than_or_equal,
                        "Node's period is less than its computation WCET");
        };

        template<bool arg__is_sensor_node, typename arg__node_type>
        struct arg_1_is_sensor_node {
          static_assert(arg__is_sensor_node,
                        "Arg 1 is not sensor node");
        };

        template<bool arg__is_Node, typename arg__actual_parameter>
        struct arg_2_is_Node {
          static_assert(arg__is_Node,
                        "Arg 2 is not class template tice::v1::Node");
        };

        template<bool arg__is_Comp, typename arg__actual_parameter>
        struct arg_2_arg_1_is_Comp {
          static_assert(arg__is_Comp,
                        "Node's computation is not specified using macro Comp in tice/v1.hpp");
        };

        template<bool arg__is_function_pointer, typename arg__actual_parameter>
        struct arg_2_arg_1_arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Node's computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, typename arg__actual_parameter>
        struct arg_2_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's computation WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
        struct arg_2_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's computation WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, typename arg__wcet>
        struct arg_2_arg_1_arg_2_is_positive {
          static_assert(arg__is_positive,
                        "Node's computation WCET is not positive");
        };

        template<bool arg__is_Ratio, typename arg__actual_parameter>
        struct arg_2_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's period is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
        struct arg_2_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's period is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_greater_than_or_equal, typename arg__period, typename arg__wcet>
        struct arg_2_arg_2_is_greater_than_or_equal_to_arg_2_arg_1_arg_2 {
          static_assert(arg__is_greater_than_or_equal,
                        "Node's period is less than its computation WCET");
        };

        template<bool arg__is_actuator_node, typename arg__actual_parameter>
        struct arg_2_is_actuator_node {
          static_assert(arg__is_actuator_node,
                        "Arg 2 is not actuator node");
        };

        template<bool arg__is_Ratio, typename arg__actual_parameter>
        struct arg_3_is_Ratio {
          static_assert(arg__is_Ratio,
                        "End-to-end minimum delay is not class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
        struct arg_3_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "End-to-end minimum delay is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_nonnegative, typename arg__actual_parameter>
        struct arg_3_is_nonnegative {
          static_assert(arg__is_nonnegative,
                        "End-to-end minimum delay is not nonnegative");
        };

        template<bool arg__is_Ratio, typename arg__actual_parameter>
        struct arg_4_is_Ratio {
          static_assert(arg__is_Ratio,
                        "End-to-end maximum delay is not class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
        struct arg_4_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "End-to-end maximum delay is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, typename arg__actual_parameter>
        struct arg_4_is_positive {
          static_assert(arg__is_positive,
                        "End-to-end maximum delay is not positive");
        };

        template<bool arg__is_less_than_or_equal_to_arg_4, typename arg__min_delay, typename arg__max_delay>
        struct arg_3_is_less_than_or_equal_to_arg_4 {
          static_assert(arg__is_less_than_or_equal_to_arg_4,
                        "End-to-end minimum delay is not less than or equal to end-to-end maximum delay");
        };

      }

      namespace correlation {

        template<bool arg__is_Node, typename arg__actual_parameter>
        struct arg_1_is_Node {
          static_assert(arg__is_Node,
                        "Arg 1 is not class template tice::v1::Node");
        };

        template<bool arg__is_Comp, typename arg__actual_parameter>
        struct arg_1_arg_1_is_Comp {
          static_assert(arg__is_Comp,
                        "Node's computation is not specified using macro Comp in tice/v1.hpp");
        };

        template<bool arg__is_function_pointer, typename arg__actual_parameter>
        struct arg_1_arg_1_arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Node's computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, typename arg__actual_parameter>
        struct arg_1_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's computation WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
        struct arg_1_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's computation WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, typename arg__wcet>
        struct arg_1_arg_1_arg_2_is_positive {
          static_assert(arg__is_positive,
                        "Node's computation WCET is not positive");
        };

        template<bool arg__is_Ratio, typename arg__actual_parameter>
        struct arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's period is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
        struct arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's period is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_greater_than_or_equal, typename arg__period, typename arg__wcet>
        struct arg_1_arg_2_is_greater_than_or_equal_to_arg_1_arg_1_arg_2 {
          static_assert(arg__is_greater_than_or_equal,
                        "Node's period is less than its computation WCET");
        };

        template<bool arg__is_actuator_node, typename arg__node_type>
        struct arg_1_is_actuator_node {
          static_assert(arg__is_actuator_node,
                        "Arg 1 is not actuator node");
        };

        template<bool arg__is_Ratio, typename arg__actual_parameter>
        struct arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Correlation threshold is not class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, typename arg__actual_parameter>
        struct arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Correlation threshold is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_nonnegative, typename arg__actual_parameter>
        struct arg_2_is_nonnegative {
          static_assert(arg__is_nonnegative,
                        "Correlation threshold is not nonnegative");
        };

        template<bool arg__is_Node, Pos arg__pos, typename arg__actual_parameter>
        struct remaining_arg_is_Node {
          static_assert(arg__is_Node,
                        "Arg is not class template tice::v1::Node");
        };

        template<bool arg__is_Comp, Pos arg__pos, typename arg__actual_parameter>
        struct node_arg_1_is_Comp {
          static_assert(arg__is_Comp,
                        "Node's computation is not specified using macro Comp in tice/v1.hpp");
        };

        template<bool arg__is_function_pointer, Pos arg__pos, typename arg__actual_parameter>
        struct node_arg_1_arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Node's computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, Pos arg__pos, typename arg__actual_parameter>
        struct node_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's computation WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__pos, typename arg__actual_parameter>
        struct node_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's computation WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, Pos arg__pos, typename arg__wcet>
        struct node_arg_1_arg_2_is_positive {
          static_assert(arg__is_positive,
                        "Node's computation WCET is not positive");
        };

        template<bool arg__is_Ratio, Pos arg__pos, typename arg__actual_parameter>
        struct node_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's period is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__pos, typename arg__actual_parameter>
        struct node_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's period is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_greater_than_or_equal, Pos arg__pos, typename arg__period, typename arg__wcet>
        struct node_arg_2_is_greater_than_or_equal_to_node_arg_1_arg_2 {
          static_assert(arg__is_greater_than_or_equal,
                        "Node's period is less than its computation WCET");
        };

        template<bool arg__is_sensor_node, Pos arg__pos, typename arg__node_type>
        struct remaining_arg_is_sensor_node {
          static_assert(arg__is_sensor_node,
                        "Arg is not sensor node");
        };

        template<bool arg__are_distinct, Pos arg__node_1_pos, Pos arg__node_2_pos>
        struct nodes_are_distinct {
          static_assert(arg__are_distinct,
                        "Duplicated nodes are not allowed");
        };

      }

      namespace program {

        template<bool arg__is_HW, typename arg__actual_parameter>
        struct arg_1_is_HW {
          static_assert(arg__is_HW,
                        "Arg 1 is not class template tice::v1::HW");
        };

        template<bool arg__is_Core_ids, typename arg__actual_parameter>
        struct arg_1_arg_1_is_Core_ids {
          static_assert(arg__is_Core_ids,
                        "Processor core IDs are not specified using class template tice::v1::Core_ids");
        };

        template<bool arg__are_not_duplicated, Pos arg__core_id_1_pos, Pos arg__core_id_2_pos, int arg__core_id>
        struct arg_1_arg_1_core_ids_are_not_duplicated {
          static_assert(arg__are_not_duplicated,
                        "Processor core IDs are not pairwise distinct");
        };

        template<bool arg__is_Node, typename arg__actual_parameter>
        struct arg_2_is_Node {
          static_assert(arg__is_Node,
                        "Arg 2 is not class template tice::v1::Node");
        };

        template<bool arg__is_followed_by_sequence_of_Feeder, Pos arg__pos, typename arg__follower>
        struct sequence_of_Node_is_followed_only_by_sequence_of_Feeder {
          static_assert(arg__is_followed_by_sequence_of_Feeder,
                        "Sequence of tice::v1::Node class templates can only be followed by sequence of tice::v1::Feeder class templates");
        };

        template<bool arg__is_followed_only_by_sequence_of_ETE_delay_or_Correlation, Pos arg__pos, typename arg__follower>
        struct sequence_of_Feeder_is_followed_only_by_sequence_of_ETE_delay_or_Correlation {
          static_assert(arg__is_followed_only_by_sequence_of_ETE_delay_or_Correlation,
                        "Sequence of tice::v1::Feeder class templates can only be followed by sequence of"
                        " either tice::v1::ETE_delay or tice::v1::Correlation class templates");
        };

        template<bool arg__is_followed_only_by_sequence_of_Correlation, Pos arg__pos, typename arg__follower>
        struct sequence_of_ETE_delay_is_followed_only_by_sequence_of_Correlation {
          static_assert(arg__is_followed_only_by_sequence_of_Correlation,
                        "Sequence of tice::v1::ETE_delay class templates can only be followed by sequence of"
                        " tice::v1::Correlation class templates");
        };

        template<bool arg__is_followed_by_nothing, Pos arg__pos, typename arg__follower>
        struct sequence_of_Correlation_is_followed_by_nothing {
          static_assert(arg__is_followed_by_nothing,
                        "Sequence of tice::v1::Correlation class templates cannot be followed by anything else");
        };

        template<bool arg__is_Comp, Pos arg__node_pos, typename arg__actual_parameter>
        struct node_arg_1_is_Comp {
          static_assert(arg__is_Comp,
                        "Node's computation is not specified using macro Comp in tice/v1.hpp");
        };

        template<bool arg__is_function_pointer, Pos arg__node_pos, typename arg__actual_parameter>
        struct node_arg_1_arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Node's computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, Pos arg__node_pos, typename arg__actual_parameter>
        struct node_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's computation WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__node_pos, typename arg__actual_parameter>
        struct node_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's computation WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, Pos arg__node_pos, typename arg__wcet>
        struct node_arg_1_arg_2_is_positive {
          static_assert(arg__is_positive,
                        "Node's computation WCET is not positive");
        };

        template<bool arg__is_Ratio, Pos arg__node_pos, typename arg__actual_parameter>
        struct node_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's period is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__node_pos, typename arg__actual_parameter>
        struct node_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's period is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_greater_than_or_equal, Pos arg__node_pos, typename arg__period, typename arg__wcet>
        struct node_arg_2_is_greater_than_or_equal_to_node_arg_1_arg_2 {
          static_assert(arg__is_greater_than_or_equal,
                        "Node's period is less than its computation WCET");
        };

        template<bool arg__are_distinct, Pos arg__node_1_pos, Pos arg__node_2_pos>
        struct nodes_are_distinct {
          static_assert(arg__are_distinct,
                        "Duplicated nodes are not allowed");
        };

        template<bool arg__is_already_specified,
                 Pos arg__feeder_or_ete_delay_or_correlation_pos, Pos arg__node_pos_in_feeder_or_ete_delay_or_correlation>
        struct node_is_already_specified {
          static_assert(arg__is_already_specified,
                        "Feeder or ETE_delay or Correlation refers to a node that is not included in the arguments to Program");
        };

        template<bool arg__is_equal, Pos arg__feeder_pos, unsigned arg__channel_count, unsigned arg__parameter_count>
        struct feeder_channel_and_consumer_parameter_counts_are_equal {
          static_assert(arg__is_equal,
                        "Consumer's parameter count and incoming arc count differ");
        };

        template<bool arg__is_compatible, Pos arg__feeder_pos, Pos arg__channel_pos, Pos arg__parameter_pos,
                 typename arg__channel_type, typename arg__parameter_type>
        struct feeder_channel_and_consumer_parameter_are_compatible {
          static_assert(arg__is_compatible,
                        "Channel cannot initialize consumer's corresponding parameter");
        };

        template<bool arg__is_callable, Pos arg__feeder_pos, typename arg__consumer_function>
        struct feeder_consumer_function_is_callable {
          static_assert(arg__is_callable,
                        "Consumer function is not callable");
        };

        template<bool arg__is_assignable, Pos arg__feeder_pos, Pos arg__producer_pos, Pos arg__channel_pos,
                 typename arg__return_type, typename arg__channel_type>
        struct feeder_producer_return_value_is_assignable_to_channel {
          static_assert(arg__is_assignable,
                        "Producer's return value is not assignable to channel");
        };

        template<bool arg__can_initialize, Pos arg__feeder_pos, Pos arg__channel_pos, typename arg__init_val_type, typename arg__channel_type>
        struct feeder_init_val_can_initialize_channel {
          static_assert(arg__can_initialize,
                        "Initial value cannot initialize channel");
        };

        template<bool arg__is_missing, Pos arg__feeder_pos, Pos arg__pos>
        struct feeder_node_arg_is_missing {
          static_assert(arg__is_missing,
                        "Missing argument whose type is tice::v1::Node");
        };

        template<bool arg__is_missing, Pos arg__feeder_pos, Pos arg__pos>
        struct feeder_channel_arg_is_missing {
          static_assert(arg__is_missing,
                        "Missing argument whose type is either tice::v1::Chan_inlit or tice::v1::Chan");
        };

        template<bool arg__is_producer_node, Pos arg__feeder_pos, Pos arg__pos, typename arg__actual_parameter>
        struct feeder_arg_is_producer_node {
          static_assert(arg__is_producer_node,
                        "Producer node is not specified using class template tice::v1::Node");
        };

        template<bool arg__is_consumer_node, Pos arg__feeder_pos, Pos arg__pos, typename arg__actual_parameter>
        struct feeder_arg_is_consumer_node {
          static_assert(arg__is_consumer_node,
                        "Consumer node is not specified using class template tice::v1::Node");
        };

        template<bool arg__is_Comp, Pos arg__feeder_pos, Pos arg__pos, typename arg__actual_parameter>
        struct feeder_node_arg_1_is_Comp {
          static_assert(arg__is_Comp,
                        "Node's computation is not specified using macro Comp in tice/v1.hpp");
        };

        template<bool arg__is_function_pointer, Pos arg__feeder_pos, Pos arg__pos, typename arg__actual_parameter>
        struct feeder_node_arg_1_arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Node's computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, Pos arg__feeder_pos, Pos arg__pos, typename arg__actual_parameter>
        struct feeder_node_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's computation WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__feeder_pos, Pos arg__pos, typename arg__actual_parameter>
        struct feeder_node_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's computation WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, Pos arg__feeder_pos, Pos arg__pos, typename arg__wcet>
        struct feeder_node_arg_1_arg_2_is_positive {
          static_assert(arg__is_positive,
                        "Node's computation WCET is not positive");
        };

        template<bool arg__is_Ratio, Pos arg__feeder_pos, Pos arg__pos, typename arg__actual_parameter>
        struct feeder_node_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's period is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__feeder_pos, Pos arg__pos, typename arg__actual_parameter>
        struct feeder_node_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's period is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_greater_than_or_equal, Pos arg__feeder_pos, Pos arg__pos, typename arg__period, typename arg__wcet>
        struct feeder_node_arg_2_is_greater_than_or_equal_to_feeder_node_arg_1_arg_2 {
          static_assert(arg__is_greater_than_or_equal,
                        "Node's period is less than its computation WCET");
        };

        template<bool arg__is_channel, Pos arg__feeder_pos, Pos arg__pos, typename arg__actual_parameter>
        struct feeder_arg_is_channel {
          static_assert(arg__is_channel,
                        "Channel is not specified using class template tice::v1::Chan_inlit or tice::v1::Chan");
        };

        template<bool arg__is_object_type, Pos arg__feeder_pos, Pos arg__pos, typename arg__actual_parameter>
        struct feeder_channel_arg_1_is_object_type {
          static_assert(arg__is_object_type,
                        "Channel type is not an object type (neither void nor reference type nor function type)");
        };

        template<bool arg__is_cv_unqualified, Pos arg__feeder_pos, Pos arg__pos, typename arg__actual_parameter>
        struct feeder_channel_arg_1_is_cv_unqualified {
          static_assert(arg__is_cv_unqualified,
                        "Channel type cannot be cv-qualified (qualified with either const or volatile or both)");
        };

        template<bool arg__is_not_nullptr, Pos arg__feeder_pos, Pos arg__pos>
        struct feeder_channel_arg_2_is_not_nullptr {
          static_assert(arg__is_not_nullptr,
                        "Channel initial value pointer cannot be null");
        };

        template<bool arg__are_distinct, Pos arg__feeder_1_pos, Pos arg__feeder_2_pos>
        struct feeders_are_distinct {
          static_assert(arg__are_distinct,
                        "Two feeders are not allowed to specify the same consumer");
        };

        template<bool arg__has_no_loop, Pos arg__feeder_pos, Pos arg__producer_pos, Pos arg__consumer_pos>
        struct graph_has_no_loop {
          static_assert(arg__has_no_loop,
                        "Tice graph cannot have a loop");
        };

        template<bool arg__has_no_multiple_edges, Pos arg__feeder_pos, Pos arg__producer_1_pos, Pos arg__producer_2_pos>
        struct graph_has_no_multiple_edges {
          static_assert(arg__has_no_multiple_edges,
                        "Tice graph cannot have multiple edges");
        };

        template<bool arg__is_sensor_node, Pos arg__node_pos, typename arg__node_type>
        struct isolated_node_is_sensor_node {
          static_assert(arg__is_sensor_node,
                        "Isolated node is not sensor node");
        };

        template<bool arg__is_sensor_node, Pos arg__node_pos, typename arg__node_type>
        struct source_node_is_sensor_node {
          static_assert(arg__is_sensor_node,
                        "Source node is not sensor node");
        };

        template<bool arg__is_actuator_node, Pos arg__node_pos, typename arg__node_type>
        struct sink_node_is_actuator_node {
          static_assert(arg__is_actuator_node,
                        "Sink node is not actuator node");
        };

        template<bool arg__has_no_cycle, typename arg__DAG_cycle>
        struct graph_has_no_cycle {
          static_assert(arg__has_no_cycle,
                        "Tice graph cannot have a cycle");
        };

        template<bool arg__are_connected, Pos arg__constraint_pos, Pos arg__sensor_node_pos, Pos arg__actuator_node_pos>
        struct sensor_and_actuator_nodes_are_connected {
          static_assert(arg__are_connected,
                        "There is no path from sensor to actuator");
        };

        template<bool arg__is_Node, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_1_is_Node {
          static_assert(arg__is_Node,
                        "Arg 1 is not class template tice::v1::Node");
        };

        template<bool arg__is_Comp, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_1_arg_1_is_Comp {
          static_assert(arg__is_Comp,
                        "Node's computation is not specified using macro Comp in tice/v1.hpp");
        };

        template<bool arg__is_function_pointer, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_1_arg_1_arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Node's computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_1_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's computation WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_1_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's computation WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, Pos arg__ete_delay_pos, typename arg__wcet>
        struct ete_delay_arg_1_arg_1_arg_2_is_positive {
          static_assert(arg__is_positive,
                        "Node's computation WCET is not positive");
        };

        template<bool arg__is_Ratio, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's period is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's period is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_greater_than_or_equal, Pos arg__ete_delay_pos, typename arg__period, typename arg__wcet>
        struct ete_delay_arg_1_arg_2_is_greater_than_or_equal_to_ete_delay_arg_1_arg_1_arg_2 {
          static_assert(arg__is_greater_than_or_equal,
                        "Node's period is less than its computation WCET");
        };

        template<bool arg__is_sensor_node, Pos arg__ete_delay_pos, typename arg__node_type>
        struct ete_delay_arg_1_is_sensor_node {
          static_assert(arg__is_sensor_node,
                        "Arg 1 is not sensor node");
        };

        template<bool arg__is_Node, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_2_is_Node {
          static_assert(arg__is_Node,
                        "Arg 2 is not class template tice::v1::Node");
        };

        template<bool arg__is_Comp, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_2_arg_1_is_Comp {
          static_assert(arg__is_Comp,
                        "Node's computation is not specified using macro Comp in tice/v1.hpp");
        };

        template<bool arg__is_function_pointer, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_2_arg_1_arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Node's computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_2_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's computation WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_2_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's computation WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, Pos arg__ete_delay_pos, typename arg__wcet>
        struct ete_delay_arg_2_arg_1_arg_2_is_positive {
          static_assert(arg__is_positive,
                        "Node's computation WCET is not positive");
        };

        template<bool arg__is_Ratio, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_2_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's period is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_2_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's period is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_greater_than_or_equal, Pos arg__ete_delay_pos, typename arg__period, typename arg__wcet>
        struct ete_delay_arg_2_arg_2_is_greater_than_or_equal_to_ete_delay_arg_2_arg_1_arg_2 {
          static_assert(arg__is_greater_than_or_equal,
                        "Node's period is less than its computation WCET");
        };

        template<bool arg__is_actuator_node, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_2_is_actuator_node {
          static_assert(arg__is_actuator_node,
                        "Arg 2 is not actuator node");
        };

        template<bool arg__is_Ratio, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_3_is_Ratio {
          static_assert(arg__is_Ratio,
                        "End-to-end minimum delay is not class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_3_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "End-to-end minimum delay is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_nonnegative, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_3_is_nonnegative {
          static_assert(arg__is_nonnegative,
                        "End-to-end minimum delay is not nonnegative");
        };

        template<bool arg__is_Ratio, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_4_is_Ratio {
          static_assert(arg__is_Ratio,
                        "End-to-end maximum delay is not class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_4_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "End-to-end maximum delay is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, Pos arg__ete_delay_pos, typename arg__actual_parameter>
        struct ete_delay_arg_4_is_positive {
          static_assert(arg__is_positive,
                        "End-to-end maximum delay is not positive");
        };

        template<bool arg__is_less_than_or_equal_to_arg_4, Pos arg__ete_delay_pos, typename arg__min_delay, typename arg__max_delay>
        struct ete_delay_arg_3_is_less_than_or_equal_to_ete_delay_arg_4 {
          static_assert(arg__is_less_than_or_equal_to_arg_4,
                        "End-to-end minimum delay is not less than or equal to end-to-end maximum delay");
        };

        template<bool arg__is_between_min_and_max_delays, Pos arg__constraint_pos,
                 typename arg__min, typename arg__end_to_end_delay, typename arg__max, typename arg__end_to_end_path, Pos arg__sensor_k_th_period>
        struct end_to_end_delay_is_between_min_and_max_delays {
          static_assert(arg__is_between_min_and_max_delays,
                        "Min and max end-to-end delays are not respected");
        };

        template<bool arg__is_Node, Pos arg__correlation_pos, typename arg__actual_parameter>
        struct correlation_arg_1_is_Node {
          static_assert(arg__is_Node,
                        "Arg 1 is not class template tice::v1::Node");
        };

        template<bool arg__is_Comp, Pos arg__correlation_pos, typename arg__actual_parameter>
        struct correlation_arg_1_arg_1_is_Comp {
          static_assert(arg__is_Comp,
                        "Node's computation is not specified using macro Comp in tice/v1.hpp");
        };

        template<bool arg__is_function_pointer, Pos arg__correlation_pos, typename arg__actual_parameter>
        struct correlation_arg_1_arg_1_arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Node's computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, Pos arg__correlation_pos, typename arg__actual_parameter>
        struct correlation_arg_1_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's computation WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__correlation_pos, typename arg__actual_parameter>
        struct correlation_arg_1_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's computation WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, Pos arg__correlation_pos, typename arg__wcet>
        struct correlation_arg_1_arg_1_arg_2_is_positive {
          static_assert(arg__is_positive,
                        "Node's computation WCET is not positive");
        };

        template<bool arg__is_Ratio, Pos arg__correlation_pos, typename arg__actual_parameter>
        struct correlation_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's period is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__correlation_pos, typename arg__actual_parameter>
        struct correlation_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's period is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_greater_than_or_equal, Pos arg__correlation_pos, typename arg__period, typename arg__wcet>
        struct correlation_arg_1_arg_2_is_greater_than_or_equal_to_correlation_arg_1_arg_1_arg_2 {
          static_assert(arg__is_greater_than_or_equal,
                        "Node's period is less than its computation WCET");
        };

        template<bool arg__is_actuator_node, Pos arg__correlation_pos, typename arg__node_type>
        struct correlation_arg_1_is_actuator_node {
          static_assert(arg__is_actuator_node,
                        "Arg 1 is not actuator node");
        };

        template<bool arg__is_Ratio, Pos arg__correlation_pos, typename arg__actual_parameter>
        struct correlation_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Correlation threshold is not class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__correlation_pos, typename arg__actual_parameter>
        struct correlation_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Correlation threshold is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_nonnegative, Pos arg__correlation_pos, typename arg__actual_parameter>
        struct correlation_arg_2_is_nonnegative {
          static_assert(arg__is_nonnegative,
                        "Correlation threshold is not nonnegative");
        };

        template<bool arg__is_Node, Pos arg__correlation_pos, Pos arg__pos, typename arg__actual_parameter>
        struct correlation_remaining_arg_is_Node {
          static_assert(arg__is_Node,
                        "Arg is not class template tice::v1::Node");
        };

        template<bool arg__is_Comp, Pos arg__correlation_pos, Pos arg__pos, typename arg__actual_parameter>
        struct correlation_node_arg_1_is_Comp {
          static_assert(arg__is_Comp,
                        "Node's computation is not specified using macro Comp in tice/v1.hpp");
        };

        template<bool arg__is_function_pointer, Pos arg__correlation_pos, Pos arg__pos, typename arg__actual_parameter>
        struct correlation_node_arg_1_arg_1_is_function_pointer {
          static_assert(arg__is_function_pointer,
                        "Node's computation is not passed as a function pointer");
        };

        template<bool arg__is_Ratio, Pos arg__correlation_pos, Pos arg__pos, typename arg__actual_parameter>
        struct correlation_node_arg_1_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's computation WCET is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__correlation_pos, Pos arg__pos, typename arg__actual_parameter>
        struct correlation_node_arg_1_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's computation WCET is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_positive, Pos arg__correlation_pos, Pos arg__pos, typename arg__wcet>
        struct correlation_node_arg_1_arg_2_is_positive {
          static_assert(arg__is_positive,
                        "Node's computation WCET is not positive");
        };

        template<bool arg__is_Ratio, Pos arg__correlation_pos, Pos arg__pos, typename arg__actual_parameter>
        struct correlation_node_arg_2_is_Ratio {
          static_assert(arg__is_Ratio,
                        "Node's period is not specified using class template tice::v1::Ratio");
        };

        template<bool arg__has_nonzero_denominator, Pos arg__correlation_pos, Pos arg__pos, typename arg__actual_parameter>
        struct correlation_node_arg_2_has_nonzero_denominator {
          static_assert(arg__has_nonzero_denominator,
                        "Node's period is not tice::v1::Ratio instance with nonzero denominator");
        };

        template<bool arg__is_greater_than_or_equal, Pos arg__correlation_pos, Pos arg__pos, typename arg__period, typename arg__wcet>
        struct correlation_node_arg_2_is_greater_than_or_equal_to_correlation_node_arg_1_arg_2 {
          static_assert(arg__is_greater_than_or_equal,
                        "Node's period is less than its computation WCET");
        };

        template<bool arg__is_sensor_node, Pos arg__correlation_pos, Pos arg__pos, typename arg__node_type>
        struct correlation_remaining_arg_is_sensor_node {
          static_assert(arg__is_sensor_node,
                        "Arg is not sensor node");
        };

        template<bool arg__are_distinct, Pos arg__correlation_pos, Pos arg__node_1_pos, Pos arg__node_2_pos>
        struct correlation_nodes_are_distinct {
          static_assert(arg__are_distinct,
                        "Duplicated nodes are not allowed");
        };

        template<bool arg__is_within_threshold, Pos arg__constraint_pos, typename arg__correlation, typename arg__threshold,
                 typename arg__confluent_path_1, typename arg__confluent_path_2, Pos arg__confluent_node_k_th_period>
        struct correlation_is_within_threshold {
          static_assert(arg__is_within_threshold,
                        "Correlation threshold is not respected");
        };

        template<bool arg__is_available, typename arg__HW__hw_desc>
        struct backend_is_available {
          static_assert(arg__is_available,
                        "Backend to realize the expressed Tice model is unavailable for the given hardware description");
        };

        template<bool arg__is_successful, typename arg__total_utilization, typename arg__total_utilization_bound,
                 typename arg__max_utilization, typename arg__max_utilization_bound>
        struct gedf_schedulability_test_is_successful {
          static_assert(arg__is_successful,
                        "gEDF (global earliest-deadline first) backend cannot realize the expressed Tice model because the total processor"
                        " utilization is greater than its bound and/or the processor utilization of some task exceeds the maximum bound"
                        " (Try reducing the WCETs of the function blocks first before redesigning the Tice model with"
                        " greater periods and/or less nodes)");
        };

      }

    }
    //\end{compile-time error messages}

  }
}

#include "internals/v1/v1_internals.hpp"

#endif // TICE_V1_HPP
