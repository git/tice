;;; tice-template-pattern.el --- Automate edits of Tice template patterns

;; Copyright (C) 2019  Tadeus Prastowo (GPG public key ID: 0x277B48A6)

;; Author: Tadeus Prastowo <0x66726565@gmail.com>
;; Version: 1.3
;; Keywords: convenience, tools
;; Homepage: https://savannah.nongnu.org/projects/tice

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package allows the efficient edits of Tice template patterns,
;; which aid C++ template metaprogramming.  The full description of
;; Tice template patterns is available in section
;; "[Internal API Unit Namespace and Template Pattern Facility]" of file
;; "HACKING" that resides in the Tice codebase that can be found at the
;; homepage of this package.  To use this package, `point' must be within
;; an internal API unit namespace that has activated the template pattern
;; facility as explained in the file "HACKING".
;;
;; To start a new template pattern, place `point' at the beginning of an empty
;; line and type `M-x tice-start RET'.  Complete the body of the definition as
;; explained in the function description (`C-h f tice-start RET'), and type
;; `M-x tice-end RET' to complete the definition and automatically start the
;; corresponding definition of "decl" (to prevent the automatic definition of
;; "decl", type `C-u M-x tice-end RET' instead; later on, to manually define
;; "decl", type `M-x tice-m-d RET' when `point' is at the beginning of an
;; empty line following a "base" definition).  Note that the variable
;; `tice-template-pattern-base-opts-parameter-pack-identifier-regex' can be
;; customized to let the resulting "decl" definition automatically account for
;; a template parameter pack.
;;
;; In completing the "decl" definition, if `point' is placed at the first
;; character of either "prms" or "decl" in the argument list and
;; `M-x tice-tog RET' is typed, "prms" (or "decl") will be replaced with its
;; associated "decl" (or "prms"), and the parameter list of the "decl" being
;; defined will be updated accordingly.
;;
;; After the "decl" definition has been completed, type `M-x tice-done' to
;; complete the definition of a single template pattern as explained in section
;; "[Template Pattern Definition]" of the file "HACKING" (alternatively,
;; instead of typing `M-x tice-done', place `point' at the beginning of an
;; empty line following a "decl" definition and type `M-x tice-m-a RET' to
;; define "args" and then place `point' at the beginning of an empty line
;; following the "args" definition and type `M-x tice-m-p RET' to define
;; "prms", both of which require no manual modification).
;;
;; To invoke either the "decl" or the "args" or the "prms" of a template
;; pattern, type either `M-x tice-u-d' or `M-x tice-u-a' or `M-x tice-u-p',
;; respectively.

;;; Code:

;;\begin{public interface}

(defgroup tice-template-pattern nil
  "Tice template pattern configurations."
  :prefix "tice-template-pattern-"
  :group 'convenience)

(defcustom tice-template-pattern-begin-marker "^#include[[:space:]]+\"v1_internals_begin.hpp\""
  "The regex to match the start of a template-patterns zone."
  :type 'regexp
  :safe 'booleanp
  :package-version '(tice-template-pattern . "1.2"))

(defcustom tice-template-pattern-end-marker "^#include[[:space:]]+\"v1_internals_end.hpp\""
  "The regex to match the end of a template-patterns zone."
  :type 'regexp
  :safe 'booleanp
  :package-version '(tice-template-pattern . "1.2"))

(defcustom tice-template-pattern-base-opts-parameter-pack-identifier-regex "args\\(?:__[[:alnum:]_]+\\|[^[:alnum:]_]*\\)"
  "If the regex matches the second argument of an invocation to \"opts\" in a \"base\" definition body,
`tice-template-pattern-make-decl' will treat the \"opts\" as a C++ template parameter pack and provide
a suitable definition of \"decl\"."
  :type 'regexp
  :safe 'booleanp
  :package-version '(tice-template-pattern . "1.2"))

;;;###autoload
(defun tice-template-pattern-start-base (suffix-sequence-sans-leading-underscore)
  "Given that `point' is at the beginning of an empty physical line, start the definition of \"base\" that, after its body is completed, must be
ended using `tice-template-pattern-end-base'.  When constructing the definition body, the first argument of any invocation to \"opts\" must be left
blank, while the third argument must be left blank only if it has a default (otherwise, the third argument must be \"()\").  When invoking
`tice-template-pattern-end-base', `point' must be placed immediately after the last character of the definition body."
  (interactive "MSuffix sequence sans leading underscore: ")
  (unless (tice-template-pattern--beginning-of-empty-physical-line-p)
    (error "Not at the beginning of an empty physical line"))
  (let ((suffix-sequence (concat "_" suffix-sequence-sans-leading-underscore)))
    (unless (string-match-p (tice-template-pattern--regex--suffix-sequence 'nonlocal nil t) suffix-sequence)
      (error "Invalid suffix sequence sans leading underscore `%s'" suffix-sequence-sans-leading-underscore))
    (insert "#define base" suffix-sequence "() \\\n")))

;;;###autoload
(defun tice-template-pattern-end-base (&optional sans-decl)
  "Complete the definition of \"base\" that has been started using `tice-template-pattern-start-base'.
The `point' must be placed immediately after the last character of the definition body.

If SANS-DECL is set to non-nil (interactively with a prefix arg), the corresponding \"decl\" will not be automatically defined."
  (interactive "P")
  (save-excursion
    (save-match-data
      (save-restriction
        (let* ((base-data (let ((end-pos (point))
                                (regex (tice-template-pattern--regex--start-base)))
                            (if (tice-template-pattern--search-backward regex)
                                (let ((start-pos (match-beginning 0)))
                                  (narrow-to-region start-pos end-pos)
                                  `(,start-pos ,(match-string 3) ,(match-beginning 1) ,(match-beginning 2)))
                              (error "Cannot find the incomplete definition of \"base\" using `%s'" regex))))
               (start-pos (nth 0 base-data))
               (has-I-p (null (nth 1 base-data)))
               (prmlst-start-pos (nth 2 base-data))
               (I-start-pos (nth 3 base-data))
               (opts-count 0)
               (prmlst "")
               (default-list ""))
          (while (tice-template-pattern--search-forward (tice-template-pattern--regex--base-body-entry))
            (let ((text (match-string 1)))
              (if text
                  (setq prmlst (concat prmlst ", " text))
                (setq opts-count (1+ opts-count)
                      prmlst (concat prmlst ", _" (int-to-string opts-count))))))
          (setq prmlst (; remove leading comma and space
                        substring prmlst 2))
          (unless (looking-at-p "[[:space:]]*$")
            (error "Definition body of \"base\" ends improperly with `%s'" (match-string 0)))
          (let ((format-string (concat "_%-" (int-to-string (length (int-to-string opts-count))) "d")))
            (while (tice-template-pattern--search-backward (tice-template-pattern--regex--base-body-entry-opts))
              (and (match-string 3)
                   (save-excursion
                     (save-match-data
                       (goto-char (match-beginning 3))
                       (delete-region (point) (match-end 3))
                       (let ((text (concat "_d" (int-to-string opts-count))))
                         (setq default-list (concat text ", " default-list))
                         (insert " " text)))))
              (save-excursion
                (save-match-data
                  (goto-char (match-beginning 1))
                  (delete-region (point) (match-end 1))
                  (insert (format format-string opts-count))))
              (setq opts-count (1- opts-count))))
          (if has-I-p
            (let ((I "I"))
              (setq prmlst (concat I ", " prmlst))
              (goto-char I-start-pos)
              (insert I)))
          (goto-char prmlst-start-pos)
          (insert default-list prmlst)
          (let ((end-pos (point-max)))
            (widen)
            (indent-region start-pos end-pos))))))
  (unless (consp sans-decl)
    (insert "\n")
    (tice-template-pattern-make-decl)))

;;;###autoload
(defun tice-template-pattern-done ()
  "Complete the definition of a template pattern that has been started using `tice-template-pattern-start-base' and partially completed using
`tice-template-pattern-end-base' when given no argument.  The `point' must be within a \"decl\" definition or its body."
  (interactive)
  (save-match-data
    (save-excursion
      (save-restriction
        (tice-template-pattern-narrow)
        (unless (looking-at (tice-template-pattern--regex--body 0))
          (while (and (tice-template-pattern--search-backward "^.") ; only fails at (point-min)
                      (not (looking-at (tice-template-pattern--regex--body 0))))))))
    (unless (and (match-end 0) (<= (point) (match-end 0)))
      (error "The point at %d is neither within a \"decl\" definition nor its body" (point)))
    (goto-char (match-end 0))
    (insert "\n")
    (tice-template-pattern-make-args)
    (insert "\n")
    (tice-template-pattern-make-prms)))

;;;###autoload
(defun tice-template-pattern-make-decl ()
  "Given a template pattern whose \"base\" has already been properly defined, define the corresponding \"decl\".
The `point' must be at the beginning of an empty physical line.

The variable `tice-template-pattern-base-opts-parameter-pack-identifier-regex' will be used to recognize which \"opts\" in the \"base\" definition
is a template parameter pack."
  (interactive)
  (unless (tice-template-pattern--beginning-of-empty-physical-line-p)
    (error "Not at the beginning of an empty physical line"))
  (let* ((initial-pos (point))
         (text-to-yank (save-excursion
                         (save-match-data
                           (save-restriction
                             (tice-template-pattern-narrow)
                             (let ((def-regex (tice-template-pattern--regex--def "base")))
                               (if (tice-template-pattern--search-backward def-regex)
                                   `(,(match-string 1) ,(match-string 2) ,(match-string 3) ,(match-string 4)
                                     ,(if (tice-template-pattern--search-forward
                                           (tice-template-pattern--regex--base-body-entry-opts
                                            nil t t tice-template-pattern-base-opts-parameter-pack-identifier-regex)
                                           initial-pos)
                                          `(,(match-string 1)
                                            ,(let* ((prm-ident (match-string 1))
                                                    (text (match-string 3))
                                                    (default-fn-seq (if (string-match (tice-template-pattern--regex--param-ident-seq) prm-ident)
                                                                        (match-string 1 prm-ident)
                                                                      (error (concat
                                                                              "This template pattern \"base\" definition body has an \"opts\" whose"
                                                                              " second argument is determined to be a parameter pack based on"
                                                                              " the variable"
                                                                              " `tice-template-pattern-base-opts-parameter-pack-identifier-regex'"
                                                                              " but whose first argument is not a parameter identifier")))))
                                               (if (string-match (tice-template-pattern--regex--default-function nil nil default-fn-seq) text)
                                                   (match-string 1 text)
                                                 (error (concat
                                                         "This template pattern \"base\" definition body has an \"opts\" whose"
                                                         " second argument is determined to be a parameter pack based on"
                                                         " the variable `tice-template-pattern-base-opts-parameter-pack-identifier-regex'"
                                                         " but whose third argument is not a default function identifier whose core part seq is"
                                                         " " default-fn-seq)))))))
                                 (error "Cannot find the definition of \"base\" of this template pattern by `%s'" def-regex)))))))
         (suffix-sequence (nth 0 text-to-yank))
         (has-I-p (not (string= "" (nth 2 text-to-yank))))
         (default-ident-list (let ((text (nth 1 text-to-yank)))
                               (if (string= "" text)
                                   text
                                 (concat text " "))))
         (param-list (nth 3 text-to-yank))
         (pack-param-ident (nth 4 text-to-yank))
         (base-param-list (concat (if pack-param-ident
                                      (tice-template-pattern--replace-all-matches (concat (nth 1 pack-param-ident)
                                                                                          (tice-template-pattern--regex--ppspc-comma-ppspc 0 0))
                                                                                  ""
                                                                                  default-ident-list)
                                    default-ident-list)
                                "I, "
                                param-list))
         (base-arg-list (tice-template-pattern--replace-all-matches
                         (concat "\n"
                                 (tice-template-pattern--regex--group (tice-template-pattern--regex--param-ident-seq t)))
                         "\ntype(\\1, )"
                         (let ((default-ident-list-with-pack-marker (if pack-param-ident
                                                                        (tice-template-pattern--replace-all-matches (nth 1 pack-param-ident)
                                                                                                                    "()" default-ident-list)
                                                                      default-ident-list)))
                           (tice-template-pattern--replace-all-matches ",[[:space:]]*\\([^\\][^\n]\\)"
                                                                       ",\\\\\n\\1"
                                                                       (concat default-ident-list-with-pack-marker
                                                                               (if has-I-p "I##I, ")
                                                                               param-list)
                                                                       nil
                                                                       (let ((default-ident-list-length
                                                                               (length default-ident-list-with-pack-marker)))
                                                                         (if (= default-ident-list-length 0)
                                                                             0
                                                                           (- default-ident-list-length
                                                                              (if has-I-p
                                                                                  0
                                                                                2))))))))
         (base-arg-list-with-pack-marker (if pack-param-ident
                                             (tice-template-pattern--replace-all-matches (format "type(%s, \\(\\))" (nth 0 pack-param-ident))
                                                                                         "typename..." base-arg-list 1)
                                           base-arg-list)))
    (insert "#define decl" suffix-sequence "(" base-param-list ")\\\n"
            "base" suffix-sequence "(" base-arg-list-with-pack-marker ")")
    (save-excursion
      (save-match-data
        (save-restriction
          (narrow-to-region initial-pos (point))
          (while (tice-template-pattern--search-backward "args")
            (tice-template-pattern--args-to-prms)))))
    (indent-region initial-pos (point))))

;;;###autoload
(defun tice-template-pattern-make-args ()
  "Given a template pattern whose \"decl\" has already been properly defined, define the corresponding \"args\" and return nil.
The `point' must be at the beginning of an empty physical line.  Upon return, the `point' is at the position immediately after the last character in
the definition body."
  (interactive)
  (unless (tice-template-pattern--beginning-of-empty-physical-line-p)
    (error "Not at the beginning of an empty physical line"))
  (let* ((text-to-yank (save-excursion
                         (save-match-data
                           (save-restriction
                             (tice-template-pattern-narrow)
                             (let ((def-regex (tice-template-pattern--regex--def "decl" t t)))
                               (if (tice-template-pattern--search-backward def-regex)
                                   `(,(match-string 1) ,(match-string 2) ,(tice-template-pattern--make-base-arglst))
                                 (error "Cannot find the definition of \"decl\" of this template pattern by `%s'" def-regex)))))))
         (suffix-sequence (nth 0 text-to-yank))
         (param-ident-list (nth 1 text-to-yank))
         (base-arg-list (nth 2 text-to-yank)))
    (insert "#define args" suffix-sequence "(I, " param-ident-list ") base" suffix-sequence "(" base-arg-list ")")))

;;;###autoload
(defun tice-template-pattern-make-prms ()
  "Given a template pattern whose \"decl\" has already been properly defined, define the corresponding \"prms\" and return nil.
The `point' must be at the beginning of an empty physical line.  Upon return, the `point' is at the position immediately after the last character in
the definition body."
  (interactive)
  (unless (tice-template-pattern--beginning-of-empty-physical-line-p)
    (error "Not at the beginning of an empty physical line"))
  (let* ((text-to-yank (save-excursion
                         (save-match-data
                           (save-restriction
                             (tice-template-pattern-narrow)
                             (let ((def-regex (tice-template-pattern--regex--def "decl" nil t t)))
                               (if (tice-template-pattern--search-backward def-regex)
                                   `(,(match-string 1)
                                     ,(tice-template-pattern--replace-all-matches
                                       (tice-template-pattern--regex--default-function) "()" (match-string 2) 1))
                                 (error "Cannot find the definition of \"decl\" of this template pattern by `%s'" def-regex)))))))
         (suffix-sequence (nth 0 text-to-yank))
         (decl-default-suppressed-list (nth 1 text-to-yank)))
    (insert "#define prms" suffix-sequence "(...) decl" suffix-sequence "("
            decl-default-suppressed-list (if (string= "" decl-default-suppressed-list) "" " ") "__VA_ARGS__)")))

;;;###autoload
(defun tice-template-pattern-toggle ()
  "If `point' is at the first character of the string \"decl\"/\"prms\" that is an argument to an invocation to \"base\" that comprises the body of
the definition of \"decl\", replace \"decl\"/\"prms\" with an invocation to its \"prms\"/\"decl\" and adjust the parameter list of \"decl\"
accordingly."
  (interactive)
  (save-excursion
    (save-match-data
      (save-restriction
        (tice-template-pattern-narrow)
        (let* ((invocation-pos-begin (point))
               (invocation-data (if (looking-at (tice-template-pattern--regex--base-arglst-parent-invocation t))
                                    `(,(match-string 1) ,(match-string 2) (,(match-beginning 3) ,(match-end 3)) ,(match-end 0))
                                  (error "Point is not at the first character of either \"decl\"/\"prms\"")))
               (invocation-name (nth 0 invocation-data))
               (invocation-suffix-sequence (nth 1 invocation-data))
               (invocation-default-list-pos-begin (nth 0 (nth 2 invocation-data)))
               (invocation-default-list-pos-end (nth 1 (nth 2 invocation-data)))
               (invocation-pos-end (nth 3 invocation-data))
               (decl-data (if (tice-template-pattern--search-backward
                               (tice-template-pattern--regex--def "decl" nil t t))
                              `(,(match-beginning 0) ,(match-string 1) ,(match-beginning 2) ,(match-end 2))
                            (error "Cannot find the definition of \"decl\" whose body invokes \"base\" supplying \"%s%s\" as one of its arguments"
                                   invocation-name invocation-suffix-sequence)))
               (decl-pos-begin (nth 0 decl-data))
               (decl-suffix-sequence (nth 1 decl-data))
               (decl-default-list-pos-begin (nth 2 decl-data))
               (decl-default-list-pos-end (nth 3 decl-data)))
          (let ((replace-invocation-name #'(lambda (begin text)
                                             (save-excursion
                                               (save-match-data
                                                 (goto-char begin)
                                                 (delete-region begin (+ begin 4))
                                                 (insert text))))))
            (tice-template-pattern--assert-decl-invokes-base-supplying-this-argument decl-pos-begin decl-suffix-sequence
                                                                                     invocation-name invocation-suffix-sequence invocation-pos-end
                                                                                     t)
            (narrow-to-region decl-pos-begin (match-end 0))
            (if (string= invocation-name "prms")
                (progn
                  (funcall replace-invocation-name invocation-pos-begin "decl")
                  (tice-template-pattern--prms-to-decl invocation-pos-begin
                                                       invocation-suffix-sequence
                                                       invocation-default-list-pos-begin
                                                       invocation-default-list-pos-end
                                                       decl-default-list-pos-begin
                                                       decl-default-list-pos-end
                                                       (match-beginning 2)
                                                       (match-end 2)
                                                       (match-beginning 4)
                                                       (match-end 4)))
              (funcall replace-invocation-name invocation-pos-begin "prms")
              (tice-template-pattern--decl-to-prms invocation-default-list-pos-begin
                                                   invocation-default-list-pos-end
                                                   decl-default-list-pos-begin
                                                   decl-default-list-pos-end))
            (let ((decl-pos-end (point-max)))
              (widen)
              (indent-region decl-pos-begin decl-pos-end))))))))

;;;###autoload
(defun tice-template-pattern-use-decl (suffix-sequence-sans-leading-underscore)
  "Insert at `point' an invocation to \"decl_SUFFIX-SEQUENCE-SANS-LEADING-UNDERSCORE\"."
  (interactive "MSuffix sequence sans leading underscore: ")
  (tice-template-pattern--use "decl" suffix-sequence-sans-leading-underscore))

;;;###autoload
(defun tice-template-pattern-use-args (suffix-sequence-sans-leading-underscore)
  "Insert at `point' an invocation to \"args_SUFFIX-SEQUENCE-SANS-LEADING-UNDERSCORE\"."
  (interactive "MSuffix sequence sans leading underscore: ")
  (tice-template-pattern--use "args" suffix-sequence-sans-leading-underscore))

;;;###autoload
(defun tice-template-pattern-use-prms (suffix-sequence-sans-leading-underscore)
  "Insert at `point' an invocation to \"prms_SUFFIX-SEQUENCE-SANS-LEADING-UNDERSCORE\"."
  (interactive "MSuffix sequence sans leading underscore: ")
  (tice-template-pattern--use "prms" suffix-sequence-sans-leading-underscore))

;;\end{public interface}

;;;; The internals

;;\begin{utility}

(defun tice-template-pattern--beginning-of-empty-physical-line-p ()
  "Return non-nil if `point' is at the beginning of an empty physical line when no narrowing is in effect; otherwise, return nil."
  (save-restriction
    (widen)
    (looking-at-p "^$")))

(defun tice-template-pattern--search-forward (regex &optional bound)
  "Search for REGEX in the region that starts at `point' and ends at BOUND.
If the regular expression is not found, return nil with no side effect.  Otherwise, move point to the position immediately after the last character
of the matched text, change the `match-data', and return the new point."
  (re-search-forward regex bound t))

(defun tice-template-pattern--search-backward (regex &optional bound)
  "Search backwards for REGEX in the regions that starts at the position immediately preceding `point' and ends at BOUND.
If the regular expression is not found, return nil with no side effect.  Otherwise, move point to the first character of the matched text, change
the `match-data', and return the new point."
  (re-search-backward regex bound t))

(defun tice-template-pattern--replace-all-matches (regex newtext string &optional subexp start literal fixedcase)
  "Combine `string-match' and `replace-match' in an iteration to work on STRING so that all REGEX matches are replaced with NEWTEXT."
  (let ((string-with-one-more-match-to-replace string))
    (save-match-data
      (while (not (null (string-match regex string-with-one-more-match-to-replace start)))
        (setq string-with-one-more-match-to-replace (replace-match newtext fixedcase literal string-with-one-more-match-to-replace subexp))))
    string-with-one-more-match-to-replace))

;;;###autoload
(defun tice-template-pattern-narrow ()
  "Narrow to the region between `tice-template-pattern-begin-marker' and `tice-template-pattern-end-marker' lines, inclusive."
  (interactive)
  (widen)
  (save-excursion
    (let ((begin-pos (tice-template-pattern--search-backward tice-template-pattern-begin-marker))
          (end-pos (tice-template-pattern--search-forward tice-template-pattern-end-marker)))
      (if (and begin-pos end-pos)
          (narrow-to-region begin-pos end-pos)
        (error "Cannot find either Tice template pattern begin marker `%s' or end marker `%s', or both"
               tice-template-pattern-begin-marker tice-template-pattern-end-marker)))))

;;\end{utility}

;;\begin{regex}

;;\begin{translation region 1A: update-template-pattern.cpp}

(defun tice-template-pattern--regex--alt (&rest regexps)
  "Return a string that results from the concatenation of non-nil elements in REGEXPS with separator string \"\\|\".
REGEXPS is expected to consist of a number of strings and/or nil.
If REGEXPS is nil or all of its elements are nil, return an empty string.
If REGEXPS has only one non-nil element, return that element."
  (let ((result "")
        (i 0)
        (regexps-count (length regexps)))
    (unless (= regexps-count 0)
      (while (and (< i regexps-count)
                  (null (nth i regexps)))
        (setq i (1+ i)))
      (if (< i regexps-count)
          (setq result (nth i regexps)))
      (setq i (1+ i))
      (while (< i regexps-count)
        (let ((text (nth i regexps)))
          (if text
              (setq result (concat result "\\|" text))))
        (setq i (1+ i))))
    result))

(defun tice-template-pattern--regex--group (regex &optional not-captured)
  (concat "\\(" (if not-captured "?:" "") regex "\\)"))

;;\end{translation region 1A: update-template-pattern.cpp}

(defconst tice-template-pattern--regex--line-continuation
  "[\\]\n"
  "The logical line continuation token of the C/C++ preprocessor.")

(defconst tice-template-pattern--regex--ppspc
  (tice-template-pattern--regex--group (tice-template-pattern--regex--alt "[[:space:]]"
                                                                          tice-template-pattern--regex--line-continuation)
                                       t)
  "A space as understood by a C/C++ preprocessor.")

(defun tice-template-pattern--regex--ppspc (minimum-cardinality)
  (format "%s%s" tice-template-pattern--regex--ppspc (cond ((= 0 minimum-cardinality) "*")
                                                           ((= 1 minimum-cardinality) "+")
                                                           (t (format "\\{%d,\\}" minimum-cardinality)))))

(defun tice-template-pattern--regex--ppspc-comma (ppspc-minimum-cardinality)
  (format "%s," (tice-template-pattern--regex--ppspc ppspc-minimum-cardinality)))

(defun tice-template-pattern--regex--comma-ppspc (ppspc-minimum-cardinality)
  (format ",%s" (tice-template-pattern--regex--ppspc ppspc-minimum-cardinality)))

(defun tice-template-pattern--regex--ppspc-comma-ppspc (lhs-ppspc-minimum-cardinality rhs-ppspc-minimum-cardinality)
  (format "%s,%s"
          (tice-template-pattern--regex--ppspc lhs-ppspc-minimum-cardinality)
          (tice-template-pattern--regex--ppspc rhs-ppspc-minimum-cardinality)))

(defun tice-template-pattern--regex--comma-separated-list (regex &optional not-captured)
  "Return a regular expression that matches a non-empty comma-separated list of REGEX.
By default, there is one capture group that captures the non-empty comma-separated list as a whole.  If NOT-CAPTURED is set to non-nil, there is no
capture group."
  (tice-template-pattern--regex--group (concat regex
                                               (tice-template-pattern--regex--group (concat (tice-template-pattern--regex--ppspc-comma-ppspc 0 0)
                                                                                            regex)
                                                                                    t) "*")
                                       not-captured))

(defun tice-template-pattern--regex--c-ident-first-char (&optional not-matched)
  (concat "[" (if not-matched "^" "") "_[:alpha:]]"))

;;\begin{translation region 2A: update-template-pattern.cpp}

(defun tice-template-pattern--regex--c-ident-next-char (&optional not-matched)
  (concat "[" (if not-matched "^" "") "_[:alnum:]]"))

(defconst tice-template-pattern--regex--quasi-bol
  "^[[:space:]]*")

(defconst tice-template-pattern--regex--def-begin
  "^#define[[:space:]]+"
  "A regular expression that matches the definition of a template pattern function-like macro.
Note that section [Template Pattern Definition] of HACKING guarantees that the C preprocessing directive and the macro identifier will be in a
single physical line (a single logical line, instead, can consist of two or more physical lines terminated with a backslash).")

(defconst tice-template-pattern--regex--suffix-sequence-part-seq
  "[[:digit:]]+"
  "The regular expression that matches the sequence subpart of a nonlocal or a local part of a suffix sequence.
The sequence subpart encodes the depth of either the nonlocal or the local template pattern inheritance.")

(defconst tice-template-pattern--regex--suffix-sequence-part-str
  "[[:alpha:]]*"
  "The regular expression that matches the string subpart of a nonlocal or a local part of a suffix sequence.
The string subpart encodes the branch of either the nonlocal or the local template pattern inheritance.")

(defun tice-template-pattern--regex--suffix-sequence-part (&optional seq-not-captured str-not-captured seq-subpart)
  "Return a regular expression that matches either a nonlocal or a local part of a suffix sequence but not the whole suffix sequence.
The part consists of a sequence (`tice-template-pattern--regex--suffix-sequence-part-seq') and a
string (`tice-template-pattern--regex--suffix-sequence-part-str') subparts.  If SEQ-SUBPART is a positive integer, the returned regular expression
only matches the suffix sequence part whose sequence subpart is the given integer.  By default, there are two capture groups that capture the
sequence and the string subparts as the first and the second groups, respectively.  Set either SEQ-NOT-CAPTURED or STR-NOT-CAPTURED, or both, to
non-nil to omit either or both of the capture groups."
  (unless (or (null seq-subpart)
              (and (integerp seq-subpart)
                   (> seq-subpart 0)))
    (error "SEQ-SUBPART is not a positive integer"))
  (tice-template-pattern--regex--group (concat "_"
                                               (tice-template-pattern--regex--group (if seq-subpart
                                                                                        (int-to-string seq-subpart)
                                                                                      tice-template-pattern--regex--suffix-sequence-part-seq)
                                                                                    seq-not-captured)
                                               (tice-template-pattern--regex--group tice-template-pattern--regex--suffix-sequence-part-str
                                                                                    str-not-captured))
                                       t))

(defun tice-template-pattern--regex--suffix-sequence (&optional mode not-captured disable-capture seq-subpart)
  "Return a regular expression that matches a suffix sequence as a whole, which consists of a nonlocal and an optional local parts
(each of which is formalized by `tice-template-pattern--regex--suffix-sequence-part'), unless MODE is set to either the symbol 'nonlocal, in which
case the local part must be absent, or the symbol 'local, in which case the local part must be present.

By default, SEQ-SUBPART must be nil, and there is only one capture group that captures the suffix sequence as a whole.  If NOT-CAPTURED is set to
non-nil, there are two groups if MODE is 'nonlocal; otherwise, there are four groups.  The first two groups capture the sequence and string subparts
of the nonlocal part, respectively, while the last two groups capture the sequence and string subparts of the local part, respectively.  The last
two groups are nil if the optional local part is not present, which is never the case if MODE is 'local.  If DISABLE-CAPTURE is set to non-nil,
there is no capture group.

If MODE is either 'nonlocal or 'local, SEQ-SUBPART can be set to a positive integer so that the regular expression only matches a suffix sequence
whose nonlocal part if MODE is 'nonlocal or local part if MODE is 'local has the integer as the sequence subpart."
  (unless (or (null mode) (eq mode 'nonlocal) (eq mode 'local))
    (error "MODE is non-nil but is neither 'nonlocal nor 'local"))
  (if (and (null mode) seq-subpart)
      (error "SEQ-SUBPART can only be non-nil if MODE is either 'nonlocal or 'local"))
  (tice-template-pattern--regex--group (concat (tice-template-pattern--regex--suffix-sequence-part (or disable-capture
                                                                                                       (not not-captured))
                                                                                                   (or disable-capture
                                                                                                       (not not-captured))
                                                                                                   (if (eq mode 'nonlocal)
                                                                                                       seq-subpart))                 ; nonlocal
                                               (unless (eq mode 'nonlocal)
                                                 (concat (tice-template-pattern--regex--suffix-sequence-part (or disable-capture
                                                                                                                 (not not-captured))
                                                                                                             (or disable-capture
                                                                                                                 (not not-captured))
                                                                                                             (if (eq mode 'local)
                                                                                                                 seq-subpart))       ; local
                                                         (unless (eq mode 'local) "?"))))
                                       (or disable-capture not-captured)))

(defun tice-template-pattern--regex--default-function-seq (&optional seq-not-captured seq)
  "Return a regular expression that matches the core identifier of a default function.
By default, there is one capture group that captures the sequence subpart of the core identifier, which is formalized by
`tice-template-pattern--regex--suffix-sequence-part-seq'.  If SEQ-NOT-CAPTURED is set to non-nil, there is no capture group.
If SEQ is set to non-nil, the returned regular expression only matches a core identifier whose sequence subpart is SEQ."
  (let ((regex (concat "^" tice-template-pattern--regex--suffix-sequence-part-seq "$")))
    (if (and seq
             (not (string-match-p regex seq)))
        (error "`%s' is not the sequence subpart of the core identifier of a default function based on the regex `%s'" seq regex)))
  (tice-template-pattern--regex--group (concat "_d"
                                               (tice-template-pattern--regex--group (or seq
                                                                                        tice-template-pattern--regex--suffix-sequence-part-seq)
                                                                                    seq-not-captured))
   t))

(defun tice-template-pattern--regex--default-function (&optional not-captured disable-capture core-part-seq require-noncore-part)
  "Return a regular expression that matches the identifier of a default function as a whole,
which consists of a noncore part (`tice-template-pattern--regex--suffix-sequence-part') that is optional if REQUIRE-NONCORE-PART is nil and a core
part (`tice-template-pattern--regex--default-function-seq').  By default, there is only one capture group that captures the identifier as a whole.
If NOT-CAPTURED is set to non-nil, there are three groups, the last of which is never nil and captures the sequence subpart of the core identifier.
The first two groups are nil if the noncore part does not exist; otherwise, the first two groups capture the sequence and the string subparts of the
noncore part, respectively.  If DISABLE-CAPTURE is set to non-nil, there is no capture group.  If CORE-PART-SEQ is set to non-nil, the returned
regular expression only matches a default function identifier that has no noncore part but only the specified CORE-PART-SEQ; there is also only one
capture group that captures the given CORE-PART-SEQ if NOT-CAPTURED and DISABLE-CAPTURE are both non-nil."
  (if core-part-seq
      (concat (tice-template-pattern--regex--group (tice-template-pattern--regex--alt
                                                    "^"
                                                    (tice-template-pattern--regex--c-ident-next-char t))
                                                   t)
              (tice-template-pattern--regex--group
               (tice-template-pattern--regex--default-function-seq (or disable-capture (not not-captured)) core-part-seq)
               (or disable-capture not-captured)))
    (tice-template-pattern--regex--group
     (concat (tice-template-pattern--regex--suffix-sequence-part (or disable-capture
                                                                     (not not-captured))
                                                                 (or disable-capture
                                                                     (not not-captured))) (if require-noncore-part "" "?") ; nonlocal
             (tice-template-pattern--regex--default-function-seq (or disable-capture (not not-captured))))
     (or disable-capture not-captured))))

(defconst tice-template-pattern--regex--template-begin
  (concat (tice-template-pattern--regex--group "^|[^[:alnum:]_]"
                                               t)
          "template[[:space:]]*<[[:space:]]*")
  "A regular expression that matches the beginning of an associated template declaration.
Note that section [Template Pattern Usage] of HACKING guarantees that `tice-template-pattern--regex--template-begin' and the following invocation of
\"decl\"/\"prms\" will be in a single physical line (a single logical line, instead, can consist of two or more physical lines terminated with a
backslash).")

(defconst tice-template-pattern--regex--prmlst-begin
  "[[:space:]]*("
  "A regular expression that matches the beginning of a parameter list.
Note that section [Template Pattern Definition] of HACKING guarantees that whatever precedes `tice-template-pattern--regex--prmlst-begin' will be in
a single physical line (a single logical line, instead, can consist of two or more physical lines terminated with a backslash).")

;;\end{translation region 2A: update-template-pattern.cpp}

(defconst tice-template-pattern--regex--prmlst-end
  (concat (tice-template-pattern--regex--ppspc 0) ")")
  "A regular expression that matches the end of a parameter list.
Note that section [Template Pattern Definition] of HACKING gives no guarantee that whatever follows `tice-template-pattern--regex--prmlst-end' will
be in a single physical line (a single logical line, instead, can consist of two or more physical lines terminated with a backslash).")

(defun tice-template-pattern--regex--zero-or-more-physical-lines (&optional end-regex not-captured end-regex-captured-p)
  "Return a regular expression that matches zero or more backslash-terminated physical lines and the beginning of the following
non-backslash-terminated physical line.  If END-REGEX is set to non-nil, the returned regular expression matches not only the beginning of the
non-backslash-terminated physical line but also everything up to and including the part matched by END-REGEX.

By default, there is one capture group that captures everything that is matched but without the part that is matched by END-REGEX.  If
END-REGEX-CAPTURED-P is set to non-nil, however, the capture group also captures the part that is matched by END-REGEX.  If NOT-CAPTURED is set to
non-nil, there is no capture group."
  (let ((end-regex-text (or end-regex
                            "")))
    (concat (; capture group open parenthesis 1
             tice-template-pattern--regex--group (concat (tice-template-pattern--regex--group (concat
                                                                                               ".*?"
                                                                                               tice-template-pattern--regex--line-continuation)
                                                                                              t) "*.*?" (if end-regex-captured-p end-regex-text))
                                                 not-captured)
            (unless end-regex-captured-p end-regex-text))))

(defun tice-template-pattern--regex--rest-of-arglst (&optional not-captured)
  "Return a regular expression that matches the remainder of an invocation's argument list across a number of backslash-terminated physical lines.

By default, there is one capture group that captures the all of the remainder without the closing parenthesis of the argument list.  If NOT-CAPTURED
is set to non-nil, there is no capture group."
  (; capture group open parenthesis 1
   tice-template-pattern--regex--zero-or-more-physical-lines (concat tice-template-pattern--regex--prmlst-end "[[:space:]]*$")
                                                             not-captured))

(defun tice-template-pattern--regex--param-ident-seq (&optional seq-not-captured)
  "Return a regular expression that matches the core identifier of a parameter.
By default, there is one capture group that captures the sequence subpart of the core identifier, which is formalized by
`tice-template-pattern--regex--suffix-sequence-part-seq'.  If SEQ-NOT-CAPTURED is set to non-nil, there is no capture group."
  (tice-template-pattern--regex--group (concat "_"
                                               (tice-template-pattern--regex--group tice-template-pattern--regex--suffix-sequence-part-seq
                                                                                    seq-not-captured))
                                       t))

(defun tice-template-pattern--regex--param-ident (&optional not-captured disable-capture)
  "Return a regular expression that matches the identifier of a parameter as a whole,
which consists of an optional noncore part (`tice-template-pattern--regex--suffix-sequence-part'), and a core
part (`tice-template-pattern--regex--param-ident-seq').  By default, there is only one capture group that captures the identifier as a whole.  If
NOT-CAPTURED is set to non-nil, there are three groups, the last of which is never nil and captures the sequence subpart of the core identifier.
The first two capture groups are nil if the noncore part does not exist; otherwise, the first two capture groups capture the sequence and the string
subparts of the noncore part, respectively.  If DISABLE-CAPTURE is set to non-nil, there is no capture group."
  (tice-template-pattern--regex--group (concat (tice-template-pattern--regex--suffix-sequence-part (or disable-capture
                                                                                                       (not not-captured))
                                                                                                   (or disable-capture
                                                                                                       (not not-captured))) "?" ; nonlocal
                                               (tice-template-pattern--regex--param-ident-seq (or disable-capture (not not-captured))))
                                       (or disable-capture not-captured)))

(defun tice-template-pattern--regex--default-list (regex &optional not-captured)
  "Return a regular expression that matches the possibly-empty comma-suffixed list of REGEX; REGEX must have no capture group.
The matched list starts immediately after the opening parenthesis of the parameter list and ends immediately before either I or the comma following
the last comma of the matched list.  By default, there is one capture group that captures the matched list as a whole without leading and trailing
spaces.  If NOT-CAPTURED is set to non-nil, there is no capture group."
  (concat
   (tice-template-pattern--regex--ppspc 0)
   (; capture group open parenthesis 1
    tice-template-pattern--regex--group
    (tice-template-pattern--regex--alt (concat (tice-template-pattern--regex--comma-separated-list regex
                                                                                                   t)
                                               (tice-template-pattern--regex--ppspc-comma 0))
                                       "")
    not-captured)
   (tice-template-pattern--regex--ppspc 0)))

(defun tice-template-pattern--regex--param-list (regex &optional not-captured)
  "Return a regular expression that matches the non-empty comma-separated list of REGEX; REGEX must have no capture group.
The matched list starts immediately after the comma that follows I and ends at the last character matched by REGEX.  By default, there is one
capture group that captures the matched list without leading spaces.  If NOT-CAPTURED is set to non-nil, there is no capture group."
  (concat
   (tice-template-pattern--regex--ppspc 0)
   (; capture group open parenthesis 1
    tice-template-pattern--regex--comma-separated-list regex
                                                       not-captured)))

(defun tice-template-pattern--regex--prmlst-default-list (&optional not-captured)
  "Return a regular expression that matches the possibly-empty comma-suffixed list of default function identifiers in a \"decl\"/\"args\"/\"prms\"
parameter list.  The matched list starts immediately after the opening parenthesis of the parameter list and ends immediately before either I or the
comma following the last comma of the matched list.  By default, there is one capture group that captures the matched list as a whole without
leading and trailing spaces.  If NOT-CAPTURED is set to non-nil, there is no capture group."
  (; capture group open parenthesis 1
   tice-template-pattern--regex--default-list (tice-template-pattern--regex--default-function nil t)
                                              not-captured))

(defun tice-template-pattern--regex--prmlst-param-list (&optional not-captured base-param-list-p)
  "Return a regular expression that matches the non-empty comma-separated list of parameter identifiers in a \"decl\"/\"args\"/\"prms\" parameter
list or, if BASE-PARAM-LIST-P is set to non-nil, in a \"base\" parameter list, which may have parent \"arg\"s in addition to or in place of the
parameter identifiers.  The matched list starts immediately after the comma that follows I and ends at the last character of the last parameter
identifier.  By default, there is one capture group that captures the matched list without leading spaces.  If NOT-CAPTURED is set to non-nil, there
is no capture group."
  (; capture group open parenthesis 1
   tice-template-pattern--regex--param-list (tice-template-pattern--regex--group
                                             (tice-template-pattern--regex--alt (tice-template-pattern--regex--param-ident nil t)
                                                                                (if base-param-list-p
                                                                                    (concat "args"
                                                                                            (tice-template-pattern--regex--suffix-sequence 'nonlocal
                                                                                             nil t))))
                                             t)
                                            not-captured))

(defun tice-template-pattern--regex--prmlst (&optional base-prmlst-p default-not-captured I-not-captured param-not-captured)
  "Return a regular expression that matches the parameter list of a \"base\"/\"decl\"/\"args\"/\"prms\" definition.
For \"base\", BASE-PRMLST-P must be set to non-nil; otherwise, BASE-PRMLST-P must be set to nil.  By default, there are three capture groups that
capture as the first group the possibly-empty comma-suffixed list of default function identifiers, as the second group either I along with its
trailing comma or an empty string, and as the third group the non-empty comma-separated list of parameter identifiers.  Set either
DEFAULT-NOT-CAPTURED or I-NOT-CAPTURED or PARAM-NOT-CAPTURED to non-nil to omit any of the groups."
  (concat tice-template-pattern--regex--prmlst-begin
          (; capture group open parenthesis 1
           tice-template-pattern--regex--prmlst-default-list default-not-captured)
          (; capture group open parenthesis 2
           tice-template-pattern--regex--group (concat (tice-template-pattern--regex--group (concat "I"
                                                                                                    (tice-template-pattern--regex--ppspc-comma 0))
                                                                                            t) (if base-prmlst-p "?"))
                                               I-not-captured)
          (; capture group open parenthesis 3
           if base-prmlst-p
              (tice-template-pattern--regex--prmlst-param-list param-not-captured t)
            (tice-template-pattern--regex--prmlst-param-list param-not-captured))
          tice-template-pattern--regex--prmlst-end))

(defun tice-template-pattern--regex--local-def (name
                                                nonlocal-part
                                                &optional local-part-not-captured param-not-captured body-not-captured capture-nonlocal-part)
  "Return a regular expression that matches the local definition of NAME with NAME being either \"args\" or \"prms\" and whose nonlocal-part of the
suffix sequence is NONLOCAL-PART.  If NONLOCAL-PART is an integer instead of a string, the integer is assumed to count the number of capture-group
open-parentheses that precedes the returned regular expression; the integer must be non-negative and less than either nine if
LOCAL-PART-NOT-CAPTURED, PARAM-NOT-CAPTURED, and BODY-NOT-CAPTURED are set to non-nil, eight if only two of them are non-nil, seven if only one of
them is non-nil, and six otherwise.

By default, NONLOCAL-PART is assumed to be an integer, and there are four capture groups that captures as the first group the nonlocal part of the
suffix sequence, as the second group the local part of the suffix sequence, as the third group the non-empty comma-separated list of parameter
identifiers, which follows the comma that follows the \"I\" in the parameter list, without leading and trailing spaces, and as the fourth group the
non-empty comma-separated list of the arguments of the invocation to NAME that is suffixed with the nonlocal part of the suffix sequence; the
non-empty comma-separated list of the arguments also follows the comma that follows the \"I\" in the argument list, without leading and trailing
spaces.  To omit either the first or second or third or fourth group, set either NONLOCAL-PART to a string or LOCAL-PART-NOT-CAPTURED or
PARAM-NOT-CAPTURED or BODY-NOT-CAPTURED to non-nil, respectively.  If NONLOCAL-PART is a string and the first group is not to be omitted, set
CAPTURE-NONLOCAL-PART to non-nil."
  (unless (or (string= name "args") (string= name "prms"))
    (error "The NAME `%s' is neither \"args\" nor \"prms\"" name))
  (if (integerp nonlocal-part)
      (unless (and (<= 0 nonlocal-part) (<= nonlocal-part (+ 8
                                                             (if local-part-not-captured 0 -1)
                                                             (if param-not-captured 0 -1)
                                                             (if body-not-captured 0 -1))))
        (error "NONLOCAL-PART is either a negative integer or greater than the maximum that allows for all capture groups to be referenced"))
    (let ((regex (tice-template-pattern--regex--suffix-sequence-part t t)))
      (unless (and (stringp nonlocal-part)
                   (string-match-p regex nonlocal-part))
        (error "NONLOCAL-PART is neither an integer nor a string that matches the nonlocal part pattern `%s'" regex))))
  (concat tice-template-pattern--regex--def-begin
          name
          (; capture group open parenthesis 1
           if (stringp nonlocal-part)
              (tice-template-pattern--regex--group nonlocal-part
                                                   (not capture-nonlocal-part))
            (tice-template-pattern--regex--group (tice-template-pattern--regex--suffix-sequence-part t t)))
          (; capture group open parenthesis 2
           tice-template-pattern--regex--group (tice-template-pattern--regex--suffix-sequence-part t t)
                                               local-part-not-captured)
          tice-template-pattern--regex--prmlst-begin
          (tice-template-pattern--regex--ppspc 0)
          "I" (tice-template-pattern--regex--ppspc-comma 0)
          (; capture group open parenthesis 3
           tice-template-pattern--regex--prmlst-param-list param-not-captured)
          tice-template-pattern--regex--prmlst-end
          (tice-template-pattern--regex--ppspc 0)
          name
          (if (integerp nonlocal-part)
              (format "\\%d" (1+ nonlocal-part))
            nonlocal-part)
          tice-template-pattern--regex--prmlst-begin
          (tice-template-pattern--regex--ppspc 0)
          "I" (tice-template-pattern--regex--ppspc-comma-ppspc 0 0)
          (; capture group open parenthesis 4
           tice-template-pattern--regex--rest-of-arglst body-not-captured)))

(defun tice-template-pattern--regex--def (name
                                          &optional
                                          default-not-captured I-not-captured param-not-captured disable-capture
                                          nonlocal-suffix-sequence)
  "Return a regular expression that matches the nonlocal definition of NAME with NAME being either \"base\" or \"decl\" or \"args\" or \"prms\".
Set NAME to \"decl-base\" to match a nonlocal definition of \"decl\" whose parameter list is that of its \"base\".

If NONLOCAL-SUFFIX-SEQUENCE is non-nil, the nonlocal definition must have NONLOCAL-SUFFIX-SEQUENCE as the suffix sequence if
NONLOCAL-SUFFIX-SEQUENCE is a string, which can be a regular expression.  If NONLOCAL-SUFFIX-SEQUENCE is an integer, the nonlocal definition must
have a nonlocal suffix sequence whose sequence subpart is the integer.

By default, there is one capture group that captures as the first group the nonlocal suffix sequence, and if NAME is not \"prms\", there are three
additional capture groups that capture as the second group the possibly-empty comma-suffixed list of default function identifiers, as the third
group either I along with its trailing comma or an empty string, and as the fourth group the non-empty comma-separated list of parameter
identifiers.  Set either DEFAULT-NOT-CAPTURED or I-NOT-CAPTURED or PARAM-NOT-CAPTURED to non-nil to omit any of the second, third, and fourth
groups.  Set DISABLE-CAPTURE to non-nil to have no capture group."
  (unless (or (string= name "base") (string= name "decl-base") (string= name "decl") (string= name "args") (string= name "prms"))
    (error "The NAME `%s' is neither \"base\" nor \"decl-base\" nor \"decl\" nor \"args\" nor \"prms\"" name))
  (concat tice-template-pattern--regex--def-begin
          (substring name 0 4)
          (; capture group open parenthesis 1
           tice-template-pattern--regex--group (if nonlocal-suffix-sequence
                                                   (if (stringp nonlocal-suffix-sequence)
                                                       nonlocal-suffix-sequence
                                                     (if (integerp nonlocal-suffix-sequence)
                                                         (tice-template-pattern--regex--suffix-sequence 'nonlocal nil t nonlocal-suffix-sequence)
                                                       (error "NONLOCAL-SUFFIX-SEQUENCE is non-nil but neither a string nor an integer")))
                                                 (tice-template-pattern--regex--suffix-sequence 'nonlocal nil t))
                                               disable-capture)
          (if (string= name "prms")
              "(...)"
            (; capture group open parenthesis 2, 3, 4
             tice-template-pattern--regex--prmlst (or (string= name "base") (string= name "decl-base"))
                                                  (or disable-capture default-not-captured)
                                                  (or disable-capture I-not-captured)
                                                  (or disable-capture param-not-captured)))))

(defun tice-template-pattern--regex--base-arglst-default-list (&optional not-captured)
  "Return a regular expression that matches the possibly-empty comma-suffixed list of default function identifiers in the argument list of an
invocation to \"base\" or an invocation to \"decl\" that is made within the argument list of an invocation to \"base\".  The matched list starts
immediately after the opening parenthesis of the argument list and ends immediately before either I or the comma following the last comma of the
matched list.  By default, there is one capture group that captures the matched list as a whole without leading and trailing spaces.  If
NOT-CAPTURED is set to non-nil, there is no capture group."
  (; capture group open parenthesis 1
   tice-template-pattern--regex--default-list (tice-template-pattern--regex--group (tice-template-pattern--regex--alt
                                                                                    "()"
                                                                                    (tice-template-pattern--regex--default-function nil t))
                                                                                   t)
                                              not-captured))

(defun tice-template-pattern--regex--base-arglst-parent-invocation (&optional not-captured disable-capture name)
  "Return a regular expression that matches the invocation to either \"decl\" or \"prms\", or to NAME if it is non-nil, that is made within the
argument list of an invocation to \"base\".  The NAME must be either \"decl\" or \"prms\".  By default, there is one capture group that captures the
invocation as a whole.  If NOT-CAPTURED is set to non-nil, then there are five capture groups that captures as the first and second groups the
identifier of the invoked function-like macro without its nonlocal suffix sequence and the nonlocal suffix sequence itself, respectively, as the
third group the possibly-empty comma-suffixed list of default function identifiers, as the fourth group either \"I\" or an empty string, both of
which is suffixed with a comma, and as the fifth group the non-empty comma-separated list of parameter identifiers.  If DISABLE-CAPTURE is set to
non-nil, there is no capture group."
  (unless (or (null name) (string-match-p (tice-template-pattern--regex--alt "^decl$" "^prms$") name))
    (error "The given name `%s' is neither \"decl\" nor \"prms\"" name))
  (tice-template-pattern--regex--group
   (concat (; capture group open parenthesis 1
            tice-template-pattern--regex--group (or name
                                                    (tice-template-pattern--regex--alt "decl"
                                                                                       "prms"))
                                                (or disable-capture
                                                    (not not-captured)))
           (; capture group open parenthesis 2
            tice-template-pattern--regex--suffix-sequence 'nonlocal nil (or disable-capture
                                                                            (not not-captured)))
           tice-template-pattern--regex--prmlst-begin
           (; capture group open parenthesis 3
            tice-template-pattern--regex--base-arglst-default-list (or disable-capture
                                                                       (not not-captured)))
           (; capture group open parenthesis 4
            tice-template-pattern--regex--group (concat "I?" (tice-template-pattern--regex--ppspc-comma 0)))
           (; capture group open parenthesis 5
            tice-template-pattern--regex--param-list (tice-template-pattern--regex--param-ident nil t)
                                                     (or disable-capture
                                                         (not not-captured)))
           tice-template-pattern--regex--prmlst-end)
   (or disable-capture
       not-captured)))

(defun tice-template-pattern--regex--base-arglst-type-invocation (&optional not-captured)
  "Return a regular expression that matches the invocation to either \"type\" or \"typr\" that is made within the argument list of an invocation to
\"base\".  The regular expression ends at the first non-space character of the second argument of the invocation to either \"type\" or \"typr\", not
at the closing parenthesis that follows the second argument as a whole, because the second argument may use a number of matching-parenthesis pairs,
which cannot be expressed in a regex (such case can be expressed using a context-free grammar).  By default, there is one capture group that
captures the first argument of the invocation to either \"type\" or \"typr\" without leading and trailing spaces.  If NOT-CAPTURED is set to
non-nil, there is no capture group."
  (concat "typ[er]"
          tice-template-pattern--regex--prmlst-begin
          (tice-template-pattern--regex--ppspc 0)
          (; capture group open parenthesis 1
           tice-template-pattern--regex--group (tice-template-pattern--regex--param-ident-seq t)
                                               not-captured)
          (tice-template-pattern--regex--ppspc-comma-ppspc 0 0)))

(defun tice-template-pattern--regex--invocation (name suffix-sequence &optional localp)
  "Return a regular expression that matches the invocation to one of the followings:
1.  If NAME is \"base-decl\", a \"base\" that comprises the body of a \"decl\" definition.
2.  If NAME is \"decl-base\", a          \"decl\" that is one of the arguments to a \"base\" invocation made by a \"decl\" definition body.
3.  If NAME is \"prms-base\", a nonlocal \"prms\" that is one of the arguments to a \"base\" invocation made by a \"decl\" definition body.
4.  If NAME is \"args-base\", a nonlocal \"args\" that is one of the arguments to a \"base\" invocation made by a nonlocal-\"args\" definition body.
5.  If NAME is \"decl\",      a \"decl\"          in a usage context.
6.  If NAME is \"args\",      a nonlocal \"args\" in a usage context.
7.  If NAME is \"prms\",      a nonlocal \"prms\" in a usage context.
8.  If NAME is \"base-args\", a \"base\" that comprises the body of a nonlocal-\"args\" definition.
9.  If NAME is \"decl-prms\", a \"decl\" that comprises the body of a nonlocal-\"prms\" definition.
10  If NAME is \"locl-args\", a local \"args\"    in a usage context.
11. If NAME is \"locl-prms\", a local \"prms\"    in a usage context.
12. If NAME is \"args-args\", a nonlocal \"args\" that comprises the body of a local-\"args\" definition.
13. If NAME is \"prms-prms\", a nonlocal \"prms\" that comprises the body of a local-\"prms\" definition.

+-----------------------------------------------------------------------------------------------+-+-+-+-+-+-+-+-+-+--+--+--+--+
|Requirement/feature                                                                            |1|2|3|4|5|6|7|8|9|10|11|12|13|
+-----------------------------------------------------------------------------------------------+-+-+-+-+-+-+-+-+-+--+--+--+--+
|SUFFIX-SEQUENCE can be a regex reference integer instead of a string                           |X| | | | | | |X|X|  |  |X |X |
|SUFFIX-SEQUENCE as a string must be a nonlocal suffix sequence                                 |X|X|X|X|X|X|X|X|X|  |  |X |X |
|SUFFIX-SEQUENCE as a string must be a    local suffix sequence                                 | | | | | | | | | |X |X |  |  |
+-----------------------------------------------------------------------------------------------+-+-+-+-+-+-+-+-+-+--+--+--+--+
|Possibly-empty comma-suffixed list of the default function arguments must not exist            | | |X|X| |X|X| | |X |X |X |X |
|Possibly-empty comma-suffixed list of the default function arguments must exist                |X|X| | |X| | |X|X|  |  |  |  |
|`-only consists of default function invocations                                                | | | | |X| | | | |  |  |  |  |
|`-only consists of a number of \"()\"                                                          | | | | | | | | |X|  |  |  |  |
|`-only consists of a number of identifiers with both noncore and core parts and/or \"()\"      | |X| | | | | | | |  |  |  |  |
|`-only consists of a number of core-part-only identifiers and/or one \"(...)\"                 |X| | | | | | | | |  |  |  |  |
|`-only consists of a number of \"()\" and/or one \"(...)\"                                     | | | | | | | |X| |  |  |  |  |
+-----------------------------------------------------------------------------------------------+-+-+-+-+-+-+-+-+-+--+--+--+--+
|\"I\" along with its trailing comma must exist                                                 | | | | |X|X|X| | |  |  |X |X |
|Either \"I\" or \"\", both having a trailing comma, must exist                                 | |X|X| | | | | | |  |  |  |  |
|\"I\" along with its trailing comma must not exist                                             | | | | | | | | |X|  |  |  |  |
|\"I##I\" along with its trailing comma is an optional unit                                     |X| | | | | | | | |  |  |  |  |
|\"I\" along with its trailing comma is an optional unit                                        | | | | | | | |X| |  |  |  |  |
|\"I\" must exist but its trailing comma exists iff a non-empty comma-separated list follows    | | | | | | | | | |X |X |  |  |
+-----------------------------------------------------------------------------------------------+-+-+-+-+-+-+-+-+-+--+--+--+--+
|Non-empty comma-separated list of non-default-function non-I non-I##I arguments is optional    | | | | | | | | | |X |X |  |  |
|Non-empty comma-separated list of non-default-function non-I non-I##I arguments must exist     |X|X|X| |X|X|X|X|X|  |  |X |X |
|`-only consists of identifiers with both noncore and core parts                                | |X|X| | | | |
|`-only consists of a number of core-part-only identifiers and/or invocations to parent \"args\"| | | | | | | |X| |  |  |  |  |
|`-only consists of \"__VA_ARGS__\"                                                             | | | | | | | | |X|  |  |  |  |
|`-only consists of invocations to \"type\"/\"typr\" or parent \"decl\"/\"prms\"                |X| | | | | | | | |  |  |  |  |
|`-only consists of a number of \"_\" and/or \"X\" and/or invocations to \"R\"                  | | | | | |X| | | |  |  |  |  |
|`-only consists of a number of \"_\" and/or \"X\" and/or invocations to \"R\" and/or \"RR\"    | | | | |X| |X| | |  |  |  |  |
+-----------------------------------------------------------------------------------------------+-+-+-+-+-+-+-+-+-+--+--+--+--+
|Number of capture groups if SUFFIX-SEQUENCE is a string (one less in case of an integer)       |
+-----------------------------------------------------------------------------------------------+-+-+-+-+-+-+-+-+-+--+--+--+--+

If NONLOCAL-SUFFIX-SEQUENCE is a string, \"base\" must have NONLOCAL-SUFFIX-SEQUENCE as the suffix sequence; otherwise, NONLOCAL-SUFFIX-SEQUENCE
must be an integer that can be used as a reference number to refer to a preceding capture group that gives the suffix sequence.  If
NONLOCAL-SUFFIX-SEQUENCE is a string, there are four capture groups that capture as the first group the nonlocal suffix sequence of \"base\", as the
second group the possibly-empty comma-suffixed list of the default function arguments, as the third group either \"I##I\" along with its trailing
comma or an empty string, and as the fourth group a non-empty comma-separated sequence of invocations to a more general \"decl\"/\"prms\" or to
\"type\"/\"typr\".  Otherwise, the first capture group is not present, resulting in three capture groups only."
  (concat name
          (; capture group open parenthesis 1 or 0
           tice-template-pattern--regex--group (if (stringp suffix-sequence)
                                                   suffix-sequence
                                                 (if (and (integerp suffix-sequence)
                                                          (<= 0 suffix-sequence) (<= suffix-sequence 9))
                                                     (format "\\%d" suffix-sequence)
                                                   (error (concat "SUFFIX-SEQUENCE is neither a string nor a regular expression reference"
                                                                  " number"))))
                                               (integerp suffix-sequence))
          tice-template-pattern--regex--prmlst-begin
          (; capture group open parenthesis 2 or 1
           tice-template-pattern--regex--base-arglst-default-list)
          (; capture group open parenthesis 3 or 2
           tice-template-pattern--regex--group
           (tice-template-pattern--regex--alt (concat "I##I" (tice-template-pattern--regex--ppspc-comma 0))
                                              ""))
          (tice-template-pattern--regex--ppspc 0)
          (; capture group open parenthesis 4 or 3
           tice-template-pattern--regex--rest-of-arglst)))

(defun tice-template-pattern--regex--body (decl-suffix-sequence &optional decl-with-base-prmlst-p)
  "Return a regular expression that matches the definition of \"decl\" whose body invokes \"base\".
If DECL-SUFFIX-SEQUENCE is a string, both of the \"decl\" and \"base\" must have DECL-SUFFIX-SEQUENCE as the suffix sequence; otherwise, the
\"decl\" and \"base\" are only required to have the same unknown suffix sequence, and DECL-SUFFIX-SEQUENCE is assumed to be a non-negative integer
less than nine that counts the number of capture-group open-parentheses that precedes the returned regular expression.  If DECL-WITH-BASE-PRMLST-P
is set to non-nil, the parameter list of \"decl\" obeys the format of the parameter list of \"base\".  There are four capture groups over the
definition body that capture as the first group the nonlocal suffix sequence of \"base\", as the second group the possibly-empty comma-suffixed list
of the default function arguments, as the third group either \"I##I\" along with its trailing comma or an empty string, and as the fourth group a
non-empty comma-separated sequence of invocations to a more general \"decl\"/\"prms\" or to \"type\"/\"typr\"."
  (unless (or (stringp decl-suffix-sequence)
              (and (integerp decl-suffix-sequence)
                   (<= 0 decl-suffix-sequence) (< decl-suffix-sequence 9)))
    (error (concat "DECL-SUFFIX-SEQUENCE is neither a string nor a non-negative integer less than nine"
                   "  that counts the number of preceding capture-group open-parentheses")))
  (concat (tice-template-pattern--regex--def (if decl-with-base-prmlst-p
                                                 "decl-base"
                                               "decl") t t t (stringp decl-suffix-sequence) (if (stringp decl-suffix-sequence)
                                                                                                decl-suffix-sequence))
          (tice-template-pattern--regex--ppspc 0)
          (tice-template-pattern--regex--invocation "base" (if (stringp decl-suffix-sequence)
                                                               decl-suffix-sequence
                                                             (1+ decl-suffix-sequence)))))

(defun tice-template-pattern--regex--start-base ()
  "Return a regular expression that matches the definition of \"base\", including its body, that has been started using
`tice-template-pattern-start-base' but has not yet been completed using `tice-template-pattern-end-base'.  There are three capture groups, the first
two of which are never nil and capture the empty parameter list of \"base\" and the empty placeholder of I, respectively.  The third group if
non-nil means that \"args\" is the first in the definition body, otherwise the first one is \"opts\"."
  (concat tice-template-pattern--regex--def-begin
          "base"
          (tice-template-pattern--regex--suffix-sequence 'nonlocal nil t)
          "("
          (; capture group open parenthesis 1
           tice-template-pattern--regex--group "")
          ") "
          (; capture group open parenthesis 2
           tice-template-pattern--regex--group "")
          tice-template-pattern--regex--line-continuation
          (; capture group open parenthesis 3
           tice-template-pattern--regex--base-body)))

(defun tice-template-pattern--regex--base-body ()
  "Return a regular expression that matches the body of a \"base\" definition.
There is one capture group that is non-nil if the body starts with the string \"args\", otherwise the body starts with the string \"opts\".
If the capture group is non-nil, it captures the string \"args\" along with its nonlocal suffix sequence."
  (concat (tice-template-pattern--regex--ppspc 0)
          (; capture group open parenthesis 1
           tice-template-pattern--regex--base-body-entry nil nil t)
          (tice-template-pattern--regex--group (concat (tice-template-pattern--regex--ppspc 0)
                                                       (tice-template-pattern--regex--base-body-entry t))
                                               t) "*"
          "[[:space:]]*$"))

(defun tice-template-pattern--regex--base-body-entry-opts (&optional not-captured first-argument-nonempty-p third-argument-nonempty-p
                                                                     second-argument-regex)
  "Return a regular expression that matches the argument list of an invocation to \"opts\" in the definition body of \"base\".
By default, there are three capture groups capture as the first, second, and third groups the first, second, and third arguments of the invocation,
respectively.  The third capture group is nil if either THIRD-ARGUMENT-NONEMPTY-P is nil and the third argument of the invocation is not omitted or
THIRD-ARGUMENT-NONEMPTY-P is non-nil and the third argument of the invocation is \"()\"; otherwise, it is non-nil.  Additionally, the first argument
is expected to be empty unless FIRST-ARGUMENT-NONEMPTY-P is set to non-nil, in which case the first argument is expected to be a parameter
identifier.  Analogously, the third argument is expected to be either empty or \"()\" unless THIRD-ARGUMENT-NONEMPTY-P is set to non-nil, in which
case the third argument is expected to be a default function identifier or \"()\".  Similarly, the second argument is expected to match a valid C++
identifier unless the regular expression SECOND-ARGUMENT-REGEX is non-nil, in which case the second argument is expected to match the regular
expression.  If NOT-CAPTURED is set to non-nil, there is no capture group."
  (concat "opts"
          tice-template-pattern--regex--prmlst-begin
          (if first-argument-nonempty-p "[[:space:]]*")
          (; capture group open parenthesis 1
           tice-template-pattern--regex--group (if first-argument-nonempty-p
                                                   (tice-template-pattern--regex--param-ident-seq t)
                                                 "[[:space:]]*")
                                               not-captured)
          (if first-argument-nonempty-p "[[:space:]]*") ",[[:space:]]*"
          (; capture group open parenthesis 2
           tice-template-pattern--regex--group (if second-argument-regex
                                                   second-argument-regex
                                                 (concat (tice-template-pattern--regex--c-ident-first-char)
                                                         (tice-template-pattern--regex--c-ident-next-char) "*"))
                                               not-captured)
          "[[:space:]]*,"
          (tice-template-pattern--regex--group (tice-template-pattern--regex--alt "[[:space:]]*()[[:space:]]*"
                                                                                  (; capture group open parenthesis 3
                                                                                   tice-template-pattern--regex--group
                                                                                   (concat "[[:space:]]*"
                                                                                           (if third-argument-nonempty-p
                                                                                               (concat
                                                                                                (tice-template-pattern--regex--default-function nil
                                                                                                                                                t)
                                                                                                "[[:space:]]*")))
                                                                                   not-captured))
                                               t)
          ")"))

(defun tice-template-pattern--regex--base-body-entry (&optional disable-capture first-argument-nonempty-p indicator-only-p)
  "Return a regular expression that matches either an invocation to \"opts\" or the string \"args\" that is followed by a nonlocal suffix sequence
in the definition body of \"base\".  There are four capture groups.  If the first capture group is non-nil, then \"args\" is matched and the group
contains the matched string along with its nonlocal suffix sequence; in this case, the other capture groups are nil.  In contrast, if the first
capture group is nil, then an invocation to \"opts\" is matched and has its first, second, and third invocation arguments captured by the second,
third, and fourth capture groups, respectively, with the fourth capture group being non-nil if the third invocation argument is empty (if the fourth
capture group is nil, then the third argument is \"()\").  By default, the first argument of the invocation is expected to be empty unless
FIRST-ARGUMENT-NONEMPTY-P is set to non-nil, in which case the first argument is expected to be a parameter identifier.  If DISABLE-CAPTURE is set
to non-nil, there is no capture group.  If INDICATOR-ONLY-P is set to non-nil, there is only one capture group that is nil if \"opts\" is matched
and that is non-nil if \"args\" along with its suffix sequence are captured instead."
  (tice-template-pattern--regex--group
   (tice-template-pattern--regex--alt
    (; capture group open parenthesis 1
     tice-template-pattern--regex--group (concat "args" (tice-template-pattern--regex--suffix-sequence 'nonlocal nil t))
                                         disable-capture)
    (; capture group open parenthesis 2, 3, 4
     tice-template-pattern--regex--base-body-entry-opts (or disable-capture indicator-only-p) first-argument-nonempty-p))
   t))

;;\end{regex}

;;\begin{operations on suffix sequence}

(defun tice-template-pattern--make-suffix-sequence-str (seq-subpart str-subpart-prefix)
  "Return a nonlocal suffix sequence whose sequence subpart is SEQ-SUBPART and whose string subpart is STR-SUBPART-PREFIX that is appended with one
uppercase letter that is obtained by checking in alphabetical order starting from the letter \"A\" to the letter \"Z\" for the first letter that
makes the returned nonlocal suffix sequence distinct among the existing nonlocal suffix sequences whose sequence subparts are SEQ-SUBPART."
  (save-excursion
    (save-match-data
      (save-restriction
        (tice-template-pattern-narrow)
        (let ((last-char ?A))
          (catch 'nonlocal-suffix-sequence
            (while (<= last-char ?Z)
              (goto-char (point-min))
              (let ((nonlocal-suffix-sequence (format "_%s%s%c" seq-subpart str-subpart-prefix last-char)))
                (unless (tice-template-pattern--search-forward (tice-template-pattern--regex--def "base" t t t nil nonlocal-suffix-sequence))
                  (throw 'nonlocal-suffix-sequence nonlocal-suffix-sequence)))
              (setq last-char (1+ last-char)))
            (error "All single uppercase letter has already been used")))))))

(defun tice-template-pattern--add-suffix-sequence (suffix-sequence)
  "Return SUFFIX-SEQUENCE as a distinct suffix sequence by adjusting both SUFFIX-SEQUENCE and the existing suffix sequences as necessary."
  (save-excursion
    (save-match-data
      (save-restriction
        (let* ((suffix-sequence-data (let ((regex (tice-template-pattern--regex--suffix-sequence nil t)))
                                       (unless (string-match regex suffix-sequence)
                                         (error "SUFFIX-SEQUENCE `%s' fails to match the suffix sequence pattern `%s'" suffix-sequence regex))
                                       `(,(match-string 1 suffix-sequence) ,(match-string 2 suffix-sequence)
                                         ,(match-string 3 suffix-sequence) ,(match-string 4 suffix-sequence))))
               (nonlocal-seq (nth 0 suffix-sequence-data))
               (nonlocal-str (nth 1 suffix-sequence-data))
               (local-seq (nth 2 suffix-sequence-data))
               (local-str (nth 3 suffix-sequence-data))
               (has-local-part-p local-seq))
          (catch 'distinct-suffix-sequence
            (tice-template-pattern-narrow)
            (goto-char (point-min))
            (unless (tice-template-pattern--search-forward
                     (funcall (if has-local-part-p
                                  'tice-template-pattern--regex--local-def
                                'tice-template-pattern--regex--def) (if has-local-part-p "prms" "base") t t t nil (string-to-number 1)))
              (throw 'distinct-suffix-sequence "1"))
            (let ((existing-suffix-sequence (match-string 1)))
              (string-match (tice-template-pattern--regex--suffix-sequence 'nonlocal t) existing-suffix-sequence)
              (let ((existing-seq (match-string 1) existing-suffix-sequence)
                    (existing-str (match-string 2) existing-suffix-sequence))
                (if (string= "" existing-str)
                    (throw 'distinct-suffix-sequence (tice-template-pattern--make-suffix-sequence-str existing-seq existing-str))
                  (throw 'distinct-suffix-sequence (tice-template-pattern--give-suffix-sequence-sibling existing-seq)))))))))))

;;\end{operations on suffix sequence}

;;\begin{public interface implementation}

(defun tice-template-pattern--assert-decl-invokes-base-supplying-this-argument (decl-begin
                                                                                decl-suffix-sequence
                                                                                invocation-name
                                                                                invocation-suffix-sequence
                                                                                invocation-end
                                                                                &optional
                                                                                set-match-data-p
                                                                                decl-with-base-prmlst-p)
  "Raise an error if DECL-BEGIN is not at the first character of a \"decl\" definition with nonlocal suffix sequence DECL-SUFFIX-SEQUENCE whose
definition body invokes \"base\" with the same nonlocal suffix sequence, supplying as one of the arguments an invocation to INVOCATION-NAME with
nonlocal suffix sequence INVOCATION-SUFFIX-SEQUENCE, that ends at INVOCATION-END.  If DECL-WITH-BASE-PRMLST-P is set to non-nil, the parameter list
of \"decl\" obeys the format of the parameter list of \"base\".  If no error is raised and SET-MATCH-DATA-P is set to non-nil, `match-data' is set
to have four capture groups over the definition body that capture as the first group the nonlocal suffix sequence of \"base\", as the second group
the possibly-empty comma-suffixed list of the default function arguments, as the third group either \"I##I\" along with its trailing comma or an
empty string, and as the fourth group a non-empty comma-separated sequence of invocations to a more general \"decl\"/\"prms\" or to
\"type\"/\"typr\"."
  (let ((match-data (save-excursion
                      (save-match-data
                        (goto-char decl-begin)
                        (if (not (looking-at
                                  (tice-template-pattern--regex--body decl-suffix-sequence decl-with-base-prmlst-p)))
                            (error "Cannot find an invocation to \"base%s\" in the body of the definition of \"decl%s\""
                                   decl-suffix-sequence decl-suffix-sequence)
                          (if (>= invocation-end (match-end 0))
                              (error "The \"%s%s\" is not one of the arguments of the invocation to \"base%s\""
                                     invocation-name invocation-suffix-sequence decl-suffix-sequence)
                            (match-data)))))))
    (if set-match-data-p
        (set-match-data match-data))))

(defun tice-template-pattern--prepend-suffix-sequence-to-default-or-param-core-part (regex-fn suffix-sequence)
  "Use the regular expression produced by either `tice-template-pattern--regex--default-function' or `tice-template-pattern--regex--param-ident' as
REGEX-FN to find all matches in the visible part of the buffer so that every match that has only the core part is given SUFFIX-SEQUENCE as its
prefix."
  (unless (or (eq regex-fn 'tice-template-pattern--regex--default-function)
              (eq regex-fn 'tice-template-pattern--regex--param-ident))
    (error "The REGEX-FN is neither `tice-template-pattern--regex--default-function' nor `tice-template-pattern--regex--param-ident'"))
  (save-excursion
    (save-match-data
      (let ((suffix-sequence-length (length suffix-sequence)))
        (goto-char (point-min))
        (while (tice-template-pattern--search-forward (funcall regex-fn t))
          (let ((has-noncore-part-p (not (null (match-string 1)))))
            (unless has-noncore-part-p
              (goto-char (match-beginning 0))
              (insert suffix-sequence)
              (goto-char (+ (match-end 0) suffix-sequence-length)))))))))

(defun tice-template-pattern--args-to-prms ()
  "If `point' is at the first character of the string \"args\" that is an argument to an invocation to \"base\" that comprises the body of the
definition of \"decl\" whose parameter list is that of \"base\", replace \"args\" with an invocation to its \"prms\" and adjust the parameter list
of \"decl\" accordingly."
  (save-excursion
    (save-match-data
      (save-restriction
        (tice-template-pattern-narrow)
        (let* ((args-data (if (looking-at (concat "args"
                                                  (tice-template-pattern--regex--suffix-sequence 'nonlocal)))
                              `(,(match-string 1) ,(match-beginning 0) ,(match-end 0))
                            (error "Point is not at the first character of \"args\"")))
               (args-suffix-sequence (nth 0 args-data))
               (args-pos-begin (nth 1 args-data))
               (args-pos-end (nth 2 args-data))
               (decl-data (if (tice-template-pattern--search-backward
                               (tice-template-pattern--regex--def "decl-base" t))
                              `(,(string-match-p (concat "^args" args-suffix-sequence (tice-template-pattern--regex--group
                                                                                       (tice-template-pattern--regex--alt ","
                                                                                                                          "$")
                                                                                       t))
                                                 (match-string 3))
                                ,(match-string 1) ,(match-beginning 0) ,(match-end 0))
                            (error (concat "Cannot find the definition of \"decl\" whose body invokes \"base\""
                                           " supplying \"args%s\" as one of its arguments")
                                   args-suffix-sequence)))
               (decl-suffix-sequence (nth 1 decl-data))
               (decl-pos-begin (nth 2 decl-data))
               (decl-pos-end (nth 3 decl-data))
               (I-is-needed (and (nth 0 decl-data)
                                 (if (tice-template-pattern--search-backward (tice-template-pattern--regex--def "base" t nil t nil
                                                                                                                decl-suffix-sequence))
                                     (string= "" (match-string 2))
                                   (error "Cannot find the definition of \"base%s\"" decl-suffix-sequence))))
               (prms-prmlst (if (tice-template-pattern--search-backward (tice-template-pattern--regex--def
                                                                         "args" t t nil nil args-suffix-sequence))
                                (match-string 2)
                              (error "Cannot find the definition of \"args%s\"" args-suffix-sequence)))
               prms-prmlst-pos-begin
               prms-prmlst-pos-end
               prms-invocation-prmlst)
          (tice-template-pattern--assert-decl-invokes-base-supplying-this-argument decl-pos-begin decl-suffix-sequence
                                                                                   "args" args-suffix-sequence args-pos-end
                                                                                   nil t)
          (goto-char args-pos-begin)
          (delete-region args-pos-begin args-pos-end)
          (insert "prms" args-suffix-sequence "(" (if I-is-needed "I" "") ", ")
          (setq prms-prmlst-pos-begin (point))
          (insert prms-prmlst)
          (setq prms-prmlst-pos-end (point))
          (insert ")")
          (save-restriction
            (narrow-to-region prms-prmlst-pos-begin prms-prmlst-pos-end)
            (tice-template-pattern--prepend-suffix-sequence-to-default-or-param-core-part 'tice-template-pattern--regex--param-ident
                                                                                          args-suffix-sequence)
            (setq prms-invocation-prmlst (buffer-string)))
          (save-restriction
            (goto-char decl-pos-begin)
            (narrow-to-region decl-pos-begin decl-pos-end)
            (unless (tice-template-pattern--search-forward (concat "args" args-suffix-sequence))
              (error "Cannot find \"args%s\" in the parameter list of \"decl%s\""
                     args-suffix-sequence decl-suffix-sequence))
            (goto-char (match-beginning 0))
            (delete-region (match-beginning 0) (match-end 0))
            (insert prms-invocation-prmlst)))))))

(defun tice-template-pattern--prms-to-decl (invoked-prms-begin
                                            invoked-prms-suffix-sequence
                                            invoked-prms-default-list-begin
                                            invoked-prms-default-list-end
                                            decl-default-list-begin
                                            decl-default-list-end
                                            base-arglst-default-begin
                                            base-arglst-default-end
                                            base-arglst-begin
                                            base-arglst-end)
  "Use INVOKED-PRMS-SUFFIX-SEQUENCE to obtain the default list to be yanked in the region delineated by INVOKED-PRMS-DEFAULT-LIST-BEGIN and
INVOKED-PRMS-DEFAULT-LIST-END and in the region delineated by DECL-DEFAULT-LIST-BEGIN and DECL-DEFAULT-LIST-END at the appropriate point that is
determined using INVOKED-PRMS-BEGIN and the regions delineated by BASE-ARGLST-DEFAULT-BEGIN and BASE-ARGLST-DEFAULT-END and by BASE-ARGLST-BEGIN and
BASE-ARGLST-END, respectively."
  (save-excursion
    (save-match-data
      (save-restriction
        (let ((default-list (save-excursion
                              (save-match-data
                                (save-restriction
                                  (tice-template-pattern-narrow)
                                  (goto-char (point-min))
                                  (let ((regex (tice-template-pattern--regex--def "decl" nil t t nil invoked-prms-suffix-sequence)))
                                    (if (tice-template-pattern--search-forward regex)
                                        (match-string 2)
                                      (error "Cannot find the definition of \"decl%s\" using `%s'" invoked-prms-suffix-sequence regex))))))))
          (unless (string= "" default-list)
            (let ((default-list-pos (save-excursion
                                      (save-match-data
                                        (save-restriction
                                          (widen)
                                          (narrow-to-region base-arglst-default-begin base-arglst-end)
                                          (goto-char invoked-prms-begin)
                                          (catch 'default-list-pos
                                            (while (tice-template-pattern--search-backward
                                                    (tice-template-pattern--regex--alt
                                                     (; capture group open parenthesis 1, 2, 3, 4, 5
                                                      tice-template-pattern--regex--base-arglst-parent-invocation t)
                                                     (; capture group open parenthesis 6
                                                      tice-template-pattern--regex--base-arglst-type-invocation))
                                                    base-arglst-begin)
                                              (unless (looking-at-p "prms")
                                                (let* ((trailing-comma (tice-template-pattern--regex--ppspc-comma 0))
                                                       (last-default-regex (if (looking-at-p "typ[er]")
                                                                               (let* ((core-part-seq
                                                                                       (save-match-data
                                                                                         (let ((param-ident (match-string 6)))
                                                                                           (string-match
                                                                                            (tice-template-pattern--regex--param-ident-seq)
                                                                                            param-ident)
                                                                                           (match-string 1 param-ident))))
                                                                                      (regex (concat (tice-template-pattern--regex--default-function
                                                                                                      nil t core-part-seq)
                                                                                                     trailing-comma)))
                                                                                 (save-excursion
                                                                                   (goto-char base-arglst-default-begin)
                                                                                   (if (tice-template-pattern--search-forward
                                                                                        regex base-arglst-default-end)
                                                                                       regex)))
                                                                             (let ((regex (concat
                                                                                           (tice-template-pattern--regex--group
                                                                                            (tice-template-pattern--regex--alt "." "\n") t) "*"
                                                                                           (tice-template-pattern--regex--default-function nil nil
                                                                                                                                           nil t)
                                                                                           trailing-comma))
                                                                                   (parent-decl-default-list (match-string 3)))
                                                                               (if (string-match regex parent-decl-default-list)
                                                                                   (concat (match-string 1 parent-decl-default-list)
                                                                                           trailing-comma))))))
                                                  (unless (; either "type"/"typr" is not defaulted or "decl" has no default
                                                           null last-default-regex)
                                                    (widen)
                                                    (goto-char decl-default-list-begin)
                                                    (narrow-to-region decl-default-list-begin decl-default-list-end)
                                                    (if (tice-template-pattern--search-forward last-default-regex)
                                                        (throw 'default-list-pos (match-end 0))
                                                      (error (concat "The default list of \"decl\" in the region between %d and %d, inclusive,"
                                                                     " fails to match the last function identifier pattern `%s'")
                                                             decl-default-list-begin decl-default-list-end last-default-regex))))))
                                            decl-default-list-begin))))))
              (let ((default-list-all-noncore-present (save-restriction
                                                        (goto-char invoked-prms-default-list-begin)
                                                        (delete-region invoked-prms-default-list-begin invoked-prms-default-list-end)
                                                        (narrow-to-region invoked-prms-default-list-begin invoked-prms-default-list-end)
                                                        (insert default-list " ")
                                                        (tice-template-pattern--prepend-suffix-sequence-to-default-or-param-core-part
                                                         'tice-template-pattern--regex--default-function
                                                         invoked-prms-suffix-sequence)
                                                        (buffer-substring invoked-prms-default-list-begin (1- (point))))))
                (goto-char decl-default-list-begin)
                (if (= decl-default-list-begin decl-default-list-end)
                    (insert default-list-all-noncore-present " ")
                  (goto-char default-list-pos)
                  (insert (if (= default-list-pos decl-default-list-begin) "" " ")
                          default-list-all-noncore-present
                          (if (= default-list-pos decl-default-list-begin) " " "")))))))))))

(defun tice-template-pattern--trailing-spaces-end (last-char-pos)
  "Return the position immediately following the last whitespace character following LAST-CHAR-POS.  If no whitespace character follows
LAST-CHAR-POS, LAST-CHAR-POS is returned."
  (save-excursion
    (save-match-data
      (goto-char last-char-pos)
      (if (looking-at "[[:space:]]*")
          (match-end 0)
        last-char-pos))))

(defun tice-template-pattern--decl-to-prms (invoked-decl-default-list-begin
                                            invoked-decl-default-list-end
                                            decl-default-list-begin
                                            decl-default-list-end)
  "Delete from the region delineated by DECL-DEFAULT-LIST-BEGIN and DECL-DEFAULT-LIST-END the default list that is found in the region delineated by
INVOKED-DECL-DEFAULT-LIST-BEGIN and INVOKED-DECL-DEFAULT-LIST-END that is also deleted."
  (save-excursion
    (save-match-data
      (save-restriction
        (unless (= invoked-decl-default-list-begin invoked-decl-default-list-end)
          (let* ((default-list-regex (save-restriction
                                       (goto-char invoked-decl-default-list-begin)
                                       (narrow-to-region invoked-decl-default-list-begin
                                                         (tice-template-pattern--trailing-spaces-end invoked-decl-default-list-end))
                                       (let ((separator-regex (tice-template-pattern--regex--ppspc-comma-ppspc 0 0)))
                                         (while (tice-template-pattern--search-forward separator-regex)
                                           (replace-match separator-regex nil t)))
                                       (let ((begin (point-min))
                                             (end (point-max)))
                                         (prog1
                                             (buffer-substring begin end)
                                           (delete-region begin end))))))
            (narrow-to-region decl-default-list-begin (tice-template-pattern--trailing-spaces-end decl-default-list-end))
            (goto-char (point-min))
            (unless (tice-template-pattern--search-forward default-list-regex)
              (error "Cannot find the default list of the invoked \"decl\" in the default list of the \"decl\" definition using `%s'"
                     default-list-regex))
            (delete-region (match-beginning 0) (match-end 0))))))))

(defun tice-template-pattern--make-base-arglst ()
  "Return as a string, a comma-separated sequence of arguments to invoke \"base\" whose invocation comprises the body of the definition of \"args\".
The `point' must be at the first character of the `tice-template-pattern--regex--def' of \"decl\"."
  (save-excursion
    (save-match-data
      (let* ((decl-suffix-sequence (let ((current-pos (point)))
                                     (if (looking-at (tice-template-pattern--regex--def "decl" t t t))
                                         (match-string 1)
                                       (error (concat "The `point' at char %d is not at the first character of"
                                                      " the `tice-template-pattern--regex--def' of \"decl\"")
                                              current-pos))))
             (base-invocation (if (tice-template-pattern--search-forward (tice-template-pattern--regex--body decl-suffix-sequence))
                                  `(,(match-string 2) ,(match-string 3) ,(match-string 4))
                                (error (concat "Cannot find an invocation of \"base%s\" in the body"
                                               " of the definition of \"decl%s\"")
                                       decl-suffix-sequence decl-suffix-sequence)))
             (has-I-p (not (string= "" (nth 1 base-invocation))))
             (default-ident-list (let ((current-text (nth 0 base-invocation)))
                                   (if (string= "" current-text)
                                       current-text
                                     (let ((text (concat current-text " ")))
                                       (tice-template-pattern--replace-all-matches (tice-template-pattern--regex--default-function)
                                                                                   "()"
                                                                                   (progn
                                                                                     (if (string-match "()" text)
                                                                                         (replace-match "(...)" nil nil text)
                                                                                       text)))))))
             (I (if has-I-p "I, " ""))
             (param-list (cl-labels
                             ((process-text
                               (text)
                               (cond ((string-match (concat "^" (tice-template-pattern--regex--ppspc 0) "$")
                                                    text)
                                      "")
                                     ((string-match (concat "^"
                                                            (tice-template-pattern--regex--ppspc-comma-ppspc 0 0)
                                                            (; capture group open parenthesis 1
                                                             tice-template-pattern--regex--base-arglst-parent-invocation t))
                                                    text)
                                      (concat ", args" (match-string 2 text) "(" (match-string 4 text) " " (match-string 5 text) ")"
                                              (process-text (substring text (match-end 0)))))
                                     ((string-match (concat "^"
                                                            (tice-template-pattern--regex--ppspc-comma-ppspc 0 0)
                                                            (; capture group open parenthesis 1
                                                             tice-template-pattern--regex--base-arglst-type-invocation))
                                                    text)
                                      (concat ", " (match-string 1 text)
                                              (process-text
                                               (let ((text-with-one-unmatched-open-parenthesis (substring text (match-end 0)))
                                                     (unmatched-open-parenthesis-count 1))
                                                 (while (> unmatched-open-parenthesis-count 0)
                                                   (let* ((open-parenthesis-start-pos
                                                           (string-match ".*?("
                                                                         text-with-one-unmatched-open-parenthesis))
                                                          (open-parenthesis-succeeding-pos (match-end 0))
                                                          (close-parenthesis-start-pos
                                                           (string-match ".*?)"
                                                                         text-with-one-unmatched-open-parenthesis))
                                                          (close-parenthesis-succeeding-pos (match-end 0)))
                                                     (if (null close-parenthesis-start-pos)
                                                         (error (concat "The remaining content body of the argument list of"
                                                                        " a \"base\" invocation `%s' has no closing parenthesis"
                                                                        text-with-one-unmatched-open-parenthesis)))
                                                     (if (or (null open-parenthesis-start-pos)
                                                             (> open-parenthesis-succeeding-pos
                                                                close-parenthesis-succeeding-pos))
                                                         (progn
                                                           (setq unmatched-open-parenthesis-count (1- unmatched-open-parenthesis-count))
                                                           (setq text-with-one-unmatched-open-parenthesis
                                                                 (substring text-with-one-unmatched-open-parenthesis
                                                                            close-parenthesis-succeeding-pos)))
                                                       (setq unmatched-open-parenthesis-count (1+ unmatched-open-parenthesis-count))
                                                       (setq text-with-one-unmatched-open-parenthesis
                                                             (substring text-with-one-unmatched-open-parenthesis
                                                                        open-parenthesis-succeeding-pos)))))
                                                 text-with-one-unmatched-open-parenthesis))))
                                     (t
                                      (error (concat "The remaining to-be-processed argument list of a \"base\" invocation `%s'"
                                                     " fails to start with an invocation to either \"decl\" or \"prms\" or \"type\" or \"typr\"")
                                             text)))))
                           (substring (process-text (concat ", " (nth 2 base-invocation))) 2))))
        (concat default-ident-list I param-list)))))

(defun tice-template-pattern--replace-all-param-ident-with-underscore (text)
  "Replace every occurrence of `tice-template-pattern--regex--param-ident' in TEXT with an underscore."
  (tice-template-pattern--replace-all-matches
   (tice-template-pattern--regex--alt (concat (tice-template-pattern--regex--ppspc-comma-ppspc 1 2) "_")
                                      (concat (tice-template-pattern--regex--ppspc-comma 1) " _")
                                      (concat (tice-template-pattern--regex--comma-ppspc 2) "_")
                                      ",_")
   ", _"
   (tice-template-pattern--replace-all-matches (tice-template-pattern--regex--param-ident nil t) "_" text)))

(defun tice-template-pattern--use (name suffix-sequence-sans-leading-underscore)
  "Insert at `point' an invocation to \"NAME_SUFFIX-SEQUENCE-SANS-LEADING-UNDERSCORE\".
If DEFAULT-LIST-TEXT is set to non-nil, it is prepended to the invocation's argument list as default function invocations."
  (unless (or (string= name "decl") (string= name "args") (string= name "prms"))
    (error "The NAME `%s' is neither \"decl\" nor \"args\" nor \"prms\"" name))
  (let* ((insert-pos (point))
         (suffix-sequence (concat "_" suffix-sequence-sans-leading-underscore))
         (prmlst (save-excursion
                   (save-match-data
                     (save-restriction
                       (tice-template-pattern-narrow)
                       (goto-char (point-min))
                       (unless (tice-template-pattern--search-forward (tice-template-pattern--regex--def name nil nil nil nil suffix-sequence))
                         (error "Cannot find the definition of \"%s%s\" in the region between %d and %d, inclusive"
                                name suffix-sequence (point-min) (point-max)))
                       (if (string= name "prms")
                           (unless (tice-template-pattern--search-backward (tice-template-pattern--regex--def "args" nil nil nil nil
                                                                                                              suffix-sequence))
                             (error "Cannot find the definition of \"%s%s\" in the region between %d and %d, inclusive"
                                    "args" suffix-sequence (point-min) (point-max))))
                       (let* ((I (match-string 3))
                              (param-list (match-string 4))
                              (default-list-text (if (string= name "decl")
                                                  (let ((text (match-string 2))
                                                        (max-length 0)
                                                        (offset 0)
                                                        (result ()))
                                                    (if (string= "" text)
                                                        text
                                                      (while (string-match (tice-template-pattern--regex--default-function) text offset)
                                                        (setq max-length (max max-length (- (match-end 1) (match-beginning 1)))
                                                              offset (match-end 0)
                                                              result (cons (match-string 1 text) result)))
                                                      (let ((suffix "(),\n"))
                                                        (concat (mapconcat #'(lambda (text)
                                                                               (format (concat "%-" (int-to-string max-length) "s") text))
                                                                           (nreverse result)
                                                                           suffix)
                                                                suffix)))))))
                         (concat default-list-text I (unless (string= "" I) " ")
                                 (tice-template-pattern--replace-all-param-ident-with-underscore param-list))))))))
    (save-excursion
      (save-restriction
        (narrow-to-region insert-pos insert-pos)
        (insert name suffix-sequence "(" prmlst ")")
        (indent-region insert-pos (prog1
                                      (point-max)
                                    (widen)))))))

;;\end{public interface implementation}

(provide 'tice-template-pattern)

;;; tice-template-pattern.el ends here
