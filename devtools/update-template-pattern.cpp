/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <string>
#include <set>
#include <vector>
#include <regex>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <functional>
#include <exception>
#include <stdexcept>
#include <memory>
#include <libgen.h>

using namespace std::literals;

namespace {

  const unsigned c_backslash_max_column = 148;

};
namespace {

  void load_file_linewise(std::ifstream &file, const std::function<bool(std::string &line)> &process_line, std::string *line_buffer = nullptr) {

    static std::string shared_line_buffer;

    if (line_buffer == nullptr) {
      line_buffer = &shared_line_buffer;
    }

    do {

      if (file.eof()) {
        break;
      }

      line_buffer->clear();

      std::getline(file, *line_buffer);
      if (file.good()) {
        line_buffer->push_back('\n');
      } else if (file.fail() && !file.eof()) {
        throw std::runtime_error("failed read");
      }

    } while (process_line(*line_buffer));

  }

};
namespace {

  void extract_prefix_suffix__run(std::ifstream &file, std::string &buffer, const std::string &expected_marker_name) {

    if (file.fail()) {
      throw std::runtime_error("file cannot be read");
    }

    static const std::regex marker("^//\\\\(begin|end)\\{frequently maintained part\\}\\s*$", std::regex_constants::optimize);
    std::smatch marker_name;

    const bool is_retained = expected_marker_name == "begin";

    load_file_linewise(file,

                       [&is_retained, &buffer, &marker_name, &expected_marker_name](std::string &line_buffer) {

                         if (std::regex_search(line_buffer, marker_name, marker)) {
                           if (marker_name[1] == expected_marker_name) {
                             buffer.append(line_buffer);
                             return false;
                           } else {
                             throw std::runtime_error("unexpected marker name `"s + marker_name[1].str()
                                                      + "' (expected: `" + expected_marker_name + "')");
                           }
                         }

                         if (is_retained) {
                           buffer.append(line_buffer);
                         }

                         return true;

                       });

    if (marker_name.empty()) {
      throw std::runtime_error(expected_marker_name + " marker is missing");
    }

  }

  std::pair<std::string, std::string::const_iterator> extract_prefix_suffix(std::ifstream &file) {

    std::pair<std::string, std::string::const_iterator> result;

    auto &buffer = result.first;

    extract_prefix_suffix__run(file, buffer, "begin");

    auto prefix_size = buffer.size();

    extract_prefix_suffix__run(file, buffer, "end");

    auto prev_fmtflags = file.flags();
    std::noskipws(file);
    std::copy(std::istream_iterator<char>(file), std::istream_iterator<char>(), std::back_inserter(buffer));
    file.flags(prev_fmtflags);

    result.second = buffer.cbegin() + prefix_size;

    return result;
  }

};
namespace {
  namespace regex {

    //\begin{translation region 1A: tice-template-pattern.el}

    std::string alt(const std::vector<std::shared_ptr<std::string>> &regexps) {
      // Return a string that results from the concatenation of non-nil elements in REGEXPS with separator string \"\\|\".  REGEXPS is expected to
      // consist of a number of strings and/or nil.  If REGEXPS is nil or all of its elements are nil, return an empty string.  If REGEXPS has only
      // one non-nil element, return that element.
      std::string result;
      unsigned i = 0;
      unsigned regexps_count = regexps.size();
      if (!(regexps_count == 0)) {
        while ((i < regexps_count)
               && (regexps[i].get() == nullptr)) {
          ++i;
        }
        if (i < regexps_count) {
          result = *regexps[i].get();
        }
        ++i;
        while (i < regexps_count) {
          {
            std::string *text = regexps[i].get();
            if (text) {
              result = result + "|" + *text;
            }
          }
          ++i;
        }
      }
      return result;
    }

    std::string group(const std::string &regex, bool not_captured = false) {
      return "("s + (not_captured ? "?:" : "") + regex + ")";
    }

    //\end{translation region 1A: tice-template-pattern.el}

    //\begin{translation region 2A: tice-template-pattern.el}

    std::string c_ident_next_char(bool not_matched = false) {
      return "["s + (not_matched ? "^" : "") + "_[:alnum:]]";
    }

    const std::string quasi_bol("^[[:space:]]*");

    const std::string def_begin("^#define[[:space:]]+");
    // A regular expression that matches the definition of a template pattern function-like macro.  Note that section [Template Pattern Definition]
    // of HACKING guarantees that the C preprocessing directive and the macro identifier will be in a single physical line (a single logical line,
    // instead, can consist of two or more physical lines terminated with a backslash).

    const std::string suffix_sequence_part_seq("[[:digit:]]+");
    // The regular expression that matches the sequence subpart of a nonlocal or a local part of a suffix sequence.  The sequence subpart encodes
    // the depth of either the nonlocal or the local template pattern inheritance.

    const std::string suffix_sequence_part_str("[[:alpha:]]*");
    // The regular expression that matches the string subpart of a nonlocal or a local part of a suffix sequence.  The string subpart encodes the
    // branch of either the nonlocal or the local template pattern inheritance.

    std::string suffix_sequence_part(bool seq_not_captured = false, bool str_not_captured = false,
                                     std::shared_ptr<unsigned> seq_subpart = nullptr) {
      // Return a regular expression that matches either a nonlocal or a local part of a suffix sequence but not the whole suffix sequence.  The
      // part consists of a sequence (`regex::suffix_sequence_part_seq') and a string (`regex::suffix_sequence_part_str') subparts.  If SEQ-SUBPART
      // is a positive integer, the returned regular expression only matches the suffix sequence part whose sequence subpart is the given integer.
      // By default, there are two capture groups that capture the sequence and the string subparts as the first and the second groups,
      // respectively.  Set either SEQ-NOT-CAPTURED or STR-NOT-CAPTURED, or both, to non-nil to omit either or both of the capture groups.
      if (!((seq_subpart == nullptr)
            || (*seq_subpart > 0))) {
        throw std::runtime_error("SEQ-SUBPART is not a positive integer");
      }
      return group(("_"s
                    + group(seq_subpart ? std::to_string(*seq_subpart) : suffix_sequence_part_seq,
                            seq_not_captured)
                    + group(suffix_sequence_part_str,
                            str_not_captured)),
                   true);
    }

    enum class suffix_sequence_mode {nonlocal, local};

    std::string suffix_sequence(std::shared_ptr<suffix_sequence_mode> mode = nullptr, bool not_captured = false, bool disable_capture = false,
                                std::shared_ptr<unsigned> seq_subpart = nullptr) {
      // Return a regular expression that matches a suffix sequence as a whole, which consists of a nonlocal and an optional local parts
      // (each of which is formalized by `regex::suffix_sequence_part'), unless MODE is set to either the symbol 'nonlocal, in which
      // case the local part must be absent, or the symbol 'local, in which case the local part must be present.

      // By default, SEQ-SUBPART must be nil, and there is only one capture group that captures the suffix sequence as a whole.  If NOT-CAPTURED is
      // set to non-nil, there are two groups if MODE is 'nonlocal; otherwise, there are four groups.  The first two groups capture the sequence and
      // string subparts of the nonlocal part, respectively, while the last two groups capture the sequence and string subparts of the local part,
      // respectively.  The last two groups are nil if the optional local part is not present, which is never the case if MODE is 'local.  If
      // DISABLE-CAPTURE is set to non-nil, there is no capture group.

      // If MODE is either 'nonlocal or 'local, SEQ-SUBPART can be set to a positive integer so that the regular expression only matches a suffix
      // sequence whose nonlocal part if MODE is 'nonlocal or local part if MODE is 'local has the integer as the sequence subpart.
      if (!(mode == nullptr || *mode == suffix_sequence_mode::nonlocal || *mode == suffix_sequence_mode::local)) {
        throw std::runtime_error("MODE is non-nil but is neither 'nonlocal nor 'local");
      }
      if (mode == nullptr && seq_subpart) {
        throw std::runtime_error("SEQ-SUBPART can only be non-nil if MODE is either 'nonlocal or 'local");
      }
      return group((suffix_sequence_part((disable_capture
                                          || !not_captured),
                                         (disable_capture
                                          || !not_captured),
                                         (mode && *mode == suffix_sequence_mode::nonlocal
                                          ? seq_subpart
                                          : nullptr))                                        // nonlocal
                    + (mode && *mode == suffix_sequence_mode::nonlocal
                       ? ""
                       : (suffix_sequence_part((disable_capture
                                                || !not_captured),
                                               (disable_capture
                                                || !not_captured),
                                               (mode && *mode == suffix_sequence_mode::local
                                                ? seq_subpart
                                                : nullptr))                                  // local
                          + (mode && *mode == suffix_sequence_mode::local ? "" : "?")))),
                   disable_capture || not_captured);
    }

    std::string default_function_seq(bool seq_not_captured = false, std::shared_ptr<std::string> seq = nullptr) {
      // Return a regular expression that matches the core identifier of a default function.
      // By default, there is one capture group that captures the sequence subpart of the core identifier, which is formalized by
      // `regex::suffix_sequence_part_seq'.  If SEQ-NOT-CAPTURED is set to non-nil, there is no capture group.
      // If SEQ is set to non-nil, the returned regular expression only matches a core identifier whose sequence subpart is SEQ.
      static const std::regex seq_regex(suffix_sequence_part_seq, std::regex_constants::optimize);
      if (seq && !std::regex_match(*seq, seq_regex)) {
        throw std::runtime_error("`"s + *seq + "' is not the sequence subpart of the core identifier of a default function based on the regex `"
                                 + suffix_sequence_part_seq + "'");
      }
      return group(("_d"s
                    + group((seq
                             ? *seq
                             : suffix_sequence_part_seq),
                            seq_not_captured)),
                   true);
    }

    std::string default_function(bool not_captured = false, bool disable_capture = false, const std::string &core_part_seq = "",
                                 bool require_noncore_part = false) {
      // Return a regular expression that matches the identifier of a default function as a whole, which consists of a noncore part
      // (`regex::suffix_sequence_part') that is optional if REQUIRE-NONCORE-PART is nil and a core part (`regex::default_function_seq').  By
      // default, there is only one capture group that captures the identifier as a whole.  If NOT-CAPTURED is set to non-nil, there are three
      // groups, the last of which is never nil and captures the sequence subpart of the core identifier.  The first two groups are nil if the
      // noncore part does not exist; otherwise, the first two groups capture the sequence and the string subparts of the noncore part,
      // respectively.  If DISABLE-CAPTURE is set to non-nil, there is no capture group.  If CORE-PART-SEQ is set to non-nil, the returned regular
      // expression only matches a default function identifier that has no noncore part but only the specified CORE-PART-SEQ; there is also only one
      // capture group that captures the given CORE-PART-SEQ if NOT-CAPTURED and DISABLE-CAPTURE are both non-nil.
      return group(!core_part_seq.empty()
                   ? (group(alt({std::make_shared<std::string>("^"),
                                 std::make_shared<std::string>(c_ident_next_char(true))}),
                            true)
                      + default_function_seq(disable_capture || !not_captured, std::make_shared<std::string>(core_part_seq)))
                   : (suffix_sequence_part(disable_capture
                                           || !not_captured,
                                           disable_capture
                                           || !not_captured) + (require_noncore_part ? "" : "?") // nonlocal
                      + default_function_seq(disable_capture || !not_captured)),
                   disable_capture || not_captured);
    }

    const std::string template_begin(group("^|[^[:alnum:]_]",
                                           true)
                                     + "template[[:space:]]*<[[:space:]]*");
    // A regular expression that matches the beginning of an associated template declaration.  Note that section [Template Pattern Usage] of HACKING
    // guarantees that `regex::template_begin' and the following invocation of "decl"/"prms" will be in a single physical line (a single logical
    // line, instead, can consist of two or more physical lines terminated with a backslash).

    const std::string prmlst_begin("[[:space:]]*\\(");
    // A regular expression that matches the beginning of a parameter list.  Note that section [Template Pattern Definition] of HACKING guarantees
    // that whatever precedes `regex::prmlst_begin' will be in a single physical line (a single logical line, instead, can consist of two or more
    // physical lines terminated with a backslash).

    //\end{translation region 2A: tice-template-pattern.el}

  };
  namespace comparator {

    struct suffix_sequence : // sorts based on Tice template pattern's multiple inheritance hierarchy that is traversed breadth-first
      std::less<std::string>
    {

      enum part_comparator_result { FALSE, TRUE, UNDECIDABLE};

      static part_comparator_result part_comparator(const std::string &lhs_seq_str, const std::string &rhs_seq_str,
                                                    const std::string &lhs_alpha, const std::string &rhs_alpha) {

        const unsigned lhs_seq = std::stoi(lhs_seq_str);
        const unsigned rhs_seq = std::stoi(rhs_seq_str);
        if (lhs_seq != rhs_seq) {
          return lhs_seq < rhs_seq ? part_comparator_result::TRUE : part_comparator_result::FALSE;
        }

        // lhs_seq == rhs_seq:

        if (lhs_alpha != rhs_alpha) {
          return lhs_alpha < rhs_alpha ? part_comparator_result::TRUE : part_comparator_result::FALSE;
        }

        // lhs_alpha == rhs_alpha:

        return part_comparator_result::UNDECIDABLE;

      };

      static const std::string pattern_string;

      static const std::regex pattern;

      bool operator()(const std::string &lhs, const std::string &rhs) const {

        std::smatch lhs_match;
        std::regex_match(lhs, lhs_match, pattern);
        if (lhs_match.empty()) {
          throw std::runtime_error("LHS suffix sequence fails to match its regex: "s + lhs + " (regex: " + pattern_string + ")");
        }
        std::smatch rhs_match;
        std::regex_match(rhs, rhs_match, pattern);
        if (rhs_match.empty()) {
          throw std::runtime_error("RHS suffix sequence fails to match its regex: "s + rhs + " (regex: " + pattern_string + ")");
        }

        switch (part_comparator(lhs_match[1], rhs_match[1], lhs_match[2], rhs_match[2])) {
        case part_comparator_result::TRUE:
          return true;
        case part_comparator_result::FALSE:
          return false;
        default:
          ;
        }

        const bool lhs_has_local = lhs_match[3].matched;
        const bool rhs_has_local = rhs_match[3].matched;
        if (lhs_has_local != rhs_has_local) {
          return rhs_has_local;
        }

        // lhs_has_local == rhs_has_local:

        if (!lhs_has_local) {
          return false; // lhs == rhs
        }

        // lhs_has_local && rhs_has_local:

        switch (part_comparator(lhs_match[3], rhs_match[3], lhs_match[4], rhs_match[4])) {
        case part_comparator_result::TRUE:
          return true;
        case part_comparator_result::FALSE:
        default: // lhs == rhs
          return false;
        }
      }

    };

    

    const std::string suffix_sequence::pattern_string(regex::suffix_sequence(nullptr, true)
                                                      /* the suffix sequence with two capturing groups for the nonlocal part followed by two
                                                       * optional capturing groups for the local part
                                                       */                          );

    const std::regex suffix_sequence::pattern(pattern_string, std::regex_constants::optimize);

  };
  namespace comparator {

    struct default_function : /* sorts based on Tice template pattern's multiple inheritance hierarchy that is traversed breadth-first and then on
                               * the default parameter sequence in ascending order
                               */
      std::less<std::string>
    {
      static const std::string pattern_string;

      static const std::regex pattern;

      bool operator()(const std::string &lhs, const std::string &rhs) const {

        std::smatch lhs_match;
        std::regex_match(lhs, lhs_match, pattern);
        if (lhs_match.empty()) {
          throw std::runtime_error("LHS default function fails to match its regex: "s + lhs + " (regex: " + pattern_string + ")");
        }
        std::smatch rhs_match;
        std::regex_match(rhs, rhs_match, pattern);
        if (rhs_match.empty()) {
          throw std::runtime_error("RHS default function fails to match its regex: "s + rhs + " (regex: " + pattern_string + ")");
        }

        const bool lhs_has_nonlocal = lhs_match[1].matched;
        const bool rhs_has_nonlocal = rhs_match[1].matched;
        if (lhs_has_nonlocal != rhs_has_nonlocal) {
          return lhs_has_nonlocal;
        }

        // lhs_has_nonlocal == rhs_has_nonlocal:

        if (lhs_has_nonlocal) {
          switch (suffix_sequence::part_comparator(lhs_match[1], rhs_match[1], lhs_match[2], rhs_match[2])) {
          case suffix_sequence::part_comparator_result::TRUE:
            return true;
          case suffix_sequence::part_comparator_result::FALSE:
            return false;
          default:
            ;
          }
        }

        // lhs_nonlocal == rhs_nonlocal || !lhs_has_nonlocal && !rhs_has_nonlocal:

        const unsigned lhs_fn_seq = std::stoi(lhs_match[3]);
        const unsigned rhs_fn_seq = std::stoi(rhs_match[3]);
        return lhs_fn_seq < rhs_fn_seq;

      }

    };

    

    const std::string default_function::pattern_string(regex::default_function(true)
                                                      /* the suffix sequence with two optional capturing groups for the nonlocal part followed by
                                                       * one capturing group for the function sequence
                                                       */                            );

    const std::regex default_function::pattern(pattern_string, std::regex_constants::optimize);

  };
};

int main(int argc, char *argv[]) {

  if (argc < 4) {
    std::cerr << "Usage: " << basename(argv[0]) << " PREAMBLE_FILE POSTAMBLE_FILE FILE FILE...\n"
              << "  PREAMBLE_FILE is a path to a Tice internal unit file whose name is\n"
              << "    formatted as VERSION_internals_begin.hpp\n"
              << "  POSTAMBLE_FILE is a path to a Tice internal unit file whose name is\n"
              << "    formatted as VERSION_internals_end.hpp\n"
              << "  FILE is a path to a Tice internal unit file other than\n"
              << "    PREAMBLE_FILE and POSTAMBLE_FILE\n"
              << "\n"
              << "WARNING: Both PREAMBLE_FILE and POSTAMBLE_FILE will be updated (_overwritten_)\n"
              << "         based on the given FILEs with the following invariant: either\n"
              << "         PREAMBLE_FILE or POSTAMBLE_FILE, but not both, may be corrupted during\n"
              << "         the update _if and only if_ the program raises an error that says so\n"
              << "\n"
              << "Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>\n"
              << "                                           (GPG public key ID: 0x277B48A6)\n";
    return EXIT_FAILURE;
  }

  const std::string preamble_file_path(argv[1]);
  const std::string postamble_file_path(argv[2]);

  std::set<std::string, comparator::suffix_sequence> distinct_suffix_sequences;
  std::set<std::string, comparator::default_function> distinct_default_functions;
  {
    for (unsigned i = 3; i < argc; ++i) {
      std::ifstream input_file(argv[i], std::ios_base::binary);

      try {
        load_file_linewise(input_file,

                           [&distinct_suffix_sequences, &distinct_default_functions](std::string &line_buffer) {
                             static const std::regex
                               pattern((regex::group((""s
                                                      + (// suffix sequence pattern:
                                                         regex::def_begin + "args" // `args' is used by both nonlocal and local template patterns
                                                         + regex::suffix_sequence() /* the suffix sequence with one capturing group */)
                                                      + "|"
                                                      + (// default function pattern:
                                                         regex::quasi_bol
                                                         + regex::group((regex::template_begin + "decl"
                                                                         + regex::suffix_sequence_part(true, true)
                                                                         + regex::prmlst_begin
                                                                         + "[[:space:]]*"),
                                                                        true) + "?"
                                                         + regex::default_function() /* the default function with one capturing group */)),
                                                     true)
                                        + regex::prmlst_begin), // both patterns are a function-like macro invocation pattern
                                       std::regex_constants::optimize);

                             std::smatch result;
                             if (std::regex_search(line_buffer, result, pattern)) {
                               if (result[1].matched) {
                                 distinct_suffix_sequences.insert(result[1]);
                               } else if (result[2].matched) {
                                 distinct_default_functions.insert(result[2]);
                               }
                             }

                             return true;

                           });

      } catch (const std::exception &e) {
        std::cerr << "Error: failure in processing file `" << argv[i] << "': " << e.what() << std::endl;
        return EXIT_FAILURE;
      }

    }
  }

  std::pair<std::string, std::string::const_iterator> preamble;
  {
    std::ifstream input_file(preamble_file_path, std::ios_base::binary);
    try {
      preamble = extract_prefix_suffix(input_file);
    } catch (const std::exception &e) {
      std::cerr << "Error: cannot extract PREAMBLE_FILE: " << e.what() << std::endl;;
      return EXIT_FAILURE;
    }
  }
  std::pair<std::string, std::string::const_iterator> postamble;
  {
    std::ifstream input_file(postamble_file_path, std::ios_base::binary);
    try {
      postamble = extract_prefix_suffix(input_file);
    } catch (const std::exception &e) {
      std::cerr << "Error: cannot extract POSTAMBLE_FILE: " << e.what() << std::endl;;
      return EXIT_FAILURE;
    }
  }

  {
    static const std::regex nonlocal_only_pattern(regex::suffix_sequence_part(true, true), std::regex_constants::optimize);

    // Update PREAMBLE_FILE:
    try {
      std::ofstream output_file(preamble_file_path, std::ios_base::binary);

      output_file << std::left;

      std::copy(preamble.first.cbegin(), preamble.second, std::ostream_iterator<char>(output_file));

      for (auto itr = distinct_default_functions.begin(), itr_end = distinct_default_functions.end(); itr != itr_end; ++itr) {
        std::string line("#define "s + *itr + "(...)");
        output_file << "#ifdef " << *itr << '\n'
                    << "#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation\n"
                    << "#else\n"
                    << std::setw(c_backslash_max_column - 1) << line << "\\\n"
                    << "  (= __VA_ARGS__)\n"
                    << "#endif\n";
      }

      output_file << '\n';

      output_file << std::setw(c_backslash_max_column - 1) << "#if (false" << "\\\n";
      for (auto itr = distinct_suffix_sequences.begin(), itr_end = distinct_suffix_sequences.end(); itr != itr_end; ++itr) {
        std::string line("     "s
                         + (std::regex_match(*itr, nonlocal_only_pattern)
                            ? "|| defined(base"s + *itr + ") " + "|| defined(decl" + *itr + ") "
                            : "")
                         + "|| defined(args" + *itr + ") " + "|| defined (prms" + *itr + ")");
        output_file << std::setw(c_backslash_max_column - 1) << line << "\\\n";
      }
      output_file << "     )\n"
                  << "#error [DEBUG] Tice template pattern facility has not been deactivated prior to reactivation\n"
                  << "#endif\n";

      std::copy(preamble.second, preamble.first.cend(), std::ostream_iterator<char>(output_file));

    } catch (const std::exception &e) {
      std::cerr << "Error: PREAMBLE_FILE may be corrupted by this update failure: " << e.what() << std::endl;;
      return EXIT_FAILURE;
    }

    // Update POSTAMBLE_FILE:
    try {
      std::ofstream output_file(postamble_file_path, std::ios_base::binary);

      output_file << std::left;

      std::copy(postamble.first.cbegin(), postamble.second, std::ostream_iterator<char>(output_file));

      for (auto itr = distinct_default_functions.begin(), itr_end = distinct_default_functions.end(); itr != itr_end; ++itr) {
        output_file << "#undef " << *itr << '\n';
      }

      for (auto itr = distinct_suffix_sequences.begin(), itr_end = distinct_suffix_sequences.end(); itr != itr_end; ++itr) {
        output_file << '\n';
        if (std::regex_match(*itr, nonlocal_only_pattern)) {
          output_file << "#undef base" << *itr << '\n'
                      << "#undef decl" << *itr << '\n';
        }
        output_file << "#undef args" << *itr << '\n'
                    << "#undef prms" << *itr << '\n';
      }

      std::copy(postamble.second, postamble.first.cend(), std::ostream_iterator<char>(output_file));

    } catch (const std::exception &e) {
      std::cerr << "Error: PREAMBLE_FILE may be corrupted by this update failure: " << e.what() << std::endl;;
      return EXIT_FAILURE;
    }
  }

}
