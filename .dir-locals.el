;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 ;;
;;                                            (GPG public key ID: 0x277B48A6) ;;
;;                                                                            ;;
;; This program is free software: you can redistribute it and/or modify       ;;
;; it under the terms of the GNU General Public License as published by       ;;
;; the Free Software Foundation, either version 3 of the License, or          ;;
;; (at your option) any later version.                                        ;;
;;                                                                            ;;
;; This program is distributed in the hope that it will be useful,            ;;
;; but WITHOUT ANY WARRANTY; without even the implied warranty of             ;;
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              ;;
;; GNU General Public License for more details.                               ;;
;;                                                                            ;;
;; You should have received a copy of the GNU General Public License          ;;
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((c++-mode
  (before-save-hook whitespace-cleanup)
  (page-delimiter . "^[[:space:]]*")
  (indent-tabs-mode)
  (c-backslash-max-column . 147) ; the value must be one less than that of fill-column because the value is the zero-based index of the last column
                                 ; allowed by the value of fill-column
  (fill-column . 148))
 (emacs-lisp-mode
  (before-save-hook whitespace-cleanup)
  (fill-column . 148)
  (emacs-lisp-docstring-fill-column . 148))
 (sh-mode
  (fill-column . 148))
 (text-mode
  (fill-column . 80))
 (".gitignore"
  (fundamental-mode
   (eval . (text-mode))))
 ("HACKING"
  (fundamental-mode
   (eval . (text-mode))))
 ("LICENSE"
  (fundamental-mode
   (eval . (text-mode))))
 ("README"
  (fundamental-mode
   (eval . (text-mode))))
 ("experiments"
  (fundamental-mode
   (eval . (cond ((string-match "^README$"
                                (file-name-nondirectory (buffer-file-name)))
                  (text-mode))
                 ((string-match "\\(^\\(v[[:digit:]]+-\\)?run_common\\|\\.gnuplot\\)$"
                                (file-name-nondirectory (buffer-file-name)))
                  (sh-mode))))))
 ("devtools/bin"
  (fundamental-mode
   (eval . (cond ((string-match "^current-tice-version-number$"
                                (file-name-nondirectory (buffer-file-name)))
                  (sh-mode))))
   (subdirs . nil)))
 ("tests"
  (fundamental-mode
   (eval . (cond ((string-match "^v[[:digit:]]+_[[:alnum:]]+_test\\(-.+\\)?$"
                                (file-name-nondirectory (buffer-file-name)))
                  (sh-mode))))
   (subdirs . nil))
  (nroff-mode
   (eval . (cond ((string-match "^v[[:digit:]]+_[[:alnum:]]+_test\\(-.+\\)?$"
                                (file-name-nondirectory (buffer-file-name)))
                  (sh-mode))))
   (subdirs . nil))))
