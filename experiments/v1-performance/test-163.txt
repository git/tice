 ##############################################################################
 # Copyright (C) 2020  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

# The following is exactly identical to that of test-160.txt except for the
# application of one end-to-end delay constraint and one correlation constraints
# on the only source and sink nodes.

v1(5/1000)    # Altitude sensor (h_s)
v2(5/1000)    # Vertical acceleration sensor (a_z_s)
v3(5/1000)    # Pitch rate sensor (q_s)
v4(5/1000)    # Vertical speed sensor (V_z_s)
v5(5/1000)    # True airspeed sensor (V_a_s)
v6(10/1000)   # Altitude filter (h_f)
v7(10/1000)   # Vertical acceleration filter (a_z_f)
v8(10/1000)   # Pitch rate filter (q_f)
v9(10/1000)   # Vertical speed filter (V_z_f)
v10(10/1000)  # Airspeed filter (V_a_f)
v11(20/1000)  # Altitude hold (h_h)
v12(20/1000)  # Altitude control (V_z)
v13(20/1000)  # Airspeed control (V_a)
v14(5/1000)   # Elevator actuator (L)
v15(5/1000)   # Engine thrust actuator (E)
v16(5/1000)   # A single source node
v17(5/1000)   # A single sink node
E(v1, v6)
E(v2, v7)
E(v3, v8)
E(v4, v9)
E(v5, v10)
E(v6, v11)
E(v7, v8, v9, v11, v12)
E(v8, v9, v10, v13)
E(v12, v14)
E(v13, v15)
E(v16, v1)
E(v16, v2)
E(v16, v3)
E(v16, v4)
E(v16, v5)
E(v14, v15, v17)
D(v16, v17)
C(v17, v16)
