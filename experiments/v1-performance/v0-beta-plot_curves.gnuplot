 ##############################################################################
 # Copyright (C) 2020  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

# Input: data_file as a string
#        validation_id as an int
#        quadratic_1_measurement_column_idx as an int
#        quadratic_2_measurement_column_idx as an int
#        linear_measurement_column_idx as an int
# Output: eight real variables, namely FE_A,    FE_B,    FE_C,
#                                      FE_BE_A, FE_BE_B, FE_BE_C,
#                                      BE_M,    BE_C
# Reserved variables: a_1, b_1, d_1,
#                     a_2, b_2, d_2,
#                     s,
#                     measurement_pattern,
#                     measurement_extraction_cmd,
#                     measurement_0,
#                     measurement_1,
#                     measurement_2.

measurement_pattern = "\\( \\([[:digit:].]\\+\\)\\(([[:digit:].]\\+)\\)\\?\\)"
measurement_extraction_cmd(test_id, measurement_column, data_file) \
  = sprintf("sed -n 's@^\\[[^]]\\+\\]\\[test-%d\\.txt\\]" . measurement_pattern . "\\{%d\\}" . measurement_pattern . ".*@\\5@p' %s", \
            test_id, measurement_column, data_file)

test_id_0 = 200 + (validation_id - 1) * 10
test_id_1 = 204 + (validation_id - 1) * 10
test_id_2 = 207 + (validation_id - 1) * 10

a_1 = real(n_2 * n_2 - n_0 * n_0);
b_1 = real(n_2 - n_0);
a_2 = real(n_2 * n_2 - n_1 * n_1);
b_2 = real(n_2 - n_1);
s = b_2 / b_1;

measurement_0 = real(system(measurement_extraction_cmd(test_id_0, quadratic_1_measurement_column_idx, data_file)))
measurement_1 = real(system(measurement_extraction_cmd(test_id_1, quadratic_1_measurement_column_idx, data_file)))
measurement_2 = real(system(measurement_extraction_cmd(test_id_2, quadratic_1_measurement_column_idx, data_file)))
d_1 = measurement_2 - measurement_0;
d_2 = measurement_2 - measurement_1;
FE_A = (s * d_1 - d_2) / (s * a_1 - a_2);
FE_B = (d_1 - a_1 * FE_A) / b_1;
FE_C = measurement_0 - FE_A * n_0 * n_0 - FE_B * n_0;

measurement_0 = real(system(measurement_extraction_cmd(test_id_0, quadratic_2_measurement_column_idx, data_file)))
measurement_1 = real(system(measurement_extraction_cmd(test_id_1, quadratic_2_measurement_column_idx, data_file)))
measurement_2 = real(system(measurement_extraction_cmd(test_id_2, quadratic_2_measurement_column_idx, data_file)))
d_1 = measurement_2 - measurement_0;
d_2 = measurement_2 - measurement_1;
FE_BE_A = (s * d_1 - d_2) / (s * a_1 - a_2);
FE_BE_B = (d_1 - a_1 * FE_BE_A) / b_1;
FE_BE_C = measurement_0 - FE_BE_A * n_0 * n_0 - FE_BE_B * n_0;

measurement_0 = real(system(measurement_extraction_cmd(test_id_0, linear_measurement_column_idx, data_file)))
measurement_2 = real(system(measurement_extraction_cmd(test_id_2, linear_measurement_column_idx, data_file)))
BE_M = (measurement_2 - measurement_0) / (n_2 - n_0);
BE_C = measurement_0 - BE_M * n_0;
