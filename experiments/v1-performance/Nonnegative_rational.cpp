/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <limits>
#include <cmath>

#include "Nonnegative_rational.hpp"

namespace {

  unsigned long long gcd(unsigned long long a, unsigned long long b) {
    unsigned long long r = a % b;
    while (r != 0) {
      a = b, b = r, r = a % b;
    }
    return b;
  }

}

namespace tice {
  namespace v1 {

    Nonnegative_rational::Nonnegative_rational(unsigned long long num, unsigned long long den) :
      num(num), den(den), gcd(0)
    {
      static const unsigned long long max = static_cast<unsigned long long>(std::sqrt(std::numeric_limits<unsigned long long>::max()));
      if (den == 0) {
        throw Invalid_den(den);
      }
      if (num > max) {
        throw Num_out_of_range(num, max);
      }
      if (den > max) {
        throw Den_out_of_range(den, max);
      }
    }

    Nonnegative_rational::Nonnegative_rational(const Nonnegative_rational &o) noexcept :
      num(o.num), den(o.den), gcd(o.gcd) {
    }

    Nonnegative_rational &Nonnegative_rational::operator =(const Nonnegative_rational &rhs) noexcept {
      num = rhs.num;
      den = rhs.den;
      gcd = rhs.gcd;
      return *this;
    }

    Nonnegative_rational::operator double() const noexcept {
      return num / static_cast<double>(den);
    }

    unsigned long long Nonnegative_rational::get_num() const noexcept {
      return num;
    }

    unsigned long long Nonnegative_rational::get_den() const noexcept {
      return den;
    }

    unsigned long long Nonnegative_rational::get_gcd() noexcept {
      if (gcd == 0) {
        gcd = ::gcd(num, den);
      }
      return gcd;
    }

    Nonnegative_rational Nonnegative_rational::simplify() const noexcept {
      Nonnegative_rational result(num, den);
      if (result.get_gcd() != 1) {
        result.num /= result.gcd;
        result.den /= result.gcd;
        result.gcd = 1;
      }
      return result;
    }

    bool Nonnegative_rational::is_zero() const noexcept {
      return num == 0;
    }

    Nonnegative_rational operator +(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs) {
      if (lhs.is_zero()) {
        return rhs.simplify();
      } else if (rhs.is_zero()) {
        return lhs.simplify();
      }
      Nonnegative_rational lhs_simplified(lhs.simplify());
      Nonnegative_rational rhs_simplified(rhs.simplify());
      unsigned long long lcm = (lhs_simplified.get_den() * rhs_simplified.get_den()
                                / gcd(lhs_simplified.get_den(), rhs_simplified.get_den()));
      unsigned long long lhs_multiplier = lcm / lhs_simplified.get_den();
      unsigned long long rhs_multiplier = lcm / rhs_simplified.get_den();
      return Nonnegative_rational(lhs_multiplier * lhs_simplified.get_num() + rhs_multiplier * rhs_simplified.get_num(),
                                  lcm).simplify();
    }

    Nonnegative_rational operator -(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs) {
      if (lhs.is_zero() && !rhs.is_zero()) {
        throw Nonnegative_rational::Subtractor_too_large();
      } else if (rhs.is_zero()) {
        return lhs.simplify();
      }
      Nonnegative_rational lhs_simplified(lhs.simplify());
      Nonnegative_rational rhs_simplified(rhs.simplify());
      unsigned long long lcm = (lhs_simplified.get_den() * rhs_simplified.get_den()
                                / gcd(lhs_simplified.get_den(), rhs_simplified.get_den()));
      unsigned long long lhs_multiplier = lcm / lhs_simplified.get_den();
      unsigned long long rhs_multiplier = lcm / rhs_simplified.get_den();
      if (lhs_multiplier * lhs_simplified.get_num() < rhs_multiplier * rhs_simplified.get_num()) {
        throw Nonnegative_rational::Subtractor_too_large();
      }
      return Nonnegative_rational(lhs_multiplier * lhs_simplified.get_num() - rhs_multiplier * rhs_simplified.get_num(),
                                  lcm).simplify();
    }

    Nonnegative_rational operator *(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs) {
      if (lhs.is_zero() || rhs.is_zero()) {
        return {0};
      }
      Nonnegative_rational lhs_simplified(lhs.simplify());
      Nonnegative_rational rhs_simplified(rhs.simplify());
      unsigned long long gcd_lhs_den_rhs_num = gcd(lhs_simplified.get_den(), rhs_simplified.get_num());
      unsigned long long gcd_lhs_num_rhs_den = gcd(lhs_simplified.get_num(), rhs_simplified.get_den());
      return Nonnegative_rational((lhs_simplified.get_num() / gcd_lhs_num_rhs_den) * (rhs_simplified.get_num() / gcd_lhs_den_rhs_num),
                                  (lhs_simplified.get_den() / gcd_lhs_den_rhs_num) * (rhs_simplified.get_den() / gcd_lhs_num_rhs_den)).simplify();
    }

    Nonnegative_rational operator /(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs) {
      if (rhs.is_zero()) {
        throw Nonnegative_rational::Divide_by_zero();
      }
      return (lhs * Nonnegative_rational(rhs.get_den(), rhs.get_num())).simplify();
    }

    Nonnegative_rational operator %(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs) {
      if (rhs.is_zero()) {
        throw Nonnegative_rational::Divide_by_zero();
      }
      Nonnegative_rational division_result = (lhs / rhs).simplify();
      return Nonnegative_rational(division_result.get_num() % division_result.get_den(),
                                  division_result.get_den()).simplify();
    }

    Nonnegative_rational &Nonnegative_rational::operator +=(const Nonnegative_rational &rhs) {
      return *this = *this + rhs;
    }

    Nonnegative_rational &Nonnegative_rational::operator *=(const Nonnegative_rational &rhs) {
      return *this = *this * rhs;
    }

    bool operator ==(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs) {
      if (lhs.is_zero() && rhs.is_zero()) {
        return true;
      } else if (lhs.is_zero() || rhs.is_zero()) {
        return false;
      }
      Nonnegative_rational division_result = lhs / rhs;
      return division_result.get_num() == division_result.get_den();
    }

    bool operator <(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs) {
      if (lhs.is_zero() && rhs.is_zero()) {
        return false;
      } else if (lhs.is_zero() && !rhs.is_zero()) {
        return true;
      } else if (!lhs.is_zero() && rhs.is_zero()) {
        return false;
      }
      Nonnegative_rational division_result = lhs / rhs;
      return division_result.get_num() < division_result.get_den();
    }

    bool operator <=(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs) {
      return lhs < rhs || lhs == rhs;
    }

    bool operator >(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs) {
      return !(lhs <= rhs);
    }

    bool operator >=(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs) {
      return !(lhs < rhs);
    }

    bool operator !=(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs) {
      return !(lhs == rhs);
    }

    std::ostream &operator <<(std::ostream &out, const Nonnegative_rational &o) {
      return out << to_string(o);
    }

    namespace {

      enum floor_or_ceil_op {
        FLOOR,
        CEIL
      };
      unsigned long long floor_or_ceil(floor_or_ceil_op op, const Nonnegative_rational &o, const unsigned long long &scale) {
        const Nonnegative_rational ensure_scale_will_not_cause_an_overflow_by_the_constructor_enforced_invariant(scale);
        return o.get_num() * scale / o.get_den() + (op && o.get_num() * scale % o.get_den());
      }

    }

    unsigned long long floor(const Nonnegative_rational &o, const unsigned long long &scale) {
      return floor_or_ceil(floor_or_ceil_op::FLOOR, o, scale);
    }

    unsigned long long ceil(const Nonnegative_rational &o, const unsigned long long &scale) {
      return floor_or_ceil(floor_or_ceil_op::CEIL, o, scale);
    }

    std::string to_string(const Nonnegative_rational &o) {
      if (o.get_den() == 1) {
        return std::to_string(o.get_num());
      } else {
        return std::to_string(o.get_num()) + '/' + std::to_string(o.get_den());
      }
    }

  }
}
