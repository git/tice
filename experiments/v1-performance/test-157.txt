 ##############################################################################
 # Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

v1(1)
v2(10)
v3(100)
v4(1000)
v5(1)
v6(10)
v7(100)
v8(1000)
v9(1)
v10(10)
v11(100)
v12(1000)
v13(1)
v14(10)
v15(100)
v16(1000)
v17(1)
v18(10)
v19(100)
v20(1000)
v21(1)
v22(10)
v23(100)
v24(1000)
v25(1)
v26(10)
v27(100)
E(v26, v27)
E(v25, v26)
E(v24, v25)
E(v23, v24)
E(v22, v23)
E(v21, v22)
E(v20, v21)
E(v19, v20)
E(v18, v19)
E(v17, v18)
E(v16, v17)
E(v15, v16)
E(v14, v15)
E(v13, v14)
E(v12, v13)
E(v11, v12)
E(v10, v11)
E(v9, v10)
E(v8, v9)
E(v7, v8)
E(v1, v2, v3, v4, v5, v6, v7)
E(v1, v2, v3, v4, v5, v6)
E(v1, v2, v3, v4, v5)
E(v1, v2, v3, v4)
E(v1, v2, v3)
E(v1, v2)
C(v27, v1)
