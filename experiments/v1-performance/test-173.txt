 ##############################################################################
 # Copyright (C) 2020  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

# The following is exactly identical to that of test-170.txt except for the
# application of one end-to-end delay constraint and one correlation constraints
# on the only source and sink nodes.

v1(1)    # Task ISR_10.  A sporadic task with a minimum interarrival time of 0.7 ms.
v2(1)    # Task ISR_5.   A sporadic task with a minimum interarrival time of 0.9 ms.
v3(1)    # Task ISR_6.   A sporadic task with a minimum interarrival time of 1.1 ms.
v4(1)    # Task ISR_4.   A sporadic task with a minimum interarrival time of 1.5 ms.
v5(1)    # Task ISR_8.   A sporadic task with a minimum interarrival time of 1.7 ms.
v6(5)    # Task ISR_7.   A sporadic task with a minimum interarrival time of 4.9 ms.
v7(5)    # Task ISR_11.  A sporadic task with a minimum interarrival time of 5   ms.
v8(5)    # Task ISR_9.   A sporadic task with a minimum interarrival time of 6   ms.
v9(10)   # Task ISR_1.   A sporadic task with a minimum interarrival time of 9.5 ms.
v10(10)  # Task ISR_2.   A sporadic task with a minimum interarrival time of 9.5 ms.
v11(10)  # Task ISR_3.   A sporadic task with a minimum interarrival time of 9.5 ms.
v12(1)   # Task T_1ms.       A periodic task with a period of    1    ms.
v13(5)   # Task Angle_Sync.  A periodic task with a period of    6.66 ms.
v14(2)   # Task T_2ms.       A periodic task with a period of    2    ms.
v15(5)   # Task T_5ms.       A periodic task with a period of    5    ms.
v16(10)  # Task T_10ms.      A periodic task with a period of   10    ms.
v17(20)  # Task T_20ms.      A periodic task with a period of   20    ms.
v18(50)  # Task T_50ms.      A periodic task with a period of   50    ms.
v19(100) # Task T_100ms.     A periodic task with a period of  100    ms.
v20(200) # Task T_200ms.     A periodic task with a period of  200    ms.
v21(200) # Task T_1000ms.    A periodic task with a period of 1000    ms.
v22(1)   # A single source node
v23(1)   # A single sink node
E(v2, v7, v12)
E(v12, v16, v19, v13)
E(v1, v14)
E(v4, v5, v15)
E(v3, v8, v9, v10, v11, v16)
E(v12, v14, v15, v16, v19, v17)
E(v12, v14, v15, v16, v19, v18)
E(v6, v19)
E(v16, v19, v20)
E(v16, v19, v21)
E(v22, v11)
E(v22, v10)
E(v22, v9)
E(v22, v7)
E(v22, v6)
E(v22, v8)
E(v22, v3)
E(v22, v1)
E(v22, v4)
E(v22, v2)
E(v22, v5)
E(v13, v17, v18, v20, v21, v23)
D(v22, v23)
C(v23, v22)
