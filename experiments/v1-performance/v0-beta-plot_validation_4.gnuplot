#!/usr/bin/gnuplot

 ##############################################################################
 # Copyright (C) 2020  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

#\begin{settings}
validation_id = 4

x_delta = 3
yrange_min = .5
yrange_max = 256

n_0 = 9
n_2 = 30

left_margin = 6.8          # char
right_margin = 11.7        # char
left_label_x_pos = 7.15
#\end{settings}

load "v0-beta-plot-common.gnuplot"

set logscale y 2

set xrange [n_0:n_2]
set yrange [yrange_min:yrange_max]
set xtics n_0, x_delta offset 0,.65

skip_count = gcc_first_measurement_line_number - 1 + (validation_id - 1) * test_count_per_validation
data_file = gcc_data_file

set label "Average and standard deviation (parenthesized) of compilation time in seconds (log_2 scale)" at left_label_x_pos, screen 0.5 center rotate
unset xlabel
unset ylabel
set key left Left reverse samplen 2
plot \
    for [column in "3 2 4"] data_file skip skip_count using (x_delta * $0 + n_0):(column((int(column)))) with points \
    title (column == 4 ? "Tice back-end executed by GCC" : column == 2 ? "Tice front-end executed by GCC" : "Tice front-end and back-end executed by GCC"), \
    data_file skip skip_count using ($0 < 2 ? x_delta * $0 + n_0 : -1):3:3 notitle with labels left offset character .25, .7 font data_label_font, \
    data_file skip skip_count using (2 <= $0 && $0 < 5 ? x_delta * $0 + n_0 : -1):3:3 notitle with labels offset character 0, .8 font data_label_font, \
    data_file skip skip_count using ($0 == 5 ? x_delta * $0 + n_0 : -1):3:3 notitle with labels offset character 2, 1 font data_label_font, \
    data_file skip skip_count using ($0 == 6 ? x_delta * $0 + n_0 : -1):3:3 notitle with labels offset character 3, 1 font data_label_font, \
    data_file skip skip_count using ($0 == 7 ? x_delta * $0 + n_0 : -1):3:3 notitle with labels left offset character .5, 1 font data_label_font, \
    data_file skip skip_count using ($0 < 3 ? x_delta * $0 + n_0 : -1):2:2 notitle with labels left offset character 1.25, -.5 font data_label_font, \
    data_file skip skip_count using (3 <= $0 && $0 < 5 ? x_delta * $0 + n_0 : -1):2:2 notitle with labels offset character 0, -.7 font data_label_font, \
    data_file skip skip_count using ($0 == 5 ? x_delta * $0 + n_0 : -1):2:2 notitle with labels offset character 2, -.7 font data_label_font, \
    data_file skip skip_count using ($0 == 6 ? x_delta * $0 + n_0 : -1):2:2 notitle with labels offset character 3, -.7 font data_label_font, \
    data_file skip skip_count using ($0 == 7 ? x_delta * $0 + n_0 : -1):2:2 notitle with labels left offset character .5, -1 font data_label_font, \
    2**((x - n_0) / real(x_delta) + 1) title sprintf("2^{(x - %d)/%d + 1}", n_0, x_delta) with lines linetype 1

skip_count = clang_first_measurement_line_number - 1 + (validation_id - 1) *  test_count_per_validation
data_file = clang_data_file

unset label
set xlabel "Arc count" offset 0, 1
set xtics n_0, x_delta format ""
set key left samplen 2
plot \
    for [column in "3 2 4"] data_file skip skip_count using (x_delta * $0 + n_0):(column((int(column)))) with points \
    title (column == 4 ? "Tice back-end executed by Clang" : column == 2 ? "Tice front-end executed by Clang" : "Tice front-end and back-end executed by Clang"), \
    data_file skip skip_count using ($0 < 2 ? x_delta * $0 + n_0 : -1):3:3 notitle with labels left offset character .25, .7 font data_label_font, \
    data_file skip skip_count using (2 <= $0 && $0 < 5 ? x_delta * $0 + n_0 : -1):3:3 notitle with labels offset character 0, 1 font data_label_font, \
    data_file skip skip_count using ($0 == 5 ? x_delta * $0 + n_0 : -1):3:3 notitle with labels offset character 2, 1 font data_label_font, \
    data_file skip skip_count using ($0 == 6 ? x_delta * $0 + n_0 : -1):3:3 notitle with labels offset character 3, 1 font data_label_font, \
    data_file skip skip_count using ($0 == 7 ? x_delta * $0 + n_0 : -1):3:3 notitle with labels left offset character .5, 1 font data_label_font, \
    data_file skip skip_count using ($0 < 3 ? x_delta * $0 + n_0 : -1):2:2 notitle with labels left offset character 1.25, -.5 font data_label_font, \
    data_file skip skip_count using (3 <= $0 && $0 < 5 ? x_delta * $0 + n_0 : -1):2:2 notitle with labels offset character 0, -.7 font data_label_font, \
    data_file skip skip_count using ($0 == 5 ? x_delta * $0 + n_0 : -1):2:2 notitle with labels offset character 2, -.7 font data_label_font, \
    data_file skip skip_count using ($0 == 6 ? x_delta * $0 + n_0 : -1):2:2 notitle with labels offset character 3, -.7 font data_label_font, \
    data_file skip skip_count using ($0 == 7 ? x_delta * $0 + n_0 : -1):2:2 notitle with labels left offset character .5, -1 font data_label_font, \
    2**((x - n_0) / real(x_delta) + 1) title sprintf("2^{(x - %d)/%d + 1}", n_0, x_delta) with lines linetype 1

unset multiplot
