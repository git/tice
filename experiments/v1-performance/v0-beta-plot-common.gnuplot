#!/usr/bin/gnuplot

 ##############################################################################
 # Copyright (C) 2020  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

# Input: validation_id as an int
#        left_margin as a double
#        right_margin as a double

gcc_data_file = "v0-beta-data-gcc.txt"     # must be sorted on the test IDs, which are between 200 and 259, inclusive
gcc_first_measurement_line_number = int(system("grep -hnm1 '^\\[[^]]\\+\\]\\[test-200\\.txt\\]' " . gcc_data_file . " | sed 's@^\\([^:]\\+\\).*@\\1@'"))

clang_data_file = "v0-beta-data-clang.txt" # must be sorted on the test IDs, which are between 200 and 259, inclusive
clang_first_measurement_line_number = int(system("grep -hnm1 '^\\[[^]]\\+\\]\\[test-200\\.txt\\]' " . clang_data_file . " | sed 's@^\\([^:]\\+\\).*@\\1@'"))

test_count_per_validation = 8

data_label_font = "STIX,9"

curves_plotter_file = "v0-beta-plot_curves.gnuplot"

set terminal epscairo font data_label_font dashed size 8.43 cm, 8 cm
set output sprintf("v0-beta-validation_%d.eps", validation_id)

set multiplot layout 2,1 margins char left_margin, right_margin, .85, 0.2 spacing char 9, .75

set linetype 1 linecolor black dashtype 2 pointtype 3
set linetype 2 linecolor black dashtype 3 pointtype 2
set linetype 3 linecolor black pointtype 7 pointsize .5
