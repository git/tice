 ##############################################################################
 # Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 #
 #                                            (GPG public key ID: 0x277B48A6) #
 #                                                                            #
 # This program is free software: you can redistribute it and/or modify       #
 # it under the terms of the GNU General Public License as published by       #
 # the Free Software Foundation, either version 3 of the License, or          #
 # (at your option) any later version.                                        #
 #                                                                            #
 # This program is distributed in the hope that it will be useful,            #
 # but WITHOUT ANY WARRANTY; without even the implied warranty of             #
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
 # GNU General Public License for more details.                               #
 #                                                                            #
 # You should have received a copy of the GNU General Public License          #
 # along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
 ##############################################################################

v1(1)
v2(1)
v3(1)
v4(1)
v5(1)
v6(1)
v7(1)
v8(1)
v9(1)
v10(1)
v11(1)
v12(1)
v13(1)
v14(1)
v15(1)
E(v1, v2)
E(v1, v2, v3)
E(v1, v2, v3, v4)
E(v3, v4, v5)
E(v4, v5, v6)
E(v5, v6, v7)
E(v6, v7, v8)
E(v7, v8, v9)
E(v8, v9, v10)
E(v9, v10, v11)
E(v10, v11, v12)
E(v11, v12, v13)
E(v12, v13, v14)
E(v13, v14, v15)
D(v1, v15)
