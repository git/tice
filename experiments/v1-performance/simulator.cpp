/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <iostream>
#include <string>
#include <regex>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <vector>
#include <algorithm>
#include <memory>
#include <utility>
#include <fstream>
#include <exception>
#include <type_traits>
#include <list>
#include <functional>
#include <cstdlib>

#include "Nonnegative_rational.hpp"

using tice::v1::Nonnegative_rational;

namespace {
  Nonnegative_rational simulation_time(0);
}
namespace {

  typedef unsigned long Node_id;

  typedef unsigned Node_name;

  class Invalid_node_name :
    public std::exception
  {
  public:
    static constexpr Node_name value = 0;

    Invalid_node_name() noexcept :
      std::exception() {
    }

    Invalid_node_name(const Invalid_node_name &src) noexcept :
      std::exception(src) {
    }

    Invalid_node_name &operator =(const Invalid_node_name &rhs) noexcept {
      std::exception::operator =(rhs);
      return *this;
    }

    const char *what() const noexcept override {
      return "Node name is invalid (i.e., not a positive integer)";
    }
  };

  std::string to_string(const Node_name &node_name) {
    if (node_name == Invalid_node_name::value) {
      throw Invalid_node_name();
    }
    return std::string("v") + std::to_string(node_name);
  }

  class Node;

  class Mailbox;

  typedef std::vector<Node> Node_database;

  typedef std::unordered_map<Node_name, Node_id> Node_name_index;

  class Node {
    Node_id id;
    Node_name name;
    Nonnegative_rational period;
    Nonnegative_rational release_time;
    bool is_producer;
    bool is_consumer;
    bool is_isolated;
    bool is_source;
    bool is_sink;
    std::unique_ptr<Mailbox> memory;
    std::unique_ptr<Mailbox> output;
  public:
    static bool is_simulation_stopped;
    static std::unordered_set<Node_id> source_nodes;
    static std::unordered_set<Node_id> sink_nodes;
    static Nonnegative_rational min_period;
  public:
    unsigned topological_order;
    std::unordered_set<Node_id> parents;
    typedef decltype(parents.cbegin()) parents_const_itr;
    std::unordered_set<Node_id> children;
    typedef decltype(children.cbegin()) children_const_itr;
    std::unordered_set<Node_id> reachable_sink_nodes;
    std::unordered_set<Node_id> reachable_source_nodes;
  public:
    Node(const Node_id &id, const Node_name &name, const Nonnegative_rational &period) noexcept;
    Node_id get_id() const noexcept;
    Node_name get_name() const noexcept;
    Nonnegative_rational get_period() const noexcept;
    Nonnegative_rational get_release_time() const noexcept;
    void schedule();
    void mark_producer() noexcept;
    void mark_consumer() noexcept;
    bool is_sensor_node() const noexcept;
    bool is_actuator_node() const noexcept;
    void initialize_memory_and_output_buffer();
    void read(Node_database &node_db);
    void write();
    bool is_stopped(const Node_database &node_db) const;
    const Mailbox &get_memory() const;
    const Mailbox &get_output() const;
  public:
    struct Uninitialized_memory {
    };
    struct Uninitialized_output_buffer {
    };
  };

  typedef unsigned long Slot_id;

  class Mailbox {
    static bool has_ete_delay_constraint;
    static long long slot_count;
    static std::unordered_map<Node_id, Slot_id> node_to_slot_id;
    static std::unordered_map<Slot_id, Node_id> slot_to_node_id;
    struct Slot {
      bool is_readable;
      Nonnegative_rational earliest_sensing_time;
      Nonnegative_rational latest_sensing_time;
      bool is_done;
      std::vector<Nonnegative_rational> sensing_times_coming_from_all_possible_paths;

      Slot();
      Slot(const Nonnegative_rational &sensing_time, const bool &is_done);
      Slot(const Slot &slot);
      Slot &operator =(const Slot &slot);
    };
    std::vector<Slot> slots;
    Node &owner;
  public:
    static void set_has_ete_delay_constraint() noexcept;
    static long long get_slot_count() noexcept;
    Mailbox(Node &owner);
    Mailbox(const Mailbox &mailbox);
    Mailbox &operator =(const Mailbox &mailbox) noexcept;
    void create_event(const Node &node);
    void merge_events(const Mailbox &mailbox);
    bool is_stopped(const Node_database &node_db) const noexcept;
    std::unique_ptr<bool> get_is_done(const Node_id &source_node_id) const;
    std::unique_ptr<Nonnegative_rational> get_earliest_sensing_time(const Node_id &source_node_id) const;
    std::unique_ptr<Nonnegative_rational> get_latest_sensing_time(const Node_id &source_node_id) const;
    std::unique_ptr<decltype(Slot::sensing_times_coming_from_all_possible_paths)>
    get_sensing_times_coming_from_all_possible_paths(const Node_id &source_node_id) const;
  };

}
namespace {

  Node_name make_node_name(const std::string &name) {
    return std::stoi(name);
  }

}
namespace {

  bool Node::is_simulation_stopped = false;
  std::unordered_set<Node_id> Node::source_nodes;
  std::unordered_set<Node_id> Node::sink_nodes;
  Nonnegative_rational Node::min_period(0);

  Node::Node(const Node_id &id, const Node_name &name, const Nonnegative_rational &period) noexcept :
    id(id), name(name), period(period), release_time(0),
    is_producer(false), is_consumer(false), is_isolated(true), is_source(false), is_sink(false), topological_order(0) {
  }

  Node_id Node::get_id() const noexcept {
    return id;
  }

  Node_name Node::get_name() const noexcept {
    return name;
  }

  Nonnegative_rational Node::get_period() const noexcept {
    return period;
  }

  Nonnegative_rational Node::get_release_time() const noexcept {
    return release_time;
  }

  void Node::schedule() {
    release_time += period;
  }

  void Node::mark_producer() noexcept {
    if (is_isolated) {
      is_isolated = false;
      is_producer = true;
      is_source = true;
      source_nodes.insert(get_id());
      reachable_source_nodes.insert(get_id());
    } else if (is_consumer && is_sink) {
      is_sink = false;
      sink_nodes.erase(get_id());
      reachable_sink_nodes.erase(get_id());
    }
  }

  void Node::mark_consumer() noexcept {
    if (is_isolated) {
      is_isolated = false;
      is_consumer = true;
      is_sink = true;
      sink_nodes.insert(get_id());
      reachable_sink_nodes.insert(get_id());
    } else if (is_producer && is_source) {
      is_source = false;
      source_nodes.erase(get_id());
      reachable_source_nodes.erase(get_id());
    }
  }

  bool Node::is_sensor_node() const noexcept {
    return is_isolated || is_source;
  }

  bool Node::is_actuator_node() const noexcept {
    return is_sink;
  }

  void Node::initialize_memory_and_output_buffer() {
    memory = std::make_unique<Mailbox>(*this);
    if (is_sensor_node()) {
      if (is_source) {
        memory->create_event(*this);
      }
    }
    output = std::make_unique<Mailbox>(*this);
  }

  void Node::read(Node_database &node_db) {
    if (!memory) {
      throw Uninitialized_memory();
    }
    if (is_sensor_node()) {
      if (is_source) {
        memory->create_event(*this);
      }
    } else {
      Mailbox update(*this);
      for (const auto &parent_node_id : parents) {
        update.merge_events(node_db[parent_node_id].get_output());
      }
      *memory = update;
    }
  }

  void Node::write() {
    if (!memory) {
      throw Uninitialized_memory();
    }
    if (!output) {
      throw Uninitialized_output_buffer();
    }
    *output = *memory;
  }

  bool Node::is_stopped(const Node_database &node_db) const {
    if (!output) {
      throw Uninitialized_output_buffer();
    }
    return output->is_stopped(node_db);
  }

  const Mailbox &Node::get_memory() const {
    if (!memory) {
      throw Uninitialized_memory();
    }
    return *memory;
  }

  const Mailbox &Node::get_output() const {
    if (!output) {
      throw Uninitialized_output_buffer();
    }
    return *output;
  }

  bool operator ==(const Node &lhs, const Node &rhs) {
    return (lhs.get_release_time() == rhs.get_release_time()
            && lhs.topological_order == rhs.topological_order);
  }

  bool operator <(const Node &lhs, const Node &rhs) {
    return (lhs.get_release_time() < rhs.get_release_time()
            || (lhs.get_release_time() == rhs.get_release_time()
                && lhs.topological_order < rhs.topological_order));
  }

  bool operator <=(const Node &lhs, const Node &rhs) {
    return (lhs < rhs || lhs == rhs);
  }

  bool operator >(const Node &lhs, const Node &rhs) {
    return !(lhs <= rhs);
  }

  bool operator >=(const Node &lhs, const Node &rhs) {
    return !(lhs < rhs);
  }

  bool operator !=(const Node &lhs, const Node &rhs) {
    return !(lhs == rhs);
  }

  std::ostream &operator <<(std::ostream &out, const Node &o) {
    out << 'v' << o.get_name() << '[' << o.get_id() << "](" << o.get_period() << ")@" << static_cast<double>(o.get_release_time())
        << '=' << o.get_release_time()
        << '[';
    for (const auto &node_id : Node::source_nodes) {
      out << '(';
      if (o.get_memory().get_earliest_sensing_time(node_id)) {
        out << *o.get_memory().get_earliest_sensing_time(node_id);
      } else {
        out << ' ';
      }
      out << ", ";
      if (o.get_memory().get_latest_sensing_time(node_id)) {
        out << *o.get_memory().get_latest_sensing_time(node_id);
      } else {
        out << ' ';
      }
      out << ", ";
      if (o.get_memory().get_is_done(node_id)) {
        out << *o.get_memory().get_is_done(node_id);
      } else {
        out << ' ';
      }
      out << ')';
    }
    out << "][";
    for (const auto &node_id : Node::source_nodes) {
      out << '(';
      if (o.get_output().get_earliest_sensing_time(node_id)) {
        out << *o.get_output().get_earliest_sensing_time(node_id);
      } else {
        out << ' ';
      }
      out << ", ";
      if (o.get_output().get_latest_sensing_time(node_id)) {
        out << *o.get_output().get_latest_sensing_time(node_id);
      } else {
        out << ' ';
      }
      out << ", ";
      if (o.get_output().get_is_done(node_id)) {
        out << *o.get_output().get_is_done(node_id);
      } else {
        out << ' ';
      }
      out << ')';
    }
    return out << ']';
  }

}
namespace {

  bool Mailbox::has_ete_delay_constraint = false;

  long long Mailbox::slot_count = -1;

  std::unordered_map<Node_id, Slot_id> Mailbox::node_to_slot_id;

  std::unordered_map<Slot_id, Node_id> Mailbox::slot_to_node_id;

  void Mailbox::set_has_ete_delay_constraint() noexcept {
    has_ete_delay_constraint = true;
  }

  long long Mailbox::get_slot_count() noexcept {
    return slot_count;
  }

  Mailbox::Slot::Slot() :
    is_readable(false), earliest_sensing_time(0), latest_sensing_time(0), is_done(false), sensing_times_coming_from_all_possible_paths() {
  }

  Mailbox::Slot::Slot(const Nonnegative_rational &sensing_time, const bool &is_done) :
    is_readable(true), earliest_sensing_time(sensing_time), latest_sensing_time(sensing_time), is_done(is_done),
    sensing_times_coming_from_all_possible_paths{sensing_time} {
  }

  Mailbox::Slot::Slot(const Slot &slot) :
    is_readable(slot.is_readable),
    earliest_sensing_time(slot.is_readable ? slot.earliest_sensing_time : Nonnegative_rational(0)),
    latest_sensing_time(slot.is_readable ? slot.latest_sensing_time : Nonnegative_rational(0)),
    is_done(slot.is_readable ? slot.is_done : false),
    sensing_times_coming_from_all_possible_paths(slot.is_readable
                                                 ? slot.sensing_times_coming_from_all_possible_paths
                                                 : decltype(sensing_times_coming_from_all_possible_paths)()) {
  }

  Mailbox::Slot &Mailbox::Slot::operator =(const Slot &slot) {
    is_readable = slot.is_readable;
    earliest_sensing_time = slot.is_readable ? slot.earliest_sensing_time : Nonnegative_rational(0);
    latest_sensing_time = slot.is_readable ? slot.latest_sensing_time : Nonnegative_rational(0);
    is_done = slot.is_readable ? slot.is_done : false;
    sensing_times_coming_from_all_possible_paths = (slot.is_readable
                                                    ? slot.sensing_times_coming_from_all_possible_paths
                                                    : decltype(sensing_times_coming_from_all_possible_paths)());

    return *this;
  }

  Mailbox::Mailbox(Node &owner) :
    slots(slot_count == -1 ? Node::source_nodes.size() : slot_count), owner(owner)
  {
    if (slot_count == -1) {
      Slot_id id = 0;
      for (const auto &node_id : Node::source_nodes) {
        node_to_slot_id[node_id] = id++;
        slot_to_node_id[node_to_slot_id[node_id]] = node_id;
      }
      slot_count = slots.size();
    }
  }

  Mailbox::Mailbox(const Mailbox &mailbox) :
    Mailbox(mailbox.owner)
  {
    *this = mailbox;
  }

  Mailbox &Mailbox::operator =(const Mailbox &mailbox) noexcept {
    for (unsigned i = 0; i < slot_count; ++i) {
      slots[i] = mailbox.slots[i];
    }
    return *this;
  }

  void Mailbox::create_event(const Node &node) {
    slots[node_to_slot_id[node.get_id()]] = {node.get_release_time(), Node::is_simulation_stopped};
  }

  void Mailbox::merge_events(const Mailbox &mailbox) {
    for (unsigned i = 0; i < slot_count; ++i) {
      if (mailbox.slots[i].is_readable) {
        if (slots[i].is_readable) {
          slots[i].earliest_sensing_time = std::min(slots[i].earliest_sensing_time, mailbox.slots[i].earliest_sensing_time);
          slots[i].latest_sensing_time = std::max(slots[i].latest_sensing_time, mailbox.slots[i].latest_sensing_time);
          if (has_ete_delay_constraint) {
            slots[i].sensing_times_coming_from_all_possible_paths.insert(slots[i].sensing_times_coming_from_all_possible_paths.cend(),
                                                                         mailbox.slots[i].sensing_times_coming_from_all_possible_paths.cbegin(),
                                                                         mailbox.slots[i].sensing_times_coming_from_all_possible_paths.cend());
          }
          slots[i].is_done &= mailbox.slots[i].is_done;
        } else {
          slots[i] = mailbox.slots[i];
        }
      } else {
        if (slots[i].is_readable) {
          slots[i].is_done &= (mailbox.owner.reachable_source_nodes.find(slot_to_node_id[i]) == mailbox.owner.reachable_source_nodes.end());
        }
      }
    }
  }

  bool Mailbox::is_stopped(const Node_database &node_db) const noexcept {
    if (owner.is_sensor_node()) {
      return Node::is_simulation_stopped;
    }
    for (unsigned i = 0; i < slot_count; ++i) {
      if (slots[i].is_readable) {
        if (!slots[i].is_done) {
          return false;
        }
      } else {
         if (owner.is_actuator_node()) {
          if (node_db[slot_to_node_id[i]].reachable_sink_nodes.find(owner.get_id()) != node_db[slot_to_node_id[i]].reachable_sink_nodes.end()) {
            return false;
          }
        } else {
          if (owner.reachable_source_nodes.find(slot_to_node_id[i]) != owner.reachable_source_nodes.end()) {
            return false;
          }
        }
      }
    }
    return true;
  }

  std::unique_ptr<bool> Mailbox::get_is_done(const Node_id &source_node_id) const {
    if (slots[node_to_slot_id[source_node_id]].is_readable) {
      return std::make_unique<bool>(slots[node_to_slot_id[source_node_id]].is_done);
    }
    return {};
  }

  std::unique_ptr<Nonnegative_rational> Mailbox::get_earliest_sensing_time(const Node_id &source_node_id) const {
    if (slots[node_to_slot_id[source_node_id]].is_readable) {
      return std::make_unique<Nonnegative_rational>(slots[node_to_slot_id[source_node_id]].earliest_sensing_time);
    }
    return {};
  }

  std::unique_ptr<Nonnegative_rational> Mailbox::get_latest_sensing_time(const Node_id &source_node_id) const {
    if (slots[node_to_slot_id[source_node_id]].is_readable) {
      return std::make_unique<Nonnegative_rational>(slots[node_to_slot_id[source_node_id]].latest_sensing_time);
    }
    return {};
  }

  std::unique_ptr<decltype(Mailbox::Slot::sensing_times_coming_from_all_possible_paths)>
  Mailbox::get_sensing_times_coming_from_all_possible_paths(const Node_id &source_node_id) const {
    if (slots[node_to_slot_id[source_node_id]].is_readable) {
      return std::make_unique<decltype(Mailbox::Slot::sensing_times_coming_from_all_possible_paths)>(slots[node_to_slot_id[source_node_id]]
                                                                                                     .sensing_times_coming_from_all_possible_paths);
    }
    return {};
  }

}
namespace {
  namespace regex {

    template<bool arg__is_not_captured = false>
    const std::string node_ident_str(std::string("v(") + (arg__is_not_captured ? "?:" : "") + "[123456789][[:digit:]]*)");

    const std::regex node_ident(node_ident_str<>,
                                std::regex_constants::optimize);

    const std::regex node(node_ident_str<> + "\\(([123456789][[:digit:]]*)(?:/([123456789][[:digit:]]*))?\\)",
                          std::regex_constants::optimize);

    const std::regex feeder("E\\(((?:" + node_ident_str<true> + ", ?)+)" + node_ident_str<> + "\\)",
                            std::regex_constants::optimize);

    const std::regex ete_delay("D\\(" + node_ident_str<> + ", ?" + node_ident_str<> + "\\)",
                               std::regex_constants::optimize);

    const std::regex correlation("C\\(" + node_ident_str<> + "((?:, ?" + node_ident_str<true> + ")+)\\)",
                                 std::regex_constants::optimize);

  }
}
namespace {

  enum Error_code {
    none,

    empty_node,
    feeders_not_after_nodes,
    ete_delays_or_correlations_not_after_feeders,
    correlations_not_after_ete_delays,
    nothing_not_after_correlations,

    duplicated_node,

    unspecified_consumer_node,
    duplicated_feeder,
    unspecified_producer_node,
    duplicated_producer_node,

    graph_has_cycle,

    unspecified_ete_delay_sensor_node,
    ete_delay_invalid_sensor_node,
    unspecified_ete_delay_actuator_node,
    ete_delay_invalid_actuator_node,
    ete_delay_unconnected_nodes,

    unspecified_correlation_actuator_node,
    correlation_invalid_actuator_node,
    unspecified_correlation_sensor_node,
    duplicated_correlation_sensor_node,
    correlation_unconnected_nodes,

    invalid_option,
    cannot_open_file_for_writing,
    cannot_write_to_file,
    cannot_close_file_after_writing,
  };

}
namespace {

  struct Ete_delay_constraint {
    Node_id sensor_node_id;
    Node_id actuator_node_id;
    std::set<Nonnegative_rational> monitoring_result;

    std::vector<Nonnegative_rational> previous_sensing_times_coming_from_all_possible_paths;
    std::set<Nonnegative_rational> last_to_first_ete_delay_max;
  };

}
namespace {

  struct Correlation_constraint {
    Node_id actuator_node_id;
    std::vector<Node_id> sensor_node_ids;
    std::unordered_set<Node_id> confluent_node_ids;
    std::set<Nonnegative_rational> monitoring_result;
  };

}
namespace {

  class File_generator
  {
  public:
    class Missing_node :
      public std::exception
    {
      std::shared_ptr<std::string> name;

    public:
      Missing_node(const std::string &node_name);

    public:
      Missing_node() noexcept;
      Missing_node(const Missing_node &src) noexcept;
      Missing_node &operator =(const Missing_node &rhs) noexcept;
      ~Missing_node();
      const char *what() const noexcept override;
    };

    class Feeder_data;

    class Node_data
    {
      friend File_generator;

      Node_data(const unsigned &idx, const std::string &name, const Nonnegative_rational &period);

      unsigned idx;
      std::string name;
      Nonnegative_rational period;
      Feeder_data *feeder;
      bool is_producer;
    };

    class Feeder_data
    {
      friend File_generator;

      Feeder_data(File_generator *const &owner, Node_data *const &consumer);

      File_generator *owner;
      std::list<Node_data *> producers;
      Node_data *consumer;

    public:
      void add_producer(const Node_name &name);
    };

    class Ete_delay_constraint_data
    {
      friend File_generator;

      Ete_delay_constraint_data(Node_data *const &sensor, Node_data *const &actuator,
                                const Nonnegative_rational &lower_bound, const Nonnegative_rational &upper_bound);

      Node_data *sensor;
      Node_data *actuator;
      Nonnegative_rational lower_bound;
      Nonnegative_rational upper_bound;
    };

    class Correlation_constraint_data
    {
      friend File_generator;

      Correlation_constraint_data(File_generator *const &owner, Node_data *const &actuator, const Nonnegative_rational &threshold);

      File_generator *owner;
      std::list<Node_data *> sensors;
      Node_data *actuator;
      Nonnegative_rational threshold;

    public:
      void add_sensor(const Node_name &name);
    };

  private:
    std::string file_name_prefix;
    std::unordered_map<Node_name, Node_data *> node_dict;
    std::list<Node_data> node_list;
    std::list<Feeder_data> feeder_list;
    std::list<Ete_delay_constraint_data> ete_delay_constraint_list;
    std::list<Correlation_constraint_data> correlation_constraint_list;

    Node_data *get_node(const Node_name &name);
    void write_file(std::ostream &output_file);

  public:
    File_generator(const std::string &file_name_prefix);
    Error_code run();

  public:
    void add_node(const Node_name &name, const Nonnegative_rational &period);
    Feeder_data &add_feeder(const Node_name &consumer_name);
    void add_ete_delay_constraint(const Node_name &sensor_name, const Node_name &actuator_name,
                                  const Nonnegative_rational &lower_bound, const Nonnegative_rational &upper_bound);
    Correlation_constraint_data &add_correlation_constraint(const Node_name &actuator_name, const Nonnegative_rational &threshold);
  };

}
namespace {

  File_generator::Missing_node::Missing_node(const std::string &node_name) :
    name(std::make_shared<std::string>(node_name)) {
  }

  File_generator::Missing_node::Missing_node() noexcept :
    name() {
  }

  File_generator::Missing_node::Missing_node(const File_generator::Missing_node &src) noexcept :
    std::exception(src), name(src.name) {
  }

  File_generator::Missing_node &File_generator::Missing_node::operator =(const File_generator::Missing_node &rhs) noexcept {
    std::exception::operator =(rhs);
    name = rhs.name;
    return *this;
  }

  File_generator::Missing_node::~Missing_node() {
  }

  const char *File_generator::Missing_node::what() const noexcept {
    return name->c_str();
  }

  File_generator::Node_data::Node_data(const unsigned &idx, const std::string &name, const Nonnegative_rational &period) :
    idx(idx), name(name), period(period), feeder(nullptr), is_producer(false) {
  }

  File_generator::Feeder_data::Feeder_data(File_generator *const &owner, Node_data *const &consumer) :
    owner(owner), producers(), consumer(consumer) {
  }

  void File_generator::Feeder_data::add_producer(const Node_name &name) {
    Node_data *producer = owner->get_node(name);
    producer->is_producer = true;
    producers.push_back(producer);
  }

  File_generator::Ete_delay_constraint_data::Ete_delay_constraint_data(Node_data *const &sensor, Node_data *const &actuator,
                                                                       const Nonnegative_rational &lower_bound,
                                                                       const Nonnegative_rational &upper_bound) :
    sensor(sensor), actuator(actuator), lower_bound(lower_bound), upper_bound(upper_bound) {
  }

  File_generator::Correlation_constraint_data::Correlation_constraint_data(File_generator *const &owner, Node_data *const &actuator,
                                                                           const Nonnegative_rational &threshold) :
    owner(owner), sensors(), actuator(actuator), threshold(threshold) {
  }

  void File_generator::Correlation_constraint_data::add_sensor(const Node_name &name) {
    sensors.push_back(owner->get_node(name));
  }

  File_generator::Node_data *File_generator::get_node(const Node_name &name) {
    auto node_dict_entry = node_dict.find(name);
    if (node_dict_entry == node_dict.end()) {
      throw Missing_node(std::to_string(name));
    }
    return node_dict_entry->second;
  }

  template<typename T>
  void write_file(std::ostream &output_file, T itr, T itr_end,
                  const std::function<void(decltype(output_file),
                                           const std::remove_reference_t<decltype(*itr)> &)> &printer, const std::string &separator) {
    if (itr == itr_end) {
      return;
    }
    printer(output_file, *itr);
    while (++itr != itr_end) {
      printer(output_file << separator, *itr);
    }
  }

  template<typename T>
  void write_file(std::ostream &output_file, T itr, T itr_end,
                  const std::function<void(decltype(output_file))> &printer, const std::string &separator) {
    write_file(output_file, itr, itr_end, [printer](decltype(output_file) output_file, const std::remove_reference_t<decltype(*itr)> &) {
                                            printer(output_file);
                                          }, separator);
  }

  void File_generator::write_file(std::ostream &output_file) {
    output_file << ("/******************************************************************************\n"
                    " * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *\n"
                    " *                                            (GPG public key ID: 0x277B48A6) *\n"
                    " *                                                                            *\n"
                    " * This program is free software: you can redistribute it and/or modify       *\n"
                    " * it under the terms of the GNU General Public License as published by       *\n"
                    " * the Free Software Foundation, either version 3 of the License, or          *\n"
                    " * (at your option) any later version.                                        *\n"
                    " *                                                                            *\n"
                    " * This program is distributed in the hope that it will be useful,            *\n"
                    " * but WITHOUT ANY WARRANTY; without even the implied warranty of             *\n"
                    " * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *\n"
                    " * GNU General Public License for more details.                               *\n"
                    " *                                                                            *\n"
                    " * You should have received a copy of the GNU General Public License          *\n"
                    " * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *\n"
                    " ******************************************************************************/\n"
                    "\n"
                    "#ifndef TICE_V1_NOGEN\n"
                    "#include <iostream>\n"
                    "#include <limits>\n"
                    "#include <chrono>\n"
                    "#include <algorithm>\n"
                    "#include <string>\n"
                    "#include <thread>\n"
                    "#include <libgen.h>\n"
                    "#include <Nonnegative_rational.hpp>\n"
                    "#endif\n"
                    "\n"
                    "#include <v1.hpp>\n"
                    "\n"
                    "using namespace tice::v1;\n"
                    "\n"
                    "typedef unsigned Event;\n"
                    "typedef Chan_inlit<Event, 0> Channel;\n"
                    "\n"
                    "#ifdef TICE_V1_NOGEN\n"
                    "\n"
                    "typedef HW<Core_ids<>> hw_desc;\n"
                    "typedef Ratio<") << Node::min_period.get_num() << ", " << Node::min_period.get_den()
                << ("> wcet;\n"
                    "\n");
    for (const Node_data &node : node_list) {
      output_file << (node.is_producer ? "Event" : "void")
                  << " " << "fn_" << node.name << "(";
      if (node.feeder) {
        ::write_file(output_file, node.feeder->producers.cbegin(), node.feeder->producers.cend(),
                     [](std::ostream &output_file) {
                       output_file << "const Event &";
                     }, ", ");
      }
      output_file << ");\n";
    }
    output_file << ("\n"
                    "#else // TICE_V1_NOGEN\n"
                    "\n"
                    "#ifdef FAIR_COMPARISON_OF_FRONT_END_VS_BACK_END\n"
                    "typedef HW<Core_ids<>> hw_desc;\n"
                    "#else // FAIR_COMPARISON_OF_FRONT_END_VS_BACK_END\n"
                    "#ifndef CORE_IDS\n"
                    "#define CORE_IDS\n"
                    "#endif\n"
                    "typedef HW<Core_ids<CORE_IDS>> hw_desc;\n"
                    "static_assert(hw_desc::backend_is_enabled,"
                    " \"Macro CORE_IDS must be defined as a non-empty comma-separated list of integers\");\n"
                    "#endif // FAIR_COMPARISON_OF_FRONT_END_VS_BACK_END\n"
                    "\n"
                    "#ifdef USE_BAD_WCET\n");
    for (const Node_data &node : node_list) {
      output_file << "typedef Ratio<" << node.period.get_num() << ", " << node.period.get_den() << "> " << node.name << "_wcet;\n";
    }
    output_file << ("#else // USE_BAD_WCET\n"
                    "#ifndef WCET_IN_SECONDS\n"
                    "#error Macro WCET_IN_SECONDS must be defined as comma-separated numerator and denominator\n"
                    "#endif\n"
                    "typedef Ratio<WCET_IN_SECONDS> wcet;\n");
    output_file << ("#endif // USE_BAD_WCET\n"
                    "\n"
                    "typedef std::chrono::high_resolution_clock Clock;\n"
                    "struct Release_stats {\n"
                    "  unsigned cnt;\n"
                    "  double min;\n"
                    "  double avg;\n"
                    "  double max;\n"
                    "  unsigned data_err_cnt;\n"
                    "  unsigned release_err_cnt;\n"
                    "  const char *name;\n"
                    "  const Nonnegative_rational *period;\n"
                    "  Nonnegative_rational period_tolerance;\n"
                    "  const decltype(Clock::now()) *first_release_time;\n"
                    "  Release_stats() :\n"
                    "    cnt(0), min(std::numeric_limits<double>::max()), avg(0), max(std::numeric_limits<double>::lowest()),\n"
                    "    data_err_cnt(0), release_err_cnt(0), name(nullptr), period(nullptr), period_tolerance(0),\n"
                    "    first_release_time(nullptr) {\n"
                    "  }\n"
                    "  void add(double release_time) {\n"
                    "    ++cnt;\n"
                    "    min = std::min(min, release_time);\n"
                    "    avg = ((cnt - 1) * avg + release_time) / cnt;\n"
                    "    max = std::max(max, release_time);\n"
                    "  }\n"
                    "\n"
                    "  struct Data_err_stats {\n"
                    "    Nonnegative_rational consumer_logical_release_time;\n"
                    "    const char *producer_name;\n"
                    "    const Nonnegative_rational *producer_period;\n"
                    "    unsigned long long producer_write_count;\n"
                    "    unsigned long long expected_write_count;\n"
                    "    Data_err_stats() :\n"
                    "      consumer_logical_release_time(0), producer_name(nullptr), producer_period(nullptr),\n"
                    "      producer_write_count(0), expected_write_count(0) {\n"
                    "    }\n"
                    "  } data_err_stats;\n"
                    "  void data_err(const Nonnegative_rational &consumer_logical_release_time,\n"
                    "                const char *&producer_name, const Nonnegative_rational &producer_period,\n"
                    "                const unsigned long long &producer_write_count, const unsigned long long &expected_write_count) {\n"
                    "    if (data_err_cnt++ == 0) {\n"
                    "      data_err_stats.consumer_logical_release_time = consumer_logical_release_time;\n"
                    "      data_err_stats.producer_name = producer_name;\n"
                    "      data_err_stats.producer_period = &producer_period;\n"
                    "      data_err_stats.producer_write_count = producer_write_count;\n"
                    "      data_err_stats.expected_write_count = expected_write_count;\n"
                    "    }\n"
                    "  }\n"
                    "\n"
                    "  struct {\n"
                    "    unsigned long long logical_release_time;\n"
                    "    unsigned long long expected_release_count;\n"
                    "  } release_err_stats;\n"
                    "  void release_err(const unsigned long long &logical_release_time, const unsigned long long &expected_release_count) {\n"
                    "    if (release_err_cnt++ == 0) {\n"
                    "      release_err_stats.logical_release_time = logical_release_time;\n"
                    "      release_err_stats.expected_release_count = expected_release_count;\n"
                    "    }\n"
                    "  }\n"
                    "\n"
                    "  Release_stats &set_name(const char *name) {\n"
                    "    Release_stats::name = name;\n"
                    "    return *this;\n"
                    "  }\n"
                    "  Release_stats &set_period(const Nonnegative_rational *period) {\n"
                    "    Release_stats::period = period;\n"
                    "    period_tolerance = *period * Nonnegative_rational(1, 100);\n"
                    "    return *this;\n"
                    "  }\n"
                    "  Release_stats &set_first_release_time(const decltype(Clock::now()) *first_release_time) {\n"
                    "    Release_stats::first_release_time = first_release_time;\n"
                    "    return *this;\n"
                    "  }\n"
                    "} release_stats[") << node_list.size()
                << ("];\n"
                    "std::ostream &operator <<(std::ostream &out, const Release_stats &stats) {\n"
                    "  return out << (stats.name ? stats.name : \"\")\n"
                    "             << \": [\" << stats.min << \"---\" << stats.avg << \"---\" << stats.max << \"]#\" << stats.cnt\n"
                    "             << \" (data error #\" << stats.data_err_cnt << \", release error #\" << stats.release_err_cnt << \")\";\n"
                    "}\n"
                    "\n"
                    "void check_release_time(Release_stats &stats, const decltype(Clock::now()) &release_time,\n"
                    "                        const unsigned long long &expected_release_count) {\n"
                    "  auto logical_release_time"
                    " = std::chrono::duration_cast<std::chrono::nanoseconds>(release_time - *stats.first_release_time).count();\n"
                    "  auto expected_interval_start_time = floor(*stats.period * Nonnegative_rational(expected_release_count)"
                    " - stats.period_tolerance,\n"
                    "                                            1000000000);\n"
                    "  auto expected_interval_end_time = ceil(*stats.period * Nonnegative_rational(expected_release_count + 1)"
                    " + stats.period_tolerance,\n"
                    "                                         1000000000);\n"
                    "  if (!(expected_interval_start_time < logical_release_time && logical_release_time < expected_interval_end_time)) {\n"
                    "    stats.release_err(logical_release_time, expected_release_count);\n"
                    "  }\n"
                    "}\n"
                    "\n"
                    "void check_write_count(Release_stats &stats, const Nonnegative_rational &logical_release_time, const char *producer_name,\n"
                    "                       const Nonnegative_rational &producer_period, const unsigned long long &producer_write_count) {\n"
                    "  auto expected_write_count = floor(logical_release_time / producer_period);\n"
                    "  if (producer_write_count != expected_write_count) {\n"
                    "    stats.data_err(logical_release_time, producer_name, producer_period, producer_write_count, expected_write_count);\n"
                    "  }\n"
                    "}\n"
                    "\n");
    for (const Node_data &node : node_list) {
      output_file << (node.is_producer ? "Event" : "void")
                  << " " << "fn_" << node.name << "(";
      if (node.feeder) {
        ::write_file(output_file, node.feeder->producers.cbegin(), node.feeder->producers.cend(),
                     [](std::ostream &output_file, Node_data *const &producer) {
                       output_file << "const Event &" << producer->name;
                     }, ", ");
      }
      output_file << (") {\n"
                      "  static const Nonnegative_rational period(") << node.period.get_num() << ", " << node.period.get_den()
                  << (");\n"
                      "  static Nonnegative_rational logical_release_time(0);\n"
                      "  static unsigned long long expected_release_count(0);\n"
                      "  static decltype(Clock::now()) first_release_time;\n"
                      "  static decltype(Clock::now()) prev_release_time;\n"
                      "  static const char node_name[] = \"") << node.name
                  << ("\";\n"
                      "  static auto &release_stat = (release_stats[") << node.idx
                  << ("].set_name(node_name)\n"
                      "                               .set_period(&period).set_first_release_time(&first_release_time));\n"
                      "\n"
                      "  if (logical_release_time.is_zero()) {\n"
                      "    first_release_time = prev_release_time = Clock::now();\n"
                      "  } else {\n"
                      "    auto release_time = Clock::now();\n"
                      "\n"
                      "    release_stat.add(std::chrono::duration_cast<std::chrono::duration<double>>(release_time - prev_release_time).count());\n"
                      "    prev_release_time = release_time;\n"
                      "\n"
                      "    check_release_time(release_stat, release_time, ++expected_release_count);\n"
                      "  }\n"
                      "\n");
      if (node.feeder) {
        ::write_file(output_file, node.feeder->producers.cbegin(), node.feeder->producers.cend(),
                     [](std::ostream &output_file, Node_data *const &producer) {
                       output_file << "  static const Nonnegative_rational " << producer->name << "_period("
                                   << producer->period.get_num() << ", " << producer->period.get_den()
                                   << (");\n"
                                       "  check_write_count(release_stat, logical_release_time, \"") << producer->name << "\", "
                                   << producer->name << "_period, " << producer->name << ");\n";
                     }, "\n");
        output_file << '\n';
      }
      output_file << "  logical_release_time += period;\n"
                  << (node.is_producer
                      ? "  return expected_release_count + 1;\n"
                      : "")
                  << "}\n";
    }
    output_file << ("\n"
                    "#endif // TICE_V1_NOGEN\n"
                    "\n"
                    "#if defined(TICE_V1_NOGEN) || !defined(USE_BAD_WCET)\n");
    for (const Node_data &node : node_list) {
      output_file << "typedef wcet " << node.name << "_wcet;\n";
    }
    output_file << ("#endif\n"
                    "\n");
    for (const Node_data &node : node_list) {
      output_file << "typedef Node<Comp(&fn_" << node.name << ", " << node.name << "_wcet), Ratio<"
                  << node.period.get_num() << ", " << node.period.get_den() << ">> " << node.name << ";\n";
    }
    output_file << ("\n"
                    "tice_v1_gen(P, Program<\n"
                    "  hw_desc,\n");
    ::write_file(output_file, node_list.cbegin(), node_list.cend(), [](std::ostream &output_file, const Node_data &node) {
                                                                      output_file << "  " << node.name;
                                                                    }, ",\n");
    for (const Feeder_data &feeder : feeder_list) {
      output_file << (",\n"
                      "  Feeder<");
      ::write_file(output_file, feeder.producers.cbegin(), feeder.producers.cend(), [](std::ostream &output_file, Node_data *const &node) {
                                                                                      output_file << node->name;
                                                                                    }, ", Channel,\n         ");
      output_file << ", Channel, " << feeder.consumer->name << '>';
    }
    for (const Ete_delay_constraint_data &ete_delay_constraint : ete_delay_constraint_list) {
      output_file << (",\n"
                      "  ETE_delay<") << ete_delay_constraint.sensor->name << ", " << ete_delay_constraint.actuator->name
                  << ", Ratio<" << ete_delay_constraint.lower_bound.get_num() << ", " << ete_delay_constraint.lower_bound.get_den()
                  << ">, Ratio<" << ete_delay_constraint.upper_bound.get_num() << ", " << ete_delay_constraint.upper_bound.get_den() << ">>";
    }
    for (const Correlation_constraint_data &correlation_constraint : correlation_constraint_list) {
      output_file << (",\n"
                      "  Correlation<") << correlation_constraint.actuator->name
                  << (",\n"
                      "              Ratio<") << correlation_constraint.threshold.get_num() << ", " << correlation_constraint.threshold.get_den()
                  << (">,\n"
                      "              ");
      ::write_file(output_file, correlation_constraint.sensors.cbegin(), correlation_constraint.sensors.cend(),
                   [](std::ostream &output_file, Node_data *const &node) {
                     output_file << node->name;
                   }, ",\n              ");
      output_file << ">";
    }
    auto default_run_duration = 10 * ceil(simulation_time);
    output_file << ("\n"
                    ">);\n"
                    "\n"
                    "int main(int argc, char **argv) {\n"
                    "\n"
                    "  P p;\n"
                    "\n"
                    "#ifndef TICE_V1_NOGEN\n"
                    "  if (argc != 2) {\n"
                    "    std::cerr << \"Usage: \" << basename(argv[0]) << \" RUN_DURATION_IN_SECONDS\\n\"\n"
                    "              << (\"If RUN_DURATION_IN_SECONDS is given the special value `DEFAULT', it defaults to\\n\"\n"
                    "                  \"ten times the total duration of the simulation that generates this program,\\n\"\n"
                    "                  \"which is ") << default_run_duration
                << (" second(s)\\n\"\n"
                    "                  \"\\n\"\n"
                    "                  \"Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>\\n\"\n"
                    "                  \"                                           (GPG public key ID: 0x277B48A6)\\n\");\n"
                    "    return 1;\n"
                    "  }\n"
                    "\n"
                    "  std::chrono::seconds run_duration(") << default_run_duration
                << ("); {\n"
                    "    std::string running_time_str(argv[1]);\n"
                    "    if (running_time_str != \"DEFAULT\") {\n"
                    "      unsigned running_time_tick_count; {\n"
                    "        char *end_of_string = nullptr;\n"
                    "        running_time_tick_count = std::strtol(running_time_str.c_str(), &end_of_string, 10);\n"
                    "        if (end_of_string == running_time_str.c_str() || *end_of_string != '\\0') {\n"
                    "          std::cerr << \"Error: Failed to parse `\" << running_time_str << \"' as RUN_DURATION_IN_SECONDS\\n\";\n"
                    "          return 2;\n"
                    "        }\n"
                    "      }\n"
                    "      run_duration = std::chrono::seconds(running_time_tick_count);\n"
                    "    }\n"
                    "  }\n"
                    "\n"
                    "  p.run();\n"
                    "  if (p.get_error_code()) {\n"
                    "    std::cerr << \"Error: \" << std::strerror(p.get_error_code()) << '\\n';\n"
                    "    return 3;\n"
                    "  }\n"
                    "  std::this_thread::sleep_for(run_duration);\n"
                    "  p.stop();\n"
                    "  p.wait();\n"
                    "\n"
                    "  bool has_error = false;\n"
                    "  for (const auto &release_stat : release_stats) {\n"
                    "    if (release_stat.release_err_cnt) {\n"
                    "      has_error || (has_error = true);\n"
                    "\n"
                    "      const auto &node_name = release_stat.name;\n"
                    "      const auto &node_period = *release_stat.period;\n"
                    "      const auto &logical_release_time = release_stat.release_err_stats.logical_release_time;\n"
                    "      const auto &expected_release_count = release_stat.release_err_stats.expected_release_count;\n"
                    "\n"
                    "      auto interval_start_time = node_period * Nonnegative_rational(expected_release_count - 1);\n"
                    "      auto next_interval_start_time = node_period * Nonnegative_rational(expected_release_count);\n"
                    "      std::cerr << \"Error: \" << node_name << \" is released at \" << (logical_release_time / 1000000000.0)\n"
                    "                << \" outside [\" << static_cast<double>(interval_start_time) << '=' << interval_start_time\n"
                    "                << \", \" << static_cast<double>(next_interval_start_time) << '=' << next_interval_start_time << \")\\n\";\n"
                    "    }\n"
                    "    if (release_stat.data_err_cnt) {\n"
                    "      has_error || (has_error = true);\n"
                    "\n"
                    "      const auto &consumer_name = release_stat.name;\n"
                    "      const auto &consumer_logical_release_time = release_stat.data_err_stats.consumer_logical_release_time;\n"
                    "      const auto &producer_name = release_stat.data_err_stats.producer_name;\n"
                    "      const auto &producer_period = *release_stat.data_err_stats.producer_period;\n"
                    "      const auto &producer_write_count = release_stat.data_err_stats.producer_write_count;\n"
                    "      const auto &expected_write_count = release_stat.data_err_stats.expected_write_count;\n"
                    "\n"
                    "      std::cerr << \"Error: \" << consumer_name << \" at \" << consumer_logical_release_time\n"
                    "                << \" reads from \" << producer_name\n"
                    "                << \" data written at \" << (producer_period * Nonnegative_rational(producer_write_count))\n"
                    "                << \" not at \" << (producer_period * Nonnegative_rational(expected_write_count)) << '\\n';\n"
                    "    }\n"
                    "  }\n"
                    "  for (const auto &release_stat : release_stats) {\n"
                    "    std::cerr << release_stat << '\\n';\n"
                    "  }\n"
                    "  return has_error ? 4 : 0;\n"
                    "#endif // TICE_V1_NOGEN\n"
                    "}\n");
  }

  File_generator::File_generator(const std::string &file_name_prefix) :
    file_name_prefix(file_name_prefix) {
  }

  Error_code File_generator::run() {
    auto write = [this](const std::string &file_path) {
                   std::ofstream output_file(file_path);
                   if (!output_file) {
                     std::cerr << "Error: Cannot open file `" << file_path << "' for writing\n";
                     return Error_code::cannot_open_file_for_writing;
                   }
                   write_file(output_file);
                   if (!output_file) {
                     std::cerr << "Error: Failure when writing file `" << file_path << "'\n";
                     return Error_code::cannot_write_to_file;
                   }
                   output_file.close();
                   if (!output_file) {
                     std::cerr << "Error: Cannot close file `" << file_path << "' after writing\n";
                     return Error_code::cannot_close_file_after_writing;
                   }
                   return Error_code::none;
                 };

    if (node_dict.empty()) {
      return Error_code::none;
    }

    Error_code rc = write(file_name_prefix + "test.cpp");
    if (rc != Error_code::none) {
      return rc;
    }

    unsigned i = 0;
    for (Ete_delay_constraint_data &ete_delay_constraint : ete_delay_constraint_list) {
      const std::string file_name_prefix(this->file_name_prefix + "test-" + std::to_string(++i));

      const Nonnegative_rational tightest_lower_bound(ete_delay_constraint.lower_bound);
      ete_delay_constraint.lower_bound *= Nonnegative_rational(1001, 1000);
      rc = write(file_name_prefix + "a.cpp");
      ete_delay_constraint.lower_bound = tightest_lower_bound;
      if (rc != Error_code::none) {
        return rc;
      }

      const Nonnegative_rational tightest_upper_bound(ete_delay_constraint.upper_bound);
      ete_delay_constraint.upper_bound *= Nonnegative_rational(999, 1000);
      rc = write(file_name_prefix + "b.cpp");
      ete_delay_constraint.upper_bound = tightest_upper_bound;
      if (rc != Error_code::none) {
        return rc;
      }
    }

    i = 0;
    for (Correlation_constraint_data &correlation_constraint : correlation_constraint_list) {
      if (correlation_constraint.threshold.is_zero()) {
        ++i;
        continue;
      }
      const Nonnegative_rational tightest_threshold(correlation_constraint.threshold);
      correlation_constraint.threshold *= Nonnegative_rational(999, 1000);
      rc = write(file_name_prefix + "test-" + std::to_string(++i) + ".cpp");
      correlation_constraint.threshold = tightest_threshold;
      if (rc != Error_code::none) {
        return rc;
      }
    }

    return Error_code::none;
  }

  void File_generator::add_node(const Node_name &name, const Nonnegative_rational &period) {
    static unsigned node_idx = 0;
    node_list.push_back({node_idx++, to_string(name), period});
    node_dict[name] = &node_list.back();
  }

  File_generator::Feeder_data &File_generator::add_feeder(const Node_name &consumer_name) {
    Node_data *consumer = get_node(consumer_name);
    feeder_list.push_back({this, consumer});
    return *(consumer->feeder = &feeder_list.back());
  }

  void File_generator::add_ete_delay_constraint(const Node_name &sensor_name, const Node_name &actuator_name,
                                                const Nonnegative_rational &lower_bound, const Nonnegative_rational &upper_bound) {
    ete_delay_constraint_list.push_back({get_node(sensor_name), get_node(actuator_name), lower_bound, upper_bound});
  }

  File_generator::Correlation_constraint_data &File_generator::add_correlation_constraint(const Node_name &actuator_name,
                                                                                          const Nonnegative_rational &threshold) {
    correlation_constraint_list.push_back({this, get_node(actuator_name), threshold});
    return correlation_constraint_list.back();
  }
}

Error_code run(File_generator *generator = nullptr) {

#define access(pointer_to_object, ...) if (pointer_to_object) pointer_to_object->__VA_ARGS__
#define access_expr(pointer_to_object, ...) pointer_to_object ? &pointer_to_object->__VA_ARGS__ : nullptr

  Node_database node_db;
  Node_name_index node_name_idx;
  std::vector<Ete_delay_constraint> ete_delays_to_monitor;
  std::vector<Correlation_constraint> correlations_to_monitor;
  std::vector<Node *> simulation_scheduler;
  Nonnegative_rational simulation_delta(0);
  bool hyperperiod_has_been_reached = false;
  unsigned long long step_count_to_complete_simulation = 0;
  unsigned long long step_count_to_reach_hyperperiod = 0;
  Nonnegative_rational hyperperiod_time(0);
  {//\begin{parsing}
    std::string input_line;
    std::smatch m;

    {//\begin{parse nodes}
      while (std::getline(std::cin, input_line)
             && std::regex_match(input_line, m, regex::node)) {

        node_db.push_back({node_db.size(), make_node_name(m[1]), {std::stoull(m[2]), m[3].matched ? std::stoull(m[3]) : 1}});
        if (node_name_idx.find(node_db.back().get_name()) != node_name_idx.end()) {
          std::cerr << "Error: Duplicated node v" << node_db.back().get_name() << '\n';
          return Error_code::duplicated_node;
        }
        Node::min_period = (Node::min_period.is_zero()
                            ? node_db.back().get_period()
                            : std::min(Node::min_period, node_db.back().get_period()));
        access(generator, add_node(node_db.back().get_name(), node_db.back().get_period()));
        node_name_idx.insert({node_db.back().get_name(), node_db.back().get_id()});
        if (simulation_delta == Nonnegative_rational(0)) {
          simulation_delta = node_db.back().get_period();
        } else {
          simulation_delta = std::min(simulation_delta, node_db.back().get_period());
        }

        input_line.clear();
      }
      if (node_name_idx.empty()) {
        std::cerr << "Error: No node specified\n";
        return Error_code::empty_node;
      }
      simulation_scheduler.reserve(node_name_idx.size());
      for (auto itr = node_db.begin(); itr != node_db.end(); ++itr) {
        simulation_scheduler.push_back(&*itr);
      }
      if (!std::cin) {
        goto end_of_parsing;
      }
    }//\end{parse nodes}

    {//\begin{parse feeders}
      std::unordered_set<Node_name> already_specified_cons;
      while (std::regex_match(input_line, m, regex::feeder)) {

        Node_name cons_name = make_node_name(m[2]);
        if (node_name_idx.find(cons_name) == node_name_idx.end()) {
          std::cerr << "Error: Consumer node v" << cons_name << " has not been specified\n";
          return Error_code::unspecified_consumer_node;
        }
        if (already_specified_cons.find(cons_name) == already_specified_cons.end()) {
          already_specified_cons.insert(cons_name);
        } else {
          std::cerr << "Error: Duplicate feeder with consumer node v" << cons_name << '\n';
          return Error_code::duplicated_feeder;
        }
        Node &cons_node = node_db[node_name_idx[cons_name]];
        File_generator::Feeder_data *feeder_data = access_expr(generator, add_feeder(cons_name));

        cons_node.mark_consumer();

        std::unordered_set<Node_name> already_specified_prod;
        for (auto itr = m[1].first, itr_end = m[1].second; std::regex_search(itr, itr_end, m, regex::node_ident); itr = m[0].second) {
          Node_name prod_name = make_node_name(m[1]);
          if (node_name_idx.find(prod_name) == node_name_idx.end()) {
            std::cerr << "Error: Producer node v" << prod_name << " has not been specified\n";
            return Error_code::unspecified_producer_node;
          }
          if (already_specified_prod.find(prod_name) == already_specified_prod.end()) {
            already_specified_prod.insert(prod_name);
          } else {
            std::cerr << "Error: Feeder has duplicated producer node v" << prod_name << '\n';
            return Error_code::duplicated_producer_node;
          }
          Node &prod_node = node_db[node_name_idx[prod_name]];
          access(feeder_data, add_producer(prod_name));

          ++cons_node.topological_order;
          prod_node.mark_producer();

          prod_node.children.insert(cons_node.get_id());
          cons_node.parents.insert(prod_node.get_id());
        }

        input_line.clear();
        std::getline(std::cin, input_line);
      }
      if (already_specified_cons.empty()) {
        if (std::cin) {
          std::cerr << "Error: A sequence of nodes must be followed by a sequence of Feeders\n";
          return Error_code::feeders_not_after_nodes;
        } else {
          goto end_of_parsing;
        }
      }
      {//\begin{check cycle and build a set of reachable sinks at every node}
        std::vector<std::pair<typename Node::children_const_itr, typename Node::children_const_itr>> stack(node_db.size() + 1);
        unsigned stack_top = 0;
        stack[stack_top] = {Node::source_nodes.cbegin(), Node::source_nodes.cend()};
        std::unordered_set<Node_id> nodes_cleared_of_cycle;
        std::unordered_set<Node_id> nodes_constituting_current_path;

        while (true) {
          typename Node::children_const_itr child_node_id = stack[stack_top].first;
          typename Node::children_const_itr child_node_id_end = stack[stack_top].second;
#if defined(VERBOSE) && VERBOSE > 2
          if (child_node_id == child_node_id_end) {
            std::cerr << "All children have been inspected\n";
          }
#endif

          while (stack_top != 0 && child_node_id == child_node_id_end) {
            --stack_top;
            typename Node::children_const_itr &prev_child_node_id = stack[stack_top].first;

            nodes_constituting_current_path.erase(*prev_child_node_id);
            nodes_cleared_of_cycle.insert(*prev_child_node_id);
            if (stack_top != 0) {
              node_db[*stack[stack_top - 1].first].reachable_sink_nodes.insert(node_db[*prev_child_node_id].reachable_sink_nodes.cbegin(),
                                                                               node_db[*prev_child_node_id].reachable_sink_nodes.cend());
            }
#if defined(VERBOSE) && VERBOSE > 2
            std::cerr << "Removing v" << node_db[*prev_child_node_id].get_name() << " with reachable sink nodes:";
            for (auto e : node_db[*prev_child_node_id].reachable_sink_nodes) {
              std::cerr << " v" << node_db[e].get_name();
            }
            std::cerr << '\n';
#endif

            child_node_id = ++prev_child_node_id;
            child_node_id_end = stack[stack_top].second;
          }
          if (stack_top == 0 && child_node_id == child_node_id_end) {
            break; // no cycle
          }

          do {
#if defined(VERBOSE) && VERBOSE > 2
            std::cerr << "Inspecting v" << node_db[*child_node_id].get_name();
#endif
            if (nodes_constituting_current_path.find(*child_node_id) != nodes_constituting_current_path.end()) {
#if defined(VERBOSE) && VERBOSE > 2
              std::cerr << '\n';
#endif
              std::cerr << "Error: Graph has a cycle (v" << node_db[*stack[0].first].get_name();
              for (unsigned i = 1; i <= stack_top; ++i) {
                std::cerr << ", v" << node_db[*stack[i].first].get_name();
              }
              std::cerr << ")\n";
              return Error_code::graph_has_cycle;
            }
            if (nodes_cleared_of_cycle.find(*child_node_id) == nodes_cleared_of_cycle.end()) {
#if defined(VERBOSE) && VERBOSE > 2
              std::cerr << "\nVisiting v" << node_db[*child_node_id].get_name() << '\n';
#endif
              break;
            } else {
#if defined(VERBOSE) && VERBOSE > 2
              std::cerr << ": node has been cleared of cycle\n";
#endif
              if (stack_top != 0) {
                node_db[*stack[stack_top - 1].first].reachable_sink_nodes.insert(node_db[*stack[stack_top].first].reachable_sink_nodes.cbegin(),
                                                                                 node_db[*stack[stack_top].first].reachable_sink_nodes.cend());
              }
              child_node_id = ++stack[stack_top].first;
            }
          } while (child_node_id != child_node_id_end);
          if (child_node_id == child_node_id_end) {
            continue;
          }

          nodes_constituting_current_path.insert(*child_node_id);
          stack[++stack_top] = {node_db[*child_node_id].children.cbegin(), node_db[*child_node_id].children.cend()};
        }
      }//\end{check cycle and build a set of reachable sinks at every node}
      {//\begin{build a set of reachable sources at every node}
#if defined(VERBOSE) && VERBOSE > 2
        std::cerr << "Building reachable sources at every node:\n";
#endif
        std::vector<std::pair<typename Node::parents_const_itr, typename Node::parents_const_itr>> stack(node_db.size() + 1);
        unsigned stack_top = 0;
        stack[stack_top] = {Node::sink_nodes.cbegin(), Node::sink_nodes.cend()};
        std::unordered_set<Node_id> nodes_whose_reversed_paths_have_been_traversed;

        while (true) {
          typename Node::parents_const_itr parent_node_id = stack[stack_top].first;
          typename Node::parents_const_itr parent_node_id_end = stack[stack_top].second;
#if defined(VERBOSE) && VERBOSE > 2
          if (parent_node_id == parent_node_id_end) {
            std::cerr << "\tAll parents have been inspected\n";
          }
#endif

          while (stack_top != 0 && parent_node_id == parent_node_id_end) {
            --stack_top;
            typename Node::parents_const_itr &prev_parent_node_id = stack[stack_top].first;

            nodes_whose_reversed_paths_have_been_traversed.insert(*prev_parent_node_id);
            if (stack_top != 0) {
              node_db[*stack[stack_top - 1].first].reachable_source_nodes.insert(node_db[*prev_parent_node_id].reachable_source_nodes.cbegin(),
                                                                                 node_db[*prev_parent_node_id].reachable_source_nodes.cend());
            }
#if defined(VERBOSE) && VERBOSE > 2
            std::cerr << "\tRemoving v" << node_db[*prev_parent_node_id].get_name() << " with reachable source nodes:";
            for (auto e : node_db[*prev_parent_node_id].reachable_source_nodes) {
              std::cerr << " v" << node_db[e].get_name();
            }
            std::cerr << '\n';
#endif

            parent_node_id = ++prev_parent_node_id;
            parent_node_id_end = stack[stack_top].second;
          }
          if (stack_top == 0 && parent_node_id == parent_node_id_end) {
            break; // all reversed arcs have been visited
          }

          do {
#if defined(VERBOSE) && VERBOSE > 2
            std::cerr << "\tInspecting v" << node_db[*parent_node_id].get_name();
#endif
            if (nodes_whose_reversed_paths_have_been_traversed.find(*parent_node_id) == nodes_whose_reversed_paths_have_been_traversed.end()) {
#if defined(VERBOSE) && VERBOSE > 2
              std::cerr << "\n\tVisiting v" << node_db[*parent_node_id].get_name() << '\n';
#endif
              break;
            } else {
#if defined(VERBOSE) && VERBOSE > 2
              std::cerr << ": all reversed paths starting from the node have been traversed\n";
#endif
              if (stack_top != 0) {
                node_db[*stack[stack_top - 1].first].reachable_source_nodes.insert(node_db[*stack[stack_top].first].reachable_source_nodes.cbegin(),
                                                                                   node_db[*stack[stack_top].first].reachable_source_nodes.cend());
              }
              parent_node_id = ++stack[stack_top].first;
            }
          } while (parent_node_id != parent_node_id_end);
          if (parent_node_id == parent_node_id_end) {
            continue;
          }

          stack[++stack_top] = {node_db[*parent_node_id].parents.cbegin(), node_db[*parent_node_id].parents.cend()};
        }
      }//\end{build a set of reachable sources at every node}
      {//\begin{topological sort}
        unsigned topological_idx = 0;
        std::vector<Node_id> source_nodes(Node::source_nodes.cbegin(), Node::source_nodes.cend());
        while (!source_nodes.empty()) {
          Node &node = node_db[source_nodes.back()];
          source_nodes.pop_back();

          node.topological_order = ++topological_idx;
          for (auto &child_node_id : node.children) {
            if (--node_db[child_node_id].topological_order == 0) {
              source_nodes.push_back(child_node_id);
            }
          }
        }
      }//\end{topological sort}
    }//\end{parse feeders}

    {//\begin{parse end-to-end delay constraints}
      while (std::regex_match(input_line, m, regex::ete_delay)) {
        Node_name sensor_name = make_node_name(m[1]);
        if (node_name_idx.find(sensor_name) == node_name_idx.end()) {
          std::cerr << "Error: Node v" << sensor_name << " has not been specified\n";
          return Error_code::unspecified_ete_delay_sensor_node;
        }
        const Node &sensor_node = node_db[node_name_idx[sensor_name]];
        if (!sensor_node.is_sensor_node()) {
          std::cerr << "Error: Node v" << sensor_name << " is not a sensor node\n";
          return Error_code::ete_delay_invalid_sensor_node;
        }

        Node_name actuator_name = make_node_name(m[2]);
        if (node_name_idx.find(actuator_name) == node_name_idx.end()) {
          std::cerr << "Error: Node v" << actuator_name << " has not been specified\n";
          return Error_code::unspecified_ete_delay_actuator_node;
        }
        const Node &actuator_node = node_db[node_name_idx[actuator_name]];
        if (!actuator_node.is_actuator_node()) {
          std::cerr << "Error: Node v" << actuator_name << " is not an actuator node\n";
          return Error_code::ete_delay_invalid_actuator_node;
        }

        if (sensor_node.reachable_sink_nodes.find(actuator_node.get_id()) == sensor_node.reachable_sink_nodes.end()) {
          std::cerr << "Error: Sensor node v" << sensor_node.get_name()
                    << " and actuator node v" << actuator_node.get_name() << " are not connected\n";
          return Error_code::ete_delay_unconnected_nodes;
        }

        ete_delays_to_monitor.push_back({sensor_node.get_id(), actuator_node.get_id(), {}, {}, {}});

        input_line.clear();
        std::getline(std::cin, input_line);
      }
      if (!ete_delays_to_monitor.empty()) {
        Mailbox::set_has_ete_delay_constraint();
      }
    }//\end{parse end-to-end delay constraints}

    {//\begin{parse correlation constraints}
      while (std::regex_match(input_line, m, regex::correlation)) {
        Node_name actuator_name = make_node_name(m[1]);
        if (node_name_idx.find(actuator_name) == node_name_idx.end()) {
          std::cerr << "Error: Node v" << actuator_name << " has not been specified\n";
          return Error_code::unspecified_correlation_actuator_node;
        }
        const Node &actuator_node = node_db[node_name_idx[actuator_name]];
        if (!actuator_node.is_actuator_node()) {
          std::cerr << "Error: Node v" << actuator_name << " is not an actuator node\n";
          return Error_code::correlation_invalid_actuator_node;
        }

        correlations_to_monitor.push_back({actuator_node.get_id(), {}, {}, {0}});

        auto &sensor_nodes = correlations_to_monitor.back().sensor_node_ids;
        std::unordered_set<Node_name> already_specified_sensor_node;
        for (auto itr = m[2].first, itr_end = m[2].second; std::regex_search(itr, itr_end, m, regex::node_ident); itr = m[0].second) {
          Node_name sensor_name = make_node_name(m[1]);
          if (node_name_idx.find(sensor_name) == node_name_idx.end()) {
            std::cerr << "Error: Sensor node v" << sensor_name << " has not been specified\n";
            return Error_code::unspecified_correlation_sensor_node;
          }
          if (already_specified_sensor_node.find(sensor_name) == already_specified_sensor_node.end()) {
            already_specified_sensor_node.insert(sensor_name);
          } else {
            std::cerr << "Error: Correlation constraint has duplicated sensor node v" << sensor_name << '\n';
            return Error_code::duplicated_correlation_sensor_node;
          }
          Node &sensor_node = node_db[node_name_idx[sensor_name]];

          if (sensor_node.reachable_sink_nodes.find(actuator_node.get_id()) == sensor_node.reachable_sink_nodes.end()) {
            std::cerr << "Error: Sensor node v" << sensor_node.get_name()
                      << " and actuator node v" << actuator_node.get_name() << " are not connected\n";
            return Error_code::correlation_unconnected_nodes;
          }

          sensor_nodes.push_back(sensor_node.get_id());
        }

        {//\begin{build confluent nodeset}
#if defined(VERBOSE) && VERBOSE > 2
          std::cerr << "Building confluent nodeset:\n";
#endif
          auto &confluent_node_ids = correlations_to_monitor.back().confluent_node_ids;

          std::vector<std::pair<typename Node::children_const_itr, typename Node::children_const_itr>> stack(node_db.size() + 1);
          unsigned stack_top = 0;
          std::unordered_set<Node_id> source_nodes(sensor_nodes.cbegin(), sensor_nodes.cend());
          stack[stack_top] = {source_nodes.cbegin(), source_nodes.cend()};
          std::unordered_set<Node_id> already_visited_nodes;

          while (true) {
            typename Node::children_const_itr child_node_id = stack[stack_top].first;
            typename Node::children_const_itr child_node_id_end = stack[stack_top].second;
#if defined(VERBOSE) && VERBOSE > 2
            if (child_node_id == child_node_id_end) {
              std::cerr << "\tAll children have been inspected\n";
            }
#endif

            while (stack_top != 0 && child_node_id == child_node_id_end) {
              --stack_top;
              typename Node::children_const_itr &prev_child_node_id = stack[stack_top].first;

              already_visited_nodes.insert(*prev_child_node_id);
#if defined(VERBOSE) && VERBOSE > 2
              std::cerr << "\tMark v" << node_db[*prev_child_node_id].get_name() << " as a confluent-node candidate";
#endif

              child_node_id = ++prev_child_node_id;
              child_node_id_end = stack[stack_top].second;

#if defined(VERBOSE) && VERBOSE > 2
              if (stack_top == 0) {
                std::cerr << '\n';
              } else {
                std::cerr << ", going back to v" << node_db[*stack[stack_top - 1].first].get_name() << '\n';
              }
#endif
            }
            if (stack_top == 0 && child_node_id == child_node_id_end) {
              break; // build completes
            }

#if defined(VERBOSE) && VERBOSE > 2
            std::cerr << "\tInspecting v" << node_db[*child_node_id].get_name();
#endif
            bool restart;
            do {
              restart = false;

              while (child_node_id != child_node_id_end
                     && (node_db[*child_node_id].reachable_sink_nodes.find(actuator_node.get_id())
                         == node_db[*child_node_id].reachable_sink_nodes.end())) {
#if defined(VERBOSE) && VERBOSE > 2
                std::cerr << ": does not reach the actuator node\n";
#endif
                child_node_id = ++stack[stack_top].first;
#if defined(VERBOSE) && VERBOSE > 2
                if (child_node_id != child_node_id_end) {
                  std::cerr << "\tInspecting v" << node_db[*child_node_id].get_name();
                }
#endif
              }

              while (child_node_id != child_node_id_end
                     && already_visited_nodes.find(*child_node_id) != already_visited_nodes.end()) {
#if defined(VERBOSE) && VERBOSE > 2
                std::cerr << ": a confluent node\n";
#endif
                confluent_node_ids.insert(*child_node_id);
                child_node_id = ++stack[stack_top].first;
                restart = true;

#if defined(VERBOSE) && VERBOSE > 2
                if (child_node_id != child_node_id_end) {
                  std::cerr << "\tInspecting v" << node_db[*child_node_id].get_name();
                }
#endif
              }
            } while (restart);
            if (child_node_id == child_node_id_end) {
              continue;
            }

#if defined(VERBOSE) && VERBOSE > 2
              std::cerr << ": visit it\n";
#endif
            stack[++stack_top] = {node_db[*child_node_id].children.cbegin(), node_db[*child_node_id].children.cend()};
          }
#if defined(VERBOSE) && VERBOSE > 2
          std::cerr << '{';
          if (!confluent_node_ids.empty()) {
            auto itr = confluent_node_ids.cbegin();
            std::cerr << 'v' << node_db[*itr++].get_name();
            while (itr != confluent_node_ids.cend()) {
              std::cerr << ", v" << node_db[*itr++].get_name();
            }
          }
          std::cerr << "}\n";
#endif
        }//\end{build confluent nodeset}


        input_line.clear();
        std::getline(std::cin, input_line);
      }
    }//\end{parse correlation constraints}

    if (std::cin) {
      if (ete_delays_to_monitor.empty() && correlations_to_monitor.empty()) {
        std::cerr << "Error: A sequence of feeders must be followed by a sequence of ETE delay or a sequence of correlation constraints\n";
        return Error_code::ete_delays_or_correlations_not_after_feeders;
      } else if (correlations_to_monitor.empty()) {
        std::cerr << "Error: A sequence of ETE delay constraints must be followed by a sequence of correlation constraints\n";
        return Error_code::correlations_not_after_ete_delays;
      } else {
        std::cerr << "Error: A sequence of correlation constraints must be followed by nothing\n";
        return Error_code::nothing_not_after_correlations;
      }
    }
  }//\end{parsing}
 end_of_parsing:

  {//\begin{initialize memory and output buffers}
    for (auto &node : node_db) {
      node.initialize_memory_and_output_buffer();
#if defined(VERBOSE) && VERBOSE > 0
      std::cerr << "\n\t" << node;
#endif
      node.schedule();
    }
#if defined(VERBOSE) && VERBOSE > 0
    std::cerr << '\n';
#endif
  }//\end{initialize memory and output buffers}

  {//\begin{simulation}
    unsigned remaining_hyperperiod = 1;
    std::unordered_set<Node_id> released_node_ids;
    for (simulation_time = simulation_delta.simplify(); true; simulation_time += simulation_delta) {
      ++step_count_to_complete_simulation;
      if (!hyperperiod_has_been_reached) {
        ++step_count_to_reach_hyperperiod;
      }
      std::sort(simulation_scheduler.begin(), simulation_scheduler.end(), [](const Node *lhs, const Node *rhs) { return *lhs < *rhs; });
      bool time_is_hyperperiod = true;

#if defined(VERBOSE) && VERBOSE > 0
      static bool stop_signal_caught = false;
      if (Node::is_simulation_stopped) {
        if (stop_signal_caught) {
          std::cerr << "-------------------------------------------------------------------------------";
        } else {
          stop_signal_caught = true;
          std::cerr << "===============================================================================";
        }
      } else {
        std::cerr << "-------------------------------------------------------------------------------";
      }
#endif
      released_node_ids.clear();
      for (auto itr = simulation_scheduler.begin(); itr != simulation_scheduler.end(); ++itr) {
        Node &node = **itr;
        if (time_is_hyperperiod && node.get_release_time() != simulation_time) {
          time_is_hyperperiod = false;
        }
        if (node.get_release_time() > simulation_time) {
          break;
        }
        node.write();
        node.read(node_db);
#if defined(VERBOSE) && VERBOSE > 0
        std::cerr << "\n\t" << node;
#endif
        released_node_ids.insert(node.get_id());
      }
#if defined(VERBOSE) && VERBOSE > 0
      std::cerr << '\n';
#endif

      for (auto &constraint : ete_delays_to_monitor) {
        if (released_node_ids.find(constraint.actuator_node_id) == released_node_ids.end()) {
          continue;
        }

        const auto actuator_output = node_db[constraint.actuator_node_id].get_output();
        auto sensing_time = actuator_output.get_earliest_sensing_time(constraint.sensor_node_id);
        if (!sensing_time) {
          continue;
        }
        const auto actuator_period = node_db[constraint.actuator_node_id].get_period();
#if defined(VERBOSE) && VERBOSE > 1
        const auto sensor_name = node_db[constraint.sensor_node_id].get_name();
        const auto actuator_name = node_db[constraint.actuator_node_id].get_name();
#endif

        const auto actuator_release_time = node_db[constraint.actuator_node_id].get_release_time();
        const auto ete_delay_max = actuator_release_time - *sensing_time;
        const auto ete_delay_max_latest = ete_delay_max + actuator_period;
#if defined(VERBOSE) && VERBOSE > 1
        std::cerr << "Last-to-last ETE delay from v" << sensor_name << " to v" << actuator_name << ": ["
                  << static_cast<double>(ete_delay_max) << '=' << ete_delay_max << ", "
                  << static_cast<double>(ete_delay_max_latest) << '=' << ete_delay_max_latest << "]\n";
#endif
        constraint.monitoring_result.insert(ete_delay_max);

        sensing_time = actuator_output.get_latest_sensing_time(constraint.sensor_node_id);
        const auto ete_delay_min = actuator_release_time - *sensing_time;
        const auto ete_delay_min_earliest = ete_delay_min - actuator_period;
#if defined(VERBOSE) && VERBOSE > 1
        std::cerr << "Last-to-first ETE delay from v" << sensor_name << " to v" << actuator_name << ": ["
                  << static_cast<double>(ete_delay_min_earliest) << '=' << ete_delay_min_earliest << ", "
                  << static_cast<double>(ete_delay_min) << '=' << ete_delay_min << "]\n";
#endif
        constraint.monitoring_result.insert(ete_delay_min_earliest);

        const auto sensing_times_coming_from_all_possible_paths
          = actuator_output.get_sensing_times_coming_from_all_possible_paths(constraint.sensor_node_id);
        auto &previous_sensing_times_coming_from_all_possible_paths = constraint.previous_sensing_times_coming_from_all_possible_paths;
        Nonnegative_rational last_to_first_ete_delay_max(0);
        for (unsigned i = 0, i_end = sensing_times_coming_from_all_possible_paths->size(); i < i_end; ++i) {
          const auto &parent_sensing_time = (*sensing_times_coming_from_all_possible_paths)[i];
          if (i >= previous_sensing_times_coming_from_all_possible_paths.size()) {
            previous_sensing_times_coming_from_all_possible_paths.push_back(parent_sensing_time);
          } else if (previous_sensing_times_coming_from_all_possible_paths[i] == parent_sensing_time) {
            continue;
          } else {
            previous_sensing_times_coming_from_all_possible_paths[i] = parent_sensing_time;
          }
          last_to_first_ete_delay_max = std::max(last_to_first_ete_delay_max, actuator_release_time - parent_sensing_time);
        }
        if (!last_to_first_ete_delay_max.is_zero()) {
          const auto last_to_first_ete_delay_max_earliest = last_to_first_ete_delay_max - actuator_period;
#if defined(VERBOSE) && VERBOSE > 1
          std::cerr << "Last-to-first ETE delay from v" << sensor_name << " to v" << actuator_name << ": ["
                    << static_cast<double>(last_to_first_ete_delay_max_earliest) << '=' << last_to_first_ete_delay_max_earliest << ", "
                    << static_cast<double>(last_to_first_ete_delay_max) << '=' << last_to_first_ete_delay_max << "]\n";
#endif
          constraint.last_to_first_ete_delay_max.insert(last_to_first_ete_delay_max);
        }
      }

      for (auto &constraint : correlations_to_monitor) {
        for (const auto &confluent_node_id : constraint.confluent_node_ids) {
          const auto &memory = node_db[confluent_node_id].get_memory();
          bool sensing_time_has_been_initialized = false;
          Nonnegative_rational earliest_sensing_time(0);
          Nonnegative_rational latest_sensing_time(0);

#if defined(VERBOSE) && VERBOSE > 1
          std::cerr << "Correlation at confluent node v" << node_db[confluent_node_id].get_name() << " considering sensor nodes ";
          bool first_time = true;
#endif
          for (const auto &sensor_node_id : constraint.sensor_node_ids) {
#if defined(VERBOSE) && VERBOSE > 1
            if (first_time) {
              first_time = false;
            } else {
              std::cerr << ", ";
            }
            std::cerr << 'v' << node_db[sensor_node_id].get_name();
#endif
            if (memory.get_earliest_sensing_time(sensor_node_id)) {
              if (sensing_time_has_been_initialized) {
                earliest_sensing_time = std::min(earliest_sensing_time, *memory.get_earliest_sensing_time(sensor_node_id));
                latest_sensing_time = std::max(latest_sensing_time, *memory.get_latest_sensing_time(sensor_node_id));
              } else {
                sensing_time_has_been_initialized = true;
                earliest_sensing_time = *memory.get_earliest_sensing_time(sensor_node_id);
                latest_sensing_time = *memory.get_latest_sensing_time(sensor_node_id);
              }
            }
          }
          auto correlation = latest_sensing_time - earliest_sensing_time;
#if defined(VERBOSE) && VERBOSE > 1
          std::cerr << ": " << static_cast<double>(correlation) << '=' << correlation << '\n';
#endif
          constraint.monitoring_result.insert(correlation);
        }
      }

      for (auto &released_node_id : released_node_ids) {
        node_db[released_node_id].schedule();
      }

      if (time_is_hyperperiod && remaining_hyperperiod != 0) {
        hyperperiod_has_been_reached = true;
        hyperperiod_time = simulation_time;
        --remaining_hyperperiod;
      }
      if (remaining_hyperperiod == 0 && !Node::is_simulation_stopped) {
        Node::is_simulation_stopped = true;
      }
      if (Node::is_simulation_stopped) {
        bool all_actuators_have_received_the_message = true;
        for (const auto &actuator_node : Node::sink_nodes) {
          if (!node_db[actuator_node].is_stopped(node_db)) {
            all_actuators_have_received_the_message = false;
            break;
          }
        }
        if (all_actuators_have_received_the_message) {
          break;
        }
      }
    }
  }//\end{simulation}

  {//\begin{reporting}
    std::cout << "S = " << step_count_to_complete_simulation << '@' << static_cast<double>(simulation_time)
              << '=' << simulation_time << '\n'
              << "H = " << step_count_to_reach_hyperperiod << '@' << static_cast<double>(hyperperiod_time)
              << '=' << hyperperiod_time << '\n';
    for (auto &constraint : ete_delays_to_monitor) {
      const auto last_to_first_ete_delay_min = *constraint.monitoring_result.cbegin();
      const auto last_to_first_ete_delay_max = *constraint.last_to_first_ete_delay_max.crbegin();
      const Node_name &sensor_name = node_db[constraint.sensor_node_id].get_name();
      const Node_name &actuator_name = node_db[constraint.actuator_node_id].get_name();
      std::cout << "D(v" << sensor_name
                << ", v" << actuator_name
                << ") = [" << last_to_first_ete_delay_min << " (" << static_cast<double>(last_to_first_ete_delay_min) << ')'
                << ", " << last_to_first_ete_delay_max << " (" << static_cast<double>(last_to_first_ete_delay_max) << ")]\n";
#if defined(VERBOSE) && VERBOSE > 1
      const auto last_to_last_ete_delay_max = *constraint.monitoring_result.crbegin();
      const auto last_to_last_ete_delay_max_latest = last_to_last_ete_delay_max + node_db[constraint.actuator_node_id].get_period();
      std::cerr << "L(v" << sensor_name
                << ", v" << actuator_name
                << ") = [" << last_to_last_ete_delay_max << " (" << static_cast<double>(last_to_last_ete_delay_max) << ')'
                << ", " << last_to_last_ete_delay_max_latest << " (" << static_cast<double>(last_to_last_ete_delay_max_latest) << ")]\n";
#endif
      access(generator, add_ete_delay_constraint(sensor_name, actuator_name, last_to_first_ete_delay_min, last_to_first_ete_delay_max));
    }
    for (auto &constraint : correlations_to_monitor) {
      const auto max = *constraint.monitoring_result.crbegin();
      const Node_name &actuator_name = node_db[constraint.actuator_node_id].get_name();
      std::cout << "C(v" << actuator_name;
      File_generator::Correlation_constraint_data *correlation_constraint_data = access_expr(generator,
                                                                                             add_correlation_constraint(actuator_name, max));
      for (const auto &sensor_node_id : constraint.sensor_node_ids) {
        const Node_name &sensor_name = node_db[sensor_node_id].get_name();
        std::cout << ", v" << sensor_name;
        access(correlation_constraint_data, add_sensor(sensor_name));
      }
      std::cout << ") = " << max << " (" << static_cast<double>(max) << ")\n";
    }
  }//\end{reporting}

  return Error_code::none;

#undef access
#undef access_expr
}

int main(int argc, char **argv) {
  bool generated_file_is_requested = false;
  std::string generated_file_prefix;
  if (argc > 1) {
    std::string generation_option(argv[1]);
    if (generation_option.find("-g") == 0) {
      generated_file_is_requested = true;
      if (argc == 3) {
        generated_file_prefix = argv[2];
      } else if (generation_option.size() > 2) {
        generated_file_prefix = generation_option.substr(2);
      }
    }
    if (!generated_file_is_requested) {
      std::cerr << ("Express a Tice graph to be simulated in stdin using the following grammar:\n"
                    "\n"
                    "v<POSITIVE INTEGER>(<POSITIVE INTEGER>[/<POSITIVE INTEGER>])\n"
                    "[...]\n"
                    "[E(v<POSITIVE_INTEGER>[,[ ]v<POSITIVE_INTEGER>],[ ]v<POSITIVE_INTEGER>)\n"
                    " [...]\n"
                    " [D(v<POSITIVE_INTEGER>,[ ]v<POSITIVE_INTEGER>)\n"
                    "  [...]]\n"
                    " [C(v<POSITIVE_INTEGER>,[ ]v<POSITIVE_INTEGER>[,[ ]v<POSITIVE_INTEGER>])\n"
                    "  [...]]]\n"
                    "\n"
                    "For example, a Tice graph with two sensor, one intermediary, and one actuator\n"
                    "nodes whose periods are 1/3, 1/25, 25/10, and 1, respectively, and whose\n"
                    "simulation is to report the minimum and maximum last-to-first end-to-end delays\n"
                    "from the first sensor node and the maximum correlation of the data from both\n"
                    "sensor nodes with respect to the sole actuator node is expressed in stdin as\n"
                    "follows:\n"
                    "v1(1/3)\n"
                    "v2(1/25)\n"
                    "v3(25/10)\n"
                    "v4(1)\n"
                    "E(v1, v2, v3)\n"
                    "E(v3, v4)\n"
                    "D(v1, v4)\n"
                    "C(v4, v1, v2)\n"
                    "\n"
                    "If the command-line option `-g' is provided, then the option can be given as\n"
                    "its argument a file name prefix, which can be a path with one or more\n"
                    "directories, so that a file can be created just by appending a file name to\n"
                    "the argument.  The effect of this option is that upon a successful simulation,\n"
                    "the directory will contain a file whose name is or ends in `test.cpp' that\n"
                    "(1) uses the macro directive `#include <v1.hpp>', (2) expresses the simulated\n"
                    "Tice graph using the Tice API, and (3) is compilable as a C++14 program as long\n"
                    "as a number of macros are defined as explained in the next paragraph.\n"
                    "Additionally, if the input at stdin specifies at least one `D' or `C'\n"
                    "and N and M are the number of lines specifying `D' and `C', respectively, then\n"
                    "the directory will also contain additional files whose names are or end in\n"
                    "`test-1a.cpp', `test-1b.cpp', ..., `test-Na.cpp', `test-Nb.cpp',\n"
                    "`test-1.cpp', ..., `test-M.cpp', each of which corresponds to the k-th line\n"
                    "that specifies either `D' if the name is or ends in `ka.cpp'/`kb.cpp' or\n"
                    "`C' if the name is or ends in `k.cpp' and is identical to `test.cpp' with\n"
                    "the following exception:\n"
                    "- If its name ends in `a.cpp', the n-th last-to-first end-to-end delay constraint\n"
                    "  has its lower bound's increased by 0.1%.\n"
                    "- If its name ends in `b.cpp', the n-th last-to-first end-to-end delay constraint\n"
                    "  has its upper bound's decreased by 0.1%.\n"
                    "- Otherwise, the m-th correlation constraint has its threshold decreased by 0.1%.\n"
                    "  However, if the threshold is already zero, the corresponding test file will be\n"
                    "  deleted since it is impossible to raise any compile-time error by tightening\n"
                    "  the threshold further.\n"
                    "\n"
                    "The generated C++14 program(s) can only be compiled when either macro\n"
                    "TICE_V1_NOGEN or the pair of macros CORE_IDS and WCET_IN_SECONDS is defined.\n"
                    "When TICE_V1_NOGEN is defined, the compiler will only process parts of the source\n"
                    "program that are relevant to analyze the validity of the expressed Tice model to\n"
                    "demonstrate the use-case of the compiler as a modeling tool.  On the other hand,\n"
                    "when the pair of macros is defined, the compiler will process all parts of the\n"
                    "source code to generate the executable program.  While macro CORE_IDS must be\n"
                    "defined as a non-empty comma-separated list of processor core IDs, macro\n"
                    "WCET_IN_SECONDS must be defined as a comma-separated pair of numerator and\n"
                    "denominator that expresses the maximum WCET (worst-case execution time) of the\n"
                    "program's function blocks.  Alternatively, instead of defining macro\n"
                    "WCET_IN_SECONDS, macro USE_BAD_WCET can be defined so that every node assigns\n"
                    "their periods as the WCETs of their respective function blocks.  Lastly, macro\n"
                    "FAIR_COMPARISON_OF_FRONT_END_VS_BACK_END can also be defined so that the\n"
                    "generated executable does nothing when executed because the macro prevents Tice\n"
                    "back-end from mapping the expressed Tice model into a set of real-time tasks.\n"
                    "\n"
                    "If this program was compiled by defining the macro VERBOSE, its simulation\n"
                    "produces a trace in the following format at stderr:\n"
                    "\n"
                    "\tRELEASED_NODES_AT_TIME_0\n"
                    "-------------------------------------------------------------------------------\n"
                    ".\n"
                    ".\n"
                    ".\n"
                    "-------------------------------------------------------------------------------\n"
                    "\tRELEASED_NODES_AT_TIME_H\n"
                    "===============================================================================\n"
                    "\tRELEASED_NODES_AT_TIME_s\n"
                    "-------------------------------------------------------------------------------\n"
                    ".\n"
                    ".\n"
                    ".\n"
                    "-------------------------------------------------------------------------------\n"
                    "\tRELEASED_NODES_AT_TIME_f\n"
                    "\n"
                    "That is, the simulation trace prints all nodes that are released synchronously\n"
                    "at time t in a block named `RELEASED_NODES_AT_TIME_t'.  The blocks of t = 0 and\n"
                    "t = H will contain all nodes because H is the hyperperiod of all of the nodes.\n"
                    "Once the hyperperiod is reached, the simulation is stopped graciously by feeding\n"
                    "every source node with a stop signal.  The simulation then stops once every sink\n"
                    "node has received the stop signal at time t = f.  The format of every block\n"
                    "itself is as follows:\n"
                    "NAME[ID](PERIOD)@RELEASE_TIME_IN_DECIMAL=RELEASE_TIME_AS_FRACTION[MEMORY][OUTPUT]\n"
                    "\n"
                    "An example of NAME is `v1', ID is `0', PERIOD is `5/1000',\n"
                    "RELEASE_TIME_IN_DECIMAL is `0.085', and RELEASE_TIME_AS_FRACTION is `17/200'.\n"
                    "On the other hand, MEMORY and OUTPUT show the content of the node's internal\n"
                    "memory and output buffer, which is read by the node's consumer(s).  Both MEMORY\n"
                    "and OUTPUT are either empty strings if no arc exists or follow the following\n"
                    "format otherwise where K is the number of source nodes:\n"
                    "(EVENT_FROM_SOURCE_NODE_1)...(EVENT_FROM_SOURCE_NODE_K)\n"
                    "\n"
                    "Lastly, every EVENT_FROM_SOURCE_NODE_i has four comma-separated values that\n"
                    "correspond to the earliest sensing time, the latest sensing time, the\n"
                    "last-to-first end-to-end delay, and the stop signal of every event born at source\n"
                    "node i that reaches this node, respectively.  If no value is printed, it means\n"
                    "that source node i and this node are not connected.  Beside that, the last value\n"
                    "is always 0 for every block up to and including t = H (stop signal is absent).\n"
                    "Other than that, the name of source node i can be identified by reading the first\n"
                    "block.  If the internal memory of a node in that block has some values for\n"
                    "EVENT_FROM_SOURCE_NODE_i, then source node i is none other than the node itself.\n"
                    "\n"
                    "If the preprocessor macro VERBOSE is defined with value 2, information regarding\n"
                    "end-to-end delay constraints will be printed in stderr in every block where the\n"
                    "constrained sink nodes are released with sufficient information to determine the\n"
                    "delays.  Similarly, information regarding correlation constraints will be printed\n"
                    "in stderr in every block where the confluent nodes are released with sufficient\n"
                    "information to determine the correlations.  Lastly, information regarding the\n"
                    "last-to-last end-to-end delays are reported in stderr at the end of the\n"
                    "simulation for every end-to-end delay constraint.\n"
                    "\n"
                    "Lastly, if the preprocessor macro VERBOSE is defined with value 3, information\n"
                    "regarding the DAG check will be printed in stderr before the simulation trace.\n"
                    "\n"
                    "Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>\n"
                    "                                           (GPG public key ID: 0x277B48A6)\n");
      return Error_code::invalid_option;
    }
  }

  if (generated_file_is_requested) {
    File_generator generator(generated_file_prefix);
    Error_code rc = run(&generator);
    if (rc != Error_code::none) {
      return rc;
    }
    return generator.run();
  } else {
    return run();
  }
}
