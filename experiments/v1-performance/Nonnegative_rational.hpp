/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#ifndef NONNEGATIVE_RATIONAL_HPP
#define NONNEGATIVE_RATIONAL_HPP

#include <iostream>
#include <string>

namespace tice {
  namespace v1 {

    class Nonnegative_rational {
      unsigned long long num;
      unsigned long long den;
      unsigned long long gcd;
    public:
      Nonnegative_rational(unsigned long long num, unsigned long long den = 1);
      Nonnegative_rational(const Nonnegative_rational &) noexcept;
      Nonnegative_rational &operator =(const Nonnegative_rational &) noexcept;
      Nonnegative_rational &operator +=(const Nonnegative_rational &);
      Nonnegative_rational &operator *=(const Nonnegative_rational &);
      operator double() const noexcept;
      unsigned long long get_num() const noexcept;
      unsigned long long get_den() const noexcept;
      unsigned long long get_gcd() noexcept;
      Nonnegative_rational simplify() const noexcept;
      bool is_zero() const noexcept;
    public:
      struct Invalid_den
      {
        const unsigned long long den;
        Invalid_den(unsigned long long den) :
          den(den) {
        }
      };
    public:
      struct Out_of_range
      {
        const unsigned long long max;
        Out_of_range(unsigned long long max) :
          max(max) {
        }
      };
      struct Num_out_of_range :
        Out_of_range
      {
        const unsigned long long num;
        Num_out_of_range(unsigned long long num, unsigned long long max) :
          num(num), Out_of_range(max) {
        }
      };
      struct Den_out_of_range :
        Out_of_range
      {
        const unsigned long long den;
        Den_out_of_range(unsigned long long den, unsigned long long max) :
          den(den), Out_of_range(max) {
        }
      };
    public:
      struct Divide_by_zero
      {
      };
    public:
      struct Subtractor_too_large
      {
      };
    };

    Nonnegative_rational operator +(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs);
    Nonnegative_rational operator -(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs);
    Nonnegative_rational operator *(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs);
    Nonnegative_rational operator /(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs);
    Nonnegative_rational operator %(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs);
    bool operator ==(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs);
    bool operator <(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs);
    bool operator <=(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs);
    bool operator >(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs);
    bool operator >=(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs);
    bool operator !=(const Nonnegative_rational &lhs, const Nonnegative_rational &rhs);
    std::ostream &operator <<(std::ostream &out, const Nonnegative_rational &o);
    unsigned long long floor(const Nonnegative_rational &o, const unsigned long long &scale = 1);
    unsigned long long ceil(const Nonnegative_rational &o, const unsigned long long &scale = 1);
    std::string to_string(const Nonnegative_rational &o);

  }
}

#endif // NONNEGATIVE_RATIONAL_HPP
