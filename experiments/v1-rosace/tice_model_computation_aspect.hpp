/******************************************************************************
 * Copyright (C) 2019, 2020  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#ifndef TICE_MODEL_COMPUTATION_ASPECT_HPP
#define TICE_MODEL_COMPUTATION_ASPECT_HPP

#include "controller.hpp"
#include <tice/v1.hpp>
using namespace tice::v1;

// Hardware-dependent part
#ifdef TICE_V1_NOGEN
typedef HW<Core_ids<>> target_hw;
#else
typedef HW<Core_ids<0, 1,
                    2, 3>> target_hw;
#endif

//// Function block WCETs on target_hw
typedef Comp(&f_h_s_impl,
             Ratio<1, 1000>) f_h_s;
typedef Comp(&f_a_z_s_impl,
             Ratio<1, 1000>) f_a_z_s;
typedef Comp(&f_q_s_impl,
             Ratio<1, 1000>) f_q_s;
typedef Comp(&f_V_z_s_impl,
             Ratio<1, 1000>) f_V_z_s;
typedef Comp(&f_V_a_s_impl,
             Ratio<1, 1000>) f_V_a_s;
typedef Comp(&f_h_f_impl,
             Ratio<1, 1000>) f_h_f;
typedef Comp(&f_a_z_f_impl,
             Ratio<1, 1000>) f_a_z_f;
typedef Comp(&f_q_f_impl,
             Ratio<1, 1000>) f_q_f;
typedef Comp(&f_V_z_f_impl,
             Ratio<1, 1000>) f_V_z_f;
typedef Comp(&f_V_a_f_impl,
             Ratio<1, 1000>) f_V_a_f;
typedef Comp(&f_h_h_impl,
             Ratio<1, 1000>) f_h_h;
typedef Comp(&f_V_z_impl,
             Ratio<1, 1000>) f_V_z;
typedef Comp(&f_V_a_impl,
             Ratio<1, 1000>) f_V_a;
typedef Comp(&f_L_impl,
             Ratio<1, 1000>) f_L;
typedef Comp(&f_E_impl,
             Ratio<1, 1000>) f_E;

#endif // TICE_MODEL_COMPUTATION_ASPECT_HPP
