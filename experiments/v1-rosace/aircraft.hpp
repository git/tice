/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#ifndef AIRCRAFT_HPP
#define AIRCRAFT_HPP

#include <chrono>
#include <mutex>

namespace tice {
  namespace v1 {
    namespace rosace {
      constexpr std::chrono::milliseconds simulation_step_size(5);

      constexpr double delta_e_eq = 0.012009615652468;
      constexpr double delta_th_eq = 1.5868660794926;
      constexpr double dt = std::chrono::duration_cast<std::chrono::duration<double>>(simulation_step_size).count();

      //\begin{Trimming parameters}
      constexpr double h_eq = 10000.0;
      constexpr double Va_eq = 230.0;
      constexpr double theta_eq = 0.026485847681737;
      //\end{Trimming parameters}

      class Aircraft
      {
        //\begin{common}
        std::mutex lock;

        class Device
        {
        protected:
          Aircraft &aircraft;

          Device(Aircraft &aircraft);
        };

      public:
        Aircraft();
        //\end{common}

        //\begin{actuators}
      private:
        double elevator_delta_e;
        double engine_T;

      public:
        class Elevator_actuator_device :
          public Device
        {
          friend Aircraft;

          double x1;
          double x2;
          double x1_dot; double x2_dot;

          Elevator_actuator_device(Aircraft &aircraft);

        public:
          void command(const double &V_z);

        } elevator;

      public:
        class Engine_actuator_device :
          public Device
        {
          friend Aircraft;

          double x1;
          double x1_dot;

          Engine_actuator_device(Aircraft &aircraft);

        public:
          void command(const double &V_a);

        } engine;
        //\end{actuators}

        //\begin{sensors}
      private:
        bool debut;

        double u; double w; double q; double theta; double h;
        double u_dot; double w_dot; double q_dot; double theta_dot; double h_dot;

        double CD; double CL; double Cm;
        double Xa; double Za; double Ma;

        double alpha; double qbar; double V; double rho;

        void run_simulation_for_one_step();

      public:
        class Sensor_device :
          public Device
        {
          using Device::Device;

        public:
          double sample();

        protected:
          double sensed_data;
          virtual void log_sensed_data() = 0;
        };

      public:
        class Altitude_sensor_device :
          public Sensor_device
        {
          friend Aircraft;

          using Sensor_device::Sensor_device;
          void log_sensed_data() override;

        } altitude;

        class Vertical_acceleration_sensor_device :
          public Sensor_device
        {
          friend Aircraft;

          using Sensor_device::Sensor_device;
          void log_sensed_data() override;

        } vertical_acceleration;

        class Pitch_rate_sensor_device :
          public Sensor_device
        {
          friend Aircraft;

          using Sensor_device::Sensor_device;
          void log_sensed_data() override;

        } pitch_rate;

        class Vertical_speed_sensor_device :
          public Sensor_device
        {
          friend Aircraft;

          using Sensor_device::Sensor_device;
          void log_sensed_data() override;

        } vertical_speed;

        class True_airspeed_sensor_device :
          public Sensor_device
        {
          friend Aircraft;

          using Sensor_device::Sensor_device;
          void log_sensed_data() override;

        } true_airspeed;
        //\end{sensors}

      };

      extern Aircraft aircraft;

    }
  }
}

#endif // AIRCRAFT_HPP
