/******************************************************************************
 * Copyright (C) 2019, 2020  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

extern const double
  init_h_s, init_a_z_s, init_q_s, init_V_z_s, init_V_a_s,
  init_h_f, init_a_z_f, init_q_f, init_V_z_f, init_V_a_f,
  init_h_h, init_V_z  , init_V_a;

double f_a_z_s_impl();  double f_h_s_impl();
double f_V_z_s_impl();  double f_q_s_impl();
double f_V_a_s_impl();
double f_h_f_impl(const double &h_s);
double f_a_z_f_impl(const double &a_z_s);
double f_q_f_impl(const double &q_s);
double f_V_z_f_impl(const double &V_z_s);
double f_V_a_f_impl(const double &V_a_s);
double f_h_h_impl(const double &h_f);
double f_V_z_impl(const double &h_h, const double &a_z_f,
                  const double &q_f, const double &V_z_f);
double f_V_a_impl(const double &q_f, const double &V_z_f,
                  const double &V_a_f);
void f_L_impl(const double &V_z);
void f_E_impl(const double &V_a);

#endif // CONTROLLER_HPP
