/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#ifndef TICE_MODEL_HPP
#define TICE_MODEL_HPP

#ifndef TICE_V1_NOGEN
#define TEMPORAL_CONSTRAINTS
#endif

#include <tice/v1.hpp>
#include "tice_model_computation_aspect.hpp"
#include "tice_model_communication_aspect.hpp"

using namespace tice::v1;

typedef Node<f_h_s  , Ratio<5 , 1000>> h_s  ; // Altitude sensor
typedef Node<f_a_z_s, Ratio<5 , 1000>> a_z_s; // Vertical acceleration sensor
typedef Node<f_q_s  , Ratio<5 , 1000>> q_s  ; // Pitch rate sensor
typedef Node<f_V_z_s, Ratio<5 , 1000>> V_z_s; // Vertical speed sensor
typedef Node<f_V_a_s, Ratio<5 , 1000>> V_a_s; // True airspeed sensor
typedef Node<f_h_f  , Ratio<10, 1000>> h_f  ; // Altitude filter
typedef Node<f_a_z_f, Ratio<10, 1000>> a_z_f; // Vertical acceleration filter
typedef Node<f_q_f  , Ratio<10, 1000>> q_f  ; // Pitch rate filter
typedef Node<f_V_z_f, Ratio<10, 1000>> V_z_f; // Vertical speed filter
typedef Node<f_V_a_f, Ratio<10, 1000>> V_a_f; // Airspeed filter
typedef Node<f_h_h  , Ratio<20, 1000>> h_h  ; // Altitude hold
typedef Node<f_V_z  , Ratio<20, 1000>> V_z  ; // Altitude control
typedef Node<f_V_a  , Ratio<20, 1000>> V_a  ; // Airspeed control
typedef Node<f_L    , Ratio<5 , 1000>> L    ; // Elevator actuator
typedef Node<f_E    , Ratio<5 , 1000>> E    ; // Engine thrust actuator
typedef Program</* 1*/target_hw,
                /* 2*/h_s, /* 3*/a_z_s, /* 4*/q_s, /* 5*/V_z_s, /* 6*/V_a_s,
                /* 7*/h_f, /* 8*/a_z_f, /* 9*/q_f, /*10*/V_z_f, /*11*/V_a_f,
                /*12*/h_h, /*13*/V_z  , /*14*/V_a, /*15*/L    , /*16*/E,
                /*17*/Feeder<h_s  ,   h_s_to_h_f  , h_f>,
                /*18*/Feeder<a_z_s, a_z_s_to_a_z_f, a_z_f>,
                /*19*/Feeder<q_s  ,   q_s_to_q_f  , q_f>,
                /*20*/Feeder<V_z_s, V_z_s_to_V_z_f, V_z_f>,
                /*21*/Feeder<V_a_s, V_a_s_to_V_a_f, V_a_f>,
                /*22*/Feeder<h_f  ,   h_f_to_h_h  , h_h>,
                /*23*/Feeder<h_h  ,   h_h_to_V_z  ,
                             a_z_f, a_z_f_to_V_z  ,
                             q_f  ,   q_f_to_V_z  ,
                             V_z_f, V_z_f_to_V_z  , V_z>,
                /*24*/Feeder<q_f  ,   q_f_to_V_a  ,
                             V_z_f, V_z_f_to_V_a  ,
                             V_a_f, V_a_f_to_V_a  , V_a>,
                /*25*/Feeder<V_z  ,   V_z_to_L    , L>,
                /*26*/Feeder<V_a  ,   V_a_to_E    , E>
                /*27*/TEMPORAL_CONSTRAINTS> Prog;

#endif // TICE_MODEL_HPP
