/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <chrono>
#include <iostream>
#include "aircraft.hpp"

namespace tice {
  namespace v1 {
    namespace rosace {

      class Logger
      {
      public:
        typedef std::chrono::high_resolution_clock Clock;

        static constexpr std::chrono::minutes duration {10};
        static constexpr std::chrono::milliseconds resolution {50};

      private:
        enum traced_value {
          h_s,   // Altitude sensor
          a_z_s, // Vertical acceleration sensor
          q_s,   // Pitch rate sensor
          V_z_s, // Vertical speed sensor
          V_a_s, // True airspeed sensor
          sensor_count,
          L = sensor_count, // Elevator actuator
          E,                // Engine thrust actuator
          size,
          actuator_count = size - sensor_count,
        };

        double trace_data[traced_value::size][1 + duration / resolution];
        static constexpr unsigned trace_data_size = sizeof(trace_data[0]) / sizeof(trace_data[0][0]);
        static constexpr unsigned trace_skip_size = resolution / simulation_step_size;

        static constexpr unsigned simulation_step_count = duration / simulation_step_size + 1;

        struct {
          typename Clock::time_point sensing_time;
          unsigned simulation_forward_count;
        } simulation_data[traced_value::sensor_count * simulation_step_count];
        static constexpr unsigned simulation_data_size = sizeof(simulation_data) / sizeof(simulation_data[0]);

        static constexpr unsigned trace_data_fp_printout_scale = 15; // follows `rosace-reference_data.csv'
        static constexpr unsigned sensing_time_data_fp_printout_scale = 9; // nanosecond resolution

        bool has_been_started;

      public:
        Logger() = default;

        void start();

        void log_simulation_step(const typename Clock::time_point &sensing_time, const unsigned &simulation_forward_count);
        void log_altitude(const double &h_s);
        void log_vertical_acceleration(const double &a_z_s);
        void log_pitch_rate(const double &q_s);
        void log_vertical_speed(const double &V_z_s);
        void log_true_airspeed(const double &V_a_s);
        void log_elevation_delta(const double &L);
        void log_thrust_delta(const double &E);

        void print(std::ostream &stdout, std::ostream &stderr);

      };

      extern Logger logger;

    }
  }
}

#endif // LOGGER_HPP
