/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <thread>
#include <iostream>
#include "aircraft.hpp"
#include "logger.hpp"

int main()
{
  using namespace tice::v1;
  using rosace::Logger;
  using Clock = Logger::Clock;
  auto t = Clock::now(), t_end = t + Logger::duration + Logger::resolution;
  unsigned offset = 0;

  auto &logger = rosace::logger;

  logger.log_thrust_delta(offset++);
  logger.log_elevation_delta(offset++);

  logger.start();

  unsigned i = 0;
  constexpr unsigned i_reset_interval = Logger::resolution / rosace::simulation_step_size;
  while (t < t_end) {
    logger.log_true_airspeed(offset);
    logger.log_simulation_step(Clock::now(), 1);

    logger.log_thrust_delta(offset + 5);
    logger.log_elevation_delta(offset + 6);

    logger.log_vertical_acceleration(offset + 1);
    logger.log_simulation_step(Clock::now(), 0);

    logger.log_pitch_rate(offset + 2);
    logger.log_simulation_step(Clock::now(), 0);

    logger.log_vertical_speed(offset + 3);
    logger.log_simulation_step(Clock::now(), 0);

    logger.log_altitude(offset + 4);
    logger.log_simulation_step(Clock::now(), 0);

    i %= i_reset_interval;
    if (!i++) {
      offset += 7;
    }

    std::this_thread::sleep_until(t += rosace::simulation_step_size);
  }

  logger.print(std::cout, std::cerr);
}
