/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <cstdint>
#include <limits>
#include <iostream>
#include <thread>
#include <chrono>
#include <mutex>
#include <cstring>
#include <atomic>
#include <condition_variable>
#include <cerrno>
#include <cstdio>
#include "controller.hpp"
#include "logger.hpp"

//\begin{platform-specific header files}
#include <sched.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <linux/sched.h>
#include <sys/mount.h>
//\end{platform-specific header files}

namespace {

  const int core_ids[] = {0, 1, 2, 3};

  const int wcet_in_ms = 1;

  enum Node_ID {
    h_s, a_z_s, q_s, V_z_s, V_a_s,
    h_f, a_z_f, q_f, V_z_f, V_a_f,
    h_h,   V_z, V_a,     L,     E,
    count,
  };

  const int node_period_in_ms[Node_ID::count] = {
     5,  5,  5,  5,  5,
    10, 10, 10, 10, 10,
    20, 20, 20,  5,  5,
  };

}
namespace {

  class Node
  {
    static std::atomic<bool> not_stopped;
    static long error_code;
    static std::mutex start_lock;
    static std::mutex ready_to_start_lock;
    static std::condition_variable ready_to_start;
    static Node nodes[Node_ID::count];

  public:
    static long get_error_code()
    {
      return error_code;
    }

    static void run()
    {
      //\begin{platform-specific region}
      cpu_set_t coreset;
      CPU_ZERO(&coreset);
      for (int i = 0, i_end = sizeof(core_ids) / sizeof(core_ids[0]); i < i_end; ++i) {
        CPU_SET(core_ids[i], &coreset);
      }
      if (sched_setaffinity(0, sizeof(coreset), &coreset)) {
        error_code = errno;
        return;
      }

      char one_line[256];
      bool cpuset_is_mounted = false;
      {
        std::FILE *mountinfo_file = std::fopen("/proc/self/mountinfo", "rb");
        if (!mountinfo_file) {
          error_code = errno;
          return;
        }
        while (std::fgets(one_line, sizeof(one_line), mountinfo_file)) {
          if (std::strstr(one_line, " cpuset ")) {
            cpuset_is_mounted = true;
            break;
          }
        }
        std::fclose(mountinfo_file);
      }

      char cpuset_mount_point[128];
      if (cpuset_is_mounted) {
        char *start_ptr = std::strstr(one_line, " /");
        if (!start_ptr) {
          error_code = ENOENT;
          return;
        }
        start_ptr = std::strstr(start_ptr + 1, " /");
        if (!start_ptr) {
          error_code = ENOENT;
          return;
        }
        ++start_ptr;

        char *end_ptr = std::strstr(start_ptr, " - ");
        if (!end_ptr) {
          error_code = ENOENT;
          return;
        }
        for (int i = 0; i < 2; ++i) {
          *end_ptr = '\0';
          end_ptr = std::strrchr(start_ptr, ' ');
          if (!end_ptr) {
            error_code = ENOENT;
            return;
          }
        }
        *end_ptr = '\0';

        if (end_ptr - start_ptr >= sizeof(cpuset_mount_point)) {
          error_code = ENAMETOOLONG;
          return;
        }
        std::strcpy(cpuset_mount_point, start_ptr);
      } else {
#define default_cpuset_mount_point "/dev/cpuset"
        if (sizeof(default_cpuset_mount_point) > sizeof(cpuset_mount_point)) {
          error_code = ENAMETOOLONG;
          return;
        }
        std::strcpy(cpuset_mount_point, default_cpuset_mount_point);
#undef default_cpuset_mount_point

        struct stat stat_result;
        if (stat(cpuset_mount_point, &stat_result)) {
          if (errno == ENOENT) {
            if (mkdir(cpuset_mount_point, 0755)) {
              error_code = errno;
              return;
            }
          } else {
            error_code = errno;
            return;
          }
        } else {
          error_code = EEXIST;
          return;
        }

        if (mount("cpuset", cpuset_mount_point, "cgroup", 0, "cpuset")) {
          error_code = errno;
          return;
        }
      }

      char cpuset_dir_path[128];
      for (int i = 1; i < std::numeric_limits<int>::max(); ++i) {
        if (std::snprintf(cpuset_dir_path, sizeof(cpuset_dir_path), "%s/tice-%llu",
                          cpuset_mount_point, static_cast<long long unsigned>(i)) >= sizeof(cpuset_dir_path)) {
          error_code = ENAMETOOLONG;
          return;
        }
        struct stat stat_result;
        if (stat(cpuset_dir_path, &stat_result)) {
          if (errno != ENOENT) {
            continue;
          }
        } else {
          if (std::remove(cpuset_dir_path)) {
            continue;
          }
        }
        break;
      }
      if (mkdir(cpuset_dir_path, 0755)) {
        error_code = errno;
        return;
      }

      char load_balancing_file_path[128];
      char cpuset_file_path[128];
      char task_file_path[128];
      if ((std::snprintf(load_balancing_file_path, sizeof(load_balancing_file_path), "%s/cpuset.sched_load_balance",
                         cpuset_mount_point) >= sizeof(load_balancing_file_path))
          || (std::snprintf(cpuset_file_path, sizeof(cpuset_file_path), "%s/cpuset.cpus",
                            cpuset_dir_path) >= sizeof(cpuset_file_path))
          || (std::snprintf(task_file_path, sizeof(task_file_path), "%s/tasks",
                            cpuset_dir_path) >= sizeof(task_file_path))) {
        error_code = ENAMETOOLONG;
        return;
      }

      struct stat stat_result;
      if (stat(load_balancing_file_path, &stat_result)
          || stat(cpuset_file_path, &stat_result)
          || stat(task_file_path, &stat_result)) {
        error_code = errno;
        return;
      }

      std::FILE *load_balancing_file = std::fopen(load_balancing_file_path, "rb");
      if (!load_balancing_file) {
        error_code = errno;
        return;
      }
      int load_balancing_is_active;
      if (std::fscanf(load_balancing_file, "%d", &load_balancing_is_active) != 1) {
        error_code = errno;
        std::fclose(load_balancing_file);
        return;
      }
      if (std::fclose(load_balancing_file)) {
        error_code = errno;
        return;
      }
      if (load_balancing_is_active) {
        load_balancing_file = std::fopen(load_balancing_file_path, "wb");
        if (load_balancing_file) {
          std::fprintf(load_balancing_file, "0\n");
          if (std::fclose(load_balancing_file)) {
            error_code = errno;
            return;
          }
        } else {
          error_code = errno;
          return;
        }
      }

      std::FILE *cpuset_file = std::fopen(cpuset_file_path, "wb");
      if (cpuset_file) {
        std::fprintf(cpuset_file, "%d", core_ids[0]);
        for (int i = 1, i_end = sizeof(core_ids) / sizeof(core_ids[0]); i < i_end; ++i) {
          std::fprintf(cpuset_file, ",%d", core_ids[i]);
        }
        std::fprintf(cpuset_file, "\n");
        if (std::fclose(cpuset_file)) {
          error_code = errno;
          return;
        }
      } else {
        error_code = errno;
        return;
      }

      std::FILE *task_file = std::fopen(task_file_path, "wb");
      if (task_file) {
        std::fprintf(task_file, "%llu\n", static_cast<long long unsigned>(getpid()));
        if (std::fclose(task_file)) {
          error_code = errno;
          return;
        }
      } else {
        error_code = errno;
        return;
      }
      //\end{platform-specific region}

      std::unique_lock<std::mutex> start_lock(Node::start_lock);
      std::unique_lock<std::mutex> ready_to_start_lock(Node::ready_to_start_lock);
      for (int i = 0; i < Node_ID::count; ++i) {
        nodes[i].thread = std::thread(&Node::run_periodic_task, &nodes[i]);
        ready_to_start.wait(ready_to_start_lock);
        if (error_code) {
          stop();
          start_lock.unlock();
          wait();
          break;
        }
      }
    }

    static void stop()
    {
      std::atomic_store_explicit(&not_stopped, false, std::memory_order::memory_order_relaxed);
    }

    static void wait()
    {
      for (int i = 0; i < Node_ID::count; ++i) {
        if (nodes[i].thread.joinable()) {
          nodes[i].thread.join();
        }
      }
    }

  private:
    std::thread thread;
    int period_in_ms;
    void (*computation)(const int &release_idx, const int &period_in_ms);

    inline long setup_scheduler()
    {
      //\begin{platform-specific region}
      struct {
        uint32_t size;
        uint32_t sched_policy;
        uint64_t sched_flags;
        int32_t sched_nice;
        uint32_t sched_priority;
        uint64_t sched_runtime;
        uint64_t sched_deadline;
        uint64_t sched_period;
      } scheduling_attributes = {0};
      scheduling_attributes.size = sizeof(scheduling_attributes);
      scheduling_attributes.sched_policy = SCHED_DEADLINE;
      scheduling_attributes.sched_runtime
        = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::duration<int, std::milli>(wcet_in_ms)).count();
      scheduling_attributes.sched_deadline
        = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::duration<int, std::milli>(period_in_ms)).count();
      return syscall(SYS_sched_setattr, 0, &scheduling_attributes, 0) ? errno : 0;
      //\end{platform-specific region}
    }

    inline bool wait_for_start_signal(const long &scheduler_setup_error_code)
    {
      std::unique_lock<std::mutex> ready_to_start_lock(Node::ready_to_start_lock);
      error_code = scheduler_setup_error_code;
      ready_to_start.notify_one();
      ready_to_start_lock.unlock();

      std::lock_guard<std::mutex> wait(start_lock);
      return !error_code;
    }

    inline void run_periodically_until_stopped()
    {
      typedef std::chrono::high_resolution_clock::duration Delta;
      const Delta period = std::chrono::duration_cast<Delta>(std::chrono::duration<int, std::milli>(period_in_ms));
      std::chrono::time_point<std::chrono::high_resolution_clock> release_time(std::chrono::high_resolution_clock::now());
      unsigned release_idx = 0;
      while (std::atomic_load_explicit(&not_stopped, std::memory_order::memory_order_relaxed)) {
        computation(release_idx++, period_in_ms);
        std::this_thread::sleep_until(release_time += period);
      }
    }

    void run_periodic_task()
    {
      if (wait_for_start_signal(setup_scheduler())) {
        run_periodically_until_stopped();
      }
    }

  public:
    Node(const int &period_in_ms, void (*computation)(const int &release_idx, const int &period_in_ms))
      : thread(), period_in_ms(period_in_ms), computation(computation) {
    }

  };

}
namespace {

  std::atomic<bool> Node::not_stopped = ATOMIC_VAR_INIT(true);
  long Node::error_code = 0;
  std::mutex Node::start_lock;
  std::mutex Node::ready_to_start_lock;
  std::condition_variable Node::ready_to_start;

}
namespace {

  static constexpr double epsilon = 1E4 * std::numeric_limits<double>::epsilon();

  inline std::intmax_t ceil(double x)
  {
    std::intmax_t integral_part = static_cast<std::intmax_t>(x);
    return integral_part + (x < 0
                            ? ((integral_part - 1) - x <= -epsilon ? 0 : -1)
                            : (x - integral_part < epsilon ? 0 : 1));
  }

  inline std::intmax_t floor(double x)
  {
    std::intmax_t integral_part = static_cast<std::intmax_t>(x);
    return integral_part + (x < 0
                            ? (x - integral_part <= -epsilon ? -1 : 0)
                            : ((integral_part + 1) - x < epsilon ? 1 : 0));
  }

  inline bool lt(double lhs, double rhs)
  {
    constexpr double positive_infinity = std::numeric_limits<double>::infinity();

    if (lhs == positive_infinity && rhs == positive_infinity) {
      return false;
    } else if (lhs == positive_infinity) {
      return false;
    } else if (rhs == positive_infinity) {
      return true;
    } else {
      return lhs < rhs - epsilon;
    }
  }

}
namespace {

  template<typename arg__channel_type>
  class Channel
  {
  private:
    std::mutex buffer_lock;
    arg__channel_type communication_array[3];
    std::intmax_t usage_array[3];

  public:
    Channel(const arg__channel_type &initial_value, const double &producer_period, const double &consumer_period) :
      communication_array{initial_value, initial_value, initial_value},
      usage_array{0, 0, ceil(producer_period / consumer_period)} {
    }

    arg__channel_type *get_buffer_to_write(int producer_release_idx,
                                           const double &producer_period, const double &consumer_period)
    {
      static int last_communication_array_idx = 1;

      ++producer_release_idx;

      const int consumer_end_release_idx = ceil(producer_period * (producer_release_idx + 1)
                                                / consumer_period);

      if (lt(consumer_period * (consumer_end_release_idx - 1),
             producer_period * producer_release_idx)) {
        return nullptr;
      }

      const int consumer_start_release_idx = floor(producer_period * producer_release_idx / consumer_period);

      int i = 2;
      if (usage_array[0] <= consumer_start_release_idx) {
        if (usage_array[1] <= consumer_start_release_idx && usage_array[1] < usage_array[0]) {
          i = 1;
        } else if (!(usage_array[2] <= consumer_start_release_idx && usage_array[2] < usage_array[0])) {
          i = 0;
        }
      } else if (usage_array[1] <= consumer_start_release_idx) {
        if (!(usage_array[2] <= consumer_start_release_idx && usage_array[2] < usage_array[1])) {
          i = 1;
        }
      }

      std::lock_guard<std::mutex> critical_region(buffer_lock);
      usage_array[i] = consumer_end_release_idx;
      return communication_array + i;
    }

    arg__channel_type &get_buffer_to_read(const int &consumer_release_idx)
    {
      std::lock_guard<std::mutex> critical_region(buffer_lock);

      if (usage_array[0] > consumer_release_idx) {

        if (usage_array[1] > consumer_release_idx && usage_array[1] < usage_array[0]) {
          return communication_array[1];
        }
        if (usage_array[2] > consumer_release_idx && usage_array[2] < usage_array[0]) {
          return communication_array[2];
        }

        return communication_array[0];
      }

      if (usage_array[1] > consumer_release_idx) {

        if (usage_array[2] > consumer_release_idx && usage_array[2] < usage_array[1]) {
          return communication_array[2];
        }

        return communication_array[1];
      }

      return communication_array[2];
    }
  };

}
namespace {

  Channel<double>   h_s_to_h_f  (init_h_s  , node_period_in_ms[Node_ID::h_s  ], node_period_in_ms[Node_ID::h_f  ]);
  Channel<double> a_z_s_to_a_z_f(init_a_z_s, node_period_in_ms[Node_ID::a_z_s], node_period_in_ms[Node_ID::a_z_f]);
  Channel<double>   q_s_to_q_f  (init_q_s  , node_period_in_ms[Node_ID::q_s  ], node_period_in_ms[Node_ID::q_f  ]);
  Channel<double> V_z_s_to_V_z_f(init_V_z_s, node_period_in_ms[Node_ID::V_z_s], node_period_in_ms[Node_ID::V_z_f]);
  Channel<double> V_a_s_to_V_a_f(init_V_a_s, node_period_in_ms[Node_ID::V_a_s], node_period_in_ms[Node_ID::V_a_f]);
  Channel<double>   h_f_to_h_h  (init_h_f  , node_period_in_ms[Node_ID::h_f  ], node_period_in_ms[Node_ID::h_h  ]);
  Channel<double>   h_h_to_V_z  (init_h_h  , node_period_in_ms[Node_ID::h_h  ], node_period_in_ms[Node_ID::V_z  ]);
  Channel<double> a_z_f_to_V_z  (init_a_z_f, node_period_in_ms[Node_ID::a_z_f], node_period_in_ms[Node_ID::V_z  ]);
  Channel<double>   q_f_to_V_z  (init_q_f  , node_period_in_ms[Node_ID::q_f  ], node_period_in_ms[Node_ID::V_z  ]);
  Channel<double> V_z_f_to_V_z  (init_V_z_f, node_period_in_ms[Node_ID::V_z_f], node_period_in_ms[Node_ID::V_z  ]);
  Channel<double>   q_f_to_V_a  (init_q_f  , node_period_in_ms[Node_ID::q_f  ], node_period_in_ms[Node_ID::V_a  ]);
  Channel<double> V_z_f_to_V_a  (init_V_z_f, node_period_in_ms[Node_ID::V_z_f], node_period_in_ms[Node_ID::V_a  ]);
  Channel<double> V_a_f_to_V_a  (init_V_a_f, node_period_in_ms[Node_ID::V_a_f], node_period_in_ms[Node_ID::V_a  ]);
  Channel<double>   V_z_to_L    (init_V_z  , node_period_in_ms[Node_ID::V_z  ], node_period_in_ms[Node_ID::L    ]);
  Channel<double>   V_a_to_E    (init_V_a  , node_period_in_ms[Node_ID::V_a  ], node_period_in_ms[Node_ID::E    ]);

}
namespace {

  template<typename T, typename... Ts>
  inline void update_channels(const T &producer_output, Ts *... channels) {
    [](auto...){}((channels && (*channels = producer_output, false))...);
  }

  Node Node::nodes[Node_ID::count] = {
    {node_period_in_ms[Node_ID::h_s  ], [](const int &release_idx, const int &period) {
                                          update_channels(f_h_s_impl(),
                                                          h_s_to_h_f.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::h_f]));
                                        }},
    {node_period_in_ms[Node_ID::a_z_s], [](const int &release_idx, const int &period) {
                                          update_channels(f_a_z_s_impl(),
                                                          a_z_s_to_a_z_f.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::a_z_f]));
                                        }},
    {node_period_in_ms[Node_ID::q_s  ], [](const int &release_idx, const int &period) {
                                          update_channels(f_q_s_impl(),
                                                          q_s_to_q_f.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::q_f]));
                                        }},
    {node_period_in_ms[Node_ID::V_z_s], [](const int &release_idx, const int &period) {
                                          update_channels(f_V_z_s_impl(),
                                                          V_z_s_to_V_z_f.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::V_z_f]));
                                        }},
    {node_period_in_ms[Node_ID::V_a_s], [](const int &release_idx, const int &period) {
                                          update_channels(f_V_a_s_impl(),
                                                          V_a_s_to_V_a_f.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::V_a_f]));
                                        }},
    {node_period_in_ms[Node_ID::h_f  ], [](const int &release_idx, const int &period) {
                                          update_channels(f_h_f_impl(h_s_to_h_f.get_buffer_to_read(release_idx)),
                                                          h_f_to_h_h.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::h_h]));
                                        }},
    {node_period_in_ms[Node_ID::a_z_f], [](const int &release_idx, const int &period) {
                                          update_channels(f_a_z_f_impl(a_z_s_to_a_z_f.get_buffer_to_read(release_idx)),
                                                          a_z_f_to_V_z.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::V_z]));
                                        }},
    {node_period_in_ms[Node_ID::q_f  ], [](const int &release_idx, const int &period) {
                                          update_channels(f_q_f_impl(q_s_to_q_f.get_buffer_to_read(release_idx)),
                                                          q_f_to_V_z.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::V_z]),
                                                          q_f_to_V_a.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::V_a]));
                                        }},
    {node_period_in_ms[Node_ID::V_z_f], [](const int &release_idx, const int &period) {
                                          update_channels(f_V_z_f_impl(V_z_s_to_V_z_f.get_buffer_to_read(release_idx)),
                                                          V_z_f_to_V_z.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::V_z]),
                                                          V_z_f_to_V_a.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::V_a]));
                                        }},
    {node_period_in_ms[Node_ID::V_a_f], [](const int &release_idx, const int &period) {
                                          update_channels(f_V_a_f_impl(V_a_s_to_V_a_f.get_buffer_to_read(release_idx)),
                                                          V_a_f_to_V_a.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::V_a]));
                                        }},
    {node_period_in_ms[Node_ID::h_h  ], [](const int &release_idx, const int &period) {
                                          update_channels(f_h_h_impl(h_f_to_h_h.get_buffer_to_read(release_idx)),
                                                          h_h_to_V_z.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::V_z]));
                                        }},
    {node_period_in_ms[Node_ID::V_z  ], [](const int &release_idx, const int &period) {
                                          update_channels(f_V_z_impl(h_h_to_V_z.get_buffer_to_read(release_idx),
                                                                     a_z_f_to_V_z.get_buffer_to_read(release_idx),
                                                                     q_f_to_V_z.get_buffer_to_read(release_idx),
                                                                     V_z_f_to_V_z.get_buffer_to_read(release_idx)),
                                                          V_z_to_L.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::L]));
                                        }},
    {node_period_in_ms[Node_ID::V_a  ], [](const int &release_idx, const int &period) {
                                          update_channels(f_V_a_impl(q_f_to_V_a.get_buffer_to_read(release_idx),
                                                                     V_z_f_to_V_a.get_buffer_to_read(release_idx),
                                                                     V_a_f_to_V_a.get_buffer_to_read(release_idx)),
                                                          V_a_to_E.get_buffer_to_write(release_idx, period, node_period_in_ms[Node_ID::E]));
                                        }},
    {node_period_in_ms[Node_ID::L    ], [](const int &release_idx, const int &period) {
                                          f_L_impl(V_z_to_L.get_buffer_to_read(release_idx));
                                        }},
    {node_period_in_ms[Node_ID::E    ], [](const int &release_idx, const int &period) {
                                          f_E_impl(V_a_to_E.get_buffer_to_read(release_idx));
                                        }},
  };

}

int main()
{
  tice::v1::rosace::logger.start();

  Node::run();
  if (Node::get_error_code()) {
    std::cerr << "Error: " << std::strerror(Node::get_error_code()) << '\n';
    return 1;
  }

  std::this_thread::sleep_for(tice::v1::rosace::Logger::duration
                              + tice::v1::rosace::Logger::resolution);

  Node::stop();

  Node::wait();

  tice::v1::rosace::logger.print(std::cout, std::cerr);

  return 0;
}
