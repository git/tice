/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <thread>
#include <iostream>
#include "controller.hpp"
#include "logger.hpp"

int main()
{
  using namespace tice::v1;
  using rosace::Logger;
  using Clock = Logger::Clock;
  auto t = Clock::now(), t_end = t + Logger::duration + Logger::resolution;

  auto &logger = rosace::logger;

  logger.start();

  double h_s = init_h_s;
  double a_z_s = init_a_z_s;
  double q_s = init_q_s;
  double V_z_s = init_V_z_s;
  double V_a_s = init_V_a_s;
  double h_f = init_h_f;
  double a_z_f = init_a_z_f;
  double q_f = init_q_f;
  double V_z_f = init_V_z_f;
  double V_a_f = init_V_a_f;
  double h_h = init_h_h;
  double V_z = init_V_z;
  double V_a = init_V_a;

  unsigned i = 0, j = 0;
  while (t < t_end) {
    // Released at 0 ms
    f_L_impl(V_z);
    f_E_impl(V_a);

    double V_z_at_20_ms = f_V_z_impl(h_h, a_z_f, q_f, V_z_f);
    double V_a_at_20_ms = f_V_a_impl(q_f, V_z_f, V_a_f);

    h_h = f_h_h_impl(h_f);

    f_h_f_impl(h_s);
    f_a_z_f_impl(a_z_s);
    f_q_f_impl(q_s);
    f_V_z_f_impl(V_z_s);
    f_V_a_f_impl(V_a_s);

    f_h_s_impl();
    f_a_z_s_impl();
    f_q_s_impl();
    f_V_z_s_impl();
    f_V_a_s_impl();

    std::this_thread::sleep_until(t += rosace::simulation_step_size);

    // Released at 5 ms
    f_L_impl(V_z);
    f_E_impl(V_a);

    h_s = f_h_s_impl();
    a_z_s = f_a_z_s_impl();
    q_s = f_q_s_impl();
    V_z_s = f_V_z_s_impl();
    V_a_s = f_V_a_s_impl();

    std::this_thread::sleep_until(t += rosace::simulation_step_size);

    // Released at 10 ms
    f_L_impl(V_z);
    f_E_impl(V_a);

    h_f = f_h_f_impl(h_s);
    a_z_f = f_a_z_f_impl(a_z_s);
    q_f = f_q_f_impl(q_s);
    V_z_f = f_V_z_f_impl(V_z_s);
    V_a_f = f_V_a_f_impl(V_a_s);

    f_h_s_impl();
    f_a_z_s_impl();
    f_q_s_impl();
    f_V_z_s_impl();
    f_V_a_s_impl();

    std::this_thread::sleep_until(t += rosace::simulation_step_size);

    // Released at 15 ms
    f_L_impl(V_z);
    f_E_impl(V_a);

    h_s = f_h_s_impl();
    a_z_s = f_a_z_s_impl();
    q_s = f_q_s_impl();
    V_z_s = f_V_z_s_impl();
    V_a_s = f_V_a_s_impl();

    std::this_thread::sleep_until(t += rosace::simulation_step_size);

    // Released at 20 ms
    V_z = V_z_at_20_ms;
    V_a = V_a_at_20_ms;
  }

  logger.print(std::cout, std::cerr);
}
