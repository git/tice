/******************************************************************************
 * Copyright (C) 2019, 2020  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#ifndef TICE_MODEL_COMMUNICATION_ASPECT_HPP
#define TICE_MODEL_COMMUNICATION_ASPECT_HPP

#include "controller.hpp"
#include <tice/v1.hpp>
using namespace tice::v1;

// Data-dependent part
typedef Chan<double, &init_h_s  >   h_s_to_h_f  ;
typedef Chan<double, &init_a_z_s> a_z_s_to_a_z_f;
typedef Chan<double, &init_q_s  >   q_s_to_q_f  ;
typedef Chan<double, &init_V_z_s> V_z_s_to_V_z_f;
typedef Chan<double, &init_V_a_s> V_a_s_to_V_a_f;
typedef Chan<double, &init_h_f  >   h_f_to_h_h  ;
typedef Chan<double, &init_h_h  >   h_h_to_V_z  ;
typedef Chan<double, &init_a_z_f> a_z_f_to_V_z  ;
typedef Chan<double, &init_q_f  >   q_f_to_V_z  ;
typedef Chan<double, &init_V_z_f> V_z_f_to_V_z  ;
typedef Chan<double, &init_q_f  >   q_f_to_V_a  ;
typedef Chan<double, &init_V_z_f> V_z_f_to_V_a  ;
typedef Chan<double, &init_V_a_f> V_a_f_to_V_a  ;
typedef Chan<double, &init_V_z  >   V_z_to_L    ;
typedef Chan<double, &init_V_a  >   V_a_to_E    ;

#endif // TICE_MODEL_COMMUNICATION_ASPECT_HPP
