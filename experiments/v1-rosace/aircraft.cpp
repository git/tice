/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <cmath>
#include "logger.hpp"
#include "controller.hpp"
#include "aircraft.hpp"

namespace tice {
  namespace v1 {
    namespace rosace {

      namespace { // Period/Frequency of the nodes
        constexpr double dt_de = dt;
        constexpr double dt_dx = dt;
      }

      namespace { // Atmosphere parameters
        constexpr double rho0 = 1.225;
        constexpr double g0 = 9.80665;
        constexpr double T0_0 = 288.15;
        constexpr double T0_h = -0.0065;
        constexpr double Rs = 287.05;
      }

      namespace { // Aircraft parameters
        constexpr double masse = 57837.5;
        constexpr double I_y = 3781272.0;
        constexpr double S = 122.6;
        constexpr double cbar = 4.29;
        constexpr double CD_0 = 0.016;
        constexpr double CD_alpha = 2.5;
        constexpr double CD_deltae = 0.05;
        constexpr double CL_alpha = 5.5;
        constexpr double CL_deltae = 0.193;
        constexpr double alpha_0 = -0.05;
        constexpr double Cm_0 = 0.04;
        constexpr double Cm_alpha = -0.83;
        constexpr double Cm_deltae = -1.5;
        constexpr double Cm_q = -30;
      }

      Aircraft::Aircraft() :
        lock(),

        altitude(*this), vertical_acceleration(*this), pitch_rate(*this), vertical_speed(*this), true_airspeed(*this),
        elevator(*this), engine(*this),

        elevator_delta_e(0.0),
        engine_T(0.0),

        debut(true),

        u(0.0), w(0.0), q(0.0), theta(0.0), h(0.0),
        u_dot(0.0), w_dot(0.0), q_dot(0.0), theta_dot(0.0), h_dot(0.0),

        CD(0.0), CL(0.0), Cm(0.0),
        Xa(0.0), Za(0.0), Ma(0.0),

        alpha(0.0), qbar(0.0), V(0.0), rho(0.0)
      {
        elevator.command(init_V_z);
        engine.command(init_V_a);
      }
    }
    namespace rosace {

      Aircraft::Device::Device(Aircraft &aircraft) :
        aircraft(aircraft) {
      }

    }
    namespace rosace {

      Aircraft::Elevator_actuator_device::Elevator_actuator_device(Aircraft &aircraft) :
        Device(aircraft),
        x1(delta_e_eq),
        x2(0.0),
        x1_dot(0.0), x2_dot(0.0) {
      }

      void Aircraft::Elevator_actuator_device::command(const double &V_z)
      {
        constexpr double omega = 25.0, xi = 0.85;

        logger.log_elevation_delta(V_z);

        // Output
        {
          std::lock_guard<decltype(aircraft.lock)> critical_region(aircraft.lock);
          aircraft.elevator_delta_e = x1;
        }
        // State Equation
        x1_dot = x2;
        x2_dot = -omega * omega * x1 - 2.0 * xi * omega * x2 + omega * omega * V_z;
        // Update State
        x1 += dt_de * x1_dot;
        x2 += dt_de * x2_dot;
      }

    }
    namespace rosace {

      Aircraft::Engine_actuator_device::Engine_actuator_device(Aircraft &aircraft) :
        Device(aircraft),
        x1(delta_th_eq),
        x1_dot(0.0) {
      }

      void Aircraft::Engine_actuator_device::command(const double &V_a)
      {
        constexpr double tau = 0.75;

        logger.log_thrust_delta(V_a);

        // Output
        {
          std::lock_guard<decltype(aircraft.lock)> critical_region(aircraft.lock);
          aircraft.engine_T = 26350.0 * x1;
        }
        // State Equation
        x1_dot = -tau * x1 + tau * V_a;
        // Update State
        x1 += dt_dx * x1_dot;
      }

    }
    namespace rosace {

      double Aircraft::Sensor_device::sample()
      {
        aircraft.run_simulation_for_one_step();
        log_sensed_data();
        return sensed_data;
      }

    }
    namespace rosace {

      void Aircraft::run_simulation_for_one_step()
      {
        typedef std::chrono::high_resolution_clock Clock;
        static typename Clock::time_point time_for_one_step;

        std::lock_guard<decltype(lock)> critical_region(lock);

        unsigned simulation_forward_count = 0;
        const auto now = Clock::now();
        if (debut) {
          time_for_one_step = now;
          debut = false;
          u = Va_eq * std::cos(theta_eq);
          w = Va_eq * std::sin(theta_eq);
          q = 0.0;
          theta = theta_eq;
          h = h_eq;
        }
        if (now >= time_for_one_step) {
          simulation_forward_count = 1 + (std::chrono::duration_cast<std::chrono::milliseconds>(now - time_for_one_step)
                                          / simulation_step_size);
          time_for_one_step += simulation_forward_count * simulation_step_size;
        }
        logger.log_simulation_step(now, simulation_forward_count);
        if (!simulation_forward_count) {
          return;
        }

        double delta_e = elevator_delta_e;
        double T = engine_T;
        while (simulation_forward_count--) {
          rho = rho0 * std::pow(1.0 + T0_h / T0_0 * h, -g0 / (Rs * T0_h) - 1.0);
          alpha = std::atan(w / u);
          V = std::sqrt(u * u + w * w);
          qbar = 0.5 * rho * V * V;
          CL = CL_deltae * delta_e + CL_alpha * (alpha - alpha_0);
          CD = CD_0 + CD_deltae * delta_e + CD_alpha * (alpha - alpha_0) * (alpha - alpha_0);
          Cm = Cm_0 + Cm_deltae * delta_e + Cm_alpha * alpha + 0.5 * Cm_q * q * cbar / V;
          Xa = -qbar * S * (CD * std::cos(alpha) - CL * std::sin(alpha));
          Za = -qbar * S * (CD * std::sin(alpha) + CL * std::cos(alpha));
          Ma = qbar * cbar * S * Cm;

          // Output
          altitude.sensed_data = h;
          vertical_acceleration.sensed_data = g0 * std::cos(theta) + Za / masse;
          pitch_rate.sensed_data = q;
          vertical_speed.sensed_data = w * std::cos(theta) - u * std::sin(theta);
          true_airspeed.sensed_data = V;
          // State Equation
          u_dot = -g0 * std::sin(theta) - q * w + (Xa + T) / masse;
          w_dot = g0 * std::cos(theta) + q * u + Za / masse;
          q_dot = Ma / I_y;
          theta_dot = q;
          h_dot = u * std::sin(theta) - w * std::cos(theta);
          // Update State
          u += dt * u_dot;
          w += dt * w_dot;
          q += dt * q_dot;
          theta += dt * theta_dot;
          h += dt * h_dot;
        }
      }

    }
    namespace rosace {

      void Aircraft::Altitude_sensor_device::log_sensed_data()
      {
        logger.log_altitude(sensed_data);
      }

    }
    namespace rosace {

      void Aircraft::Vertical_acceleration_sensor_device::log_sensed_data()
      {
        logger.log_vertical_acceleration(sensed_data);
      }

    }
    namespace rosace {

      void Aircraft::Pitch_rate_sensor_device::log_sensed_data()
      {
        logger.log_pitch_rate(sensed_data);
      }

    }
    namespace rosace {

      void Aircraft::Vertical_speed_sensor_device::log_sensed_data()
      {
        logger.log_vertical_speed(sensed_data);
      }

    }
    namespace rosace {

      void Aircraft::True_airspeed_sensor_device::log_sensed_data()
      {
        logger.log_true_airspeed(sensed_data);
      }

    }
    namespace rosace {

      Aircraft aircraft;

    }
  }
}
