/******************************************************************************
 * Copyright (C) 2019, 2020  Tadeus Prastowo <0x66726565@gmail.com>           *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include "aircraft.hpp"
#include "controller.hpp"

namespace { // Reference inputs

  constexpr double h_c = 11000.0;
  constexpr double V_a_c = 0.0;

}
namespace { // Controller parameters

  namespace { // Altitude hold
    constexpr double Kp_h = 0.1014048;
    constexpr double Ki_h = 0.0048288;
    constexpr double Vz_c = -2.5;
  }

  namespace { // V_a
    constexpr double K1_intVa = 0.049802610664357;
    constexpr double K1_Va = -0.486813084356079;
    constexpr double K1_Vz = -0.077603095495388;
    constexpr double K1_q = 21.692383376322041;
  }

  namespace { // V_z
    constexpr double K2_intVz = 0.000627342822264;
    constexpr double K2_Vz = -0.003252836726554;
    constexpr double K2_q = 0.376071446897134;
    constexpr double K2_az = -0.001566907423747;
  }

}

extern const double init_h_s = 10000.0;
extern const double init_a_z_s = 0.000006963226284;
extern const double init_q_s = 0.0;
extern const double init_V_z_s = 0.0;
extern const double init_V_a_s = 230.0;
extern const double init_h_f = init_h_s;
extern const double init_a_z_f = init_a_z_s;
extern const double init_q_f = init_q_s;
extern const double init_V_z_f = init_V_z_s;
extern const double init_V_a_f = init_V_a_s;
extern const double init_h_h = f_h_h_impl(tice::v1::rosace::h_eq);
extern const double init_V_z = 0.012009615652468;
extern const double init_V_a = 1.586866079492600;

double f_h_s_impl()
{
  return tice::v1::rosace::aircraft.altitude.sample();
}

double f_a_z_s_impl()
{
  return tice::v1::rosace::aircraft.vertical_acceleration.sample();
}

double f_q_s_impl()
{
  return tice::v1::rosace::aircraft.pitch_rate.sample();
}

double f_V_z_s_impl()
{
  return tice::v1::rosace::aircraft.vertical_speed.sample();
}

double f_V_a_s_impl()
{
  return tice::v1::rosace::aircraft.true_airspeed.sample();
}

double f_h_f_impl(const double &h_s)
{
  static double x1 = 0.0, x2 = 0.0;
  static double x1_tmp = 0.0, x2_tmp = 0.0;
  static bool debut = true;
  /* 100 Hz coefficients */
  constexpr double a0 = 0.766000101841272, a1 = -1.734903205885821;
  constexpr double b0 = 0.014857648981438, b1 = 0.016239246974013;

  if (debut) {
    debut = false;
    x1 = tice::v1::rosace::h_eq * (1.0 + a1 - b1);
    x2 = tice::v1::rosace::h_eq;
  }
  // Output
  const double y = x2;
  // State
  x1_tmp = -a0 * x2 + b0 * h_s;
  x2_tmp = x1 - a1 * x2 + b1 * h_s;
  // Update
  x1 = x1_tmp;
  x2 = x2_tmp;

  return y;
}

double f_a_z_f_impl(const double &a_z_s)
{
  static double x1 = 0.0, x2 = 0.0;
  static double x1_tmp = 0.0, x2_tmp = 0.0;
  static bool debut = true;
  /* 100 Hz coefficient */
  constexpr double a0 = 0.411240701442774, a1 = -1.158045899830964;
  constexpr double b0 = 0.107849979167580, b1 = 0.145344822444230;

  if (debut) {
    debut = false;
    x1 = 0.0;
    x2 = 0.0;
  }
  // Output
  const double y = x2;
  // State
  x1_tmp = -a0 * x2 + b0 * a_z_s;
  x2_tmp = x1 - a1 * x2 + b1 * a_z_s;
  // Update
  x1 = x1_tmp;
  x2 = x2_tmp;

  return y;
}

double f_q_f_impl(const double &q_s)
{
  static double x1 = 0.0, x2 = 0.0;
  static double x1_tmp = 0.0, x2_tmp = 0.0;
  static bool debut = true;
  /* 100 Hz coefficients */
  constexpr double a0 = 0.766000101841272, a1 = -1.734903205885821;
  constexpr double b0 = 0.014857648981438, b1 = 0.016239246974013;

  if (debut) {
    debut = false;
    x1 = 0.0;
    x2 = 0.0;
  }
  // Output
  const double y = x2;
  // State
  x1_tmp = -a0 * x2 + b0 * q_s;
  x2_tmp = x1 - a1 * x2 + b1 * q_s;
  // Update
  x1 = x1_tmp;
  x2 = x2_tmp;

  return y;
}

double f_V_z_f_impl(const double &V_z_s)
{
  static double x1 = 0.0, x2 = 0.0;
  static double x1_tmp = 0.0, x2_tmp = 0.0;
  static bool debut = true;
  /* 100 Hz coefficients */
  constexpr double a0 = 0.956543675476034, a1 = -1.955578398054313;
  constexpr double b0 = 0.000479064865372430, b1 = 0.000486212556348925;

  if (debut) {
    debut = false;
    x1 = 0.0;
    x2 = 0.0;
  }
  // Output
  const double y = x2;
  // State
  x1_tmp = -a0 * x2 + b0 * V_z_s;
  x2_tmp = x1 - a1 * x2 + b1 * V_z_s;
  // Update
  x1 = x1_tmp;
  x2 = x2_tmp;

  return y;
}

double f_V_a_f_impl(const double &V_a_s)
{
  static double x1 = 0.0, x2 = 0.0;
  static double x1_tmp = 0.0, x2_tmp = 0.0;
  static bool debut = true;
  /* 100 Hz coefficients */
  constexpr double a0 = 0.956543675476034, a1 = -1.955578398054313;
  constexpr double b0 = 0.000479064865372430, b1 = 0.000486212556348925;

  if (debut) {
    debut = false;
    x1 = tice::v1::rosace::Va_eq * (1.0 + a1 - b1);
    x2 = tice::v1::rosace::Va_eq;
  }
  // Output
  const double y = x2;
  // State
  x1_tmp = -a0 * x2 + b0 * V_a_s;
  x2_tmp = x1 - a1 * x2 + b1 * V_a_s;
  // Update
  x1 = x1_tmp;
  x2 = x2_tmp;

  return y;
}

double f_h_h_impl(const double &h_f)
{
  constexpr double Ts_h = 1.0 / 50.0;
  static double integrator = 532.2730285;

  if ((h_f - h_c) < -50) {
    // Output
    return Vz_c;
  } else if ((h_f - h_c) > 50) {
    // Output
    return -Vz_c;
  }

  // Output
  const double y = Kp_h * (h_f - h_c) + Ki_h * integrator;
  // State
  integrator += Ts_h * (h_f - h_c);

  return y;
}

double f_V_z_impl(const double &h_h, const double &a_z_f,
                  const double &q_f, const double &V_z_f)
{
  constexpr double Ts_K2 = 1.0 / 50.0;
  static double integrator = 0.0;

  // Output
  const double y = K2_intVz * integrator + K2_Vz * V_z_f + K2_q * q_f + K2_az * a_z_f + tice::v1::rosace::delta_e_eq;
  // State
  integrator += Ts_K2 * (h_h - V_z_f);

  return y;
}

double f_V_a_impl(const double &q_f, const double &V_z_f,
                  const double &V_a_f)
{
  constexpr double Ts_K1 = 1.0 / 50.0;
  static double integrator = 0.0;

  // Output
  const double y = K1_intVa * integrator + K1_Va * (V_a_f - tice::v1::rosace::Va_eq) + K1_Vz * V_z_f + K1_q * q_f + tice::v1::rosace::delta_th_eq;
  // State
  integrator += Ts_K1 * (V_a_c - V_a_f + tice::v1::rosace::Va_eq);

  return y;
}

void f_L_impl(const double &V_z)
{
  tice::v1::rosace::aircraft.elevator.command(V_z);
}

void f_E_impl(const double &V_a)
{
  tice::v1::rosace::aircraft.engine.command(V_a);
}
