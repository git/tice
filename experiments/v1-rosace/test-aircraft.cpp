/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <iostream>
#include "aircraft.hpp"
#include "logger.hpp"

int main()
{
  using namespace tice::v1;

  rosace::aircraft.true_airspeed.sample();
  rosace::aircraft.vertical_acceleration.sample();
  rosace::aircraft.pitch_rate.sample();
  rosace::aircraft.vertical_speed.sample();
  rosace::aircraft.altitude.sample();

  rosace::logger.print(std::cout, std::cerr);
}
