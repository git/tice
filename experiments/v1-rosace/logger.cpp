/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <iomanip>
#include "aircraft.hpp"
#include "logger.hpp"

namespace tice {
  namespace v1 {
    namespace rosace {

      Logger logger;

    }
    namespace rosace {

      constexpr decltype(Logger::duration) Logger::duration;

      constexpr decltype(Logger::resolution) Logger::resolution;

      void Logger::start()
      {
        has_been_started = true;
      }

      void Logger::log_simulation_step(const typename Clock::time_point &sensing_time, const unsigned &simulation_forward_count)
      {
        static unsigned i = 0;

        if (!has_been_started) {
          i = 0;
        }

        if (i < simulation_data_size) {
          simulation_data[i].sensing_time = sensing_time;
          simulation_data[i].simulation_forward_count = simulation_forward_count;
          ++i;
        }
      }

#define generate(fn_ident, prm_ident)                           \
      void Logger::log_##fn_ident(const double &prm_ident)      \
      {                                                         \
        static unsigned invocation_count = 0;                   \
        static unsigned i = 0;                                  \
                                                                \
        if (!has_been_started) {                                \
          i = 0;                                                \
        }                                                       \
                                                                \
        invocation_count %= trace_skip_size;                    \
        if (!invocation_count++ && i < trace_data_size) {       \
          trace_data[traced_value::prm_ident][i++] = prm_ident; \
        }                                                       \
                                                                \
        if (!has_been_started) {                                \
          invocation_count = 0;                                 \
        }                                                       \
      }

      generate(altitude, h_s)
      generate(vertical_acceleration, a_z_s)
      generate(pitch_rate, q_s)
      generate(vertical_speed, V_z_s)
      generate(true_airspeed, V_a_s)
      generate(elevation_delta, L)
      generate(thrust_delta, E)

#undef generate

      void Logger::print(std::ostream &stdout, std::ostream &stderr)
      {
        stdout << std::setprecision(trace_data_fp_printout_scale)
               << std::fixed;
        stderr << std::setprecision(sensing_time_data_fp_printout_scale)
               << std::fixed;

        for (unsigned i = 0, j = 0, k = 0,
               i_end = has_been_started ? simulation_step_count : 1,
               k_end = has_been_started ? simulation_data_size : 1; i < i_end; ++i) {
          stderr << "Sensed at ";
          bool first_time = true;
          for (auto logical_time_interval_end = (i + 1) * simulation_step_size;
               k < k_end && simulation_data[k].sensing_time - simulation_data[0].sensing_time <= logical_time_interval_end;
               ++k) {
            stderr << (first_time ? (first_time = false, "") : ", ")
                   << std::chrono::duration_cast<std::chrono::duration<double>>(simulation_data[k].sensing_time
                                                                                - simulation_data[0].sensing_time).count()
                   << " (" << simulation_data[k].simulation_forward_count << ')';
          }
          stderr << '\n';

          if (i % trace_skip_size == 0) {
            stdout << std::chrono::duration_cast<std::chrono::duration<double>>(j * Logger::resolution).count() << ','
                   << trace_data[traced_value::V_a_s][j] << ','
                   << trace_data[traced_value::a_z_s][j] << ','
                   << trace_data[traced_value::q_s][j] << ','
                   << trace_data[traced_value::V_z_s][j] << ','
                   << trace_data[traced_value::h_s][j] << ','
                   << trace_data[traced_value::E][j] << ','
                   << trace_data[traced_value::L][j] << '\n';
            ++j;
          }
        }
      }

    }
  }
}
