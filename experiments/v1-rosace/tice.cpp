/******************************************************************************
 * Copyright (C) 2019  Tadeus Prastowo <0x66726565@gmail.com>                 *
 *                                            (GPG public key ID: 0x277B48A6) *
 *                                                                            *
 * This program is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation, either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * This program is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * You should have received a copy of the GNU General Public License          *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.      *
 ******************************************************************************/

#include <thread>
#include <cstring>
#include <iostream>
#include "tice_model.hpp"
#include "logger.hpp"

tice_v1_gen(prog, Prog);

int main()
{
  tice::v1::rosace::logger.start();

  prog p;

  p.run();
  if (p.get_error_code()) {
    std::cerr << "Error: " << std::strerror(p.get_error_code()) << '\n';
    return 1;
  }

  std::this_thread::sleep_for(tice::v1::rosace::Logger::duration
                              + tice::v1::rosace::Logger::resolution);

  p.stop();

  p.wait();

  tice::v1::rosace::logger.print(std::cout, std::cerr);

  return 0;
}
